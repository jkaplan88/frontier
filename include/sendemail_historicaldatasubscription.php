<?php

require_once('phpmailer/class.phpmailer.php');

$mail = new PHPMailer();

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if( $_POST['template-contactform-name'] != '' AND $_POST['template-contactform-email'] != '' AND $_POST['template-contactform-newuser'] != '' AND $_POST['template-contactform-newpass'] != '') {

        $name = $_POST['template-contactform-name'];
        $company = $_POST['template-contactform-company'];
        $email = $_POST['template-contactform-email'];
        $newuser = $_POST['template-contactform-newuser'];
        $newpass = $_POST['template-contactform-newpass'];
        $service = $_POST['template-contactform-service'];
        $subject = $_POST['template-contactform-subject'];
        $message = $_POST['template-contactform-message'];

        $subject = isset($subject) ? $subject : 'New Historical Weather Data Subscription Request';
        $subject2 = isset($subject2) ? $subject2 : 'New Historical Weather Data Subscription Request';
        $subject3 = isset($subject3) ? $subject3 : 'You Submitted a Frontier Weather Subscription Request';
        $sendertext = isset($sendertext) ? $sendertext : 'You Submitted the following information to Frontier Weather. Please allow up to 24 hours for a request to be processed.<br><br>';
  

        $botcheck = $_POST['template-contactform-botcheck'];

        $toemail = 'sstrum@frontierweather.com'; // Your Email Address
        $toname = 'Stephen Strum'; // Your Name

        if( $botcheck == '' ) {

            $mail->SetFrom( $email , $name );
            $mail->AddReplyTo( $email , $name );
            $mail->AddAddress( $toemail , $toname );
            $mail->Subject = $subject2;

            $name = isset($name) ? "Name: $name<br><br>" : '';
            $company = isset($company) ? "Company: $company<br><br>" : '';
            $email = isset($email) ? "Email: $email<br><br>" : '';
            $newuser = isset($newuser) ? "Requested Username: $newuser<br><br>" : '';
            $newpass = isset($newpass) ? "Requested Password: $newpass<br><br>" : '';
            $service = isset($service) ? "Subscription Package Requested: $service<br><br>" : '';
            $message = isset($message) ? "Message: $message<br><br>" : '';

            $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

            $body = "$name $company $email $newuser $newpass $service $message $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();

            if( $sendEmail == true ):
                echo 'We have <strong>successfully</strong> received your request. Please allow up to 24 hours for the request to be processed.';
            $mail->SetFrom( $toemail , $toname );
            $mail->AddReplyTo( $toemail , $toname );
            $mail->AddAddress( $email , $name );
	    $mail->addBCC( $toemail , $toname );
            $mail->Subject = $subject3;


            $body = "$sendertext $name $company $email $newuser $newpass $service $message $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();


            else:
                echo 'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '';
            endif;
        } else {
            echo 'Bot <strong>Detected</strong>.! Clean yourself Botster.!';
        }
    } else {
        echo 'Please <strong>Fill up</strong> all the Fields and Try Again.';
    }
} else {
    echo 'An <strong>unexpected error</strong> occured. Please Try Again later.';
}

?>