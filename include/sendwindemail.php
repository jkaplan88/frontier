<?php

require_once('phpmailer/class.phpmailer.php');

$mail = new PHPMailer();

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if( $_POST['template-contactform-name'] != '' AND $_POST['template-contactform-email'] != '' AND $_POST['template-contactform-company'] != '' ) {

        $name = $_POST['template-contactform-name'];
        $company = $_POST['template-contactform-company'];
        $email = $_POST['template-contactform-email'];
        $phone = $_POST['template-contactform-phone'];

        $subject = isset($subject) ? $subject : 'New Wind Trial Request Form Submitted';

        $botcheck = $_POST['template-contactform-botcheck'];

        $toemail = 'sstrum@frontierweather.com'; // Your Email Address
        $toname = 'Stephen Strum'; // Your Name

        if( $botcheck == '' ) {

            $mail->SetFrom( $email , $name );
            $mail->AddReplyTo( $email , $name );
            $mail->AddAddress( $toemail , $toname );
            $mail->Subject = $subject;

            $name = isset($name) ? "Name: $name<br><br>" : '';
            $company = isset($company) ? "Company: $company<br><br>" : '';
            $email = isset($email) ? "Email: $email<br><br>" : '';
            $phone = isset($phone) ? "Phone: $phone<br><br>" : '';

            $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

            $body = "$name $company $email $phone $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();

            if( $sendEmail == true ):
		header('location: http://www.frontierweather.com/windtrial/windsuccess.html');
             else:
                echo 'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '';
            endif;
        } else {
            echo 'Bot <strong>Detected</strong>.! Clean yourself Botster.!';
        }
    } else {
		header('location: http://www.frontierweather.com/windtrial/winderror.html');
     }
} else {
    echo 'An <strong>unexpected error</strong> occured. Please Try Again later.';
}

?>