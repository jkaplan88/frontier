<?php

require_once('phpmailer/class.phpmailer.php');

$mail = new PHPMailer();

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if( $_POST['template-contactform-name'] != '' AND $_POST['template-contactform-email'] != '' AND $_POST['template-contactform-message'] != '' ) {

        $name = $_POST['template-contactform-name'];
        $company = $_POST['template-contactform-company'];
        $email = $_POST['template-contactform-email'];
        $olduser = $_POST['template-contactform-olduser'];
        $oldpass = $_POST['template-contactform-oldpass'];
        $newuser = $_POST['template-contactform-newuser'];
        $newpass = $_POST['template-contactform-newpass'];
        $subject = $_POST['template-contactform-subject'];
        $message = $_POST['template-contactform-message'];

        $subject = isset($subject) ? $subject : 'New Message From Contact Form';

        $botcheck = $_POST['template-contactform-botcheck'];

        $toemail = 'sstrum@frontierweather.com'; // Your Email Address
        $toname = 'Stephen Strum'; // Your Name

        if( $botcheck == '' ) {

            $mail->SetFrom( $email , $name );
            $mail->AddReplyTo( $email , $name );
            $mail->AddAddress( $toemail , $toname );
            $mail->Subject = $subject;

            $name = isset($name) ? "Name: $name<br><br>" : '';
            $company = isset($company) ? "Company: $company<br><br>" : '';
            $email = isset($email) ? "Email: $email<br><br>" : '';
            $olduser = isset($olduser) ? "Existing Username: $olduser<br><br>" : '';
            $oldpass = isset($oldpass) ? "Existing Password: $oldpass<br><br>" : '';
            $newuser = isset($newuser) ? "New Username: $newuser<br><br>" : '';
            $newpass = isset($newpass) ? "New Password: $newpass<br><br>" : '';
            $message = isset($message) ? "Message: $message<br><br>" : '';

            $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

            $body = "$name $company $email $olduser $oldpass $newuser $newpass $message $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();

            if( $sendEmail == true ):
                echo 'We have <strong>successfully</strong> received your request. Please allow up to 24 hours for the request to be processed.  We will notify you when the new username/password is active.';
            else:
                echo 'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '';
            endif;
        } else {
            echo 'Bot <strong>Detected</strong>.! Clean yourself Botster.!';
        }
    } else {
        echo 'Please <strong>Fill up</strong> all the Fields and Try Again.';
    }
} else {
    echo 'An <strong>unexpected error</strong> occured. Please Try Again later.';
}

?>