<?php

require_once('phpmailer/class.phpmailer.php');

$mail = new PHPMailer();


if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if( $_POST['template-contactform-name'] != '' AND $_POST['template-contactform-email'] != '' AND $_POST['template-contactform-newuser'] != '' AND $_POST['template-contactform-newpass'] != '' AND $_POST['template-contactform-service'] != '') {

        $name = $_POST['template-contactform-name'];
        $company = $_POST['template-contactform-company'];
        $address = $_POST['template-contactform-address'];
        $city = $_POST['template-contactform-city'];
        $state = $_POST['template-contactform-state'];
        $zip = $_POST['template-contactform-zip'];
        $country = $_POST['template-contactform-country'];
        $email = $_POST['template-contactform-email'];
        $newuser = $_POST['template-contactform-newuser'];
        $newpass = $_POST['template-contactform-newpass'];
        $service = $_POST['template-contactform-service'];
        $emaillist = $_POST['template-contactform-emaillist'];
        $modelmaillist = $_POST['template-contactform-modelmaillist'];
        $agreementyes = $_POST['template-contactform-agreementyes'];
        $subject = $_POST['template-contactform-subject'];
        $message = $_POST['template-contactform-message'];
 
  
        $subject = isset($subject) ? $subject : 'New Message From Contact Form';
        $subject2 = isset($subject2) ? $subject2 : 'New Weather Subscription Request';
        $subject3 = isset($subject3) ? $subject3 : 'You Submitted a Frontier Weather Subscription Request';
        $subthrough = isset($subthrough) ? $subthrough : date('l jS F (Y-m-d)', strtotime('+8 days'));
        $sendertext = isset($sendertext) ? $sendertext : 'You submitted the following request for trial access to WDTs Frontier products. Please allow up to 24 hours for your trial account request to be processed. <br><br>';

        $botcheck = $_POST['template-contactform-botcheck'];

        $toemail = 'sstrum@frontierweather.com'; // Your Email Address
        $toname = 'Stephen Strum'; // Your Name

        if( $botcheck == '' ) {

            $mail->SetFrom( $email , $name );
            $mail->AddReplyTo( $email , $name );
	    $mail->addBCC( $toemail , $toname );
            $mail->AddAddress( $email , $name );
            $mail->Subject = $subject3;
				
            $name = isset($name) ? "Name: $name<br><br>" : '';
            $company = isset($company) ? "Company: $company<br><br>" : '';
            $address = isset($address) ? "Address: $address<br><br>" : '';
            $city = isset($city) ? "Address: $city" : '';
            $state = isset($state) ? "$state " : '';
            $zip = isset($zip) ? "$zip<br><br>" : '';
            $country = isset($country) ? "Country: $country<br><br>" : '';
            $email = isset($email) ? "Email: $email<br><br>" : '';
            $newuser = isset($newuser) ? "Requested Username: $newuser<br><br>" : '';
            $newpass = isset($newpass) ? "Requested Password: $newpass<br><br>" : '';
            $service = isset($service) ? "Subscription Package Requested: $service<br><br>" : '';
            $emaillist= isset($emaillist) ? "Add to email list for reports: $emaillist<br><br>" : '';
            $modelmaillist = isset($modelmaillist) ? "Add to email list for models: $modelmaillist<br><br>" : '';
            $agreementyes = isset($agreementyes) ? "Agrees to subscriber agreement: $agreementyes<br><br>" : '';
            $message = isset($message) ? "Message: $message<br><br>" : '';
            
            $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

            $body = "$sendertext Once activated, your subscription will be valid through $subthrough <br><br> $name $company $address $city $state $zip $country $email $newuser $newpass $service $emaillist $modelmaillist $agreementyes $message $referrer";

            $mail->MsgHTML( $body );
            $sendEmail = $mail->Send();

            if( $sendEmail == true ):
                echo 'We have <strong>successfully</strong> received your request. Please allow up to 24 hours for the request to be processed.';

            else:
                echo 'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '';
            endif;
        } else {
            echo 'Bot <strong>Detected</strong>.! Clean yourself Botster.!';
        }
    } else {
        echo 'Please <strong>Fill up</strong> all the Fields and Try Again.';
    }
} else {
    echo 'An <strong>unexpected error</strong> occured. Please Try Again later.';
}

?>