<?php

// Desired folder structure

$dirname = $_POST['dirfield'];

if( !ctype_alnum( $dirname ) )
   die( 'Invalid characters, no special characters allowed in user name!' );

$structure = "../{$dirname}";
$fullpath = "frontierweather.com/custom/{$dirname}/";

// To create the nested structure, the $recursive parameter 
// to mkdir() must be specified.

if (!mkdir($structure, 0777, true)) {
    die('Failed to create folder...');
}
else {
    echo "{$fullpath} folder has been created...will return to setup form in a few seconds";
}
copy('../index.html', "{$structure}/index.html");



redirect('index.html', false);

function redirect($url, $statusCode = 303)
{

   header('Refresh: 5; ' . $url, true, $statusCode);
   die();
}

// ...
?>
