<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id$
*
*/

require_once("SimpleHTTPClient.php");

class ssi {

	var $sunparams = array();
	var $debug=0;

	var $cfg;
	var $var = array();


	Function ssi (&$cfg, &$var, $debug) {
		$this->cfg = $cfg;
		$this->var = $var;
		$this->debug = $debug;


	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}


	Function parse_line(&$line, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {
		if ($post_parse) {
			$line = preg_replace('/(.*)<!--\#include\s+file="(.+)"\s*-->(.*)/e', "\$this->_ssi_include_file('$1', '$3', '$2', \$template, \$print_now, \$save_file_fh, \$pm, \$extra_parse, \$hashes);", $line);

			$line = preg_replace('/(.*)<!--\#include\s+virtual="(.+)"\s*-->(.*)/e', "\$this->_ssi_include_virtual('$1', '$3', '$2', \$template, \$print_now, \$save_file_fh, \$pm, \$extra_parse, \$hashes);", $line);

			$line = preg_replace('/(.*)<!--\#exec\s+cmd="(.+)"\s*-->(.*)/e', "\$this->_ssi_include_virtual('$1', '$3', '$2', \$template, \$print_now, \$save_file_fh, \$pm, \$extra_parse, \$hashes);", $line);

		}
		return array($line, $pm);
	}



	function _ssi_include_file($before, $after, $ssi_file, &$template, $print_now, &$fh, $pm, &$extra_parse, &$hashes) {
		$debug = $this->debug;

		$include_str = '';
		// include before string
		if ($print_now) {$template->parse_line($before, 1, $fh, $pm, $extra_parse, $hashes);}
		else {$include_str = $template->parse_line($before, 0, $fh, $pm, $extra_parse, $hashes);}

		if (file_exists($ssi_file)) {
			if ($fc = file($ssi_file)) {
				$pline = '';
				foreach ($fc as $line) {
					if (preg_match('/^(.*)__\s*$/', $line, $m)) { $pline .= $m[1]; }
					else {
						if ($print_now) {$template->parse_line($pline.$line, 1, $fh, $pm, $extra_parse, $hashes);}
						else {$include_str .= $template->parse_line($pline.$line, 0, $fh, $pm, $extra_parse, $hashes);}
						$pline='';
					}
				}
			}
			else {
				if ($print_now) { print "ERROR processing HWSSI directive. ";}
				else { $include_str .= "ERROR processing HWSSI directive. "; }
			}
		}
		else {
			if ($print_now) { print "ERROR processing HWSSI directive. ";}
			else { $include_str .= "ERROR processing HWSSI directive. "; }
		}

		if ($print_now) {$template->parse_line($after, 1, $fh, $pm, $extra_parse, $hashes);}
		else {$include_str .= $template->parse_line($after, 0, $fh, $pm, $extra_parse, $hashes);}


   		return $include_str;
   	}




	function _ssi_include_virtual($before, $after, $include_url, &$template, $print_now, &$fh, $pm, &$extra_parse, &$hashes) {
		$debug = $this->debug;

		$include_str = '';
		// include before string
		if ($print_now) {$template->parse_line($before, 1, $fh, $pm, $extra_parse, $hashes);}
		else {$include_str = $template->parse_line($before, 0, $fh, $pm, $extra_parse, $hashes);}

		$domain  = '';
		if (preg_match('!^(https?)://([^/]+)(.*)$!i', $include_url, $m)) {
			$port = ((strtolower($m[1])) == 'https') ? 443 : 80;
			$domain = $m[2];
			$include_url = $m[3];
			$include_url = preg_replace('!/+!', '/', $include_url);

			if (preg_match('/:(\d+)$/', $domain, $m)) {
				$port = $m[1];
				$domain = preg_replace('/:(\d+)$/', '', $domain);
			}

			$cfg = $this->cfg;

			$http_mode = $cfg->val('SystemSettings', 'http_mode');
			$http_timeout = $cfg->val('SystemSettings', 'http_timeout');
			if (!$http_timeout) $http_timeout = 10;

			$output = '';
			$output =& HTTPGet($include_url, $domain, $port, '', $cfg->val('SystemSettings', 'proxy_server'),
						    $cfg->val('SystemSettings', 'proxy_user'),
             					    $cfg->val('SystemSettings', 'proxy_pw'), $debug, $http_timeout);


			if ($print_now) {$template->parse_line($output, 1, $fh, $pm, $extra_parse, $hashes);}
			else {$include_str .= $template->parse_line($output, 0, $fh, $pm, $extra_parse, $hashes);}
		}

		if ($print_now) {$template->parse_line($after, 1, $fh, $pm, $extra_parse, $hashes);}
		else {$include_str .= $template->parse_line($after, 0, $fh, $pm, $extra_parse, $hashes);}

		return $include_str;
	}





}

?>