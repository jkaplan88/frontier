<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: CurlHTTPClient.php,v 1.3 2006/06/27 11:06:26 wxfyhw Exp $
*
*/

function  HTTPGet ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=10) {

	$data =& fetch_http($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug);
	$buf =& $data[0];
//     ini_set('user_agent','MSIE 4\.0b2;');
//     $dh = fopen("http://$hostname$url",'r');
//     $result = fread($dh,8192);
//	$buf =& $result;


	if ($buf == '') {
		if ($debug) {print "Error fetching http<br>\n";}
		$buf = '-hwERR Empty Page Returned\n';
	}
	else {
		if ($debug) print "HTTP fetched<br>\n";
	}
	return $buf;
}


function  HTTPHead ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=10) {
	$data =& fetch_http($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug);

	$buf =& $data[1];

	if ($buf == '') {
		if ($debug) {print "Error fetching http<br>\n";}
		$buf = '-hwERR Empty Page Returned\n';
	}
	else {
		if ($debug) print "HTTP fetched<br>\n";
	}
	return $buf;
}

function fetch_http ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=10) {

	if ($port == '') $port = 80;

	if (!preg_match('/^http/i', $url)) {
		if (!preg_match('/^\//', $url)) $url = "/$url";
		$url = "http://$hostname$url";
	}
//$url = preg_replace('/adds.aviationweather.noaa.gov/', 'www.hamweather.com', $url);

	if ($debug) print "Curl fetching $url...timeout=$timeout<br>\n";



	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, 'HAMweather 3.98');
	curl_setopt ($ch, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result=curl_exec ($ch);
	curl_close ($ch);

if ($debug) print "result=$result<br>\n";
if (preg_match('/Cannot find product/i', $result)) $result='';

	return array ($result, '');


}





?>