<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchZandH.php,v 1.58 2006/12/20 05:47:30 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");
require_once("hamlib/HW3Plugins/JDay.php");
require_once("hamlib/HW3Plugins/FetchSunInfo.php");

class FetchZandH {

	var $cfg;

	var $found_zone;
	var $tzname;
	var $tzdif;

	var $state;

	var $found_hourly;

	//Objects
	var $objs = array('_zone', 'taf2zone', 'MetarDecoded');
	var $_zone=NULL;
	var $MetarDecoded=NULL;
	var $taf2zone=NULL;

	var $debug = false;

	var $hforecastdate ='';
	var $hforecastdate24 = '';
	var $hhour = 'N/A';
	var $hhour24h = 'N/A';
	var $htime = 'N/A';

	var $htime24h = '';
	var $hdate = 'N/A';
	var $hepoch='';

	var $core_path ='';

	var $vars = array();
	var $form = array();


	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	Function FetchZandH($core_path) {
		$this->core_path = $core_path;
		$this->tzdif=0;
		$this->tzname='';
	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function pcounties($num, $state="") {
		$place = $this->_zone->counties[$num];
		if (preg_match('/\W\w\w$/', $place)) {
			$place = preg_replace('/ (\w\w)$/', ' county $1', $place);
		}
		elseif ($state) {

		if (!stristr ($place, ' county')) $place .= ' county';
			$place .= ' ' . $this->state;
		}

		$place = preg_replace('/ county la\b/i', ' parish LA', $place);

		return $this->pretty_word($place);

	}

	Function pcities($num, $state='') {
		$place = $this->_zone->cities[$num];
		if ($state<> '' && !preg_match('/\W\w\w$/', $place)) $place .= ' ' . $this->state;
		return $this->pretty_word($place);
	}


	Function pretty_word ($word) {
		$word = preg_replace("/([�������\w]+)/e","ucwords(strtolower('$1'))", $word);
		$word = preg_replace('/ (\w\w)\s*$/e', "' ' . ucwords('$1')", $word);

		return $word;
	}


	Function get_taf(&$var, &$form, &$cfg, $ma, $t, $icao, $code, $debug){

		$found =0;
		if ($code >3) {

//			$cache_path = $cfg->val('Paths', 'cache');
//			if (!$cache_path) { $cache_path = 'cache'; }
//			elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
			$cache_path =$cfg->val('Paths', 'cache');
			if (!$cache_path) { $cache_path =  'cache'; }
			if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

			$icao = strtoupper($icao);

			$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'taf_cache_file'));
			if (!$cache_file) {$cache_file = $cache_path . strtolower("/taf-$icao.html"); }

			$cache_file2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'taf_cache_file2'));
			if (!$cache_file2) {$cache_file2 = '';}

			$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'taf_prefix'));
			$url = '/'.$icao;

			//Call Fetch Forcast
			$data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,$url,
				$cfg->val('URLs', 'taf_url'),
				$prefix,
				$cfg->val('URLs', 'taf_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
			list($which_used, $wx_data) = $data;

			if ($wx_data == '' || preg_match('/-hwERR|404.+Not Found/is',$wx_data)) {
				if ($this->debug) { "Error fetching data $wx_data<br>\n"; }
				$found=0;
			}
			else {
				require_once('hamlib/HW3Plugins/Wx/TAF2Zone.php');

				$this->encoded_taf = $wx_data;
				$this->taf2zone = new TAF2Zone($debug);
				$this->taf2zone->tz($this->tzdif,$this->tzname);

	              		//$wx_types =& $cfg->Parameters('WxInfo');
	              		$wx_types =& $cfg->SectionParams('WxInfo');
	              		$phrases =& $cfg->SectionParams('TAF Phrases');
	              		$wx_descript=& $cfg->SectionParams('TAF WxDescript');
	              		$sky_cond =& $cfg->SectionParams('TAF SkyCond');
	              		$cloud_types =& $cfg->SectionParams('TAF CloudTypes');
	              		$this->taf2zone->process_taf($wx_data,$wx_types,  $phrases, $wx_descript, $sky_cond, $cloud_types);

	              		$found = 1;
	              	}
              	}

              	$this->found_taf = $found;
              	return $found;;


	}




	Function do_taf(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$this->cfg = $cfg;

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		if (isset($form['icao'])) { $icao = $form['icao']; }
		elseif (isset($var['icao'])) { $icao = $var['icao']; }
		else { $icao='';}

		$alt_zone_info = (isset($var['alt_zone_info'])) ? $var['alt_zone_info'] : '';
		if ($icao) $alt_zone_info = "$icao:4%$alt_zone_info";
		$found = 0;

		$azones = explode('%', $alt_zone_info);
		foreach ($azones as $zone_area) {
			list($icao, $code) = explode(':', $zone_area);
			if ($code != 4) continue;

			$found = $this->get_taf($var, $form, $cfg, $ma, $t, $icao, $code, $debug);

              		if ($found) break;
              	}

              	$this->found_taf = $found;
              	return $t;
          }



	Function do_zone(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		if (strtolower($var['country']) == 'ca') {return $this->do_zone_ca($var, $form, $forecast,$cfg, $forecast_info, $debug);}

		$this->cfg = $cfg;

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		//Grab place info from var hash
		$country = $var["country"];
		$state = $var["state"];
		$place = $var["place"];
		$alt_zone_info = $var["alt_zone_info"];

		if (!$country) {$country = 'us';}

		$this->state = strtoupper($state);

//		$cache_path = $cfg->val('Paths', 'cache');
//		if (!$cache_path) { $cache_path = 'cache'; }
//		elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found = 0;

		if (isset($form['icao'])) { $icao = $form['icao']; }
		elseif (isset($var['icao'])) { $icao = $var['icao']; }
		else { $icao='';}
		if ($icao && !preg_match("/$icao:4/i", $alt_zone_info)) $alt_zone_info .= "%$icao:4";

		$zone_areas = explode("%", $alt_zone_info);
if ($debug) print "DEBUG: alt_zone_info=$alt_zone_info<br>\n";

		while(list(, $zone_area) = each($zone_areas)) {
			if ($zone_area == '') continue;
			$temp_array = explode(":", $zone_area);

			$alt_info = $temp_array[0];
			$code = (isset($temp_array[1])) ? $temp_array[1] : '2';

			$find_place = "";
			$find_zone = "";
			$find_state = "";

			//We are looking for a us zone forecast
			if ($code < 4) {
				if ($code < 2) {
              				// means alt_info is a place name
               				$find_place = $alt_info;
            			}
				elseif(preg_match('/^(?:([a-zA-Z][a-zA-Z])[Zz]?)?(\d+)$/', $alt_info, $m)) {
					$find_state = strtolower($m[1]);
					$find_zone = strtolower($m[2]);
				}
				else { next;}

				$cwa = (isset($form['cwa'])) ? $form['cwa'] : ( (isset($var['cwa'])) ? $var['cwa'] : '');
				$cwa = preg_replace('/\W/', '', strtoupper($cwa));

				if (!$find_state) {
					$find_state = strtolower($state);
				}
            			$use_find_state = ($find_state == 'dc') ? 'va' : $find_state;

				$find_place = $find_zone ? sprintf('%03u', $find_zone) : $find_place;
				$find_place = preg_replace('/\W/', "", $find_place);

				$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'zone_cache_file'));
				if (!$cache_file) {$cache_file = $cache_path . strtolower("/$country-$find_state-zone.txt"); }

				$cache_file2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'zone_cache_file2'));
				if (!$cache_file2) {$cache_file2 = $cache_path . strtolower("/$country-$find_state-$find_place-zone.txt");}

				$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'zone_prefix'));

				//Call Fetch Forcast
				$data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
					$cfg->val('URLs', 'zone_url'),
					$prefix,
					$cfg->val('URLs', 'zone_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
				list($which_used, $wx_data) = $data;

				if ($wx_data == '') {
					continue;
				}

				require_once("hamlib/HW3Plugins/Wx/Zone/USA.php");
				$this->_zone = new zone($debug);
				$found_zone = $this->_zone->find_zone($find_place, $find_zone, $find_state, $wx_data);

				if ($found_zone) {
					$this->state = strtoupper($find_state);
					if ($which_used != 2 && $data[2] != $data[3]) {
                 					save_file($data[3], "\n\n" .$this->_zone->header. "\n\n." . $this->_zone->forecast_text . "\n\n\$\$\n\n");
               				}


               				$wxinfo =& $cfg->Parameters('WxInfo');
               				if (isset($form['daysonly'])) {$daysonly = $form['daysonly']; }
               					else {$daysonly=$cfg->val('Defaults', 'daysonly'); }
               				$temp ='';
               				$this->_zone->tzdif = $this->tzdif;
               				$this->_zone->process_zone($wxinfo, $daysonly, $temp, '');
               				$found = 1;
               				$this->taf2zone=NULL;
               				break;
				}
				else {
//               				save_file($cache_file2, "\n\nNOT AVBL\n\n\$\$\n\n");
            			}
			}
			else {
				$found =  $this->get_taf($var, $form, $cfg, $ma, $t, $icao, $code, $debug);
				if ($found) { $this->_zone = NULL; break;}
	         	}


		}

		$this->found_zone = $found;

		return $t;
	}



	Function do_zone_ca(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		//if ($var['country'] != 'ca') return $this->do_zone($var, $form, $forecast,$cfg, $forecast_info, $debug);

		$this->cfg = $cfg;

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		//Grab place info from var hash
		$country = $var["country"];
		$state = $var["state"];
		$place = $var["place"];
		$alt_zone_info = $var["alt_zone_info"];

		if (!$country) {$country = 'ca';}

		$this->state = strtoupper($state);

//		$cache_path = $cfg->val('Paths', 'cache');
//		if (!$cache_path) { $cache_path = 'cache'; }
//		elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found = 0;
		if (isset($form['icao'])) { $icao = $form['icao']; }
		elseif (isset($var['icao'])) { $icao = $var['icao']; }
		else { $icao='';}
		if ($icao && !preg_match("/$icao:4/i", $alt_zone_info)) $alt_zone_info .= "%$icao:4";

		$zone_areas = explode("%", $alt_zone_info);
		foreach ($zone_areas as $zone_area) {
			$za = explode(":", $zone_area);
			$alt_info = $za[0];
			if (isset($za[1])) { $alt_extended = $za[1]; } else {$alt_extended='';}
			if (isset($za[2])) { $code = $za[2]; } else {$code='';}

			if (!$code && $alt_extended > 2) $code = $alt_extended;

			$find_place = "";
			$find_zone = "";
			$find_state = "";
			if ($code < 4) {
				$alt_items = explode('--', $alt_info);
				$alt_info_url = (isset($alt_items[0])) ? $alt_items[0] : '';
				$alt_info_place = (isset($alt_items[1])) ? $alt_items[1] : '';
//				list($alt_info_url, $alt_info_place) = explode('--', $alt_info);


				$find_place = ($alt_info_place) ? $alt_info_place: $place;
				$find_state = strtolower($state);
//print "alt_zone_info=$alt_zone_info<br>find_place=$find_place<br>\n";
				$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'ca_zone_cache_file'));
				if (!$cache_file) {
					$cache_file = $cache_path . preg_replace('/\s+/', '_', strtolower("/$country-$alt_info_url")) ;
				}
				$cache_file2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'ca_zone_cache_file2'));
				if (!$cache_file2) {
					$cache_file2 = $cache_path .  preg_replace('/\s+/', '_', "/$country-$find_state-$find_place-$iwin_file");
				}


				$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'ca_zone_prefix'));
				//Call Fetch Forcast
				list($which_used, $wx_data) = $data  = &fetch_forecast($cfg, $ma, $cache_file, $cache_file2,$alt_info_url,
					$cfg->val('URLs', 'ca_zone_url'),
					$prefix,
					$cfg->val('URLs', 'ca_zone_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

				if ($wx_data == '') {
					return $t;
				}

				require_once("hamlib/HW3Plugins/Wx/Zone/Canada.php");
				$this->_zone = new CAzone($debug);
				$found_zone = $this->_zone->find_zone($find_place, 0, $find_state, $wx_data);

				if ($found_zone) {
					if ($which_used != 2 && $alt_extended) {
						list($ext_info_url, $ext_info_place) = explode('--', $alt_extended);
						$find_place = ($ext_info_place) ? $ext_info_place : $place;

						$fc_file = $cache_path . preg_replace('/\s+/', '_', strtolower("/$country-$ext_info_url"));

						list($which_used, $wx_data) = $data  = &fetch_forecast($cfg, $ma, $fc_file, '',$ext_info_url,
							$cfg->val('URLs', 'ca_zone_url'),
							$prefix,
							$cfg->val('URLs', 'ca_zone_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

						$extended = new CAzone($debug);
						$found_ext = $extended->find_zone($find_place, 0, $find_state, $wx_data);
						if ($found_ext) {
							$this->_zone->set('forecast_text', $this->_zone->forecast_text . "\n" . $extended->forecast_text . "\n\n");
						}
					}

					if ($which_used != 2) {
						save_file($cache_file2, "\n\n".$this->_zone->header . $this->_zone->forecast_text."\n\nEnd\n\n");
					}

               				$wxinfo =& $cfg->Parameters('WxInfo');
               				if (isset($form['daysonly'])) {$daysonly = $form['daysonly']; }
               					else {$daysonly=$cfg->val('Defaults', 'daysonly'); }
               				$temp='';
               				$this->_zone->process_zone($daysonly, $temp, $wxinfo);
               				$found = 1;
               				$this->taf2zone = NULL;
               				break;
               			}
//               			else {
//               				save_file($cache_file2, "\n\nNOT AVBL\n\n\$\$\n\n");
//               			}
               		}
               		else {
				$found =  $this->get_taf($var, $form, $cfg, $ma, $t, $icao, $code, $debug);
				if ($found) { $this->_zone = NULL; break;}
               		}
               	}

               	$this->found_zone = $found;
//print "found=$found<br>\n";
               	return $t;
	}



	Function hourly(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$this->cfg =& $cfg;
		$this->vars =& $var;
		$this->form =& $form;

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

   		$error = "";


		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


   		$country = strtolower($var["country"]);
   		$state = $var["state"];
   		$place = $var["place"];
   		$alt_cc_info = $var["alt_cc_info"];

   		if (!$country) {$country = "us";}
   		$state = strtolower($state);

   		$hplace = ""; $hstate = ""; $hcountry = "";

//		$cache_path = $cfg->val('Paths', 'cache');
//		if (!$cache_path) { $cache_path = 'cache'; }
//		elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;


		$found_hourly = 0;
		$this->found_hourly = $found_hourly;
		$find_place = ""; $code = ""; $find_state = "";
		$cc_areas = explode("%", $alt_cc_info);

		foreach ($cc_areas as $cc_area) {
			$temp_array = explode(":", $cc_area);
			$find_place = $temp_array[0];
			$code = (isset($temp_array[1])) ? $temp_array[1] : '';


			if ($code == 2 || $code == 3) {
				$find_place = preg_replace('/\W/', "", strtoupper($find_place));

				$forecast_info = explode(",", $cfg->Val('ForecastTypes', 'metar'));
				$iwin_file = $forecast_info[0];
				$ma = $forecast_info[1];
				$t = $forecast_info[2];

				$var["icao"] = $find_place;

				$find_place = strtolower($find_place);
				$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'metar_cache_file'));
				if (!$cache_file) {$cache_file = $cache_path . strtolower("/metar-$find_place.html"); }

				$cache_file2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'metar_cache_file2'));
				if (!$cache_file2) {$cache_file2 = $cache_path . strtolower("/metar-$find_place-small.html");}
				$find_place = strtoupper($find_place);


				$kprefix = 'metar_prefix'; $kpostfix='metar_postfix'; $kurl='metar_url';
				$pws = (strlen($find_place) > 4) ? true : false;
				if ($pws && $cfg->val('URLs', 'pws_url')) {
					$kprefix = 'pws_prefix';
					$kpostfix  = 'pws_postfix';
					$kurl = 'pws_url';
				}
				elseif ($country != 'us' && $cfg->val('URLs', 'metar_url_int')) {
					$kprefix = 'metar_prefix_int';
					$kpostfix  = 'metar_postfix_int';
					$kurl = 'metar_url_int';
				}


				$prefix = preg_replace("/%%(\w+)%%/e", '$$1',  $cfg->Val('URLs', $kprefix));




				 $isold = false;
				 $used_db = false;
				 if ($cfg->val('METAR', 'usedb') && !$pws) {
				 	require_once("hamlib/HW3Plugins/FetchMetarFromDB.php");
				 	$metardb = new FetchMetarFromDB($cfg, $debug);
				 	$t = preg_replace('/$find_place:[23]/i', '', $alt_cc_info);
				 	$icao2 = ( preg_match('/\b(\w\w\w\w):[23]/', $t, $mm)) ? $mm[1] : '';
				 	list ($error, $isold, $tfind_place) = $metardb->fetch_metar($find_place, $icao2);
				 	if ($tfind_place) $find_place = $tfind_place;
				 	if ($error && $debug) print "Error fetching METAR from DB <br />";

				 	if (!$error && !$isold) {
				 		$used_db = 1;
				 		$found_hourly=1;

				 		$junk = '';
				 		$wx_data =& $junk;

				 		$hplace = $metardb->getValue('hplace');
				 		$hstate = $metardb->getValue('hstate');
				 		$hcountry = $metardb->getValue('hcountry');
				 		$continue_processing = false;
				 	}
				}




				if (!$used_db || $pws) {


					//Call Fetch Forcast
					$data =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
						$cfg->val('URLs', $kurl) ,
						$prefix,
						 $cfg->val('URLs', $kpostfix ) ,
						$cfg->val('SystemSettings', 'proxy_port'), $debug,0);

					list($which_used, $wx_data) = $data;

					if (preg_match('/Multiple Choices/', $wx_data) || preg_match('/-hwError/', $wx_data)) { $wx_data = '';}
				}



				if ($used_db && !$pws) {
					$this->MetarDecoded = $metardb;
				}
				elseif ($wx_data != '') {


					if (preg_match('/<current_observation/', $wx_data)) {
						require_once("hamlib/HW3Plugins/Wx/Currents/NOAAXML.php");
						$wxinfo =& $cfg->SectionParams('NOAAIconWx');
						$this->MetarDecoded = new CurrentsNOAAXML($wxinfo);
						$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
					}
					elseif (preg_match('/adds\.aviationweather/', $wx_data)) {
						require_once("hamlib/HW3Plugins/Wx/Currents/NOAAADDS.php");
						$this->MetarDecoded = new CurrentsNOAAADDS();
						$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
					}
					else {
						require_once("hamlib/HW3Plugins/Wx/MetarDecoded.php");
						$this->MetarDecoded = new MetarDecoded();
						$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
					}
				}

				if (!$used_db) {

					if ($which_used != 2 && $data[2] != $data[3]) {
						$found_hourly = $this->MetarDecoded->process_metar($find_place, getKeyVal($form, "type", ""), $wx_data);
						if ($found_hourly && $cache_file != $cache_file2) {
							save_file($data[3], "PLACE: $find_place, ".$this->MetarDecoded->hplace.', '.$this->MetarDecoded->hstate.', '.$this->MetarDecoded->hcountry . "\n" . $this->MetarDecoded->hforecastdate .
								"\nLINE: " . $this->MetarDecoded->decoded_line . "\nMETAR: " . $this->MetarDecoded->metar);
						}
					}
					else {
						$ttype = isset($form['type']) ? $form['type'] : 0;
						$found_hourly = $this->MetarDecoded->process_short_metar($find_place, $ttype, $wx_data);
					}
				}

					$this->set = "true";
					$this->found_hourly = $found_hourly;
			         	if ($found_hourly) {
						$hdate = $this->MetarDecoded->hforecastdate;
						if (preg_match('/(.*)?(\d\d)(\d\d) UTC(.*)/', $hdate, $matches)) {
							$hour = $matches[2]; $min=$matches[3];
							$tdate = ($matches[4]) ? $matches[4] : $matches[1];
							$this->hhour = $this->fixtzhr($hour);
							$this->hhour24h = $this->fixtzhr($hour,'','',24);
							$this->htime = $this->fixtzhr($hour, $min,'',0);
							$this->htime24h = $this->fixtzhr($hour, $min,'',24);
							$this->hdate = trim($tdate);

							if (preg_match('/(\d\d\d\d)\.(\d\d).(\d\d)/', $this->hdate, $m)) {
								$this->hepoch = gmmktime($hour,$min,0,$m[2],$m[3],$m[1]);
							}



						}
						else {
							$this->hhour = $this->hhour24 = $this->htime = $this->htime24 = $this->hdate = 'N/A';
						}

						$this->hforecastdate = $this->local_metar_date($hdate);
						$this->hforecastdate24h = $this->local_metar_date($hdate,24);

						if (preg_match('/\d\d\d\d\.\d\d.(\d\d)/', $this->hdate, $m) && abs(gmdate('d') - $m[1]) > 1) {
							continue;
						}
						else {
							break;

						}
					}

			} //end if




		} //while cc_areas



		return $t;
	}

	Function zandh(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$t = "";

		$this->cfg = $cfg;

		$t_fi = explode(',',$cfg->val('ForecastTypes', 'metar'));
		$th = $this->hourly($var, $form, "hourly", $cfg, $t_fi, $debug);

		$t_fi = explode(',',$cfg->val('ForecastTypes', 'zone'));
		$tz = $this->do_zone($var, $form, "zone", $cfg, $t_fi, $debug);
		$temp_array = explode(",", $cfg->val('ForecastTypes', 'zandh'));
		$t = $temp_array[2];
		return $t;


	}

	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = NULL;

		if ($index <> '') {

			if (isset($this->$key)) {
				$tmp =& $this->$key;
				if(isset($tmp[$index])) $val = $tmp[$index];
			}

			foreach ($this->objs as $obj) {
				if (is_null($val) && isset($this->$obj->$key)) {
					$tmp =& $this->$obj->$key;
					if (isset($tmp[$index])) {
						$val = $tmp[$index];
						break;
					}
				}
			}


		}
		else {
			if (isset($this->$key)) $val = $this->$key;
			if ($val == '' && isset($this->_zone->$key)) $val = $this->_zone->$key;
			if ($val == '' && isset($this->taf2zone->$key)) $val = $this->taf2zone->$key;
			if ($val == '' && isset($this->MetarDecoded->$key)) $val = $this->MetarDecoded->$key;

		}

		error_reporting($old_error_reporting);

//print "val=$val<br>\n";
		return $val;
	}



	Function hsky_actual() {
		if (isset($this->MetarDecoded->hsky)) {
			return strtoupper ($this->MetarDecoded->hsky);
		}
		else { return ''; }
	}

   Function get_wx_item ($wx) {
      $wxinfo='';

      $wxparms = $this->cfg->Parameters('WxInfo');
      while (list($key,$wxitem) = each($wxparms)) {
	$wxitem = str_replace('/', '\/', $wxitem);
         if (preg_match("/$wxitem/i", $wx)) {
            $wxinfo = $this->cfg->Val('WxInfo', $wxitem);
            break;
         }
      }

      return $wxinfo;
   }

   Function get_displayname ($wx) {
      $wxinfo = $this->get_wx_item($wx);
      if ($wxinfo == '') { return $wx; }
      else {
      	$dayornight = $this->dayornight($wx);
         	$wxdata = explode('|', $wxinfo);
         	if (!$dayornight && isset($wxdata[3]) && $wxdata[3]) { return $wxdata[3];}  else { return $wxdata[0];}
      }
   }


   Function hsky_conditions_displayname() {
	if (isset($this->MetarDecoded->hsky_conditions)) {
		return $this->get_displayname($this->MetarDecoded->hsky_conditions);
	}
	else { return '';}
   }

   Function hweather_displayname() {
	if (isset($this->MetarDecoded->hweather)) {
		return $this->get_displayname($this->MetarDecoded->hweather);
	}
	else { return '';}
   }

   Function hsky() {
      $wx = $this->hsky_actual();
      return $this->get_displayname($wx);
   }

   Function hicon ($day='') {

      if ($day) { return $this->day_icon($day); }
      $wx = $this->hsky_actual();

      $wxinfo = $this->get_wx_item($wx);
      if ($wxinfo == '') { $wxinfo = $this->cfg->val('WxInfo', 'N/A'); }

      $wxdata = explode('|', $wxinfo);

      if ($this->dayornight($wx)) {
         return $wxdata[1];
      }
      else {
         return $wxdata[2];
      }


   }





	Function ZoneOrTAF(){
		if (isset($this->_zone)) {return  '_zone'; }
		elseif (isset($this->taf2zone)) { return  'taf2zone'; }
		else {return '';}
	}


	Function day_wx($day='') {
		$wx='';$wxdt='';

		if ($day == '') return'';

		$obj = $this->ZoneOrTAF();
		if (!$obj) return '';


		if (isset($this->$obj->day_wx[$day])) {
			$wx = $this->$obj->day_wx[$day];
			if (isset($this->$obj->day_title[$day])) {
				$wxdt = $this->$obj->day_title[$day];
			}
		}

		$wx_info = $this->cfg->val('WxInfo', $wx);
		$wxdata = explode('|', $wx_info);

		if (isset($wxdata[4]) && preg_match('/NIGHT|EVENING|NUIT|CE SOIR/', $wxdt)) {
			return $wxdata[4];
		}
		else {  return $wxdata[0];}

	}

	Function day_title_custom ($day='') {
		if ($day == '') return '';

		$obj = $this->ZoneOrTAF();
		if (!$obj) return '';


		if (isset($this->$obj->day_title[$day])) {
			$daytitle = $this->$obj->day_title[$day];
		}
		else {	return '';}

		$customdt = $this->cfg->Val('DayTitles', $daytitle);
		if ($customdt == '') {
			$dtparms = $this->cfg->Parameters('DayTitles');
			while (list($key,$custom) = each($dtparms)) {
				if (preg_match("/$custom/i", $daytitle)) {
					$customdt = $this->cfg->Val('DayTitles', $custom);
					break;
				}
			}
		}
		if ($customdt == '') { $customdt = $daytitle; }

		return $customdt;
	}

   Function day_icon ($tday='') {
      if ($tday == '') return $this->hicon();

	$obj = $this->ZoneOrTAF();
	if (!$obj) return '';

	if (isset($this->$obj->day_wx[$tday])) {
	      $wx = $this->$obj->day_wx[$tday];
	      $wx_dt = strtoupper($this->$obj->day_title[$tday]);

	      $wxinfo = $this->cfg->val('WxInfo', $wx);
	      if ($wxinfo == '') { $wxinfo = $this->cfg->val('WxInfo', 'N/A'); }

	      $wxdata =  explode('|', $wxinfo);
	      if (preg_match('/NIGHT|EVENING/', $wx_dt)) {
	         return $wxdata[2];
	      }
	      else {
	         return $wxdata[1];
	      }
	}
	else { return ''; }

   }


	Function fixtzhr($val, $min=0, $add_colon=0, $time24=0) {
	   $wtime = $val * 60 +$min + $this->tzdif * 60;
	   if ($wtime < 0) $wtime += 1440;
	   if ($wtime > 1439) $wtime -= 1440;
	   $val = ($wtime/60); $val = (int) $val;

	   $min = sprintf('%02u', $wtime % 60);

	   if ($time24) {
	      $val = sprintf('%02u',$val);
	      if ($min != '') { $val .=sprintf('%02u', $min); }
	   }
	   else {
	      if ($val == 12 ) { $val = '12'.$min;  if ($val==12) {$val='12 noon';} else {$val .= ' pm';}}
	      elseif ($val == 0 ) { $val = '12'. $min;  if ($val==12) {$val='12 mid';} else {$val .= ' am';}}
	      elseif ($val < 12) {$val = $val . $min.' AM';}
	      else { $val -=12; $val = $val . $min.' PM'; }
	   }

	   if (!$add_colon)    $val = preg_replace('/^(\d?\d)(\d\d)/', "$1:$2", $val);
	   return strtoupper($val);

	}

	Function local_metar_date($metardate, $time24=0) {

		if (strtolower($metardate) == 'unknown') { return $metardate; }

		$months = array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
		$weekdays = array('SUN','MON','TUE','WED','THU','FRI','SAT');

		if ($this->debug) 	print "metar_date=$metardate<br>\n";

		if (preg_match('/^(\d\d\d\d)\.(\d\d)\.(\d\d)\s+(\d?\d)(\d\d)\s+UTC/', $metardate, $matches)) {
			list ($all,$year, $mon, $day, $hour, $min) = $matches;
			$tzname = $this->tzname; $tzdif=$this->tzdif;

			$gmtime = gmmktime($hour,$min,0,$mon,$day,$year) + $tzdif * 3600;
			$wday = gmdate('D', $gmtime);

			$mon = gmdate('M', $gmtime);
			$day = gmdate('j', $gmtime);
			$year = gmdate('Y', $gmtime);

			$utime ='';
			if ($time24) {
				$utime = gmdate('Hi', $gmtime);
			}
			else {
				$utime = gmdate('g:i A', $gmtime);
			}
			$result =  "$utime $tzname $wday $mon $day $year";

//
//			$dateinfo = getdate(gmmktime($hour,$min,0,$mon,$day,$year));
//			$wday = $dateinfo['wday']; $mon = $dateinfo['mon']-1;
//			$day =$dateinfo['mday']; $year =  $dateinfo['year'];
//
//			$result='';
//			if ($time24) {
//				$result=$this->fixtzhr($hour,$min,0,24) . " $tzname $weekdays[$wday] $months[$mon] $day $year";
//			}
//			else {
//				$result=$this->fixtzhr($hour,$min,1) . " $tzname $weekdays[$wday] $months[$mon] $day $year";
//			}
//
			return strtoupper($result);
		}
	}




	Function dayornight ($wx) {

		if (isset($this->vars['lat']) && isset($this->vars['lon']) && $this->htime24h) {
			$astro = new FetchSunInfo('');
			$astro->set_sunrise_params($this->vars, $this->form, '', $this->cfg, array('','',''), $this->debug);

			$eday = (strtoupper($astro->sunset(0)) == 'NONE') ? -1 : sprintf('%04u', str_replace(':', '', $astro->sunset(0)));
			$bday = (strtoupper($astro->sunrise(0)) == 'NONE') ? -1 : sprintf('%04u', str_replace(':', '', $astro->sunrise(0)));
			$hour = sprintf('%04u', str_replace(':', '', $this->htime24h));
//print "bday=$bday...eday=$eday...hour=$hour<br>\n";
		}
		else {
			$bday = $this->cfg->val('Defaults', 'day begin');
			$eday = $this->cfg->val('Defaults', 'day end');

			$hour = gmdate('H', time() + $this->tzdif * 3600);
			if ($hour<0) $hour+=24; if ($hour>23) $hour-=24;

		}


		$dn = 0;
		if ($bday < 0 && $eday < 0) {
			$dn =0;
		}
		elseif ($bday < 0 && $eday > 0 && $hour < $eday) {
			$dn =  1;
		}
		elseif ($eday < 0 || ($bday < $eday && $hour >= $bday && $hour < $eday) || ($bday > $eday && ($hour >= $bday || $hour < $eday)) || preg_match('/SUNNY/', $wx) ) {
			$dn =  1; //day time
		}

		return $dn;
	}

	Function forecast_local_expiration() {

		$debug = $this->debug;
		$exp = $this->_zone->forecast_expiration;
		if (isset($this->tzdif) && isset($this->tzname) && $this->tzdif != ''  && preg_match('/^(\d?\d)(\d\d) GMT (\w\w\w) (\d\d) (\d\d\d\d)$/', $exp, $m)) {
			$hour = $m[1]; $min = $m[2]; $mname = $m[3]; $day = $m[4]; $year = $m[5];
			$mon = $this->month_names[$mname];
			$jday = jday($mon, $day, $year);

			$wtime = $hour * 60 + $min + $this->tzdif * 60;
			if ($wtime < 0) { $jday--;}
			if ($wtime > 1439) { $jday++;}
			list ($nmon, $nday, $nyear, $nw) = jdate($jday);
			$ntime = $this->fixtzhr($hour,$min,1,0);
			return strtoupper($ntime . ' ' . $this->tzname . ' '.  weekDayString($nw) . ' ' . $this->month_nums[$nmon-1] . sprintf(' %02u %04u', $nday, $nyear));
		}
		else {
			return $exp;
		}

	}










}

?>