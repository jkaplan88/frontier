<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchCANexradLoopInfo.php,v 1.7 2006/12/20 05:47:30 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");

class FetchCANexradLoopInfo {
	var $debug = 0;
	var $nexrad_total_loop_images= 0;
	var $nexrad_loop_images = array();
	var $core_path ='';

	Function FetchNexradLoopInfo($core_path) {
		$this->core_path = $core_path;
	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function fetch_loop_data (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		if (isset($form['radar_icao'])){$radar_icao =$form['radar_icao']; }
		elseif (isset($var['radar_icao'])){$radar_icao = $var['radar_icao']; }
		else {$radar_icao = '';}

		$radar_icao = strtoupper($radar_icao);

		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found = 0;

		$fc_file = $cache_path . strtolower("/ca-radarloop-info-$radar_icao.txt");
		$fc_file2 = '';
		$url='';

		$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('SystemSettings', 'nexrad_prefix'));

		$data = &fetch_forecast($cfg,$ma, $fc_file, $fc_file2, $url,
			$cfg->val('SystemSettings', 'nexrad_url'),
			$prefix,
			$cfg->val('SystemSettings', 'nexrad_postfix'),
			 $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

		$which_used = $data[0];
		$wx_data = $data[1];

		$images = array();
		$total = 0;
		if (preg_match_all('/(COMPOSITE_([A-Z][A-Z][A-Z])_PRECIP_[A-Z]{4}_\d\d\d\d_\d\d_\d\d_\d\d_\d\d\.\w+)">/',
	   				$wx_data, $m) || preg_match_all('/(([A-Z][A-Z][A-Z])_PRECIP_[A-Z]{4}_\d\d\d\d_\d\d_\d\d_\d\d_\d\d\.\w+)">/',
	   				$wx_data, $m)) {
			for ($i = 0; $i < sizeof($m[0]); $i++) {
				if (strtoupper($m[2][$i]) == $radar_icao) {
					array_unshift($images, $m[1][$i]);
					$total++;
				}

			}
		}
		$this->nexrad_loop_images =& $images;
		$this->nexrad_total_loop_images = $total;

		return $t;
	}


	Function  nexrad_total_loop_images() {
		return $this->nexrad_total_loop_images;
	}

	Function nexrad_loop_images($num) {

		if ($num > 0) $num--;
		$res='';
		if (isset($this->nexrad_loop_images[$num])) $res = $this->nexrad_loop_images[$num];
		return $res;
	}

	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		if ($index <> "") {
			$tmp = $this->$key;
			if (isset($tmp[$index])) $val = $tmp[$index];
		}
		else {
			$val = $this->$key;
		}


		error_reporting($old_error_reporting);

		return $val;
	}

	Function nexrad_latest_image() {

		$num = $this->nexrad_total_loop_images-1;
		$res='';
		if (isset($this->nexrad_loop_images[$num])) $res = $this->nexrad_loop_images[$num];
		return $res;

	}


}

?>