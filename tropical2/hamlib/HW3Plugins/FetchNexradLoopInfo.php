<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchNexradLoopInfo.php,v 1.9 2003/12/05 23:50:48 lee Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");

class FetchNexradLoopInfo {

	var $debug = 0;
	var $nexrad_total_loop_images= 0;
	var $nexrad_loop_images = array();
	var $core_path ='';

	Function FetchNexradLoopInfo($core_path) {
		$this->core_path = $core_path;
	}

      Function Debug ($mode) {
         $this->debug = $mode;
      }



	Function fetch_loop_data (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		if (isset($form['radar_icao'])){$radar_icao =$form['radar_icao']; }
		elseif (isset($var['radar_icao'])){$radar_icao = $var['radar_icao']; }
		else {$radar_icao = '';}

		if (isset($form['hwvradartype'])){$radar_type =$form['hwvradartype']; }
		elseif (isset($var['hwvradartype'])){$radar_type = $var['hwvradartype']; }
		else {$radar_type = '';}

		if ($radar_type == 'conus')  $radar_icao = $radar_type;

		if (isset($var['country'])) { $country = strtolower($var['county']); }
		else { $country = 'us'; }


		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found = 0;

		$fc_file = $cache_path . strtolower("/$country-$radar_icao-$radar_type.txt");
		$fc_file2 = '';

		$rt_url = '';
		if ($radar_type == 'conus') { $rt_url = 'DS.74rcm'; }
			elseif ($radar_type == 'base') { $rt_url = 'DS.p19r0'; }
			elseif ($radar_type == 'composite') { $rt_url = 'DS.p38cr'; }
			elseif ($radar_type == 'onehr') {$rt_url = 'DS.78ohp';}
			elseif ($radar_type == 'stormtotal') {$rt_url = 'DS.80stp';}

		$url = "/$rt_url/". strtolower($radar_icao) . '.txt';
		$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'nexrad_prefix'));

		$data = &fetch_forecast($cfg,$ma, $fc_file, $fc_file2, $url,
			$cfg->val('URLs', 'nexrad_url'),
			$prefix,
			$cfg->val('URLs', 'nexrad_postfix'),
			 $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

		$which_used = $data[0];
		$wx_data = $data[1];

	//   dd.20010113_dt.184100.gif

		$images = array();
		if (preg_match_all('/((?:\w\w\.\d\d\d\d\d\d\d\d_\w\w\.\d\d\d\d\d\d_)?\w\w\.\d\d\d\d\d\d\d\d_\w\w\.\d\d\d\d\d\d\.gif)/',
	   				$wx_data, $matches)) {
			for ($i = 0; $i < sizeof($matches[0]); $i++) {
				array_push($images, $matches[1][$i]);
			}
		}
		$this->nexrad_loop_images =& $images;
		$this->nexrad_total_loop_images = sizeof($matches[0]);

		return $t;
	}


	Function  nexrad_total_loop_images() {
		return $this->nexrad_total_loop_images;
	}

	Function nexrad_loop_images($num) {

		if ($num > 0) $num--;
		$res='';
		if (isset($this->nexrad_loop_images[$num])) $res = $this->nexrad_loop_images[$num];
		return $res;
	}

	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		if ($index <> "") {
			$tmp = $this->$key;
			if (isset($tmp[$index])) $val = $tmp[$index];
		}
		else {
			$val = $this->$key;
		}


		error_reporting($old_error_reporting);

		return $val;
	}


}

?>