<?php
error_reporting(0);
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* ImagePluginAPI - this may be used to easily make map based plugins for use with
* HW3. It requires that HW3image be installed as it uses several of
*	its tools.
*
* Revision:
* $Id: ImagePluginAPI.php,v 1.10 2007/09/23 22:09:39 wxfyhw Exp $
*
*/

define ('ImagePluginAPI_VERSION', 1.2);

# we will load and exten the HW3PluginAPI so that we can
# inherent the normal Plugin API routines as well.
require_once(HAMLIB_PATH . '/HW3Plugins/HW3PluginAPI.php');

# lets load the HW3image tools we will most likely use
require_once (HAMLIB_PATH . '/HW/Maps/Projection.php');
require_once (HAMLIB_PATH . '/HW/Maps/Params.php');
require_once (HAMLIB_PATH . '/HW/HW3image/LoadImage.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Tools.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Tools/AddTitle.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Tools/Legend.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Tools/Text.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Color.php');


# lets define the most common constants we will use
define ('Image_Settings', 'Image Settings');
define ('Title_Settings', 'Title Settings');
define ('Legend_Settings', 'Legend Settings');
define ('Label_Settings', 'Label Settings');
define ('Plot_Cities_Settings', 'PlotCities Settings');
define ('Plot_Cities', 'PlotCities');
define ('Point_Settings', 'Point Settings');
define ('Point_Labels', 'Point Labels');
define ('Points_to_Plot', 'Points to Plot');
define ('Watermark_Settings', 'Watermark Settings');
define ('Watermarks', 'Watermarks');
define ('Labels', 'Labels');

class ImagePluginAPI extends HW3PluginAPI {


	var $color;
	var $error = '';
	var $template_name;
	var $map_name='';
	var $map_width=0;
	var $map_height= 0;

	var $use_map = '';
	var $save_map_name = '';
	var $save_map_path = '';
	var $sweetspot_x= null;
	var $sweetspot_y = null;

	var $minx = 0;
	var $maxx = 0;
	var $miny = 0;
	var $maxy = 0;
	var $cropx=0;
	var $cropy=0;
	var $cropscale=1;


	Function ImagePluginAPI($core_path) {
		parent::HW3PluginAPI($core_path);
		$this->color = new HWimageColor();
		$this->texter = new HWimageText();
		$this->tools = new HWimageTools;
	}



	Function get_savename($clean_forecast, $usemap) {
		return $clean_forecast . '_' . $usemap ;
	}

	Function get_usemap ($usemap, $parms) {
		return $usemap;
	}

	Function main(&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {

		parent::main($var, $form, $forecast, $cfg, $forecast_info, $debug);

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;
		$this->template_name = $t;

		# are we no web access & someone browsing? if so lets leave
		if ($cfg->val('SystemSettings', 'no_web_access') && isset($_SERVER['REQUEST_METHOD'])) {
			return $t;
		}

		$clean_forecast = preg_replace('/\W/', '', $forecast);
		$settings_name = Image_Settings;
#
#		#LETS LOAD OUT  PARMS
		$parms = $cfg->SectionParams($settings_name);

		if (! is_array($parms)) $parms = array();
		foreach ($parms as $key => $value) {
			$value = preg_replace('/%%(\w+)%%/e', "\$this->varreplace(\"$1\");", $value);
			$value = preg_replace('/<\?(.+?)\?>/e', 'eval(\'return \1;\');', $value);
			$parms[$key] = $value;
		}
		if ($debug) var_dump($parms);


		# the map type
		$gdext = (isset($parms['gdext'])) ? $parms['gdext'] : '';
		if (!$gdext) $gdext = 'png';


		# lets determine the map to use:
		$usemap = (isset($form['usemap'])) ? $this->cleanval($form['usemap']) : '';
		if (!$usemap) { $usemap = $parms['usemap']; }

		$usemap = $this->get_usemap($usemap, $parms);
		$this->usemap = $usemap;
		$this->usedmap = $usemap;

		# Lets determine the save map information
		# we will get the save path & name.. then if nocache does not
		# equal "1" then we will check to see if already exists &
		# less than cache max age.. if so we leave otherwise
		# we fetch & recreate the image.
		$save_map_path = (isset($parms['save_map_path'])) ? $parms['save_map_path'] : '';
		if (!$save_map_path || !is_dir($save_map_path)) {
			$this->error = "Save Directory does not exist: $save_map_path\n";
			if ($debug) print $this->error;
			return $t;
		}
		$this->save_map_path = $save_map_path;

		$save_map_name = preg_replace('/\W/', '', ($cfg->val($settings_name, 'allow_save_name') && isset($form['sn'])) ? $form['sn'] : '');

		if ($save_map_name == '') {
			$save_map_name = (isset($parms['save_map_name'])) ? $this->cleanval($parms['save_map_name']) : '';
			if ($cfg->val($settings_name, 'allow_save_name') && isset($form['ne'])) $save_map_name .=  $form['ne'];
			$save_map_name = preg_replace('/\W/', '', $save_map_name);
		}


		if (!$save_map_name) {
			$this->save_map_name = $save_map_name = $this->get_savename($clean_forecast, $usemap);
		}

		# lets gett he save gd format
		$save_gdext= (isset($parms['save_map_gdext'])) ? $parms['save_map_gdext'] : $gdext;

		$this->map_name = "$save_map_name.$save_gdext";

		// if not nocache then lets see if already created
		$nocache = (isset($parms['nocache'])) ? $parms['nocache'] :  (isset($form['nocache'])) ? $form['nocache'] : 0;
		if (!$nocache && !preg_match('/MAPS_\w/', $usemap)) {
			$tfile = "$save_map_path/$save_map_name.$save_gdext";
			$tma = (isset($parms['cache_max_age']) && $parms['cache_max_age'] >= 1) ? $parms['cache_max_age'] : 15;

			if (file_exists($tfile)) {
				$fage = (time() - filemtime($tfile))/60;
				if ($debug) { print "<br>$save_map_name.$save_gdext age=$fage<br>\n";}
				if ($fage <= $tma) {
					if ($cfg->val('SystemSettings', 'redirect_to_image')) {
						$rurl = $parms['save_map_url'] . "/$save_map_name.$save_gdext";
						header("Location: $rurl");
						exit;
					}
					else {
						return $t;
					}
				}
			}
		}


		# lets call the pre_plto routine
		if (!$this->pre_plot($parms)) return $this->template_name;



		$tools = new HWimageTools;


		$orig_usemap = $usemap;
		$orig_save_map_name = $save_map_name;

		#this code will allow making a lot of maps from a single call
		# if usemap=MAPS_xxxxxx
		# then we will get all the config settings from [MAPS_xxxxx]
		# each setting should be in the "map_name=save_as" format.
		$usemaps = array();
		$usemap_flag='';
		if (preg_match('/^(MAPS_\w+)$/', $usemap, $m)) {
			$usemap_flag=$m[1];
			$usemaps = $cfg->SectionParams($usemap_flag);

		}
		else {
			$usemaps[$usemap]= $save_map_name;
		}

		foreach ($usemaps as $usemap => $val) {

			$tarr = preg_split('/\|/', $val);
			$save_map_name = $tarr[0];
			$map_vars = (isset($tarr[1])) ? $tarr[1] : '';

			$map_vars = preg_split('/,/', $map_vars);
			foreach ($map_vars as $key => $value) {
				$form[$key] = $value;
			}



			if ($usemap_flag) {
				$usemap_parms = $cfg->SectionParams($usemap_flag . ' ' . Image_Settings);
				foreach ($usemap_parms as $key => $value) {
					$parms{$key}  = preg_replace('/%%(?:(\w\w)_)?(\w+)%%/e', "\$this->_format(\$this->varreplace(\"$2\"), \"$1\");", $value);
					$parms{$key} = preg_replace('/\<\?(.+?)\?>/e', 'eval (\'return $1;\');', $parms{$key});						}
			}


			if (!$save_map_name) { $save_map_name = $this->get_savename($clean_forecast, $usemap);}
			if ($this->debug) print "<br><br>save_map_name=$save_map_name<br><br>";


			# the blank map path
			$blank_map_path = (isset($parms['blank_map_path'])) ? $parms['blank_map_path'] : '';

			# lets make sure the map exists
			if (!file_exists("$blank_map_path/$usemap.$gdext")) {
				$this->error = "Map $usemap.$gdext not found\n";
				if ($debug) print $this->error;
				return $t;
			}



			#lets make sure that the map meta file exists
			if (!file_exists("$blank_map_path/$usemap.txt")) {
				$this->error = "Mapfile $usemap.txt not found\n";
				if ($debug) print $this->error;
				return $t;
			}


			# lets load the image, error out if an error
			list ($image, $error) = load_image ($blank_map_path,$usemap,$gdext);
			if ($error) {
				$this->error = $error;
				if ($debug) print $this->error;

				return $t;
			}
			if ($parms['truecolor'] && !imageistruecolor($image)) {
				$timage = imagecreatetruecolor(imagesx($image), imagesy($image));
				imagecopy($timage, $image, 0,0,0,0, imagesx($timage), imagesy($timage));
				$image = $timage;
			}


			# ok lets get the map projection
			$map_params = new HWmapParams();
			$mapinfo_path = "$blank_map_path/$usemap.txt";
			$map_info = $map_params->load_map_info($mapinfo_path);
			if (!$map_info) {
				$this->error = "Cannot load map info $mapinfo_path";
				if ($debug) print $this->error . "<br>\n";

			}


			$map_proj  = new_projection($map_info);

			if ($debug) {
				print "using map=$usemap.txt\n";
				var_dump($map_info);
			}



			$blend_level = (isset($parms['blend_level'])) ? $parms['blend_level'] +0 : 100;
			if ($blend_level < 1 || $blend_level > 100) $blend_level = 100;

			$contour_labels='';
			$continue = 1;

			if ($blend_level == 100) {
				$continue = $this->plot_map($image, $map_proj, $parms);

			}
			else {
				$temp_image = imagecreate(imagesx($image), imagesy($image));
				$color = new HWimageColor();
				$offwhite = $color->get_color($temp_image, 'FFFFEF');
				imagecolortransparent($temp_image,$offwhite);
				$continue = $this->plot_map($temp_image, $map_proj, $parms);


				imagecopymerge($image, $temp_image,0,0,0,0,imagesx($temp_image), imagesy($temp_image), $blend_level);
				imagedestroy($temp_image);

			}
			if ($this->debug) print "are we continuing? $continue<br>";
			if (!$continue)  return $this->template_name;

			if ($this->debug) print "looks like we are continuing!<br>";



			# because of gd issues with how we plot the contours, we have to plot in pallette then convert to
			# true color. Would be nice if there was a simple pallete to true color command
			if (isset($parms['truecolor']) && $parms['truecolor'] && !imageistruecolor($image) ) {
				$temp_image = imagecreatetruecolor(imagesx($image), imagesy($image));
				imagecopy($temp_image,$image,0,0,0,0,imagesx($image), imagesy($image));
				$image =& $temp_image;
			}

			# if we need an overlay and it exists lets do it
			$mapoverlay = (isset($form['overlay'])) ? $form['overlay'] :  (isset($parms['overlay'])) ? $parms['overlay']  : 1;
			$mapoverlay = str_replace('THISMAP', $usemap, $mapoverlay);
			if ($mapoverlay ==1) $mapoverlay= $usemap . '_overlay:100';
			if ($debug) print "mapoverlay=$mapoverlay<br>\n";

			if ($mapoverlay) {
				$overlay_path = (isset($parms['overlay_path']) && is_dir($parms['overlay_path'])) ? $parms['overlay_path'] : $blank_map_path;
				$overlay_gdext = (!empty($parms['overlay_gdext'])) ? $parms['overlay_gdext'] : $gdext;
				$this->add_overlays($image, $overlay_path, $overlay_gdext, $mapoverlay);
			}

			$tprojection = (isset($map_info['projection'])) ? $map_info['projection'] : ( (isset($map_info['PROJECTION'])) ? $map_info['PROJECTION'] : '' );
			if (!empty($parms['autocrop']) && (preg_match('/mercator|grid/i', $tprojection) || isset($map_info['lat1']))) {
				$image = $this->autocrop($image, $map_proj, $map_info, $parms);
			}

			$this->pre_legend($image,$map_proj,$parms);


			# lets add legend if needed
			if ($cfg->val(Legend_Settings, 'add_legend')) {

				$colors_name = (!empty($parms['color_section'])) ? $parms['color_section'] : (preg_replace('/\W/', '', $forecast) . ' Colors');
				$contour_colors = $cfg->SectionParams($colors_name);
				if (!is_array($contour_colors)) $contour_colors = array();
				$this->add_legend($image, $parms, $contour_colors,$cfg, $form);
			}


			# lets add city names if needed
			$plotcities = (isset($form['plotcities'])) ? $form['plotcities'] :
					$cfg->val(Plot_Cities_Settings, 'plotcities');
			if ($plotcities) {
				$this->plot_cities($image, $map_proj, $parms, $cfg);

			}


			# lets add labels
			if ($cfg->val(Label_Settings, 'add_labels')) {
				$this->add_general_labels($image, $parms, $wx_data, $cfg);
			}



			# lets add watermarks
			if ($cfg->val(Watermark_Settings, 'add_watermarks')) {
				$this->add_watermarks($image, $parms, $cfg, $form);
			}



			# if we need to add a title lets do it.
			if ($cfg->val(Title_Settings, 'add_title')) {
				$image = $this->add_title($image, $parms, $cfg);
			}


			if ($debug) print "saving image $save_map_path/$save_map_name.$save_gdext";
			$quality = (!empty($parms['save_quality'])) ? intval($parms['save_quality']+0) : -1;
			$tools->save_image($image, $save_map_path , $save_map_name . '.' . $save_gdext , $save_gdext, $quality);


			// if we need to save a thumb lets do that now
			if (!empty($parms['save_thumb']))  $this->save_thumbnail($image,$parms, $save_map_path,$save_map_name,$save_gdext,$quality);

			$this->map_width = imagesx($image);
			$this->map_height = imagesy($image);

		}




		if ($cfg->val('SystemSettings', 'redirect_to_image')) {
			$rurl = $parms['save_map_url'] . "/$save_map_name.$save_gdext";
			header( "Location: $rurl");
			exit;
		}



		return $t;

	} # end sub zone


	function save_thumbnail (&$image, &$parms, $save_map_path, $save_map_name, $save_gdext, $quality = -1) {

		$thumb_w = (!empty($parms['thumb_width'])) ? round($parms['thumb_width']+0) : 0;
		$thumb_h = (!empty($parms['thumb_height'])) ? round($parms['thumb_height']+0) : 0;

		// if width is set but height isnt, then we keep the same ratio
		if ($thumb_w > 0 && $thumb_h <= 0) $thumb_h = round (imagesy($image) * $thumb_w / imagesx($image));
		elseif ($thumb_h > 0 && $thumb_w <= 0) $thumb_w = round (imagesx($image) * $thumb_h / imagesy($image));

		if ($thumb_w > 0 && $thumb_h > 0) {
			$post = (!empty($parms['thumb_post'])) ? preg_replace('/\W/', '',$parms['thumb_post']) : '';
			$thumb_gdext = (!empty($parms['thumb_gdext']) && preg_match('/^(png|gif|jpg|jpeg)$/', $parms['thumb_gdext'])) ? $parms['thumb_gdext'] : $save_gdext;

			$thumb_image = (imageistruecolor($image)) ? imagecreatetruecolor($thumb_w,$thumb_h) : imagecreate($thumb_w,$thumb_h);
			if ($thumb_image) {
				imagecopyresampled($thumb_image,$image,0,0,0,0,$thumb_w,$thumb_h,imagesx($image),imagesy($image));

				$thumb_quality = (!empty($parms['thumb_quality'])) ? round($parms['thumb_quality']+0) : $quality;
				$this->tools->save_image($thumb_image, $save_map_path , $save_map_name . $post . '.' . $thumb_gdext , $thumb_gdext, $thumb_quality);
				imagedestroy($thumb_image);
			}
		}
	}


	function add_title(&$image, &$parms, &$cfg) {
		$title_parms = $cfg->SectionParams(Title_Settings);

		$title_settings=array();
		foreach ($title_parms as $key => $value) {
			$value = preg_replace('/%%(?:(\w\w)_)?(\w+)%%/e', "\$this->_format(\$this->varreplace(\"$2\"),\"$1\");", $value);
			$value = preg_replace('/\<\?(.+?)\?>/e',  'eval(\'return \1;\');', $value);
			$title_settings["$key"] = $value;
		}
		if (isset($parms['truecolor'])) $title_parms['turecolor'] = $parms['truecolor'];

		$titler = new AddTitle();
		return $titler->add_title($image, $title_settings);

	}

	function pre_plot(&$parms) {
		# this routine will either not be used or overwitten
		# this is where you would normally override
		# with code to go out & fetch the data you need

		return 1;
	}

	function pre_legend($image,$map_proj,$parms) {

	}

	function plot_map(&$image, &$map_proj, &$parms) {
		# this routine will normally be overridden

		return 1;
	}


	function add_overlays(&$image, $blank_map_path,$gdext, $mapoverlay) {

		$overlays = array();
		if (is_array($mapoverlay)) {
			$overlays = $mapoverlay;
		}
		elseif ($mapoverlay) { $overlays = preg_split('/,/', $mapoverlay);}
		foreach ($overlays as $toverlay) {
			if ($this->debug) print "overlay=$toverlay<br>\n";
			$map_overlay=''; $oblend = 100;
			$toverlay_items = explode(':', $toverlay);
			if (isset($toverlay_items[0])) $map_overlay = $toverlay_items[0];
			if (isset($toverlay_items[1])) $oblend = $toverlay_items[1];
			if ($oblend < 1 || $oblend  > 100) $oblend=100;
			if (file_exists("$blank_map_path/$map_overlay.$gdext")) {
				list ($overlay, $error) = load_image ($blank_map_path,$map_overlay ,$gdext);
				if (!$error) {

					if ($oblend < 100) imagecopymerge($image, $overlay,0,0,0,0,imagesx($overlay), imagesy($overlay), $oblend);
					else {

						if (imageistruecolor($overlay) && imageistruecolor($image)) {
							imagealphablending($image, true);
							imagealphablending($overlay, true);

						}
						imagecopy($image, $overlay,0,0,0,0,imagesx($overlay), imagesy($overlay));
					}
				}
				imagedestroy($overlay);
			}
			elseif ($this->debug) {
				print "overlay: $blank_map_path/$map_overlay.$gdext does not exist<br>\n";
			}
		}

	}


	function add_legend(&$image, &$parms, &$contour_colors, &$cfg) {

		$gdext = $parms['gdext'];

		$legend_parms = $cfg->SectionParams(Legend_Settings);

		foreach ($legend_parms as $key => $value) {
			$value  = preg_replace('/%%(?:(\w\w)_)?(\w+)%%/e', "\$this->_format(\$this->varreplace(\"$2\"), \"$1\");", $value);
			$value = preg_replace('/\<\?(.+?)\?>/e', 'eval(\'return \1;\');',$value);
			$legend_parms[$key] = $value;
		}

		#maybe the add_legend was conditions so lets leave if it now zero
		if (! $legend_parms['add_legend']) return;

		$method = (isset($legend_parms['add_method'])) ? strtolower($legend_parms['add_method']) : '';

		if (!$method || preg_match('/draw/', $method)) {

			$legend_parms['colors'] =& $contour_colors;
			if (!isset($legend_parms['start']))  $legend_parms['start'] = 0;
			if (!isset($legend_parms['stop']))  $legend_parms['stop'] = 0;
			if (!isset($legend_parms['interval']))  $legend_parms['interval'] = 1;
			if (!isset($legend_parms['no_gradient']))  $legend_parms['no_gradient'] = 0;

			$w = imagesx($image); $h = imagesy($image);
			$legend_bar_height = (isset($legend_parms['legend_bar_height'])) ? strtolower($legend_parms['legend_bar_height']) : 50;
			$legend_bar_color = (isset($legend_parms['legend_bar_color'])) ? strtolower($legend_parms['legend_bar_color']) : '';


			if ($legend_bar_color && $legend_bar_height) {
				$temp_image = imagecreate($w,$legend_bar_height);
				$color = new HWImageColor;
				$color->get_color($temp_image, $legend_bar_color);

				$legend_bar_y = (isset($legend_parms['legend_bar_y'])) ? strtolower($legend_parms['legend_bar_y']) : 0;
				$blend = (isset($legend_parms['legend_bar_blend'])) ? strtolower($legend_parms['legend_bar_blend']) : 100;

				if ($blend == '') $blend = 100;
				imagecopymerge($image,$temp_image,0,$legend_bar_y,0,0,$w,$legend_bar_height, $blend);
				imagedestroy($temp_image);
			}



			if ($legend_parms['interval'] || $legend_parms['no_gradient']) {
				$legend = new HWimageLegend();
				$legend->draw_legend($image, $legend_parms);
			}
		}
		else {
			#lets load the legend file
			$legend_file = $legend_parms['legend_image'];
			$legends_path = $legend_parms['legend_path'];

			$legend_x = (isset($legend_parms['legend_x'])) ? strtolower($legend_parms['legend_x']) : 'center';
			$legend_y = (isset($legend_parms['legend_y'])) ? strtolower($legend_parms['legend_y']) : 'center';

			$legend_blend = (isset($legend_parms['legend_blend'])) ? $legend_parms['legend_blend']+0 : 100;
			$legend_halign = (isset($legend_parms['legend_halign'])) ? strtolower($legend_parms['legend_halign']) : '';
			$legend_valign = (isset($legend_parms['legend_valign'])) ? strtolower($legend_parms['legend_valign']) : 'top';


			$this->insertimage ($image, $legends_path, $legend_file, $legend_x,$legend_y,$legend_halign,$legend_valign,$legend_blend);



		}

	}

	Function  add_general_labels (&$image, &$parms,&$wx_data, &$cfg ) {

		$label_settings = $cfg->SectionParams(Label_Settings);
		$labels = $cfg->SectionParams(Labels);
		if (!$labels) return;

		$text = $this->texter;

		list($w,$h) = array(imagesx($image), imagesy($image));

		foreach ($labels as $key => $value) {
			$tlabel  = preg_replace('/%%(?:(\w\w)_)?(\w+)%%/e', "\$this->_format(\$this->varreplace(\"$2\"), \"$1\");", $value);

			$tlabel = preg_replace('/\<\?(.+?)\?>/e', 'eval (\'return $1;\');', $tlabel);

			list ($label, $x, $y, $halign,$valign) = explode('|', $tlabel);

			$error = $text->do_text($image, $x,$y,$label,$halign,$valign, $label_settings);
			if ($error && $this->debug) print "$error<br>\n";
		}

	}



	function plot_cities($image, $map_proj, $parms, $cfg) {

		$cgi_path = $this->core_path;

		$label_settings = $cfg->SectionParams(Plot_Cities_Settings);
		$labels = $cfg->SectionParams(Plot_Cities);

		$text = new HWimageText();

		$form =array(); $var = array();
		require_once (HAMLIB_PATH . '/HW/ParsePlace.php');
		require_once (HAMLIB_PATH . '/HW/DBAccess.php');
		$cityparser= new ParsePlace($form,$var,$cfg,$cgi_path, $this->debug);
		$db_access = $this->_get_db_object($cfg, $cgi_path);

		$color = $this->color;

		$lastpands = '';
		$lastx = 0; $lasty = 0;

		sort($labels);

		foreach ($labels as $key => $value) {
			if (!$value) continue;
			list ($pands,$display, $x, $y, $halign,$valign,$rest) = explode('|', $value);

			$xoffset = 0; $yoffset = 0;
			if (!preg_match('/^lon:/i',$x) && !preg_match('/^lat:/i',$y)) {
				$tlon = preg_match('/^lon:([-+]?\d+(?:\.\d+)?)(?::([+-]?\d+))?$/i',$x,$mx);
				$tlat = preg_match('/^lat:([-+]?\d+(?:\.\d+)?)(?::([+-]?\d+))?$/i',$y,$my);
				$lon = $mx[0]; $xoffset = $mx[1];
				$lat = $my[0]; $yoffset = $my[1];
				unset($tlon,$tlat);

				list($x,$y) = $map_proj->get_xy($lon,$lat);
			}
			else {
				if (preg_match('/^[\+\-]/', $x)) {
					$xoffset = $x;
					$x = '';
				}
				if (preg_match('/^[\+\-]/', $y)) {
					$yoffset = $y;
					$y = '';
				}
			}

			if ($x == '' || $y == '' || !$display) {
				if ($pands == $lastpands) {
					$x = $lastx;
					$y = $lasty;
				}
				else {
					$form['pands'] = $pands;
					$cityparser->process_place($db_access,0);
					if ($x == '' || $y == '') {
						list ($x,$y) = $map_proj->get_xy($var{lon}, $var{lat});
					}
					$lastpands = $pands;
					$lastx = $x;
					$lasty = $y;
				}
				if (!$display) {
					$display = preg_replace("/([�������\w]+)/e","ucwords(strtolower('$1'))", $var['place']);
				}
			}

			$x += $xoffset; $y += $yoffset;
			if ($display == 'SQUARE') {
				list($w,$h,$f,$c,$dx,$dy,$dc) = explode('/\|/', $rest);
				if (!$w) $w = 5;
				if (!$h) $h = $w;
				if (!isset($f)) $f = 1;
				$c = $color->get_color($image,$c);
				if ($dc) {
					$dc = $color->get_color($image,$dc);
				}

				$w2 = intval($w/2+.5);
				$h2 = intval($h/2+.5);
				if ($f) {
					if ($dx || $dy) {
						imagefilledrectangle($image, $x-$w2+$dx, $y-$h2+$dy, $x+$w2, $y+$h2, $c);
					}
					imagefilledrectangle($image, $x-$w2, $y-$h2, $x+$w2, $y+$h2, $c);
				}
				else {
					if ($dx || $dy) {
						imagerectangle($image, $x-$w2+$dx, $y-$h2+$dy, $x+$w2, $y+$h2, $c);
					}
					imagerectangle($image, $x-$w2, $y-$h2, $x+$w2, $y+$h2, $c);
				}
			}
			elseif ($display == 'CIRCLE') {
				list($w,$h,$f,$c,$dx,$dy,$dc) = explode('/\|/', $rest);
				if (!$w) $w = 5;
				if (!$h) $h = $w;
				if (!isset($f)) $f = 1;
				$c = $color->get_color($image,$c);
				if ($dc) {
					$dc = $color->get_color($image,$dc);
				}
				if ($f) {
					if ($dx || $dy) {
						imagefilledarc($image, $x+$dx, $y+$dy, $w,$h,0,360,$dc);
					}
					imagefilledarc($image, $x,$y,$w,$h,0,360,$c);
				}
				else {
					if ($dx || $dy) {
						imagearc($image, $x+$dx, $y+$dy, $w,$h,0,360,$dc);
					}
					imagearc($image, $x,$y,$w,$h,0,360,$c);
				}
			}
			else {
				list($halign,$valign) = explode('/\|/', $rest);
				if (!$halign) $halign = $label_settings['default_halign'];
				if (!$valign) $valign = $label_settings['default_valign'];
				$error = $text->do_text($image, $x,$y,$display,$halign,$valign, $label_settings);
			}
			if ($error && $this->debug) print "$error<br>\n";
		}

	}

	function check_autocrop_minmax ($x,$y) {

		if (!is_numeric($this->minx) || $x < $this->minx) { $this->minx = $x; }
		if (!is_numeric($this->miny) || $y < $this->miny) { $this->miny = $y; }
		if (!isset($this->maxx) || $x > $this->maxx) { $this->maxx = $x; }
		if (!isset($this->maxy) || $y > $this->maxy) { $this->maxy = $y; }
	}

	Function autocrop($image, $map_proj, $map_info, $parms) {
		$minx = $this->minx;
		$miny = $this->miny;
		$maxx = $this->maxx;
		$maxy = $this->maxy;

		if (1) {#isset($minx) && isset($miny) && isset($maxx) && isset($maxy)) {
			$nw = $parms['autocrop_width'];
			$nh = $parms['autocrop_height'];
			list($w,$h) = array(imagesx($image), imagesy($image));

			$resample = (!empty($parms['autocrop_resample'])) ? 1 : 0;

			if (($w > $nw || $h > $nh)) {

				# do we have a valid sweet spot?
				$spx = $this->sweetspot_x;
				$spy = $this->sweetspot_y;
				$use_sp = ($spx >= 0 && $spx < $w && $spy > 0 && $spy < $h) ? 1 : 0;


				$padding = 	(isset($parms['autocrop_padding'])) ? $parms['autocrop_padding']+0 :0;
				$padding_left = 	(isset($parms['autocrop_padding_left'])) ? $parms['autocrop_padding_left']+0 : $padding;
				$padding_right = 	(isset($parms['autocrop_padding_right'])) ? $parms['autocrop_padding_right']+0 : $padding;
				$padding_top = 	(isset($parms['autocrop_padding_top'])) ? $parms['autocrop_padding_top']+0 : $padding;
				$padding_bottom = 	(isset($parms['autocrop_padding_bottom'])) ? $parms['autocrop_padding_bottom']+0 : $padding;

				$minx -= $padding_left;
				$maxx += $padding_right;
				$miny -= $padding_top;
				$maxy += $padding_bottom;



				if ($minx < 0) $minx = 0;
				if ($maxx > $w || $maxx==0) $maxx = $w;
				if ($miny < 0) $miny = 0;
				if ($maxy > $h || $maxy == 0) $maxy = $h;

				$sw = $maxx-$minx;
				$sh = $maxy-$miny;

				$this->DebugPrint("SweetSpot=($spx,$spy)");
				$this->DebugPrint("UseSweetSpot=$use_sp");
				$this->DebugPrint("new w,h=($nw,$nh)");
				$this->DebugPrint("original w,h=($w,$h)");
				$this->DebugPrint("SweetBox (minx,miny),(maxx,maxy)=($minx,$miny),($maxx,$maxy)");
				$this->DebugPrint("SweetBox w,h=($sw,$sh)");

				$this->cropscale=1;
				$startx= $starty = 0;
				$srcW = $srcH = 0;
$this->DebugPrint("resample=$resample $sw > $nw || $sh > $nh");
				if ($resample && ($sw > $nw || $sh > $nh)) {

					$starty = $miny;
					if ($starty < 0) $starty = 0 ;

					$startx = $minx;
					if ($startx < 0)$startx = 0 ;

					if ($sw/$nw > $sh / $nh) {
						#width is greater find new height
						$th = $sw * $nh / $nw;

						$this->cropscale = $nh / $nw;

						$d = round(($th - $sh)/2);
						if ($d > 0 && $d < $starty) $starty -= $d ;
						$sh = $th;
					}
					else {
						#height is larger find new width
						$tw = $sh * $nw / $nh;

						$d = round(($tw - $sw)/2);
						if ($d > 0 && $d <= $startx) $startx -= $d ;
						$sw = $tw;
					}
					$srcW = $sw;
					$srcH = $sh;

				}
				else {
					$srcW = $nw;
					$srcH = $nh;


					# lets get the start xpos
					if ($sw > $nw) {
						# uhoh the sweet width is larger than are new allowed width
						# if we have a sweet spot, then we need to make sure it is in the view.
						# So try to make the crop show our sweet spot and as much as possible of the
						# sweet width
						# if there is no sweet spot then we just center the crop horizontally
						# on the sweet area
						$startx = $minx + intval( ($sw-$nw)/2+.5);
						if ($use_sp) {
							$offset = $parms['sweetspot_xoffset'];
							if (strtolower($offset) == 'center') {
								$startx = intval($spx - $nw/2);
							}
							else {
								$offset += 0;
								if ($offset < 1) $offset = 1;

								if ($spx < $startx ) {
									$startx = $spx - $offset;
								}
								elseif ($spx +$offset > $startx + $nw) {
									$startx = $spx + $offset - $nw;
								}
							}

							if ($startx < 0) { $startx = 0;}
							elseif ($startx + $nw > $w) { $startx = $w-$nw; }
						}
					}
					elseif ($minx <= 0) {
						# hmm how did we get aminx < 0.. not allowed
						$startx =0;
					}
					else {
						# this is nice.. our new allowed width is greater than our
						# sweet width, so lets center the sweet area horizontally
						$startx = $minx - intval( ($nw-$sw)/2 +.5);
						if ($startx < 0) $startx = 0;
						if ($startx + $nw > $w) $startx = ($w-$nw);
					}

					if ($sh > $nh) {
						# our sweet height is to large.. what to do?
						# just like our sweet width we need to make sure we
						# have out sweet spot in view, otherwise center our
						# sweet area vertically.
						$starty = $miny + intval( ($sh-$nh)/2+.5);
						if ($use_sp) {
							$offset = $parms['sweetspot_yoffset'];
							$offset_top = (!empty($parms['sweetspot_yoffset_top'])) ? $parms['sweetspot_yoffset_top']+0 : $offset;
							$offset_bottom = (!empty($parms['sweetspot_yoffset_bottom'])) ? $parms['sweetspot_yoffset_bottom']+0 : $offset;

							if (strtolower($offset) == 'center') {
								$starty = intval($spy - $nh/2);
							}
							else {
								$offset += 0;
//								if ($offset < 1) $offset = 1;
								if ($spy-$offset_top < $starty ) {
									$starty = $spy - $offset_top;
								}
								elseif ($spy + $offset_bottom > $starty + $nh) {
									$starty = $spy + $offset_bottom - $nh;
//									$starty = $nh - ($spy + $offset_bottom);
								}
							}

							if ($starty < 0) { $starty = 0;}
							elseif ($starty + $nh > $h) { $starty = $h-$nh; }
						}
					}
					elseif ($miny <= 0) {
						# how can we start below 0? time to fix
						$starty = 0;
					}
					else {
						# all is good.. the new allowed height is larger than our
						# sweet height. lets center out sweet area vertically
						$starty = $miny - intval( ($nh-$sh)/2 +.5);
						if ($starty < 0) $starty = 0;
						if ($starty + $nh > $h) $starty = ($h-$nh);
					}
				}

				if (imageistruecolor($image)) {
					$timage = imagecreatetruecolor($nw,$nh);
				}
				else {
					$timage = imagecreate($nw,$nh);
				}
				#imagecopy($timage,$image,0,0,$startx,$starty,$nw,$nh);

				$this->cropscale = $nw/$srcW;


				imagecopyresampled($timage,$image,0,0, $startx,$starty,$nw,$nh,$srcW,$srcH);
				$image = $timage;

				# save our cropx and y in case its needed by plugin
				$this->cropx = round ($startx * $this->cropscale);
				$this->cropy = round($starty * $this->cropscale);

				# do projection reset
				$mapinfo = array("width" => $nw, "height" => $nh);

				list($mapinfo["lon1"], $mapinfo["lat1"]) = $map_proj->get_lonlat($startx,$starty);
				list($mapinfo["lon2"], $mapinfo["lat2"]) = $map_proj->get_lonlat($startx+$nw,$starty+$nh);

				$map_proj->init_projection($mapinfo);


			}
		}

		return $image;
	}


	Function varreplace($var) {
		$result = (isset($this->var[$var])) ? $this->cleanval($this->var[$var]) : '';
		if ($result == '') $result = (isset($this->form[$var])) ? $this->cleanval($this->form[$var]) : '';

		return $result;
	}

	Function  _format ($val, $format) {

		if ($format == 'pc') {	preg_replace("/([�������\w]+)/e","ucwords(strtolower('$1'))", $val); }
		elseif ($format == 'uc') { $val = strtoupper($val);}
		elseif ($format == 'lc') { $val = strtolower($val);}
		elseif ($format == 'r0') { $val = round($val, 0)+0; }
		return $val;
	}


	function  add_watermarks (&$image, &$parms, &$cfg,  &$hash)  {

		$watermark_path = $cfg->val(Watermark_Settings, 'watermark_path');
		$watermark_parms = $cfg->SectionParams(Watermarks);
		foreach ($watermark_parms as $key => $value) {

			$winfo = preg_replace('/\<\?(.+?)\?>/e',  'eval(\'return \1;\');', $value);
			$wm = explode('|', $winfo);
			$tc=0;
			if (sizeof($wm) == 7) {
				list($filename,$x,$y,$halign, $valign, $blend, $tc) = $wm;
			}
			else {
				list($filename,$x,$y,$halign, $valign, $blend) = $wm;
			}

			$this->insertimage($image, $watermark_path, $filename, $x,$y,$halign,$valign,$blend,$tc);


		}


		return $image;
	}

	Function  insertimage (&$image, $filepath, $file, $x,$y,$halign,$valign,$blend, $cache = false) {


//		print "$image, $filepath, $file, $x,$y,$halign,$valign,$blend<br>\n";

		$filepath = preg_replace('/[\/]$/', '', $filepath);

		$filename = ($filepath) ? "$filepath/$file" : $file;

		$iw=0; $ih=0;

		if ($filename && file_exists($filename)) {
			$ext = '';
			if (preg_match('/\.(\w+)$/', $file, $m)) $ext = $m[1];

			$use_filename = preg_replace('/\.(\w+)$/', '', $file);

			$img = ($cache) ? $this->cache_get("$filepath$use_filename") : '';
			if (empty($img)) {
				list ($img, $error) = load_image($filepath, $use_filename, $ext);
				if ($cache) $this->cache_add("$filepath$use_filename",$img);
			}


			if ($img) {
				$w = imagesx($image);
				$h = imagesy($image);
				$iw = imagesx($img);
				$ih = imagesy($img);

				$x = strtolower($x);
				$y = strtolower($y);

				$halign = strtolower($halign);
				$valign = strtolower($valign);

				if ($x == 'center') {
					$x = floor($w/2 - $iw/2);
					if ($halign == 'center') {
						$halign='';
					}
				}
				elseif ($x == 'right') {
					$x = $w - $iw;
				}
				elseif ($x == 'left') {
					$x = 0;
				}

				if ($y == 'center') {
					$y = floor($h/2 + $ih/2);
					if ($valign == 'center') {
						$valign='';
					}
				}
				elseif ($y == 'bottom') {
					$y =$h -$ih;
				}
				elseif ($y == 'top') {
					$y = 0+$ih;
				}

				if ($halign == 'center') {
					$x = floor($x - $iw/2);
				}
				elseif ($halign == 'right') {
					$x = $x - $iw;
				}

				if ($valign == 'center') {
					$y = floor($y - $ih/2);
				}
				elseif ($valign == 'top') {
					$y = $y;
				}
				elseif ($valign == 'bottom') {
					$y -=$ih;
				}

				$alphablend = ($blend < 0) ? 1 : 0;
				$blend = abs($blend);
				if ($blend < 0 || $blend > 100) { $blend = 100;}

				if (imageistruecolor($img) && !imageistruecolor($image)) {
					$newimage = imagecreatetruecolor($w,$h);
					imagecopy($newimage,$image,0,0,0,0,$w,$h);
					$image = $newimage;
				}
//print "...(x,y) = ($x,$y)<br/>";


				if ($blend < 100) {
					imagecopymerge($image,$img, $x,$y,0,0,$iw,$ih,$blend);
				}
				else {
					ImageAlphaBlending($image, true);
					imagecopy($image,$img, $x,$y,0,0,$iw,$ih);
				}
				unset($img);
			}

			return array($x,$y,$iw,$ih);
		}
	}

} // end class
?>