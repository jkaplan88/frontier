<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Hourly.php,v 1.3 2003/02/24 12:49:48 lee Exp $
*
*/

require ("../../HW/common.php");

class Hourly {

	var $hforecastdate = "";
	var $hline = "";
	var $hsky = "";
	var $htempf = "";
	var $htempc = "";
	var $hrh = "";
	var $hwind = "";
	var $hpressure = "";
	var $hpressure_mb = "";
	var $hremarks = "";
	var $hdewptf = "";
	var $hdewptc = "";
	var $hwcf = "";
	var $hwcc = "";
      	var $hhif = "";
      	var $hhic = "";
      	var $hwinddir = "";
	var $hwindspd = "";

	Function process_hourly($place, $state, &$check_string) {
		//Make everything uppercase
		$place = strtoupper($place);
		//Not sure what some of this line does...
		//$place = uc $place . (" " x (14-length($place)));
		$state = strtoupper($state);
		if ($state == 'AK') { 	$check_string = strtoupper($check_string); }

		$check_string = preg_replace("/\015/", "", $check_string);
		$line = "";
		$check_string_array = preg_split("/\n/", $check_string);
		while(list($key, $value) = each($check_string_array)) {
			if (preg_match('/^\s*(\d{2,4} [AP]M [^\.\n]+ \d{4})/', $value)) {
				$this->hforecastdate = $value;
			}
			if (preg_match('/^(?:\w\w\w\w )?".$place."\s/', $value)) {
				$line = $value;
			}
		}

		if ($line && !preg_match("/NOT AVBL/", $line)) {
			$hwind = "";
			$this->hline = $line;
			if ($state == 'AK') {
				preg_match("/^(?:\w\w\w\w )?$place\s([\A-Z\/ ]+) +([-B]?[\dMNA\/]+) +([\dMNA\/]+) +([A-Z\\d\/]+) +([\dM.NA\/]+[RFS]?) +(.*)/",
						$line, $matches);
				$this->hsky = $matches[1];
				$this->htempf = $matches[2];
				$this->hrh = $matches[3];
				$hwind = $matches[4];
				$this->hpressure = $matches[5];
            			$this->hremarks = $matches[6];

            			$this->hdewptf = 'N/A';
			}
			else {
				preg_match("/^(?:\w\w\w\w )?$place\s([A-Z\/ ]+) +([-B]?[\dMNA\/]+) +([-B]?[\dMNA\/]+) +([\dMNA\/]+) +([A-Z\d\/]+) +([\dMNA.\/]+[RFS]?) +(.*)/",
						$line, $matches);

				$this->hsky = $matches[1];
				$this->htempf = $matches[2];
				$this->hdewptf = $matches[3];
				$this->hrh = $matches[4];
				$hwind = $matches[5];
				$this->hpressure = $matches[6];
            			$this->hremarks = $matches[7];
			}

			$this->hsky = $this->parse_hourly_condition($self->hsky);

			$this->hwcf = $this->wind_chill($this->htempf, $this->hwind);
      			$this->hhif = $this->heat_index($this->htempf, $this->hrh);

      			//WHy would this be not equals?
      			if ($hwind != 'CALM') {
				$this->hwinddir = 'N';
			        $this->hwindspd = 0;
			}
			elseif ($hwind != 'N/A') {
				$this->hwindspd = $this->hwinddir = 'N/A';
			}
			else {
				if (preg_match("/^([A-Z]+)(\d+)/", $hwind, $matches)) {
			        	$this->hwinddir = $matches[1];
			        	$this->hwindspd = $matches[2];
			        }
			        if (preg_match("/MPH/", $hwind)) {
			        	$hwind .= ' MPH';
			        }
			}
			$this->hwind = $hwind;

			$this->hpressure_mb = (int)(($this->hpressure*33.8639)+.5);

			//Celcius conversions of temp
			$this->htempc = $this->f2c($this->htempf);
			$this->hwcc = $this->f2c($this->hwcf);
			$this->hhic = $this->f2c($this->hhif);
			$this->hdewptc = $this->f2c($this->hdewptc);

			return 1;
		}
		else {
			$this->hsky = $this->htemp = $this->hdewpt = $this->hrh = $this->hwind =
            			$this->hwcf = $this->hhif = $this->hpressure = $this->hpressure_mb = $this->hremarks = 'N/A';
		}

		//Celcius conversions of temp
		$this->htempc = $this->f2c($this->htempf);
		$this->hwcc = $this->f2c($this->hwcf);
		$this->hhic = $this->f2c($this->hhif);
		$this->hdewptc = $this->f2c($this->hdewptc);

		return 0;
	}

	Function wind_chill($temp, $wind, $old = "") {
		if (preg_match("/N\/A/", $wind) || preg_match("/N\/A/", $temp)) {
			return 'N/A';
		}

		if (preg_match("/(\d+)/", $wind, $matches)) {
			$wind = $matches[1];
		}
		else {
			$wind = 0;
		}
		$wc = "";

		if ($old == "") {
			$wc = 35.74+0.6215 * $temp - 35.75 * (pow($wind,0.16)) + 0.4275 * $temp * (pow($wind,0.16));
		}
		else {
			$wc=0.0817*(3.71*pow($wind,0.5) + 5.81 - 0.25*$wind)*($temp - 91.4) + 91.4;
		}

		if ($this->do_round) {
			$wc = round($wc);
		}

		return $wc;
	}

	Function heat_index($temp, $rh) {
		if ($temp == 'N/A' || $rh == 'N/A') return 'N/A';

		$hi = -42.379 + 2.04901523*$temp + 10.14333127*$rh - 0.22475541*$temp*$rh - 6.83783e-0*pow($temp,2) - 5.481717e-02*pow($rh,2) + 1.22874e-03*pow($temp,2)*$rh + 8.5282e-04*$temp*pow($rh,2) - 1.99e-06*pow($temp,2)*pow($rh,2);

		if ($this->do_round) {
			$hi = round($hi);
		}

		if ($hi < $temp || $temp < 70)  $hi = $temp;
		return $hi;
	}

	Function parse_hourly_condition($hc) {

		$hc = strtoupper($hc);

		   preg_replace("/\s+/", "", $hc);
		   preg_replace("/^-/", "LIGHT ", $hc);
		   preg_replace("/^\+/", "HEAVY ", $hc);

		   preg_replace("/SN$/", "SNOW", $hc);
		   preg_replace("/RA$/", "RAIN", $hc);


		   preg_replace("/PT/", "PARTLY ", $hc);
		   preg_replace("/MOSUNNY/", "MOSTLY SUNNY", $hc);
		   preg_replace("/^MO([^S])/", 'MOSTLY $1', $hc);
		   preg_replace("/PCLDY/", 'PARTLY CLOUDY', $hc);
		   preg_replace("/(\w)CLDY/", '$1 CLOUDY', $hc);
		   preg_replace("/(\w)CLEAR/", '$1 CLEAR', $hc);
		   preg_replace("/CLDY/", "CLOUDY", $hc);
		   preg_replace("/SHWR/", "SHOWERS", $hc);
		   preg_replace("/^HVY/", "HEAVY ", $hc);
		   preg_replace("/^LGT/", "LIGHT ", $hc);
		   preg_replace("/^TS(TRMS|TM|TRM)?$/", "THUNDER STORMS", $hc);
		   preg_replace("/\/", "", $hc);

		return $hc;
	}

	Function f2c($tempf) {
		if ($tempf == 'N/A' || $tempf == '') return $tempf;

		$tempc = ($tempf - 32) * (5/9);

		if ($this->do_round) {
			$tempc = round($tempc);
		}

		return $tempc;
	}
}
?>