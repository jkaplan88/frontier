<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: USA.php,v 1.36 2006/09/29 19:30:37 wxfyhw Exp $
*
*/


class Zone {

	var $forecastdate = "";
	var $forecast_text = "";
	var $header = "";
	var $warnings = "";
	var $zone_warnings = '';

	var $debug=0;

	var $day_total = 0;
	var $day_fc = array();
	var $day_wx = array();
	var $day_hi = array();
	var $day_lo = array();
	var $day_pop = array();
	var $day_title = array();
	var $day_wind = array();
	var $day_winddir = array();

	var $forecast_day='';

	var $cities = array();
	var $cities_total = -1;
	var $counties = array();
	var $counties_total = -1;

	var $tzdif=0;



	//Day Titles array
	var $day_titles = array("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY");

	var $days_hash = array("SUNDAY"=>0, "MONDAY"=>1, "TUESDAY"=>2,"WEDNESDAY"=>3,"THURSDAY"=>4,
         "FRIDAY"=>5, "SATURDAY"=>6);

	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	var $spec_temps = array();
	var $spec_pops = array();

	var $use_today = 'TONIGHT';


	Function Zone($debug) {
		$this->debug = $debug;
	}

	Function find_zone($place, $wx_zone, $state, &$wx_info) {
		//Make place and state upper case
		$place = strtoupper($place);
		$state = strtoupper($state);

		$section = "";

		//Return an error if there was an error in the wx info
		if (preg_match("/[Ee]rror/", $wx_info)) {
			return 0;
		}

		//Remove the funny character
		$wx_info = preg_replace('/^ +/m', '', $wx_info);
		$wx_info = preg_replace('/\036\036/', '$$', $wx_info);
		$wx_info = preg_replace('/\03/', "\n\n$$\n", $wx_info);
		$wx_info = preg_replace('/\r/', '', $wx_info);

		if (!preg_match('/\$\$\n/',$wx_info)) $wx_info .= "\n\n\$\$\n\n";
		$wx_info = preg_replace('/\n\n([A-Z][A-Z]Z\d\d\d)/', "\n\n\$\$\n\n\n$1", $wx_info);

		//Break up the wx_info into sections
		preg_match_all('/\n(\w\wZ.*?\n.*?)\n(?:\$\$|NNNN?|<HR>)/s', $wx_info, $sections);
if ($this->debug) "total sections = " . sizeof($sections[0]) . "<br>\n";
		//Loop through the sections and see if it's the one we are looking for
		for ($i = 0; $i < sizeof($sections[0]); $i++) {
			//Sep. header and section
//if ($this->debug) print "section=" . $sections[1][$i] . "<br>\n";
			if (!preg_match('/^\.\w+(?: \w+)?\.\.\./m', $sections[1][$i]))
				$sections[1][$i] = preg_replace('/^(\w+(?: \w+)?\.\.\.\w+\.? \w+\.? \w+)/m','.$1', $sections[1][$i]);

			preg_match('/^(.+?\n)\./s', $sections[1][$i], $matches);

			//Make sure a header match was made before continuing
			if (sizeof($matches) != 0) {
				$this->header = $matches[1];
				$section = preg_replace('/^(.+?\n)\./s', "\012", $sections[1][$i]);


				//Check to see if this is the section we want
				if (($wx_zone && $this->check_zones($wx_zone, $state,  $this->header)) ||
					(!$wx_zone && $place && preg_match('/$place/', $this->header))) {
					$section = preg_replace('/=\s*$/', "", $section);
					$this->forecast_text = "\n".$section;

					$this->get_dates($this->header);
	        				return true;
				}
			}
		}

		$this->forecastdate = '';
		$this->forecast_text = '';
		$this->header = '';
		return false;
	}

	Function check_zones($wx_zone, $state, &$header) {

		$state = strtoupper($state);

		if (preg_match('/('.$state.'[Z].+?-)\s*?\d\d\d\d\d\d-/s', $header, $matches)) {
			$main_zone_args = $matches[1];

			preg_match_all('/(\w\w)Z([^A-Za-z]+)/', $main_zone_args, $matches);
			for ($i = 0; $i < sizeof($matches[0]); $i++) {
				//Continue only if state matches
				if ($state == $matches[1][$i]) {
					$zone_args = $matches[2][$i];

					preg_match_all('/(.+?)-/', $zone_args, $matches2);
					for ($j = 0; $j < sizeof($matches2[0]); $j++) {
						$zone_arg = $matches2[1][$j];

						if ($zone_arg == $wx_zone)
							return true;
						if (preg_match('/(\d+)>(\d+)/', $zone_arg, $matches3)) {
							if ($wx_zone >= $matches3[1] && $wx_zone <= $matches3[2])
								return true;
						}

					}
				}
			}
		}

		return false;
	}



	Function process_zone( &$wx_array, $daysonly, &$day_title_array,$today='', $shortterm=0) {
		$debug = $this->debug;

		$days_hash = $this->days_hash;

		if (!$today) $today = $this->use_today;

		$forecast_date = $this->forecastdate;

		$day_title = "";
		$day_fc = "";
		$day_fcs = array();
		$day_wxs = array();
		$day_his = array();
		$day_los = array();
		$day_pops = array();
		$day_total = 0;
		$line = "";
		$firstnight = 0;
		$counter = 0;
		$spec_temps = $sc_counter = 0;

 		$day_winds = array();
		$day_winddirs = array();



		if (!$day_title_array) {$day_title_array =& $this->day_titles;}


		$ftext = $this->forecast_text;
		//Skipped for now
		if (preg_match('/(?:\.<|\&\&)([^=.]+)/', $ftext, $m)) {
			$ftext = preg_replace('/(?:\.<|\&\&)([^=.]+)/', '.', $ftext);
			$spec_temps = $this->get_special_temps($m[1]);
		}

$spec_temps=0;

		$lines = preg_split('/\012\.(?!\.)/', $ftext);
		foreach ($lines as $line) {
			$line = trim($line);
if ($this->debug) print "line=$line<br>\n";
			if ($debug) print "line=<pre>$line</pre><br>\n";
			//If a watch, warning, etc then handle in a different way
			if (!$shortterm && preg_match('/WARNING|WATCH|ADVISORY|THREAT/', $line) ||
				preg_match('/^\.\.\.?.+\.\.\./s', $line)) {
				$this->zone_warnings .= preg_replace(array('/^\.+/m', '/\.+$/m'), array('',''), $line) . "\n";
			}
			//Otherwise handle as regular line
			elseif($line) {
				if (preg_match('/^(.*?)\.\.+(.*)$/s', $line, $line_matches)) {
					$day_title = $line_matches[1];
					$day_fc = $line_matches[2];
					if (!preg_match('/(?:EXTENDED (?:FORECAST|OUTLOOK))|3 TO \d DAY FORECAST/', $day_title)) {
if ($this->debug) print "day_title=$day_title<br>\n";
						//Skipped for now
						if ($daysonly && preg_match('/NIGHT|EVENING/', $day_title)) {
							if (!$counter && !$firstnight) {
								$firstnight = 1;
								if ($daysonly > 1) continue;
								$day_title = $today;

							}
							elseif($counter) {
								if ($spec_temps && $sc_counter < $spec_temps) {
									$day_los[$counter-1] = $this->spec_temps[avg][$sc_counter];
								}
								else {
									list($junk,$day_los[$counter-1]) = $this->parse_temps($day_title, $day_fc);
								}

								if (preg_match('/\s[Aa][Nn][Dd]\s/', $day_title)) {
									list($day1,$day2) = preg_split('/\s[Aa][Nn][Dd]\s/', $day_title);
									$day_title = (preg_match('/NIGHT|EVENING/', $day1)) ? $day2 : $day1;
								}
								elseif (!preg_match('/\sTHROUGH\s/', $day_title))  { $sc_counter++; continue;}
							}
						}

						//Store the day title and day fc
						$this->day_title[$counter] = $day_title;
						$day_fcs[$counter] = $day_fc;

						$day_fc = preg_replace('/\s+/', ' ', $day_fc);

						//Now, get the day weather
						if ($wx_array) {
							$wxtype = "";
							if (preg_match('/^(.+?LIKELY)/', $day_fc, $matches)) {
								$wxtype = $this->get_wxtype($matches[1], $wx_array);
							}
							if (!$wxtype || $wxtype == 'N/A') {
								$wxtype = $this->get_wxtype($day_fc, $wx_array);
							}
							$day_wxs[$counter] = $wxtype;
						}

						//Get the pop
						$day_pops[$counter] = $this->parse_pop($day_fc);

						$day_winds[$counter] = $this->parse_wind($day_fc);
						$day_winddirs[$counter] = $this->parse_winddir($day_fc);


						if ($spec_temps && $sc_counter < $spec_temps) {
							if (preg_match('/NIGHT|EVENING/', $day_title)) {
								$day_his[$counter] = '';
								$day_los[$counter] = $this->spec_temps['avg'][$sc_counter];
							}
							else {
								$day_his[$counter] = $this->spec_temps['avg'][$sc_counter];
								$day_los[$counter] = '';
							}
						}
						else {
							list($day_his[$counter], $day_los[$counter]) = $this->parse_temps($day_title, $day_fc);
						}


					         //if the day title has and then lets spilt up into proper days
					         if (preg_match('/\s[Aa][Nn][Dd]\s/', $this->day_title[$counter])) {
					         	$tmp_array = preg_split('/\s[Aa][Nn][Dd]\s/', $this->day_title[$counter]);
					         	$day1 = $tmp_array[0];
					         	$day2 = $tmp_array[1];
					         	$this->day_title[$counter] = $day1;
							$this->day_title[++$counter] = $day2;
							$day_fcs[$counter] = $day_fcs[$counter-1];
							$day_wxs[$counter]  = $day_wxs[$counter -1];
							$day_his[$counter] = $day_his[$counter-1];
							$day_los[$counter] = $day_los[$counter-1];
							$day_pops[$counter] = $day_pops[$counter - 1];
							$day_winds[$counter] = $day_winds[$counter - 1];
	     					$day_winddirs[$counter] = $day_winddirs[$counter - 1];
					         }
					         elseif (preg_match('/\sTHROUGH\s/', $this->day_title[$counter])) {
					         	list($day1, $day2) = preg_split('/\sTHROUGH\s/', $this->day_title[$counter]);
					         	$day1 = trim($day1);	$day2 = trim($day2);

					         	list($day1s,$usedprevday) = $this->get_day_number($day1,($counter > 0) ? $this->day_title[$counter-1] : '', $forecast_date);
					         	list($day2e,$junk) = $this->get_day_number($day2,'', $forecast_date);
					         	if ($debug) {
					         		print "day1=$day1...(" . $this->day_title[$counter-1] . ")...$day2=$day2e<br>\n";
					         		print "......".$this->day_title[$counter]."...$day_fc($counter) <br>\n";
					         	}

				         		$i = $day1s-1;
				         		$cc=0;
				         		while ($i != $day2e  && $day2e >= 0) {
				         			if (++$i > 6) $i = 0;
				         			++$cc;
				         			if ($daysonly) {
				         				if ($cc == 1 && $usedprevday) {
				         					if ($debug) print"used prev day<br>\n";
				         					list ($junk, $day_los[$counter-1]) = $this->parse_temps($day1,$day_fc);
				         					$this->day_title[$counter] = $this->day_titles[$i];
				         				}
				         				else {
				         					if ($cc == 1) {
				         						$this->day_title[$counter] = $this->day_titles[$i];
				         					}
				         					else {
				         						if ($usedprevday && $cc == 2) {
				         							$this->day_title[$counter] = $this->day_titles[$i];
				         						}
				         						else {
				         							$this->day_title[++$counter] = $this->day_titles[$i];
				         							$day_fcs[$counter] = $day_fcs[$counter-1];
												$day_wxs[$counter]  = $day_wxs[$counter -1];
												$day_his[$counter] = $day_his[$counter-1];
												$day_los[$counter] = $day_los[$counter-1];
												$day_pops[$counter] = $day_pops[$counter - 1];
						     					$day_winds[$counter] = $day_winds[$counter - 1];
	          									$day_winddirs[$counter] = $day_winddirs[$counter - 1];

											}
										}
									}
								}
								else {
									if ($cc == 1) {
										$this->day_title[$counter] = $day1;
									}
									else {
										$this->day_title[++$counter] = $this->day_titles[$i];
		         							$day_fcs[$counter] = $day_fcs[$counter-1];
										$day_wxs[$counter]  = $day_wxs[$counter -1];
										$day_his[$counter] = $day_his[$counter-1];
										$day_los[$counter] = $day_los[$counter-1];
										$day_pops[$counter] = $day_pops[$counter - 1];
							   			$day_winds[$counter] = $day_winds[$counter - 1];
	       								$day_winddirs[$counter] = $day_winddirs[$counter - 1];
									}
								}

							}
							if ($i == $day2e && $daysonly) {
								$this->day_title[++$counter] = $this->day_titles[$i];
         							$day_fcs[$counter] = $day_fcs[$counter-1];
								$day_wxs[$counter]  = $day_wxs[$counter -1];
								$day_his[$counter] = $day_his[$counter-1];
								$day_los[$counter] = $day_los[$counter-1];
								$day_pops[$counter] = $day_pops[$counter - 1];
						 		$day_winds[$counter] = $day_winds[$counter - 1];
	      						$day_winddirs[$counter] = $day_winddirs[$counter - 1];
							}
						}

					} //$day_title preg filter
				} //$line preg filter

				$counter++;
          			$sc_counter++;
			} //elseif ($line)
		} //end of for loop

		if ($daysonly > 1) {

			$wday = gmdate('w', time()+($this->tzdif*3600));
			$tday = gmdate('j', time()+($this->tzdif*3600));

			// if there is a first night we need to attempt to figure if its a still night even if we
			// may be on a new day so our days do not get off by 1

			if (!$firstnight || ($this->forecast_day && $this->forecast_day < $tday)) {
				$wday--;
				if ($debug) print "fday=".$this->forecast_day ."<br>tday=$tday<br>\n";
			}

			for ($i=0; $i< $counter; $i++) {
				if (++$wday > 6) {$wday = 0;}
				if ($daysonly == 2) {$this->day_title[$i] = $day_title_array[$wday]; }
				else {$this->day_title[$i] = substr($day_title_array[$wday],0,3); }
			}
		} //end if ($daysonly > 1)

		$counter--;
		$day_total = $counter;

		//lets step through and see if we need to add the 3rd days lows
		if ($daysonly && $counter > 1) {
			for ($i = 1; $i < $counter; $i++) {
				if ($day_los[$i] == '') {
					if (isset ($day_los[$i-1]) && isset($day_los[$i+1]) && $day_los[$i-1] != '' && $day_los[$i+1] != '') {
						$t = ($day_los[$i-1]+$day_los[$i+1])/2;
						if ($t>0) {$t = floor($t+.5); } else {$t = floor($t-.5);}
						$day_los[$i]=$t;
					}
				}
			}
		}

		$header = $this->header;
		if (preg_match('/\d\d\d\d\d\d-\s+(?:[^-\n]+\n)?(\w.+?)-?\n(?:\d+|INCLUDING)/s', $header, $matches)) {
			$this->counties = preg_split('/-/', $matches[1]);
			$this->counties_total = sizeof($this->counties)-1;
		}

		if (preg_match('/INCLUDING THE CITIES OF[\s.]*?(\w.+?)\n\d+/s', $header, $matches)) {
			$this->cities = preg_split('/\.\.\./', $matches[1]);
			$this->cities_total = sizeof($this->cities)-1;
		}


		$this->day_total = $day_total;
		$this->day_fc = $day_fcs;
		$this->day_wx = $day_wxs;
		$this->day_hi = $day_his;
		$this->day_lo = $day_los;
		$this->day_pop = $day_pops;
		$this->day_wind = $day_winds;
		$this->day_winddir = $day_winddirs;


		return $counter;
	}

	Function get_wxtype($wx_str, $types) {
		for ($i = 0; $i < sizeof($types); $i++) {
			$t= str_replace('/','\/',$types[$i]);
			if (preg_match('/\b'.$t.'/i', $wx_str)) {
				return 	$types[$i];
			}
		}
		return "";
	}


	Function parse_pop($forecast) {
		$pop = "";

		$forecast = strtolower($forecast);

		if (preg_match('/chance\s+?of\s+?[^.\d]+?(\d+)\s+?percent/', $forecast, $matches)) {
			$pop = $matches[1];
		}
		elseif (preg_match('/(\d+)\s+?percent(?:\s+?chance)?/', $forecast, $matches)) {
			$pop = $matches[1];
		}

		return $pop;
	}


	Function parse_wind($forecast) {
		$wind = "";

		$forecast = strtolower($forecast);

  		if (preg_match('/(\d+)\s+?mph?/', $forecast, $matches)) {
			$wind = $matches[1];
		}

		return $wind;
	}
	Function parse_winddir($forecast) {
		$winddir = "";

		$forecast = strtolower($forecast);

  		if (preg_match('/(\w+)\s+?winds?/', $forecast, $matches)) {
			$winddir = $matches[1];
		}

		return $winddir;
	}


	Function parse_temps($day_title, $forecast) {
		$segment = "";
		$low = "";
		$high = "";

		$debug = $this->debug;

		$day_title = strtolower($day_title);
		$forecast = strtolower($forecast);


		$match[0] = '/\//'; $replacement[0] = "";
		$match[1] = '/teens/'; $replacement[1] = "10s";
		$match[2] = '/freezing/'; $replacement[2] = "32";
		$match[3] = '/trade[^.]+?\./'; $replacement[3] = "";
		$match[4] = '/\binland\b/'; $replacement[4] = "";
		$match[5] = '/\salong[^.]+?\./'; $replacement[5] = "\.";
		$match[6] = '/\bbut\b/'; $replacement[6] = ".";
		$match[7] = '/[\n\r\t\f ]+/'; $replacement[7] = " ";
		$match[8] = '/\sexcept/'; $replacement[8] = "\.";
		$match[9] = '/ ?with heat indices[^.]+?\./'; $replacement[9] = "\.";
		$match[10] = '/high cloudiness/'; $replacement[10] = " ";
		$match[11] = '/low cloud/'; $replacement[11] = " ";
		$match[12] = '/high/'; $replacement[12] = ". high";
		$match[13] = '/accumulation[^.]+?\./'; $replacement[13] = "";
		$match[14] = '/(\d+)\s+below(?:\s+zero)?/'; $replacement[14] = "-\\1";
		$match[15] = '/(\d+)\s+above/'; $replacement[15] = "+\\1";
		$match[16] = '/\bzero/'; $replacement[16] = "0";
		$match[17] = '/wind\s+chills[^.]*\./'; $replacement[17] = "";
		$match[18] = '/\babout\b/'; $replacement[18] = "near";
		$match[19]  = '/diminishing[^\.]+/'; $replacement[19] = '';
		$match[20] = '/\d+\s+?mile/'; $replacement[20] = '';
		$match[21] = '/wind[^y\.][^\.]+/'; $replacement[21] = '';
		$match[22] = '/near\s+?record(?:\s+?temp)/'; $replacement[22] = '';
		$match[23] = '/\s+\./'; $replacement[23] = '.';
		$match[24] = '/heat\s+index[^.]*\./'; $replacement[24] = '';
		$match[25] = '/\bcoastside\b/'; $replacement[25] = '';


		$forecast = preg_replace($match, $replacement, $forecast);

		if (preg_match('/(high|low) temperatures\.\.\.(.+)$/s', $forecast, $m)) {
			$type = $m[1]; $temps = $m[2];
			$sum = 0; $num =0;

			if (preg_match_all('/\s(-?\d+) at/m', $temps, $m)) {

				for ($j =0; $j < sizeof($m[0]); $j++) {
					$sum+= $m[1][$j]; $num++;
				}
				$sum /= $num;
				if ($sum > 0) { $sum = floor($sum+.5);} else {$sum = floor($sum-.5);}

				if ($type == 'high') { return array($sum, ''); }
				else { return array('', $sum);}
			}
		}

		//echo "Forecast says:".$forecast."<br>";

		if (preg_match('/night|evening/', $day_title) &&
			!preg_match('/ (and|through) /', $day_title)) {

			if ($debug) print "Doing night<br>\n";
			$forecast = preg_replace('/\blows? (in)/', '\1', $forecast);
			$low = $this->get_temp(preg_replace('/highs?[^.]+\./', '.', $forecast));
			$high = "";
		}
		else {
			if (preg_match('/(\blows? [^\.]+\.)/', $forecast, $matches)) {
				if ($debug) print "doing low<br>\n";
				if ($debug) print "forecast=$forecast<br>seg=".$matches[1]."<br>\n";

				$segment = substr($matches[1], 3);
				$segment = preg_replace('/ and/', ".", $segment);
				$low = $this->get_temp(preg_replace('/highs?[^.]+\./', '.',$segment));
			}
			else {
				$low = "";
			}

			$forecast = preg_replace('/highs? [^\d\.]+\./', "", $forecast);
			if (preg_match('/((?:highs?|temperatures?) [^\.]+\.)/', $forecast, $matches)) {
				$segment = substr($matches[1], 4);
				$segment = preg_replace('/ and/', ".", $segment);
				$high = $this->get_temp($segment);
				if ($low > $high)
					$low = "";
			}
		}

		return array($high, $low);
	}

	Function get_temp($segment) {
		$temp = "";
		$debug = $this->debug;

		if ($debug) echo "Searching segment:".$segment."<br>\n";
		if (preg_match('/([\d-]+)\s*?-\s*?([\d-]+)(?![\d ]*?mph)/', $segment, $m)) {
			$fst = $m[1];
			if ($m[2] <0) {  $fst = (substr($m[1],0,1) != '+' && $m[1]>0) ? -$m[1] : $m[1];}
			$temp =  ($fst + $m[2])/2;
			$temp = ($temp < 0) ? round($temp-.5) : round($temp+.5);
		}
		elseif(preg_match('/mid[^.]+upper[^\d.+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1],0,strlen($m[1]) -1);
			$temp .= "7";
		}
		elseif(preg_match('/upper\s+?([+-]?\d+)s?\s+?to\s+?lower\s+?([+-]?\d+)s?[^\d.+-]*?\./', $segment, $m)) {
			$temp1 = substr($m[1],0,strlen($m[1]) -1) . '9';
			$temp2 = substr($m[2],0,strlen($m[1]) -1) . '1';
			$temp = floor(($temp2 - $temp1)/2) + $temp1;
		}

		elseif(preg_match('/upper[^.\d+-]+([+-]?\d+)[^\d.]+(?:low|mid)[^\d.+-]+([+-]?\d+)s?[^\d.]*\./', $segment, $m)) {
			$temp = $m[2];
		}
		elseif(preg_match('/upper[^.\d+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1],0,strlen($m[1]) -1);
			$temp .= "9";
		}
		elseif(preg_match('/low[^.\d]+mid[^.\d+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1],0,strlen($m[1]) -1);
			$temp .= "3";
		}
		elseif(preg_match('/mid[^\d.]+\d+s?\s+?to\s+?mid\s+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = $m[1];
		}
		elseif(preg_match('/upper\s+?([+-]?\d+)s?\s+?to\s+?upper\s+?([+-]?\d+)s?\./', $segment, $m)) {
			$temp = round(($m[2] - $m[1])/2) + $m[1];
		}
		elseif(preg_match('/the\s+?([+-]?\d+)s?[^\d.+-]*?\sto\s[^\d.+-]*?upper\s+?([+-]?\d+)s?[^\d.+-]*?\./', $segment, $m)) {
			$temp1 = substr($m[1],0,strlen($m[1]) -1) . '5';
			$temp2 = substr($m[2],0,strlen($m[1]) -1) . '9';
			$temp = floor(($temp2 - $temp1)/2) + $temp1;
		}
		elseif(preg_match('/(-?\d+)s?\s+to[^\d.+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$fst=$m[1];
			if ($m[2] <0) {  $fst = (substr($m[1],0,1) != '+' && $m[1]>0) ? -$m[1] : $m[1];}
			$temp =  ($fst + $m[2])/2;
			$temp = ($temp < 0) ? round($temp-.5) : round($temp+.5);
		}
		elseif(preg_match('/mid[^\d.+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1],0,strlen($m[1])-1);
			$temp .= "5";
		}
		elseif(preg_match('/([+-]?\d+)\D+valleys\D+([+-]?\d+)\D+ridge/', $segment, $m)) {
			$temp = round(($m[2] - $m[1])/2) + $m[1];
		}
		elseif(preg_match('/(?:around|toward|near)[^\d.+-]+([+-]?\d+)(?![^.mfp]*(?:mph|feet|percent))/', $segment, $m)) {
			$temp = $m[1];
		}
		elseif(preg_match('/in\s+the\s+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1], 0, strlen($m[1]) -1);
			$temp .="5";
		}
		elseif(preg_match('/low[^\d.+-]+([+-]?\d+)s?\./', $segment, $m)) {
			$temp = substr($m[1],0,strlen($m[1]) -1);
			$temp .= "1";
		}
		elseif(preg_match('/(-?\d+)\s+to\s+([+-]?\d+)(?![^.mf]*(?:mph|feet))/', $segment, $m)) {
			$temp = round(($m[2] - $m[1])/2) + $m[1];
		}
		elseif(preg_match('/[sS]\s+?([+-]\d+)\./', $segment, $m)) {
			$temp = $m[1];
		}
		else {
			$temp = "";
		}

		if ($debug) print "...temp found = $temp<br>\n";
		if (substr($temp,0,1) == '+') {
			$temp += 0;
		}

		return $temp;
	}

	Function get_special_temps($temp_str) {
		$total = 0;
		$avg_temps = array();

		preg_match_all('/^([A-Z ]+)([\d-][\d -]+)\/([\d -]+)$/', $temp_str, $matches);
		for ($i = 0; $i < sizeof($matches[0]); $i++) {
			$place = trim($matches[1][$i]);
			$temps = preg_replace('/\s+/', " ", trim(preg_replace('/--/', "", $matches[2][$i])));
			$pops = preg_replace('/\s+/', " ", trim(preg_replace('/--/', "", $matches[3][$i])));

			if ($temps) {
				$total++;
				$temps = explode(' ', $temps);
				$i = 0;

				for ($j = 0; $j < sizeof($temps); $j++) {
					$avg_temps[$i++] += $temps[$j];
				}

				$this->spec_temps[$place] = $temps;
			}

			if ($pops) {
				$this->spec_pops[$place] = explode(' ', $pops);
			}
		}

		if ($total == 0) {return 0;}

		$num_days = sizeof($avg_temps);
		for ($i = 0; $i < $num_days; $i++) {
			if ($avg_temps[$i] < 0) {
				$tmp = $avg_temps[$i]/$total -.5;
				$avg_temps[$i] = settype($tmp, "integer");
			}
			else {
				$tmp = $avg_temps[$i]/$total +.5;
				$avg_temps[$i] = settype($tmp, "integer");
			}
		}

		$this->spec_temps["avg"] = $avg_temps;

		return $num_days;
	}


	Function get_dates(&$header_ref) {
		$debug = $this->debug;

		if (preg_match('/(\d{1,4}) +?([AP]M)? *?(\w+) (?:\(\d{1,4} [AP]M \w+\) )?(\w+) (\w+) +?(\d\d?) +?(\d\d\d\d)/',
				$header_ref, $m)) {
			$time = $m[1];
			$ampm = $m[2];

			if (!$ampm) {
				if (strlen($time) < 4) {
					$ampm = 'AM';
				}
				else {
					$hour = substr($time,0,2);
					if ($hour >= 12) {
						$ampm = 'PM';
						if ($hour > 12) $hour -= 12;
					}
				}
			}
			$this->forecastdate = "$time $ampm $m[3] $m[4] $m[5] " . sprintf('%02u %04u', $m[6], $m[7]);

			$this->forecast_day = $m[6];
			$bmon = $this->month_names[$m[5]]-1;
			$bday = $m[6]; $byear = $m[7];
			list ($bmon, $bday, $byear) = array($this->month_names[$m[5]]-1,$m[6],$m[7]);

			if (preg_match (' /-(\d\d)(\d\d\d\d)-\n/', $header_ref, $m)) {
				$eday = $m[1]; $ehrm=$m[2];
				$this->forecast_kill_time = $m[1] . $m[2];

				if ($eday < $bday) { $emon = ($bmon == 11) ? 0 : $bmon+1; }
					else { $emon = $bmon;}
				$eyear = ($emon < $bmon && $emon == 0) ? $byear +1 : $byear;

				$this->forecast_expiration = "$ehrm GMT " . $this->month_nums[$emon] . sprintf(' %02u %04u', $eday, $eyear);

				$eall = sprintf('%04u%02u%02u%04u', $eyear, $emon+1, $eday, $ehrm);
				$call = gmdate('YmdHi', time());
				if ($eall < $call) {$this->forecast_expired =1; } else {$this->forecast_expired = 0; }
			}
		}
		else {
			$this->forecastdate = "UNKNOWN";
			$this->forecast_expiration = 'UNKNOWN';
			$this->forecast_expired  = 0;
		}

   }



	Function get_day_number ($day_name, $prev_day_name, $date) {

		# if day_name is simple day name such as monday then we can return
		if (isset($this->days_hash[$day_name]))  return array($this->days_hash[$day_name], 0);

		# more complex day name. 1st lets see if it contains "night or evening.
		# if so then strip the night & day and see if it works

		if (preg_match('/NIGHT|EVENING/', $day_name)) {

			# lets take off the night/evening and see if we have a good day
			# if so then return the day number
			$tday = preg_replace('/\s*(?:NIGHT|EVENING)/', '', $day_name);
			if (isset($this->days_hash[$tday]))  return array($this->days_hash[$tday], 1);

			if ($prev_day_name) {
				list($pday, $night) = $this->get_day_number($prev_day_name, '', '');
				if ($pday > -1) return array($pday,1);
			}
			if (preg_match('/(\w\w\w)\s+?(\d\d?)\s+?(\d\d\d\d)/', $date, $m)) {
				return array($this->weekday($m[3],$this->month_names[$m[1]], $m[2]), 0);
			}
			else { return array(-1,0);}
		}
		else {
			if ($prev_day_name) {
				list($pday, $junk) = $this->get_day_number($prev_day_name,'','');
				if ($pday > -1) {
					$pday++; if ($pday > 6) $pday=0;
					return array($pday,1);
				}
			}
			if (preg_match('/(\w\w\w)\s+?(\d\d?)\s+?(\d\d\d\d)/', $date, $m)) {
				return array($this->weekday($m[3],$this->month_names[$m[1]], $m[2]), 0);
			}
			else { return array(-1,0);}
		}
	}

	Function weekday ($year, $mon, $day) {

		$mon -= 2;
		if ($mon <= 0) {--$year;	$mon += 12;}

		 $cent = floor($year/100);
		$year %= 100;

		 $wday = ($day + floor(2.6*$mon - 0.2)	- 2*$cent + $year + floor($year/4) + floor($cent/4));
		while ($wday < 0) { $wday += 7; }
		$wday %= 7;

		return $wday;
	}



}
?>