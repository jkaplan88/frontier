<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: NOAAXML.php,v 1.4 2006/03/09 02:04:47 wxfyhw Exp $
*
*/

Class CurrentsNOAAXML {

	var $NOAAIconWx = array();

	var $do_round = "";
	var $decoded_line = "";
	var $hsky = 'N/A';
	var $hsky_conditions = '';
	//var $hweather = 'N/A';
	var $htempf = 'N/A';
	var $htempc = 'N/A';
	var $hdewptf = 'N/A';
	var $hdewptc = 'N/A';
	var $hrh = 'N/A';
	var $hwind = 'N/A';
	var $hwinddir = 'N/A';
	var $hwindspd = 'N/A';
	var $hwinddir_deg = 'N/A';
	var $hwcf = 'N/A';
	var $hwcf_old = 'N/A';
	var $hwcc = 'N/A';
	var $hhif = 'N/A';
	var $hhic = 'N/A';
	var $hpressure = 'N/A';
	var $hpressure_mb = 'N/A';
	var $hremarks = 'N/A';
	var $hvisibility = 'N/A';
	var $hvisibility_miles = 'N/A';
	var $hvisibility_meters= 'N/A';
	var $hquk = 'N/A';
	var $hqul = 'N/A';
	var $metar = 'N/A';
	var $weather = "";
	var $hweather='';
	var $gusts = "";
	var $hforecastdate='';

	var $fuelmoisture = 'N/A';
	var $fueltempf = 'N/A';
	var $batvolt = 'N/A';

	var $pwssoftware_type = 'N/A';
	var $pwsurltext = 'N/A';
	var $pwslon = 'N/A';
	var $pwslat = 'N/A';
	var $pwsurl = 'N/A';
	var $pwsrainin = 'N/A';

	var $hplace='';
	var $hstate='';
	var $hcountry = '';

	var $default_hsky = 'fair';

	var $months = array('JAN' => 0, 'FEB' => 1, 'MAR' => 2, 'APR' => 3, 'MAY' => 4, 'JUN' =>5, 'JUL' =>6, 'AUG' =>7, 'SEP' =>8, 'OCT'=>9, 'NOV'=>10, 'DEC'=>11);

	var $tzs = array('EST'=>-5,'CST'=>-6,'MST'=>-7,'PST'=>-8,'AST'=>-9,'KST'=>-9, 'EDT'=>-4,'CDT'=>-5,'MDT'=>-6,'PDT'=>-7, 'ADT' => -8, 'KDT'=> -8, 'HST' =>-10);


	Function CurrentsNOAAXML(&$noaaiconwx) {
		$this->NOAAIconWx =& $noaaiconwx;
	}




	Function convert_wind($hwind) {
		$hwind = preg_replace('/EAST/i','E',$hwind);
		$hwind = preg_replace('/WEST/i','W',$hwind);
		$hwind = preg_replace('/NORTH/i','N',$hwind);
		$hwind = preg_replace('/SOUTH/i','S',$hwind);
		$hwind = preg_replace('/VARIABLE/i','VRB',$hwind);

		return $hwind;
	}
	Function process_metar($icao, $type, &$metar_data) {
	      	$found = 0;
	      	$place ='';
	      	$state='';
	      	$country='';

	      	if (preg_match('/<location>([^\n]+), *([^,\n]+)<\/location>/', $metar_data, $m)) {
	      		$place = $m[1];
	      		$state = $m[2];
	      		if ($type) $place .=", $state";
	      		$country = $state;
	      	}
	      	$this->hplace = $place;
	      	$this->hstate=$state;
	      	$this->hcountry=$country;

//	<observation_time_rfc822>Fri,  9 Dec 2005 10:00:00 -0400 AST</observation_time_rfc822>
			if (preg_match('/<observation_time_rfc822>.+?(\d\d?)\s+?([A-Z]\w\w)\s+?(\d\d\d\d)\s+?(\d\d?):(\d\d):(\d\d)\s+?([\+\-]?\d\d)(\d\d)/', $metar_data, $m)) {

				$hday = $m[1];
				$hmonth = strtoupper($m[2]);
				$hmon = (isset($this->months[$hmonth])) ? $this->months[$hmonth]+1 : gmdate('m');
				$hyear = $m[3];

				$hh = $m[4];
				$hm = $m[5];
				$hs = $m[6];

				$htzdif = $m[7] + $m[8]/60;


				$newtime = gmmktime($hh,$hm,$hs,$hmon,$hday,$hyear) - 3600 * $htzdif;
				$this->hforecastdate = gmdate('Y.m.d Hi', $newtime) . ' UTC';

			}
			else {
				$this->hforecastdate = 'UNKNOWN';
			}



			if (preg_match('/<wind_dir>([^<]+)</', $metar_data,$m)) {$this->hwinddir = $this->convert_wind($m[1]);}
			if (preg_match('/<wind_degrees>([^<]+)</', $metar_data,$m)) {$this->hwinddir_deg = $m[1];}
			if (preg_match('/<wind_mph>([^<]+)</', $metar_data,$m)) {$this->hwindspd = $m[1];}
			if ($this->hwindspd == 'NA') {$this->hwindspd = 'N/A';} else { $this->hwindspd = round($this->hwindspd);}
			if (preg_match('/<wind_gust_mph>([^<]+)</', $metar_data,$m)) {$this->gusts = $m[1];}
			if ($this->gusts == 'NA') $this->gusts = 'N/A';
			if (preg_match('/<wind_string>(\w[^<]+)</', $metar_data,$m)) {
				$hwind = strtoupper($m[1]);

				if ($hwind == 'NA' && $this->hwinddir != 'NA' && $this->hwindspd > 0) {
					$hwind = strtoupper($this->hwinddir) . ' ' . round($this->hwindspd);
					if ($this->gusts > 0) $hwind .= ' G ' . round($this->gusts);
					$hwind .= ' MPH';
				}

				$hwind = preg_replace('/FROM THE/','',$hwind);
				$hwind = preg_replace('/\bAT\b/','',$hwind);
				$hwind = $this->convert_wind($hwind);

				if (preg_match('/GUSTING/', $hwind)) {
					$hwind = preg_replace('/MPH/','',$hwind);
					$hwind = preg_replace('/GUSTING(?:\s+?TO\s+?)?/','MPH G ',$hwind);
				}
				$hwind = preg_replace('/\s\s+/',' ',$hwind);
				$this->hwind = $hwind;
			}






			#Time for the visibility
			if (preg_match('/<visibility_mi>([^<]+)</', $metar_data,$m)) {
				$visibility = preg_replace('/greater than/', '>', $m[1]);
				$visibility = preg_replace('/\s+/', ' ', $visibility);

				$vis = preg_replace('/[^\d\s\/\.]/','', $visibility);
				$vis = preg_replace('/\.$/', '', trim($vis));
				$vis = eval('return ' . $vis . ';');

				if (preg_match('/meters/i', $visibility))  {
					$this->hvisibility_meters =  $vis;
					$this->hvisibility_miles = $vis/1000*0.621371;
				}
				else {
					$this->hvisibility_meters =  $vis*1609;
					$this->hvisibility_miles = $vis;
				}

				$this->hvisibility_miles = $vis;
				$this->hvisibility = $visibility;
			}






			if (preg_match('/<weather>([^<]+)</',$metar_data,$m)) {
				$twx = strtoupper($m[1]);
				if ($twx == 'NA') $twx='N/A';
				$this->weather =  $this->hsky= $twx;
			}

		     if ($this->hsky == '' || $this->hsky == 'N/A') {

		     	$icon='';
		     	if (preg_match('/<icon_url_name>(\w+)\.\w\w\w</', $metar_data,$m)) $icon = strtoupper($m[1]);
		     	if (isset($this->NOAAIconWx[$icon])) {
		     		$this->weather = $this->hsky = $this->NOAAIconWx[$icon];
		     	}
		     	else {
		     		$this->hsky = $this->default_hsky;
		     	}
		     }


			if (preg_match('/<temp_f>([^<]+)</',$metar_data,$m)) {$this->htempf= $m[1];}
			if (preg_match('/<temp_c>([^<]+)</',$metar_data,$m)) {$this->htempc= $m[1];}

			if (preg_match('/<dewpoint_f>([^<]+)</',$metar_data,$m)) {$this->hdewptf= $m[1];}
			if (preg_match('/<dewpoint_c>([^<]+)</',$metar_data,$m)) {$this->hdewptc= $m[1];}

			if (preg_match('/<relative_humidity>([^<]+)</',$metar_data,$m)) {$this->hrh= $m[1];}
			if (preg_match('/<relative_humidity>([^<]+)</',$metar_data,$m)) {$this->hrh= $m[1];}

			if (preg_match('/<pressure_in>([^<]+)</',$metar_data,$m)) {$this->hpressure= $m[1];}
			if (preg_match('/<pressure_mb>([^<]+)</',$metar_data,$m)) {$this->hpressure_mb= $m[1];}

			if (preg_match('/<heat_index_f>([^<]+)</',$metar_data,$m)) {$this->hhif= $m[1];}
			if (preg_match('/<heat_index_c>([^<]+)</',$metar_data,$m)) {$this->hhic= $m[1];}

			if (preg_match('/<windchill_f>([^<]+)</',$metar_data,$m)) {$this->hwcf= $m[1];}
			if (preg_match('/<windchill_c>([^<]+)</',$metar_data,$m)) {$this->hwcc= $m[1];}


			if (preg_match('/N/', $this->hwcf)) {
				$this->hwcf = $this->htempf;
				$this->hwcc = $this->htempc;
			}

			if (preg_match('/N/', $this->hhif)) {
				$this->hhif = $this->htempf;
				$this->hhic = $this->htempc;
			}





	  		$this->decoded_line = strtoupper($icao).'|'.$this->hsky.'|'.$this->htempf.'|'.$this->hdewptf.'|'.
			      $this->hrh.'|'.$this->hwind.'|'.$this->gusts.'|'.$this->hpressure.'|'.$this->hwcf.'|'.$this->hhif.'|'.
			      $this->hquk.'|'.$this->hqul.'|'.$this->hwinddir.'|'.$this->hwindspd.'|'.$this->hvisibility.'|'.$this->hwcf_old .'|'.
			      $this->pwsurltext.'|'.$this->pwslon.'|'.$this->pwslat.'|'.$this->pwsurl.'|'.$this->pwssoftware_type.'|'.
			      $this->pwsrainin.'|'.$this->hweather.'|'.$this->hsky_conditions.'|'.$this->hvisibility_miles .'|'.
			      $this->hwinddir_deg . '|' . $this->hvisibility_meters . '|' .
			      $this->fueltempf . '|' . $this->fuelmoisture . '|' . $this->batvolt;





		return 1;

	}

	Function process_short_metar($icao, $type, $metar_data) {

		$place = $state = $country ='';
		$found =0;


      	if (preg_match('/<location>([^\n]+), *([^,\n]+)<\/location>/', $metar_data, $m)) {
      		$place = $m[1];
      		$state = $m[2];
      		if ($type) $place .=", $state";
      		$country = $state;
      	}


		if (preg_match('/(\d+\.\d+\.\d+ \d\d\d\d UTC)/', $metar_data, $m)) {
			$this->hforecastdate= $m[1];
		}
		elseif (preg_match('/([a-z]{3}) (\d{2}), (\d{4}) \- ([\d\:]+) ([AP]M) ([a-z]{3})[^\n]*\n/', $metar_data, $m)) {
			$this->hforecastdate = $m[1].' '.$m[2].', '.$m[3].' - '.$m[4].' '.$m[5].' '.$m[6];
		}
		else {
			$this->hforecastdate = 'UNKNOWN';
		}

		if (preg_match('/^LINE: +(.+)$/m', $metar_data, $m)) {
			$line= $m[1]. '|';
			list($icao, $this->hsky,$this->htempf,$this->hdewptf,
				$this->hrh,$this->hwind,$this->gusts,$this->hpressure,$this->hwcf,$this->hhif,
				$this->hquk,$this->hqul,$this->hwinddir,$this->hwindspd,$this->hvisibility,$this->hwcf_old,
				$this->pwsurltext,$this->pwslon,$this->pwslat,$this->pwsurl,$this->pwssoftware_type,
				$this->pwsrainin,$this->hweather,$this->hsky_conditions,$this->hvisibility_miles,
				$this->hwinddir_deg, $this->hvisibility_meters, $this->fueltempf, $this->fuelmoisture,$this->batvolt) =
				explode('|', $line);

			$this->hpressure_mb  = $this->Inches2MB($this->hpressure);
			if (preg_match('/^METAR: (.+)$/m', $metar_data,$m)) {
				$this->metar = $m[1];
			}

			//Celcius conversions of temp
			$this->htempc = $this->f2c($this->htempf);
			$this->hwcc = $this->f2c($this->hwcf);
			$this->hhic = $this->f2c($this->hhif);
			$this->hdewptc = $this->f2c($this->hdewptc);

			return 1;
		}
	}



	Function set($key, $value) {
		$key = trim($key);
		if ($key != '') $this->$key=$value;
	}

	Function wind_chill($temp, $wind, $old = "") {
		if (preg_match("/N\/A/", $wind) || preg_match("/N\/A/", $temp)) {
			return 'N/A';
		}

		if (preg_match("/(\d+)/", $wind, $matches)) {
			$wind = $matches[1];
		}
		else {
			$wind = 0;
		}
		$wc = "";
		$wind = intval($wind);
		if ($wind == 0) {
		   $wc = $temp;
		}
		elseif ($old == "") {
			$wc = 35.74+0.6215 * $temp - 35.75 * pow($wind,0.16) + 0.4275 * $temp * pow($wind, 0.16);
		}
		else {
			$wc=0.0817*(3.71*pow($wind,0.5) + 5.81 - 0.25*$wind)*($temp - 91.4) + 91.4;
		}
		if ($this->do_round) {
			$wc = round($wc);
		}

		if ($wc > $temp) $wc = $temp;
		return $wc;
	}

	Function heat_index($temp, $rh) {
		if ($temp == 'N/A' || $rh == 'N/A') return 'N/A';
		$temp = intval($temp); $rh = intval($rh);

		$hi = -42.379 + 2.04901523*$temp + 10.14333127*$rh - 0.22475541*$temp*$rh - (6.83783E-03)*pow($temp,2) - (5.481717E-02)*pow($rh,2) + (1.22874E-03)*pow($temp,2)*$rh + (8.5282E-04)*$temp*pow($rh,2) - (1.99E-06)*pow($temp,2)*pow($rh,2);

		if ($this->do_round) {
			$hi = round($hi);
		}

		if ($hi < $temp || $temp < 70)  $hi = $temp;
		return $hi;
	}

	Function f2c($tempf) {
		if ($tempf == 'N/A' || $tempf == '') return $tempf;

		$tempc = ($tempf - 32) * (5/9);

		if ($this->do_round) {
			$tempc = round($tempc);
		}

		return $tempc;
	}

	Function Inches2MB($in) {
		if ($in == 'N/A' || $in == '') return $in;
		$mb = floor(($in *33.86389)+.5);
		return $mb;
	}




}
?>
