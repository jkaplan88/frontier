<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: WxCondition.php,v 1.1 2005/08/15 02:29:21 wxfyhw Exp $
*
*/


class WxCondition {

	var $debug = 0;
	var $items = array();

	Function WxCondition(&$parms) {
		$this->items = $parms;
	}

	Function get($name) {
		if ($name == '') { $name = $this->items['type'];}

		if (isset($this->items[$name])) {
			return $this->items[$name];
		}
		else { return '';}
	}

	Function get_settings() {
		$temp = array();

		foreach ($this->items as $key => $value) {
			$temp[$key] = $value;
		}

		return &$temp;
	}

	Function set($parms) {

		if (is_array($parms)) {
			foreach ($parms as $key => $value) {
				$this->items[$key] = $value;
			}
		}
		else {
			$this->items['type'] = $parms;
		}
	}



# rounding is a built in function that can be called
# can be called by:
# $cond->round; - rounds the current condition to zero decimal places
#
# $cond->round(#) - rounds the current condition to # decimal places
# $cond->round(decimal_places => #) - same as above
#
# $cond->round(XX,YY) - rounds XX to YY decimal places
# $cond->round(number => XX,
#			per => YY) - same as above
#
# $cond->round( scientific => ZZ) - will round current condition to
#						      same # of decimal places as ZZ
#
# $cond->round(number => XX,
#			per => ZZ) - will round XX to same number of
#						     decimal places as ZZ

	Function round () {
		$val = $dec = 0;
		if (func_num_args() == 0) {
			$val = $this->get($this->get('type'));
		}
		elseif (func_num_args() == 1) { 
			$arg = func_get_arg(0);
			if (is_array($arg) {
				$val = (isset($arg['number'])) ? $arg['number'] : $val = $this->get($this->get('type'));
				if (isset($arg['decimal_places'])) {$dec = $arg['decimal_places'];}
				elseif (isset($arg['per'])) {
					if (preg_match('/\.(\d+)/', $arg['per'], $m)) {
						$dec = strlen($m[1]);
					}
					else { $dec = 0;}
				}
			}
			else {
				$dec = func_get_arg(0); $val = $this->get($this->get('type'));
			}
		}	
		else  { $val = func_get_arg(0); $dec = func_get_arg(1); }
		
		return sprintf('%.'.$dec.'f',round($val * pow(10,$dec) + $rounder)/pow(10,$dec));
	}




}


?>