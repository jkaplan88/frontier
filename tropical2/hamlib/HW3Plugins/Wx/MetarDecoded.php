<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: MetarDecoded.php,v 1.22 2006/03/09 02:04:47 wxfyhw Exp $
*
*/

Class MetarDecoded {

	var $do_round = "";
	var $decoded_line = "";
	var $hsky = 'N/A';
	var $hsky_conditions = '';
	//var $hweather = 'N/A';
	var $htempf = 'N/A';
	var $htempc = 'N/A';
	var $hdewptf = 'N/A';
	var $hdewptc = 'N/A';
	var $hrh = 'N/A';
	var $hwind = 'N/A';
	var $hwinddir = 'N/A';
	var $hwindspd = 'N/A';
	var $hwinddir_deg = 'N/A';
	var $hwcf = 'N/A';
	var $hwcc = 'N/A';
	var $hhif = 'N/A';
	var $hhic = 'N/A';
	var $hpressure = 'N/A';
	var $hpressure_mb = 'N/A';
	var $hremarks = 'N/A';
	var $hvisibility = 'N/A';
	var $hvisibility_miles = 'N/A';
	var $hvisibility_meters= 'N/A';
	var $hquk = 'N/A';
	var $hqul = 'N/A';
	var $metar = 'N/A';
	var $weather = "";
	var $hweather='';
	var $gusts = "";
	var $hforecastdate='';

	var $fuelmoisture = 'N/A';
	var $fueltempf = 'N/A';
	var $batvolt = 'N/A';


	var $hplace='';
	var $hstate='';
	var $hcountry = '';

	var $default_hsky = 'fair';

	var $hsixhrhif = 'N/A';
	var $hsixhrlof = 'N/A';
	var $h24hrhif = 'N/A';
	var $h24hrlof = 'N/A';
	var $hsixhrhi_c = 'N/A';
	var $hsixhrlo_c = 'N/A';
	var $h24hrhi_c = 'N/A';
	var $h24hrlo_c = 'N/A';
	var $hthrhrprec = 'N/A';
	var $hsixhrprec = 'N/A';
	var $h24hrprec = 'N/A';



	Function process_metar($icao, $type, &$metar_data) {
	      	$found = 0;

	      	//Get the place name
	      	if (preg_match('/<TITLE> *Current Weather Conditions +\- +([^\n]+), *([^,\n]+), ([^,\n]+) *<\/TITLE>/',
	      			$metar_data, $m)) {
	      		$this->hplace = $m[1];
	      		$this->hstate = $m[2];
	      		if ($type) {$place .= ", $state";}
	      		$this->hcountry = $m[3];
	      	}
	      	elseif (preg_match('/<TITLE> *Current Weather Conditions +\- +([^,\n]+),([^,\n]+) *<\/TITLE>/',
	      			$metar_data, $m)) {
	      		$this->hplace = $m[1];
	      		$this->hstate = $m[2];
	      		$this->hcountry = $m[2];
	      	}
	      	elseif (preg_match('/^([^\n]+), *([^,\n]+), *([^,\n]+) *\(\w\w\w\w\)/',
	      			$metar_data, $m)) {
	      		$this->hplace = $m[1];
	      		$this->hstate = $m[2];
	      		if ($type) {$this->hplace .= ", $state";}
	      		$this->hcountry = $m[3];
	      	}
	      	else {
	      		$this->hplace = '';
	      		$this->hstate = '';
	      		$this->hcountry = '';
	      	}

	      	//Get the date
	     if (preg_match("/(\d+\.\d+\.\d+ \d\d\d\d UTC)/", $metar_data, $m)) {
			$this->hforecastdate = $m[1];
		}
		elseif (preg_replace("/([a-z]{3}) (\d{2}), (\d{4}) \- ([\d\:]+) ([AP]M) ([a-z]{3})[^\n]*\n/", $metar_data, $m)) {
		      	$this->hforecastdate = $m[1]." ".$m[2].", ".$m[3]." - ".$m[4]." ".$m[5]." ".$m[6];
		}
		else {
			$this->hforecastdate = "UNKNOWN";
		}

		//Clean out any HTML code
		$metar_data = preg_replace("/<[^>]+>/", "", $metar_data);
//print "metar_data=$metar_data<br>\n";

		//Get the wind data
		if (preg_match("/Wind[\s\r\n .:]+[Vv]ariable at ([\d ]*)MPH[^\n]*\n/",
				$metar_data, $m)) {
			$this->hwind = 'VRB'.($m[1]+0).' MPH';
			$this->hwinddir = 'VRB';
			$this->hwindspd = $m[1]+0;
			$this->hwinddir_deg = 'VRB';
		}
		elseif (preg_match("/Wind[\s\r\n .:]+from the ([A-Za-z]*) \(([\d A-Za-z�]*)\) at ([Cc]alm|[\d.]*) +?MPH[^\n]*\n/",
				$metar_data, $m)) {
			if (strtolower($m[3]) == "calm") {
				$this->hwind = "calm";
			        	$this->hwinddir = 'N';
			        	$this->hwindspd = 0;
			        	$this->hwinddir_deg=0;
		        }
		        else {
		        		$this->hwind = $m[1].' '.($m[3]+0).' MPH';
				$this->hwinddir = $m[1];
				$this->hwindspd = $m[3]+0;
				$wd = trim($m[2]);
				if (preg_match('/^(\d+)/', $wd,$m)) {
					$this->hwinddir_deg = $m[1];
				}
				else { $this->hwinddir_deg = 'N/A';}

		        }
		}
		elseif (preg_match("/Wind[\s\r\n .:]+[Cc]alm[^\n]*\n/",
				$metar_data)) {
			$this->hwind = "calm";
			$this->hwinddir = 'N';
			$this->hwindspd = 0;
			$this->hwinddir_deg =0;
		}

		if (preg_match("/[gG]usting to ([\d.]+) +?MPH/",
				$metar_data, $m)) {
			$this->hwind .= ' G '.($m[1]+0);
      			$this->gusts = $m[1]+0;
		}

		//Get the visibility
		if (preg_match('/Visibility[\s\r\n .:]+([A-Za-z \d\/\(\)]+)[^\n]*\n/',
				$metar_data, $m)) {
			$visibility = preg_replace("/greater than/", ">", $m[1]);
			$visibility = preg_replace('/\s+/', ' ', $visibility);

			$vis = preg_replace('/[^\d\s\/]/', '', $visibility);
			$vis = preg_replace(array('/^\s+/', '/\s+$/', '/\s+/'), array('','','+'), $vis);
			$vis = eval('return ' . $vis . ';');
			//if (preg_match('/meters/i', $visibility)) $vis = $vis/1000*0.621371;
			if (preg_match('/meters/i', $visibility))  {
				$this->hvisibility_meters =  $vis;
				$this->hvisibility_miles = $vis/1000*0.621371;
			}
			else {
				$this->hvisibility_meters =  $vis*1609;
				$this->hvisibility_miles = $vis;
			}

			//$this->hvisibility_miles = $vis;
			$this->hvisibility = $visibility;
		}

		//Get the Wx/sky
		if (preg_match("/^ *?Weather[ :]+([a-zA-Z\d][a-zA-Z \d]*)[^\n\/-]*\n/m",
				$metar_data, $m)) {
			$this->weather = $this->hsky = $this->hweather = $m[1];
		}

		if(preg_match("/Sky [cC]onditions[ .:]+([a-zA-Z \d]+)[^\n]*\n/",
				$metar_data, $m)) {
				$this->hsky_conditions =  $m[1];
				if (!$this->hsky || $this->hsky == "N/A") {$this->hsky = $m[1];}

		}
		if (!$this->hsky || $this->hsky == "N/A") {
			if (preg_match('/\b$icao.+?CAVOK/', $metar_data)) {
				$this->hsky='fair';
			}
			else { $this->hsky = $this->default_hsky;}
		}


		//Get the temp
		if (preg_match("/Temperature[\s.:]+(-?[\d\.]+) F \(-?[\d\.]+[^\n]*\n/",
				$metar_data, $m)) {
			$this->htempf = $m[1];
		}

		//Get the dew point
		if (preg_match('/Dew [Pp]oint[\s.:]+(-?[\d\.]+) F \(-?[\d\.]+[^\n]*\n/',
				$metar_data, $m)) {
			$this->hdewptf = $m[1];
		}

		//Get the humidity
		if (preg_match("/Relative [Hh]umidity[\s.:]+([\d]+)[^\n]*\n/",
				$metar_data, $m)) {
			$this->hrh = $m[1];
		}

		//Get the pressure
		if (preg_match('/Pressure [^\d\n]+[\s.:]+([\d\.]+) in[^\d\n]+([\d]+)[^\n]*\n/',
				$metar_data, $m)) {
			$this->hpressure = $m[1];
			$this->hpressure_mb = $m[2];
		}

		// personal weather stations
		$this->pwsurl = $this->pwsurltext = $this->pwslon = $this->pwslat =
			$this->pwssoftware_type = $this->pwsrainin = '';

		if (preg_match('/^Station Name +(.+)$/m', $metar_data, $m)) {
			$this->pwsurltext = $m[1];
		}
		if (preg_match('/^Longitude, Latitude +([\w.-]+), +([\w.-]+)/m', $metar_data,$m)) {
			$this->pwslon = $m[1]; $this->pwslat = $m[2];
		}
		if (preg_match('/^Station URL(.+)/m', $metar_data,$m)) {
			$vwsurl = preg_replace('/^\s+/', '', $m[1]);
			if (preg_match('/^https?:\/\//i', $vwsurl)) {
				$this->pwsurl=$vwsurl;
			}
			else { $this->pwsurl="http://$vwsurl"; }
		}
		if (preg_match('/Software Type +(.+)/', $metar_data, $m)) {
			$this->pwssoftware_type=$m[1];
		}
		if (preg_match('/Rain +?([\d.]+)/', $metar_data, $m)) {
			$this->pwsrainin = $m[1];
		}
###########################################
#Now the precip amt and pressure
##########################################
		if (preg_match('/Precipitation last hour[\s.:]+([\d\.]+)/', $metar_data, $m)) {
			$this->hprecn = $m[1];
		}
		if ($this->hprecn == 'N/A' && preg_match('/Precipitation last hour[\s.:]+([a-zA-Z \d]+)[^\n]*\n/', $metar_data, $m)) {
			$this->hprecn = $m[1];
		}
		if (preg_match('/Pressure tendency[\s.:]+([\d\.]+) in[^\d\n]+\(([\d\. A-Za-z]*)\)[\s.]([a-zA-Z \d]*)[^\n]*\n/', $metar_data, $m)) {
			$this->hprestrend_in = $m[1];
			$this->hprestrend_mb = $m[2];
			$this->hprestrend = $m[3];
		}
		if ($this->hprestrend == 'N/A' && preg_match('/Pressure tendency[\s.:]+([a-zA-Z \d]+)[^\n]*\n/', $metar_data, $m)) {
			$this->hprestrend = $m[1];
		}

		if (preg_match('/(-?[\d\.]+)\s+\((-?[\d\.]+)\)\s+(-?[\d\.]+)\s+\((-?[\d\.]+)\)\s+In the 6 hours preceding/', $metar_data, $m)) {
			$this->hsixhrhif = $m[1];
			$this->hsixhrhi_c = $m[2];
			$this->hsixhrlof = $m[3];
			$this->hsixhrlo_c = $m[4];
		}
		if (preg_match('/(-?[\d\.]+)\s+\((-?[\d\.]+)\)\s+(-?[\d\.]+)\s+\((-?[\d\.]+)\)\s+In the 24 hours preceding/', $metar_data, $m)) {
			$this->h24hrhif = $m[1];
			$this->h24hrhi_c = $m[2];
			$this->h24hrlof = $m[3];
			$this->h24hrlo_c = $m[4];
		}

		if (preg_match('/([\d\.]+) inches\s+In the 3 hours preceding/', $metar_data, $m)) {
			$this->hthrhrprec = $m[1];
		}
		if (preg_match('/([\d\.]+) inches\s+In the 6 hours preceding/', $metar_data, $m)) {
			$this->hsixhrprec = $m[1];
		}
		if (preg_match('/([\d\.]+) inches\s+In the 24 hours preceding/', $metar_data, $m)) {
			$this->h24hrprec = $m[1];
		}



############################################


		if (preg_match("/Fuel Temperature[\s.:]+(-?[\d\.]+) F/", $metar_data, $matches)) {
			$this->fueltempf = $matches[1];
		}
		if (preg_match("/Fuel Moisture[\s.:]+(-?[\d\.]+)/", $metar_data, $matches)) {
			$this->fuelmoisture = $matches[1];
		}
		if (preg_match("/Battery Voltage[\s.:]+(-?[\d\.]+) F/", $metar_data, $matches)) {
			$this->batvolt = $matches[1];
		}

		if (preg_match("/\bQUK(\d+)/", $metar_data, $matches)) {
			$this->hquk = $matches[1];
		}
		if (preg_match("/\bQUL(\d+)/", $metar_data, $matches)) {
			$this->hqul = $matches[1];
		}
		if (preg_match("/\b($icao [\d\w\/ \-\+]+)/", $metar_data, $matches)) {
			$this->metar = $matches[1];
		}



		$this->hwcf_old = $this->hwcf;
		$this->hwcf = $this->wind_chill($this->htempf, $this->hwind);
		$this->hhif = $this->heat_index($this->htempf, $this->hrh);

  		$this->decoded_line = strtoupper($icao).'|'.$this->hsky.'|'.$this->htempf.'|'.$this->hdewptf.'|'.
		      $this->hrh.'|'.$this->hwind.'|'.$this->gusts.'|'.$this->hpressure.'|'.$this->hwcf.'|'.$this->hhif.'|'.
		      $this->hquk.'|'.$this->hqul.'|'.$this->hwinddir.'|'.$this->hwindspd.'|'.$this->hvisibility.'|'.$this->hwcf_old.'|'.
		      $this->pwsurltext.'|'.$this->pwslon.'|'.$this->pwslat.'|'.$this->pwsurl.'|'.$this->pwssoftware_type.'|'.
		      $this->pwsrainin.'|'.$this->hweather.'|'.$this->hsky_conditions.'|'.$this->hvisibility_miles .'|'.
		      $this->hwinddir_deg . '|' . $this->hvisibility_meters . '|' .
		      $this->fueltempf . '|' . $this->fuelmoisture . '|' . $this->batvolt . '|' . $this->hprecn. '|' . $this->hprecw. '|' . $this->hprestrend_in. '|' . $this->hprestrend_mb. '|' . $this->hprestrend. '|' . $this->hsixhrhif. '|' . $this->hsixhrlof. '|' . $self ->{h24hrhif}. '|' . $self ->{h24hrlof}. '|' . $this->hsixhrhi_c. '|' . $this->hsixhrlo_c. '|' . $self ->{h24hrhi_c}. '|' . $self ->{h24hrlo_c}. '|' .$this->hthrhrprec. '|' .$this->hsixhrprec. '|' . $this->h24hrprec;


		//Celcius conversions of temp
		$this->htempc = $this->f2c($this->htempf);
		$this->hwcc = $this->f2c($this->hwcf);
		$this->hhic = $this->f2c($this->hhif);
		$this->hdewptc = $this->f2c($this->hdewptc);

		return 1;

	}

	Function process_short_metar($icao, $type, $metar_data) {

		$place = $state = $country ='';
		$found =0;

		if (preg_match('/PLACE: ([^\n]+), *([^\n]+), *([^,\n]+), ([^,\n]+)?/', $metar_data, $m)) {
			$place = $m[2]; $state = $m[3];
			if ($type) $place .= ", $state";
			if (isset($m[4])) { $country = ($m[4]) ? $m[4] : $state; }
			  else {$country = $state; }
			if (strtolower($country) == 'united states') $country = 'us';
			$this->hplace=$place;
			$this->hstate = $state;
			$this->hcountry = $country;
		}
		else {
			return 0;
		}

		if (preg_match('/(\d+\.\d+\.\d+ \d\d\d\d UTC)/', $metar_data, $m)) {
			$this->hforecastdate= $m[1];
		}
		elseif (preg_match('/([a-z]{3}) (\d{2}), (\d{4}) \- ([\d\:]+) ([AP]M) ([a-z]{3})[^\n]*\n/', $metar_data, $m)) {
			$this->hforecastdate = $m[1].' '.$m[2].', '.$m[3].' - '.$m[4].' '.$m[5].' '.$m[6];
		}
		else {
			$this->hforecastdate = 'UNKNOWN';
		}

		if (preg_match('/^LINE: +(.+)$/m', $metar_data, $m)) {
			$line= $m[1]. '|';
			@list($icao, $this->hsky,$this->htempf,$this->hdewptf,
				$this->hrh,$this->hwind,$this->gusts,$this->hpressure,$this->hwcf,$this->hhif,
				$this->hquk,$this->hqul,$this->hwinddir,$this->hwindspd,$this->hvisibility,$this->hwcf_old,
				$this->pwsurltext,$this->pwslon,$this->pwslat,$this->pwsurl,$this->pwssoftware_type,
				$this->pwsrainin,$this->hweather,$this->hsky_conditions,$this->hvisibility_miles,
				$this->hwinddir_deg, $this->hvisibility_meters, $this->fueltempf, $this->fuelmoisture,$this->batvolt,$this->hprecn, $this->hprecw, $this->hprestrend_in, $this->hprestrend_mb, $this->hprestrend, $this->hsixhrhif, $this->hsixhrlof,  $this->h24hrhif, $this->h24hrlof, $this->hsixhrhi_c, $this->hsixhrlo_c,  $this->h24hrhi_c, $this->h24hrlo_c, $this->hthrhrprec, $this->hsixhrprec, $this->h24hrprec) =
				explode('|', $line);

			$this->hpressure_mb  = $this->Inches2MB($this->hpressure);
			if (preg_match('/^METAR: (.+)$/m', $metar_data,$m)) {
				$this->metar = $m[1];
			}

			//Celcius conversions of temp
			$this->htempc = $this->f2c($this->htempf);
			$this->hwcc = $this->f2c($this->hwcf);
			$this->hhic = $this->f2c($this->hhif);
			$this->hdewptc = $this->f2c($this->hdewptc);

			return 1;
		}
	}



	Function set($key, $value) {
		$key = trim($key);
		if ($key != '') $this->$key=$value;
	}

	Function wind_chill($temp, $wind, $old = "") {
		if (preg_match("/N\/A/", $wind) || preg_match("/N\/A/", $temp)) {
			return 'N/A';
		}

		if (preg_match("/(\d+)/", $wind, $matches)) {
			$wind = $matches[1];
		}
		else {
			$wind = 0;
		}
		$wc = "";
		$wind = intval($wind);
		if ($wind == 0) {
		   $wc = $temp;
		}
		elseif ($old == "") {
			$wc = 35.74+0.6215 * $temp - 35.75 * pow($wind,0.16) + 0.4275 * $temp * pow($wind, 0.16);
		}
		else {
			$wc=0.0817*(3.71*pow($wind,0.5) + 5.81 - 0.25*$wind)*($temp - 91.4) + 91.4;
		}
		if ($this->do_round) {
			$wc = round($wc);
		}

		if ($wc > $temp) $wc = $temp;
		return $wc;
	}

	Function heat_index($temp, $rh) {
		if ($temp == 'N/A' || $rh == 'N/A') return 'N/A';
		$temp = intval($temp); $rh = intval($rh);

		$hi = -42.379 + 2.04901523*$temp + 10.14333127*$rh - 0.22475541*$temp*$rh - (6.83783E-03)*pow($temp,2) - (5.481717E-02)*pow($rh,2) + (1.22874E-03)*pow($temp,2)*$rh + (8.5282E-04)*$temp*pow($rh,2) - (1.99E-06)*pow($temp,2)*pow($rh,2);

		if ($this->do_round) {
			$hi = round($hi);
		}

		if ($hi < $temp || $temp < 70)  $hi = $temp;
		return $hi;
	}

	Function f2c($tempf) {
		if ($tempf == 'N/A' || $tempf == '') return $tempf;

		$tempc = ($tempf - 32) * (5/9);

		if ($this->do_round) {
			$tempc = round($tempc);
		}

		return $tempc;
	}

	Function Inches2MB($in) {
		if ($in == 'N/A' || $in == '') return $in;
		$mb = floor(($in *33.86389)+.5);
		return $mb;
	}




}
?>
