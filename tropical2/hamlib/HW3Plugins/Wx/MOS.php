<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: MOS.php,v 1.7 2004/06/09 13:06:40 lee Exp $
*
*/


class MOS{

	var $rows = array();
	var $hours = array();
	var $mos_date = '';
	var $mos_time = '';
	var $mos_total = '';
	var $actual = 1;

	var $mos_time_24h = '';
	var $mos_year='';
	var $mos_mon='';
	var $mos_day = '';

	var $debug =0;

	Function MOS($debug) {
		$this->debug=$debug;
	}


	Function actual($mode = -1) {
		if ($mode == -1 ) {
			return $this->actual;
		}
		else {
			$this->actual = $mode;
		}
	}


Function parse_mos($station, &$data) {
	$this->rows = array();
	$this->hours = array();

	$station = preg_replace('/^K(\w\w\w)/', 'K?\1', $station);

	if (!preg_match('/'.$station.'[^\n\d]+(\d\d?)\/(\d\d)\/(\d\d\d?\d?)\s+(\d\d)(\d\d)\s+UTC\s*/',
			$data,$matches)) {
		$this->mos_date = '';
		$this->mos_time = '';
		$this->mos_total = 0;
		return 0;
	}


	$year = ($matches[3] < 100) ? $matches[3]+2000 : $matches[3];
	$mon = $matches[1]+0;
	$day = $matches[2];
	$this->mos_date = sprintf('%04u%02u%02u', $year, $mon, $day);
	$this->mos_time = $matches[4].':'.$matches[5];
	$this->mos_time_24h = $matches[4].$matches[5];
	$this->mos_year = $year;
	$this->mos_mon= $mon;
	$this->mos_day = $day;

	if (preg_match('/\bH(?:OU)?R  (.+)/', $data, $m)) {
		$this->rows['HR'] = $m[1];
	} else { $this->rows['HR'] = ''; }

	if (preg_match('/M?N\/M?X (.+)/', $data, $m)) {
		$this->rows['N_X'] = $m[1];
	} else { $this->rows['N_X'] = ''; }

	if (preg_match('/TE?MP  ?( .+)/', $data, $m)) {
		$this->rows['TMP'] = $m[1];
	} else { $this->rows['TMP'] = ''; }

	if (preg_match('/D(?:EW)?PT (.+)/', $data, $m)) {
		$this->rows['DPT'] = $m[1];
	} else { $this->rows['DPT'] = ''; }

	if (preg_match('/CLDS?  ?( .+)/', $data, $m)) {
		$this->rows['CLD'] = $m[1];
	} else { $this->rows['CLD'] = ''; }

	if (preg_match('/WDI?R  ?( .+)/', $data, $m)) {
		$this->rows['WDR'] = $m[1];
	} else { $this->rows['WDR'] = ''; }

	if (preg_match('/WSPD?  ?( .+)/', $data, $m)) {
		$this->rows['WSP'] = $m[1];
	} else { $this->rows['WSP'] = ''; }

	if (preg_match('/P(?:OP)?06 (.+)/', $data, $m)) {
		$this->rows['P06'] = $m[1];
	} else { $this->rows['P06'] = ''; }

	if (preg_match('/P(?:OP)?12 (.+)/', $data, $m)) {
		$this->rows['P12'] = $m[1];
	} else { $this->rows['P12'] = ''; }

	if (preg_match('/Q06 (.+)/', $data, $m)) {
		$this->rows['Q06'] = $m[1];
	} else { $this->rows['Q06'] = ''; }

	if (preg_match('/Q12 (.+)/', $data, $m)) {
		$this->rows['Q12'] = $m[1];
	} else { $this->rows['Q12'] = ''; }


	if (preg_match('/QPF (?:  )?( .+)/', $data, $m)) {
		$this->rows['QPF'] = $m[1];
	} else { $this->rows['QPF'] = ''; }

	if (preg_match('/T(?:SV)?06 (.+)/', $data, $m)) {
		$this->rows['T06'] = $m[1];
	} else { $this->rows['T06'] = ''; }

	if (preg_match('/T(?:SV)?12 (.+)/', $data, $m)) {
		$this->rows['T12'] = $m[1];
	} else { $this->rows['T12'] = ''; }

	if (preg_match('/POZP?  ?( .+)/', $data, $m)) {
		$this->rows['POZ'] = $m[1];
	} else { $this->rows['POZ'] = ''; }

	if (preg_match('/POSN?  ?( .+)/', $data, $m)) {
		$this->rows['POS'] = $m[1];
	} else { $this->rows['POS'] = ''; }

	if (preg_match('/P?TYPE? (.+)/', $data, $m)) {
		$this->rows['TYP'] = $m[1];
	} else { $this->rows['TYP'] = ''; }

	if (preg_match('/CIG (?:  )?( .+)/', $data, $m)) {
		$this->rows['CIG'] = $m[1];
	} else { $this->rows['CIG'] = ''; }

	if (preg_match('/VIS (?:  )?( .+)/', $data, $m)) {
		$this->rows['VIS'] = $m[1];
	} else { $this->rows['VIS'] = ''; }

	if (preg_match('/OBV(?:IS)? (.+)/', $data, $m)) {
		$this->rows['OBV'] = $m[1];
	} else { $this->rows['OBV'] = ''; }

	if (preg_match('/SNO?W  ?( .+)/', $data, $m)) {
		$this->rows['SNW'] = $m[1];
	} else { $this->rows['SNW'] = ''; }

	//lets cycle through each hour and then cycle through the rows to save the appropriate data

	$last_hour = 0;
	$total = 0;

	if (preg_match_all('/\s(\d+)/', $this->rows['HR'], $m)) {
		for ($i = 0; $i < sizeof($m[0]); $i++) {
			$hour = $m[1][$i];
			if ($hour < $last_hour) {
				$day++;
				if ( ($day > 30 && preg_match('/^4|6|9|11$/', $mon)) || ($mon == 2 && (($day >28  && $year % 4 != 0) || ($day >29  && $year % 4 == 0)))) {
					$day = 1; $mon++;
					if ($mon > 12) { $mon = 1; $year++; }
				}
			}
			$last_hour = $hour;
			$this->hours[sprintf('%04u%02u%02u%02u',$year, $mon,$day,$hour)] = $total;
			$total++;
		}
	}

	$this->mos_total = $total;

	return $total;
	}


	Function get_item($num, $item) {
		if (isset($this->rows[$item])) {
			$result = substr($this->rows[$item], ($num+0)*3, 3);
			if ($this->actual && $num < ($this->mos_total -1) && !(strpos('N_X P06 P12 T06 T12 SNW Q06 Q12', $item, 0) ===false)) {
				$result = preg_replace('/\s+/', '', $result);
				if ($result == '') $result  = $this->get_item($num+1, $item);
			}
		}

		return $result;
	}

	Function get_items($num, $items) {
		$result  = array();
		$i =0;
		if (is_array($items)) {
			reset($items);
			foreach ($items as $value) {
				$result[$i] = $this->get_item($num, $item);
				$i++;
			}
		}
		else {
			$result[0] = $this->get_item($num, $item);
		}
	}


	Function get_items_by_hour($hour, &$items) {
		if (isset($this->hours[$hour])) {
			$use_hour= $hour;
		} else {$use_hour = ''; }

		$result =& $this->get_items($use_hour, $items);

		return $result;
	}


	Function get_items_next_hour ($day, $items=0) {
		if (strlen($day) < 3) {
			$today = sprintf('%04u%02u%02u', gmdate('Y'), gmdate('m'), gmdate('d'));
			$day += $today;
			$day .= sprintf('%02u', gmdate('H'));
		}

		$found_hour = 0;
		$hours = $this->hours;

		ksort($hours);
		reset($hours);
		while (list($hour,) = each ($hours)) {
			if ($hour+0 >= $day) { $found_hour = $hour; break;}
		}

   		if (is_array($items)) {
			if (isset($this->hours[$found_hour])) {
				$use_hour= $found_hour;
			} else {$use_hour = ''; }

			$result =& $this->get_items($use_hour, $items);
		}else {$result = $found_hour; }

		return $result;
	}

	Function get_items_prev_hour ($day, &$items) {
		if (strlen($day) < 3) {
			$today = sprintf('%04u%02u%02u', gmdate('Y'), gmdate('m'), gmdate('d'));
			$day += $today;
			$day .= sprintf('%02u', gmdate('H'));
		}

		$found_hour = 0;
		$hours = $this->hours;

		ksort($hours);
		reset($hours);
		while (list($hour,) = each ($hours)) {
			if ($hour+0 > $day) { break; }
			else {$found_hour = $hour; }
		}

   		if (is_array($items)) {
			if (isset($this->hours[$found_hour])) {
				$use_hour= $found_hour;
			} else {$use_hour = ''; }

			$result =& $this->get_items($use_hour, $items);
		}else {$result = $found_hour; }

		return $result;
	}


	Function total_hours () {
		return $this->mos_total;
	}

	Function get_id_for_hour($hour) {
		if (isset($this->hours[$hour])) {
			return $this->hours[$hour];
		}
		else {
			return '';
		}
	}

	Function get_datehour_for_id ($id) {
		$hours =& $this->hours;

		$result = '';
		reset($hours);
		while (list ($hour,$value) = each($hours)) {
			if ($value == $id) { $result = $hour; break;}
		}

		return $result;
	}



}

?>