<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchMetar.php,v 1.15 2006/05/07 17:55:14 wxfyhw Exp $
*
*/

require_once("hamlib/HW3Plugins/FetchWxData.php");

class FetchMetar {

	var $MetarDecoded = "";
	var $set = "false";
	var $debug = false;

	var $core_path = '';

	Function FetchMetar($core_path) {
		$this->core_path = $core_path;
	}

      Function Debug ($mode) {
         $this->debug = $mode;
      }


	Function metar(&$var, &$form, $forecast, &$cfg, $forecast_info_array, $debug) {
		$return_hash = array();
		$wx_info = "";
		$cache_wx_info = "";
		$find_place = "";
		$this->debug = $debug;
		$data = array();

		$country = strtolower($var["country"]);
		if (!$country) $country = 'us';


		$icao_arry = explode("%", $var["alt_cc_info"]);
		//Loop thru the icaos
		$continue_processing = true;
		while(list($key, $value) = each($icao_arry)) {
			if ($continue_processing) {
				$temp_arry = explode(":", $value);
				$find_place = $temp_arry[0];
				$code = $temp_arry[1];

				$url = preg_replace("/%%(\w+)%%/e", '$$1', (($country== 'us') ? $cfg->Val('URLs', 'metar_url') : $cfg->Val('URLs', 'metar_url_int')) );
				$prefix = preg_replace("/%%(\w+)%%/e", '$$1', (($country== 'us') ? $cfg->Val('URLs', 'metar_prefix') : $cfg->Val('URLs', 'metar_prefix_int')));
				$postfix = preg_replace("/%%(\w+)%%/e", '$$1', (($country== 'us') ? $cfg->Val('URLs', 'metar_postfix') : $cfg->Val('URLs', 'metar_postfix_int')) );
				$cache_file_1 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'metar_cache_file'));
				$cache_file_2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'metar_cache_file2'));

				$data = &fetch_forecast($var["expiration"], $cache_file_1, $cache_file_2,
					$url, $prefix, $postfix, $debug);
			      $which_used = $data[0];
			      $wx_info = $data[1];
			      if (preg_match('/Multiple Choices/', $wx_data) || preg_match('/-hwError/', $wx_data)) { $wx_data = '';}

				if ($wx_info != "")
					$continue_processing = false;
			}
		}


		if ($wx_info != "") {

			if (preg_match('/<current_observation/', $wx_data)) {
				require_once("hamlib/HW3Plugins/Wx/Currents/NOAAXML.php");
				$this->MetarDecoded = new CurrentsNOAAXML();
				$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
			}
			elseif (preg_match('/adds\.aviationweather/', $wx_data)) {
				require_once("hamlib/HW3Plugins/Wx/Currents/NOAAADDS.php");
				$this->MetarDecoded = new CurrentsNOAAADDS();
				$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
			}
			else {
				require_once("hamlib/HW3Plugins/Wx/MetarDecoded.php");
				$this->MetarDecoded = new MetarDecoded();
				$this->MetarDecoded->set('default_hsky', $cfg->val('Defaults', 'default_hsky'));
			}

			$this->MetarDecoded->process_metar($find_place, getKeyVal($form, "type", ""), $wx_info);
			$this->set = "true";

			//Write the cache file
			//$cache_filename = "metar_".$find_place."_cache.txt";
			//f (file_exists($cache_filename)) {unlink($cache_filename);}

			//$cache_fp = fopen($cache_filename, "w");
			//fwrite($cache_fp, $this->MetarDecoded->decoded_line);
		}

		//if ($cache_wx_info != "") {
		//	echo $cache_wx_info;
		//}

		return $return_hash;
	}


Function GetValue($key, $index) {
		if ($this->MetarDecoded != "") {
			$old_error_reporting = error_reporting(0);
			if ($index <> "") {
				$tmp = $this->MetarDecoded->$key;
				$val = $tmp[$index];
			}
			else {
				$val = $this->MetarDecoded->$key;
			}
			error_reporting($old_error_reporting);
			return $val;
		}
		else return "";
	}
}
?>