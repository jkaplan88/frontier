<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2006 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HWImageMap.php,v 1.2 2007/09/21 00:47:38 wxfyhw Exp $
*
*/


class HWImageMap {

	var $debug=0;

	var $areas = array();
	var $name = '';

	var $xoffset=0;
	var $yoffset = 0;

	var $scale = 1;


	Function HWImageMap ($name = '', $debug = 0) {
		$name = preg_replace('/\W/', '', $name);
		$this->name = $name;

		$this->debug = intval($debug);
	}

	function totalAreas() {
		return sizeof($this->areas);
	}

	function addRect($url, $x1,$y1,$x2,$y2, $alt='', $other='') {
		array_push($this->areas, array('rect', $url,$alt, $other, $x1,$y1,$x2,$y2));
		return sizeof($this->areas) -1;
	}
	function addCircle($url, $x,$y,$r, $alt='', $other='') {
		array_push($this->areas, array('circle',  $url, $alt,$other, $x,$y,$r));
		return sizeof($this->areas) -1;
	}
	function addPoly($url, $points, $alt='', $other='') {
		if (!is_array($points)) $points = array();
		array_push($this->areas, array('poly', $url, $alt,$other, $points));
		return sizeof($this->areas) -1;
	}
	function deleteArea($index) {
		array_splice($this->areas, $index,1);
	}
	function getAreaType($index) {
		if (isset($this->areas[$index])) return $this->areas[$index][0];
		else	return '';
	}
	function getAreaURL($index) {
		if (isset($this->areas[$index])) return $this->areas[$index][1];
		else	return '';
	}
	function getAreaAlt($index) {
		if (isset($this->areas[$index])) return $this->areas[$index][2];
		else	return '';
	}

	function getArea($index) {
		if (isset($this->areas[$index])) return $this->areas[$index];
		else	return '';
	}



	function offset($x=0,$y=0) {
		$this->xoffset = $x;
		$this->yoffset = $y;
	}
	function xoffset($x) { $this->xoffset = $x;}
	function yoffset($y) { $this->yoffset = $y;}

	function out($filename='', $name='') {
		if (empty($this->areas)) return true;

		$name = preg_replace('/\W/', '', $name);
		if (!$name) $name = $this->name;
		if (!$name) $name = 'hwimap';

		$file = ($filename) ? fopen($filename, "w") : NULL;

		if ($filename && !$file) return false;

		if ($file) fputs($file, "<map name=\"$name\">\n");
		else print "<map name=\"$name\">\n";

		foreach ($this->areas as $area) {

			$type = array_shift($area);
			$url = array_shift($area);
			$alt = array_shift($area);
			$other = array_shift($area);

			$points = '';
			if ($type == 'rect') {
				list ($x1,$y1, $x2,$y2) = $area;
				$x1 = $this->_scale($x1) - $this->xoffset;
				$x2 = $this->_scale($x2) - $this->xoffset;
				$y1 = $this->_scale($y1) - $this->yoffset;
				$y2 = $this->_scale($y2) - $this->yoffset;
				$points = "$x1,$y1,$x2,$y2";
			}
			elseif ( $type == 'circle') {
				list ($x,$y,$r) = $area;
				$x = $this->_scale($x) - $this->xoffset;
				$y = $this->_scale($y) - $this->yoffset;
				$r = $this->_scale($r);
				$points = "$x,$y,$r";
			}
			elseif ($type == 'poly') {
				for ($i=0; $i <sizeof($area[0]); $i+=2) {
					$x = $area[0][$i];
					$y = $area[0][$i+1];
					$x = $this->_scale($x) - $this->xoffset;
					$y = $this->_scale($y) - $this->yoffset;
					$points .= "$x,$y,";
				}
				$points = preg_replace('/,$/', '',$points);

			}

			$line = "<area shape=\"$type\" coords=\"$points\" href=\"$url\" ";
			if ($alt) $line .= " alt=\"$alt\" ";
			$line .= " $other />\n";

			if ($file) fputs($file, $line);
			else print $line;
		}

		if ($file) fputs($file, "</map>\n");
		else print "</map>\n";

		if ($file) fclose($file);

		return true;
	}



	function _scale($val) {

		return round($val * $this->scale);
	}






} // end class

?>
