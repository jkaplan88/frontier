<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HWmysql.php,v 1.15 2005/12/31 15:53:26 wxfyhw Exp $
*
*/

class HWmysql {
	var $db_path = "";
	var $country = "us";

	var $cfg;
	var $debug=0;


	var $dbusername = '';
	var $dbpw='';
	var $dbhostname ='';
	var $dbdatabase ='';

	var $use_persistent=1;
	var $keep_open = 1;

	var $allow_fuzzy = 1;
	var $fuzzy_min_chars = 4;



	var $dbh= NULL;



	Function HWmysql(&$cfg, $path, $debug) {
		$this->cfg =& $cfg;
		$this->db_path = $path;
		$this->debug=$debug;

		$this->dbusername = $cfg->val('Database Access', 'username'); # Username.
		$this->dbpw = $cfg->val('Database Access', 'password'); # Password.
		$this->dbhostname = $cfg->val('Database Access', 'hostname'); # Hostname.
		$this->dbdatabase =$cfg->val('Database Access', 'database_name'); # Database name.

		$this->use_persistent =$cfg->val('Database Access', 'use_persistent');
		$this->keepopen = $cfg->val('Database Access', 'keepopen');

		$this->allow_fuzzy = $cfg->val('Database Access', 'allow_fuzzy_search');

		$i =  $cfg->val('Database Access', 'fuzzy_min_chars');
		$i = floor($i); if ($i>0) $this->fuzzy_min_chars = $i;

		$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);

	}

	Function SetCountry($new_country) {
	   	$new_country=trim($new_country);
		if (!$new_country) { $new_country = 'us';}
		$this->country = strtolower($new_country);
	}


	Function db_establish($database, $dbhostname, $dbusername, $dbpassword) {
		if ($this->use_persistent) {
			$dbh = mysql_pconnect($dbhostname, $dbusername, $dbpassword) or die(mysql_error());
		}
		else {
			$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword) or die(mysql_error());
		}
		mysql_select_db($database,$dbh) or die(mysql_error());

		return $dbh;
	}

	Function check_for_alt_place($state, $place) {
		$debug = $this->debug;
		$found =0;

		$place = preg_replace("/'/", '', strtolower($place));
		$state = preg_replace("/'/", '', strtolower($state));
		$country = preg_replace("/\W/", '', strtolower($this->country));
		if (!$country) $country = 'us';

		$found = 0; $error = ''; $ret_args = array();

		$sql = "SELECT * FROM places WHERE (name='$place' or name='$place county') AND state='$state' AND country='$country' ORDER BY name;";
		if ($debug)    print "sql=$sql<br>\n";

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		$result = mysql_query($sql, $dbh);
		if (!$result) { $error = mysql_error($dbh); }
		else {
			if (list($places_id, $place,$state,$fips, $country, $cwa, $tz, $tzname, $lat, $lon, $elev, $radar_icao) = mysql_fetch_row($result)) {
				$found =1;
			}
			else {$found = 0;}

			if ($found) {
				if (!$places_id) { $error = mysql_error($dbh); }
				else {
					$found = 1;
					$alt_cc = ''; $alt_zone='';$zone='';
					$sql = "SELECT wxzone, code FROM alt_zone_info WHERE places_id=$places_id " .
						 " ORDER BY precedence;";
					if ($debug)    print "sql=$sql<br>\n";

					$result = mysql_query($sql, $dbh);
					if (!$result) { $error = mysql_error($dbh); }
					else {
						while (list($wxzone, $code) = mysql_fetch_row($result)) {
							$wxzone = trim($wxzone);
							if ($alt_zone) $alt_zone .='%';
							$alt_zone .= "$wxzone:$code";
							if (!$zone) $zone = $wxzone;

						}

						$sql = "SELECT icao, code FROM alt_cc_info WHERE places_id=$places_id " .
							" ORDER BY precedence;";
						if ($debug) print "sql=$sql<br>\n";
						$result = mysql_query($sql, $dbh);
						if (!$result) { $error = mysql_error($dbh); }
						else {
							while (list($icao, $code) = mysql_fetch_row($result)) {
								$icao = preg_replace('/\s+/', '', $icao);
								if ($alt_cc) $alt_cc .='%';
								$alt_cc .= "$icao:$code";
							}
						}
						$iscounty = preg_match('/\bcounty\b/i', $place) ? 1 : 0;
						$ret_args = array($error,$alt_zone,$alt_cc,$zone,$fips,$cwa,'',$lat,$lon,$elev, $tzname,$tz,$radar_icao, $iscounty);
					}
				}
			}
		}
		$this->CloseConn();

		if ($error != '') {return $error;}
		else {if ($found) {return $ret_args;} else {return '';}}
	}


	Function check_for_city($pands, $mode=0) {
		$debug = $this->debug;

		$cities=array();
		$error = '';
		$total=0;

		$pands = preg_replace("/'/", '', strtolower($pands));
		$country = preg_replace("/\W/", '', strtolower($this->country));
		if (!$country) $country = 'us';

		if ($debug) print "in Check for city: $pands, $country<br>\n";

		$found = 0; $error = ''; $results= array();

		if (!$mode) {
			$sql = "SELECT distinct name, state, country FROM places WHERE name='$pands' or name='$pands county';";
		}
		else {
			$sql = "SELECT distinct name, state, country FROM places WHERE name LIKE '$pands%';";



//print "sql=$sql<br>\n";exit;
		}

		if ($debug)    print "sql=$sql<br>\n";

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		if ($debug) print "sql=$sql<br>\n";
		$result = mysql_query($sql, $dbh);
		if (!$result) { $error = mysql_error($dbh); }
		else {
			for ($total = 1; $row = mysql_fetch_row ($result); ++$total) {
				if ($debug) print "...Found " . $row[0] . ', ' . $row[1] . ', ' . $row[2] . "<br>\n";
				array_push($cities, $row[0], $row[1], $row[2]);
			}
			if (--$total < 0 ) $total =0;
			$found = $total;
		}
		$this->CloseConn();

		if ($error != '') { return array(0,$error); }
		else {
			if (!$mode && !$found && $this->allow_fuzzy && strlen($pands) >= $this->fuzzy_min_chars) {
				return $this->check_for_city($pands, 1);
			}
			else {
				return array($total, $cities);
			}
		}

	}

	Function get_airport_data($airport) {
		$debug = $this->debug;

		$airport = preg_replace('/\W/', '', strtolower($airport));
		$result = array();
		$error = '';


		$sql =  "SELECT name,place, state, country, lat, lon, elevation, icao,wxzone,fips,radaricao, tz,tzname FROM airports " .
			  " WHERE airportcode='$airport';";
		if ($debug)    print "sql=$sql<br>\n";

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		$result = mysql_query($sql, $dbh);
		if (!$result) { $error = mysql_error($dbh); }
		else {
			if (!($result =mysql_fetch_row($result))) {
				$error = "ICAO infor for $airport not found";
			}
		}
		$this->CloseConn();

		if ($error) { return $error; }
		else { return $result; }
	}

	Function get_icao_data($icao) {
		$debug = $this->debug;

		$icao = str_replace("'", '', strtolower($icao));
		$result = array();
		$error = '';


		$sql =  "SELECT icao, name, state, '', country, tz, lat, lon, elevation, tzname, wxzone,fips,radaricao FROM icaos " .
			  " WHERE icao='$icao';";
		if ($debug)    print "sql=$sql<br>\n";

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		$result = mysql_query($sql, $dbh);
		if (!$result) { $error = mysql_error($dbh); }
		else {
			if (!($result =mysql_fetch_row($result))) {
				$error = "ICAO infor for $icao not found";
			}
		}
		$this->CloseConn();

		if ($error) { return $error; }
		else { return $result; }
	}


	Function get_place_from_zone($zone, $state) {
		$debug = $this->debug;

		$zone = preg_replace("/\W/", '', strtolower(str_replace("'", '', $zone)));
		$state = preg_replace("/\W/", '', strtolower(str_replace("'", '', $state)));
		$country = preg_replace("/\W/", '', strtolower($this->country));
		if (!$country) $country = 'us';

		$result = array();
		$error = '';

		if (preg_match('/^(?:([A-Z][A-Z])(Z?))?(\d+)$/', $zone, $m)) {
			$tstate = $m[1]; $tzone = $m[3];
			if (!$tstate) {
				if ($state) {
					$zone = strtoupper($state) . 'Z' . sprintf('%03u', $tzone);
					$tstate  = strtoupper($state);
				}
				else { $zone='';}
			}
			elseif (!$m[2]) {
				$zone = $tstate . 'Z' . sprintf('%03u', $tzone);
			}
		}

		if ($zone) {

			if (!$this->dbh) {
				if ($debug) print "DB connection not open.. opening <br>\n";
				$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
			}
			$dbh = $this->dbh;

			$sql =   "SELECT wxzone, name, state, lon, lat, tz FROM zones WHERE wxzone='$zone';";
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				if (!($result =mysql_fetch_row($result))) {
					$result = array($zone, "Weather zone $zone", $state, '','',0);
				}
			}
			$this->CloseConn();

			if ($error) { return $error; }
			else { return $result; }
		}
		else { return "Invalid Zone provided"; }
	}

	Function get_place_from_fips($fips) {
		$debug = $this->debug;

		$fips = preg_replace("/\D/", '', $fips);
		$country = preg_replace("/\W/", '', strtolower($this->country));
		if (!$country) $country = 'us';

		$result = array();
		$name=$lon=$lat=$wxzone=$state=$tz='';
		$error = '';

		if ($fips) {

			if (!$this->dbh) {
				if ($debug) print "DB connection not open.. opening <br>\n";
				$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
			}
			$dbh = $this->dbh;

			$sql = "SELECT  name,lon, lat,state, tz  FROM counties WHERE fips='$fips' AND country='$country';";
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				if (!(list($name,$lon,$lat,$state,$tz) =mysql_fetch_row($result))) {
					$error = "No info on Fips '$fips'";
				}
				else {
					$sql = "SELECT wxzone FROM countywxzones where fips='$fips';";
					if ($debug)    print "sql=$sql<br>\n";
					$result = mysql_query($sql, $dbh);
					if (!$result) { $error = mysql_error($dbh); }
					else {
						if (!(list($wxzone) =mysql_fetch_row($result))){
							$wxzone='';
						}
						else {
							if (preg_match('/^(\w\w)(\d+)$/', $wxzone,$m)) {
								$wxzone = strtoupper($m[1]) . 'Z'. sprintf('%03u', $m[2]);
							}
						}
					}
				}

			}
			$this->CloseConn();

			if ($error) { return $error; }
			else { return array($fips,$name,$lon,$lat,$wxzone,$state,$tz); }
		}
		else { return "Invalid Fips provided"; }
	}

	Function get_zipcode_info($zip) {
		$debug = $this->debug;

		$zip = str_replace("'", '', $zip);
		$country = preg_replace("/\W/", '', strtolower($this->country));
		if (!$country) $country = 'us';

		$result = array();
		$error = '';

		if ($zip) {

			if (!$this->dbh) {
				if ($debug) print "DB connection not open.. opening <br>\n";
				$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
			}
			$dbh = $this->dbh;

//			$sql = "SELECT zipcode, place, state, country FROM zipcodes WHERE zipcode='$zip' AND country='$country';";
			if ($country != 'us') {
				$sql = "SELECT zipcode, place, state, country FROM zipcodes_$country WHERE zipcode='$zip' ";
			}
			else {
				$sql = "SELECT zipcode, place, state, country FROM zipcodes WHERE zipcode='$zip' AND country='$country';";
			}
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				if (!($result =mysql_fetch_row($result))) {
					if ($country  != 'us') {
						$sql = "SELECT zipcode, place, state, country FROM zipcodes_$country WHERE zipcode='$zip'";
						if ($debug)    print "sql=$sql<br>\n";
						$result = mysql_query($sql, $dbh);
						if ($result) {
							if (!($result =mysql_fetch_row($result))) {
								$error = "No info on Zip '$zip'";
							}
						}
						else {$error = "No info on Zip '$zip'";}
					}
					else {
						$error = "No info on Zip '$zip'";
					}
				}
			}
			$this->CloseConn();

			if ($error) { return $error; }
			else { return $result; }
		}
		else { return "Invalid Zip provided"; }
	}


	Function match_country($tcountry) {
		$debug = $this->debug;

		$tcountry = preg_replace("/\W/", '', strtolower($tcountry));
		if (!$tcountry) $tcountry = 'us';

		$country_abbrev =''; $country_full = '';
		$error = '';

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		if (strlen($tcountry ) == 2) {
			$sql = "SELECT country_name FROM countries WHERE country_abbrev='$tcountry';";
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				list($country_full) =mysql_fetch_row($result);
				$country_abbrev = $tcountry;
			}
		}
		else {
			$sql = "SELECT country_abbrev  FROM countries WHERE country_name='$tcountry';";
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				list($country_abbrev) =mysql_fetch_row($result);
				$country_full = $tcountry;
			}
		}

		$this->CloseConn();
		return array($country_abbrev, $country_full);
	}


	Function match_region($tcountry) {
		$debug = $this->debug;

		$tcountry = preg_replace("/\W/", '', strtolower($tcountry));
		if (!$tcountry) $tcountry = 'us';

		$country_abbrev =''; $country_full = '';
		$error = '';
		$region ='';

		if (!$this->dbh) {
			if ($debug) print "DB connection not open.. opening <br>\n";
			$this->dbh = $this->db_establish($this->dbdatabase, $this->dbhostname, $this->dbusername, $this->dbpw);
		}
		$dbh = $this->dbh;

		if (strlen($tcountry ) == 2) {
			$sql = "SELECT region FROM countries WHERE country_abbrev='$tcountry'";
			if ($debug)    print "sql=$sql<br>\n";

			$result = mysql_query($sql, $dbh);
			if (!$result) { $error = mysql_error($dbh); }
			else {
				list($region) =mysql_fetch_row($result);
				$country_abbrev = $tcountry;
			}
		}


		$this->CloseConn();
		return $region;
	}


	/***************************************************
	/Function match_state
	/$name - a state name or a 2 letter state abbreviation
	/If passed a name, return it's abbreviation, if passed
	/ an abbreviation, returns the state mane
	/**************************************************/
	Function match_state($name) {
		//US and Canada have different states and abbrevs

		if ($this->country == "us") {

			$state_abbrevs = array (
		         'al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'fl', 'ga', 'hi',
		         'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn',
		         'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh',
		         'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa',
		         'wv', 'wi', 'wy', 'us', 'dc', 'pr', 'vi' );

		         $state_names = array ('alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'florida',
		         'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts',
		         'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new hampshire', 'new jersey', 'new mexico',
		         'new york', 'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode island',  'south carolina', 'south dakota',
		         'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west virginia', 'wisconsin', 'wyoming', 'united states', 'd.c.',
		         'puerto rico', 'virgin islands' );
		}
		else {
			$state_abbrevs = array ('ab', 'bc', 'pe', 'pe', 'mb', 'nb', 'ns', 'on', 'qc', 'qc', 'sk', 'nf', 'nf', 'nt', 'nu', 'yk', 'yk' );
			$state_names = array (
		         'alberta', 'british columbia', 'prince edward island', 'prince edward', 'manitoba', 'new brunswick',
		         'nova scotia', 'ontario', 'qu�bec', 'quebec', 'saskatchewan', 'newfoundland', 'new foundland', 'northwest territories',
		         'nunavut', 'yukon territory', 'yukon' );
		}
$name = strtolower($name);

		//Now loop through each array and find a match
		for ($i = 0; $i < count($state_abbrevs); $i++) {
			if ($state_abbrevs[$i] == $name) {
				return array($name, $state_names[$i]);
			}
			elseif ($state_names[$i] == $name) {
				return array($state_abbrevs[$i], $name);
			}
		}

		//Otherwise return what the name parameter was
		return array('', '');
	}


	Function CloseConn() {
		if (!$this->keepopen) {
			mysql_close($this->dbh);
			$this->dbh = null;
		}
	}



}

?>