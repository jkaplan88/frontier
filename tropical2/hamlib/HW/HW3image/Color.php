<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Color.php,v 1.2 2007/09/13 21:08:53 wxfyhw Exp $
*
*/


class HWimageColor {

	var $version = '$Id: Color.php,v 1.2 2007/09/13 21:08:53 wxfyhw Exp $';
	var $colors = array(
	   'aqua' => '00ffff',  'black' => '000000',  'blue' => '0000ff',  'fuchsia' => 'ff00ff',
	   'gray' => '808080', 'grey' => '808080',  'green' => '008000', 'lime' => '00ff00',
	   'maroon' => '800000', 'navy' => '000080', 'olive' => '808000', 'purple' => '800080',
	   'red' => 'ff0000', 'silver' => 'c0c0c0', 'teal' => '008080', 'white' => 'ffffff',
	   'yellow' => 'ffff00'
	   );	
	
	Function HWimageColor () {
		
	}
	
	Function parse_colorspec($hexcolor) {
		
		$rgb = array(0,0,0);
		if (is_array($hexcolor)) {
			$rgb = array($hexcolor[0],$hexcolor[1],$hexcolor[2]);
		}
		else {
			if (isset($this->colors[$hexcolor])) $hexcolor = $this->colors[$hexcolor];
			if (preg_match('/^(\w\w)(\w\w)(\w\w)$/',$hexcolor ,$m)) {
				$rgb = array(hexdec($m[1]), hexdec($m[2]), hexdec($m[3]));
			}
		}
		
		return $rgb;
	}
				
	Function get_color_alpha(&$image, $hexcolor, $alpha=-1) {
		
		if (!imageistruecolor($image) || $alpha < 1) return $this->get_color($image,$hexcolor);
		
		$rgb = $this->parse_colorspec($hexcolor);
		$color = -1;
		$color = imagecolorexactalpha($image, $rgb[0],$rgb[1],$rgb[2], $alpha);
		if ($color == -1) {
			$color = imagecolorallocatealpha ($image, $rgb[0],$rgb[1],$rgb[2], $alpha);
			if ($color == -1) { $color = $color = imagecolorclosestalpha ($image, $rgb[0],$rgb[1],$rgb[2], $alpha); }
		}
		return $color;
	}

	Function get_color(&$image, $hexcolor, $rule=0) {
		
		$rgb = $this->parse_colorspec($hexcolor);
		$color = -1;
		if ($rule < 0) {
			$color = imagecolorallocate($image, $rgb[0],$rgb[1],$rgb[2]);
		}
		else {
			$color = imagecolorexact($image, $rgb[0],$rgb[1],$rgb[2]);
		}
		if (!$rule && $color == -1) {
			$color = imagecolorallocate($image, $rgb[0],$rgb[1],$rgb[2]);
			if ($color == -1) { $color = $color = imagecolorclosest($image, $rgb[0],$rgb[1],$rgb[2]); }
		}
		return $color;
	}
	
	Function clean_colors (&$image, &$colors_to_keep, $replacement_color='FFFFFF', $color_mode=0, $clean_mode=0) {
		
		$keep_colors = '';
		foreach ($colors_to_keep as $color_hex) {
			$keep_colors .=' ' . $this->get_color($image,$color_hex) .' ';
		}
		
		// set white as transparent
		$rep_color = $this->get_color($image, $replacement_color);
		//imagecolortransparent($image, $rep_color);
		$w = imagesx($image); $h = imagesx($image);
		for ($j=0; $j<$h;$j++) {
			for ($i=0; $i<$w; $i++) {
				$pixel = imagecolorat($image,$i,$j);
				if ((!$mode && strpos($keep_colors, ' ' . $pixel . ' ')) || ($mode && $color_used > -1)) {
					imagesetpixel($image,$i,$j,$rep_color);
				}
			}
		}
		
		return $image;
	}
					
		
		
		
	
	
/*


*/

}


?>