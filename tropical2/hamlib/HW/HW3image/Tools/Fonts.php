<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Fonts.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $
*
*/

if (!defined('gdTinyFont')) define ('gdTinyFont',1);
if (!defined('gdSmallFont')) define ('gdSmallFont',2);
if (!defined('gdMediumBoldFont')) define ('gdMediumBoldFont',3);
if (!defined('gdLargeFont')) define ('gdLargeFont',4);
if (!defined('gdGiantFont')) define ('gdGiantFont',5);

class FontTool {

	Function FontTool() {

	}


	Function GetGDFont($name) {
		$name = strtoupper($name);
		if ($name == 'TINY') { return gdTinyFont;}
		elseif ($name == 'SMALL') { return gdSmallFont;}
		elseif ($name == 'LARGE') { return gdLargeFont;}
		elseif ($name == 'GIANT') { return gdGiantFont;}
		else { return gdMediumBoldFont; }


	}

}




?>