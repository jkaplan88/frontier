<?php
error_reporting(0);
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Text.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $
*
*/
require_once (HAMLIB_PATH . '/HW/HW3image/Color.php');

require_once (HAMLIB_PATH . '/HW/HW3image/Tools/Fonts.php');


class HWimageText {

	var $version = '$Id: Text.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $';
	var $GDVersion=1;
	var $debug=0;

	Function HWimageText ($gdversion=2.0, $debug=0) {
		$this->GDVersion=$gdversion;
		$this->debug = $debug;
	}

	Function do_text(&$image, $x,$y,$text, $halign='',$valign='', &$chest) {
		$error ='';
		if (!$image) return "Cannot place text. invalid image";
		$w = imagesx($image);
		$h = imagesy($image);

		$x = strtoupper($x);
		$y = strtoupper($y);

		$points = array();

		if ($x == 'CENTER') {
			if (!$halign) $halign='CENTER';
			$x = floor($w/2);
		}
		elseif ($x == 'LEFT') {
			if (!$halign) $halign = 'LEFT';
			$x = (isset ($chest['left_offset'])) ? $chest['left_offset'] : 0;
		}

		if ($y == 'CENTER') {
			if (!$valign) $valign='CENTER';
			$y = floor($h/2);
		}
		elseif ($y == 'BOTTOM') {
			if (!$valign) $valign = 'BOTTOM';
			$y = $h - ((isset ($chest['bottom_offset'])) ? $chest['bottom_offset'] : 0);
		}
		elseif ($y == 'TOP') {
			if (!$valign) $valign='TOP';
			$y = (isset ($chest['top_offset'])) ? $chest['top_offset'] : 0;
		}

		$halign= ($halign) ? strtoupper($halign) : 'LEFT';
		$valign=($valign) ? strtoupper($valign) : 'TOP';


		$is_TTF =0;
		$ttf_font = $chest['TTF_path'] . '/' . $chest['TTF_font'];
		if ($chest['TTF_font'] && file_exists($ttf_font)) {
			$is_TTF = 1;

			$chest['TTF_font_angle'] = (isset($chest['TTF_font_angle'])) ? $chest['TTF_font_angle'] : 0;
		}
		$chest['is_TTF'] = $is_TTF;



		$lines = $this->_break_text_into_lines ($text, $halign, $chest, $is_TTF);
		if (strtoupper(substr($halign,0,9)) == 'SPACEWRAP') $halign = substr($halign,9);

		$color = new HWimageColor();
		$font_color = $color->get_color($image, $chest['font_color']);

		$fonttool = new FontTool();

		$dropshadow_xoffset  = (isset($chest['dropshadow_xoffset'])) ? $chest['dropshadow_xoffset'] : 0;
		$dropshadow_yoffset  = (isset($chest['dropshadow_yoffset'])) ? $chest['dropshadow_yoffset'] : 0;
		$dropshadow_color = (isset($chest['dropshadow_color'])) ? $color->get_color($image, $chest['dropshadow_color']) : $color->get_color($image, '000000');
//print "<pre>"; var_dump($chest); print "</pre>\n";

		foreach ($lines as $line) {
			$line = trim($line);
			if ($line != '') {

				list ($w,$h,$b) = $this->_get_width_height($line, $chest, $is_TTF);

				list ($ux,$uy) =  $this->_do_text_justification(imagesx($image), $x,$y, $w,$h, $b, $halign, $valign, $is_TTF, $chest['TTF_font_pt']);

				array_push ($points, array($x,$y,$w,$h));

				if ($is_TTF) {
					if ($dropshadow_xoffset && $dropshadow_yoffset) {
						imagettftext($image, $chest['TTF_font_pt'],$chest['TTF_font_angle'],$ux + $dropshadow_xoffset,$uy + $dropshadow_yoffset,$dropshadow_color,$ttf_font,$line);
					}
					imagettftext($image, $chest['TTF_font_pt'],$chest['TTF_font_angle'],$ux,$uy,$font_color,$ttf_font,$line);
				}
				else {
					if ($dropshadow_xoffset && $dropshadow_yoffset) {
						imagestring($image,$fonttool->getGDFont($chest['gdfont']),$ux + $dropshadow_xoffset,$uy + $dropshadow_yoffset,$line,$dropshadow_color);
					}


					imagestring($image,$fonttool->getGDFont($chest['gdfont']),$ux,$uy,$line,$font_color);
				}
				$y+=($h+1);
			}
		}


		$this->used_points =& $points;
		return $error;
	}



	Function _get_width_height($text, $chest, $is_ttf=0) {
		$w = $h = $b = 0;

		if ($is_ttf) {
			if (! ($bounds = @imagettfbbox ( $chest['TTF_font_pt'], $chest['TTF_font_angle'], $chest['TTF_path'] . '/' .$chest['TTF_font'], $text))) {
				$text = str_replace(' ', '_', $text);
				$bounds = imagettfbbox ( $chest['TTF_font_pt'], $chest['TTF_font_angle'], $chest['TTF_path'] . '/' .$chest['TTF_font'], $text);
			}

			$top = ($bounds[5]< $bounds[7]) ? $bounds[5] : $bounds[7];
			$bottom = ($bounds[1]>$bounds[3]) ? $bounds[1] : $bounds[3];
			$left = ($bounds[0]<$bounds[2]) ? $bounds[0] : $bounds[2];
			$right = ($bounds[4]>$bounds[6]) ? $bounds[4] : $bounds[6];
			$b = ($bottom > 0) ? $bottom+1 : 0;
			$w = abs($left-$right);
			$h = abs($top-$bottom)+1;
			//if (preg_match('/[ygj,qp]/', $text)) { $h += 5;}
		}
		else {
			$w  = imagefontwidth((int)$chest['gdfont']) * strlen($text);
			$h = imagefontheight((int)$chest['gdfont'])+10;
		}

		return array($w,$h,$b);
	}


	Function _do_text_justification ( $max_width, $x, $y, $w,$h,$b, $halign='', $valign='', $is_ttf=0, $TTF_font_pt=6) {

		if (strtolower($x) == 'center') {
			$x = floor(($max_width - $w)/2);
		}
		elseif ($halign == 'CENTER') {
			$x -= floor($w/2);
		}
		elseif ($halign == 'RIGHT') {
			$x -= $w;
		}
		// other wise assume lect is just x

		$m = ($is_ttf) ? -1 : 1;

		if ($is_ttf) {
			if ($valign == 'CENTER') {
				$y = $y + floor($h/2);
			}
			elseif ($valign == 'TOP') {
				$y += $TTF_font_pt;
			}
			elseif ($valign == 'BASE' || $valign == 'BOTTOM') {
				$y -= $b;
			}
		}
		else {
			if ($valign == 'CENTER') {
				$y -= floor($h/2);
			}
			elseif ($valign != 'TOP') {
				$y -= $h;
			}
		}

		return array ($x,$y);
	}



	Function _break_text_into_lines($text, $halign, &$chest, $is_ttf=0) {
		$textwrap = (isset($chest['textwrap'])) ? $chest['textwrap'] : 0;

		$lines = array();

		$text = preg_replace(array('/\\n/', '/^\s+/'), array("\n",''), $text);

		$halign = strtoupper($halign);
		if (substr($halign,0,9) == 'SPACEWRAP') {
			$lines = preg_split('/\s+/', $text);
		}
		elseif ($textwrap) {
			$textlines = preg_split('/\n+/', $text);
			foreach ($textlines as $line) {
				$line = trim($line);
				if ($line) {
					$words = preg_split('/\s+/', $line);
					$num_words = count($words);
					$i =0; $phrase=''; $lw=0;

					while ($i < $num_words) {
						$word = $words[$i];
						list ($w,$h,$b) = $this->_get_width_height($word . ' ', $chest, $is_ttf);
						if ($w <= $textwrap) {
							if ($lw +$w <= $textwrap) {
								$phrase .= $word.' ';
								$lw +=$w;
							}
							else {
								if ($phrase) array_push($lines, $phrase);
								$phrase = $word .' ';
								$lw = $w;
							}
						}
						else {
							if ($phrase) array_push($lines, $phrase);
							$max_letters = floor($textwrap / $w * strlen($word) -1);
							if ($max_letters < 1) $max_letters =1;
							while (strlen($word) >= $max_letters) {
								array_push($lines, substr($word, 0, $max_letters));
								$word = substr($word, $max_letters);
							}
							if ($word != '') {
								$phrase = $word . ' ';
								list ($w,$h,$b) = $this->_get_width_height($word . ' ', $chest, $is_ttf);
							}
						}
						$i++;
					}
					if ($phrase) array_push($lines, $phrase);
				}
			}
		}
		else {
			$lines = preg_split('/\n+/', $text);
		}

		return $lines;
	}



}
?>