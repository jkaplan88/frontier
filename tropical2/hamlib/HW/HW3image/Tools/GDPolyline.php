<?php

/*
* Code for this class ported over from GD.pm module
* written by Lincoln Stein.
* http://search.cpan.org/dist/GD/GD.pm
*
* Revision:
* $Id: GDPolyline.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $
*
*/

class GDPolygon extends GDPolyline {

	Function GDPolygon ($debug=0) {
		parent::GDPolyline($debug);
		$this->isPolygon = true;
	}
}


class GDPolyline {

	var $version = '$Id: GDPolyline.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $';
	var $debug=0;

	var $isPolygon = false;

	var $points = array();


	var $bezSegs = 20;	# number of bezier segs -- number of segments in each portion of the spline produces by toSpline()
	var $csr = 0.3333333333;		# control seg ratio -- the one possibly user-tunable parameter in the addControlPoints() algorithm

	Function GDPolyline ($debug=0) {
		$this->debug = $debug;
	}

	Function Destroy () {
		$this->points = array();
	}

	Function addPt($x,$y) {
		array_push($this->points, $x,$y);
	}

	Function getPt($i) {
		$i *=2;
		if (isset($this->points[$i]) && isset($this->points[$i+1])) {
			return array($this->points[$i], $this->points[$i+1]);
		}
		else {
			return NULL;
		}
	}

	Function setPt($i, $x,$y) {
		$i*=2;
		if ($i > sizeof($this->points)) {
			$this->addPt($x,$y);
		}
		elseif ($i >=0) {
			$this->points[$i] = $x;
			$this->points[$i+1] = $y;
		}

	}

	Function deletePt($i) {
		if ($i < 0 || $i >= $this->length()) return;

		list ($x,$y) = array_splice($this->points, $i*2,2);

		return array($x,$y);
	}


	Function length() {
		return intval(sizeof($this->points)/2);
	}

	Function vertices() {
		return $this->points;
	}

	Function scale($sx,$sy) {
		$this->transform($sx,0,0,$sy,0,0);
	}


	function polyline($image, $c, $p = NULL) {
		if ($p == NULL) $p =& $this->points;

		$p1x = array_shift($p);
		$p1y = array_shift($p);

		while (!empty($p)) {
			$p2x = array_shift($p);
			$p2y = array_shift($p);
			imageline($image,$p1x,$p1y,$p2x,$p2y,$c);
		}
	}

	function polygon($image, $c, $p = NULL) {
		if ($p == NULL) $p =& $this->points;
		imagepolygon($image, $p, intval(sizeof($p)/2), $c);
	}

	function filledPolygon($image, $c, $co=NULL, $p = NULL) {
		if ($p == NULL) $p =& $this->points;
		imagefilledpolygon($image, $p, intval(sizeof($p)/2), $c);
		if ($co != NULL) imagepolygon($image, $p, intval(sizeof($p)/2), $co);
	}

	function polyDraw($image, $c, $p = NULL) {
		if ($this->isPolygon) $this->polygon($image, $c, $p);
		else $this->polyline($image, $c, $p);
	}




	Function &toSpline() {


		if ($this->isPolygon) {
			list($x,$y) = $this->getPt(0);
			$this->addPt($x,$y);
		}

		$points = $this->points;

		if (sizeof($points)/2 < 1 || (sizeof($points)/2) % 3 != 1) {
			//Invalid control points
			$null = NULL;
			return $null;
		}


		$ap1 = array(array_shift($points), array_shift($points));
		$bez = new GDPolyline();
		$bez->isPolygon = $this->isPolygon;

		$bez->addPt($ap1[0], $ap1[1]);

		while (!empty($points)) {
			$dp1 = array(array_shift($points), array_shift($points));
			$dp2 = array(array_shift($points), array_shift($points));
			$ap2 = array(array_shift($points), array_shift($points));

			for ($i =1; $i <= $this->bezSegs; $i++) {
				$t1 = $i/$this->bezSegs;
				$t0 = (1-$t1);

				# possible optimization:
				# these coefficient could be calculated just once and
				# cached in an array for a given value of $bezSegs


				$c1 =     $t0 * $t0 * $t0;
				$c2 = 3 * $t0 * $t0 * $t1;
				$c3 = 3 * $t0 * $t1 * $t1;
				$c4 =     $t1 * $t1 * $t1;

				$x = $c1 * $ap1[0] + $c2 * $dp1[0] + $c3 * $dp2[0] + $c4 * $ap2[0];
				$y = $c1 * $ap1[1] + $c2 * $dp1[1] + $c3 * $dp2[1] + $c4 * $ap2[1];
				$bez->addPt($x,$y);
			}

			$ap1 = $ap2;
		}

		if ($this->isPolygon) $this->deletePt($this->length()-1);


		return $bez;
	}

	function &addControlPoints () {

		if ($this->isPolygon) {
#print "is a polygon<br/>";
			list($x1,$y1) = $this->getPt(0);
			list($x2,$y2) = $this->getPt($this->length()-1);
//			if ($x1 != $x2 || $y1 != $y2) {
//				$this->addPt($x1,$y1);
//			}

			if ($x1 == $x2 && $y1 == $y2) {
				$this->deletePt($this->length()-1);
			}


		}

		$segAngles = $this->segAngle();
		$segLengths = $this->segLength();

		# this loop goes about creating polylines -- here called control segments --
		# that hold the control points for the final set of control points

		# each control segment has three points, and these are colinear

		# the first and last will ultimately be "director points", and
		# the middle point will ultimately be an "anchor point"

		$controlSegs = array();

		for ($i = 0; $i < $this->length(); $i++) {

			$controlSeg = new GDPolyline();

			list($ptx,$pty) = $this->getPt($i);


			if (!$this->isPolygon && ( $i== 0 || $i == $this->length()-1)) {
				$controlSeg->addPt($ptx, $pty); #director point
				$controlSeg->addPt($ptx, $pty); #anchor point
				$controlSeg->addPt($ptx, $pty); #director point

				array_push($controlSegs, $controlSeg);
				continue;
			}

			$prevLen = ($i == 0) ? $segLengths[sizeof($segLengths)-1] : $segLengths[$i-1];
			$nextLen = $segLengths[$i];
			$prevAngle = ($i == 0) ? $segAngles[sizeof($segAngles)-1] : $segAngles[$i-1];
			$nextAngle = $segAngles[$i];

			# make a control segment with control points (director points)
			# before and after the point from the polyline (anchor point)

			$controlSeg->addPt($ptx- $this->csr * $prevLen, $pty); #director point
			$controlSeg->addPt($ptx, $pty); #anchor point
			$controlSeg->addPt($ptx + $this->csr * $nextLen, $pty); #director point

			# note that:
			# - the line is parallel to the x-axis, as the points have a common $ptY
			# - the points are thus clearly colinear
			# - the director point is a distance away from the anchor point in proportion to the length of the segment it faces

			# now, we must come up with a reasonable angle for the control seg
			#  first, "unwrap" $nextAngle w.r.t. $prevAngle
			while (!($nextAngle < $prevAngle + pi())) {
				$nextAngle -= 2*pi();
			}
			while (!($nextAngle > $prevAngle - pi())) {
				$nextAngle += 2*pi();
			}

			#  next, use seg lengths as an inverse weighted average
			#  to "tip" the control segment toward the *shorter* segment
			$thisAngle = ($prevLen + $nextLen != 0) ? ($nextAngle * $prevLen + $prevAngle * $nextLen) / ($prevLen + $nextLen) : 0;

			# rotate the control segment to $thisAngle about it's anchor point
			$controlSeg->rotate($thisAngle, $ptx, $pty);

			array_push($controlSegs, $controlSeg);
		}


		# post process

		$controlPoly = new GDPolyline();
		$controlPoly->isPolygon = $this->isPolygon;

		# collect all the control segments' points in to a single control poly
		foreach ($controlSegs as $cs) {
			for ($i =0; $i< $cs->length(); $i++) {
				list ($x,$y) = $cs->getPt($i);
				$controlPoly->addPt($x,$y);
			}
		}

		if (!$controlPoly->isPolygon) {
			$controlPoly->deletePt(0);
			$controlPoly->deletePt($controlPoly->length() - 1);
		}
		else {
			list ($x,$y) = $controlPoly->getPt(0);
			$controlPoly->addPt($x,$y);
			$controlPoly->deletePt(0);
		}


		return $controlPoly;
	}

	function segAngle () {

		$points = $this->points;

		$segAngles = array();

		$p1 =  array(array_shift($points), array_shift($points));

		if ($this->isPolygon) {
			array_push($points, $p1[0], $p1[1]);
		}

		while (!empty($points)) {
			$p2 =  array(array_shift($points), array_shift($points));
			array_push ($segAngles, $this->_angle_reduce2($this->_angle($p1,$p2)));
			$p1 = $p2;
		}

		return $segAngles;
	}


	function segLength() {

		$points = $this->points;

		$segLengths = array();

		$p1 =  array(array_shift($points), array_shift($points));

		if ($this->isPolygon) {
			array_push($points, $p1[0], $p1[1]);
		}

		while (!empty($points)) {
			$p2 =  array(array_shift($points), array_shift($points));
			array_push ($segLengths, $this->_len($p1,$p2));
			$p1 = $p2;
		}

		return $segLengths		;
	}


	function rotate ( $angle, $cx=0, $cy=0) {

    		if ($cx || $cy) $this->offset(-$cx,-$cy);
    		$this->transform(cos($angle),sin($angle),-sin($angle),cos($angle),$cx,$cy);
	}

	function offset ( $dh, $dv) {
		for ($i=0; $i< $this->length(); $i++) {
			list($x,$y) = $this->getPt($i);
			$this->setPt($i, $x+$dh, $y+$dv);
		}
	}




	function transform($a, $b, $c, $d, $tx, $ty) {
		$size = $this->length();
		for ($i = 0; $i <$size; $i++) {
			list ($x,$y) = $this->getPt($i);
			$this->setPt($i, $a*$x+$c*$y+$tx, $b*$x+$d*$y+$ty);
		}
	}


	# The following helper functions are for internal
	# use of this module.  Input arguments of "points"
	# refer to an array ref of two numbers, [$x, $y]
	# as is used internally in the GD::Polygon
	#
	# _len()
	# Find the length of a segment, passing in two points.
	#
	function _len($p1,$p2) {
		$pt = $this->_subtract($p1,$p2);
		return sqrt(pow($pt[0],2) + pow($pt[1],2));
	}



	# _angle()
	# Find the angle of... well, depends on the number of arguments:
	# - one point: the angle from x-axis to the point (origin is the center)
	# - two points: the angle of the vector defined from point1 to point2
	# - three points:
	# Internal function; NOT a class or object method.
	#
	function _angle($p1, $p2=NULL, $p3=NULL) {
		$angle = NULL;
		if ($p2 == NULL) {
			return atan2($p1[1], $p1[0]);
		}
		elseif ($p3 == NULL) {
			return $this->_angle($this->_subtract($p1, $p2));
		}
		else {
			return $this->_angle($this->_subtract($p2, $p3)) - $this->_angle($this->_subtract($p2, $p1));
		}
	}

	# _subtract()
	# Find the difference of two points; returns a point.
	#
	function _subtract($p1,$p2) {
		return array($p2[0]-$p1[0], $p2[1]-$p1[1]);
	}

	# _angle_reduce1()
	# "unwraps" angle to interval -pi < angle <= +pi
	#
	function _angle_reduce1($angle) {
		while ($angle <= -pi()) { $angle += 2 * pi();}
		while ($angle >   pi()) {$angle -= 2 * pi();}
		return $angle;
	}

	# _angle_reduce2()
	# "unwraps" angle to interval 0 <= angle < 2 * pi
	#
	function _angle_reduce2($angle) {
		while ($angle <  0) {$angle += 2 * pi();}
		while ($angle >= 2 * pi()) {$angle -= 2 * pi() ;}
		return $angle;
	}




}
?>