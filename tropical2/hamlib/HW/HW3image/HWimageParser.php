<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HWimageParser.php,v 1.10 2007/09/13 21:08:53 wxfyhw Exp $
*
*/

require_once (HAMLIB_PATH . 'HW/HW3image/Image.php');

class HWimageParser {

	var $version = '$Id: HWimageParser.php,v 1.10 2007/09/13 21:08:53 wxfyhw Exp $';
	var $debug = false;

	var $core_path = '';

	var $max_integer = 32600;
	var $cfg='';
	var $var='';

	var $images = array();
	var $current = '';
	var $on = 0;
	var $single_line =0;
	var $end = 0;
	var $goto_label = '';

	var $i=0;
	var $last_line='';

	var $preparse_proc = array(
		'GETXY' => 'pp_do_getxy',
		'GETY' => 'pp_do_gety',
		'GETX' => 'pp_do_getx',
		'WIDTH' => 'pp_do_getwidth',
		'HEIGHT' => 'pp_do_getheight'
		);

	var $keyword_proc = array(
		'NEWIMAGE' => 'do_newimage',
		'FETCHIMAGE' => 'do_fetchimage',
		'LOADIMAGE' => 'do_loadimage',
		'LOADMAP' => 'do_loadmap',


		'SAVE' => 'do_save',
		'SAVEIMAGE' => 'do_save',
		'OUTPUTIMAGE' => 'do_outputimage',
		'SWITCH2IMAGE' => 'do_switch2image',
		'KILLIMAGE' => 'do_killimage',
		'INSERTIMAGE' => 'do_insertimage',
		'BLEND' => 'do_blend',

		'DRAWWIDTH' => 'do_drawwidth',
		'DRAWCOLOR' => 'do_drawcolor',
		'FILLCOLOR' => 'do_fillcolor',
		'TEXTCOLOR' => 'do_textcolor',
		'BACKGROUND' => 'do_background',
		'TRANSPARENTCOLOR' => 'do_transparentcolor',
		'ADJBRIGHTCONTRAST' => 'do_brightcontrast',
		'TRUECOLOR' => 'do_truecolor',


		'LINE' => 'do_line',
		'DASHEDLINE' => 'do_dashedline',
		'RECTANGLE' => 'do_rectangle',
		'FILL' => 'do_fill',
		'POLYGON' => 'do_polygon',
		'ARC' => 'do_arc',

		'RESIZE' => 'do_resize',
		'CROP' => 'do_crop',

		'INTERLACED' => 'do_interlace',


		'TEXT' => 'do_text',
		'TEXTAT' => 'do_text',

		'TEXTSIZE' => 'do_textsize',
		'TEXTWRAP' => 'do_textwrap',
		'TTFTEXTANGLE' => 'do_TTF_textangle',
		'TTFTEXTFONT' => 'do_TTF_textfont',
		'TTFTEXTPTSIZE' => 'do_TTF_textpt',
		'ANTIALIASING' => 'do_textantialiasing',

		'END' =>'local_end',
		'LABEL' => 'local_label',
		'GOTO' => 'local_goto'


		);

	Function HWimageParser (&$cfg,&$var, $debug) {

		$this->cfg = $cfg;
		$this->var = $var;
		$this->image = new HWimageEngine ($cfg, $cfg->val('Paths', 'hwimage_temp'), $debug);

		$this->debug = $debug;
		if ($debug) print "HWimageParser : " . $this->version . "<br>";

	}

	Function parse_line(&$line, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {

		if ($post_parse) {
			if ($this->on ) {

				return $this->post_parse($line, $post_parse, $template, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
			}
			else {
				return array($line, $pm);
			}
		}
		else {
			return $this->pre_parse($line, $post_parse, $template, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
		}
	}


	Function Debug_Mode ($mode) {
		$this->debug = $mode;
	}


	Function single_line ($value=0){
		$this->single_line = $value;
		return '';
	}


	Function cleanname ($name) {
		$name=str_replace('/','', $name);
		return preg_replace(
			array('/\.\.+/', '/[^\w\\:\-\. \/]/', '/\s/'),
			array('.','','_'),
			$name);
	}


	Function do_hwimage ($mode) {

		if (strtoupper($mode) == 'ON') {
			$this->on = 1;
			$this->end = 0;
			$this->goto_label = '';
		}
		else {
			$this->on = 0;
		}
		return '';
	}
	Function do_hwicmd ($command, $data) {
		$on = $this->on;
		$this->on =0;
		$proc = $this->preparse_proc[strtoupper($command)];
		if ($proc && method_exists($this->image, $proc)) { $b = $this->image->$proc($data);}
		else {$b='';if ($proc) print "$proc is not a command<br>\n";}
		$this->on = $on;
		return $b;
	}
	Function do_imageage($file, $ma) {
//		$file = $this->cleanname($file);
		if ($this->debug) print "file=$file<br>max age=$ma<br>\n";

		if ($ma == 0) return '1';
		if (file_exists($file)) {
			$age = (time() - filemtime($file))/60;
			if ($this->debug) print "file age = $age<br>\n";
			if ($age < $ma) return '1';
		}
		return '0';
	}

	Function do_querycode() {
		GLOBAL $QUERY_STRING;

		$checksum=0;
		for ($i=0; $i < strlen($QUERY_STRING); $i++) { $checksum += ord(substr($QUERY_STRING,$i,1))*$i;}
		$checksum .= floor($checksum / (strlen($QUERY_STRING)+1));
		return $checksum;
	}


	Function pre_parse(&$line, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {

//		$this->i++;
//		if (headers_sent()){ print "headers sent after " . $this->i . "<br>line=$line<br>last line=" . $this->last_line . "<br>\n"; exit;}


		if (preg_match('/%%REDIRECT[: ]\s*([^\s]+?)\s*[: ]REDIRECT%%/', $line, $m)) {
			header('Location: ' .$template->parse_line($m[1],0, $save_file_fh, $pm, $extra_parse, $hashes));
			exit;
		}

		$line = preg_replace('/^(\#|\/\/).+$/','', $line);
		$line = preg_replace('/%%SINGLELINE=(\d)%%/e', "\$this->seingle_line($1);", $line);
		$line = preg_replace('/%%HWIMAGE=([Oo](?:[Nn]|[Ff][Ff]))%%/e', "\$this->do_hwimage('$1');", $line);
		$line = preg_replace('/%%HWICMD:\s*([^\s]+)\s+(.*):HWICMD%%/e', "\$this->do_hwicmd('$1', \$template->parse_line('$2',0, \$save_file_fh, \$pm, \$extra_parse, \$hashes));", $line);
		$line = preg_replace('/%%CLEANNAME\s+(\S.+\S)\s+CLEANNAME%%/e', "\$this->cleanname(\$template->parse_line('$1',0, \$save_file_fh, \$pm, \$extra_parse, \$hashes));", $line);
		$line = preg_replace('/%%IMAGEAGE\s+?(\d+)\s+?(\S.+\S)\s+?IMAGEAGE%%/e',
					"\$this->do_imageage(\$template->parse_line('$2',0, \$save_file_fh, \$pm, \$extra_parse, \$hashes)," .
					"\$template->parse_line('$1',0, \$save_file_fh, \$pm, \$extra_parse, \$hashes));", $line);
		$line = preg_replace('/%%QUERYCODE%%/e', "\$this->do_querycode();", $line);
		$line = preg_replace('/%%[Hh][Ww][Ii][Vv][Aa][Rr]_([\^\w]+)%%/',
					"\$this->image->gethwivar(\$template->parse_line('$1',0, \$save_file_fh, \$pm, \$extra_parse, \$hashes));",
					$line);

//		$this->last_line = $line;
		return array($line, $pm);
	}

	Function post_parse(&$data, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {

		$debug = $this->debug;

		$end = $this->end;
		$ret_str='';
		$data = trim($data);
		if ($debug && $data !='') print "line=$data<br>\n";

		$data = preg_replace('/<!--.+?-->/s', '', $data);
		if ($this->single_line) $data = preg_replace('/[\n\r]+?/', ' ', $data);

		if (!preg_match('/^(?:\#|\/\/)/',$data)) {

			$lines = preg_split('/ :: |[\r\n]+/', ' ' . $data . ' ');
			foreach ($lines as $line) {
				$line = trim($line);
				if ($line !=''){
					if ($debug) print "Processing line $line<br>\n";
					$tokens = $this->split_command($line);
					$command = strtoupper(array_shift($tokens));
					//if ($debug ) print "Syntax error : $command<br>\n";
					$proc = (isset($this->keyword_proc[$command])) ? $this->keyword_proc[$command] : '';

					if ($proc) {
						$goto_label = $this->goto_label;
#print "goto_label=$goto_label<br>\n";
						if ($command == 'END') { $this->end = $end = 1;}

						if (!$this->end) {
							$error = ''; $end_check='';
							if (substr($proc,0,6) == 'local_') {
								if (!$goto_label || $command == 'LABEL') {
									list ($error, $parse) = $this->$proc($template, $tokens);
									if ($parse) {
										$ret_str = $template->parse_line($parse, $post_parse, $template, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
									}
								}
							}
							elseif (!$goto_label) {
								//list($error, $end_check) = $this->image->$proc($tokens);
	//print "proc=$proc...tokens=$tokens<br>\n";
								$error = call_user_func_array(array(&$this->image, $proc), $tokens);
							}
							else {
								if ($debug) print "...Skipping line <br>\n";
							}
							if ($debug && $error) print "...Error=$error<br>\n";
						}
					}
					else {
						$ret_str .= $line;
					}
				}
			}
		}

		return array($ret_str,0);
	}



	Function local_goto($template, $label) {
		if (!isset($label[0]) || !$label[0]) return array('Error: No valid Label given', '');
		$this->goto_label = $label[0];
		if ($this->debug) print "Go to label=$label[0]<br>\n";
		return array('','');
	}

	Function local_label($template, $alabel) {
		$label = isset($alabel[0]) ? $alabel[0] : '';
		if (!$label) return array('Error: No valid Label given', '');
		if ($this->debug) print "label = $label<br>\n" .
							"Current goto = " . $this->goto_label . "<br>\n";
		if ($this->goto_label == $label) $this->goto_label='';
		return array('','');
	}

	Function local_end($template, $label) {
		$this->end =1;
	}


	Function split_command($use_line) {


		if ($use_line) {
			$split = preg_split('/\s+/', $use_line ,2);
			if (count($split) == 2) { list($arg,$line) = $split;} else { $arg=$split[0]; $line='';}

			$result = array($arg);

			while ($line != '') {
				if (substr($line,0,1) == '"') {
					if (preg_match('/^\"([^\"]*)\"(?:\s+(.*))?$/', $line,$m)) {
						array_push($result, $m[1]);
						$line = isset($m[2]) ? $m[2] : '';
					}
					else {
						return array("ERROR", "Unmatched quote");
					}
				}
				elseif (substr($line,0,1) == "{") {
					if (preg_match('/^\{([^\}]*)\}(?:\s+(.*))?$/', $line,$m)) {
						$line = isset($m[2]) ? $m[2] : '';
						$eval_result = $this->eval_expr($m[1]);
						if ($eval_result[0] == 'ERROR') return $eval_result;
						array_push($result, $eval_result[1]);
					}
					else {
						return array("ERROR", "Unmatched curley brace");
					}
				}
				else{
					$split = preg_split('/\s+/', $line ,2);
					if (count($split) == 2) { list($arg,$line) = $split;} else { $arg=$split[0]; $line='';}
					array_push($result,$arg);
				}
			}
			return $result;
		}
		else { return array('','');}

	}

	Function eval_expr($expr) {
		$expr = trim(preg_replace(array('/\beq\b/','/\bne\b/'), array('==','!='),$expr));



		if (!strpos($expr,'return')) { $expr = 'return ' . $expr .';'; }
//print "eval=$expr<br>\n";

		$result = eval($expr);

		return array('',$result);
	}




}
?>