<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: BrightContrast.php,v 1.1 2003/12/17 23:37:07 lee Exp $
*
*/

Function adj_bright_contrast (&$image, $bright, $contrast) {
	if (!$contrast) $contrast =0;
	elseif ($contrast < -1) { $contrast = -1;}
	elseif ($contrast > 1) { $contrast = 1;}
	
	if (!$bright) $bright =0;
	elseif ($bright < -1) { $bright = -1;}
	elseif ($bright > 1) { $bright = 1;}
	
	if (!($bright == 0 && $contrast ==0)) {

		for( $i=0; $i < imagecolorstotal($image); $i++ ) {
			$c = imagecolorsforindex($image,$i);
			$r = $this->adj_RGB_bright_contrast($c['red'],$bright,$contrast);
			$g = $this->adj_RGB_bright_contrast($c['green'],$bright,$contrast);
			$b = $this->adj_RGB_bright_contrast($c['blue'],$bright,$contrast);
			imagecolorset($image,$i,$r,$g,$b);
		}
	}
}

Function adj_RGB_bright_contrast ($rgb, $bright, $contrast) {
	
	if ($contrast <=0) {
		# decrease contrast
		$val = abs($rgb-128) * (-$contrast);
		if ($rgb < 128) { $rgb += $val; }
		else { $rgb -= $val;}
	}
	else {
		# increase contrast
		if ($rgb < 128) { $rgb = $rgb - ($rgb * $contrast); }
		else { $rgb = $rgb + ((255-$rgb) * $contrast); }
	}
	
	$rgb = int($rgb + $bright*255);
	if ($rgb > 255) { $rgb = 255; }
	elseif ($rgb < 0) { $rgb = 0; }
	return $rgb;
}







?>