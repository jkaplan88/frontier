<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: GreyScale.php,v 1.2 2007/09/13 21:08:54 wxfyhw Exp $
*
*/


Function greyscale (&$image) {

	for( $i=0; $i < imagecolorstotal($image); $i++ ) {
		$c = ImageColorsForIndex( $image, $i );
		$t = ($c['red']+$c['green']+$c['blue'])/3;
		imagecolorset( $img, $i, $t, $t, $t );
	}
}


?>