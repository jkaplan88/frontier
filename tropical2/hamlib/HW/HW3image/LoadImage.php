<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: LoadImage.php,v 1.3 2004/05/12 10:57:58 lee Exp $
*
*/

Function load_image($image_path, $useimage, $gd_ext) {
	return load_image2 ("$image_path/$useimage.$gd_ext", $gd_ext);
}

Function load_image2 ($file, $gd_ext) {

	if (preg_match('/^http/', $file) || file_exists("$file")) {
		$lcgdext = strtolower($gd_ext);
		if ($lcgdext == 'png') {
			$image = @imagecreatefrompng("$file");
		}
		elseif ($lcgdext == 'jpg' || $lcgdext == 'jpeg') {
			$image = @imagecreatefromjpeg("$file");
		}
		elseif($lcgdext == 'gif') {
			$image = @imagecreatefromgif("$file");
		}

		return array(&$image, '');
	}
	else {
		return array('',"File $file does not exist");
	}
}






?>