<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Cookies.php,v 1.11 2006/09/30 00:08:59 wxfyhw Exp $
*
*/

Class Cookies {

	/***************************************************
	/Class Data
	/**************************************************/
	var $cookie_string = "";

	/***************************************************
	/Function GetCookie
	/Retrieves the cookie from the user and breaks it apart into it's chips
	/**************************************************/
	Function GetCookie(&$form, $cookie_name) {
		//Get the cookie from the user
		global $HTTP_COOKIE_VARS;

		//Check to make sure the cookie is there
		if (isset($HTTP_COOKIE_VARS[$cookie_name])) {
			$cookie_string = $HTTP_COOKIE_VARS[$cookie_name];
		}
		elseif (isset($_COOKIE[$cookie_name])) {
			$cookie_string = $_COOKIE[$cookie_name];
		}
		else { $cookie_string = '';}

		$this->cookie_string = $cookie_string;

		//If it's blank just return 'cause there's no cookie
		if ($cookie_string == "") {
			return array();
		}

		if (isset($form['uc'])) { $uuc = $form['uc']; }
		else {$uuc='';}
		if (strtolower($uuc) == 'hwall') $uuc='';

		if (isset($form['nc'])) { $unc = $form['nc']; }
		else {$unc='';}

		if (isset($form['rc'])) { $urc = $form['rc']; }
		else {$urc='';}

		if (isset($form['oc'])) { $uoc = $form['oc']; }
		else {$uoc='';}

		$oc = array(); $ov='';
		if ($uoc) {
			$aoc = explode(',', $uoc);
			reset($aoc);
			while (list(, $oc_sect) = each($aoc)) {
				$t= explode('|',$oc_sect);
				if (isset($t[0])) {
					$key = preg_replace('/\W/','',$t[0]);
					if (isset($t[1])) {
						$val = preg_replace('/[^\w ,.-]/','',$t[1]);
					}
					else {
						$val = '';
					}

					if ($key != '') {
						if (!$val) $val = $key;
						$oc[$key]=$val;
						$ov .= ','.$key;
					}
				}
			}
		}

		if ($uuc && strtolower($unc) == 'hwall') return array();
		if ($uuc) $uuc = ',$uuc,';
		if ($unc) $unc = ',$unc,';
		if ($ov) $ov .= ',';


		$chips = array();
		$tchips = explode('|', $cookie_string);
		for ($i = 0; $i < count($tchips); $i = $i + 2) {
			$key = preg_replace('/\W/','',$tchips[$i]);
			$val = preg_replace('/[^\w .,-]/','',$tchips[$i+1]);
			if ($key) {
				$chips[$tchips[$i]] = $tchips[$i+1];
			}
		}


		reset($chips);
		while (list($key,) = each($chips)) {
			$override = strpos($ov,",$key,");
			if (isset($form[$key]) && $override === false) continue;

			$use = 1;
			if ($override === false && $uuc) {
				$pos = strpos($uuc, ",$key,");
				if ($pos === false) $use=0;
			}
			elseif ($unc) {
				$pos = strpos($unc, ",$key,");
				if (!($pos === false)) $use =0;
			}

			if ($use) {
				if (!($override===false)) { $form[$oc[$key]] = $chips[$key]; }
				else { $form[$key] = $chips[$key]; }
			}
		}

		if ($urc) {
			$form['sc']=1;
			if (strtolower($urc) == 'hwall') { $cookie_string = ''; }
			else {
				$arc = explode(',', $rc);
				reset ($arc);
				while (list(,$remove) = each($arc)) {
					if (isset($chips[$remove])) unset($chips[$remove]);
				}
				$cookie_string ='';
				reset($chips);
				while (list($key,$value) = each ($chips)) {
					$cookie_string .= "$key|$value|";
				}
				$cookie_string = preg_replace('/\|$/', '', $cookie_string);
			}

		}


		$this->cookie_string = $cookie_string;
		return $chips;

	}

	/***************************************************
	/Function SetCookie
	/Turns the chips into a comma delim list and places them in the cookie
	/**************************************************/
	Function SetCookie($sc, &$hashes,$cookie_name, $cookie, $expires, $server_name, $script_name) {
		$new_cookie = "";

		if ($sc) {

			$chips = array();
			if ($cookie) {
				$tchips = explode('|', $cookie);
				for ($i = 0; $i < count($tchips); $i = $i + 2) {
					$chips[$tchips[$i]] = $tchips[$i+1];
				}
			}

			$asc = explode(',', $sc);
			foreach($asc as $set_var) {

				$val = $this->GetValueFromHash($set_var, $hashes);
				$set_var = preg_replace('/^chip_/', '', $set_var);
				$chips[$set_var] = $val;
			}

			$new_cookie='';
			foreach ($chips as $key => $value) {
				$new_cookie .= "$key|$value|";
			}
			$new_cookie = preg_replace('/\|$/', '', $new_cookie);

			//If expired is not set, set if for two years
			if ($expires == "" || $expires == 0) {
				$expires = time() + 63072000;
				//$expires = time() + 3600;
			}

			//Set the cookie
			setcookie($cookie_name, $new_cookie, $expires, $script_name,$server_name );
		}
	}

	Function GetCookieString() {
		return $this->cookie_string;
	}

	Function GetValueFromHash ($chip_name, &$hashes) {
		$value_found = '';

		if (preg_match('/^(\w+?)(\d*)(?:_vv(\w+))?$/', $chip_name, $m)) {
			$match = $m[1];
			$match_num = $m[2];
			if(isset($m[3])) $match_v = $m[3];
		}
		else {
			$match = $chip_name;
			$match_num = '';
			$match_v='';
		}



		foreach ($hashes as  $ref) {
			if (is_object($ref) ) {
				if (method_exists($ref, $match)) {
					$value_found = $ref->$match($match_num, $match_v);
				}
				else {
					$value_found = $ref->GetValue($match, $match_num);
				}
			}
			elseif (is_array($ref)) {
				if (isset($ref[$match])) {
					if (is_array($ref[$match]) && $match_num !='') {
						$tmp =& $ref[$match];
						if (isset($tmp[$match_num])) $value_found = $tmp[$match_num];
					}
					else {	$value_found = $ref[$match];}
				}
			}

			if ($value_found != '' && !is_null($value_found)) break;
		}

		return $value_found;

	}





	/***************************************************
	/Function SetChip
	/$key - the key the value will be stored under
	/Returns the value of $key, or blank if none
	/**************************************************/
	/*Function GetChip($key) {
		return $this->chips[$key];
	}*/

	/***************************************************
	/Function SetChip
	/$key - the key the value will be stored under
	/$value - the value to be stored
	/Sets a chip value in the cookie
	/**************************************************/
	/*Function SetChip($key, $value) {
		$this->chips[$key] = $value;
	}*/
}
?>