<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: common.php,v 1.20 2005/08/22 19:32:19 wxfyhw Exp $
*
*/


date_default_timezone_set('America/Chicago');

Function getKeyVal (&$hash, $key, $default) {
   if (isset($hash[$key])) { return $hash[$key]; }
      else {return $default; }
}







// Common Debug routines


// call this routine passing it the INIReader object
// it will print out the INI files Loaded
Function print_cfg (&$cfg) {
	$arr = $cfg->Sections();
	reset($arr);
	while (list(,$section) = each($arr)) {
		if ($section != 'Database Access' && !$cfg->Val($section, 'debug_no_show')) {
			print "<br>\nSECTION = $section <BR>\n";
			$parr = $cfg->Parameters($section);
			foreach ($parr as $param) {
				$value = $cfg->Val($section,$param);
				print "   $param = $value <br>\n";
			}
		}
	}
}


Function print_var ($varname, &$varref) {
   print "<hr><br>\n";
   print "<h2>Variable: $varname</h2><pre>\n";
   var_dump($varref);
   print "</pre><br>\n";
}


Function clean_cache(&$cache_dirs) {

	$write_cache = 0;

	foreach ($cache_dirs as $cache_dir) {
		if (!$cache_dir || !is_dir($cache_dir)) continue;

		if (file_exists("$cache_dir/cache.txt")) {
			$age = (time() - filemtime("$cache_dir/cache.txt")) / 3600;
			if ($age > 1 || $age < -0.5) {
				if (($dir = opendir($cache_dir)) !== false) {
					while ($file = readdir($dir)) {
						if ($file[0] != '.') {
							if (!is_dir("$cache_dir/$file")) {
								//print "unlink($cache_dir/$file)<br>\n";
								unlink("$cache_dir/$file");
							}

						}
					}
					closedir($dir);
					$write_cache=1;
				}
			}
		}
		else { $write_cache=1;}

		if ($write_cache) {
			if ($file = fopen("$cache_dir/cache.txt", "w")) {
				fputs($file, time(). "\n");
				fclose($file);
				@chmod("$cache_dir/cache.txt", 0666);
			}
		}
	}
}

Function clean_dirs($howoften, $ma, $dirs) {
	// $ma should be the max age in minutes
	// if not set to less than 1 minute we will reset to 1440 minutes (1 day)
	if ($ma < 1) { $ma = 1440; }
	if ($howoften < 1) $howoften = 1440	;


	foreach ($dirs as $dir) {
		if ($dir && is_dir($dir)) {

			$write_cache = 0;
			# we only want to clean the directory every $howoften minutes
			# that we we will not spend time cycling through directory on every
			# hit to the cgi
			if (file_exists("$dir/agecheck.txt")) {
				$age = (time() - filemtime("$dir/agecheck.txt")) / 60;
				if ($age > $howoften || $age < 0 ) {
					$directory = opendir($dir);
					if ($directory) {
						while ($file = readdir($directory)) {
							if (substr($file,0,1) != '.' ) {
								$age = (time() - filemtime("$dir/$file")) / 60;

								if (($age+0) > ($ma+0)) {
									unlink ("$dir/$file");
								}
							}
						}
						closedir($directory);
						$write_cache=1;
					}
				}
			}
			else { $write_cache = 1;}

			if ($write_cache) {
				if ($file = fopen("$dir/agecheck.txt", "w")) {
					fputs($file, time(). "\n");
					fclose($file);
					@chmod("$dir/agecheck.txt", 0666);
				}
			}
		}
	}
	return 1;
}

Function log_hit($path, $mode=0) {
	// mode = 0 = One file per day
	// mode = 1 = a Single file always

	global $HTTP_REMOTE_HOST, $HTTP_REMOTE_ADDR;
	global $HTTP_REQUEST_METHOD, $HTTP_QUERY_STRING, $HTTP_USER_AGENT;
	global $HTTP_REFERER;
	$exists = 0;

	if (is_dir($path)) {
		if ($mode) { $tname = 'log.txt'; }
		else { $tname = 'log' . date('Ymd') . '.txt'; }
		$ip = (getenv('REMOTE_HOST'))  ? getenv('REMOTE_HOST') : getenv('REMOTE_ADDR');
		if (preg_match('/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/', $ip, $m)) {
			$ip = sprintf('%03u.%03u.%03u.%03u', $m[1], $m[2], $m[3], $m[4]);
		}
		$fname = "$path/$tname";
		if (file_exists($fname)) $exists = 1;

		if ($file = fopen($fname, 'a')) {
			$s = $ip . '|' . date('Ymd').'|'.date('H') . ':' . date('i').':'.date('s') .
				'|'.getenv('REQUEST_METHOD') . '|' .getenv('QUERY_STRING') .
				'|' .getenv('HTTP_USER_AGENT') . '|' . getenv('HTTP_REFERER') . "\n";
			fputs($file, $s);
			fclose($file);
			if (!$exists) @chmod($fname, 0666);
		}
	}
}


function check_referer($referer_file, $referer_url, $debug) {

	if ($debug)print "Referrer Checking<br>";

	if (is_file($referer_file)) {
		$referer = strtolower(getenv('HTTP_REFERER'));

		if ($debug) print "referrer is $referer<br>\n";
		$found =0;
		$rfile = file($referer_file);
		foreach ($rfile as $pattern) {
			$pattern = trim ($pattern);
			if ($debug) print "...testing against $pattern<br>\n";

			if ((($pattern == 'allowblank') && (!$referer)) || ($referer && preg_match("/$pattern/", $referer))) {
				if ($debug) print "......MATCH FOUND <br>\n";
				$found =1;
				break;
			}
		}


		if (!$found && $referer_url) {
			header("Location: $referer_url");
			exit;
		}

	}
	else {
		if ($debug) print "referrer file $referer_file does not exist<br>\n";
	}

	return 1;
}





?>