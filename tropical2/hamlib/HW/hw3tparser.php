<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: hw3tparser.php,v 1.19 2006/05/07 17:55:45 wxfyhw Exp $
*
*
*/

class hw3tparser {

	var $cfg = '';
	var $var = array();
	var $form = array();
	var $pluginrunner;
	var $iniext = 'ini.php';
	var $pploaded = array();
	var $pproutine = array();
	var $pppre = array();
	var $pppost = array();

	var $debug=0;

	var $core_path = '';

	Function hw3tparser  ($core_path, &$cfg_ini, &$var, &$form, &$pr, $iniext, $debug=0) {
		$this->core_path = $core_path;
		$this->cfg =& $cfg_ini;
		$this->var =& $var;
		$this->form =& $form;
		$this->pluginrunner =& $pr;
		$this->iniext = $iniext;
		$this->debug = $debug;

		$this->load_parse_plugins();
	}

	Function parse_line (&$line, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {

		if (!$post_parse) {

			$line = preg_replace('/%%CFG:\s*([^:]+?)\s*:\s*([^%]+)%%/e',
				"\$this->do_CFG('$1','$2', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			$line = preg_replace('/%%COOKIE:(\w+)%%/e',
				"\$this->do_COOKIE('$1', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			$line = preg_replace('/%%FORECAST\s(.+)\sFORECAST%%/e',
				"\$this->do_FORECAST('$1', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			$line = preg_replace('/%%CONFIG=(.+?)%%/e',
				"\$this->do_CONFIG('$1', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			$line = preg_replace('/%%USE=(.+?)%%/e',
				"\$this->do_USE('$1', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			$line = preg_replace('/%%UNUSE=(.+?)%%/e',
				"\$this->do_UNUSE('$1', \$template, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

			foreach ($this->pppre as $key => $pp_name) {
				if (isset($this->pproutine[$pp_name])) {
					$rou = $this->pproutine[$pp_name];
					$plugin =& $this->pploaded[$pp_name];
					list($line, $pm) = $plugin->$rou($line, $post_parse, $template, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
				}
				else { unset($this->pppre[$key]); }
			}

		}
		else {

			$line = preg_replace('/FULL_COUNTRY_([A-Za-z][A-Za-z])/e',"\$this->do_FULL_COUNTRY('$1');",$line);
			$line = preg_replace('/COUNTRY2REGION_([A-Za-z][A-Za-z])/e',"\$this->do_COUNTRY2REGION('$1');",$line);


			foreach ($this->pppost as $key => $pp_name) {
				if (isset($this->pproutine[$pp_name])) {
					$rou = $this->pproutine[$pp_name];
					$plugin =& $this->pploaded[$pp_name];
					list($line, $pm) = $plugin->$rou($line, $post_parse, $template, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
				}
				else { unset($this->pppost[$key]); }
			}

		}

		return array($line, $print_now);

	}

	function do_FULL_COUNTRY ($arg) {
		$fc= $this->pluginrunner->dbaccess->match_country($arg);
		return (isset($fc[1])) ? $fc[1] : '';
	}
	function do_COUNTRY2REGION ($arg) {
		return $this->pluginrunner->dbaccess->match_region($arg);
	}


	Function do_FORECAST($args, &$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {

		$a = $template->parse_line($args, 0, $save_file_fh, $pm, $extra_parse, $hashes);

		if ($a) {
			$this->pluginrunner->DoAnotherPlugin($a, 0, $hashes);
		}

		return '';

	}

	Function  do_CONFIG ($ini,&$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {
	//      print "hello<br>";
		$a = $template->parse_line(str_replace('^^','%%',$ini), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		if ($a) $this->cfg->ReadINI("$a.".$this->iniext);
		$this->pluginrunner->load_hwv($this->cfg, $this->var, $this->form);

		return '';
	}


	Function  do_COOKIE ($chip,&$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$b = $template->parse_line(str_replace('^^','%%',$chip), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		if (isset($this->var['chips'][$b])) { $a = $this->var['chips'][$b];}
		else { $a = ''; }

		return $a;
	}



	Function do_CFG ($section, $key,&$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$a = $template->parse_line(str_replace('^^','%%',$section), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$b = $template->parse_line(str_replace('^^','%%',$key), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$output = $this->cfg->val($a,$b);

		return $output;
	}

	Function do_USE ($plugin,&$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$plugin = $template->parse_line(str_replace('^^','%%',$plugin), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$this->load_parse_plugin($plugin);
		if ($this->debug) print "USE $plugin<br>\n";
		return '';
	}

	Function do_UNUSE ($plugin,&$template, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$plugin = $template->parse_line(str_replace('^^','%%',$plugin), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$this->unload_parse_plugin($plugin);
		if ($this->debug) print "UNUSE $plugin<br>\n";
		return '';
	}



	Function load_parse_plugins() {
		$plugins = $this->cfg->SectionParams('Parse Plugins');

		if (is_array($plugins)) {
			foreach ($plugins as $name => $data) {
				$pd = explode('|',$data);
				if (isset($pd[4]) && $pd[4]) {
					$this->load_parse_plugin($name);
				}
			}
		}
	}


	Function load_parse_plugin($name) {

		if (!$name) return;
		if (isset($this->pploaded[$name])) return;

		$new_module = '';

		$parms = $this->cfg->val('Parse Plugins', $name);
		list($pm,$routine,$pre,$post) = explode('|', $parms);

		$pm = str_replace('::','/', $pm);
		$pos = strrpos($pm, '/');
		if ($pos !== false) {
			$new_module = substr($pm,$pos+1);
		}
		else { $new_module = $pm;}


		if ($this->debug) print "Loading Parser Plugin $pm.php<br>\n";
		require_once(HAMLIB_PATH . $pm.'.php');
		$this->pploaded[$name] = new $new_module($this->cfg, $this->var, $this->debug);
		$this->pproutine[$name] = $routine;
		if ($pre) array_push($this->pppre, $name);
		if ($post) array_push($this->pppost, $name);
		return '';
	}

	Function unload_parse_plugin($name) {
		if (isset($this->pploaded[$name])) unset($this->pploaded[$name]);
		if (isset($this->pproutine[$name])) unset($this->pproutine[$name]);
//		if (isset($this->pppre[$name])) unset($this->pppre[$name]);
//		if (isset($this->pppost[$name])) unset($this->pppost[$name]);
		return '';
	}




}
?>