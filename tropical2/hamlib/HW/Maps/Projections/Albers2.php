<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Albers2.php,v 1.1 2007/09/13 21:08:54 wxfyhw Exp $
*
*/


if (!defined('pi2')) define ('pi2', 2 * pi());





class Albers2 {

	var $version = '$Id: Albers2.php,v 1.1 2007/09/13 21:08:54 wxfyhw Exp $';
	var $debug=0;
	var $error='';
	var $info = array();

	var $pi = 0;
	var $pi2 = 0;
	var $DR = 0;



	Function Albers2 (&$info, &$args) {

		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;

		if ($debug) print "Albers version:" . $this->version . "<br>\n";

		$this->pi =  4 * atan2(1, 1);
		$this->pi2 = 2 * $this->pi;
		$this->DR = $this->pi2 / 360;


		$this->init_projection($info);
	}



	Function init_projection(&$info) {

		if (isset($info['left meters'])) { $info['lm'] = $info['left meters']; }
		elseif (!isset($info['lm'])) { $info['lm'] = 0;}

		if (isset($info['top meters'])) { $info['tm'] = $info['top meters']; }
		elseif (!isset($info['tm'])) { $info['tm'] = 0;}

		if (isset($info['right meters'])) { $info['rm'] = $info['right meters']; }
		elseif (!isset($info['rm'])) { $info['rm'] = 0;}

		if (isset($info['bottom meters'])) { $info['bm'] = $info['bottom meters']; }
		elseif (!isset($info['bm'])) { $info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		if (isset($info['sp1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['sp1']); }
		elseif (isset($info['standard parallel 1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['standard parallel 1']); }
		else { $slat1 = $this->info['slat1'] = 0;}

		if (isset($info['sp2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['sp2']); }
		elseif (isset($info['standard parallel 2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['standard parallel 2']); }
		else { $slat2 = $this->info['slat2'] = 0;}

		if (isset($info['cm'])) { $cm = $this->info['cm'] = deg2rad($info['cm']); }
		elseif (isset($info['central meridian'])) { $cm = $this->info['cm'] = deg2rad($info['central meridian']); }
		else { $cm = $this->info['cm'] = 0;}

		if (isset($info['rlat'])) { $rlat = $this->info['rlat'] = deg2rad($info['rlat']); }
		elseif (isset($info['reference latitude'])) { $rlat = $this->info['rlat'] = deg2rad($info['reference latitude']); }
		else { $rlat = $this->info['rlat'] = 0;}

		if (isset($info['semi-major axis'])) { $a = $this->info['a'] = $info['semi-major axis']; }
		else { $a = $this->info['a'] = 6378206.4000000004;}

		if (isset($info['flattening'])) { $f = $this->info['f'] = $info['flattening']; }
		else { $f = $this->info['f'] = (1/294.97870);}

		if (isset($info['rho'])) { $rho = $this->info['rho'] = $info['rho']; }
		else { $rho = $this->info['rho'] = 63710000;}

		$this->info['lm'] = $info['lm'];
		$this->info['rm'] = $info['rm'];
		$this->info['tm'] = $info['tm'];
		$this->info['bm'] = $info['bm'];

		//#Albers stuff here
		//if (($info['tm'] > 0 && $info['bm'] > 0) || ($info['tm'] < 0 && $info['bm'] < 0)) {
			$yscale = $this->info['yscale'] = (abs($this->info['tm'] - $this->info['bm']))/$this->info['height'];
//		}
//		else {
//			$yscale = $this->info['yscale'] = (($this->info['tm']) + ($this->info['bm']))/$this->info['height'];
//		}

		//if (($info['lm'] > 0 && $info['rm'] > 0) || ($info['lm'] < 0 && $info['rm'] < 0)) {
			$xscale = $this->info['xscale'] = (abs($this->info['lm'] - $this->info['rm']))/$this->info['width'];
//		}
//		else {
//			$xscale = $this->info['xscale'] = (($this->info['lm']) + ($this->info['rm']))/$this->info['width'];
//		}
if ($this->debug) { print "(xscale,yscale) = ($xscale,$yscale)<br>\n";}
		$this->info['ww'] = $this->info['lm']/$xscale;
		$this->info['hh'] = $this->info['tm']/$yscale;

//		//More strict Albers Stuff
//		list ($m1,$q1, $j1,$j2) = $this->_fetch_albers_constants($slat1);
//		list ($m2,$q2, $j1,$j2) = $this->_fetch_albers_constants($slat2);

//		$n = $this->info['n'] = (pow($m1,2) - pow($m2,2))/($q1 - $q2);
//		$c = $this->info['c'] = pow($m1,2) - $n * $q1;
//		list ($m0,$q0, $j1,$j2) = $this->_fetch_albers_constants($rlat);
//		$r0 = $this->info['r0'] = ($a/$n) * sqrt($n*$q0+$c);


		$n = $this->info['n'] = log(cos($slat1) * $this->sec($slat2)) / log( tan(pi()/4 + .5 * $slat2) * $this->cot(pi()/4 + .5 * $slat1) );
		$F = $this->info['F'] = ( cos($slat1) * pow(tan(pi()/4 + .5 * $slat1), $n) ) / $n;
		$r0 = $this->info['r0'] = $F * $a * pow($this->cot(pi()/4 + .5 * $rlat), $n);

//print "n=$n<br>\nF=$F<br>\nr0=$r0<br>\n";
//print "lm=" . $this->info['lm'] . "<br>tm=" . $this->info['tm'] . "<br>\n";
//print "xscale=$xscale<br>\nyscale=$yscale\n";

		return 1;
	}


	function get_lonlat ($x,$y) {



		$lm = $this->info['lm'];
		$tm = $this->info['tm'];
	 	$xscale = $this->info['xscale'];
	 	$yscale = $this->info['yscale'];


		$x_ns = ($x * $this->info['xscale'] + $lm);
		$y_ns = ($y * $this->info['yscale'] - $tm)*-1;

//print "x_ns=$x_ns<br>\ny_ns=$y_ns<br>\n";

		$a = $this->info['a'];
		$n = $this->info['n'];
		//$c = $this->info['c'];
		$cm = $this->info['cm'];
		$r0 = $this->info['r0'];
		$F = $this->info['F'];



      	$sgn = ($n >= 0 ) ? 1 : -1;
      	$r = $sgn * sqrt( pow($x_ns,2) + pow($r0 - $y_ns,2) );

      	$theta = atan2($x_ns , $r0-$y_ns);
      	$lat = 2 * atan2( pow($F*$a/$r,(1/$n)),1 ) - pi()/2;
      	$lon = $cm + $theta/$n;

		$lon = rad2deg($lon);
		$lat = rad2deg($lat);

		return array($lon,$lat);


}


	Function get_xy ($lon,$lat) {

	if ($this->debug) { print "(lon,lat) = ($lon,$lat)<br>\n";}
		$lon = deg2rad($lon);
		$lat = deg2rad($lat);

		$a = $this->info['a'];
		$n = $this->info['n'];
//		$c = $this->info['c'];
		$cm = $this->info['cm'];
		$r0 = $this->info['r0'];
		$F = $this->info['F'];

		$r = $a * $F * pow($this->cot(pi()/4 + .5 * $lat), $n);
      	$x_ns = $r * sin ( $n * ($lon-$cm) );
      	$y_ns = $r0 - $r * cos ( $n * ($lon-$cm) );

		$lm = $this->info['lm'];
		$tm = $this->info['tm'];
      	$xscale = $this->info['xscale'];
      	$yscale = $this->info['yscale'];

#
      	$x = ($lm > 0) ? ($x_ns- $lm)/$xscale : ($x_ns+ abs($lm))/$xscale;
      	$y = ($tm-$y_ns)/$yscale;#+$ym ;


		$x = floor($x);
		$y = floor($y);
		return array($x,$y);

	}

	Function cos2 ($num) {
		$cos = cos($num);
		return ($cos * $cos);
	}


	Function sin2 ($num) {
		$sin = sin($num);
		return ($sin * $sin);
	}

	Function sec ($num) {
		return 1/cos($num);
	}

	Function cot ($num) {

		$val = (sin($num) == 0) ? 0 : cos($num)/sin($num);
		return $val;
	}



}
?>