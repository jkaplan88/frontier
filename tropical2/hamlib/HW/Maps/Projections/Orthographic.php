<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2006 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Orthographic.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $
*
*/


class Orthographic {

	var $version = '$Id: Orthographic.php,v 1.1 2007/09/13 21:08:55 wxfyhw Exp $';
	var $debug=0;
	var $error='';
	var $info = array();

	var $pi = 0;
	var $pi2 = 0;
	var $DR = 0;
	
	Function Orthographic (&$info, &$args) {
	
		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;
		
		if ($debug) print "Orthographic version:" . $this->version . "<br>\n";
		
		$this->pi =  4 * atan2(1, 1);
		$this->pi2 = 2 * $this->pi;
		$this->DR = $this->pi2 / 360;


		$this->init_projection($info);
	
	}
	
	
	Function init_projection(&$info) {
	
		if (isset($info['left meters'])) { $info['lm'] = $info['left meters']; }
		elseif (!isset($info['lm'])) { $info['lm'] = 0;}

		if (isset($info['top meters'])) { $info['tm'] = $info['top meters']; }
		elseif (!isset($info['tm'])) { $info['tm'] = 0;}

		if (isset($info['right meters'])) { $info['rm'] = $info['right meters']; }
		elseif (!isset($info['rm'])) { $info['rm'] = 0;}

		if (isset($info['bottom meters'])) { $info['bm'] = $info['bottom meters']; }
		elseif (!isset($info['bm'])) { $info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		if (isset($info['sp1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['sp1']); }
		elseif (isset($info['standard parallel 1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['standard parallel 1']); }
		else { $slat1 = $this->info['slat1'] = 0;}

		if (isset($info['sp2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['sp2']); }
		elseif (isset($info['standard parallel 2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['standard parallel 2']); }
		else { $slat2 = $this->info['slat2'] = 0;}

		if (isset($info['cm'])) { $cm = $this->info['cm'] = deg2rad($info['cm']); }
		elseif (isset($info['central meridian'])) { $cm = $this->info['cm'] = deg2rad($info['central meridian']); }
		elseif (isset($info['rlon'])) { $cm = $this->info['cm'] = deg2rad($info['rlon']); }
		else { $cm = $this->info['cm'] = 0;}

		if (isset($info['rlat'])) { $rlat = $this->info['rlat'] = deg2rad($info['rlat']); }
		elseif (isset($info['reference latitude'])) { $rlat = $this->info['rlat'] = deg2rad($info['reference latitude']); }
		else { $rlat = $this->info['rlat'] = 0;}

		if (isset($info['semi-major axis'])) { $a = $this->info['a'] = $info['semi-major axis']; }
		else { $a = $this->info['a'] = 6378206.4000000004;}

		if (isset($info['flattening'])) { $f = $this->info['f'] = $info['flattening']; }
		else { $f = $this->info['f'] = (1/294.97870);}

		if (isset($info['rho'])) { $rho = $this->info['rho'] = $info['rho']; }
		else { $rho = $this->info['rho'] = 63710000;}

		$this->info['lm'] = abs($info['lm']);
		$this->info['rm'] = abs($info['rm']);
		$this->info['tm'] = abs($info['tm']);
		$this->info['bm'] = abs($info['bm']);

		//#Albers stuff here
		if (($info['tm'] > 0 && $info['bm'] > 0) || ($info['tm'] < 0 && $info['bm'] < 0)) {
			$yscale = $this->info['yscale'] = (abs($this->info['tm'] - $this->info['bm']))/$this->info['height'];
		}
		else {
			$yscale = $this->info['yscale'] = (($this->info['tm']) + ($this->info['bm']))/$this->info['height'];
		}

		if (($info['lm'] > 0 && $info['rm'] > 0) || ($info['lm'] < 0 && $info['rm'] < 0)) {
			$xscale = $this->info['xscale'] = (abs($this->info['lm'] - $this->info['rm']))/$this->info['width'];
		}
		else {
			$xscale = $this->info['xscale'] = (($this->info['lm']) + ($this->info['rm']))/$this->info['width'];
		}
		
		if ($this->debug) { print "(xscale,yscale) = ($xscale,$yscale)<br>\n";}
		
		$this->info['ww'] = $this->info['lm']/$xscale;
		$this->info['hh'] = $this->info['tm']/$yscale;

		return 1;
	
	}
	
	Function get_xy($lon,$lat) {
		
		if ($this->debug) { print "(lon,lat) = ($lon,$lat)<br>\n";}
		$lon = deg2rad($lon);
		$lat = deg2rad($lat);
		
		if ($this->debug) { print "(radlon,radlat) = ($lon,$lat)<br>\n";}
		
		$a = $this->info['a'];
		//$r0 = $this->info['r0'];
		$cm = $this->info['cm'];
		$rlat = $this->info['rlat'];
		$lm = $this->info['lm'];
		$tm = $this->info['tm'];
		
		if ($this->debug) { print "(a,rlat,cm) = ($a, $rlat, $cm)<br>\n"; }
		
		$cosC = (sin($rlat) * sin($lat)) + (cos($rlat) * cos($lat) * cos($lon-$cm));
		if ($this->debug) { print "cosC = $cosC<br>\n"; }
		if ($cosC >= 0) {
			$x_ns = $a * cos($lat) * sin($lon-$cm);
			$y_ns = $a * (cos($rlat) * sin($lat) - sin($rlat) * cos($lat) * cos($lon-$cm));
			$x = $this->xscale($x_ns);
			$y = $this->yscale($y_ns);
		}
		else {
			$x = $y = null;
		}
		
		$x = floor($x);
		$y = floor($y);
		
		if ($this->debug) { print "(x,y) = ($x,$y)<br>\n";}
		
		return array($x,$y);
		
	}
	
	Function get_lonlat($x,$y) {
	
		if (isset($x) && isset($y)) {
			
			$lm = $this->info['lm'];
			$tm = $this->info['tm'];
			
			$xscale = $this->info['xscale'];
			$yscale = $this->info['yscale'];
			
			$a = $this->info['a'];
			$cm = $this->info['cm'];
			//$r0 = $this->info['r0'];
			$rlat = $this->info['rlat'];
			
			$x_ns = ($x * $xscale - $lm);
			$y_ns = ($y * $yscael - $tm) * -1;
			
			$r = sqrt(pow($x_ns,2) + pow($y_ns,2));
			
			$sinC = $r/$a;
			$cosC = sqrt(1 - pow($sinC,2));
			$lat = asin($cosC * sin($rlat) + ($y_ns * $sinC * cos($rlat)/$r));
			
			if ($rlat == $this->pi/2) {
				$lon = $cm + atan2($x_ns,-$y_ns);
			}
			elseif ($rlat == $this->pi/-2) {
				$lon = $cm + atan2($x_ns,$y_ns);
			}
			else {
				$lon = $cm + atan2(($x_ns * $sinC), ($r * cos($rlat) * $cosC) - ($y_ns * sin($rlat) * $sinC));
			}
			
			$lon = rad2deg($this->normalize($lon));
			$lat = rad2deg($this->normalize($lat));
			
			return array($lon,$lat);
			
		}
	
	}
	
	function xscale($x_ns) {
		return ($this->info['lm'] + $x_ns)/$this->info['xscale'];
	}
	
	function yscale($y_ns) {
		return ($this->info['tm'] - $y_ns)/$this->info['yscale'];
	}
	
	function normalize($angle) {
		
		while ($angle > $this->pi) {
			$angle -= $this->pi2;
		}
		while ($angle < $this->pi * -1) {
			$angle += $this->pi2;
		}
		
		return $angle;
		
	}

}