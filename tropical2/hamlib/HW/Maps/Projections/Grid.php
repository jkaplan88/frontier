<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Grid.php,v 1.6 2007/09/13 21:08:54 wxfyhw Exp $
*
*/


if (!defined('pi2')) define ('pi2', 2 * pi());





class Grid {

	var $version = '$Id: Grid.php,v 1.6 2007/09/13 21:08:54 wxfyhw Exp $';
	var $debug=0;
	var $error='';
	var $info = array();



	Function Grid (&$info, &$args) {

		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;

		if ($debug) print "Grid version:" . $this->version . "<br>\n";

		$this->init_projection($info);
	}


	Function init_projection(&$info) {

		if (isset($info['lm'])) { $this->info['lm'] = $info['lm']; }
		elseif (isset($info['left meters'])) { $this->info['lm'] = $info['left meters']; }
		else { $this->info['lm'] = 0;}

		if (isset($info['tm'])) { $this->info['tm'] = $info['tm']; }
		elseif (isset($info['top meters'])) { $this->info['tm'] = $info['top meters']; }
		else { $this->info['tm'] = 0;}

		if (isset($info['rm'])) { $this->info['rm'] = $info['rm']; }
		elseif (isset($info['right meters'])) { $this->info['rm'] = $info['right meters']; }
		else { $this->info['rm'] = 0;}

		if (isset($info['bm'])) { $this->info['bm'] = $info['bm']; }
		elseif (isset($info['bottom meters'])) { $this->info['bm'] = $info['bottom meters']; }
		else { $this->info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		$lon1 = $this->info['lon1'] = $info['lon1'];
		$lat1 = $this->info['lat1'] = $info['lat1'];
		$lon2 = $this->info['lon2'] = $info['lon2'];
		$lat2 = $this->info['lat2'] = $info['lat2'];
//print "($lon1,$lat1)...($lon2,$lat2)<br>\n";
		if (($lon1 >= 0 && $lon2 >=0) || ( $lon1 <=0 && $lon2 <=0)) {
			$this->info['xscale'] = abs(abs($lon1) - abs($lon2))/$this->info['width'];
		}
		elseif ($lon1 <0 && $lon2 >=0) {
			$this->info['xscale'] = abs(abs($lon1) + abs($lon2))/$this->info['width'];
		}
		else {
			$this->info['xscale'] = abs(abs(180-$lon1) + abs($lon2))/$this->info['width'];
		}

		if (($lat1 >= 0 && $lat2 >=0) || ($lat1 <=0 && $lat2 <=0)) {
			$this->info['yscale'] = abs(abs($lat1) - abs($lat2))/$this->info['height'];
		}
		else  {
			$this->info['yscale'] = abs(abs($lat1) + abs($lat2))/$this->info['height'];
		}

		return 1;
	}

	Function get_lonlat($x,$y) {

		$lon = ($x * $this->info['xscale']) + $this->info['lon1'];
		$lat = $this->info['lat1'] - ($y * $this->info['yscale']);

		return array($lon,$lat);
	}



	Function get_xy ($lon, $lat) {

		$xloc=$yloc=0;
		$lon1 = $this->info['lon1'];
		$lat1 = $this->info['lat1'];
		$lon2 = $this->info['lon2'];
		$lat2 = $this->info['lat2'];
//	print "(lon,lat) = ($lon,$lat)<br>\n";
		/*
		# if both are same sign then we take difference & devide by scale
		# if lon1 < 0 yet lon >= 0 then we add the absolute values
		# to get the total difference then divide by scale
		*/

		if (($lon1 >= 0 && $lon2 >=0)) {
			$xloc = round(abs(abs($lon1) - abs($lon))/$this->info['xscale']);
		}
		elseif ( ( $lon1 <=0 && $lon2 <=0)) {
			$xloc = round((abs($lon1) - abs($lon))/$this->info['xscale']);
		}
		elseif ($lon1 < 0 && $lon2 >= 0) {

			if ($lon < 0) {
				$xloc = round((abs($lon1) - abs($lon))/$this->info['xscale']);
			}
			else {
				$xloc = round((abs($lon1) + abs($lon))/$this->info['xscale']);
			}
		}
		else {
			$xloc = round((abs(180-$lon1) + abs($lon))/$this->info['xscale']);
		}

		if (($lat1 >= 0 && $lat2 >=0)) {
			$yloc = round((abs($lat1) - abs($lat))/$this->info['yscale']);
		}
		elseif (($lat1 <=0 && $lat2 <=0)) {
			$yloc = round(abs(abs($lat1) - abs($lat))/$this->info['yscale']);
		}
		elseif ($lat1 >= 0 && $lat2 < 0) {
			if ($lat> 0) {
				$yloc = round((abs($lat1) - abs($lat))/$this->info['yscale']);
			}
			else {
				$yloc = round((abs($lat1) + abs($lat))/$this->info['yscale']);
			}
		}


      	return array($xloc, $yloc);


	}



}
?>