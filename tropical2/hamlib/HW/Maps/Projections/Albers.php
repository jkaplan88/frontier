<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Albers.php,v 1.3 2004/04/15 12:03:08 lee Exp $
*
*/


if (!defined('pi2')) define ('pi2', 2 * pi());





class Albers {

	var $version = '$Id: Albers.php,v 1.3 2004/04/15 12:03:08 lee Exp $';
	var $debug=0;
	var $error='';
	var $info = array();

	var $pi = 0;
	var $pi2 = 0;
	var $DR = 0;



	Function Albers (&$info, &$args) {

		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;

		if ($debug) print "Albers version:" . $this->version . "<br>\n";

		$this->pi =  4 * atan2(1, 1);
		$this->pi2 = 2 * $this->pi;
		$this->DR = $this->pi2 / 360;


		$this->init_projection($info);
	}



	Function init_projection(&$info) {

		if (isset($info['left meters'])) { $info['lm'] = $info['left meters']; }
		elseif (!isset($info['lm'])) { $info['lm'] = 0;}

		if (isset($info['top meters'])) { $info['tm'] = $info['top meters']; }
		elseif (!isset($info['tm'])) { $info['tm'] = 0;}

		if (isset($info['right meters'])) { $info['rm'] = $info['right meters']; }
		elseif (!isset($info['rm'])) { $info['rm'] = 0;}

		if (isset($info['bottom meters'])) { $info['bm'] = $info['bottom meters']; }
		elseif (!isset($info['bm'])) { $info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		if (isset($info['sp1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['sp1']); }
		elseif (isset($info['standard parallel 1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['standard parallel 1']); }
		else { $slat1 = $this->info['slat1'] = 0;}

		if (isset($info['sp2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['sp2']); }
		elseif (isset($info['standard parallel 2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['standard parallel 2']); }
		else { $slat2 = $this->info['slat2'] = 0;}

		if (isset($info['cm'])) { $cm = $this->info['cm'] = deg2rad($info['cm']); }
		elseif (isset($info['central meridian'])) { $cm = $this->info['cm'] = deg2rad($info['central meridian']); }
		else { $cm = $this->info['cm'] = 0;}

		if (isset($info['rlat'])) { $rlat = $this->info['rlat'] = deg2rad($info['rlat']); }
		elseif (isset($info['reference latitude'])) { $rlat = $this->info['rlat'] = deg2rad($info['reference latitude']); }
		else { $rlat = $this->info['rlat'] = 0;}

		if (isset($info['semi-major axis'])) { $a = $this->info['a'] = $info['semi-major axis']; }
		else { $a = $this->info['a'] = 6378206.4000000004;}

		if (isset($info['flattening'])) { $f = $this->info['f'] = $info['flattening']; }
		else { $f = $this->info['f'] = (1/294.97870);}

		if (isset($info['rho'])) { $rho = $this->info['rho'] = $info['rho']; }
		else { $rho = $this->info['rho'] = 63710000;}

		$this->info['lm'] = abs($info['lm']);
		$this->info['rm'] = abs($info['rm']);
		$this->info['tm'] = abs($info['tm']);
		$this->info['bm'] = abs($info['bm']);

		//#Albers stuff here
		if (($info['tm'] > 0 && $info['bm'] > 0) || ($info['tm'] < 0 && $info['bm'] < 0)) {
			$yscale = $this->info['yscale'] = (abs($this->info['tm'] - $this->info['bm']))/$this->info['height'];
		}
		else {
			$yscale = $this->info['yscale'] = (($this->info['tm']) + ($this->info['bm']))/$this->info['height'];
		}

		if (($info['lm'] > 0 && $info['rm'] > 0) || ($info['lm'] < 0 && $info['rm'] < 0)) {
			$xscale = $this->info['xscale'] = (abs($this->info['lm'] - $this->info['rm']))/$this->info['width'];
		}
		else {
			$xscale = $this->info['xscale'] = (($this->info['lm']) + ($this->info['rm']))/$this->info['width'];
		}
if ($this->debug) { print "(xscale,yscale) = ($xscale,$yscale)<br>\n";}
		$this->info['ww'] = $this->info['lm']/$xscale;
		$this->info['hh'] = $this->info['tm']/$yscale;

		//More strict Albers Stuff
		list ($m1,$q1, $j1,$j2) = $this->_fetch_albers_constants($slat1);
		list ($m2,$q2, $j1,$j2) = $this->_fetch_albers_constants($slat2);

		$n = $this->info['n'] = (pow($m1,2) - pow($m2,2))/($q1 - $q2);
		$c = $this->info['c'] = pow($m1,2) - $n * $q1;
		list ($m0,$q0, $j1,$j2) = $this->_fetch_albers_constants($rlat);
		$r0 = $this->info['r0'] = ($a/$n) * sqrt($n*$q0+$c);



		return 1;
	}




	Function get_xy ($lon,$lat) {

	if ($this->debug) { print "(lon,lat) = ($lon,$lat)<br>\n";}
		$lon = deg2rad($lon);
		$lat = deg2rad($lat);

		$a = $this->info['a'];
		$n = $this->info['n'];
		$c = $this->info['c'];
		$cm = $this->info['cm'];
		$r0 = $this->info['r0'];

if ($this->debug) { print "(a,n,c,cm,r0) = ($a, $n, $c, $cm, $r0)<br>\n"; }
		list ($m, $q, $v, $junk) = $this->_fetch_albers_constants($lat);
if ($this->debug) { print "(m,q,v) = ($m, $q, $v)<br>\n"; }

		$theta = $n * ($lon - $cm);
		$r = ($a/$n) * sqrt($n*$q+$c);

		$x_ns = ($r * sin($theta)) ;
		$y_ns = ($r * cos($theta) - $r0) ;
if ($this->debug) { print "(x_ns,y_ns) = ($x_ns,$y_ns)<br>\n";}

		$lm = $this->info['lm'];
		$tm = $this->info['tm'];
if ($this->debug) { print "(lm,tm) = ($lm,$tm)<br>\n";}
		$xm=($lat < $cm) ? -1+($lm-$x_ns)/$lm : 1-($lm-$x_ns)/$lm;

		$ym = ($lat < $this->info['rlat']) ? 1-($tm-$y_ns)/$tm : -1+($tm-$y_ns)/$tm;
if ($this->debug) { print "(xm,ym) = ($xm,$ym)<br>\n";}
		$x = ($lm+$x_ns)/$this->info['xscale']+$xm ;
		$y = ($tm-$y_ns)/$this->info['yscale']+$ym ;


		$x = floor($x);
		$y = floor($y);
		return array($x,$y);

	}

	Function _fetch_albers_constants ($lat) {

	   $sin_lat = sin($lat);
	   $sin2_lat = $this->sin2($lat);
	   $a = $this->info['a'];
	   $f = $this->info['f'];
if ($this->debug) { print "(lat,sin_lat,sin2_lat,a,f) = ($lat, $sin_lat, $sin2_lat, $a, $f)<br>\n"; }

	   $esq = $f * (2-$f);
	   $e = sqrt($esq);
	   $v = $a / sqrt(1 - $esq*$sin2_lat);
	   $q = (1-$esq) * (($sin_lat/(1-$esq*$sin2_lat)) + (1/(2*$e)) * log((1 + $e*$sin_lat)/(1-$e*$sin_lat)));
	   $m = ($v*cos($lat))/$a;


	   return array($m, $q, $v, $esq);

	}

	Function cos2 ($num) {
		$cos = cos($num);
		return ($cos * $cos);
	}


	Function sin2 ($num) {
		$sin = sin($num);
		return ($sin * $sin);
	}





}
?>