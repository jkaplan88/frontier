<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Params.php,v 1.2 2007/09/13 21:08:54 wxfyhw Exp $
*
*/


class HWmapParams {

	var $version = '$Id: Params.php,v 1.2 2007/09/13 21:08:54 wxfyhw Exp $';
	var $debug=0;
	var $error='';

	var $map_info=array();

	Function HWmapParams ( $debug=0) {

		$this->debug = $debug;

		if ($debug) print "HWmapParams version:" . $this->version . "<br>\n";
	}

	Function error() {
		return $this->error;
	}

	Function get_map_data() {
		$args = func_get_args();

		$data = arrray();
		if (isset($args[0]) && strtoupper($args[0]) == 'ALL') {
			$keys = array_keys($this->map_info);
			foreach ($keys as $key) { array_push ($data, $this->map_info[$key]);}
		}
		else {
			foreach ($args as $key) { array_push ($data, $args[$key]);}
		}

		return $data;
	}

	Function set_map_data(&$set) {
		if (is_array($set)) {
			foreach ($set as $key) { $this->map_info[$key] = $set[$key];}
		}
	}


	Function load_map_info(&$map_meta_file	) {

		$map_info = array();
		$this->error='';

		$fp = fopen($map_meta_file, "rb");
		if ($fp) {
			$buffer = fread($fp, filesize($map_meta_file));
			fclose($fp);
			$lines = preg_split("/[\r\n]+/", $buffer);

			foreach ($lines as $line) {
				$line = trim($line);
				if ($line) {
					$parts = explode('=', $line,2);
					if (count($parts) == 2) {$val=trim($parts[1]);}
						else {$val='';}
					$key =trim($parts[0]);
					if ($key) { $map_info[$key] =$val;}
				}
			}

			$this->map_info =& $map_info;
			return $map_info;
		}
		else {
			$this->map_info=array();
			return null;
		}
	}









}
?>