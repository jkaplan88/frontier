<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Projection.php,v 1.3 2007/09/20 23:08:04 wxfyhw Exp $
*
*/


Function new_projection () {
	$args = func_get_args();
	$info = array_shift($args);



	if (isset($info['projection'])) {
		$projection = ucfirst(strtolower(trim($info['projection'])));
		if (!$projection) $projection = 'Grid';
	}
	else { $projection = 'Grid';}

	require_once(HAMLIB_PATH .'HW/Maps/Projections/'.$projection.'.php');

	$pro =  new $projection ($info,$args);

	return $pro;
}

?>