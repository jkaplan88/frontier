<?php
/*

%%LOAD_CONFIG=tropical_plot_common%%
%%LOAD_CONFIG=troplegend%%

[Legend Settings]
add_legend=1

add_method=image
legend_image=tropinvest_legend_640x480.png
legend_x=0
legend_y=0




[infobox settings]
add_infobox=1
bgimage=infobox_hrz4.png

y=58

[infobox labels]
position=%%lat%%%%latdir%%, %%lon%%%%londir%%|57|22|left|bottom
moving=
winds=%%windmph%%mph / %%windkts%%kts|178|22|left|bottom
pressure=%%pressuremb%%mb / %%pressurein%%in|313|18|left|bottom

*/
?>