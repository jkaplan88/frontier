<?php
/*

; load the image pluing api configs, which we can then
; override. This allows us to only change items we need to
; while allowing the end user to keep font sizes, credits
; set in a single config file.
%%LOAD_CONFIG=imagepluginapi%%


[SystemSettings]
#remove the pound sign to make the plug-in redirect to the created image
# instead of outputting a page
redirect_to_image=0



[balloon settings]
perpline_pattern=1111111000
perpline_color=000000
perpline_antialias=1

outline_color=000000
outline_fillcolor=FFFFFF
crossover_catch=1

radial_check=2


[Image Settings]

;cache_max_age=999999999999

# for flat fmaps, gdext =png for topo maps gdext=jpg
gdext=png


overlay_gdext=png

# used for jpeg compression 0-100 (100 eqals best quality)
# can be used for png compression  0-9 ()= best quality)
#save_quality=90

trop_tz=-5
trop_tzname=EST

balloon_outline_mode=0
balloon_perpline_mode=<? ('%%zoom%%' > 0) ? 2 : 0 ?>
balloon_bubble_mode=-1


# ctype can be circle or square
track_type=<? ('%%lo%%') ? 0 : 'circle' ?>
track_width=8
track_filled=1
track_lines=1

#forecast_type=balloon,circle,symbol
forecast_type=<? ('%%zoom%%' > 0) ? 'balloon,symbol' : 'balloon,circle' ?>
forecast_width=8
forecast_filled=1
#forecast_lines=dashed:1111111000
forecast_lines=1
forecast_balloon_blend=50

plot_current_position=1
plot_current_position_name=<? ('%%pn%%') ? 1 : 0 ?>
plot_info_box=<? ('%%ib%%' == '' || '%%ib%%') ? 1 : 0 ?>

; method of indicating current position:
; can be "symbol", "circle" or "square"
pcl_method=symbol

; if pcl_method = circle or square then the following are used
;pcl_color=FF0000
;pcl_line_color=000000
pcl_filled=1
pcl_width=10
pcl_height=10


windfield_filled=1
windfield_blend=80

seas_filled=1
seas_blend=100


truecolor=1
true_color=1

overlay=0


# sets the default method to handle auto map selection
# and zooming. set to AUTO to use the large maps
# set to AUTO2 to use the cropped large maps. this uses less
# resources and will work on many shared servers, but greatly
# increases the number of maps required.
auto_map_select=AUTO2


autocrop=1
autocrop_width=640
autocrop_height=480

autocrop_resample=1
autocrop_padding=30
autocrop_padding_top=80


; the storms current position (or last known position)
; is the sweet spot.. with auto crop, if the sweet area (all the storms points)
; is larger than the auto crop area, we will need to make sure we include the
; sweet spot. You can set the x & y offsets to a numeric value or
; to the word "center" to center the sweetspot inthe autocrop
sweetspot_xoffset=center
sweetspot_yoffset=center



tropsymbols_path=%%INI:Paths:html_side%%/images/tropsymbols

; lets put the tropical maps in a set directory to keep things clean
blank_map_path=%%INI:Image Settings:blank_map_path%%/tropical

; save map directory
; lets not use the standard hw3image dir so we can have unlimited caching
; with out the images being deleted. this will improve speed
save_map_path=%%INI:Paths:html_side%%/images/tropical
save_map_url=%%INI:Paths:html_side_url%%/images/tropical


no_active_label=No Active Storms


[model settings]
; defines is the model /lat/lons are used to calc the min maxs of the autocropbox
use_minmax=1

; if model_lines is non 0 then lines will be drawn from forecast poitn to point
model_lines=1
; if use_splines is non 0 then splines will be use when drawing the forecast lines
;	this will provide smoother curving lines etc
use_splines=1

; model_type controls what to plot at each point. can be blank, circle or square
model_type=circle
; if model_type is circle or square, then model_filled controls if its a filled circle or square
model_filled=1
; if model_type is circle or square, then model_width controls the size in pixels of the circle or square
model_width=6


; time options when plotting
;
; skip_prior_hours: if none 0 then means do not plot model points
; 	that occur prior to the time of the current positions.
;	this is usefule if you want to not show forecast points that have passed
skip_prior_hours=1

; some models have -hours, so setting min_hour controls the hour of the model forecast to use
min_hour= 0
; a few models may go out 144-168 days, you can set the max_hour to limit this.
max_hour= 168

; Set use_models to the model names to include in the map
use_models=OFCL,AVNO,BAMM,CMC,EGRR,GFDL,NGPS,LBAR,HWRF,BAMD,BAMS,CCON,CONE,UKM

; if set to 1 then you can pass use_models in form
; if set to 1 a user can pass use_models=BAMM,OFCL
; 	and it will override the INI use_models setting
allow_use_models=1



[model Colors]
# line_color is the default line color to use if non provided
line_color=000000

#set a color for the model forecast plots
OFCL=eaeaea
AVNO=ff0000
BAMM=00e1ff
EGRR=fff800
CMC=b400ff
GFDL=90ff00
NGPS=ffa300
LBAR=8ADFBA

[tropsystem Symbols]
I=s3dglow_invest.png
ET=s3dglow_td.png
TD=s3dglow_td.png
TS=s3dglow_ts.png
H1=s3dglow_hurr1.png
H2=s3dglow_hurr2.png
H3=s3dglow_hurr3.png
H4=s3dglow_hurr4.png
H5=s3dglow_hurr5.png



[tropsystem Symbols Text]
# you can specify text to be placed inthe center of the tropsystem symbol
# this is useful to place the hurrican category number
# set to blank or comment out to not have Text
TD=
TS=
;H1=1
;H2=2
;H3=3
;H4=4
;H5=5


[tropsystem Symbols Text settings]
xoffset=-1
yoffset=0

TTF_path=%%INI:Paths:TTF_path%%
TTF_font=Timeless.ttf
TTF_font_pt=9
TTF_font_angle=0
gdfont=SMALL
font_color=FFFFFF




[tropsystem forecast Symbols]
I=s3d_invest.png
ET=s3d_td.png
TD=s3d_td.png
TS=s3d_ts.png
H1=s3d_hurr1.png
H2=s3d_hurr2.png
H3=s3d_hurr3.png
H4=s3d_hurr4.png
H5=s3d_hurr5.png


[tropsystem forecast Symbols Text]
# you can specify text to be placed inthe center of the tropsystem symbol
# this is useful to place the hurrican category number
# set to blank or comment out to not have Text
TD=
TS=
;H1=1
;H2=2
;H3=3
;H4=4
;H5=5


[tropsystem forecast Symbols Text settings]
xoffset=0
yoffset=0

TTF_path=%%INI:Paths:TTF_path%%
TTF_font=Timeless.ttf
TTF_font_pt=9
TTF_font_angle=0
gdfont=SMALL
font_color=FFFFFF



[tropsystem Name Text settings]
xoffset=0
yoffset=0
halign=center
valign=top
TTF_path=%%INI:Paths:TTF_path%%
TTF_font=Timeless.ttf
TTF_font_pt=9
TTF_font_angle=0
gdfont=SMALL
font_color=FFFFFF



[tropsystem Colors]
; Invest color
I=9933CC

;Extra Tropical Color - If uncommented and color set then in forecast if
;	NOAA states "extratropical" this color will be used and color based on Wind ignored
;ET=FF00FF

; Tropical Depression Color
TD=10F1FF

; Tropical Storm color
TS=7Bff00

; Hurricant colors for cats 1 -5
H1=FFFF00
H2=FEAE00
H3=FF0000
H4=FE00FE
H5=FFCCFF
line_color=000000

#forecast_line_color = 000000
marker_color=FFFFFF

[windfield Colors]
34=CD8500
50=
64=FF0000

[seas Colors]
12=FFFF00




[tropsystem automaps]
nt_zoom0=atlantic_merc_640x480
nt_zoom1=atlantic_merc_1440x1080
nt_zoom2=atlantic_merc_2560x1920
nt_zoom3=atlantic_merc_5120x3840
nt_zoom4=atlantic_merc_7040x5280

pz_zoom0=tropics_merc_640x480
pz_zoom1=tropics_merc_1440x1080
pz_zoom2=tropics_merc_2560x1920
pz_zoom3=tropics_merc_5120x3840
pz_zoom4=tropics_merc_7040x5280

pa_zoom0=pacific_merc_640x480
pa_zoom1=pacific_merc_1440x1080
pa_zoom2=pacific_merc_2560x1920
pa_zoom3=pacific_merc_5120x3840
pa_zoom4=pacific_merc_7040x5280

pa_pz_zoom0=pacific_merc_640x480
pa_pz_zoom1=pacific_merc_1440x1080
pa_pz_zoom2=pacific_merc_2560x1920
pa_pz_zoom3=pacific_merc_5120x3840
pa_pz_zoom4=pacific_merc_7040x5280

default_zoom0=tropics_merc_640x480
default_zoom1=tropics_merc_1440x1080
default_zoom2=tropics_merc_2560x1920
default_zoom3=tropics_merc_5120x3840
default_zoom4=tropics_merc_7040x5280



[infobox settings]
add_infobox=1

; the height & width are used only when x/y position is set to "auto"
width=550
height=38


bgimage_path=%%INI:Paths:html_side%%/images/legends/tropical
bgimage=infobox_hrz3.png
x=center
y=78
bgimage_halign=center
bgimage_valign=top



TTF_path=%%INI:Paths:TTF_path%%
TTF_font=Timeless.ttf
TTF_font_pt=8
TTF_font_angle=0
gdfont=LARGE
font_color=000000
dropshadow_xoffset=0
dropshadow_yoffset=0
dropshadow_color=000000

[infobox labels]
position=%%lat%%%%latdir%%, %%lon%%%%londir%%|57|22|left|bottom
moving=%%direction%% %%speedmph%%mph / %%speedkts%%kts|184|22|left|bottom
winds=%%windmph%%mph / %%windkts%%kts|323|22|left|bottom
pressure=%%pressuremb%%mb / %%pressurein%%in|457|18|left|bottom



[Legend Settings]
add_legend=0
legend_path=%%INI:Paths:html_side%%/images/legends/tropical


[Title Settings]
add_title=1
title_text=<? $this->storm_name ?> Storm Track
truecolor=1
height=30
bg_color=101A66
bg_blend_level=90

title_TTF_font=Timeless.ttf
title_text_y=21
title_TTF_font_pt=12

credit_TTF_font=Timeless.ttf
credit_TTF_font_pt=11
credit_text_y=21



[Label Settings]
TTF_font_pt=10


[Labels]
issue_date=<? (date('I')) ? gmdate('D M j Y h:i A', time() - 4 * 3600) . ' EDT' : gmdate('D M j Y h:i A', time() - 5 * 3600) . ' EST' ?>|10|bottom|left|bottom



[marker settings]

TTF_path=%%INI:Paths:TTF_path%%
TTF_font=Timeless.ttf
TTF_font_pt=10
TTF_font_angle=0
gdfont=LARGE
font_color=FFFFFF
dropshadow_xoffset=1
dropshadow_yoffset=1
dropshadow_color=000000


[Watermarks]
#hw=hwwmark_png24_640x480_a35.png|right|bottom|||100|1
#hw=HW_watermark_tiled_640x480.png|0|0|left|top|100|1

[HWI Sections]
# we need to add a cach directory where we will be saving our imap files to include in the output
IMAP=%%INI:Paths:html_side%%/images/tropical



*/
?>