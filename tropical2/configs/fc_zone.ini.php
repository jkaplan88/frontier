<?php
/*
%%LOAD_CONFIG=wxinfo%%
%%LOAD_CONFIG=fc_tafzone%%



[URLs]

zone_url=weather.noaa.gov|www.weather.gov
zone_prefix=/pub/data/forecasts/zone/%%find_state%%/%%find_state%%z%%find_place%%.txt|/data/%%cwa%%/ZFP%%cwa%%
zone_postfix=

zone_cache_file=%%cache_path%%/%%country%%-%%find_state%%-%%find_place%%-zone.txt|%%cache_path%%/%%country%%-%%find_state%%-%%iwin_file%%
zone_cache_file2=


ca_zone_url=www.weatheroffice.gc.ca
ca_zone_prefix=/forecast/textforecast_e.html?Bulletin=




[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
zone=zone.html,H10:H16:H22:H3:30,zone.html,FetchZandH,do_zone,1
#zone=zone.html,120,zone.html,FetchZandH,zone,1

*/
?>