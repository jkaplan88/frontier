<?php

/*

[Legend Settings]
add_legend=1

add_method=image
legend_image=troptypes_legend_640x480.png
legend_x=0
legend_y=0




add_labels=label,labelhur,labeltrop

no_gradient=1

legend_order=TD,TS,H1,H2,H3,H4,H5
label_what=('%%INI:Title Settings:title_small%%') ? ( ($items = array('TD' => 'TD', 'TS'=> 'TS', 'H1' => 'Cat 1', 'H2' => 'Cat 2', 'H3' => 'Cat 3', 'H4' => 'Cat 4', 'H5' => 'Cat 5')) ? $items["$i"] : '')  : ( ($items=array('TD' => 'Depression', 'TS'=> 'Storm', 'H1' => 'Cat 1', 'H2' => 'Cat 2', 'H3' => 'Cat 3', 'H4' => 'Cat 4', 'H5' => 'Cat 5')) ? $items["$i"] : '')
label_xoffset=36
label_yoffset=0

#legend_y=38
legend_size=500


labelhur_start=1
labelhur_stop=1
labelhur_interval=1
labelhur_where=375;
labelhur_when=(1)
labelhur_what='Hurricane'
labelhur_xoffset=16
labelhur_yoffset= -23
labelhur_TTF_font=Timeless.ttf
labelhur_font_color=FFFFFF
labelhur_TTF_font_pt=10
labelhur_gdfont=small
labelhur_dropshadow_xoffset=2
labelhur_dropshadow_yoffset=2
labelhur_dropshadow_color=000000


labeltrop_start=1
labeltrop_stop=1
labeltrop_interval=1
labeltrop_where=130;
labeltrop_when=(1)
labeltrop_what='Tropical'
labeltrop_xoffset=16
labeltrop_yoffset= -23
labeltrop_TTF_font=Timeless.ttf
labeltrop_font_color=FFFFFF
labeltrop_TTF_font_pt=10
labeltrop_gdfont=small
labeltrop_dropshadow_xoffset=2
labeltrop_dropshadow_yoffset=2
labeltrop_dropshadow_color=000000

*/
?>