<?php
/*

%%LOAD_CONFIG=fc_tropicalinfo%%

[URLs]
tropical_url=www.srh.noaa.gov|iwin.nws.noaa.gov|weather.noaa.gov
tropical_prefix=/data/%%regionoffice%%/TWS%%uc_regionabbrev%%|/pub/data/text/AB%%uc_region%%30/KNHC.TXT|/pub/data/raw/ab/ab%%lc_region%%30.knhc.tws.%%regionabbrev%%.txt
tropical_postfix=

tropical_cache_file=%%cache_path%%/AB%%region%%30_K%%regionoffice%%.txt

[ForecastTypes]
tropsummary=,20,tropoutlook.html,FetchTropicalInfo,trop_raw,0

*/
?>
