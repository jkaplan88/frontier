<?php
/*

tropical_url=iwin.nws.noaa.gov
tropical_prefix=/pub/data/text/wt%%region%%%%data_type%%%%event%%/KNHC.TXT
tropical_postfix=

tropical_cache_file=%%cache_path%%/WT%%region%%%%data_type%%%%event%%.txt



[HTML tropsystem Colors]

use_rowcolors=1
TD=10F1FF
TS=7Bff00
H1=FFFF00
H2=FEAE00
H3=FF0000
H4=FE00FE
H5=FFCCFF

[System Settings]

#hw or noaa maps for sats/sst
#if using noaa set both values to 0

use_hwsst=0
sst_mapsize=640x480

use_hwsat=0

# sat map size will control the size of the sats used.
# If using HW sats set to 800x600, 640x480, 480x360 or 320x240
sat_mapsize=640x480



# set max_zoom to the max zoom value to
# offer in the html output. Note that the plugin
# comes with maps for zooms 0-2 by default. There is a
# separate download of maps to support zooms 3 & 4
max_zoom=2

default_zoom=1
default_mapsize=640x480
adv_mapsize=640x480


[HWI Sections]
# we need to add a cach directory where we will be saving our imap files to include in the output
IMAP=%%INI:Paths:html_side%%/images/tropical

*/
?>
