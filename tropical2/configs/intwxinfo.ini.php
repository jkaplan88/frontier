<?php
/*
[IntWxInfo]

CL=Clear|sunny.gif|sunnyn.gif
FW=Partly Cloudy|pcloudy.gif|pcloudyn.gif
SC=Partly Cloudy|pcloudy.gif|pcloudyn.gif
BK=Mostly Cloudy|mcloudy.gif|mcloudyn.gif
OV=Overcast|cloudy.gif|cloudyn.gif

RA=Rain|rain.gif|rain.gif

RASH=Showers|pcloudyr.gif|pcloudyrn.gif
SN=Snow|snow.gif|snow.gif
SNSH=Snow Showers|snowshowers.gif|snowshowers.gif
RASN=Rain and Snow|rainandsnow.gif|rainandsnow.gif
FL=Flurries|flurries.gif|flurries.gif
FZ=Freezing Rain|freezingrain.gif|freezingrain.gif
FZDZ=Freezing Drizzle|freezingrain.gif|freezingrain.gif
SL=Sleet|sleet.gif|sleet.gif

; following mixutres can also potentially occur
FZSL=Sleet and Freezing Rain|sleet.gif|sleet.gif
FZRA=Rain and Freezing Rain|freezingrain.gif|freezingrain.gif
FZSLRA=Rain and Ice|freezingrain.gif|freezingrain.gif
FZSN=Ice and Snow|rainandsnow.gif|rainandsnow.gif
FZSLSN=Ice and Snow|rainandsnow.gif|rainandsnow.gif
SLRA=Rain and Sleet|sleet.gif|sleet.gif
SLSN=Sleet and Snow|rainandsnow.gif|rainandsnow.gif
MIX=Wintry Mix|rainandsnow.gif|rainandsnow.gif




FWRAT1=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
FWRAT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
FWRAT3=Chance of Severe T-Storm|chancetstorm.gif|chancetstormn.gif
FWRAT4=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif
FWRAT5=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif

FWRASHT1=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
FWRASHT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
FWRASHT3=Chance of Severe T-Storm|chancetstorm.gif|chancetstormn.gif
FWRASHT4=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif
FWRASHT5=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif


FWRA=Rain Possible|pcloudyr.gif|pcloudyrn.gif
FWVLRA=Isolated Shower Possible|pcloudyr.gif|pcloudyrn.gif
FWLRA=Light Rain Possible|pcloudyr.gif|pcloudyrn.gif


SCRAT1=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRAT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRAT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRAT3=Chance of Severe T-Storm|chancetstorm.gif|chancetstormn.gif
SCRAT4=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif
SCRAT5=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif

SCRASHT1=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRASHT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRASHT2=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
SCRASHT3=Chance of Severe T-Storm|chancetstorm.gif|chancetstormn.gif
SCRASHT4=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif
SCRASHT5=Severe T-Storms Likely|chancetstorm.gif|chancetstormn.gif



SCRA=Rain Possible|pcloudyr.gif|pcloudyrn.gif
SCVLRA=Isolated Shower Possible|pcloudyr.gif|pcloudyrn.gif
SCLRA=Light Rain Possible|pcloudyr.gif|pcloudyrn.gif


BKRAT1=Chance of T-Storm|tstorm.gif|tstormn.gif
BKRAT2=Chance of T-Storm|tstorm.gif|tstormn.gif
BKRAT3=Chance of Severe T-Storm|tstorm.gif|tstormn.gif
BKRAT4=Severe T-Storms Likely|tstorm.gif|tstormn.gif
BKRAT5=Severe T-Storms Likely|tstorm.gif|tstormn.gif

BKRASHT1=Chance of T-Storm|tstorm.gif|tstormn.gif
BKRASHT2=Chance of T-Storm|tstorm.gif|tstormn.gif
BKRASHT3=Chance of Severe T-Storm|tstorm.gif|tstormn.gif
BKRASHT4=Severe T-Storms Likely|tstorm.gif|tstormn.gif
BKRASHT5=Severe T-Storms Likely|tstorm.gif|tstormn.gif


BKRA=Occasional Showers|mcloudyr.gif|mcloudyrn.png
BKVLRA=Scattered Showers|mcloudyr.gif|mcloudyrn.png
BKLRA=Occasional Light Rain|mcloudyr.gif|mcloudyrn.png

OVRAT1=Cloudy - Chance of T-Storm|tstorm.gif|tstormn.gif
OVRAT2=Cloudy - Chance of T-Storm|tstorm.gif|tstormn.gif
OVRAT3=Cloudy - Chance of Severe T-Storm|tstorm.gif|tstormn.gif
OVRAT4=Cloudy - Severe T-Storms Likely|tstorm.gif|tstormn.gif
OVRAT5=Cloudy - Severe T-Storms Likely|tstorm.gif|tstormn.gif

OVRASHT1=Cloudy - Chance of T-Storm|tstorm.gif|tstormn.gif
OVRASHT2=Cloudy - Chance of T-Storm|tstorm.gif|tstormn.gif
OVRASHT3=Cloudy - Chance of Severe T-Storm|tstorm.gif|tstormn.gif
OVRASHT4=Cloudy - Severe T-Storms Likely|tstorm.gif|tstormn.gif
OVRASHT5=Cloudy - Severe T-Storms Likely|tstorm.gif|tstormn.gif


OVRA=Rain|rain.gif|rain.gif
OVVLRA=Cloudy - Light Shower Possible|rain.gif|rain.gif

FWRASH=Showers Possible|pcloudyr.gif|pcloudyrn.gif
SCRASH=Showers Possible|pcloudyr.gif|pcloudyrn.gif
BKRASH=Occasional Showers|mcloudyr.gif|pcloudyrn.gif

FWFL=Flurries Possible|pcloudys.gif|pcloudysfn.png
SCFL=Flurries Possible|pcloudys.gif|pcloudysfn.png
BKFL=Occasional Flurries|mcloudys.gif|mcloudysfn.png
OVFL=Cloudy with Flurries|flurries.gif|flurries.gif

FWSNSH=Flurries Possible|pcloudys.gif|pcloudysn.gif
SCSNSH=Flurries Possible|pcloudys.gif|pcloudysn.gif
BKSNSH=Occasional Flurries|mcloudys.gif|mcloudysn.gif


N/A=N/A|na.gif|na.gif

[WxIntensities]
VL=Very light
L=light
H=Heavy
VH=Very Heavy

[Replacements]
GOTTINGEN=Goettingen





*/

?>