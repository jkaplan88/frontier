<?php
/*


[Parse Plugins]
HWimage=HW::HW3image::HWimageParser|parse_line|1|1|0

[Defaults]
gd_ext=png

template_ext=hwi3

default_ttf=


[Paths]
hw3image_cache=%%INI:Paths:html_side%%/images/hw3image
hw3image_cache_url=%%INI:Paths:html_side_url%%/images/hw3image

hw3image_fcicons=%%INI:Paths:html_side%%/images/hwi_fcicons

hwimage_temp=%%INI:Paths:hw3image_cache%%


# the following is the file path to the directory where the True Type Fonts are stored
ttf_path=

[CleanCacheDirs]
clean_cache=%%INI:CleanCacheDirs:clean_cache%%,%%INI:Paths:hw3image_cache%%


[PassTemplates]
hwiplotter=hwiplotter.hwi3
test=test.hwi3
hw3image_demo=hw3image_demo.html

*/
?>