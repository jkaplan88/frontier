<?php
/*


[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
plotwindfieldforecast=,15,tropmap.html,MakeTropMap,main,0


%%LOAD_CONFIG=fc_plotwindfield%%

%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
plot_order=windfield_forecast,forecasts,current_position

plot_windfield_forecast=1
plot_current_position=1
plot_forecasts=1
plot_tracks=1


balloon_outline_mode=2
balloon_perpline_mode=0


forecast_type=circle
forecast_max_hours=84
marker_distance_forecast=<? ('%%zoom%%' > 2) ? 75 : 35 ?>


tropsymbols_path=<? ('%%zoom%%' > 2) ? '%%INI:Paths:html_side%%/images/tropsymbols' : '%%INI:Paths:html_side%%/images/tropsymbols/mini' ?>
pcl_method=symbol
pcl_color=
pcl_line_color=000000
pcl_filled=1
pcl_width=<? ('%%zoom%%' > 1) ? 10 : 6 ?>
pcl_height=<? ('%%zoom%%' > 1) ? 10 : 6 ?>


windfield_filled=0
windfield_blend=100



windfield_forecast_blend=100
windfield_forecast_interval=1800
windfield_forecast_filled=1


overlay=1


[windfield Colors]
34=FFC601
50=FF9701
64=FE0000

[tropsystem Colors]
forecast_line_color=000000

[infobox settings]
y=58

[Title Settings]
title_text=<? ('%%INI:Title Settings:title_small%%') ? preg_replace('/Tropical Depression/i', 'TD', $this->storm_name . ' Wind Field Forecast') : $this->storm_name . ' Wind Field Forecast' ?>



[tropsystem Name Text settings]
dropshadow_xoffset=1
dropshadow_yoffset=1
dropshadow_color=000000


[Legend Settings]
add_legend=1

add_method=image
legend_image=tropwindfore_legend_640x480.png
legend_x=0
legend_y=0





*/
?>