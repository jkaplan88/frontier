<?php
error_reporting  (E_ALL);
error_reporting(E_ALL);
ini_set('display_errors','1');
@set_magic_quotes_runtime(0);

$core_path = dirname(__FILE__) . '/';
$iniext = 'ini.php';
$force_windows=0;

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: hw3.php,v 1.43 2008/08/22 17:11:22 wxfyhw Exp $
*
*/

$HW3VERSION = "3.99";
$HW3BETA = '';

$HW3REVISION = '$Id: hw3.php,v 1.43 2008/08/22 17:11:22 wxfyhw Exp $';


if (!is_dir($core_path)) {$core_path = './';}
elseif ($core_path && !preg_match('/\/$/', $core_path)) {$core_path .= '/';}
define('CORE_PATH', $core_path);
define ('HAMLIB_PATH', CORE_PATH . 'hamlib/');

// Lets set the include path but we must use a semicolor for windows.
if (substr(PHP_OS, 0, 3) == 'WIN' || $force_windows) {
	ini_set("include_path",ini_get("include_path").";".$core_path);
}
else {
	ini_set("include_path",ini_get("include_path").":".$core_path);
}

$configs_path = $core_path . 'configs';

//Load in the class files
require (HAMLIB_PATH . "HW/common.php");
require (HAMLIB_PATH . "HW/Cookies.php");
require (HAMLIB_PATH . "HW/INIReader.php");
require (HAMLIB_PATH . "HW/HandleInput.php");
require (HAMLIB_PATH . "HW/Template.php");
require (HAMLIB_PATH . "HW/DBAccess.php");
require (HAMLIB_PATH . "HW/hw3tparser.php");
require (HAMLIB_PATH . "HW/PluginRunner.php");
require (HAMLIB_PATH . "HW/ParsePlace.php");

//Varaibles
$form = array();
$var = array();
$chips = array();
$forecast_hash = array();
$template_hashes = array();
$cookie_orig = "";
$settings = "SystemSettings";
$use_cookies = false;
$debug=0;

$var['base'] = $core_path;


//Load up the ini file
$cfg = new INIReader($configs_path, $iniext,$debug);
$cfg->ReadINI("hw3.$iniext");

//Set the use_cookies boolean to true if the ini file is set to use cookies
if ($cfg->Val($settings, "allow_cookies") && $cfg->Val($settings, "allow_cookies") != "false") $use_cookies = true;

parse_input($form, $debug);


//Get the cookies and place then in the var hash
if ($use_cookies) {
	$cookies = new Cookies();
} else { $cookies = ''; }


// check for bad referer before we go too far.
if ( $cfg->val('referrer', 'referrer_check')) {
	check_referer( $core_path . $cfg->val('referrer', 'referrer_file'),$cfg->val('referrer', 'referrer_url'),
				$cfg->val('referrer', 'referrer_debug'));
}



//Place all input into the $form hash
process_input($form, $var, $cfg, $cookies,$debug);
if ($use_cookies) $cookie_orig = $cookies->GetCookieString();

// handle debug
$debug = $cfg->val($settings, 'debug_mode');
if ($debug == 1 ) { $debug = (isset($form['debug'])) ? $form['debug'] : 0; }
elseif ($debug == 3) {
	if (isset($form['debugpw']) && $form['debugpw'] && isset($form['debug'])) {
		$debug = ($cfg->val($settings, 'debug_pw') == $form['debugpw'])  ? $form['debug'] : 0;

	}
	else {$debug=0;}
}


$cfg->Debug($debug);

if ($debug || isset($form['version'])) {
	print "HW3 version=$HW3VERSION ";
	if ($HW3BETA > 0) print " (Beta $HW3BETA)\n";
	print "<br>hw3.php cvs revision = $HW3REVISION<br>\n";
}

if ($debug) {

   $cfg->Debug($debug);
   print "<h2>Before loading configs</h2>\n";
   print_cfg($cfg);
   print_var('Form', $form);
   print_var('var', $var);
}
$var['allow_mp'] = (isset($form['allow_mp'])) ? $form['allow_mp'] : $cfg->val('Defaults', 'allow_multiple_places');


# Begin added 2007.05.29
# new ini capabilities.

# lets see if we need to set the user based on the domain
if ($cfg->Val('SystemSettings', 'use_sites')) {
	$cfg->ReadINI('sites');
	$host = (!empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : ( (!empty($_SERVER['SERVER_NAME'])) ? $_SERVER['SERVER_NAME'] : '');

	$tuser = ($host) ? preg_replace('/\W/','',$cfg->Val('DomainUsers',$host)) : '';
	if (!$tuser) $tuser = preg_replace('/\W/','',$cfg->Val('DomainUsers','default'));
	if ($tuser) {
		$form['users'] = $tuser;
		$var['usedDomain'] = 1;
	}
	else {
		$var['usedDomain'] = 0;
	}
}

# lets put the config files in order to load
$tconfigorder = preg_replace('/[^\w,]+/','',$cfg->Val('SystemSettings', 'config_order'));
if (!$tconfigorder) $tconfigorder = 'user,users,theme,config,forecast';

$config_order = explode(',',$tconfigorder);
$configs = '';
$nofc = getKeyVal($form, 'nofc',0);
$tforecast = $form['forecast'];
foreach ($config_order as $config_item) {
	if ($config_item == 'forecast') {
		if (!$nofc && $tforecast) {
			$configs .= 'fc_'.$tforecast . ',';
		}
	}
	else {
		$configs .= getKeyVal($form, $config_item, '') . ',';
	}
}

# now lets load the config files

// lets load any config files requested
$arry=explode(',', $configs);
reset($arry);
foreach ($arry as $name) {
	if ($debug) print "loading ini: $name<br/>";
	if ($name)      $cfg->ReadINI($name.'.'. $iniext);
}



# end added 2007.05.29

// commented below on 2007.05.29
//
//// lets load any config files requested
//$arry=explode(',', $form['config']);
//reset($arry);
//while (list(,$name) = each ($arry)) {
//   if ($name != '') {
//      $cfg->ReadINI($name.'.'. $iniext);
//   }
//}
//
//
//// Lets load the foreacst ini file unless the nofc param true
//if (!getKeyVal($form, 'nofc',0)) {
//   $tforecast = $form['forecast']; $tforecast = preg_replace('/\W/', '', $tforecast);
//   $cfg->ReadINI('fc_'.$tforecast.'.' . $iniext);
//}

define('HTTP_MODE', $cfg->val('SystemSettings', 'http_mode') +0);

// Do logging
if ($cfg->val('SystemSettings', 'logging')) {
	$logpath = $cfg->val('Paths', 'logs');
	if (!$logpath) $logpath = 'logs';
	$logmode = $cfg->val('SystemSettings', 'log_mode');
	if (!$logmode) $logmode = 0;
	log_hit($logpath,$logmode);

	# lets clean out the logs directory if we need to:
	# we will clean it once every 1440 minutes (1 day)
	$lma = $cfg->val('SystemSettings', 'log_file_max_age');
	if ($lma > 0) { clean_dirs(1440, $lma, array($logpath)); }
}


if ($debug) {
   print "<hr><br><h2>After loading configs</h2>\n";
   print_cfg($cfg);
   print_var('Form', $form);
   print_var('var', $var);

}


$scripturl = trim($cfg->val('Paths', 'scripturl'));
if ($scripturl) { $var['scripturl'] = $scripturl;}
else { $var['scripturl'] = '';}

// lets clean the cahe directory if need be
$cache_path =$cfg->val('Paths', 'cache');
if (!$cache_path) { $cache_path =  'cache'; }
if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) {
	$cache_path = $core_path . $cache_path;
}
$CleanCacheDirs = explode(',', $cfg->val('CleanCacheDirs', 'clean_cache'));
array_push($CleanCacheDirs, $cache_path);
clean_cache($CleanCacheDirs);


// db dir =
$dbdir = $cfg->val('Database Access', 'flatfilepath');
if ($dbdir =='') $dbdir = $core_path . 'hw3dbs/';
if (!preg_match('/^(?:\/|[A-Za-z]:)/', $dbdir) ) {
	$dbdir = $core_path . $dbdir;
}
$db_access = new_dbaccess($cfg->val('Database Access', 'type'), $cfg, $dbdir,$debug);
$place_parser= new ParsePlace($form,$var,$cfg,'', $debug);

$template_dir = $cfg->val('Paths', 'templates');
if ($template_dir == '') $template_dir = $core_path . 'templates';
$hwiext = $cfg->val('Defaults', 'template_ext');
if (!$hwiext) $hwiext = 'html';

$t = '';
$out = 1;
if (!isset($form["forecast"])) {
	$form["forecast"] = '';
}
//If forecast equals pass
if ($form["forecast"] == "pass") {
	$pass = "";
	if (isset($var['pass']) && $var['pass'] != '') {
		$pass = $var["pass"];
	}
	else {
		$pass = $cfg->Val("Defaults", "pass");
	}
	$t = $cfg->val("PassTemplates", $pass);
	$out = 1;
	$dppset = isset($form['dpp']);
	if (($dppset && $form['dpp'] != '0') ||  (!$dppset && $cfg->val('Defaults', 'dpp') != '0')) {
		//Perform Process Place
		$place_parser->process_place($db_access);
		if (isset($var["dopass"]) && isset($var["mult_places_pass"])) {
			$t = $cfg->Val("PassTemplates", $var["mult_places_pass"]);
		}
		elseif (!empty($var['BADPLACE']) && $cfg->val('SystemSettings', 'badplace_when_pass')) {
			$t = $cfg->val("Defaults", "bad_place_template");
		}
	}

	if (!$t && file_exists("$template_dir/$pass.$hwiext" )) $t = "$pass.$hwiext"  ;

}

// do we need to save to a file?
// if we are outputting to a file lets make sure we have valid info for file
// to write to.. other wise die with error
$savefile ='';
if (isset($form['om']) && $form['om'] > 0) {
	$of = (isset ($form['of'])) ?  preg_replace('/\W+/', '', $form['of']) : '';
	if ($of) {

		$savefile = $cfg->val('output files', $of);
		if (!$savefile) {
			$ofsm = $cfg->val('output files security', 'mode');
			$ofsd = $cfg->val('output files security', 'default path');
			if ($ofsm) {
				if ($ofsm == 2) {
					$ofp = (isset ($form['ofp'])) ?preg_replace('/\W+/', '', $form['ofp']) : '';
					if ($ofp) $ofp .='/';
					if (!is_dir($ofsd . $ofp) && !mkdir($ofsd . $ofp)) {
							$savefile ='';
						}
					else {
						$savefile = $ofsd . $ofp .$of . $cfg->val('output files security', 'default extension');
					}
				}
				elseif ($ofsm) {
					$savefile = $ofsd . $of . $cfg->val('output files security', 'default extension');
				}
			}
		}
	}
	else {
		$savefile = $cfg->val('Defaults', 'output file');
	}

	// if this is a relative path we need to add the default path to the front
	if ($savefile && !preg_match('/^(\/|[A-Za-z]:)/', $savefile)) {
		$savefile = $cfg->val('output files security', 'default path') . $savefile;
	}

	if ($form['om'] > 0 && $form['om'] < 3 && !$savefile) {
		exit ("Error: of parameter : $of does not make a valid output file");
	}
	elseif (is_dir($savefile)) {
		exit ("Error: of parameter : $of cannot be a directory");
	}
	elseif ($debug) {
		echo "savefile = $savefile<br>\n";
	}
}
else { $form['om']=0;}





if (!isset($var["allow_mp"])) {$var["allow_mp"] = "";}
$wx_object = "";
$pr = new PluginRunner($core_path,$form,$var,$cfg,'', $iniext, $debug, $place_parser, $db_access);
if ($form["forecast"] != "pass") {
	//print "debug=$debug<br>\n";
	//If the template is still unset, get the forecast info
	if ($t == '') {
		list($t, $wx_object) = $pr->RunHWPlugin($var["allow_mp"]);
	}
}


//Now set the cookie
if ($use_cookies && isset($form['sc'])) {
	$cookie_hashes = array(&$var, &$form,&$wx_object);
	$cookies->SetCookie( $form['sc'], $cookie_hashes,  $cfg->Val($settings, "cookie_name"), $cookie_orig, 0, $cfg->Val('SystemSettings', 'server_name'), $cfg->Val('SystemSettings', 'script_name'));
	unset($cookie_hashes);
}

//Display the template

$extra_parse = new hw3tparser($core_path, $cfg, $var, $form, $pr, $iniext, $debug);
$template = new Template($template_dir . '/include', $hwiext, $debug);

$template_html_encode = $cfg->Val('SystemSettings', 'template_html_encode');
if (preg_match('/^\d$/', $template_html_encode)) $template->html_enchode = $template_html_encode;

if ($cfg->val('SystemSettings', 'html_replace_pattern')) $template->html_replace_pattern = $cfg->val('SystemSettings', 'html_replace_pattern');
if ($cfg->val('SystemSettings', 'html_replace_str')) $template->html_replace_str = $cfg->val('SystemSettings', 'html_replace_str');


	//   set up the HW include special fields..
	//   i.e IMAP=path here thus in template we can allow
	//    %%HWI=IMAP:xxxxx%%
	$hwisections = $cfg->Parameters('HWI Sections');
	foreach($hwisections as $hwi_sec) {
		$val = $cfg->val('HWI Sections', $hwi_sec);
		$template->set($hwi_sec,$val);
	}



	//handle alt tag. if there. we first try & get file from AltTemplate ini
	// if not then if there is no 'alt template security' set in ini we try
	// the alt param with 'html' extension.. if all fails we default back to
	// the standard template set in $t
	if (isset($var['alt']) && !(!empty($var['BADPLACE']) && $cfg->val('SystemSettings', 'badplace_when_alt'))) {
		$alt = preg_replace('/\W/','',$var['alt']);
		$talt = $cfg->val('AltTemplates', $alt);
		if (!$talt && !$cfg->val('SystemSettings', 'alt template security')) {
			if ($alt)  $talt = $alt . '.' . $hwiext;
			if ($debug) print "<br>alt=$template_dir/$talt<br>\n";
			if ($talt && file_exists("$template_dir/$talt")) $t = $talt;
		}
	}



	$pr->load_hwv($cfg, $var, $form);;



$template_hashes[0] =& $var;
$template_hashes[1] =& $form;

if (is_object($wx_object)) $template_hashes[2] =& $wx_object;
if ($t && file_exists($template_dir .'/'.$t)) {
	$mime = $cfg->val('SystemSettings', 'mime_type');
   	if ($mime) header('Content-Type: ' . $mime);
	$template->print_template($template_dir .'/'.$t, $extra_parse, $template_hashes, $savefile, $form['om']);
}


?>