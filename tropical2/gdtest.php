<?php

/*
##########################################################
# HW3image Pre-Install Tester
# This script is copyright(c) 2003 by HAMweather, LLC, all rights reserved.
# It is subject to the license agreement that can be found at the following
# URL: http://www.hamweather.com/install/licenseinfo.html
#########################################################

# This script will inform you of PHP version and compatibility and
# check to see if you have the GD 
# and from this will inform you on what version of HAMweather you can
# run on your system as is

*/

Function GDInfo() {
	
	ob_start();
	phpinfo(8);
	$phpinfo=ob_get_contents();
	ob_end_clean();
	$phpinfo=strip_tags($phpinfo);
	$phpinfo=stristr($phpinfo,"gd version");
	$phpinfo=stristr($phpinfo,"version");
	if (preg_match('/^\D*(\d[\d.]+)/', $phpinfo, $m)) {
		$phpinfo = $m[1];
	}
	else {
		$end=strpos($phpinfo,".");
		$phpinfo=substr($phpinfo,0,$end);
		$length = strlen($phpinfo)-1;
		$phpinfo=substr($phpinfo,$length);
	}

	if (preg_match('/FreeType\s+?Support\s+?enabled/', $phpinfo)) {
		$freetype = true;
	}
	else { $freetype = false; }
	
	return array($phpinfo, $freetype);
	
}

Function GDSupport() {
	ob_start();
	phpinfo(8);
	$phpinfo=ob_get_contents();
	ob_end_clean();
	
	if (preg_match('/\bgd\b.+?(<table.+?<\/table>)/s', $phpinfo,$m)) {
		return $m[1];
	}
	else {return '';}
}
			

list($gdversion,$freetype) = GDInfo();

?>
<html><head><title>HAMweather 3 : GD Test</title></head>
<body><table border='1' width='100%' cellspacing='0'>


<tr><td colspan='2' align='center' bgcolor='#FFCC00'><b><font face='Arial' size='4'>HAMweather 3 : </font></b><b><font face='Arial' size='4'>GD Test</font></b></td></tr>
<tr><td width='200' bgcolor='#FFFF99' valign='top'>PHP compiled with GD support?</td>

<?php

if ($gdversion > 0) {
	$gdsupport = GDSupport();
	print "<td bgcolor='#FFFF99'><b>Yes!</b><br>PHP was compiled with GD.</td></tr>\n";
   	print "<tr><td width='200' bgcolor='#FFCC00' valign='top'>GD Version Found:</td>\n";
   	print "<td bgcolor='#FFCC00'><b>$gdversion</b><br>\n";
   	print "The server should be able to execute HW3image or other HW3 image creating plug-ins.";
   	print "</td></tr>\n";

	print "<tr><td width='200' bgcolor='#FFCC00' valign='top'>Font Support:</td>\n";   	
	if ($freetype) {
		print "<td bgcolor='#FFCC00'><b>Free Type Font Support.</b><br>".
			 "You should be able to use GD Fonts and True Type Fonts\n"; 
	}
	else {
		print "<td bgcolor='#FFCC00'><b>Internal GD Font Support.</b><br>" .
			 "You will only be able to use the built in GD Fonts.\n";
	}
	print "</td></tr>\n";
	
	print "<tr><td width='200' bgcolor='#FFCC00' valign='top'>GD Features supported:</td>\n";
   	print "<td bgcolor='#FFCC00'><b>$gdsupport</b>\n";   	
   	print "</td></tr>\n";	
}
else {
   print "<td bgcolor='#FFFF99'>NO!<br>php was not compiled with GD support<br>\n";
   print "<font color=red><b>This error will need to be resolved before you can use HW3image or other HW3 image creating plug-ins.</b></font></td></tr>\n";
}

print "</table>\n";
print "<center><p align='center'><font face='arial' size=1> Copyright 2003, <a href='http://www.hamweather.com'>HAMweather, LLC</a></p></center>\n";
print "</body></html>\n";



?>

