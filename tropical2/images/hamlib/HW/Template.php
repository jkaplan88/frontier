<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Template.php,v 1.45 2008/10/01 02:30:10 wxfyhw Exp $
*
*/


class Template {

	var $storage = array('lf'=>"\n",'cr'=>"\r",'crlf'=>"\r\n");

	var $hwi_path = '';
	var $hwi_ext = 'html';
	var $print_comments = 1;
	var $print_blanks = 1;
	var $debug = 0;
	var $uvars = array();
	var $html_replace_pattern = '/\n/';
	var $html_replace_str = '<br/>';

	var $html_encode = 1;

	var $revision = '$Id: Template.php,v 1.45 2008/10/01 02:30:10 wxfyhw Exp $';


	Function Template($path, $ext, $debug) {
	   $this->hwi_path = $path;

	   $this->debug = $debug;

	   if (!isset($ext) || $ext == '') {
	      $this->hwi_ext = 'html';
	   }
	   else{
	      $this->hwi_ext = $ext;
	   }

	}

	Function debug ($debug) {
		$this->debug=$debug;
	   	if ($debug) {
			print "template.php (revision=$revision)<br>\n";
		}
	}

	/***************************************************
	/Function print_template
	/$template_file - the file to load the template from
	/$extra_parse - reference to a routine to run in addition to normal parsing
	/$hashes - an array of hashes used for finding values
	/$save_file - the file to save to, if we're saving to a file too
	/$pm - the print mode (0 = web, 1 = web and file, 2 = file)
	/Returns error message (none if operation went ok)
	/Parses a template file and outputs it to the appropriate location
	/**************************************************/
	Function print_template($template_file, &$extra_parse, &$hashes, $save_file, $pm) {
		//Open the file and place it's contents in a line by line array
		if (!$file_contents = file($template_file)) {
			return "Error: File could not be read";
		}


		$save_file = str_replace('..', '', $save_file);
		if ($save_file) {
			$save_file = str_replace('//', '/', $save_file);

		    	if (!$fh = fopen($save_file, 'w')) {
		         		print "Cannot open file ($save_file)";
		    	}
		}
		else { $fh=0;}


		//Loop through the file contents and parse the line
		$curr_line = "";
		while (list($line_num, $line) = each($file_contents)) {
			if (preg_match("/^(.*)__\s*$/", $line)) {
				$curr_line .= preg_replace("/^(.*)__\s*$/", "$1", $line);
			}
			else {
				$this->parse_line($curr_line.$line, 1, $fh, $pm, $extra_parse, $hashes);
				$curr_line = "";
			}
		}
		if ($curr_line <> "") {
			$this->parse_line($curr_line.$line, 1, $fh, $pm, $extra_parse, $hashes);
		}

		return "";
	}

   Function do_set_print_blanks ($val) {
      if ($val == '') { $val = 0; }
      $this->print_blanks = $val;
      return '';
   }

   Function do_set_print_comments($val) {
      if ($val == '') { $val = 0; }
      $this->print_comments = $val;
      return '';
   }

   Function do_set_html_encode($val) {
      if ($val == '') { $val = 0; }
      $this->html_encode = $val;
      return '';
   }


	/***************************************************
	/Function parse_line
	/$line - the line to be parsed
	/$print_now - 0 to parse but not print, 1 to parse and print
	/$save_file_fh - the file handler to the save file
	/$pm - print mode
	/$extra_parse - extra routine to parse with
	/$hashes - array of hashes used to find values in
	/Returns the parsed line
	/**************************************************/
	Function parse_line($line, $print_now, $save_file_fh, $pm, &$extra_parse, &$hashes) {

		$tpm = $pm;

		//Replace %25 with %
		$line = preg_replace('/%25/', "%", $line);

		//Trim off final spaces
		$line = preg_replace('/__(\s*)$/', "$1", $line);


		$line = preg_replace('/%%HTMLENCODE=(\d)%%/e',
				"\$this->do_set_html_encode('$1');",
				$line);


		$line = preg_replace('/%%PRINTBLANKS=(\d)%%/e',
				"\$this->do_set_print_blanks('$1');",
				$line);

		$line = preg_replace('/%%PRINTCOMMENTS=(\d)%%/e',
				"\$this->do_set_print_comments('$1');",
				$line);

         	if (!$this->print_comments) { $line = preg_replace('/<!--[^#].*?-->/', '', $line); }


		//Repeat
		$line = preg_replace('/%%([Rr][Ee]PEAT)\s+?([\w%]+)\s+?([\w%]+)(.+?)\s\1%%/e',
				"\$this->do_parse_repeat('$2', '$3', '$4', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//If...then...else
		$line = preg_replace('/%%IF(\d*?)\s(.+?)\sTHEN\1\s(.+?)(?:\sELSE\1\s(.+?))?\sIF\1%%/se',
				"\$this->do_if_then_else('$2', '$3', '$4', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Code blocks
		$line = preg_replace('/%%([Cc][Oo]DE)\s(.+?)\s\1%%/e',
				"\$this->do_parse_code('$2', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Let
		$line = preg_replace('/%%([Ll][Ee][Tt])\s(hwv[\w%]+)\s*?=(.+?)\s\1%%/e',
				"\$this->do_let('$2', '$3', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Extra parse
		if ($extra_parse) {
			list($line, $tpm) = $extra_parse->parse_line($line,0, $this, $print_now, $save_file_fh, $tpm, $extra_parse, $hashes);
		}
		//Another Repeat
		$line = preg_replace('/%%(_[Rr][Ee]PEAT)\s+?([\w%]+)\s+?([\w%]+)(.+?)\s\1%%/e',
				"\$this->do_parse_repeat('$2', '$3', '$4', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Another Let
		$line = preg_replace('/%%(_[Ll][Ee][Tt])\s(hwv[\w%]+)\s*?=(.+?)\s\1%%/e',
				"\$this->do_let('$2', '$3', \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Check for ^^ within the line, parse again if there is
		$line = preg_replace("/\^\^([^\^]+?)\^\^/e",
				"\$this->parse_line(\"%%\".\"$1\".\"%%\", 0, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);

		//Find replacement
		$line = preg_replace("/%%([\w]+)%%/e", "\$this->find_replacement(\"$1\", \$hashes);", $line);
		//$line = preg_replace("/%%([\w]+)%%/e", "(\$this->html_encode) ? htmlentities(\$this->find_replacement(\"$1\", \$hashes)) : \$this->find_replacement(\"$1\", \$hashes);", $line);

         	$line = preg_replace('/%%hwi=\s*?([\%\w:]+)%%/e',
				"\$this->do_hwi_include('','','$1', \$print_now, \$save_file_fh, \$pm, \$extra_parse, \$hashes);",
				$line);


      if ($extra_parse) {
            list($line, $tpm) = $extra_parse->parse_line($line,1, $this, $print_now, $save_file_fh, $tpm, $extra_parse, $hashes);
      }

         if (!$this->print_comments) { $line = preg_replace('/<!--.+?-->/', '', $line); }
         if (!$this->print_blanks && ($line == '' || preg_match('/^\s+$/', $line))) {
            $line = '';
         }


		//Print the line
		if (($print_now == 1) && ($tpm== 0 || $tpm == 1)) {
			echo $line;
		}

		if ($save_file_fh && $print_now && $tpm > 0) {
			fwrite($save_file_fh, $line);
		}

		return $line;
	}



	/***************************************************
	/Function do_let
	/$key - the key to store the value in
	/$val - the value to store
	/$save_file_fh - the save file's filehandler
	/$extra_parse - a function that parses beyond the normal parsing
	/$hashes - an array of hashes for finding values in
	/Parses the line to be looped, and loops it
	/**************************************************/
	Function do_let($key, $val, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$key = $this->parse_line($key, 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$val = $this->do_parse_code("return ".$val.";", $save_file_fh, $pm, $extra_parse, $hashes);
		$this->storage[$key] = $val;
		return "";
	}

	/***************************************************
	/Function do_if_then_else
	/$if - the block of code to eval for the boolean check
	/$then - parsed if $if is true
	/$else - parsed if $if is false
	/$save_file_fh - the save file's filehandler
	/$extra_parse - a function that parses beyond the normal parsing
	/$hashes - an array of hashes for finding values in
	/Parses the line to be looped, and loops it
	/**************************************************/
	Function do_if_then_else($if, $then, $else, $save_file_fh, $pm, &$extra_parse, &$hashes) {
		$tmp = "";
		if ($this->do_parse_code("return ".$if.';', $save_file_fh, $pm, $extra_parse, $hashes)) {

			$tmp = $this->parse_line($this->clean_quotes($then), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		}
		elseif($else) {

			$tmp = $this->parse_line($this->clean_quotes($else), 0, $save_file_fh, $pm, $extra_parse, $hashes);
		}



		return $tmp;
	}

	/***************************************************
	/Function do_parse_repeat
	/$start - the value to start the loop on
	/$stop - the value to stop the loop on
	/$string - what will be looped
	/$save_file_fh - the save file's filehandler
	/$extra_parse - a function that parses beyond the normal parsing
	/$hashes - an array of hashes for finding values in
	/Parses the line to be looped, and loops it
	/**************************************************/
	Function do_parse_repeat($start, $stop, $string, &$save_file_fh, $pm, &$extra_parse, &$hashes) {

		//Parse out the start and stop
		$start = $this->parse_line($start, 0, $save_file_fh, $pm, $extra_parse, $hashes);
		$stop = $this->parse_line($stop, 0, $save_file_fh, $pm, $extra_parse, $hashes);
           $string = $this->clean_quotes($string);
		$output = "";
		$expr = "";
		//Run the loop in the right direction
		if ($start <= $stop) {
			for ($i = $start; $i <= $stop; $i++) {
				//Replace hwcounter with the loop counter
				$expr = preg_replace("/%%hwcounter%%/", $i, $string);
				//Parse the line to be looped
				$output .= $this->parse_line($expr, 0, $save_file_fh, $pm, $extra_parse, $hashes);
			}
		}
		else {
			for ($i = $start; $i >= $stop; $i--) {
				//Replace hwcounter with the loop counter
				$expr = preg_replace("/%%hwcounter%%/", $i, $string);
				//Parse the line to be looped
				$output .= $this->parse_line($expr, 0, $save_file_fh, $pm, $extra_parse, $hashes);
			}
		}

		return $output;
	}


      Function clean_quotes ($expr) {
         $expr = str_replace("\\\"", "\"", $expr);
         return str_replace("\\'", "'", $expr);
      }

	/***************************************************
	/Function do_parse_code
	/$expr - the expression to eval
	/$save_file_fh - the save file's filehandler
	/$extra_parse - a function that parses beyond the normal parsing
	/$hashes - an array of hashes for finding values in
	/Parses the line to be looped, and loops it
	/**************************************************/
	Function do_parse_code($expr, $save_file_fh, $pm, &$extra_parse, &$hashes) {

		$expr = $this->parse_line($expr, 0, $save_file_fh, $pm, $extra_parse, $hashes);

           $expr = $this->clean_quotes($expr);
         $expr = preg_replace(array('/\beq\b/','/([^\'\w])ne([^\'\w])/'), array('==', '$1!=$2'), $expr);
           if ($this->debug) {print "<br>expr=\"$expr\"<br>\n";}
		return eval($expr);

	}




      Function do_eval ($expr) {
         $expr = preg_replace(array('/\beq\b/','/([^\'\w])ne([^\'\w])/'), array('==', '$1!=$2'), $expr);
         return eval($expr);
      }


	/***************************************************
	/Function find_replacement
	/$match - the name to find a match for in the hashes
	/$hashes - the array of hashes to look in
	/Hunts through the hashes for $match and returns the value found
	/**************************************************/
	Function find_replacement($match, &$hashes, $didUrlEncode = false) {
		$match_num = "";
		$match_v = $match_vv = "";
		$value_found = "";


		//Break up if there is a _vv
		if (preg_match("/^(\w+)_vv(\w+)$/", $match, $m)) {
			$match = $m[1];
			$match_v = $m[2];
			$match_vv = '_vv' . $m[2];
		}
		//Break up if there is a number
		if (preg_match("/^(.+?)(\d+)$/", $match, $m)) {
			$match = $m[1];
			$match_num = $m[2];
		}

		//If there is a prefix, then perform the required action
		if (preg_match("/^pc_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true);
			$value_found = preg_replace("/([�������\w]+)/e","ucwords(strtolower('$1'))", $value_found);
		}
		elseif (preg_match("/^lc_/", $match)) {
			$value_found = strtolower($this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true));
		}
		elseif (preg_match("/^uc_/", $match)) {
			$value_found = strtoupper($this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true));
		}
		elseif (preg_match('/^cl_/', $match)) {
			$value_found = preg_replace('/[^\w\&; ]+/', '', $this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true));
		}
		elseif (preg_match("/^url_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 4).$match_num.$match_vv, $hashes, true);
			//$value_found = preg_replace("/ /", "+", $value_found);
			$value_found = urlencode(html_entity_decode($value_found,ENT_QUOTES));
			$didUrlEncode = true;
		}
		elseif (preg_match("/^fc_/", $match)) {
			$value_found = $this->fix_case($this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true));
		}
		elseif (preg_match("/^fc2_/", $match)) {
			$value_found = $this->fix_case($this->find_replacement(substr($match, 4).$match_num.$match_vv, $hashes, true), true);
		}
		elseif (preg_match("/^html_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 5).$match_num.$match_vv, $hashes, true);
			$value_found = preg_replace($this->html_replace_pattern, $this->html_replace_str, $value_found);
		}
		elseif (preg_match("/^nq_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true);
			$value_found = preg_replace("/['\"]/", "", $value_found);
		}
		elseif (preg_match("/^br_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 3).$match_num.$match_vv, $hashes, true);
			$value_found = preg_replace("/([�������\w])\s+([�������\w])/", "$1<br/>$2", $value_found);
		}
		elseif (preg_match('/^dec(\d?)_/', $match)) {
			//echo substr($match, 0, strpos($match, "_")+1);
			//echo "<br>";
			//echo substr($match, strpos($match, "_")+1, strlen($match) - strpos($match, "_"));
			$dec = str_replace("dec", "", substr($match, 0, strpos($match, "_")+1));
			$dec = str_replace("_", "", $dec);
			$value_found = $this->find_replacement(substr($match, 4+strlen($dec)).$match_num.$match_vv, $hashes, true);
			if (is_numeric($value_found)) {
			      $dec = ($dec > 0) ? $dec : 0;
				$value_found = sprintf("%1.".$dec."f", $value_found);
			}
		}
		elseif (preg_match("/^cel[sc]ius_/", $match)) {
			$value_found = $this->find_replacement(substr($match, 8).$match_num.$match_vv, $hashes, true);
			if (is_numeric($value_found)) {
				$value_found = ($value_found-32)/9*5;
			}


		}
		elseif (preg_match("/^(fahrenheit|farenheit)_/", $match, $tm)) {
			$value_found = $this->find_replacement(substr($match, strlen($tm[1])+1).$match_num.$match_vv, $hashes, true);
			if (is_numeric($value_found)) {
				$value_found = $value_found*9/5+32;
			}
		}

		//If we do not have a value from a previous interation, then find one
		if (!is_numeric($value_found) && $value_found == '') {
			//Search the hash list
			foreach ($hashes as  $ref) {
				if (is_object($ref) ) {
					if (method_exists($ref, $match)) {
						$value_found = $ref->$match($match_num, $match_v);
					}
					else {
						$value_found = $ref->GetValue($match, $match_num);
					}
				}
				elseif (is_array($ref)) {
					if (isset($ref[$match])) {
						if (is_array($ref[$match]) && $match_num !='') {
							$tmp =& $ref[$match];
							if (isset($tmp[$match_num])) $value_found = $tmp[$match_num];
						}
						else {	$value_found = $ref[$match];}
					}
				}
//print "bmatch=$match$match_num$match_v...$value_found(" . gettype($value_found) . ")<br>\n";
 				if ( ($value_found) != '' && !is_null($value_found)) break;

//print "amatch=$match$match_num$match_v...$value_found<br>\n";
			}
			//Search the storage hash
			if ($value_found == '') {
				if (isset($this->storage[$match]))  {
					$value_found = $this->storage[$match];
				}
			}
		}


		if (!$didUrlEncode && $this->html_encode > 0 && substr($match,0,9) != 'hwv_wxdic') {
			if ($this->html_encode == 2) {
				$value_found = htmlentities($value_found,ENT_QUOTES);
			}
			elseif ($this->html_encode) {
				if (strpos($value_found, '<') !== false && 	strpos($value_found, '>') !== false ) {
					// ok we have html so we have to play friendly
					$value_found = preg_replace('/^([^<]+)/e', "htmlentities('$1',ENT_QUOTES)", $value_found);
					$value_found = preg_replace('/^([^<]+)/e', "htmlentities('$1',ENT_QUOTES)", $value_found);
					$value_found = preg_replace('/>([^<]+)/e', "'>' . htmlentities('$1',ENT_QUOTES)", $value_found);


				}
				else $value_found = htmlentities($value_found,ENT_QUOTES);
			}

			$value_found = str_replace('&amp;amp;', '&amp;', $value_found);
			$value_found = preg_replace('/\&amp;(\w+|#\d+);/', '&$1;', $value_found);
		}



		return $value_found;
	}


	Function fix_case($str, $blf = false) {

		$str = strtolower($str);
		if (!$blf) $str = preg_replace('/\s+/', ' ', $str);
		$str = preg_replace("/\b(at|including)(\s+?)(\w)/e", '"$1"."$2".strtoupper("$3")', $str);
		$str = preg_replace("/\b(\w+)(\s+?)(river|creek|basin)\b/e", 'ucwords("$1")."$2".ucwords("$3")', $str);
		$str = preg_replace("/\b(noaa|nws)\b/e", 'strtoupper("$1")', $str);
		$str = preg_replace("/^\s*?([a-z])/e", 'strtoupper("$1")', $str);
		$str = preg_replace("/([.:]\s*?)([a-z])/e", '"$1".strtoupper("$2")', $str);
		$str = preg_replace("/(monday|tuesday|wednesday|thursday|friday|saturday|sunday)/e", 'ucwords("$1")', $str);
		$str = preg_replace("/\b(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)/e", 'ucwords("$1")', $str);


		//Proper case for state names
		$str = preg_replace("/\b(alabama|alaska|arizona|arkansas|california|colorado|connecticut|delaware|florida|georgia|guam|hawaii|idaho|illinois|indiana|iowa|kansas|kentucky|louisiana|maine|maryland|massachusetts|michigan|minnesota|mississippi|missouri|montana|nebraska|nevada|new hampshire|new jersey|new mexico|new york city|new york|north carolina|north dakota|northern mariana islands|ohio|oklahoma|oregon|pennsylvania|puerto rico|rhode island|south carolina|south dakota|tennessee|texas|utah|vermont|virginia|washington|west virginia|wisconsin|wy|wyoming|canada|alberta|british columbia|manitoba|new brunswick|newfoundland|northwest territories|nova scotia|ontario|prince edward island|qu�bec|quebec|saskatchewan|yukon territory)\b/e", 'ucwords("$1")', $str);
		//Upper case for weather directions (used less often in weather statements)
		//Fixes abbrev. preceeded and followed by spaces or a period.
		$str = preg_replace("/\b(n|e|w|s|ne|nne|nw|nnw|sw|ssw|se|se)\b/e", 'strtoupper("$1")', $str);
		//Upper case for State (and city) abreviations (without states: in, or, on. State abbrevs. are rarer in weather statements, usually the state name should do the trick)
		//Fixes abbrev. preceeded and followed by spaces or a period.
		$str = preg_replace("/\b(al|ak|az|ar|ca|co|ct|de|fl|ga|gu|hi|id|il|ia|ks|ky|la|me|md|ma|mi|mn|ms|mo|mt|ne|nv|nh|nj|nm|ny|nyc|n.y.c.|nc|nd|nmi|oh|ok|pa|pr|ri|sc|sd|tn|tx|ut|vt|va|wa|wv|wi|wy|d.c.|dc|ab|bc|mb|nb|nf|nt|ns|pe|qc|sk|yk|l.a.|s.f.)\b/e", 'strtoupper("$1")', $str);

		return $str;
	}



	/***************************************************
	/Function print_template
	/$template_file - the file to load the template from
	/$extra_parse - reference to a routine to run in addition to normal parsing
	/$hashes - an array of hashes used for finding values
	/$save_file - the file to save to, if we're saving to a file too
	/$pm - the print mode (0 = web, 1 = web and file, 2 = file)
	/Returns error message (none if operation went ok)
	/Parses a template file and outputs it to the appropriate location
	/**************************************************/
	Function do_hwi_include($before, $after, $hwi_nick, $print_now, $save_file_fh, $pm, &$extra_parse, &$hashes) {


		$include_str = '';
		//Open the file and place it's contents in a line by line array
		if ($print_now) {$this->parse_line($before, 1, $save_file_fh, $pm, $extra_parse, $hashes); }
		  else {$include_str = $this->parse_line($before, 0, $save_file_fh, $pm, $extra_parse, $hashes);}

		if (preg_match('/^([A-Z]+):(.+)$/', $hwi_nick, $m)) {
			$sec=$m[1]; $hwi_nick=$m[2];

			if ($this->debug) { print"hwi: sec=$sec<br>\n"; }
			if (!$sec || !isset($this->uvars[$sec])) return '';
			$dir = $this->parse_line($this->uvars[$sec], 0, $save_file_fh, $pm, $extra_parse, $hashes);

			if ($this->debug) { print"hwi: dir=$dir<br>\n"; }
			if (!is_dir($dir)) return '';
			$template_file = $dir . '/' . $hwi_nick . '.' . strtolower($sec);

		}
		else {
			$template_file = $this->hwi_path . '/' . $hwi_nick . '.' . $this->hwi_ext;
		}
		if ($this->debug) { print"include file $template_file<br>\n"; }

		//Open the file and place it's contents in a line by line array
		if (!file_exists($template_file)) {
		      if ($this->debug) { print"$template_file is not a file.<br>\n"; }
		      return '';
		}
		elseif (!$file_contents = file($template_file)) {
			if ($this->debug) { print"Error: File could not be read<br>\n"; }
			return "Error: File could not be read";
		}
		else {
      		//Loop through the file contents and parse the line
      		$curr_line = "";
      		while (list($line_num, $line) = each($file_contents)) {
      			if (preg_match("/^(.*)__\s*$/", $line)) {
      				$curr_line .= preg_replace("/^(.*)__\s*$/", "$1", $line);
      			}
      			else {
      			      if ($print_now) {
      			         $this->parse_line($curr_line.$line, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
      				}
      				else {
      				   $include_str .= $this->parse_line($curr_line.$line, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
      				}
                           $curr_line = "";
      			}
      		}
      		if ($curr_line <> "") {
			if ($print_now) {
				$this->parse_line($curr_line.$line, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
                     	}
                    	else {
                        		$include_str .= $this->parse_line($curr_line.$line, $print_now, $save_file_fh, $pm, $extra_parse, $hashes);
                     	}
      		}
   	   }

           if ($print_now) {$this->parse_line($after, 1, $save_file_fh, $pm, $extra_parse, $hashes); }
            else {$include_str .= $this->parse_line($after, 0, $save_file_fh, $pm, $extra_parse, $hashes);}



		return $include_str;
	}

	Function set ($key, $val) {
		$this->uvars[$key] = $val;
	}




}


?>