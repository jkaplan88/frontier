<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HandleInput.php,v 1.13 2008/07/17 14:13:37 wxfyhw Exp $
*
*/



Function parse_input(&$form, $debug) {
//	GLOBAL $HTTP_GET_VARS, $HTTP_POST_VARS;


	// Begin snippet to get code from command line args
	// provided by cabel sasser
	//If there are command line arguments, get them
	if (isset($_SERVER['argv'])) {
		// Loop through each command line xxx=yyy pair and put them in the form hash
		foreach($_SERVER['argv'] as $value) {
			if (strstr($value, "=")) {
				list ($cmdkey, $cmdvalue) = split("=", $value);
				$cmdvalue = preg_replace('/\+/', ' ', $cmdvalue);
				$cmdkey = preg_replace('/\W/','', $cmdkey);
				$cmdvalue = preg_replace('/[^\w�������� \.,\-]/', '', $cmdvalue);
				$form[$cmdkey] = $cmdvalue;
			}
		}
	}
	//End snippet

	//Loop through GET vars and place then into the form hash
	while (list ($key, $value) = each($_GET)) {
		$key = preg_replace('/\W/', '',$key);
		if (preg_match('/(https?|ftp):/', $value)) $value='';
		$form[$key] = preg_replace('/[^\w�������� \.,\-]/', '', $value);

	}

	//Loop through POST vars and place then into the form hash
	while (list ($key, $value) = each($_POST)) {
		$key = preg_replace('/\W/', '',$key);
		if (preg_match('/(https?|ftp):/', $value)) $value='';
		$form[$key] = preg_replace('/[^\w�������� \.,\-]/', '', $value);
	}

}

Function process_input(&$form, &$var, &$cfg, $get_cookies, $debug) {

	if ($get_cookies) {
		 if (!isset($form['pands']) && (isset($form['place']) || isset($form['fips']) || isset($form['county']) || isset($form['icao']) || isset($form['zone']))) {
		 	$killpands = 1;
		}
		else { $killpands = 0;}

		$var['chips']  = $get_cookies->GetCookie($form, $cfg->Val('SystemSettings', "cookie_name"));
		if($killpands && isset($form['pands'])) unset($form['pands']);
	}

//   my $configs = "$$form{user},$$form{theme},$$form{config}";
//   $configs =~ s/[^\w,]//g; $configs =~ s/,,+//g;
//   $$form{config} = ($configs) ? $configs : $cfg->val('Defaults', 'configs');


   $configs = getKeyVal($form, 'config', '');
   $configs = preg_replace('/[^\w,]/','',$configs); $configs =preg_replace('/,,+/',',',$configs);
   $form['config'] = $configs or $cfg->Val('Defaults', 'configs');


   if (isset($form['pass'])) {$var['pass'] = preg_replace('/\W/', '', $form['pass']); }
      else {$var['pass']='';}
   if (isset($form['alt'])) {$var['alt'] = preg_replace('/\W/', '', $form['alt']);}
      else {$var['alt']=''; }


   // lets get the max days default
   $maxdays = getKeyVal($form, 'maxdays', getKeyVal($var, 'maxdays', $cfg->Val('Defaults', 'maxdays')));
   $var['maxdays'] = $maxdays ? $maxdays : 5;

   // lets get the valid forecast value from either forecast or 'do'
   $forecast = getKeyVal($form, 'forecast', getKeyVal($form,'do', false));
   $forecast = $forecast ? $forecast : getKeyVal($form, 'do', false);
   $forecast = $forecast ? $forecast : $cfg->Val('Defaults', 'forecast');
   $forecast = preg_replace('/\W/','',strtolower($forecast));
   if (!$forecast) $forecast = $cfg->Val('Defaults', 'forecast');

   $form['forecast'] = $forecast;
   $form['nofc'] = getKeyVal($form, 'nofc', false);
   $form['mode'] = getKeyVal($form, 'mode', false);
   $form['dpf'] = getKeyVal($form, 'dpf', false);
//
//   $form['forecast'] =  or getKeyVal($form, 'do', false) or
//                        $cfg->Val('Defaults', 'forecast');



if ($debug) {print "forecast = ".$form['forecast']."<br>default: forecast=" . $cfg->Val('Defaults', 'forecast') . "<br>\n";}

}

?>