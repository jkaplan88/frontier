<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchSunInfo.php,v 1.11 2008/08/24 01:51:45 wxfyhw Exp $
*
*/


class FetchSunInfo {

	var $sunparams = array();
	var $debug=0;
	var $core_path = '';

	var $newMoon;
	var $synodicMoon;



	Function FetchSunInfo ($core_path) {
		$this->core_path = $core_path;

		$this->newMoon= mktime(22, 55, 04, 1, 26, 1971); // 1971-01-26 22:55:04
		$this->synodicMoon = 2.78 + 60 * (48 + 60 * (12 + 24 * 29)); // seconds

		$this->newMoon = mktime(6, 39, 0, 2, 16, 1999);

	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		if ($index <> "") {
			$tmp = $this->$key;
			if (isset($tmp[$index])) $val = $tmp[$index];
		}
		else {
			$val = $this->$key;
		}


		error_reporting($old_error_reporting);

		return $val;
	}


	Function sunrise($ampm=0) {
		return $this->calc_sun_time(-.0145439, 'rise',$ampm);
	}
	Function sunset($ampm=0) {
		return $this->calc_sun_time(-.0145439, 'set',$ampm);
	}
	Function astronomical_twlight_rise($ampm=0) {
		return $this->calc_sun_time(-.309017, 'rise',$ampm);
	}
	Function astronomical_twlight_set($ampm=0) {
		return $this->calc_sun_time(-.309017, 'set',$ampm);
	}
	Function nautical_twilight_rise($ampm=0) {
		return $this->calc_sun_time(-.207912, 'rise',$ampm);
	}
	Function nautical_twilight_set($ampm=0) {
		return $this->calc_sun_time(-.207912, 'set',$ampm);
	}
	Function civil_twilight_rise($ampm=0) {
		return $this->calc_sun_time(-.104528, 'rise',$ampm);
	}
	Function civil_twilight_set($ampm=0) {
		return $this->calc_sun_time(-.104528, 'set',$ampm);
	}

	Function moonrise($ampm=0) {
		return $this->calc_moon_time( 'rise',$ampm);
	}
	Function moonset($ampm=0) {
		return $this->calc_moon_time('set',$ampm);
	}



	Function set_sunrise_params(&$var, &$form, $forecast, &$cfg, $forecast_info,  $debug){

		//$temp_array = explode(",", $forecast_info);
		$t = $forecast_info[2];

	//	my ($lat, $lon, $year, $mon, $day, $tz);
		$tz =0;

		if (isset($form['lat']) && isset($form['lon'])) {
			$lat = $form['lat']; $lon = $form['lon'];
		}
		elseif (isset($var['lat']) && isset($var['lon'])) {
			$lat = $var['lat']; $lon = $var['lon'];
		}
		else { $lat = $lon = 'N/A';}

		if (isset($form['tz'])) { $tz = $form['tz'];}
			elseif( isset($var['tz'])) { $tz = $var['tz'];}
			elseif( isset($var['tzdif'])) { $tz = $var['tzdif'];}

		if (isset($form['epoch']) && is_numeric($form['epoch'])) {
			$today = getdate($form['epoch']);
			$day = $today['mday']; $mon = $today['mon']; $year=$today['year'];
		}
		elseif (isset($form['year']) && isset($form['month']) && isset($form['day'])) {
			$year = $form['year'];
			$mon = $form['month'];
			$day = $form['day'];
		}
		elseif (isset($var['year']) && isset($var['month']) && isset($var['day'])) {
			$year = $var['year'];
			$mon = $var['month'];
			$day = $var['day'];
		}
		else {
			$today = getdate();
			$day = $today['mday']; $mon = $today['mon']; $year=$today['year'];
		}

		// calculate days since jan 1
		$yday = (367*$year-floor((7*($year+(($mon+9)/12)))/4)+floor((275*$mon)/9)+$day-730530);

		$this->sunparams['lat'] = $lat;
		$this->sunparams['lon'] = $lon;
		$this->sunparams['tz'] = $tz;
		$this->sunparams['yday'] = $yday;
		$this->sunparams['day'] = $day;
		$this->sunparams['mon'] = $mon;
		$this->sunparams['year'] = $year;

		return $t;
	}




	Function convert_to_ampm($ctime) {

		list($hour, $min) = explode(':', $ctime);
		$ampm = 'am';
		if (!$hour || $hour == 24) { $hour = 12; }
		elseif ($hour == 12) { $ampm = 'pm'; }
		elseif ($hour > 12) {
			$hour -=12;
			$ampm = 'pm';
		}

		return  sprintf("%d:%02d %s", $hour,$min, $ampm);
	}



Function calc_sun_time($R, $type, $ampm) {

	$latitude = $this->sunparams['lat'];
	$longitude = $this->sunparams['lon'];
	if ($latitude == 'N/A' || $longitude == 'N/A') {return 'N/A';}

	$time_zone = $this->sunparams['tz'];
	$yday = $this->sunparams['yday'];

	$A = 1.5708;
	$B = 3.14159;
	$C = 4.71239;
	$D = 6.28319;
	$E = 0.0174533 * $latitude;
	$F = 0.0174533 * $longitude;
	$G = 0.261799  * $time_zone;

	$J = ($type == 'rise') ? $A : $C;
	$K = $yday + (($J - $F) / $D);
	$L = ($K * .017202) - .0574039;   # Solar Mean Anomoly
	$M = $L + .0334405 * sin($L);     # Solar True Longitude
	$M += 4.93289 + (3.49066E-04) * sin(2 * $L);
	$M = $this->_normalize($M, $D);              # Quadrant Determination
	if (($M / $A) - floor($M / $A) == 0) $M += 4.84814E-06 ;
	$P = sin($M) / cos($M);           # Solar Right Ascension
	$P = atan2(.91746 * $P, 1);

	# Quadrant Adjustment
	if ($M > $C) {  $P += $D;   }
		elseif ($M > $A) {  $P += $B;  }

	$Q = .39782 * sin($M);            # Solar Declination
	$Q = $Q / sqrt(-$Q * $Q + 1);     # This is how the original author wrote it!
	$Q = atan2($Q, 1);

	$S = $R - (sin($Q) * sin($E));
	$S = $S / (cos($Q) * cos($E));

	if (abs($S) > 1) return 'none' ;      # Null phenomenon

	$S = $S / sqrt(-$S * $S + 1);
	$S = $A - atan2($S, 1);
	if ($type == 'rise') $S = $D - $S ;

	$T = $S + $P - 0.0172028 * $K - 1.73364; # Local apparent time
	$U = $T - $F;                     # Universal timer
	$V = $U + $G;                     # Wall clock time
	$V = $this->_normalize($V, $D);
	$V = $V * 3.81972;

	$hour = floor($V);
	$min  = floor(($V - $hour) * 60 + 0.5);
	if ($min > 59) { $min -= 60; if (++$hour > 23) { $hour = '00'; } }


	if (is_string($ampm) && $ampm == 'epoch') {

		return $this->_make_epoch($hour,$min,$time_zone);
	}
	elseif ($ampm) {
		return  $this->convert_to_ampm(sprintf("%d:%02d", $hour,$min));
	}
	else {
		return  sprintf("%02d:%02d", $hour,$min);
	}



}




Function calc_moon_time($type, $ampm) {

	$latitude = $this->sunparams['lat'];
	$longitude = -$this->sunparams['lon'];
  	if ($latitude == 'N/A' || $longitude == 'N/A') return 'N/A' ;

	$time_zone = $this->sunparams['tz']/24;
	$year = $this->sunparams['year'];
	$month = $this->sunparams['mon'];
	$day = $this->sunparams['day'];

	$date = $this->_mjd($year, $month, $day, 0) - $time_zone;


//	#define the altitudes for each object
//	#treat twilight as a separate object 3, so sinalt routine
//	#falls through to finding Sun altitude again
	$sl = $this->_sn($latitude);
	$cl = $this->_cn($latitude);
	$sinho = $this->_sn(8 / 60);         #moonrise - average diameter used

	$above = 0;
	$xe = 0;
	$ye = 0;
	$z1 = 0;
	$z2 = 0;
	$nz = 0;
	$utrise = 0;
	$utset = 0;
	$rise = 0;
	$sett = 0;
	$hour = 1;
	$zero2 = 0;
	$ym = $this->_sinalt($date, ($hour - 1), $longitude, $cl, $sl) - $sinho;
	$y0 = 0;
	$yp = 0;

	if ($ym > 0) {   $above = 1;  }
	else {   $above = 0;  }
	//  #used later to classify non-risings

  do {
    $y0 = $this->_sinalt($date, $hour, $longitude, $cl, $sl) - $sinho;
    $yp = $this->_sinalt($date, ($hour + 1), $longitude, $cl, $sl) - $sinho;
    list($xe, $ye, $z1, $z2, $nz) = $this->_quad($ym, $y0, $yp);

    if ($nz == 1) {        # simple rise / set event
      if ($ym < 0) {       # must be a rising event
	$utrise = $hour + $z1;
	$rise = 1;
      }
      else {               # must be setting
	$utset = $hour + $z1;
	$sett = 1;
      }
    }
    if ($nz == 2) {        # rises and sets within interval
      if ($ye < 0) {       # minimum - so set then rise
	$utrise = $hour + $z2;
	$utset = $hour + $z1;
      }
      else {               # maximum - so rise then set
	$utrise = $hour + $z1;
	$utset = $hour + $z2;
      }
      $rise = 1;
      $sett = 1;
      $zero2 = 1;
    }
    $ym = $yp;     #reuse the ordinate in the next interval
    $hour = $hour + 2;
  } while ($hour != 25 && (($rise * $sett) != 1));

  # logic to sort the various rise and set states
  if ($rise == 1 || $sett == 1) {   #current object rises and sets today
    if ($type == 'rise') {
      if ($rise == 1) {
	 $hour = floor($utrise);
	 $min  = floor(($utrise - $hour) * 60 + 0.5);
	if ($min > 59) { $min -= 60; if (++$hour > 23) { $hour = '00'; } }

	if ($ampm == 'epoch') {
		return $this->_make_epoch($hour,$min,$time_zone);
	}
	elseif ($ampm) {
	  return  $this->convert_to_ampm(sprintf("%d:%02d", $hour,$min));
	}
	else {
	  return  sprintf("%d:%02d", $hour,$min);
	}
      }
      else {
	return "N/A";
      }
    }
    else {
      if ($sett == 1) {
	 $hour = floor($utset);
	 $min  = floor(($utset - $hour) * 60 + 0.5);
	if ($min > 59) { $min -= 60; if (++$hour > 23) { $hour = '00'; } }

	if ($ampm == 'epoch') {
		return $this->_make_epoch($hour,$min,$time_zone);
	}
	elseif ($ampm) {
	  return  $this->convert_to_ampm(sprintf("%d:%02d", $hour,$min));
	}
	else {
	  return  sprintf("%d:%02d", $hour,$min);
	}
      }
      else {
	return "N/A";
      }
    }
  }
  else {                            #current object not so simple
    if ($above == 1) {
      return "Always above horizon\n";
    }
    else {
      return "Always below horizon\n";
    }
  }
}

function _make_epoch ($hour,$min, $tz=0) {

	$day = $this->sunparams['day'];
	$mon = $this->sunparams['mon'];
	$year = $this->sunparams['year'];

	$epoch = gmmktime($hour,$min,0,$mon,$day,$year);

	$epoch -= 3600*$tz;

	return $epoch;
}

function is_daylight($epoch) {

	$sunrise = $this->sunrise('epoch');
	$sunset = $this->sunset('epoch');

	$isday = false;
	if ($sunrise == 'none' && $sunset== 'none') {
		$isday = false;
	}
	else {
		if ($sunrise == 'none') {
			if ($epoch < $sunset) $isday = true;
		}
		elseif ($sunset=='none') {
			if ($epoch >= $sunrise) $isday = true;
		}
		elseif ($sunrise < $sunset && $epoch >= $sunrise  && $epoch < $sunset) {
			$isday = true;
		}
		elseif ($sunset < $sunrise && ($epoch < $sunset || $epoch >= $sunrise)) {
			$isday = true;
		}
	}

	return $isday;

}


Function _cn($x) {
  return cos($x * .0174532925199433);
}

Function _fpart($x) {
  # returns fractional part of a number

  $x = $x - floor($x);
  $x = abs($x);

  return $x;
}


Function _hm($ut) {
  # returns number containing the time written in hours and minutes
  # rounded to the nearest minute
  $ut = floor($ut * 60 + .5) / 60;   #round ut to nearest minute
  $h = floor($ut);
  $m = floor(60 * ($ut - $h) + .5);
  $hm = floor(100 * $h + $m);

  return $hm;
}

Function _lmst($mjd, $glong) {
  #    returns the local siderial time for
  #    the mjd and longitude specified
  $mjd0 = floor($mjd);
  $ut = ($mjd - $mjd0) * 24;
  $t = ($mjd0 - 51544.5) / 36525;
  $gmst = 6.697374558 + 1.0027379093 * $ut;
  $gmst = $gmst + (8640184.812866 + (.093104 - .0000062 * $t) * $t) * $t / 3600;
  $lmst = 24 * $this->_fpart(($gmst - $glong / 15) / 24);

  return $lmst;
}


Function _mjd($y,$m,$d,$h) {
  #   returns modified julian date
  #   number of days since 1858 Nov 17 00:00h
  #   valid for any date since 4713 BC
  #   assumes gregorian calendar after 1582 Oct 15, Julian before
  #   Years BC assumed in calendar format, i.e. the year before 1 AD is 1 BC

  $a = 10000 * $y + 100 * $m + $d;
  $b = 0;
  if ($y < 0) {
    $y = $y + 1;
  }
  if ($m <= 2) {
    $m = $m + 12;
    $y = $y - 1;
  }
  if ($a <= 15821004.1) {
    $b = -2 + floor (($y + 4716) / 4) - 1179;
  }
  else {
    $b = floor($y / 400) - floor($y / 100) + floor($y / 4);
  }
  $a = 365 * $y - 679004;
  $mjd = $a + $b + floor(30.6001 * ($m + 1)) + $d + $h / 24;

  return $mjd;
}


Function  _moon($t) {
  # returns ra and dec of Moon to 5 arc min (ra) and 1 arc min (dec)
  # for a few centuries either side of J2000.0
  # Predicts rise and set times to within minutes for about 500 years
  # in past - TDT and UT time diference may become significant for long
  # times

  $p2 = 6.283185307;
  $ARC = 206264.8062;
  $COSEPS = .91748;
  $SINEPS = .39778;
  $L0 = $this->_fpart(.606433 + 1336.855225 * $t);     #mean long Moon in revs
  $L = $p2 *$this-> _fpart(.374897 + 1325.55241 * $t); #mean anomaly of Moon
  $LS = $p2 * $this->_fpart(.993133 + 99.997361 * $t); #mean anomaly of Sun
  $d = $p2 * $this->_fpart(.827361 + 1236.853086 * $t);#diff longitude sun and moon
  $F = $p2 * $this->_fpart(.259086 + 1342.227825 * $t);#mean arg latitude
  # longitude correction terms
  $dL = 22640 * sin($L) - 4586 * sin($L - 2 * $d);
  $dL = $dL + 2370 * sin(2 * $d) + 769 * sin(2 * $L);
  $dL = $dL - 668 * sin($LS) - 412 * sin(2 * $F);
  $dL = $dL - 212 * sin(2 * $L - 2 * $d) - 206 * sin($L + $LS - 2 * $d);
  $dL = $dL + 192 * sin($L + 2 * $d) - 165 * sin($LS - 2 * $d);
  $dL = $dL - 125 * sin($d) - 110 * sin($L + $LS);
  $dL = $dL + 148 * sin($L - $LS) - 55 * sin(2 * $F - 2 * $d);
  # latitude arguments
  $S = $F + ($dL + 412 * sin(2 * $F) + 541 * sin($LS)) / $ARC;
  $h = $F - 2 * $d;
  # latitude correction terms
  $N = -526 * sin($h) + 44 * sin($L + $h) - 31 * sin($h - $L) - 23 * sin($LS + $h);
  $N = $N + 11 * sin($h - $LS) - 25 * sin($F - 2 * $L) + 21 * sin($F - $L);
  $lmoon = $p2 * $this->_fpart($L0 + $dL / 1296000);  #Lat in rads
  $bmoon = (18520 * sin($S) + $N) / $ARC;      #Long in rads
  # convert to equatorial coords using a fixed ecliptic
  $CB = cos($bmoon);
  $x = $CB * cos($lmoon);
  $V = $CB * sin($lmoon);
  $W = sin($bmoon);
  $y = $COSEPS * $V - $SINEPS * $W;
  $Z = $SINEPS * $V + $COSEPS * $W;
  $rho = sqrt(1 - $Z * $Z);
  $dec = (360 / $p2) * atan2($Z / $rho, 1);
  $ra = (48 / $p2) * atan2($y / ($x + $rho), 1);
  if ($ra < 0) {
    $ra = $ra + 24;
  }

  return array($ra, $dec);
}


Function _quad($ym,$y0,$yp) {
#  finds a parabola through three points and returns values of
#  coordinates of extreme value (xe, ye) and zeros if any (z1, z2)
#  assumes that the x values are -1, 0, +1

  $z1 = 0;
  $z2 = 0;
  $nz = 0;
  $a = .5 * ($ym + $yp) - $y0;
  $b = .5 * ($yp - $ym);
  $c = $y0;
  $xe = -$b / (2 * $a);                  #x coord of symmetry line
  $ye = ($a * $xe + $b) * $xe + $c;      #extreme value for y in interval
  $dis = $b * $b - 4 * $a * $c;          #discriminant
  if ($dis > 0) {                           #there are zeros
    $dx = .5 * sqrt($dis) / abs($a);
    $z1 = $xe - $dx;
    $z2 = $xe + $dx;
    if (abs($z1) <= 1) {$nz = $nz + 1;}     #This zero is in interval
    if (abs($z2) <= 1) {$nz = $nz + 1;}     #This zero is in interval
    if ($z1 < -1) { $z1 = $z2;}
  }

  return array($xe, $ye, $z1, $z2, $nz);
}



Function _sinalt($mjd0,$hour,$glong,$cphi,$sphi) {
  # returns sine of the altitude of either the sun or the moon given the
  # modified julian day number at midnight UT and the hour of the UT day,
  # the longitude of the observer, and the sine and cosine of the latitude
  # of the observer

  $ra = 0;
  $dec = 0;
  $instant = $mjd0 + $hour / 24;
  $t = ($instant - 51544.5) / 36525;

  list($ra, $dec) = $this->_moon($t);

  $tau = 15 * ($this->_lmst($instant, $glong) - $ra);   #hour angle of object
  $sinalt = $sphi * $this->_sn($dec) + $cphi * $this->_cn($dec) * $this->_cn($tau);
  return $sinalt;
}

Function _sn($x) {
  return sin($x * .0174532925199433);
}


Function _normalize($Z,$D) {

	// "Trying to normalize with zero offset..." if ($D == 0);
	if ($D != 0) {
		while ($Z < 0)   {$Z +=$D;}
		while ($Z >= $D) {$Z -=$D;}
	}
	return $Z;
}


Function _jday($y,$m,$d) {

	$yy = $y - floor((12-$m)/10);
	$mm = $m+9;
	if ($mm >= 12) $mm -=12;
	$k3 =floor(floor(($yy/100)+49)*.75)-38;
	$j = floor(365.25*($yy+4712)) + floor(30.6*$mm+.5) +$d+59;
	if ($j > 2299160) $j -= $k3 ;

	return $j;

}

Function _jtime($t) {
        $j = ($t / 86400) + 2440587.5;	# (seconds /(seconds per day)) + julian date of epoch
        return ($j);
}

Function moon_next_quarter($which=-1,$time=0) {
	// which < 0 = next quarter
	// which >= 0 means we need to return the date of this


	// use preset time if not passed.
	if ($time <= 0) {
		$time = mktime(0,0,0,$this->sunparams['mon'], $this->sunparams['day'],$this->sunparams['year']);
	}

	// lets get the previous new moon based on the requested time.
	$qMoon = $this->newMoon + floor(($time - $this->newMoon) / $this->synodicMoon) * $this->synodicMoon;

	// now we add 1/4 synodicMoon until we are > than the current time
	$phase = 0;
	$bContinue = true;
	while ($bContinue) {
		$qMoon += $synodicMoon / 4;
		$phase++;
		if ($phase > 3) $phase = 0;

		$bContinue = ( $qMoon > $time && ($which < 0 || ($which >=0 && $phase == $which ) ) ) ? false : true;
	}

	// return the time of the next quarter and the phrase
	// 0 = new, 1 = 1st quarter, 2=full moon, 3 = new
	return array($qMoon,$phase);
}




Function moon_illumination_phase($time = 0) {
	if ($time > 0) {
		$j = $this->_jtime($time);
	}
	else {
		$j = $this->_jday($this->sunparams['year'], $this->sunparams['mon'], $this->sunparams['day']);
	}
	$v = (0.84415+$j-2451550.1)/29.530588853;
	$v -= floor($v); if ($v < 0) $v += 1;
	return $v;
}




Function moon_age_icon($total, $time =0) {
	$il = sprintf("%.2f", $this->moon_illumination_phase($time));
	$il = $il - .5; if ($il < 0) $il +=1;

	$m = floor($il * ($total-1) +.5)+1;
	 if ($m < 0) $m = 0;  if ($m > $total) $m = $total;
	$m -= floor($total/2+.5); if ($m < 1) $m += $total;

	return $m;
}


Function moon_age($time = 0) {
	return $this->moon_illumination_phase($time)*29.530588853;
}

Function moon_phase_icon() {
   $name = preg_replace('/\s+/','_', strtolower($this->moon_phase_name()));
   $name = preg_replace('/\W/','',$name);
   return $name . '.gif';

}


Function moon_illumination($time=0) {
  $il = sprintf("%.2f", $this->moon_illumination_phase($time));
  $il = $il * 2; if ($il > 1) $il = -$il + 2;
  $il *=100;

  return $il;
}


Function moon_phase_name($time=0) {
	$il = sprintf("%.2f", $this->moon_illumination_phase($time));
	$il = $il - .5; if ($il < 0) $il +=1;

	if ($il < .02) { return "Full Moon"; }
	elseif ($il < .24) { return "Waning Gibbous Moon"; }
	elseif($il < .26) { return "Last Quarter Moon"; }
	elseif($il < .49) { return "Waning Crescent Moon"; }
	elseif($il < .52) { return "New Moon"; }
	elseif($il < .74) { return "Waxing Crescent Moon"; }
	elseif($il < .76) { return "First Quarter Moon"; }
	elseif($il < .99) { return "Waxing Gibbous Moon"; }
	else { return "Full Moon";  }

}




}
?>