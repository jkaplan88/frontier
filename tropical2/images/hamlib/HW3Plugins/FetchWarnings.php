<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchWarnings.php,v 1.18 2008/07/17 14:13:35 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");
require_once("hamlib/HW3Plugins/JDay.php"); // We use our own functions in case calendar functions r not installed

class FetchWarnings {
	var $cfg;

	var $debug = false;

	var $tzname='';
	var $tzdif=0;

	var $state;
	var $found;
	var $forecast;
	var $warning_found =0;

	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);


	var $warning_types = array('FFW' , 'Flash Flood Warning',
						'FFA' , 'Flash Flood Advisory',
						'FFS' , 'Flash Flood Statement',
						'FFL' , 'FFL Info',
						'FLS' , 'Flood Statement',
						'NPW' , 'Non-precipitation Warning',
						'SVR' , 'Severe Thunderstorm Warning',
						'TOR' , 'Tornado Warning',
						'WSW' , 'Winter Storm Warning');

	var $core_path ='';

	Function FetchWarnings($core_path) {
		$this->core_path = $core_path;

	}


	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function watches (&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {
		return $this->warnings($var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function special (&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {
		return $this->warnings($var, $form, $forecast, $cfg, $forecast_info, $debug);
	}

	Function warnings (&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {

		$this->cfg = $cfg;
		$types=0;

		//if ($var["tzname"] && $var["tzdif"]) {
		if (isset($var["tzname"]) && isset($var["tzdif"])) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}
		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

//		$cache_path = $cfg->val('Paths', 'cache');
//		if (!$cache_path) { $cache_path = 'cache'; }
//		elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$this->forecast=$forecast;

		//Grab place info from var hash
		//$country = preg_replace('/[^A-Za-z]/', '', $var["country"]);
		$country = (!empty($var['country'])) ? preg_replace('/[^A-Za-z]/', '', $var["country"]) : '';
		if (!$country) $country = 'us';
		$state = preg_replace('/\W/', '', ( isset($form['state'])) ? $form['state'] :$var["state"]);


		$warning_data =& $this->_fetch_warning($cfg,$var, $forecast, $state, $country, $iwin_file, $ma, $cache_path, $debug);

		if (isset($form['types'])) $types = $form['types'];
		if (!$types) $types = $cfg->val('Warnings Addin', 'types');
		$types = str_replace(' ','',strtoupper($types));

		if (isset($form['active'])) {$active = $form['active'];}
			else {$active = $cfg->val('Warnings Addin', 'active'); }


		if (preg_match('/cap:alert/', $warning_data)) {
			require_once('hamlib/HW3Plugins/Wx/Warnings/CAP.php');
			$this->warnings = new WarningsCAP($debug);
		}
		elseif (preg_match('/<rss [^>\n]+>/', $warning_data)) {
			require_once('hamlib/HW3Plugins/Wx/Warnings/NOAARSS.php');
			$this->warnings = new WarningsNOAARSS($debug);
		}
		else {
			require_once('hamlib/HW3Plugins/Wx/Warnings.php');
			$this->warnings = new Warnings($debug);
		}

		$zone = $var['zone'];
		if (preg_match('/^\w\wZ(\d+)$/', $zone, $matches)) $zone = $matches[1];
		$county = $var['county'];
		if (preg_match('/^\w\wC(\d+)$/', $county, $matches)) $county=$matches[1];

		$this->warning_found = $this->warnings->find_warning($zone, $county, $state, $warning_data, $types);

//print "warning_found =" . $this->warning_found . "<br>\n";
		return $t;


	}


	Function &_fetch_warning (&$cfg, &$vars,$forecast, $state, $country, $iwin_file, $ma, $cache_path, $debug) {
if ($debug) print "_fetch_warning routine<br>\n";

		if (!$forecast) $forecast = 'warnings';
		if (!$country) $country='us';
		if (!$state) $state = 'va';


   		$zone = (isset($vars['zone'])) ? strtoupper($vars['zone']) : '';
   		$county = (isset($vars['county'])) ?  $vars['county'] : '';

   		$wzone = ($zone) ? "$zone," : '';
   		$wzone .= $county;



   		$warncounty = '';
   		$cfips='';
   		if (preg_match('/(\d\d\d)$/', $county, $m)) {
   			$cfips = $m[1];
   			$warncounty = strtoupper($state . 'c' . $cfips);
   		}

		$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'warnings_cache_file'));
		if (!$cache_file) $cache_file = $cache_path . strtolower("/$country-$state-$iwin_file");


		$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'warnings_prefix'));

		list(,$wx_data) = fetch_forecast($cfg,$ma, $cache_file, '','',
			$cfg->val('URLs', 'warnings_url'),
			$prefix,
			$cfg->val('URLs', 'warnings_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
		return $wx_data;
	}


	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		$ft = $this->forecast; if (!$ft) $ft= 'warnings';
		$len = strlen($ft);

		if (substr($key,0,$len) == $ft) {
			$key = 'warning' . substr($key, $len);

			if ($index <> "") {
				$tmp = $this->$key;
				$val = $tmp[$index];
			}
			else {
				$val = $this->$key;
			}

			if ($val == '' && $this->warnings != "") {
				if ($index <> "") {
					$tmp = $this->warnings->$key;
					if (isset($tmp[$index])) $val = $tmp[$index];
				}
				else {
					$val = $this->warnings->$key;
				}
			}
		}


		error_reporting($old_error_reporting);

//print "val=$val<br>\n";
		return $val;
	}


	Function warnings_type_name($num) {
//print "num=$num<br>\n";
		if (isset($this->warnings->warning_wmo_type[$num])) {
			$type = strtoupper($this->warnings->warning_wmo_type[$num]);
//print "type=$type<br>\n";
			if (isset($this->warnings_types[$type])) {return $this->warning_types[$type];}
				else {return $type; }
		}
		else { return '';}

	}


	Function found_warning() {
		if (isset($this->warning_found)) {
			return $this->warning_found;
		}
		else {
			return;
		}
	}


	Function watches_local_expiration($num) {
		return $this->warnings_local_expiration($num);
	}
	Function special_local_expiration($num) {
		return $this->warnings_local_expiration($num);
	}
	Function warning_local_expiration($num) {
		return $this->warnings_local_expiration($num);
	}

	Function special_local_date($num) {
		return $this->warnings_local_date($num);
	}
	Function warning_local_date($num) {
		return $this->warnings_local_date($num);
	}


	Function special_type_name($num) {
		if (isset($this->warnings->warning_wmo_type[$num])) {
			$type = strtoupper($this->warnings->warning_wmo_type[$num]);
			if (isset($this->warnings_types[$type])) {return $this->warning_types[$type];}
				else {return $type; }
		}
		else { return '';}
	}

	Function watches_type_name($num) {
		if (isset($this->warnings->warning_wmo_type[$num])) {
			$type = strtoupper($this->warnings->warning_wmo_type[$num]);
			if (isset($this->warnings_types[$type])) {return $this->warning_types[$type];}
				else {return $type; }
		}
		else { return '';}
	}

	Function warnings_local_date($num) {
		$debug = $this->debug;

		if (isset($this->warnings->warning_date[$num])) {
			$exp = $this->warnings->warning_date[$num];
			if ($debug) print " date = $exp<br>\n";

			if ($this->tzdif && $this->tzname && preg_match('/^(\d?\d)(\d\d) GMT (\w\w\w) (\d\d) (\d\d\d\d)$/', $exp, $m)) {
				$hour = $m[1]; $min = $m[2]; $mname = $m[3]; $day=$m[4]; $year = $m[5];
				$mon = $this->month_names[strtoupper($mname)];

				$jday = jday($mon, $day, $year);
				$wtime = $hour * 60 + $min + $this->tzdif * 60;
				if ($wtime < 0) { $jday--;}
				if ($wtime > 1439) { $jday++;}

				list ($nmon, $nday, $nyear, $nw) = jdate($jday);
				$ntime = $this->fixtzhr($hour, $min, 1, 0);

				# 853 PM CDT MON MAY 5 2003

				return strtoupper($ntime . ' ' . $this->tzname . ' ' . weekDayString($nw) . ' ' . $this->month_nums[$nmon-1] . sprintf(' %02u %04u', $nday, $nyear));
			}
			else {
				return $exp;
			}
		}

		return '';
	}

	Function warnings_local_expiration($num) {
		$debug = $this->debug;

		if (isset($this->warnings->warning_expiration[$num])) {
			$exp = $this->warnings->warning_expiration[$num];
			if ($debug) print "expiration date = $exp<br>\n";

			if ($this->tzdif && $this->tzname && preg_match('/^(\d?\d)(\d\d) GMT (\w\w\w) (\d\d) (\d\d\d\d)$/', $exp, $m)) {
				$hour = $m[1]; $min = $m[2]; $mname = $m[3]; $day=$m[4]; $year = $m[5];
				$mon = $this->month_names[$mname];

				$jday = jday($mon, $day, $year);
				$wtime = $hour * 60 + $min + $this->tzdif * 60;
				if ($wtime < 0) { $jday--;}
				if ($wtime > 1439) { $jday++;}

				list ($nmon, $nday, $nyear, $nw) = jdate($jday);
				$ntime = $this->fixtzhr($hour, $min, 1, 0);

				# 853 PM CDT MON MAY 5 2003

				return strtoupper($ntime . ' ' . $this->tzname . ' ' . weekDayString($nw) . ' ' . $this->month_nums[$nmon-1] . sprintf(' %02u %04u', $nday, $nyear));
			}
			else {
				return $exp;
			}
		}

		return '';
	}


	Function fixtzhr($val, $min=0, $add_colon=0, $time24=0) {
	   $wtime = $val * 60 +$min + $this->tzdif * 60;
	   if ($wtime < 0) $wtime += 1440;
	   if ($wtime > 1439) $wtime -= 1440;
	   $val = ($wtime/60); $val = (int) $val;

	   $min = sprintf('%02u', $wtime % 60);

	   if ($time24) {
	      $val = sprintf('%02u',$val);
	      if ($min != '') { $val .=sprintf('%02u', $min); }
	   }
	   else {
	      if ($val == 12 ) { $val = '12'.$min;  if ($val==12) {$val='12 noon';} else {$val .= ' pm';}}
	      elseif ($val == 0 ) { $val = '12'. $min;  if ($val==12) {$val='12 mid';} else {$val .= ' am';}}
	      elseif ($val < 12) {$val = $val . $min.' AM';}
	      else { $val -=12; $val = $val . $min.' PM'; }
	   }

	   if (!$add_colon)    $val = preg_replace('/^(\d?\d)(\d\d)/', "$1:$2", $val);
	   return strtoupper($val);

	}





}


?>
