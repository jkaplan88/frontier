<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchMOS.php,v 1.26 2008/07/17 14:13:35 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");


class FetchMOS{

	var $debug =0;
	var $cfg;

	var $do_round=1;

	var $clouds = array('CL'=>'Clear','FW'=>'Mostly Sunny', 'SC'=>'Partly Cloudy', 'BK'=>'Mostly Cloudy', 'OV'=>'Cloudy');
	var $precip_types = array('Z'=>'Frozen Precip', 'S'=>'Snow', 'R'=>'Rain');
	var $obv_types = array('FW'=>'Mostly Sunny', 'HZ'=>'Haze', 'BR'=>'Mist','FG'=>'Fog','BL'=>'Blowing Snow', 'F'=>'Fog', 'H'=>'Haze', 'K'=>'Smoke');

	var $mos_placediff = 0;
	var $mos_icao = '';
	var $mos_text='';
	var $mos_found = 0;
	var $tzname = 'UTC';
	var $tzdif = 0;

	var $total_hours = 0;

	var $get_mos = 'avn';

	var $core_path='';

	var $month_names = array('JANUARY', 'FEBURARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER');
	var $weekday_names= array('SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY');


	Function FetchMOS($core_path) {
		$this->core_path = $core_path;

	}

	Function Debug($debug) {
		$this->debug = $debug;
	}

	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		if ($index <> "") {
			$tmp = $this->$key;
			if (isset($tmp[$index])) $val = $tmp[$index];
		}
		else {
			$val = $this->$key;
		}


		error_reporting($old_error_reporting);

		return $val;
	}


	Function ngmmos(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		$this->get_mos = 'ngm';
		return $this->avnmos($var, $form, $forecast, $cfg, $forecast_info, $debug);
	}

	Function etamos(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		$this->get_mos = 'eta';
		return $this->avnmos($var, $form, $forecast, $cfg, $forecast_info, $debug);
	}


	Function avnmos(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		$this->cfg = $cfg;
//		$this->get_mos = 'avn';

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;
		//Grab place info from var hash
		$country = $var["country"];
		$state = strtolower($var["state"]);
		$place = $var["place"];
		$alt_cc_info = $var["alt_cc_info"];

		if (!$country) {$country = 'us';}

		$get_mos = $this->get_mos;
		if (!$get_mos) $get_mos = 'avn';

		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found_hourly = 0;
		$this->found_hourly = $found_hourly;
		$find_place = ""; $code = ""; $find_state = "";
		$cc_areas = explode("%", $alt_cc_info);
		while(list($key, $cc_area) = each($cc_areas)) {
			$ta = explode(":", $cc_area);
			$find_place = $ta[0];
			if (isset($ra[1])) {$code = $ta[1];}
			else { $code = '3';}

			if ($find_place && ($code == 2 || $code == 3)) {
				$find_place = preg_replace('/\W/', "", strtoupper($find_place));
				$var['icao'] = $find_place;
				$place_file = $find_place;
				if ($get_mos == 'ngm') $find_place = preg_replace('/^[Kk]/', '', $find_place);

				$fc_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', $get_mos . 'mos_cache_file'));
				if (!$fc_file) {$fc_file = "$cache_path/$get_mos" . "mos-$place_file.html";}

				$fc_file2 = '';
				$url = '/' . $find_place . '.txt';

				$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', $get_mos .'mos_prefix'));
				//Call Fetch Forcast
				$data = &fetch_forecast($cfg, $ma, $fc_file, $fc_file2,$url,
					$cfg->val('URLs', $get_mos .'mos_url'),
					$prefix,
					$cfg->val('URLs', $get_mos .'mos_postfix'),
					 $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

                      		$which_used = $data[0];
			      	$wx_data = $data[1];

				if ($wx_data != '') {
					require_once("hamlib/HW3Plugins/Wx/MOS.php");
					$this->MOS = new MOS($debug);
					$total = $this->MOS->parse_mos($find_place,  $wx_data);
					$this->mos_text = $wx_data;

					if ($total > 0) {
						$this->mos_icao = $find_place;
						$this->mos_place_diff = 0;
						$this->mos_found = 1;
						$this->start('next');
						break;
					}
				}
			}
		}

		return $t;
	}


	Function breakdown($bd) {
		if ($bd < 1) $bd = 1;
		$this->breakdown = $bd;
	}

	Function start($start) {
		if ($start != 'prev' && $start != 'beginning') $start = 'next';

		$hour = gmdate('H');
		$today = sprintf('%04u%02u%02u%02u',gmdate('Y'), gmdate('m'), gmdate('d'), $hour);
		$start_hour = $this->MOS->get_items_next_hour($today);
		$start_hour_id = $this->MOS->get_id_for_hour($start_hour);

		if ($start_hour_id > 0 && ($hour % 3) > 0) {
			$this->start_hour_id = $start_hour_id -1;
			$this->start_offset = ($hour % 3);
			$this->total_hours = ($this->MOS->total_hours()*3 - $start_hour_id - ($hour % 3)) +1;
		}
		else {
			$this->start_hour_id = $start_hour_id;
			$this->start_offset = 0;
			$this->total_hours = ($this->MOS->total_hours()*3 - 1 - $start_hour_id) +1;
		}
	}


	Function cloud_cover($cloud) {
		$cloud = trim(strtoupper($cloud));
		if (isset($this->clouds[$cloud])) {
			return $this->clouds[$cloud];
		}
		else { return $cloud; }
	}

	Function ceiling_height($code) {

		if ($code ==1) { return '<200';}
		elseif ($code == 2) {return '200 to 400'; }
		elseif ($code == 3) {return '500 to 900'; }
		elseif ($code == 4) {return '1000 to 3000';}
		elseif ($code == 5) {return '3100 to 6500';}
		elseif ($code == 6) {return '6600 to 12000';}
		//elsif ($code ==7 && $cloud eq 'CL') { return 'Unlimited'; }
		elseif ($code == 7) { return '>12000'; }
	}


	Function visibility($code) {

		if ($code ==1) { return '.25';}
		elseif ($code == 2) {return '.5'; }
		elseif ($code == 3) {return '.75'; }
		elseif ($code == 4) {return '3';}
		elseif ($code == 5) {return '5';}
		elseif ($code == 6) {return '6';}
		//elseif ($code ==7 && $cloud eq 'CL') { return 'Unlimited'; }
		elseif ($code == 7) { return '10'; }
	}

	Function obstruction($obv) {
		$obv = trim(strtoupper($obv));
		if (isset($this->obv_types[$obv])) {
			return $this->obv_types[$obv];
		}
		else { return $obv; }
	}

	Function precip($typ) {
		$typ = trim(strtoupper($typ));
		if (isset($this->precip_types[$typ])) {
			return $this->precip_types[$typ];
		}
		else { return $typ; }
	}


	Function mos_date_time() {
		$months = array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
		$result = '';
		if (preg_match('/(\d\d\d\d)(\d\d)(\d\d)/', $this->MOS->mos_date, $m)) {
			$year = $m[1]; $mon = $m[2]; $day = $m[3];
			if (preg_match('/(\d\d):?(\d\d)/', $this->MOS->mos_time, $m)) {
				$hour = $m[1]; $min = $m[2];
				$tzname = $this->tzname; $tzdif = $this->tzdif;
				$td = getdate(gmmktime($hour, $min, 0, $mon, $day, $year));
				$mon = $td['mon']-1;
				$result = $months[$mon] . ' ' . sprintf('%02u %04u %02u:%02u ', $td['mday'], $td['year'], $td['hours'], $td['minutes']) . ' ' . strtoupper($tzname);
			}
		}

		return $result;
	}


	Function mos_wx($hour_id, $return_raw=0) {
		$debug = $this->debug;

		$wx = 'N/A';

		if ($this->get_mos_item($hour_id, 'P06') > 30) {
			$typ = $this->precip($this->get_mos_item($hour_id, 'TYP'));

			if (!$typ) {
				$tmp = $this->get_mos_item($hour_id, 'TMP');
				if ($tmp == '' || $tmp = '**') {
					$typ = $this->precip('R');
				}
				elseif ($tmp> 32) {
					$typ = $this->precip('R');
				}
				else {
					$typ = $this->precip('S');
				}
			}
			if ($typ) $wx = $typ;
		}
		else {
			$obv = $this->obstruction($this->get_mos_item($hour_id, 'OBV'));
			if ($obv && $obv != 'N') {
				$wx =  $obv;
			}
			else {
				$cld = $this->cloud_cover($this->get_mos_item($hour_id, 'CLD'));
				if ($cld) $wx=$cld;
			}
		}

		if ($debug) print "return_raw = $return_raw";
		if ($return_raw) {
			$res = $wx;
		}
		else {
			$wx_info = $this->get_wx_item($wx);
			if (!$wx_info) { $wx_info = $self->{cfg_}->val('WxInfo', 'N/A'); }
			 $wxdata = explode('|', $wxinfo);
			$res = $wxdata[0];
		}

		return $res;
	}


	Function get_wx_item ($wx) {
		$wxinfo='';

		$wxparms = $this->cfg->Parameters('WxInfo');
		while (list($key,$wxitem) = each($wxparms)) {
			$wxitem = str_replace('/','\/',$wxitem);
			if (preg_match("/$wxitem/i", $wx)) {
				$wxinfo = $this->cfg->Val('WxInfo', $wxitem);
				break;
			}
		}

		return $wxinfo;
	}


	Function fixtzhr($val, $min=0, $add_colon=0, $time24=0) {
	   $wtime = $val * 60 +$min + $this->tzdif * 60;
	   if ($wtime < 0) $wtime += 1440;
	   if ($wtime > 1439) $wtime -= 1440;
	   $val = ($wtime/60); $val = (int) $val;

	   $min = sprintf('%02u', $wtime % 60);

	   if ($time24) {
	      $val = sprintf('%02u',$val);
	      if ($min != '') { $val .=sprintf('%02u', $min); }
	   }
	   else {
	      if ($val == 12 ) { $val = '12'.$min;  if ($val==12) {$val='12 noon';} else {$val .= ' PM';}}
	      elseif ($val == 0 ) { $val = '12'. $min;  if ($val==12) {$val='12 mid';} else {$val .= ' AM';}}
	      elseif ($val < 12) {$val = $val . $min.' AM';}
	      else { $val -=12; $val = $val . $min.' PM'; }
	   }

	   if (!$add_colon)    $val = preg_replace('/^(\d?\d)(\d\d)/', "$1:$2", $val);
	   return $val;

	}


	Function get_mos_item($hour_id, $item) {
		$item = strtoupper($item);

		$val = '';
		$orig_hour_id = $hour_id;

		$start_hour_id = $this->start_hour_id;
		$offset = $this->start_offset;
		$item3 = substr($item, 0, 3); if ($item3 == 'HR_') $item3 = 'HR';

		if (is_int(strpos('WEE WDA DAY MON YEAR', $item3))) {
			return $this->mos_date_info($hour_id, $item);
		}


		if (--$hour_id < 0 || $hour_id > ($this->total_hours-1)) return '';
		$hour_id += $offset;

		if ($hour_id % 3 == 0) {
			$val = $this->MOS->get_item($hour_id/3 + $start_hour_id, $item3);
			if (is_int(strpos('HR WSP TMP DPT', $item3))) { $val = ($val == '') ? '**' : $val+0;}
			if ($val == 999 || ($val == 99 && (strpos('TMP DPT P06 P12 Q06 Q12 T06 T12', $item3 === false)))) $val = '**';
		}
		else {
			$x1 = $this->MOS->get_item(floor($hour_id/3) + $start_hour_id, $item3);
			$x2 = $this->MOS->get_item(floor($hour_id/3) + 1 + $start_hour_id, $item3);

			if (is_int(strpos('HR WSP TMP DPT', $item3)) && ($x1 == '' || $x2 == '')) {
				$val = '**';
			}
			elseif ($x1 == 999 || $x2 == 999 || (($x2 == 99 || $x1 == 99) && (strpos('TMP DPT  P06 P12 Q06 Q12 T06 T12', $item3) === false))) {
				$val = '**';
			}
			elseif(!(strpos(' WSP CLD TYP OBV', $item3) === false)) {
				$val = (($hour_id % 3) < (3/2)) ? $x1 : $x2;
			}
			elseif($item3 == 'HR') {
				$val = $x1 + ($hour_id % 3);
         				if ($val > 23) $val -= 24;
	         		}
	         		elseif(!(strpos('P06 P12 T06 T12 Q06 Q12 N_X', $item3) === false)){
	         			$val = $x2;
	         		}
	         		else {
				$val = ($x2 - $x1) / 3 * ($hour_id % 3) + $x1;
				if ($val > 0) { $val = floor($val+.5); } elseif ($val < 0) {$val = floor($val -.5);}
			}
		}
//print "item=$item...val=$val<br>\n";
		if ($item == 'CLD_FULL') { $val = $this->cloud_cover($val);}
		elseif ($item == 'CIG_FULL') {$val = $this->ceiling_height($val); }
		elseif ($item == 'VIS_FULL') { $val = $this->visibility($val);}
		elseif ($item == 'OBV_FULL') { $val = $this->obstruction($val);}
		elseif ($item == 'TYP_FULL') {
			if ($this->get_mos_item($orig_hour_id, 'P06') >20) {
				if (!$val) {
					$tmp = $this->get_mos_item($hour_id, 'TMP');
					if ($tmp == '' || $tmp = '**') {
						$val = $this->precip('R');
					}
					elseif ($tmp> 32) {
						$val = $this->precip('R');
					}
					else {
						$val = $this->precip('S');
					}
				}
				else {
					$val = $this->precip($val);
				}
			}
			else {$val = '-';}
		}
		elseif ($item == 'WDR_FULL') { $val= $this->wind_dir($val); }
		elseif ($item == 'HR_FULL') { $val = $this->fixtzhr($val);}


		if ($item3 == 'WSP' && strval($val) == '00') $val = '0';
		return $val;

	}


	Function wind_dir($dir) {

		$dir *= 10;

		$dir_eng='';
		if  ($dir < 15) {$dir_eng = "N";}
		elseif ($dir < 30) {$dir_eng = "NNE";}
		elseif ($dir < 60) {$dir_eng = "NE";}
		elseif ($dir < 75) {$dir_eng = "ENE";}
		elseif ($dir < 105) {$dir_eng = "E";}
		elseif ($dir < 120) {$dir_eng = "ESE";}
		elseif ($dir < 150) {$dir_eng = "SE";}
		elseif ($dir < 165) {$dir_eng = "SSE";}
		elseif ($dir < 195) {$dir_eng = "S";}
		elseif ($dir < 210) {$dir_eng = "SSE";}
		elseif ($dir < 240) {$dir_eng = "SW";}
		elseif ($dir < 265) {$dir_eng = "SSW";}
		elseif ($dir < 285) {$dir_eng = "W";}
		elseif ($dir < 300) {$dir_eng = "WNW";}
		elseif ($dir < 330) {$dir_eng = "NW";}
		elseif ($dir < 345) {$dir_eng = "NNW";}
		else {$dir_eng = "N";}

		return $dir_eng;
	}

	Function total_hours(){
		return $this->total_hours;
	}

	Function mos_rh($hour_id) {

		$tmp = trim($this->get_mos_item($hour_id, 'TMP'));
		$dpt = trim($this->get_mos_item($hour_id, 'DPT'));
		if ($tmp == '' || $dpt == '' || $tmp =='**' || $dpt == '**') return 'N/A';

		$tmpc = ($tmp-32)*5/9;
		$dptc = ($dpt-32)*5/9;

		$Es=6.11*pow(10.0,(7.5*$tmpc/(237.7+$tmpc))) ;
		$E=6.11*pow(10.0,(7.5*$dptc/(237.7+$dptc)));

		$rh = floor($E/$Es*100+.5); if ($rh > 100) $rh = 100;
		if (!$Es) { return 'N/A'; }
		else {return $rh; }
	}


	Function mos_wx_icon($hour_id) {
		$wx = strtoupper($this->mos_wx($hour_id, 1));

		$wxinfo = $this->get_wx_item($wx);
		if ($wxinfo == '') { $wxinfo = $this->cfg->val('WxInfo', 'N/A'); }

		$wxdata = explode('|', $wxinfo);

		$hour = $this->get_mos_item($hour_id, 'HR') + $this->tzdif;
		if ($hour < 0) $hour += 24;
		if ($hour > 23) $hour -= 24;

		$bday = $this->cfg->val('Defaults', 'day begin');
		$eday = $this->cfg->val('Defaults', 'day end');

		if ($bday < $eday && $hour >= $bday && $hour < $eday) {
			return $wxdata[1];
		}
		else {
			return $wxdata[2];
		}
	}

	Function mos_hi ($hour_id) {
		$tmp = trim($this->get_mos_item($hour_id, 'TMP'));
		$rh = $this->mos_rh($hour_id);
		$hi = $this->_heat_index($tmp,$rh);

		if ($hi == '**') { return $hi; }
		elseif ($hi < $tmp) { return $tmp; }
		else { return $hi;}
	}


	Function mos_wc ($hour_id) {
		$tmp = trim($this->get_mos_item($hour_id, 'TMP'));
		$wsp = trim($this->get_mos_item($hour_id, 'WSP'));
		$wc = $this->_wind_chill($tmp,$wsp);

		if ($wc == '**') { return $wc; }
		elseif ($wc > $tmp) { return $tmp; }
		else { return $wc;}
	}



	Function _wind_chill($temp, $wind, $old = "") {
		if ($temp == '**' || $wind == '**') return '**';

		if (preg_match("/(\d+)/", $wind, $matches)) {
			$wind = $matches[1];
		}
		else {
			$wind = 0;
		}
		$wc = "";
		$wind = intval($wind);
		if ($wind == 0) {
		   $wc = $temp;
		}
		elseif ($old == "") {
			$wc = 35.74+0.6215 * $temp - 35.75 * pow($wind,0.16) + 0.4275 * $temp * pow($wind, 0.16);
		}
		else {
			$wc=0.0817*(3.71*pow($wind,0.5) + 5.81 - 0.25*$wind)*($temp - 91.4) + 91.4;
		}
		if ($this->do_round) {
			$wc = round($wc);
		}

		if ($wc > $temp) $wc = $temp;
		return $wc;
	}

	Function _heat_index($temp, $rh) {
		if ($temp == '**' || $rh == '**') return '**';
		$temp = intval($temp); $rh = intval($rh);

		$hi = -42.379 + 2.04901523*$temp + 10.14333127*$rh - 0.22475541*$temp*$rh - (6.83783E-03)*pow($temp,2) - (5.481717E-02)*pow($rh,2) + (1.22874E-03)*pow($temp,2)*$rh + (8.5282E-04)*$temp*pow($rh,2) - (1.99E-06)*pow($temp,2)*pow($rh,2);

		if ($this->do_round) {
			$hi = round($hi);
		}

		if ($hi < $temp || $temp < 70)  $hi = $temp;
		return $hi;
	}

	Function mos_date_info ($hour_id, $item='') {
		$item = strtoupper($item);

		$start_hour_id = $this->start_hour_id;
		$offset = $this->start_offset;
		$item3 = substr($item, 0, 3);

		if (--$hour_id < 0 || $hour_id > ($this->total_hours-1)) return '';
		$hour_id += $offset;

//		if ($hour_id % 3 != 0) { $useid = floor($hour_id/3)*3; }
//		else { $useid = $hour_id; }
		$useid = floor($hour_id/3) + $start_hour_id;

		$fulldate = $this->MOS->get_datehour_for_id($useid);
		if (preg_match('/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)$/', $fulldate, $m)) {
			$year = $m[1]; $mon=$m[2]; $day=$m[3]; $hr = $m[4];
			$mon +=0;

			// lets get the local date info
			$wtime = $hr * 60 + $this->tzdif * 60;
			$hour = floor($wtime/60) + $hour_id % 3;
#print "hour_id=$hour_id...fulldate=$fulldate...hour=$hour<br>\n";
			if ($hour < 0) {
				$hour += 24;$day--;
				if ($day < 1) {
					$mon--;
					if ($mon < 0) { $mon == 12; $year--;}
					if (preg_match('/^4|6|9|11$/', $mon)) { $day = 30;}
					elseif ($mon == 2) {
						if ($year % 4 != 0) { $day = 28;}
						else {$day = 29;}
					}
					else {$day = 31;}
				}
			}
			elseif ($hour > 23) {
				$hour -= 24; $day++;
			}

			if (($day > 31) ||($day > 30 && preg_match('/^4|6|9|11$/', $mon)) || ($mon == 2 && (($day >28  && $year % 4 != 0) || ($day >29  && $year % 4 == 0)))) {
				$day = 1;
				$mon++;
				if ($mon > 12) { $mon =1; $year++; }
			}
#print "...hour=$hour...day=$day<br>\n";
			if ($item3 == 'DAY') { return sprintf('%02u',$day); }
			elseif ($item3 == 'YEA') { return $year; }
			elseif ($item3 == 'MON') {
				if (is_int(strpos($item, '_FULL'))) {
					return $this->month_names[$mon-1];
				}
				elseif (is_int(strpos($item, '_SHORT'))) {
					return substr($this->month_names[$mon-1], 0, 3);
				}
				else { return sprintf('%02u',$mon);}
			}
			elseif ($item3 == 'WDA' || $item3 == 'WEE') {
				$wday = $this->weekday($year,$mon,$day);
				if (is_int(strpos($item, '_FULL'))) {
					return $this->weekday_names[$wday];
				}
				elseif (is_int(strpos($item, '_SHORT'))) {
					return substr($this->weekday_names[$wday], 0, 3);
				}
				else { return $wday; }
			}
			else { return '';}
		}
		else { return '';}
	}

	Function weekday ($year, $mon, $day) {

		$mon -= 2;
		if ($mon <= 0) {--$year;	$mon += 12;}

		 $cent = floor($year/100);
		$year %= 100;

		 $wday = ($day + floor(2.6*$mon - 0.2)	- 2*$cent + $year + floor($year/4) + floor($cent/4));
		while ($wday < 0) { $wday += 7; }
		$wday %= 7;

		return $wday;
	}


}

?>