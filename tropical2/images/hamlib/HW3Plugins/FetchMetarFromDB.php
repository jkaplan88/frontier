<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchMetarFromDB.php,v 1.1 2008/07/17 14:13:35 wxfyhw Exp $
*
*/


class FetchMetarFromDB {

	var $cfg;
	var $debug = false;

	var $data = array();

	var $db_table='';
	var $db_maxage=0;




	Function FetchMetarFromDB($cfg, $debug) {
		$this->cfg =& $cfg;
		$this->debug = $debug;

		$this->db_username  = $cfg->val('Database Access', 'username'); # Username.
		$this->db_password  = $cfg->val('Database Access', 'password'); # Password.
		$this->db_hostname  = $cfg->val('Database Access', 'hostname'); # Hostname.
		$this->db_database  = $cfg->val('Database Access', 'database_name'); # Database name.

		if ($cfg->val('HWdata', 'database_name')) $this->db_database = $cfg->val('HWdata', 'database_name') ;			if ($cfg->val('HWdata', 'hostname')) $this->db_hostname = $cfg->val('HWdata', 'hostname') ;
		if ($cfg->val('HWdata', 'username')) $this->db_username = $cfg->val('HWdata', 'username') ;
		if ($cfg->val('HWdata', 'password')) $this->db_password = $cfg->val('HWdata', 'password') ;


		if ($cfg->val('METAR', 'database_name')) $this->db_database = $cfg->val('METAR', 'database_name') ;
		if ($cfg->val('METAR', 'hostname')) $this->db_hostname = $cfg->val('METAR', 'hostname') ;
		if ($cfg->val('METAR', 'username')) $this->db_username = $cfg->val('METAR', 'username') ;
		if ($cfg->val('METAR', 'password')) $this->db_password = $cfg->val('METAR', 'password') ;



	    $dbtable = $cfg->val('METAR', 'database_table'); # Database name.
	    $this->db_table = ($dbtable) ? $dbtable : 'hwdata_currents';

	    $ma = $cfg->val('METAR', 'db_maxage')+0;
	    $this->db_maxage = $ma;


	}


	Function db_establish($database, $dbhostname, $dbusername, $dbpassword) {
		$dbh = null;

		$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword,1) or die(mysql_error());
		mysql_select_db($database,$dbh) or die(mysql_error());


		return $dbh;
	}



Function fetch_metar($icao, $icao2='') {

	$error = '';
	$isold = 1;
	$debug = $this->debug;

	$error = true;


	$data = array();
   	$data['hsky'] = $data['hsky_conditions'] = $data['hweather'] =  $data['htempf'] = $data['hdewptf'] = $data['htempc'] = $data['hdewptc'] =
   	$data['hrh'] = $data['hwind'] = $data['hwinddir'] = $data['hwindspd'] =
     $data['hwcf'] = $data['hhif'] = $data['hwcc'] = $data['hhic'] = $data['hpressure'] =
     $data['hpressure_mb'] = $data['hremarks'] = $data['hvisibility'] =
     $data['hquk'] = $data['hqul'] = $data['metar'] = $data['gusts'] = $data['hwinddir_deg'] = $data['hvisibility_miles'] =
     $data['hvisibility_meters'] = $data['hsixhrhif'] = $data['hsixhrlof'] =
     $data['h24hrhif'] = $data['h24hrlof'] = $data['hsixhrhi_c'] = $data['hsixhrlo_c'] = $data['h24hrhi_c'] =
     $data['h24hrlo_c'] = $data['hthrhrprec'] = $data['hsixhrprec'] = $data['h24hrprec'] ='N/A';


	$icao =preg_replace('/\W/','',$icao);



	if ($icao) {
		$dbh = $this->db_establish( $this->db_database, $this->db_hostname,
		          $this->db_username,$this->db_password);
		if (!$dbh) {
			return array($error, $isold);
		}

		$sql = 'SELECT Temperature,DewPoint,RelativeHumidity,WindSpeed,WindDirection,WindDirectionEng,WindGust,Pressure,Weather,Clouds,WeatherIcon,Visibility,HeatIndex,WindChill,MaxTempSixHr,MinTempSixHr,MaxTemp24Hr,MinTemp24Hr,PrecipOneHr,PrecipThreeHr,PrecipSixHr,Precip24Hr,ReportEpoch,ReportDate FROM ' . $this->db_table . " WHERE ICAO='%%ICAO%%'";

		$ma =$this->db_maxage;
		if ($ma > 0) {
			$now = time();
			$now -= $ma * 60; # get the current time minus the max age converted to seconds
			$sql .= " AND ReportEpoch >= $now";
		}




		$tsql = preg_replace('/%%ICAO%%/', $icao, $sql);
		if ($debug) print "tsql=$tsql<br/>";
		$result = mysql_query($tsql, $dbh);
		$row = mysql_fetch_row($result);

		if ( (empty($row) || !is_numeric($row[1]) || $row[1] > 200) && $icao2) {
			$tsql = preg_replace('/%%ICAO%%/', $icao2, $sql);

			if ($debug) print "tsql=$tsql<br/>";
			$result = mysql_query($tsql, $dbh);
			$row = mysql_fetch_row($result);
			if (!empty($row)) $icao = $icao2;
		}


		if (empty($row)) {
			return array('not in db ' . mysql_error() , 1, $icao);
		}
		else {


			list ($Temperature, $DewPoint, $RelativeHumidity, $WindSpeed, $WindDirection, $WindDirectionEng, $WindGust, $Pressure, $Weather, $Clouds, $WeatherIcon, $Visibility, $HeatIndex, $WindChill, $MaxTempSixHr, $MinTempSixHr, $MaxTemp24Hr, $MinTemp24Hr, $PrecipOneHr, $PrecipThreeHr, $PrecipSixHr, $Precip24Hr, $ReportEpoch, $ReportDate) = $row;

			$data['hforecastdate'] = gmdate('Y.m.d Hi', $ReportEpoch) . ' UTC';

			$sql = "SELECT name, state, country FROM icaos WHERE icao='$icao'";
			$result2 = mysql_query($sql, $dbh);
			$row = mysql_fetch_row($result2);
			if (sizeof($row) > 0) {
				list($data['hplace'], $data['hstate'], $data['hcounty']) = $row;
			}
			$result2 = null;
			if ($debug) print "($Temperature, $DewPoint, $RelativeHumidity, $WindSpeed, $WindDirection, $WindDirectionEng, $WindGust, $Pressure, $Weather, $Clouds, $WeatherIcon, $Visibility, $HeatIndex, $WindChill, $MaxTempSixHr, $MinTempSixHr, $MaxTemp24Hr, $MinTemp24Hr, $PrecipOneHr, $PrecipThreeHr, $PrecipSixHr, $Precip24Hr, $ReportEpoch, $ReportDate)<br>";


			if ($Temperature != '' && $Temperature != 9999) {
				$data['htempc'] = $Temperature;
				$data['htempf'] = $this->C2F($Temperature);
			}


			if ($DewPoint != '' && $DewPoint != 9999) {
				$data['hdewptc'] = $DewPoint;
				$data['hdewptf'] = $this->C2F($DewPoint);
			}

			if ($RelativeHumidity != '' && $RelativeHumidity != 9999) {
				$data['hrh'] = $RelativeHumidity;
			}


			$hwind='';
			if ($WindSpeed == '' || $WindSpeed == 9999) {
				$hwind = $data['hwindspd'] = 'N/A';
			}
			else {
				$data['hwindspd'] = intval($WindSpeed/0.868391 +.5);
				$hwind = ($WindSpeed == 0) ? 'calm' : $data['hwindspd'];
				$data['hwindspd_knots'] = $WindSpeed;
			}

			$data['hwinddir_deg'] = ($WindDirection) ? $WindDirection : 'N/A';
			$data['hwinddir'] = ($WindDirectionEng) ? $WindDirectionEng : 'N/A';

			if ($hwind && $hwind > 0) { $hwind = $WindDirectionEng . ' ' . $hwind; }

			if ($WindGust != '' && $WindGust != 9999) {
				$data['gusts'] = intval($WindGust/0.868391 +.5);
				if ($WindGust > 0) {$hwind .= ' G ' . $data['gusts'];}
				$data['gusts_knots'] = $WindGust;
			}

			$data['hwind'] = (preg_match('/\d/', $hwind)) ? $hwind . ' mph' : $hwind;

			if ($Pressure != '' && $Pressure != 9999) {
				$data['hpressure_mb'] = $Pressure;
				$data['hpressure'] = intval($Pressure/33.8639 * 100+.5)/100;
			}

			if ($HeatIndex != '' && $HeatIndex != 9999) {
				$data['hhic'] = $HeatIndex;
				$data['hhif'] = $this->C2F($HeatIndex);
			}
			if ($WindChill != '' && $WindChill != 9999) {
				$data['hwcc'] = $WindChill;
				$data['hwcf'] = $this->C2F($WindChill);
			}

			if ($Visibility != '' ) {
				$data['hvisibility'] = $Visibility;

				$vis = trim(preg_replace('/[^\d\s\/]/', '', $Visibility));
				$vis = preg_replace('/\s+/', '+', $vis);

				if ($vis) eval ("\$vis = $vis;");
				if (preg_match('/km/i', $Visibility)) {$vis *= 1000;}

				if (preg_match('/SM/i', $Visibility)) {
					$data['hvisibility_miles'] = $vis;
					$data['hvisibility_meters'] = $vis*1609;
				}
				else {
					$data['hvisibility_meters'] = $vis;
					$data['hvisibility_miles'] = $vis/1000*0.621371;
				}

			}

			if ($Weather != '') {
				$data['hweather'] = $data['hweather'] = $Weather;
			}
			if ($Clouds != '') {
				$data['hsky_conditions'] = $data['hsky_conditions'] = $Clouds;
			}
			$data['hsky'] = $data['hsky'] = ($Weather) ? $Weather : (($Clouds) ? $Clouds : 'fair');


			if ($MaxTempSixHr != '' && $MaxTempSixHr != 9999) {
				$data['hsixhrhi_c'] = $MaxTempSixHr;
				$data['hsixhrhif'] = $this->C2F($MaxTempSixHr);
			}
			if ($MinTempSixHr != '' && $MinTempSixHr != 9999) {
				$data['hsixhrlo_c'] = $MinTempSixHr;
				$data['hsixhrlof'] = $this->C2F($MinTempSixHr);
			}
			if ($MaxTemp24Hr != '' && $MaxTemp24Hr != 9999) {
				$data['h24hrhi_c'] = $MaxTemp24Hr;
				$data['h24hrhif'] = $this->C2F($MaxTemp24Hr);
			}
			if ($MinTemp24Hr != '' && $MinTemp24Hr != 9999) {
				$data['h24hrlo_c'] = $MinTemp24Hr;
				$data['h24hrlof'] = $this->C2F($MinTemp24Hr);
			}

			if ($PrecipOneHr != '' && $PrecipOneHr != 9999) {
				$data['honehrprec'] = $PrecipOneHr;
			}
			if ($PrecipThreeHr != '' && $PrecipThreeHr != 9999) {
				$data['hthrhrprec'] = $PrecipThreeHr;
			}
			if ($PrecipSixHr != '' && $PrecipSixHr != 9999) {
				$data['hsixhrprec'] = $PrecipSixHr;
			}
			if ($Precip24Hr != '' && $Precip24Hr != 9999) {
				$data['h24hrprec'] = $Precip24Hr;
			}

			$isold=0;
			$error = false;

			$this->data =& $data;

			foreach ($data as $key => $val) {
				$this->$key = $val;
			}



		}

		mysql_close($dbh);
	}

	return array($error,$isold, $icao);


}


function C2F ($tempc) {
	if (!preg_match('/\d/',$tempc)) return $tempc;
	return round($tempc * 9/5+32);
}





Function GetValue($key, $index=null) {
		return  (isset($this->data[$key])) ? $this->data[$key] : '';
	}
}
?>