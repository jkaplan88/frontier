<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2007 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchTropModel.php,v 1.2 2007/09/23 22:09:52 wxfyhw Exp $
*
*/

require_once(HAMLIB_PATH . '/HW3Plugins/HW3PluginAPI.php');
require_once(HAMLIB_PATH . '/HW3Plugins/Wx/Tropical/ABRDeck.php');

class FetchTropModel extends HW3PluginAPI{

	var $ABRDeck = null;
	var $num_models = 0;


	var $model_items = array(
		'ABRD_BASIN'=> 0,
		'ABRD_CY'=> 1,
		'ABRD_DATE'=> 2,
		'ABRD_TECHNUM'=> 3,
		'ABRD_TECH'=> 4,
		'ABRD_TAU'=> 5,
		'ABRD_LatNS'=> 6,
		'ABRD_LonEW'=> 7,
		'ABRD_VMAX'=> 8,
		'ABRD_MSLP'=> 9,
		'ABRD_TY'=> 10,
		'ABRD_RAD'=> 11,
		'ABRD_WINDCODE'=> 12,
		'ABRD_RAD1'=> 13,
		'ABRD_RAD2'=> 14,
		'ABRD_RAD3'=> 15,
		'ABRD_RAD4'=> 16,
		'ABRD_RADP'=> 17,
		'ABRD_RRP'=> 18,
		'ABRD_MRD'=> 19,
		'ABRD_GUSTS'=> 20,
		'ABRD_EYE'=> 21,
		'ABRD_SUBREGION'=> 22,
		'ABRD_MAXSEAS'=> 23,
		'ABRD_INITIALS'=> 24,
		'ABRD_DIR'=> 25,
		'ABRD_SPEED'=> 26,
		'ABRD_STORMNAME'=> 27,
		'ABRD_DEPTH'=> 28,
		'ABRD_SEAS'=> 29,
		'ABRD_SEASCODE'=> 30,
		'ABRD_SEAS1'=> 31,
		'ABRD_SEAS2'=> 32,
		'ABRD_SEAS3'=> 33,
		'ABRD_SEAS4'=> 34,
		'ABRD_TAUEPOCH'=> 35,
	);




	Function FetchTropModel($core_path) {
		parent::HW3PluginAPI($core_path);
	}

	Function trop_model_data (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		parent::main($var, $form, $forecast, $cfg, $forecast_info, $debug);


		$t = "";
		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$region =  (isset($form['tregion'])) ? strtoupper($form['tregion']) : strtoupper(preg_replace('/\W/', '', $form['region']));
		$cyear = $form['year'];
		if (!$cyear) $cyear = date('Y');

		$region =  (isset($form['region'])) ? strtoupper($form['region']) : 'NT';
		$region = preg_replace('/\W/', '', $region);
		if (!$region) $region='NT';
		$lc_region = strtolower($region);

		$regionname = $regionabbrev = $regionnamens = $regionofficefull = '';

		if ($region == 'NT') {
			$regionnamens = $regionname = $this->regionname = 'atlantic';
			$regionabbrev = $this->regionabbrev = 'at';
			$regioncode = 'al';
			$regionoffice = 'NHC';
			$regionofficefull = 'knhc';
		}
		elseif ($region == 'PZ') {
			$regionname = $this->regionname = 'eastern pacific';
			$regionabbrev = $this->regionabbrev = 'ep';
			$regionnamens = 'eastern_pacific';
			$regioncode = 'ep';
			$regionoffice = 'NHC';
			$regionofficefull = 'knhc';
		}
		elseif ($region == 'PA') {
			$regionname = $this->regionname = 'central pacific';
			$regionabbrev = $this->regionabbrev = 'cp';
			$regionnamens = 'central_pacific';
			$regioncode = 'cp';
			$regionoffice = 'HFO';
			$regionofficefull = 'phfo';
		}

		$event = (isset($form['event'])) ? $form['event'] : (isset($form['eventnum'])) ? $form['eventnum'] : '';
		$eventnum2 = sprintf("%02u", $event);

		$event = preg_replace('/\D/', '', $event);

		$uc_regionabbrev = strtoupper($regionabbrev);
		$uc_regioncode = strtoupper($regioncode);

		$cache_hash = array('cache_path'=> $cache_path, 'lc_region' => strtolower($region),
				'regionnamens'=>$regionnamens, 'regionname'=>$regionname, 'regionabbrev' => $regionabbrev, 'regionofficefull'=>$regionofficefull,
				'regioncode' => $regioncode, 'uc_regioncode' => $uc_regioncode, 'eventnum2' => $eventnum2, 'archivesub' => ( ($cyear > 2005) ? "$regioncode$eventnum2" : 'dis'),
				 'event'=>$event, 'year' => $cyear);
		$hashes = array(&$cache_hash, $var, $form);


		$prefix = preg_replace("/%%(\w+)%%/e", "\$this->_switchvars('$1', \$hashes);", $cfg->Val('URLs', 'track_prefix'));
		$domain = $cfg->val('URLs', 'track_url');
		$postfix = $cfg->val('URLs', 'track_postfix');

		$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->_switchvars('$1', \$hashes);", $cfg->Val('URLs', 'track_cache_file'));
		$cache_file = preg_replace('/(?:\.\.+|\&|>|<)/', '', $cache_file);
		if ($cache_file && !preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path)) { $cache_file  = $cache_path . '/' . $cache_file;}
		$cache_file2 = '';

		if ($debug) print "cache_file = $cache_file<br>\n";


		$wx_data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$domain,
			$prefix,
			$postfix, $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
		list($which_used, $data) = $wx_data;


		$this->ABRDeck = new TropABRDeck($this->debug);
		$this->num_models = $this->ABRDeck->parse($data);




		return $t;

	}


	function modelitem ($num,$item) {
		$val = '';

		if ($num >=0 && $this->ABRDeck && preg_match('/^([A-Z0-9]+)_(\w+)$/', $item,$m)) {
			$tech = $m[1];
			$item = $m[2];

			if (isset($this->model_items[$item]) && $this->ABRDeck->tech_exists($tech)) {
				$row = $this->ABRDeck->get_track($tech,$num);
				if (!empty($row) && isset($row[$this->model_items[$item]])) $val = $row[$this->model_items[$item]];
			}
		}

		return $val;

	}

	function modeltrack_total ($num,$tech) {

		if ($tech && $this->ABRDeck) return $this->ABRDeck->get_track_total($tech);
		else return '';
	}


}


	?>