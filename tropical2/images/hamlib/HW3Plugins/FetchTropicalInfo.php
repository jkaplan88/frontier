<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchTropicalInfo.php,v 1.1 2007/09/20 03:19:23 wxfyhw Exp $
*
*/

require_once(HAMLIB_PATH . '/HW3Plugins/FetchWxData.php');


class FetchTropicalInfo {

	var $cfg;



	var $debug = 0;
	var $core_path = '';

	var $stormyear = array();
	var $stormregion = array();
	var $stormeventnum = array();
	var $stormnoaaeventnum = array();
	var $stormisactive = array();
	var $stormtype = array();
	var $stormname = array();
	var $stormadvisorynum = array();
	var $stormadvisorydate = array();
	var $stormadvisoryepochdate = array();
	var $stormlatdec = array();
	var $stormlatdir = array();
	var $stormlondec = array();
	var $stormlondir = array();
	var $stormdirection = array();
	var $stormspeed_kts = array();
	var $stormpressure_mb = array();
	var $stormwind_kts = array();
	var $stormspeed_mph = array();
	var $stormpressure_in = array();
	var $stormwind_mph = array();
	var $stormmaxname = array();
	var $stormmaxwind = array();

	var $stormstartdate = array();
	var $stormenddate = array();


	var $stormeventsecondarynum = array();
	var $stormsecondaryregion = array();
	var $stormshortname = array();
	var $stormcategory = array();

	var $stormminpressure_in = array();
	var $stormminpressure_mb = array();

//	var $stormstartdate = array();
//	var $stormenddate = array();



	var $tropyears = array();
	var $tropyears_total =0;



	var $tropical= NULL;


	Function FetchTropicalInfo($core_path) {
		$this->core_path = $core_path;
	}

	Function GetValue($key, $index=NULL) {
		$old_error_reporting = error_reporting(0);
		$val = NULL;
		if ($index  || $index == '0') {

			if (isset($this->$key)) {
				$tmp =& $this->$key;
				if(isset($tmp[$index])) $val = $tmp[$index];
			}

			if (is_null($val) &&  isset($this->tropical)) {
				if (isset($this->tropical->$key)) {
					$tmp =&  $this->tropical->$key;

					if (isset($tmp[$index])) {
						$val = $tmp[$index];
					}
				}
			}


		}
		else {
			if (isset($this->$key) && !is_array($this->$key)) $val = $this->$key;
			if ($val == '' && isset($this->tropical->$key)) $val = $this->tropical->$key;
		}

		error_reporting($old_error_reporting);


		return $val;
	}


	function switchvars ($str, &$hashes) {

		$val = '';
		if (substr($str,0,3) == 'lc_') { $val = strtolower($this->switchvars(substr($str,3), $hashes)); }
		elseif (substr($str,0,3) == 'uc_') { $val = strtoupper($this->switchvars(substr($str,3), $hashes)); }
		else {
			foreach($hashes as $hash) {
				if (is_array($hash) && isset($hash[$str])) {
					$val = $hash[$str];
					if ($val !='') break;
				}
			}
		}
		if ($val == '') $val = $this->GetValue($str,'');

		return $val;
	}



	Function trop_forecast ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (2, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_advisory ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (3, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_discussion ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (4, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_position_est ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (5, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_cyclone_update ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (6, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_probabilities ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (7, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_vtec ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (8, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}
	Function trop_windprob ( &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {
		return $this->tropical_data (9, $var, $form, $forecast, $cfg, $forecast_info, $debug);
	}


	Function tropical_data ($data_type, &$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$t = "";
		$this->cfg =& $cfg;

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;


		if ($data_type < 1 || $data_type > 7) {$data_type = $cfg->val('TropicalInfo', 'default_data_type'); }
		$data_type = preg_replace('/\D/', '', $data_type);
		if (!$data_type) $data_type = 1;

		$cyear = $form['year'];
		if (!$cyear) $cyear = date('Y');

		$region =  (isset($form['region'])) ? strtoupper($form['region']) : 'NT';
		$region = preg_replace('/\W/', '', $region);
		if (!$region) $region='NT';
		$lc_region = strtolower($region);

		$regionname = $regionabbrev = $regionnamens = $regionofficefull = '';

		if ($region == 'NT') {
			$regionnamens = $regionname = $this->regionname = 'atlantic';
			$regionabbrev = $this->regionabbrev = 'at';
			$regioncode = 'al';
			$regionoffice = 'NHC';
			$regionofficefull = 'knhc';
		}
		elseif ($region == 'PZ') {
			$regionname = $this->regionname = 'eastern pacific';
			$regionabbrev = $this->regionabbrev = 'ep';
			$regionnamens = 'eastern_pacific';
			$regioncode = 'ep';
			$regionoffice = 'NHC';
			$regionofficefull = 'knhc';
		}
		elseif ($region == 'PA') {
			$regionname = $this->regionname = 'central pacific';
			$regionabbrev = $this->regionabbrev = 'cp';
			$regionnamens = 'central_pacific';
			$regioncode = 'cp';
			$regionoffice = 'HFO';
			$regionofficefull = 'phfo';
		}

		$event = (isset($form['event'])) ? $form['event'] : (isset($form['eventnum'])) ? $form['eventnum'] : '';
		$eventnum2 = sprintf("%02u", $event);
		$this->eventnum = $event;
		$this->year = $cyear;

		$event = preg_replace('/\D/', '', $event);
		if ($event > 5) { $event = $event % 5;}
		if ($event == 0) $event = 5;

		$archive = (!empty($form['archive']) && $form['archive']+0 > 0) ? sprintf('%03u',$form['archive']+0) : '';
		if ($data_type == 9 || $archive) $event = sprintf("%02d", $event);
//		$event = sprintf("%02d", $event);


		$uc_regionabbrev = strtoupper($regionabbrev);
		$uc_regioncode = strtoupper($regioncode);

		$cache_hash = array('cache_path'=> $cache_path, 'lc_region' => strtolower($region), 'data_type' =>$data_type,
				'regionnamens'=>$regionnamens, 'regionname'=>$regionname, 'regionabbrev' => $regionabbrev, 'regionofficefull'=>$regionofficefull,
				'regioncode' => $regioncode, 'uc_regioncode' => $uc_regioncode, 'eventnum2' => $eventnum2, 'archivesub' => ( ($cyear > 2005) ? "$regioncode$eventnum2" : 'dis'),
				'archive' => $archive, 'event'=>$event);
		$hashes = array(&$cache_hash, $var, $form);

		if ($archive) {
			$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'troparchive_cache_file'));

			$prefix = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'troparchive_prefix'));
			$domain = $cfg->val('URLs', 'troparchive_url');
			$postfix = $cfg->val('URLs', 'troparchive_postfix');
		}
		else {
			$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_cache_file'));

			$prefix = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_prefix'));
			$domain = $cfg->val('URLs', 'tropical_url');
			$postfix = $cfg->val('URLs', 'tropical_postfix');
		}

		$cache_file = preg_replace('/(?:\.\.+|\&|>|<)/', '', $cache_file);
		if ($cache_file && !preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path)) { $cache_file  = $cache_path . '/' . $cache_file;}
		$cache_file2 = '';

		if ($debug) print "cache_file = $cache_file<br>\n";


		$wx_data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$domain,
			$prefix,
			$postfix, $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
		list($which_used, $data) = $wx_data;

		if (preg_match('/-hwerr/i', $data)) 	$data = '';

		// some products may come from html pages... strip out what we need
		if (preg_match('/<pre>(.+?)<\/pre>/is', $data,$m)) $data = $m[1];
error_reporting(E_ALL);
		$data = preg_replace('/[\r]/', '', $data);
		$data = preg_replace('/<[^>]+>/', '', $data);
		if ($archive) {

			#ZCZC MIATCDAT2 ALL
			#TTAA00 KNHC DDHHMM

			$event +=0;
			$replace_str = "WT$region$data_type$event " . strtoupper($regionofficefull) . " 000000";
			$data = preg_replace('/ZCZC .+\nTTAA00 \w\w\w\w DDHHMM/', $replace_str, $data);


		}





		require_once(HAMLIB_PATH . 'HW3Plugins/Wx/Tropical/Advisory.php');
		$this->tropical = new TropAdivsory($debug);

		$found_data = $this->tropical->read_data($data);


		return $t;
	}


	Function trop_raw (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$t = "";
		$this->cfg = $cfg;

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$cyear = preg_replace('/\D/', '', ( (!empty($form['year'])) ? $form['year'] : ''));
		if (!$cyear) $cyear = gmdate('Y');

		$ms = "january feburary march april may june july august september october november december";
		$months = split(' ', $ms);

		$cmonnum = (!empty($form['mon'])) ? preg_replace('/\D/', '', $form['mon']) : gmdate('n');
		$cmonnum+=0;
		$cmonfull = ($cmonnum) ? $months[$cmonnum-1] : strtolower(gmdate('F'));
		$cmon = substr($cmonfull,0,3);

		$this->cyear = $cyear;
		$this->cmon = $cmon;
		$this->cmonfull = $cmonfull;
		$this->cmonnum  = $cmonnum;


		$region =  (isset($form['tregion'])) ? strtoupper($form['tregion']) : strtoupper(preg_replace('/\W/', '', $form['region']));
		if (!preg_match('/^(?:NT|PA|PZ)$/',$region)) $region='NT';
		$lc_region = strtolower($region);
		$eventnum = (!empty($form['eventnum'])) ? intval($form['eventnum']) : 1;
		$event = ($eventnum > 5) ? $eventnum % 5 : $eventnum;
		if ($event == 0) $eventnum =5;
		$event = ($forecast == 'windprob') ? sprintf("%02d", $event) : $event;

		$this->region = $region;

		$regionname = $regionabbrev = $regionnamens = '';

		if ($region == 'NT') {
			$regionnamens = $regionname = $this->regionname = 'atlantic';
			$regionabbrev = $this->regionabbrev = 'at';
			$regioncode = 'al';
			$regionoffice = 'NHC';
		}
		elseif ($region == 'PZ') {
			$regionname = $this->regionname = 'eastern pacific';
			$regionabbrev = $this->regionabbrev = 'ep';
			$regionnamens = 'eastern_pacific';
			$regioncode = 'ep';
			$regionoffice = 'NHC';
		}
		elseif ($region == 'PA') {
			$regionname = $this->regionname = 'central pacific';
			$regionabbrev = $this->regionabbrev = 'cp';
			$regionnamens = 'central_pacific';
			$regioncode = 'cp';
			$regionoffice = 'HFO';
		}
		$uc_regionabbrev = strtoupper($regionabbrev);
		$uc_regioncode = strtoupper($regioncode);

//		$event = (isset($form['event'])) ? $form['event'] : '';
//		$event = preg_replace('/\D/', '', $event);
//		if ($event > 5) { $event = $event % 5;}
//		if ($event == 0) $event = 5;

		$cache_hash = array('cache_path'=> $cache_path, 'lc_region' => strtolower($region),
				'regionnamens'=>$regionnamens, 'regionname'=>$regionname, 'regionabbrev' => $regionabbrev, 'uc_regionabbrev' => $uc_regionabbrev,
				'cyear'=> $cyear, 'event' => $event, 'regionoffice'=>$regionoffice,
				'regioncode' => $regioncode, 'uc_regioncode' => $uc_regioncode,
				);
		$hashes = array(&$cache_hash, $var, $form);

		$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_cache_file'));
		$cache_file = preg_replace('/(?:\.\.+|\&|>|<)/', '', $cache_file);
		if ($cache_file && !preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path)) { $cache_file  = $cache_path . '/' . $cache_file;}
		$cache_file2 = '';

		$this->forecast_found = 0;
		$this->forecast_text= '';

		$prefix = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_prefix'));

		if ($debug) print "cache_file = $cache_file<br>\n";


		list($junk, $wx_data)  = fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$cfg->val('URLs', 'tropical_url'),
			$prefix,
			$cfg->val('URLs', 'tropical_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);




		if (preg_match('/<pre(?: [^>]+)?>(.+)<\/pre>/s', $wx_data, $m)) {

			$wx_data = preg_replace('/^\s+/','',$m[1]);
		}


		$wx_data = preg_replace('/\r+/','',$wx_data);
		$wx_data = preg_replace('/<[^>]+>/', '', $wx_data);

		if (preg_match('/-hwerr/i', $wx_data)) {
			$wx_data = '';
		}
		else {
			$this->forecast_text = preg_replace('/[\r]/', '', $wx_data);
			$this->forecast_found = 1;

			require_once(HAMLIB_PATH . '/HW3Plugins/Wx/Tropical/Advisory.php');
			$this->tropical = new TropAdivsory($debug);

			if (preg_match('/^(\d[ \w]+\d\d\d\d)$/',$wx_data, $m)) {
				$this->release_time = $m[0];
				$this->epoch_date = $this->tropical->_extract_epoch_date($m[0],$wx_data);
			}
		}


		return $t;
	}


	Function trop_storms (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$t = "";
		$this->cfg = $cfg;

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		//print "this->cfg from trop_storms:".$this->cfg.", cfg:$cfg; debug:$debug, forecast:$forecast, form:$form, var:$var, forecast_info:$forecast_info<br>";

		$cache_path = $cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path = 'cache'; }

		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

//		print "cache_path=$cache_path<br>";

		$region =  (isset($form['region'])) ? strtoupper($form['region']) : 'NT';
		$region = preg_replace('/\W/', '', $region);
		if (!$region) $region='NT';
		$lc_region = strtolower($region);
		if(isset($form['year']) && $form['year']) {
			$cyear = $form['year'];
		}
		else {
			$cyear = date('Y');
		}

		$regionname = $regionabbrev = $regionnamens = '';

		if ($region == 'NT') {
			$regionnamens = $regionname = $this->regionname = 'atlantic';
			$regionabbrev = $this->regionabbrev = 'at';
		}
		elseif ($region == 'PZ') {
			$regionname = $this->regionname = 'eastern pacific';
			$regionabbrev = $this->regionabbrev = 'ep';
			$regionnamens = 'eastern_pacific';
		}

		$event = (isset($form['event'])) ? $form['event'] : '';
		$event = preg_replace('/\D/', '', $event);
		if ($event > 5) { $event = $event % 5;}
		if ($event == 0) $event = 5;

		$cache_hash = array('cache_path'=> $cache_path, 'region' => $region,
				'regionnamens'=>$regionnamens, 'regionname'=>$regionname, 'regionabbrev' => $regionabbrev,
				'cyear'=> $cyear);
		$hashes = array(&$cache_hash, $var, $form);

		$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->val('URLs', 'tropical_cache_file'));
		$cache_file = preg_replace('/(?:\.\.+|\&|>|<)/', '', $cache_file);
		if ($cache_file && !preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path)) { $cache_file  = $cache_path . '/' . $cache_file;}
		$cache_file2 = '';

		$this->forecast_found = 0;

		$prefix = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_prefix'));

		if ($debug) print "cache_file = $cache_file<br>\n";


		$wx_data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$cfg->val('URLs', 'tropical_url'),
			$prefix,
			$cfg->val('URLs', 'tropical_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

		$i = $NTtotal = $PZtotal = $NTisactive = $PZisactive = 0;

		$lines = preg_split('/[\r\n]+/', $wx_data[1]);
		foreach ($lines as $line) {
			if (!$line || preg_match('/^\D/', $line)) continue;
			$i++;

			$items = explode('|', $line);

			list($this->stormyear[$i], $this->stormregion[$i],$this->stormeventnum[$i],$this->stormnoaaeventnum[$i],
			     $this->stormisactive[$i],$this->stormtype[$i],$this->stormname[$i],$this->stormadvisorynum[$i],
			     $this->stormadvisorydate[$i],$this->stormadvisoryepochdate[$i],$this->stormlatdec[$i],
			     $this->stormlatdir[$i],$this->stormlondec[$i],$this->stormlondir[$i],$this->stormdirection[$i],
			     $this->stormspeed_kts[$i],$this->stormpressure_mb[$i],$this->stormwind_kts[$i],
			     $this->stormmaxname[$i],$this->stormmaxwinds[$i]) = $items;

			if (sizeof($items) > 20) {
				$this->stormminpressure[$i] = $items[20];
				$this->stormstartdate[$i] = $items[21];
				$this->stormenddate[$i] = $items[22];
			}

			$this->stormlat[$i] = abs($this->stormlatdec[$i]) . $this->stormlatdir[$i];
			$this->stormlon[$i] = abs($this->stormlondec[$i]) . $this->stormlondir[$i];
			$this->stormspeed_mph[$i] = floor(($this->stormspeed_kts[$i]/0.868391)*10)/10;
			$this->stormwind_mph[$i] = floor(($this->stormwind_kts[$i]/0.868391)*10)/10;
			$this->stormpressure_in[$i] = floor($this->stormpressure_mb[$i]/33.8639*100+.5)/100;

			$this->stormdirection_abbrev[$i] = preg_replace(array('/NORTH/', '/WEST/', '/EAST/', '/SOUTH/', '/STATIONARY/', '/-/'),
				array('N', 'W', 'E', 'S', 'STNRY',''), $this->stormdirection[$i]);

			$this->stormcolor[$i] = $this->get_storm_color($this->stormwind_mph[$i]);
			$this->stormmaxcolor[$i] = $this->get_storm_color(round(($this->stormmaxwinds[$i]/0.868391)*10)/10);

			$this->stormmaxwinds_mph[$i] = round(($this->stormmaxwinds[$i]/0.868391)*10)/10;
			$this->stormshortname[$i] = preg_replace(array('/subtropical storm/i','/tropical depression/i', '/tropical storm/i', '/hurricane/i'), array('','', '', ''), $this->stormname[$i]);
			$this->stormcategory[$i] = $this->get_storm_cat($this->stormmaxwinds_mph[$i]);
			$this->stormminpressure[$i] = ($this->stormpressure_mb[$i] < $this->stormminpressure[$i]) ? $this->stormpressure_mb[$i] : $this->stormminpressure[$i];

			$this->stormminpressure_in[$i] = floor($this->stormminpressure[$i]/33.8639*100+.5)/100;


			if ($this->stormregion[$i] == 'NT' ) { $NTtotal++; if ($this->stormisactive[$i]) { $NTisactive++;}}
			elseif($this->stormregion[$i] == 'PZ') { $PZtotal++; if ($this->stormisactive[$i]) { $PZisactive++;}}
			elseif($this->stormregion[$i] == 'PA') { $PAtotal++; if ($this->stormisactive[$i]) { $PAisactive++;}}
		}

		$this->storm_total = $i;
		$this->storm_NTtotal = $NTtotal;
		$this->storm_PZtotal = $PZtotal;
		$this->storm_PAtotal = $PAtotal;
		$this->storm_NTisactive = $NTisactive;
		$this->storm_PZisactive = $PZisactive;
		$this->storm_PAisactive = $PAisactive;


		return $t;

	}

	Function get_storm_color ($winds) {
			$stormtype = '';
			if ($winds >= 155) { $stormtype = 'H5'; }
			elseif ($winds >= 131) { $stormtype = 'H4'; }
			elseif ($winds >= 111) { $stormtype = 'H3'; }
			elseif ($winds >= 96) { $stormtype = 'H2'; }
			elseif ($winds >= 74) { $stormtype = 'H1'; }
			elseif ($winds >= 39) { $stormtype = 'TS'; }
			else { $stormtype = 'TD'; }

			return $this->cfg->val('HTML tropsystem Colors', $stormtype);
	}

	Function get_storm_cat ($winds) {
			$stormtype = '';
			if ($winds >= 155) { $stormtype = 'H5'; }
			elseif ($winds >= 131) { $stormtype = 'H4'; }
			elseif ($winds >= 111) { $stormtype = 'H3'; }
			elseif ($winds >= 96) { $stormtype = 'H2'; }
			elseif ($winds >= 74) { $stormtype = 'H1'; }
			elseif ($winds >= 39) { $stormtype = 'TS'; }
			else { $stormtype = 'TD'; }

			return $stormtype;
	}


	Function trop_years (&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$t = "";
		$this->cfg = $cfg;

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$cache_hash = array('cache_path'=> $cache_path);
		$hashes = array(&$cache_hash, $var, $form);
		$cache_file = preg_replace("/%%(\w+)%%/e", "\$this->switchvars('$1', \$hashes);", $cfg->Val('URLs', 'tropical_cache_file'));
		$cache_file = preg_replace('/(?:\.\.+|\&|>|<)/', '', $cache_file);
		if ($cache_file && !preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path)) { $cache_file  = $cache_path . '/' . $cache_file;}
		$cache_file2 = '';

		$this->forecast_found = 0;

		$prefix = $cfg->Val('URLs', 'tropical_prefix');

		if ($debug) print "cache_file = $cache_file<br>\n";

		$wx_data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$cfg->val('URLs', 'tropical_url'),
			$prefix,
			$cfg->val('URLs', 'tropical_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);

		if (preg_match_all('/^(\d\d\d\d)$/m', $wx_data[1], $m)) {
			for ($i=0; $i< count($m[0]); $i++) {
				$this->tropyears[$i] = $m[1][$i];
			}
			$this->tropyears_total = $i;
		}
		else {
			$this->tropyears = array();
			$this->tropyears_total = 0;
		}

		return $t;
	}




}


?>