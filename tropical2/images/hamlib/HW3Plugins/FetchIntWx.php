<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchIntWx.php,v 1.5 2008/08/24 01:51:45 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");
require_once("hamlib/HW3Plugins/FetchSunInfo.php");

class FetchIntWx {

	var $cfg;
	var $debug=false;

	var $found_zone;
	var $tzname;
	var $tzdif;

	var $data = array();
	var $data_nodes = array();
	var $found_intwx;
	var $period_total;
	var $wxdata_total;


	var $db_table_24hr = '';
	var $db_table_12hr = '';
	var $db_table_3or6hr = '';
	var $db_table_places = '';
	var $usedb = false;
	var $dbintervals = '3,6,12,24';
	var $db_xml_failover = true;


	var $initialized = false;

	var $core_path ='';

	var $vars = array();
	var $form = array();


	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	Function FetchIntWx($core_path) {
		$this->core_path = $core_path;
		$this->tzdif=0;
		$this->tzname='';

	}

	Function init(&$cfg) {

		$this->initialized = true;

		$usedb = $cfg->val('INTWX','usedb');
		if ($usedb) {
			$this->usedb = true;

			$this->db_username  = $cfg->val('Database Access', 'username'); # Username.
			$this->db_password  = $cfg->val('Database Access', 'password'); # Password.
			$this->db_hostname  = $cfg->val('Database Access', 'hostname'); # Hostname.
			$this->db_database  = $cfg->val('Database Access', 'database_name'); # Database name.


			if ($cfg->val('HWdata', 'database_name')) $this->db_database = $cfg->val('HWdata', 'database_name') ;			if ($cfg->val('HWdata', 'hostname')) $this->db_hostname = $cfg->val('HWdata', 'hostname') ;
			if ($cfg->val('HWdata', 'username')) $this->db_username = $cfg->val('HWdata', 'username') ;
			if ($cfg->val('HWdata', 'password')) $this->db_password = $cfg->val('HWdata', 'password') ;

			if ($cfg->val('INTWX', 'database_name')) $this->db_database = $cfg->val('INTWX', 'database_name') ;
			if ($cfg->val('INTWX', 'hostname')) $this->db_hostname = $cfg->val('INTWX', 'hostname') ;
			if ($cfg->val('INTWX', 'username')) $this->db_username = $cfg->val('INTWX', 'username') ;
			if ($cfg->val('INTWX', 'password')) $this->db_password = $cfg->val('INTWX', 'password') ;

		    $dbtable = $cfg->val('INTWX', 'db_table_places'); # Database name.
		    $this->db_table_places = ($dbtable) ? $dbtable : 'hwdata_places';

		    $dbtable = $cfg->val('INTWX', 'db_table_12hr'); # Database name.
		    $this->db_table_12hr = ($dbtable) ? $dbtable : 'hwdata_forecasts_intwx_12hr';

		    $dbtable = $cfg->val('INTWX', 'db_table_24hr'); # Database name.
		    $this->db_table_24hr = ($dbtable) ? $dbtable : 'hwdata_forecasts_intwx_24hr';

		    $dbtable = $cfg->val('INTWX', 'db_table_3or6hr'); # Database name.
		    $this->db_table_3or6hr = ($dbtable) ? $dbtable : 'hwdata_forecasts_intwx_3or6hr';

		    $dbintervals = $cfg->val('INTWX', 'db_intervals'); # Database name.
		    if ($dbintervals) $this->dbintervals = $dbintervals;

		    $db_xml_failover = $cfg->val('INTWX', 'db_xml_failover'); # Database name.
		    if (is_numeric($db_xml_failover)) $this->db_xml_failover = $db_xml_failover;


		    $ma = $cfg->val('INTWX', 'db_maxage')+0;
		    $this->db_maxage = $ma;

		}
		else {
			$this->usedb = false;
		}

	}


	Function Debug ($mode) {
		$this->debug = $mode;
	}


	Function do_intwx(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$this->cfg = $cfg;
		$this->debug = $debug;

		if (!$this->initialized) $this->init($cfg);

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		$placeid = (isset($form['placeid'])) ? strtolower(preg_replace('/\W/','',$form['placeid'])) : '';


		if (!$placeid) {
			if (isset($var['alt_zone_info']) && preg_match('/intwx(\d+)/', $var['alt_zone_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['icao']) && $var['icao']) {
				$placeid = $var['icao'];
			}
			elseif (isset($var['alt_zone_info']) && preg_match('/^(\w\w\w\w):/', $var['alt_zone_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['alt_cc_info']) && preg_match('/^(\w\w\w\w):/', $var['alt_cc_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['closeby_icao1x']) && preg_match('/^(\w\w\w\w):/', $var['closeby_icao1x'], $m)) {
				$placeid = $m[1];
			}
		}

		$placeid = strtolower($placeid);

		$interval = (isset($form['interval'])) ? strtolower(preg_replace('/\D/','',$form['interval'])) : '';
		if ($interval == '') $interval = 24;
		$form['interval'] = $interval;


		$error='';
		$usedb = ($this->usedb && preg_match("/\\b$interval\\b/", $this->dbintervals)) ? true : false;
		$xml_fail_over = (!$usedb  || ($usedb && $this->db_xml_failover)) ? true: false;

		$this->found_intwx = false;
		if ($usedb) {
			$this->found_intwx  = $this->parse_intwx_db($placeid,$interval, $vars);
		}

		if (!$this->found_intwx && $xml_fail_over) {
			$cache_path =$cfg->val('Paths', 'cache');
			if (!$cache_path) { $cache_path =  'cache'; }
			if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;


			$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'intwx_cache_file'));
			if (!$cache_file) {$cache_file = $cache_path . strtolower("/intwx_$placeid\_$interval"); }

			$cache_file2 = '';
			$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'intwx_prefix'));


			//Call Fetch Forcast
			$data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
				$cfg->val('URLs', 'intwx_url'),
				$prefix,
				$cfg->val('URLs', 'intwx_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
			list($which_used, $wx_data) = $data;

			$this->found_intwx = $this->parse_intwx_xml($wx_data);

		}


		return $t;
	}

	function parse_intwx_db($placeid, $interval, &$vars) {
		$found = false;
		$isold = false;


		$dbh = $this->db_establish( $this->db_database, $this->db_hostname,
		          $this->db_username,$this->db_password);
		if (!$dbh) {
			return array(mysql_error(), $isold);
		}

		$sql = "SELECT a.name, a.state, a.country, a.tz, a.tzname, a.lat,a.lon, b.country_name, a.icao1 as icao , a.distance1 as icaodistance FROM " . $this->db_table_places . " a LEFT JOIN countries b ON a.country = b.country_abbrev WHERE placeid='$placeid'";
		if ($this->debug) print "$sql<br/>\n";
		$result = mysql_query($sql,$dbh);
		if ($result) {
			if ($loc_data = mysql_fetch_assoc ($result)) {
				foreach ($loc_data as $key=>$val) {
					$this->data['wxdata_' . $key] = $val;
				}
			}
		}
		elseif ($this->debug) {
			print "ERROR: " . mysql_error() . "<br/>";
		}


		// get db info
		if ($interval == 12) {
			$dbtable = $this->db_table_12hr;
			$sql = "SELECT validEpoch as validepoch_gmt, tmin as tminc, tmax as tmaxc, wx, wxint as wx_intensity, tcdc as clouds, clouds as clouds_eng, apcpmm as precipmm, windspdmps as windspeed_max_mps, windspdmps * 2.24 as windspeed_max_mph , winddir as winddirection_max, winddireng as winddirection_max_eng, dayPeriod as day_period FROM $dbtable WHERE placeid='$placeid' ORDER BY validEpoch";

		}
		elseif ($interval == 3 || $interval == 6) {
			$dbtable = $this->db_table_3or6hr;
			$sql = '';

		}
		else {
			$dbtable = $this->db_table_24hr;
			$sql = "SELECT validEpoch as validepoch_gmt, tmin as tminc, tmax as tmaxc, wx, wxint as wx_intensity, tcdc as clouds, clouds as clouds_eng, apcpmm as precipmm, windspdmps as windspeed_max_mps, windspdmps * 2.24 as windspeed_max_mph , winddir as winddirection_max, winddireng as winddirection_max_eng FROM $dbtable WHERE placeid='$placeid' ORDER BY validEpoch";
		}

		if ($this->debug) print "$sql<br/>\n";

		$result = mysql_query($sql,$dbh);
		$total = 0;
		if ($result) {
			while ($period_data = mysql_fetch_assoc ($result)) {
				array_push($this->data_nodes, $period_data);
				$total++;
			}
		}
		elseif ($this->debug) {
			print "ERROR: " . mysql_error() . "<br/>";
		}

		$this->data['wxdata_total']=$total;

		$found  = ($total) ? 1 : 0;



		if ($this->debug) print "found=$found...total=$total<br/>";

		return array($found);

	}




	function parse_intwx_xml(&$xml) {


		if (preg_match('/<location ([^>]+)>/', $xml, $m)) {
			$location = ' ' . $m[0];
			if (preg_match_all('/ (\w+)="([^"]+)"/', $location, $mm, PREG_SET_ORDER)) {
				foreach ($mm as $m) {
					$this->data['wxdata_' . $m[1]] = $m[2];
				}
			}
		}

		$total=0;
		if (preg_match_all('/<period>(.+?)<\/period>/s', $xml, $mm, PREG_SET_ORDER)) {
			foreach ($mm as $m) {
				$period_data = array();
				if (preg_match_all('/<(\w[^>]+)>([^<]+)</', $m[1], $pmm, PREG_SET_ORDER)) {
					foreach ($pmm as $pm) {
						$period_data[$pm[1]] = $pm[2];
//						print "pm[1]=" . $pm[1] . " = " . $pm[2] . "<br>\n";
					}
					array_push($this->data_nodes, $period_data);
					++$total;
				}
			}
		}
		$this->data['wxdata_total']=$total;

//		print "total=$total<br>\n";

		return ($total) ? 1 : 0;


	}


	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = NULL;

		if ($index != '') {

			if (preg_match('/^wxdata_(.+)$/', $key,$m)) {
				if (isset($this->data_nodes[$index]) && isset($this->data_nodes[$index][$m[1]])) {
					$val = $this->data_nodes[$index][$m[1]];
				}
				elseif (isset($this->$key)) {
					$tmp =& $this->$key;
					if(isset($tmp[$index])) $val = $tmp[$index];
				}
			}
		}
		else {
			if (isset($this->data[$key])) $val = $this->data[$key];
		}

		error_reporting($old_error_reporting);

		return $val;
	}





	Function dayornight ($epoch) {

		if ($epoch && isset($this->data['wxdata_lat']) && isset($this->data['wxdata_lon'])) {
			$this->vars['lat'] = $this->data['wxdata_lat'];
			$this->vars['lon'] = $this->data['wxdata_lon'];
			$tz = $this->vars['tz'] = $this->data['wxdata_tz'];

			$astro = new FetchSunInfo('');
			$this->form['epoch'] = $epoch+$tz*3600;
			$astro->set_sunrise_params($this->vars, $this->form, '', $this->cfg, array('','',''), $this->debug);

			$eday = $astro->sunset('epoch');
			$bday = $astro->sunrise('epoch');

			$hour =  $epoch ;
		}
		else {
			return 1;
		}


		if (($bday < $eday && $hour >= $bday && $hour < $eday) ) {
			return 1; //day time
		}
		else {
			return 0; // night time
		}
	}

	function period_wx($day) {

		$wx = $this->data_nodes[$day]['wx'];
		$wx_intensity = $this->data_nodes[$day]['wx_intensity'];
		$clouds_eng = $this->data_nodes[$day]['clouds_eng'];

		$fti = $this->data_nodes[$day]['fti']+0;
		$fti = ($fti > 0) ? "T$fti" : '';



		$vepoch = $this->data_nodes[$day]['validepoch_gmt'];
		$dayornight = $this->dayornight($vepoch);

		$twx_info='';

		$wx_info= $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx");

		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx$fti");

		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx");


		if (!$wx_info) {
			$twx_info= $this->cfg->val('IntWxInfo', "$wx_intensity");
			$wx_info = $this->cfg->val('IntWxInfo', "$wx");
			if ($twx_info && $wx_info) $wxinfo .= ' ';
		}
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng");
		if (!$wx_info) $wx_info = $wx;

		$tdata = preg_split('/\|/', $wx_info);
		if ($dayornight || !isset($tdata[3])) {
			return $twx_info . $tdata[0];
		}
		else {
			return $twx_info . $tdata[3];
		}
	}



	function period_icon($day) {

		$wx = $this->data_nodes[$day]['wx'];
		$wx_intensity = $this->data_nodes[$day]['wx_intensity'];
		$clouds_eng = $this->data_nodes[$day]['clouds_eng'];

		$vepoch = $this->data_nodes[$day]['validepoch_gmt'];
		$dayornight = $this->dayornight($vepoch);

		$twx_info='';

		$fti = $this->data_nodes[$day]['fti']+0;
		$fti = ($fti > 0) ? "T$fti" : '';

		$wx_info= $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx");

		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx$fti");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx$fti");


		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx");
		if (!$wx_info) {
			$wx_info = $this->cfg->val('IntWxInfo', "$wx");
		}
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng");
		if (!$wx_info) $wx_info = $wx;

		$tdata = preg_split('/\|/', $wx_info);
		if ($dayornight || !isset($tdata[2])) {
			return  $tdata[1];
		}
		else {
			return $tdata[2];
		}
	}



	Function db_establish($database, $dbhostname, $dbusername, $dbpassword) {
		$dbh = null;

		$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword,1) or die(mysql_error());
		mysql_select_db($database,$dbh) or die(mysql_error());


		return $dbh;
	}



}

?>