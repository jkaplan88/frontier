<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: SimpleHTTPClient.php,v 1.13 2008/07/17 14:13:35 wxfyhw Exp $
*
*/

function  &HTTPGet ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=20) {

	$data =& fetch_http($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug);
	$buf =& $data[0];
//     ini_set('user_agent','MSIE 4\.0b2;');
//     $dh = fopen("http://$hostname$url",'r');
//     $result = fread($dh,8192);
//	$buf =& $result;


	if ($buf == '') {
		if ($debug) {print "Error fetching http<br>\n";}
		$buf = '-hwERR Empty Page Returned\n';
	}
	else {
		if ($debug) print "HTTP fetched<br>\n";
	}
	return $buf;
}


function  &HTTPHead ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=10) {
	$data =& fetch_http($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug);

	$buf =& $data[1];

	if ($buf == '') {
		if ($debug) {print "Error fetching http<br>\n";}
		$buf = '-hwERR Empty Page Returned\n';
	}
	else {
		if ($debug) print "HTTP fetched<br>\n";
	}
	return $buf;
}

function &fetch_http ($url, $hostname, $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout=10) {

	if ($port == '') {
		if (preg_match('/:(\d+)$/', $hostname, $m)) {
			$port = $m[1];
			$hostname = preg_replace('/:\d+$/','',$hostname);
		}
		else {
			$port = 80;
		}
	}

	$uhost=$hostname; $uport=$port;
	if ($proxy_server != '') {
		$uhost = $proxy_server;
		$uport = $port;

		if (!preg_match('/^http/i', $url)) {
			if (!preg_match('/^\//', $url)) $url = "/$url";
			$url = "http://$hostname$url";
		}
	}


	if ($debug) print "Opening socket to $uhost:$uport...timeout=$timeout<br>\n";
	$socket = fsockopen($uhost, $uport, $errno, $errstr, $timeout);
	if (!$socket) {

		if ($debug) print "Error opening socket $errstr ($errno)<br>\n";
		return array("-hwERR $errstr ($errno)", '');
	}
	else {
		if ($debug) print "socket opened<br>\n";

		if ($debug) {
			$turl = preg_replace('/wx\w+/', '', $url);
			print "sending headers<br>url=$turl<br>\n";
		}



		fputs($socket, "GET $url HTTP/1.1\r\n");
		fputs($socket, "Host: $hostname\r\n");
		fputs($socket, "Accept: text/html, text/plain, image/gif, image/jpg, image/png, */*\r\n");
		fputs($socket, "User-Agent: HAMweather/3.98\r\n");
		fputs($socket, "Connection: close\r\n");
		fputs($socket, "\r\n");
		fputs($socket, "\r\n");
		if ($debug) print "headers sent now lets read<br>\n";


		$data = read_socket($socket, $debug);

		fclose($socket);


		// if we have a 301 moved permantly then lets get the new url
		if (preg_match('/301 Moved Permanently/i', $data[1]) &&preg_match('/Location: *http:\/\/([^\/\r\n]+)([^\r\n]+)/', $data[1], $m)) {
			$data =& fetch_http($m[2], $m[1], $port, $in, $proxy_server, $proxy_user, $proxy_pw, $debug, $timeout);
		}
		elseif (preg_match('/404 Not Found|Page Not Found/i', $data[1])) {
			$data[0] = '';
		}
		elseif (preg_match('/300 Multiple Choices/i', $data[1])) {
			$data[0] = '';
		}
		elseif (preg_match('/Cannot find product/i', $data[0])) {
			$data[0] = '';
		}
		return $data;
	}
}


function read_socket(&$socket, $debug) {


	$hdone=0;
	$header = '';
	$body = '';
	$lastline = '';

	while (!feof($socket)) {
		if($hdone)	{
			$line = fread ( $socket, 1024 );
			$body .= $line;
		}
		else {
			$line = fgets ($socket, 128);

			if($line == $lastline || $line == "\r\n"){ $hdone=1;}
			else {
				$lastline = $line;
				$header .= $line;
			}
		}
	}
$body = preg_replace('/^[\da-f]+\s*?[\r\n]+/', '', $body);
$body = preg_replace('/[\r\n]+0[\r\n]*$/', '', $body);

	if ($debug) print "finished reading<br>body=<pre>$body</pre><br>header=<pre>$header</pre><br>\n";

	return array($body,$header);
}





?>