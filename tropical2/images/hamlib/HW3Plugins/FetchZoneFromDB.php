<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchZoneFromDB.php,v 1.1 2008/07/17 14:13:36 wxfyhw Exp $
*
*/
class FetchZoneFromDB {

	var $cfg;
	var $debug = false;

	var $data = array();

	var $db_table='';
	var $db_maxage=0;
	var $parse_detail = 0;

	var $use_point = false;
	var $use_zfp = true;


	var $day_total = 0;

	// ZFP only
	var $day_fc = array();

	// point & zfp
	var $day_wx = array();
	var $day_hi = array();
	var $day_lo = array();
	var $day_pop = array();
	var $day_title = array();
	var $day_wind = array();
	var $day_winddir = array();

	// Point only
	var $day_winddir_eng = array();
	var $day_temp = array();
	var $day_atmin = array();
	var $day_atmax = array();
	var $day_atemp = array();
	var $day_dewpoint = array();
	var $day_rh = array();
	var $day_sky = array();
	var $day_clouds = array();

	var $day_qpf = array();
	var $day_snowfall = array();

	var $day_period = array();
	var $day_valid_epoch = array();
	var $day_wxicon = array();

	var $day_wxprob = array();
	var $day_wxint = array();
	var $day_wxcode = array();

	var $tz = 0;
	var $tzname = 'UTC';
	var $night_text = '% NIGHT';

	var $used_point = false;
	var $point_start_next = false;




	Function FetchZoneFromDB($cfg, $debug) {
		$this->cfg =& $cfg;
		$this->debug = $debug;

		$this->db_username  = $cfg->val('Database Access', 'username'); # Username.
		$this->db_password  = $cfg->val('Database Access', 'password'); # Password.
		$this->db_hostname  = $cfg->val('Database Access', 'hostname'); # Hostname.
		$this->db_database  = $cfg->val('Database Access', 'database_name'); # Database name.

		if ($cfg->val('HWdata', 'database_name')) $this->db_database = $cfg->val('HWdata', 'database_name') ;			if ($cfg->val('HWdata', 'hostname')) $this->db_hostname = $cfg->val('HWdata', 'hostname') ;
		if ($cfg->val('HWdata', 'username')) $this->db_username = $cfg->val('HWdata', 'username') ;
		if ($cfg->val('HWdata', 'password')) $this->db_password = $cfg->val('HWdata', 'password') ;


		if ($cfg->val('FORECAST', 'database_name')) $this->db_database = $cfg->val('FORECAST', 'database_name') ;
		if ($cfg->val('FORECAST', 'hostname')) $this->db_hostname = $cfg->val('FORECAST', 'hostname') ;
		if ($cfg->val('FORECAST', 'username')) $this->db_username = $cfg->val('FORECAST', 'username') ;
		if ($cfg->val('FORECAST', 'password')) $this->db_password = $cfg->val('FORECAST', 'password') ;

	    $dbtable = $cfg->val('FORECAST', 'db_table'); # Database name.
	    $this->db_table = ($dbtable) ? $dbtable : 'hwdata_forecasts';

	    $dbtable = $cfg->val('FORECAST', 'db_table_7day'); # Database name.
	    $this->db_table_7day = ($dbtable) ? $dbtable : 'hwdata_forecasts';


	    $this->use_point = (is_numeric($cfg->val('FORECAST', 'use_point'))) ? $cfg->val('FORECAST', 'use_point') : true;
	    $this->use_zfp = (is_numeric($cfg->val('FORECAST', 'use_zfp'))) ? $cfg->val('FORECAST', 'use_zfp') : true;



	    $dbtable = $cfg->val('FORECAST', 'db_table_point_3or6hr'); # Database name.
	    $this->db_table_point_3or6hr = ($dbtable) ? $dbtable : 'hwdata_point_3or6hr';

	    $dbtable = $cfg->val('FORECAST', 'db_table_point_12hr'); # Database name.
	    $this->db_table_point_12hr = ($dbtable) ? $dbtable : 'hwdata_point_12hr';

	    $dbtable = $cfg->val('FORECAST', 'db_table_point_24hr'); # Database name.
	    $this->db_table_point_24hr = ($dbtable) ? $dbtable : 'hwdata_point_24hr';

	    $sni = $cfg->val('FORECAST', 'point_start_next');
	    $this->point_start_next = (strlen($sni) == 0 || $sni) ? true : false;


	    $ma = $cfg->val('FORECAST', 'db_maxage')+0;
	    $this->db_maxage = $ma;

	    if ($cfg->val('FORECAST', 'parse_detail')) $this->parse_detail = 1;
	    else $this->parse_detail = 0;




	}


	Function db_establish($database, $dbhostname, $dbusername, $dbpassword) {
		$dbh = null;

		$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword,1) or die(mysql_error());
		mysql_select_db($database,$dbh) or die(mysql_error());


		return $dbh;
	}


	Function fetch_zone ($zone, $daysonly, $zipcode='', $icao='') {

		$this->day_total = 0;
		if ($this->use_point) {
			$this->fetch_point($zone,$daysonly,$zipcode,$icao);
			if ($this->day_total > 0) return array('',0);
		}

		if ($this->use_zfp) return $this->fetch_zfp($zone,$daysonly);

	}

	function fetch_point ($zone, $daysonly, $zipcode='', $icao = '') {

		$zone = preg_replace('/\W/', '', $zone);
		$icao = preg_replace('/\W/', '', $icao);
		$zipcode = preg_replace('/\W/', '', $zipcode);

		$debug = $this->debug;

		$this->day_total = 0;
		$this->used_point = false;

		$parse_detail = $this->parse_detail;
		$wx_array = ($parse_detail) ? $this->cfg->SectionParams('WxInfo') : array();

		$do_hour_by_hour = ($daysonly == 4 && $this->cfg->val('FORECAST', 'hourbyhour')) ?  true : false;

		$error = '';
		if ($zone || $icao || $zipcode) {

			$dbh = $this->db_establish( $this->db_database, $this->db_hostname,
			          $this->db_username,$this->db_password);
			if (!$dbh) {
				return array(mysql_error(), $isold);
			}

			$try_locs = array();
			if ($zipcode) array_push($try_locs,$zipcode);
			if ($zone) array_push($try_locs,$zone);
			if ($icao) array_push($try_locs,$icao);

			$where = '';
			if ($daysonly == 4) {
				$table = $this->db_table_point_3or6hr;
				if ($this->point_start_next)  $where .= " AND validTime > '" . gmdate('Y-m-d H:i:s', time()) . "' ";
			}
			elseif ($daysonly > 1) {
				$table = $this->db_table_point_24hr;
				$where .= " AND validTime >= '" . gmdate('Y-m-d 00:00:00', time()+$this->tz * 3600) . "' ";
			}
			else {
				 $table = $this->db_table_point_12hr;
				 $where .= " AND validTime >= '" . gmdate('Y-m-d H:i:s', time()-6 * 3600) . "' ";

			}

			$sql = 'SELECT validTime,period,tmin,tmax,tempf,atmin,atmax,atempf,wx,wxicon,sky,clouds,rh,pop12,qpf,snowfall,maxwind,maxwind_dir,maxwind_dir_eng,wxprob,wxint,wxcode FROM ' . $table . " WHERE  locid='%LOC%' " . $where . ' AND not (tempf = 9999 and sky > 100)  ORDER BY validTime ASC';

			foreach ($try_locs as $tloc) {
				$tsql = str_replace('%LOC%', $tloc,$sql);

				if ($debug) print "sql=$tsql<br>\n" ;

				$result = mysql_query($tsql,$dbh);

				if (!$result) {
					if ($debug) print mysql_error() . "<br/>\n";
				}
				else {
					if (mysql_num_rows($result)) break;
				}
			}




			if ($result) {

				$day_title = array();
				$day_fc = array();
				$day_icon = array();
				$day_wx = array();
				$day_hi = array();
				$day_lo = array();
				$day_pop = array();
				$day_wind_spd = array();
				$day_wind_dir = array();

				$day_winddir_deg = array();
				$day_temp = array();
				$day_atmin = array();
				$day_atmax = array();
				$day_atemp = array();
				$day_dewpoint = array();
				$day_rh = array();
				$day_sky = array();
				$day_clouds = array();

				$day_qpf = array();
				$day_snowfall = array();

				$day_period = array();
				$day_valid_epoch = array();

				$day_wxprob = array();
				$day_wxint = array();
				$day_wxcode = array();

				$total = 0;



				while ($data = mysql_fetch_assoc ($result)) {
					$total++;

					$data['validepoch'] = strtotime($data['validTime'] . ' UTC');

					if ($daysonly > 1 || ($data['period'] == 'D')) {
						array_push($day_hi, $data['tmax']);
						array_push($day_atmax, $data['atmax']);
						if ($daysonly <2) {
							array_push($day_lo, '');
							array_push($day_atmin, '');
						}
					}


					if ($daysonly > 1 || ($data['period'] == 'N')) {
						array_push($day_lo, $data['tmin']);
						array_push($day_atmin, $data['atmin']);
						if ($daysonly <2) {
							array_push($day_hi, '');
							array_push($day_atmax, '');
						}
					}

					array_push($day_temp, $data['tempf']);
					array_push($day_atemp, $data['atempf']);

					array_push($day_fc, $data['wx']);

					if ($parse_detail && $data['wx']) {
						array_push($day_wx, $this->_get_wxtype($data['wx'], $wx_array));
					}
					else {
						array_push($day_wx, $data['wx']);
					}


					array_push($day_icon, $data['wxicon']);
					array_push($day_wxprob, $data['wxprob']);
					array_push($day_wxint, $data['wxint']);
					array_push($day_wxcode, $data['wxcode']);

					array_push($day_sky, $data['sky']);
					array_push($day_clouds, $data['clouds']);

					array_push($day_rh, $data['rh']);

					$dewpoint = (isset($data['dewpoint'])) ? $data['dewpoint'] : '';
					array_push($day_dewpoint, $dewpoint);


					array_push($day_qpf, $data['qpf']);
					array_push($day_snowfall, $data['snowfall']);

					$pop = (empty($data['wxcode'])) ? 0 : round($data['pop12'],-1);
					array_push($day_pop, $pop);

					array_push($day_wind_spd, $data['maxwind']);
					array_push($day_wind_dir, $data['maxwind_dir_eng']);
					array_push($day_winddir_deg, $data['maxwind_dir']);

					$period = ($daysonly == 2 || $daysonly == 3) ? 'D' : $data['period'];
					array_push($day_period, $period);
					array_push($day_valid_epoch, $data['validepoch']);

					$title = '';
					if ($daysonly == 4) {
						$title = gmdate('D, M d h:i ', $data['validepoch'] + $this->tz * 3600) . ' ' . $this->tzname;
					}
					else {

						$title = strtoupper(gmdate('l',$data['validepoch'] + $this->tz * 3600));

						if ($daysonly == 3) {
							$title = substr($title,0,3);
						}
						elseif ($daysonly == 0 && $data['period'] == 'N') {
							$title = str_replace('%', $title, $this->night_text);
						}
					}
					array_push($day_title, $title);
				}

				$this->day_total = $total-1;

				if ($debug) { print "<pre>";var_dump($day_title);print"</pre>"; }

				$this->day_title = $day_title;
				$this->day_period = $day_period;
				$this->day_valid_epoch = $day_valid_epoch;

				$this->day_fc = $day_fc;
				$this->day_wx = $day_wx;



				$this->day_icon = $day_icon;
				$this->day_wxprob = $day_wxprob;
				$this->day_wxint = $day_wxint;
				$this->day_wxcode = $day_wxcode;

				$this->day_sky = $day_sky;
				$this->day_clouds = $day_clouds;

				$this->day_hi = $day_hi;
				$this->day_lo = $day_lo;
				$this->day_temp = $day_temp;

				$this->day_atmax = $day_atmax;
				$this->day_atmin = $day_atmin;
				$this->day_atemp = $day_atemp;

				$this->day_rh = $day_rh;
				$this->day_dewpoint = $day_dewpoint;
				$this->day_qpf = $day_qpf;
				$this->day_snowfall = $day_snowfall;
				$this->day_pop = $day_pop;

				$this->day_wind = $day_wind_spd;
				$this->day_winddir = $day_wind_dir;
				$this->day_winddir_deg = $day_winddir_deg;

				if ($do_hour_by_hour) $this->make_hour_by_hour();

			}

		}

		if ($this->day_total > 0) $this->used_point = true;

		return $this->day_total;

	}

	function make_hour_by_hour () {


		$total = $this->day_total;
		if ($total < 2) return;

		$data = array();

		$data['day_title'] = array(); //
		$data['day_fc'] = array(); //
		$data['day_icon'] = array(); //
		$data['day_wx'] = array(); //
		$data['day_hi'] = array(); //
		$data['day_lo'] = array(); //
		$data['day_pop'] = array(); //
		$data['day_wind'] = array(); //
		$data['day_winddir'] = array();

		$data['day_winddir_deg'] = array(); //
		$data['day_temp'] = array(); //
		$data['day_atmin'] = array(); //
		$data['day_atmax'] = array(); //
		$data['day_atemp'] = array(); //
		$data['day_dewpoint'] = array(); //
		$data['day_rh'] = array();; //
		$data['day_sky'] = array();; //
		$data['day_clouds'] = array();; //

		$data['day_qpf'] = array(); //
		$data['day_snowfall'] = array(); //

		$data['day_period'] = array(); //
		$data['day_valid_epoch'] = array(); //

		$data['day_wxprob'] = array(); //
		$data['day_wxint'] = array(); //
		$data['day_wxcode'] = array(); //

		for ($i=0; $i < $total; $i++) {
			if ($i < $total -1) {

				$sepoch = $this->day_valid_epoch[$i];
				$nepoch = $this->day_valid_epoch[$i+1];

				$dhours = intval(($nepoch - $sepoch) / 3600);
				$d2hours = $dhours/2;

				for ($j=0; $j <=$dhours; $j++) {

					if ($j== 0) $this->_hourbyhour_push_row($data, $i);
					elseif ($sepoch + $j * 3600 < $nepoch) {
						// perform hour y hour calc
						$tepoch = $sepoch + $j * 3600;
						array_push($data['day_valid_epoch'], $tepoch );
						array_push($data['day_title'],  gmdate('D, M d h:i ', $tepoch + $this->tz * 3600) . ' ' . $this->tzname);

						array_push($data['day_period'], $this->day_period[$i] );

						array_push($data['day_fc'], $this->day_fc[$i] );
						array_push($data['day_icon'], $this->day_icon[$i] );
						array_push($data['day_wx'], $this->day_wx[$i] );
						array_push($data['day_wxprob'], $this->day_wxprob[$i] );
						array_push($data['day_wxint'], $this->day_wxint[$i] );
						array_push($data['day_wxcode'], $this->day_wxcode[$i] );


						array_push($data['day_hi'], $this->day_hi[$i] );
						array_push($data['day_lo'], $this->day_lo[$i] );

						array_push($data['day_pop'], round(($this->day_pop[$i]-$this->day_pop[$i+1])/$dhours * $j + $this->day_pop[$i] ));

						array_push($data['day_wind'], round(($this->day_wind[$i]-$this->day_wind[$i+1])/$dhours * $j + $this->day_wind[$i] ));
						array_push($data['day_winddir'], ( ($j < $d2hours) ? $this->day_winddir[$i] : $this->day_winddir[$i+1]));
						array_push($data['day_winddir_deg'], ( ($j < $d2hours) ? $this->day_winddir_deg[$i] : $this->day_winddir_deg[$i+1]));

						array_push($data['day_temp'], round(($this->day_temp[$i]-$this->day_temp[$i+1])/$dhours * $j + $this->day_temp[$i]) );
						array_push($data['day_atmin'], $this->day_atmin[$i] );
						array_push($data['day_atmax'], $this->day_atmax[$i] );
						array_push($data['day_atemp'], round(($this->day_atemp[$i]-$this->day_atemp[$i+1])/$dhours * $j + $this->day_atemp[$i] ));

						array_push($data['day_dewpoint'], round(($this->day_dewpoint[$i]-$this->day_dewpoint[$i+1])/$dhours * $j + $this->day_dewpoint[$i] ));
						array_push($data['day_rh'], round(($this->day_rh[$i]-$this->day_rh[$i+1])/$dhours * $j + $this->day_rh[$i] ));

						array_push($data['day_sky'], round(($this->day_sky[$i]-$this->day_sky[$i+1])/$dhours * $j + $this->day_sky[$i] ));
						array_push($data['day_clouds'], ( ($j < $d2hours) ? $this->day_clouds[$i] : $this->day_clouds[$i+1]));

						array_push($data['day_qpf'], round(($this->day_qpf[$i]-$this->day_qpf[$i+1])/$dhours * $j + $this->day_qpf[$i] ));
						array_push($data['day_snowfall'], round(($this->day_snowfall[$i]-$this->day_snowfall[$i+1])/$dhours * $j + $this->day_snowfall[$i] ));
					}
				}


			}
			else $this->_hourbyhour_push_row($data, $i);

		}


		$this->day_title = $data['day_title'];
		$this->day_period = $data['day_period'];
		$this->day_valid_epoch = $data['day_valid_epoch'];

		$this->day_fc = $data['day_fc'];
		$this->day_wx = $data['day_wx'];



		$this->day_icon = $data['day_icon'];
		$this->day_wxprob = $data['day_wxprob'];
		$this->day_wxint = $data['day_wxint'];
		$this->day_wxcode = $data['day_wxcode'];

		$this->day_sky = $data['day_sky'];
		$this->day_clouds = $data['day_clouds'];

		$this->day_hi = $data['day_hi'];
		$this->day_lo = $data['day_lo'];
		$this->day_temp = $data['day_temp'];

		$this->day_atmax = $data['day_atmax'];
		$this->day_atmin = $data['day_atmin'];
		$this->day_atemp = $data['day_atemp'];

		$this->day_rh = $data['day_rh'];
		$this->day_qpf = $data['day_qpf'];
		$this->day_snowfall = $data['day_snowfall'];
		$this->day_pop = $data['day_pop'];

		$this->day_wind = $data['day_wind'];
		$this->day_winddir = $data['day_winddir'];
		$this->day_winddir_deg = $data['day_winddir_deg'];

	}

	function _hourbyhour_push_row(&$data, $i) {

		$keys = array_keys($data);
		foreach ($keys as $key) {
			$tarray =& $this->$key;
			if (isset($tarray[$i])) array_push($data[$key], $tarray[$i]);
		}



	}



	Function fetch_zfp ($zone, $daysonly) {

		$error = '';
		$isold = 0;
		$debug = $this->debug;
		$parse_detail = $this->parse_detail;
		$wx_array = ($parse_detail) ? $this->cfg->SectionParams('WxInfo') : array();
//print "parse_detail=$parse_detail<br/>";
//print "<pre>";var_dump($wx_array);print"</pre>";
		$zone = preg_replace('/\W/', '', $zone);
		if ($zone) {

			$dbh = $this->db_establish( $this->db_database, $this->db_hostname,
			          $this->db_username,$this->db_password);
			if (!$dbh) {
				return array(mysql_error(), $isold);
			}

			$table = ($daysonly>1) ? $this->db_table_7day : $this->db_table;

			$sql = 'SELECT TimeStamp,ForecastDate,Expires,DayTotal,Day1Title,Day1Wx,Day1Icon,Day1HI,Day1LO,Day1POP,Day1FC,Day2Title,Day2Wx,Day2Icon,Day2HI,Day2LO,Day2POP,Day2FC,Day3Title,Day3Wx,Day3Icon,Day3HI,Day3LO,Day3POP,Day3FC,Day4Title,Day4Wx,Day4Icon,Day4HI,Day4LO,Day4POP,Day4FC,Day5Title,Day5Wx,Day5Icon,Day5HI,Day5LO,Day5POP,Day5FC,Day6Title,Day6Wx,Day6Icon,Day6HI,Day6LO,Day6POP,Day6FC,Day7Title,Day7Wx,Day7Icon,Day7HI,Day7LO,Day7POP,Day7FC,Day8Title,Day8Wx,Day8Icon,Day8HI,Day8LO,Day8POP,Day8FC,Day9Title,Day9Wx,Day9Icon,Day9HI,Day9LO,Day9POP,Day9FC,Day10Title,Day10Wx,Day10Icon,Day10HI,Day10LO,Day10POP,Day10FC,Day11Title,Day11Wx,Day11Icon,Day11HI,Day11LO,Day11POP,Day11FC,Day12Title,Day12Wx,Day12Icon,Day12HI,Day12LO,Day12POP,Day12FC,Day13Title,Day13Wx,Day13Icon,Day13HI,Day13LO,Day13POP,Day13FC,Day14Title,Day14Wx,Day14Icon,Day14HI,Day14LO,Day14POP,Day14FC FROM ' . $table . " WHERE Zone='$zone'";

			$ma =$this->db_maxage;
			if ($ma > 0) {
				$now = time();
				$now -= $ma * 60; # get the current time minus the max age converted to seconds
				$sql .= " AND TimeStamp >= $now";
			}

			if ($debug) print "sql=$sql<br>\n" ;

			$result = mysql_query($sql,$dbh);
			if ($data = mysql_fetch_assoc ($result)) {

				$this->forecastdate = $data['ForecastDate'];

				if (preg_match('/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/', $data['Expires'],$m)) {
					$this->forecast_expiration_epoch = gmmktime($m[4],$m[5],0,$m[2],$m[3],$m[1]);

					$this->forecast_expiration = strtoupper($m[4].$m[5] . ' GMT ' . gmdate('M d Y', $this->forecast_expiration_epoch));

					$isold = $this->forecast_expired  = ($this->forecast_expiration_epoch < time()) ? 1 : 0;
				}

				$day_title = array();
				$day_fc = array();
				$day_icon = array();
				$day_wx = array();
				$day_hi = array();
				$day_lo = array();
				$day_pop = array();
				$day_wind_spd = array();
				$day_wind_dir = array();

				$day_total = $this->day_total = $data['DayTotal']-1;
				for ($i=1; $i<=$data['DayTotal']; $i++) {
					$day = "Day$i";

					if (empty($data[$day . 'Title'])) continue;

					array_push($day_title, ($daysonly == 3) ? substr($data[$day . 'Title'],0,3) : $data[$day . 'Title']);
					array_push($day_fc, $data[$day . 'FC']);

					if ($parse_detail && $data[$day . 'FC']) {
						array_push($day_wx, $this->_get_wxtype($data[$day . 'FC'], $wx_array));
					}
					else {
						array_push($day_wx, $data[$day . 'Wx']);
					}
					array_push($day_hi, $data[$day . 'HI']);
					array_push($day_lo, $data[$day . 'LO']);
					array_push($day_pop, $data[$day . 'POP']);
					array_push($day_icon, $data[$day . 'Icon']);
					array_push($day_wind_spd, $this->day_wind_speed($data[$day . 'FC']));
					array_push($day_wind_dir, $this->day_wind_dir($data[$day . 'FC']));
				}

					if ($debug) { print "<pre>";var_dump($day_title);print"</pre>"; }
					$this->day_total = $day_total;
					$this->day_title = $day_title;
					$this->day_icon = $day_icon;
					$this->day_fc = $day_fc;
					$this->day_wx = $day_wx;
					$this->day_hi = $day_hi;
					$this->day_lo = $day_lo;
					$this->day_pop = $day_pop;
					$this->day_wind = $day_wind_spd;
					$this->day_winddir = $day_wind_dir;

			}
			else {
				$error = "$zone not in the database";
			}
			$sth = NULL;

			mysql_close($dbh);
		}



		return array($error,$isold);
	}



	Function day_wind_speed($forecast) {
		$wind = "";

		$forecast = strtolower($forecast);

  		if (preg_match('/up\s+?\s+?to\s+?([0-9]*)\s+?mph/', $forecast, $matches)) {
			$wind = $matches[1];
		}
		elseif (preg_match('/(\d+)\s+?mph?/', $forecast, $matches)) {
			$wind = $matches[1];
		}
		elseif (preg_match('/([0-9]*\s+?to\s+?[0-9]*)\s+?mph/', $forecast, $matches)) {
			$wind = $matches[1];
		}

		return $wind;
	}
	Function day_wind_dir($forecast) {
		$winddir = "";

		$forecast = strtolower($forecast);

  		if (preg_match('/(\w+)\s+?winds?/', $forecast, $matches)) {
			$winddir = $matches[1];
		}

		return $winddir;
	}


	Function _get_wxtype($wx_str, $types) {
		foreach ($types as $type => $val) {
			$t= str_replace('/','\/',$type);
			if (preg_match('/\b'.$t.'/i', $wx_str)) {
				return 	$type;
			}
		}
		return "";
	}

}
?>