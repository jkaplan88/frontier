<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: TAF2Zone.php,v 1.12 2008/07/17 14:13:36 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/Wx/TAFData.php");

class TAF2Zone{

	var $debug=0;

	var $sky_cond = array(
	      'CLR'=>'clear',
	      'SKC'=>'clear',
	      'FEW'=>'partly cloudy',
	      'SCT'=>'partly cloudy',
	      'BKN'=>'mostly cloudy',
	      'OVC'=>'overcast',
	      'NSC'=>'clear',
	      'CAVOK'=>'fair'
	);

	 var $sc_ord = array(
	   'OVC'=> 5,
	   'BKN'=>4,
	   'SCT'=>3,
	   'FEW'=>2,
	   'SKC'=>1,
	   'CLR'=>1
	   );

	var $cloud_types = array(
	'AC' => 'Altocumulus',
	'AS' => 'Alstostratus',
	'CB' => 'Cumuloninumbus',
	'CC' => 'Cirrocumulus',
	'CI' => 'Cirrus',
	'CS' => 'Cirrostratus',
	'CU' => 'Cumulus' ,
	'FC' => 'Fractocumulus',
	'FS' => 'Fractostratus',
	'NS' => 'Nimbostratus',
	'SC' => 'Stratocumulus',
	'ST' => 'Stratus',
	'TCU' => 'Towering Cumulus'
	);

	var $wx_descript = array(
	'NSW' => '',
	'MI' => 'SHALLOW',
	'BC' => 'PATCHES',
	'BL'=> 'BLOWING',
	'TS' => 'THUNDERSTORM',
	'PR' => 'PARTIAL',
	'DR' => 'LOW DRIFTING',
	'SH' => 'SHOWERS',
	'FZ' => 'FREEZING',

	'RA' => 'RAIN',
	'SN' => 'SNOW',
	'IC' => 'ICE CRYSTALS',
	'GR' => 'HAIL',
	'UP' => 'KNOWN PRECIP',
	'DZ' => 'DRIZZLE',
	'SG' => 'SNOW GRAINS',
	'PE' => 'ICE PELLETS',
	'GS' => 'SMALL HAIL',

	'FG' => 'FOG',
	'BR' => 'MIST',
	'DU' => 'WIDESPREAD DUST',
	'SA' => 'SAND',
	'VA' => 'VOLCANIC ASH',
	'HZ' => 'HAZE',
	'FU' => 'SMOKE',
	'PY' => 'SPRAY',

	'SQ' => 'SQUALL',
	'DS' => 'DUSTSTORM',
	'FC' => 'FUNNEL CLOUD',
	'PO' => 'DUST SWIRLS',
	'SS' => 'SANDSTORM'

	);

	var $phrases = array(
	'MAIN' => '%%wx%%',

	'BECMG_SKY' => 'Becoming %%wx%%',
	'BECMG_WX' => 'Beginning to %%wx%%',
	'BECMG_WXS' => 'Beginning to have %%wx%%',

	'FROM_SKY' => 'Turning %%wx%%',
	'FROM_WX' => 'Starting to %%wx%%',
	'FROM_WXS' => 'Starting to have %%wx%%',

	'PROB_WX' => 'A %%prob%% percent chance to %%wx%%',
	'PROB_WXS' => 'A %%prob%% percent chance of %%wx%%',

	'WIND' => 'With winds from the %%WIND_DIR_FULLNAME%% at %%dec0_WIND_MPH%% mph',
	'WIND_VARIABLE' => 'Winds variable in direction at %%dec0_WIND_MPH%% mph',
	'WIND_CALM' =>'Winds calm',

	'GUST' => 'and gusts up to %%dec0_WIND_MPH_GUST%% mph',

	'THEN' => 'Then ',

	'WIND_S'=> 'south',
	'WIND_W'=>'west',
	'WIND_E'=>'east',
	'WIND_N'=>'north',
	'WIND_VRB'=>'variable'

	);

	var $month = array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
	var $weekday = array('SUN','MON','TUE','WED','THU','FRI','SAT');

	var $day_part_titles = array('Morning', 'Afternoon', 'Evening', 'Overnight');


	var $day_total = 0;
	var $day_fc = array();
	var $day_wx = array();
	var $day_hi = array();
	var $day_lo = array();
	var $day_pop = array();
	var $day_title = array();

	var $zone_warnings = '';
	var $forecastdate = "";
	var $forecast_text = "";
	var $header = "";
	var $warnings = "";

	var $tzdif = 0;
	var $tzname = 'GMT';


	Function TAF2Zone($debug ) {
		$this->debug=$debug;
	}

	Function Debug($debug) {
		$this->debug = $debug;
	}

	Function _jday($m,$d,$y) {

		$yy = $y - floor((12-$m)/10);
		$mm = $m+9;
		if ($mm >= 12) $mm -=12;
		$k3 =floor(floor(($yy/100)+49)*.75)-38;
		$j = floor(365.25*($yy+4712)) + floor(30.6*$mm+.5) +$d+59;
		if ($j > 2299160) $j -= $k3 ;

		return $j;

	}

	Function _jdate($jd) {

		$wkday = ($jd + 1) % 7;       // calculate weekday (0=Sun,6=Sat)
		$jdate_tmp = $jd - 1721119;
		$y = floor((4 * $jdate_tmp - 1)/146097);
		$jdate_tmp = 4 * $jdate_tmp - 1 - 146097 * $y;
		$d = floor($jdate_tmp/4);
		$jdate_tmp = floor((4 * $d + 3)/1461);
		$d = 4 * $d + 3 - 1461 * $jdate_tmp;
		$d = floor(($d + 4)/4);
		$m = floor((5 * $d - 3)/153);
		$d = 5 * $d - 3 - 153 * $m;
		$d = floor(($d + 5) / 5);
		$y = 100 * $y + $jdate_tmp;
		if($m < 10) {$m += 3;}
		   else {$m -= 9; ++$y;}

		return array($m, $d, $y, $wkday);
	}

	Function process_taf(&$taf_data,  &$wx_types, &$phrases, &$wx_descript, &$sky_cond, &$cloud_types){
		$debug = $this->debug;
//print "phrases=$phrases<br>\n";
//print "sky_cond=$sky_cond<br>\n";
		if (!$phrases || !is_array($phrases)) $phrases =& $this->phrases;
		if (!$wx_types || !is_array($wx_types)) $wx_types = array();
		if (!$wx_descript || !is_array($wx_descript)) {$wx_descript =& $this->wx_descript;} else { $this->wx_descript =& $wx_descript;}
		if (!$sky_cond || !is_array($sky_cond)) {$sky_cond =& $this->sky_cond;} else { $this->sky_cond =& $sky_cond;}
		if (!$cloud_types || !is_array($cloud_types)) {$cloud_types =& $this->cloud_types;} else { $this->cloud_types =& $cloud_types;}


		$taf_data = preg_replace('/^.+?<PRE>(.+?)<\/PRE>.+$/is','\1',$taf_data);
		$taf = new TAFData($debug);
		$taf->ParseTAF($taf_data);

		// parse the original, begin and end date as best we can
		// we will calc the origina date ($omon, $oday, $oyear)
		//
		// then we will calc the beginning time ($bmon, $bday, $byear $bhour Z)
		// then we will calc the ending time ($emon, $eday, $eyear $ehour Z)

		//$day = gmdate('d'); $mon = gmdate('m'); $year = gmdate('Y');
		list($year, $mon, $day) = explode(' ', gmdate('Y m d'));

		if (preg_match('/^(\d\d\d\d)\/(\d\d)\/(\d\d)$/', $taf->origin_full_date, $m)) {
			$oyear = $m[1]; $omon = $m[2]; $oday = $m[3];
		}
		else {$oyear = $year; $omon = $mon; $oday = $day; }


		if ($debug) print "<br>jday($mon,$day,$year) =" . ($this->_jday($mon,$day,$year) ) . "<br>\n";
		if ($debug) print "jday($omon,$oday,$oyear)=" . ($this->_jday($omon,$oday,$oyear)) . "<br>\n";
		if (($this->_jday($mon,$day,$year) - $this->_jday($omon,$oday,$oyear)) > 2) {
			$this->taf_expired = 1;
		}
		else { $this->taf_expired = 0;}
		if ($debug) print "taf_expired=" . $this->taf_expired."<br>\n";

		$bday = $taf->begin_day+0; $bhour=$taf->begin_hour;
		$eday = $taf->end_day+0; $ehour =$taf->end_hour;
		if ($oday > $bday){
			$byear = $year;
			$bmon = $omon+1;
			if ($bmon > 12) { $bmon =1; $byear = $oyear+1;}
		}
		else {
			$bmon = $omon;
			$byear = $oyear;
		}
		if ($eday > $bday) { list ($emon, $eday, $eyear) = $this->_jdate($this->_jday($bmon,$bday,$byear)+1);}
		  else { $emon = $bmon; $eday = $bday; $eyear = $byear;}

		$otime ='';

		if (preg_match('/^\d\d(\d\d)(\d\d)$/',$taf->origin_date, $m)) {
			$hour = $m[1]; $min =$m[2];
		}
		else {
			$hour = isset($taf->data[0]['begin_hour']) ? $taf->data[0]['begin_hour'] : 0;
			$min =0;
		}

		$hour += $this->tzdif;
		if ($hour < 0) {
			$hour += 24;
			list($omon, $oday, $oyear) = $this->_jdate($this->_jday($omon, $oday, $oyear)-1);
		}
		elseif($hour > 23) {
			$hour -=24;
			list($omon, $oday, $oyear) = $this->_jdate($this->_jday($omon, $oday, $oyear)+1);
		}

		$ampm= 'AM';
		if ($hour > 12) { $hour -= 12; $ampm = 'PM'; }
		elseif($hour == 12) { $ampm='PM';}
		$otime = sprintf('%02u%02u', $hour, $min). " $ampm";

   		// now that we have the date lets set our forecast date
   		list(,,,$wd) = $this->_jdate($this->_jday($omon, $oday, $oyear));
   		$this->forecastdate = "$otime " . $this->tzname . ' ' . $this->weekday[$wd] .' ' . $this->month[$omon-1] . " $oday $oyear ";

		   // now we need to parse through the TAF sections and create
		   // a text based forecast

		$cday = NULL; $last_hour = NULL;
		$day_period_titles = array();
		$day_period_wx = array();
		$day_period_wind = array();
		$day_period_pressure=array();
		$day_period_prob=array();
		$day_titles = array();
		$day_fc = array();

		$last_day_part=NULL; $last_phrase=NULL; $last_wx=NULL;

		$total = $taf->data_total;
		for ($i =0; $i <= $total; $i++) {
			if (preg_match('/TEMPO|INTER/', $taf->data[$i]['type'])) continue;

			$bhour = $taf->data[$i]['begin_hour'];
			if ($i < $total) {
				$ni = $this->_get_next_taf_item($taf,$i);
				if (!$ni) { $ehour = $taf->data[$ni]['end_hour']; }
				  else { $ehour = $taf->data[$ni]['begin_hour']; }
				if (!$i && $ni && $ehour == $bhour) continue;
			}
			else { $ehour = $taf->end_hour; }

			$ceday=NULL;
			if(!isset($cday)) {
				$cday = $bday;
				if ($ehour == $bhour ) { $ceday = $eday;}
			}
			elseif($bhour==0) {
				$cday = $eday;
				$ceday = $eday;
			}
			elseif($ehour <= $bhour || $ehour == 24) {
				$ceday = $eday;
			}
			if (!isset($ceday)) $ceday = $bday;

			array_push($day_period_titles, "$bhour\Z to $ehour\Z");

			$twi = (isset($taf->data[$i]['wind'])  && !$taf->data[$i]['wind'] && $i>0) ? $this->_get_last_taf_item($taf,$i) : $i;
			$twind = isset($taf->data[$twi]['wind']) ? $taf->data[$twi]['wind'] : '';
			if ($twind == '00000KT') $twind = 0;

//print "MAIN:twind=$twind<br>";
			$wind_str = ''; $gusts= '';
			if ($twind) {
//print "MAIN2:twind=$twind<br>";
				$wind_str = $taf->data[$twi]['WIND_DIR_ENG'] . 	' ' . round($taf->data[$twi]['WIND_MPH']);
				$gusts = round($taf->data[$twi]['WIND_MPH_GUST']);
				if ($gusts) { $wind_str .= ' G ' . $gusts;}
				$wind_str .= ' MPH';
			}
			array_push($day_period_wind, $wind_str);


			$tpi = (isset($taf->data[$i]['altimeter']) && !$taf->data[$i]['altimeter'] && $i>0) ? $this->_get_last_taf_item($taf,$i) : $i;
			$tpressure = isset($taf->data[$tpi]['altimeter']) ? $taf->data[$tpi]['altimeter'] : '';

			$pressure = isset($taf->data[$tpi]['pressure_in']) ? $taf->data[$tpi]['pressure_in'] : '';
			if ($pressure) $pressure .= ' in';
			array_push($day_period_pressure, $pressure);

			$wx=''; $used_sky='';
			if (isset($taf->data[$i]['weather'])) {
				$wx = $this->_process_wx_condition($taf->data[$i]['weather'][0]);
			}
			if(!$wx && isset($taf->data[$i]['sky'])) {
				$wx = $this->_process_sky_conditions($taf->data[$i]['sky']);
				$used_sky =1;
			}
			elseif(!$wx) {
				$wx='fair';
				$used_sky =1;
			}
			array_push($day_period_wx, strtoupper($wx));

			$prob = (isset($taf->data[$i]['prob'])) ? $taf->data[$i]['prob'] : '';
			array_push($day_period_prob, $prob);

			//lets tweak  $wx a little bit
			$wx = strtolower($wx);
			if ($wx == 'thunderstorm rain') $wx = 'thunderstorm';

			//now lets get the phrase that we need
			$type = $taf->data[$i]['type'];
			$twdireng= isset($taf->data[$twi]['WIND_DIR_ENG']) ? $taf->data[$twi]['WIND_DIR_ENG'] : '';
			$phrase = $this->_get_phrase($type, $phrases, $wx, $used_sky,$twdireng, $twind, $gusts);

			$orig_phrase = $phrase;
			if (isset($last_day_part)) {
				if ($last_wx != $wx) {
					if (preg_match('/percent/', $phrase)) $last_phrase .= $phrases['THEN'] .' ';
					$phrase = $last_phrase . $phrase;
					$last_phrase='';
				}
				else {
					$phrase = $last_phrase;
				}
			}

			//lets make a full english wind phrasing
			$wind_dir_fullname = isset($taf->data[$twi]['WIND_DIR_ENG']) ? $taf->data[$twi]['WIND_DIR_ENG'] : '';
//print "wind_dir_fullname=$wind_dir_fullname<br>";
			$wind_dir_fullname = preg_replace('/([NEWS])/e', "\$phrases['WIND_\\1'] ", $wind_dir_fullname);
			$wind_dir_fullname = str_replace('VRB', $phrases['WIND_VRB'] . ' ', $wind_dir_fullname);

//print "phrase=$phrase<br>";
			//finally lets replace any special vars in the phrase
			$phrase = preg_replace(array('/%%wx%%/', '/%%WIND_DIR_FULLNAME%%/'),
							array($this->_get_wxtype($wx, $wx_types, 1), $wind_dir_fullname), $phrase);

			$zz=0;
			while (preg_match('/%%dec(\d?)_(\w+)%%/', $phrase, $m) || ++$zz>50) {
				$a = (strtolower(substr($m[2],0,4)) == 'wind') ? $twi : $i;
				$b = sprintf('%.' . $m[1] . 'f', $taf->data[$a][$m[2]]);
				$phrase = str_replace('%%dec'.$m[1].'_'.$m[2].'%%', $b, $phrase);
			}

			$zz=0;
			while (preg_match('/%%(\w+)%%/', $phrase, $m) || ++$zz>50) {
				$a = (strtolower(substr($m[1],0,4)) == 'wind') ? $twi : $i;
				$b = (isset($taf->data[$a][$m[1]])) ? $taf->data[$a][$m[1]] : '';
				$phrase = str_replace('%%'.$m[1].'%%', $b, $phrase);
			}

//print "last_phrase=$last_phrase... phrase=$phrase<br>\n";
			$day_part = $this->_what_part_of_day($bhour);
			$eday_part = $this->_what_part_of_day($ehour);

			if (($cday != $ceday && !($ehour <4 && $bhour >= 21) ) || $day_part != $eday_part || $i == $total) {
//print "cday=$cday...ceday=$ceday... bhour=$bhour...ehour=$ehour..daypart=$day_part...eday_part=$eday_part..i=$i...total=$total<br>\n";
				array_push($day_titles, $this->day_title_name($day_part));
				array_push($day_fc, $phrase);

				if ($type == 'PROB') { $phrase = "$orig_phrase "; }
				else {
					$wde = (isset($taf->data[$twi]['WIND_DIR_ENG'])) ? $taf->data[$twi]['WIND_DIR_ENG'] : '';
					$phrase = $this->_get_phrase('MAIN', $phrases, $wx, $used_sky,$wde, $twind, $gusts);
					$phrase = preg_replace(array('/%%wx%%/', '/%%WIND_DIR_FULLNAME%%/'),
									array($wx, $wind_dir_fullname), $phrase);

					$zz=0;
					while (preg_match('/%%dec(\d?)_(\w+)%%/', $phrase, $m) || ++$zz>50) {
						$a = (strtolower(substr($m[2],0,4)) == 'wind') ? $twi : $i;
						$b = sprintf('%.' . $m[1] . 'f', $taf->data[$a][$m[2]]);
						$phrase = str_replace('%%dec'.$m[1].'_'.$m[2].'%%', $b, $phrase);
					}

					$zz=0;
					while (preg_match('/%%(\w+)%%/', $phrase, $m) || ++$zz>50) {
						$a = (strtolower(substr($m[1],0,4)) == 'wind') ? $twi : $i;
						$b = $taf->data[$a][$m[1]];
						$phrase = str_replace('%%'.$m[1].'%%', $b, $phrase);
					}
				}

				$did = 0;
//print "final phrase= $phrase<br>";
				if (($cday!=$ceday && !($ehour <4 && $bhour >= 21) ) || $day_part != $eday_part ) {
					while ($this->_next_day_part($day_part) != $eday_part) {
						$day_part = $this->_next_day_part($day_part);
						array_push($day_titles, $this->day_title_name($day_part));
						array_push($day_fc, $phrase);
						$did++;
					}
				}

				$next_i = $this->_get_next_taf_item($taf, $i);
				if (!$next_i && $day_part != $eday_part) {
					$day_part = $this->_next_day_part($day_part);
					//print "pushing: $eday_part<br>\n";
					array_push($day_titles, $this->day_title_name($eday_part));
					array_push($day_fc, $phrase);
				}
		}

			$cday = $ceday;

			if ($i < $total) {
				if ($this->_what_part_of_day($taf->data[$this->_get_next_taf_item($taf, $i)]['begin_hour']) == $eday_part) {
					$last_day_part = $eday_part;
					$last_phrase = $phrase;
					$last_wx = $wx;
				}
				else {
					$last_day_part = NULL;
					$last_phrase =  NULL;
					$last_wx = NULL;
				}
			}
		}

		$day_wx = array();
		$day_title_names = array();
		foreach ($day_fc as $fc) {
			array_push($day_wx, $this->_get_wxtype($fc, $wx_types));
		}

		$day_total = sizeof($day_titles);
		$this->day_total = --$day_total;
		$this->day_title = $day_titles;
		$this->day_fc = $day_fc;
		$this->day_wx = $day_wx;

		return $day_total;
	}

	Function _get_phrase($type, &$phrases, $wx, $used_sky, $wind_dir, $twind, $gusts) {
		$phrase ='';

		if (preg_match('/BECMG|GRADU/', $type)) {
			if ($used_sky) { $phrase = $phrases['BECMG_SKY']; }
			else {
				if (substr($wx,-1) == 's') {$phrase = $phrases['BECMG_WXS']; }
				else {$phrase = $phrases['BECMG_WX']; }
			}
		}
		elseif ($type == 'PROB') {
			if (substr($wx,-1) == 's') {$phrase = $phrases['PROB_WXS']; }
			else {$phrase = $phrases['PROB_WX']; }
		}
		elseif($type == 'FROM') {
			if ($used_sky) { $phrase = $phrases['FROM_SKY']; }
			else {
				if (substr($wx,-1) == 's') {$phrase = $phrases['FROM_WXS']; }
				else {$phrase = $phrases['FROM_WX']; }
			}
		}
		else { $phrase = $phrases['MAIN']; }

		//if wind lets add that phrase
		if ($twind) {
			if (preg_match('/VRB/', $wind_dir)) {
				$phrase .= ' ' . $phrases['WIND_VARIABLE'];
			}
			else { $phrase .= ' ' . $phrases['WIND']; }

			if ($gusts) $phrase .= ' ' . $phrases['GUST'];
		}

		$phrase .= '. ';

		return $phrase;
	}


	Function tz($tzdif='', $tzname ='') {
		if ($tzname) {
			$this->tzdif = $tzdif;
			$this->tzname = $tzname;
		}
		return $this->tzdif;
	}

	Function day_title_name($period) {
		$names = array('Morning', 'Afternoon', 'Evening', 'Overnight');
		if (isset($names[$period])) { return $names[$period]; }
		else { return "INVALID period '$period'"; }
	}


	Function forecast_text() {
		$text ='';
		for ($i=0; $i < $this->day_total; $i++) {
			$text .= $this->day_title[$i] . '...' . $this->day_fc[$i] . "\n";
		}
		return $text;
	}

	Function _get_wxtype ($wx, &$wx_array, $display = 0) {
		$found ='';
		foreach ($wx_array as $wxtype => $value) {
			$wxtype = str_replace('/', '\/', $wxtype);
			if (preg_match("/$wxtype/i", $wx)) {
				if ($display) {
					$items = array();
					$items = preg_split('/\|/', $value);
					$found = $items[0];
				}
				else {
					$found =$wxtype;
				}
				break;
			}
		}
		return $found;
	}

	Function _next_day_part ($t) {
		if (++$t > 3) $t=0;
		return $t;
	}

	Function _get_next_taf_item(&$taf, $i) {
		$i++;
		if ($i > $taf->data_total) return 0;
		if (isset($taf->data[$i]['type']) && preg_match('/TEMPO|INTER/',$taf->data[$i]['type'])) {
			return $this->_get_next_taf_item($taf, $i);
		}
		return $i;
	}

	Function _get_last_taf_item(&$taf, $i) {
		$i--;
		if ($i <0) return -1;
		if (isset($taf->data[$i]['type']) && preg_match('/TEMPO|INTER/',$taf->data[$i]['type'])) {
			return $this->_get_last_taf_item($taf, $i);
		}
		return $i;
	}


	Function _process_sky_conditions(&$sky_toks) {
		$weather ='';
		$ovc=0; $t='';

		foreach($sky_toks as $sky) {
			if (preg_match('/^([A-Z]+)\d+([A-Z]*)$/', $sky, $m)) {
				$tt = $m[1]; $cld=$m[2];
				if ($this->sc_ord[$tt] > $ovc) {
					$ovc = $this->sc_ord[$tt];
					$t = $tt;
				}
				if ($cld && isset($this->cloud_types[$cld])) {
					if (!$weather) $weather = $this->cloud_types[$cld] . ' clouds observered';
				}
			}
			else {
				if (!$t) $t = $sky;
			}
		}

		if (isset($this->sky_cond[$t])) { return $this->sky_cond[$t]; }
		else {return ''; }
	}


	Function _process_wx_condition($wx_toks) {

		$weather = preg_replace('/SH(\w\w)/', '\1SH', $wx_toks);
		$precip='';

		if (preg_match('/^([-+]?)([A-Z]+)/', $weather, $m)) {
			$lh = $m[1]; $act=$m[2];

			if ($lh == '-') { $precip = 'LIGHT ';}
			elseif ($lh == '+') { $precip = 'HEAVY ';}

			if (strlen($act) == 2) {
				$p1 = (isset($this->wx_descript[$act])) ? $this->wx_descript[$act] : '';
				$precip .= $p1;
			}
			elseif(strlen($act) == 4) {
				$p1 = (isset($this->wx_descript[substr($act,0,2)])) ? $this->wx_descript[substr($act,0,2)] : '';
				$p2 = (isset($this->wx_descript[substr($act,2,2)])) ? $this->wx_descript[substr($act,2,2)] : '';
				$precip .= "$p1 $p2";
			}
			else {
				$precip ='';
			}
		}
		return $precip;
	}

	Function _what_part_of_day($t) {
		$t += $this->tzdif;

		if ($t >= 4 && $t < 12) {  $part = 0;}
		elseif ($t>=12 && $t<17) { $part=1;}
		elseif ($t>=17 && $t<21) { $part=2;}
		else { $part=3;}

		return $part;
	}






}


?>