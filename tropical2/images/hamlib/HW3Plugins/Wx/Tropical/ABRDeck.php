<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 2007 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: ABRDeck.php,v 1.2 2007/09/23 22:09:52 wxfyhw Exp $
*
*/

if (!defined('ABRD_BASIN')) define ('ABRD_BASIN', 0);
if (!defined('ABRD_CY')) define ('ABRD_CY', 1);
if (!defined('ABRD_DATE')) define ('ABRD_DATE', 2);
if (!defined('ABRD_TECHNUM')) define ('ABRD_TECHNUM', 3);
if (!defined('ABRD_TECH')) define ('ABRD_TECH', 4);
if (!defined('ABRD_TAU')) define ('ABRD_TAU', 5);
if (!defined('ABRD_LatNS')) define ('ABRD_LatNS', 6);
if (!defined('ABRD_LonEW')) define ('ABRD_LonEW', 7);
if (!defined('ABRD_VMAX')) define ('ABRD_VMAX', 8);
if (!defined('ABRD_MSLP')) define ('ABRD_MSLP', 9);
if (!defined('ABRD_TY')) define ('ABRD_TY', 10);
if (!defined('ABRD_RAD')) define ('ABRD_RAD', 11);
if (!defined('ABRD_WINDCODE')) define ('ABRD_WINDCODE', 12);
if (!defined('ABRD_RAD1')) define ('ABRD_RAD1', 13);
if (!defined('ABRD_RAD2')) define ('ABRD_RAD2', 14);
if (!defined('ABRD_RAD3')) define ('ABRD_RAD3', 15);
if (!defined('ABRD_RAD4')) define ('ABRD_RAD4', 16);
if (!defined('ABRD_RADP')) define ('ABRD_RADP', 17);
if (!defined('ABRD_RRP')) define ('ABRD_RRP', 18);
if (!defined('ABRD_MRD')) define ('ABRD_MRD', 19);
if (!defined('ABRD_GUSTS')) define ('ABRD_GUSTS', 20);
if (!defined('ABRD_EYE')) define ('ABRD_EYE', 21);
if (!defined('ABRD_SUBREGION')) define ('ABRD_SUBREGION', 22);
if (!defined('ABRD_MAXSEAS')) define ('ABRD_MAXSEAS', 23);
if (!defined('ABRD_INITIALS')) define ('ABRD_INITIALS', 24);
if (!defined('ABRD_DIR')) define ('ABRD_DIR', 25);
if (!defined('ABRD_SPEED')) define ('ABRD_SPEED', 26);
if (!defined('ABRD_STORMNAME')) define ('ABRD_STORMNAME', 27);
if (!defined('ABRD_DEPTH')) define ('ABRD_DEPTH', 28);
if (!defined('ABRD_SEAS')) define ('ABRD_SEAS', 29);
if (!defined('ABRD_SEASCODE')) define ('ABRD_SEASCODE', 30);
if (!defined('ABRD_SEAS1')) define ('ABRD_SEAS1', 31);
if (!defined('ABRD_SEAS2')) define ('ABRD_SEAS2', 32);
if (!defined('ABRD_SEAS3')) define ('ABRD_SEAS3', 33);
if (!defined('ABRD_SEAS4')) define ('ABRD_SEAS4', 34);
if (!defined('ABRD_TAUEPOCH')) define ('ABRD_TAUEPOCH', 35);


class TropABRDeck {

	var $debug = false;
	var $tracks = array();

	function TropABRDeck($debug = false) {
		$this->debug = $debug;
	}


	// parse a data file into its infividual tracks
	function parse (&$data) {

		$this->tracks = array();

		$lines = preg_split('/[\r\n]/',$data);

		foreach ($lines as $line) {
			$line = trim($line);
			if (!$line) continue; // ignore blank lines

			// the data us a csv/spaced file, so lets remove the spaces
			$line = preg_replace('/, +/', ',', $line);

			$parts = explode(',', $line);

			if (empty($parts[ABRD_TECH])) continue; // bad line if we have no tech type code
			else $tech = $parts[ABRD_TECH];



			if (preg_match('/(\d+)([NS])/', $parts[ABRD_LatNS], $m)) {
				$lat = $m[1]/10;
				if ($m[2] == 'S') $lat = -$lat;
				$parts[ABRD_LatNS] = $lat;
			}
			if (preg_match('/(\d+)([EW])/', $parts[ABRD_LonEW], $m)) {
				$lon = $m[1]/10;
				if ($m[2] == 'W') $lon = -$lon;
				$parts[ABRD_LonEW] = $lon;
			}

			$epoch = 0;
			if (preg_match('/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)/', $parts[ABRD_DATE],$m)) {
				$epoch = gmmktime($m[4],0,0,$m[2],$m[3],$m[1]);
				$epoch += $parts[ABRD_TAU] * 3600;
				$parts[ABRD_TAUEPOCH] = $epoch;
			}



			if (!isset($this->tracks[$tech])) $this->tracks[$tech] = array();

			array_push($this->tracks[$tech], $parts);
		}

		return sizeof($this->tracks[$tech]);

	}

	// obtain a track for the pased tech. If no index is provided, return the entire track. if index is provided return just the specified index
	function &get_track ($tech,$index=-1) {
		$track = array();


		if (!empty($this->tracks[$tech])) {
			if ($index == -1) $track =& $this->tracks[$tech];
			elseif (strtolower($index) == 'last' && sizeof($this->tracks[$tech]) > 0 ) $track = $this->tracks[$tech][sizeof($this->tracks[$tech])-1];
			elseif ($index < sizeof($this->tracks[$tech])) $track = $this->tracks[$tech][$index];
		}
		return $track;
	}


	function get_track_total($tech) {
		$total = 0;
		if (!empty($this->tracks[$tech])) $total = sizeof($this->tracks[$tech]);
		return $total;
	}



	function get_techs() {
		$techs = array();
		if (!empty($this->tracks)) $techs = array_keys($this->tracks);

		return $techs;
	}

	function tech_exists($tech) {
		if (!empty($this->tracks) && !empty($this->tracks[$tech])) return true;
		else return false;
	}








}


?>