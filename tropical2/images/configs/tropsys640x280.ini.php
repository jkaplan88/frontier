%%LOAD_CONFIG=tropical_plot_common%%


[Image Settings]
autocrop_width=640
autocrop_height=280

auto_config=tropmaps_640x480


[tropsystem automaps]
nt_zoom0=atlantic_merc_640x280

pz_zoom0=pacific_merc_640x280

pa_zoom0=pacific_merc_640x280

pa_pz_zoom0=pacific_merc_640x280

default_zoom0=fulltropics_merc_640x280


[Title Settings]
add_title=1

