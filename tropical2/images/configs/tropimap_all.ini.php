<?php

/*

[Image Settings]


#if make_image_map=1 then when plotting the map
# an imagemap file will be made as well, which can be used
# with in the html. This is useful if you want
# to allow a map to clickable
make_image_map=1

image_map_name=tropimap
#image_map_save_name=%%usemap%%
#image_map_save_name=%%usemap%%
image_map_save_ext=imap

image_map_url_current_pos=[scripturl]?forecast=tropsystems&region=[stormregion]&hwvregstormid=[stormeventnum]&year=[stormyear]&eventnum=[stormeventnum]&alt=tropsystempage

image_map_other_current_pos=onmouseover="toolTip('<div id=\'HWtropinfo\'  style=\'height:99px;\'><div id=\'storm\'>[stormtype_full]</div><div id=\'details\'><span class=\'title\'>Position at <span class=\'date\'>[stormadvisorydate]</span></span><div class=\'row\'><div class=\'label\'>Position:</div><div class=\'data\'>[stormlat], [stormlon]</div></div><div class=\'row\'><div class=\'label\'>Winds:</div><div class=\'data\'>[stormwind_mph] mph ([stormwind_kts] kts)</div></div><div class=\'row\'><div class=\'label\'>Pressure:</div><div class=\'data\'>[stormpressure_mb] mb ([stormpressure_in] in)</div></div></div></div>', '#333333', '#FFFFFF', true);" onmouseout="toolTip();"


image_map_url_track=#
image_map_other_track=onclick="return false;"  onmouseover="toolTip('<div id=\'HWtropinfo\'  style=\'height:99px;\'><div id=\'storm\'>[stormtype_full]</div><div id=\'details\'><span class=\'title\'>Position at <span class=\'date\'>[stormadvisorydate]</span></span><div class=\'row\'><div class=\'label\'>Position:</div><div class=\'data\'>[stormlat], [stormlon]</div></div><div class=\'row\'><div class=\'label\'>Winds:</div><div class=\'data\'>[stormwind_mph] mph ([stormwind_kts] kts)</div></div><div class=\'row\'><div class=\'label\'>Pressure:</div><div class=\'data\'>[stormpressure_mb] mb ([stormpressure_in] in)</div></div></div></div>', '#333333', '#FFFFFF', true);" onmouseout="toolTip();"


image_map_url_forecast=[scripturl]?forecast=tropsystems&region=[stormregion]&hwvregstormid=[stormeventnum]&year=[stormyear]&alt=tropsystempage
image_map_other_forecast=onmouseover="toolTip('<div id=\'HWtropinfo\'  style=\'background-color:#E4E4E4;height:99px;\'><div id=\'storm\'>Forecast</div><div id=\'details\'  ><span class=\'title\'>Position at <span class=\'date\'>[fdate]</span></span><div class=\'row\'><div class=\'label\'>Storm Type:</div><div class=\'data\'>[stormtype_full]</div></div><div class=\'row\'><div class=\'label\'>Position:</div><div class=\'data\'>[storm_forecast_lat][storm_forecast_lat_dir], [storm_forecast_lon][storm_forecast_lon_dir]</div></div><div class=\'row\'><div class=\'label\'>Winds:</div><div class=\'data\'>[storm_forecast_wind_mph] mph ([storm_forecast_wind_kts] kts)</div></div></div></div>', '#333333', '#FFFFFF', true);" onmouseout="toolTip();"

*/
?>