<?php
/*

%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
autocrop_width=380
autocrop_height=285


[tropsystem automaps]
nt_zoom0=atlantic_merc_380x285
nt_zoom1=atlantic_merc_760x570
nt_zoom2=atlantic_merc_1520x1140
nt_zoom3=atlantic_merc_3040x2280
nt_zoom4=atlantic_merc_4180x3135

pz_zoom0=tropics_merc_380x285
pz_zoom1=tropics_merc_760x570
pz_zoom2=tropics_merc_1520x1140
pz_zoom3=tropics_merc_3040x2280
pz_zoom4=tropics_merc_4180x3135

pa_zoom0=pacific_merc_380x285
pa_zoom1=pacific_merc_760x570
pa_zoom2=pacific_merc_1520x1140
pa_zoom3=pacific_merc_3040x2280
pa_zoom4=pacific_merc_4180x3135

pa_pz_zoom0=pacific_merc_380x285
pa_pz_zoom1=pacific_merc_760x570
pa_pz_zoom2=pacific_merc_1520x1140
pa_pz_zoom3=pacific_merc_3040x2280
pa_pz_zoom4=pacific_merc_4180x3135

default_zoom0=tropics_merc_380x285
default_zoom1=tropics_merc_760x570
default_zoom2=tropics_merc_1520x1140
default_zoom3=tropics_merc_3040x2280
default_zoom4=tropics_merc_4180x3135


[Title Settings]
add_title=1
title_small=1

title_TTF_font_pt=10
title_gdfont=MEDIUM

credit_TTF_font_pt=8

[Label Settings]
add_labels=1

TTF_font_pt=8

*/
?>
