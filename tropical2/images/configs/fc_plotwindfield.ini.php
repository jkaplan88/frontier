<?php
/*


[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
plotwindfield=,15,tropmap.html,MakeTropMap,main,0


%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
plot_windfield=1
plot_current_position=1

pcl_method=circle
pcl_color=FFFFFF
pcl_line_color=000000
pcl_filled=1
pcl_width=<? ('%%zoom%%' > 1) ? 6 : 4 ?>
pcl_height=<? ('%%zoom%%' > 1) ? 6 : 4 ?>

[plotwindfield Colors]
34=FFA800
50=
64=FF0000

[infobox settings]
y=58

[Title Settings]
title_text=<? ('%%INI:Title Settings:title_small%%') ? preg_replace('/Tropical Depression/i', 'TD', $this->storm_name . ' Wind Field') : $this->storm_name . ' Wind Field' ?>



[Legend Settings]
add_legend=1

add_method=image
legend_image=tropwindfield_legend_640x480.png
legend_x=0
legend_y=0


*/
?>