<?php
/*
[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
plotsystemforecast=,15,tropmap.html,MakeTropMap,main,0


%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
plot_tracks=1
plot_forecasts=1
plot_current_position=<? ('%%zoom%%' > 0) ? 0 : 1 ?>
color_section=tropsystem Colors
use_track_minmax=<? ('%%zoom%%' > 1) ? 0 : 1 ?>


[Title Settings]
title_text=<? (preg_match('/active/', $this->form['eventnum'])) ? 'Active Tropical Systems' . ( (preg_match('/invest/', $this->form['eventnum'])) ? ' and Invests' : '') : ( ($this->form['eventnum'] == 'invests') ? 'Active Tropical Invests' :   ( ('%%INI:Title Settings:title_small%%') ? preg_replace('/Tropical Depression/', 'TD', $this->storm_name) : ($this->storm_name . ' Forecast Track') )) ?>
%%LOAD_CONFIG=troplegend%%


*/
?>