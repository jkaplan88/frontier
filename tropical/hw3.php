<?php
/*
#######################
#  This script is copyright(c) 2008 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: hw3cacher.php,v 1.8 2008/02/12 19:04:33 wxfyhw Exp $
*
*/

error_reporting  (E_ALL);
ini_set('display_errors','1');
if (function_exists('microtime'))$HWCtime_start = microtime(true);

$HWCcache_path = './cacheoutput/';
$HWChw_path = './hw3_real.php';
$HWCcookiename = 'HW3';

$HWCshow_details = false;
$HWCcache_clean_interval = 120;


// allow_nocache:
// if set to anything but 0 or false, then passing nocache=xxxx where xxxx is the same value will prevent caching
$HWCallow_nocache = '1';

// the default cache time in minutes
$HWCcache_time = 10;  // 10 minutes


// force the script to clear out the cache directory:
// if set to anything but 0 or false, then passing HWClear=xxxx where xxxx is the same value will prevent caching
$HWCallow_clear = '1';



// the default mime to use: Nomrally HTML
$HWCmime = 'html';	//default mime to use

// cache rules to follow, If none found then assume true
$HWCcache_rules = array(
		// cache rules are arrays to run on query string
		// array( '/pattern/', Cache? True or False, max cache time in minutes, cachename_prepend, o optional mime [html by default]),
		// array( '/forecast=zandh/', true,10, 'zandh'),
		// array( '/forecast=afd/', true,30, 'afd'),
		array( '/forecast=warnings/', 	true, 1, 'warnings_'),
		array( '/\bpng\b/', false, 0, '', 'png'),
		array( '/\bgif\b/', false, 0, '',  'gif'),
		array( '/\bjpg\b/', false, 0, '', 'jpg'),
		array( '/pass=yourweather_options/', 	false, 0),


	);

$HWCuseSubDirs = true;

//*******************************************
//
// NORMALLY you do not change anything below this line
//
//*******************************************

$HWCmimes = array(
		'js' => 'text/javascript',
		'html' => 'text/html',
		'xml' => 'text/xml',
		'mp3' => 'audio/mpeg',
		'wav' => 'audio/x-wav',
		'png' => 'image/png',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',

	);




if (!preg_match('/[\/]$/',$HWCcache_path)) $HWCcache_path .= '/';
if (!is_dir($HWCcache_path)) { print "Cache directory not created"; exit;}

if ($HWCcache_clean_interval > 0) HWCclean_cache($HWCcache_path,$HWCcache_clean_interval);

if ($HWCallow_clear && !empty($_GET['HWClear']) && $HWCallow_clear == $_GET['HWClear']) HWCdeltree($HWCcache_path);

$HWCnocache =($HWCallow_nocache && !empty($_GET['nocache']) && $HWCallow_nocache == $_GET['nocache']) ? true : false;

$HWCquery = (!empty($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : '';

// if Post forecast is set we will not cache
if ((!isset($_GET['forecast']) && !isset($_GET['do'])) && (!empty($_POST['forecast']) || !empty($_POST['do']))) $HWCnocache = true;




$HWCurl = (empty($_SERVER['SERVER_NAME'])) ? '' : $_SERVER['SERVER_NAME'];
if (!empty($_SERVER['REQUEST_URI'])) $HWCurl .=  $_SERVER['REQUEST_URI'];
elseif (!empty($_SERVER['PHP_SELF'])) $HWCurl .=  $_SERVER['PHP_SELF'] . '?' . $HWCquery;
else $HWCurl .= '?' . $HWCquery;

$HWCcache_name = $HWCcache_file = '';
$HWCbcache = false;

if (!$HWCnocache && !empty($_COOKIE[$HWCcookiename])) {
	if (!preg_match('/\b(forecast|do)=\w/', $HWCquery)) $HWCnocache= true;
}


if (!$HWCnocache) {
	$HWCbcache = true;
	foreach ($HWCcache_rules as $HWCcache_rule) {
		list ($HWCRpattern, $HWCRbcache, $HWCRcache_time) = $HWCcache_rule;
		$HWCnot = false;
		if (substr($HWCRpattern,0,1) == '!' ) {
			$HWCRpattern = substr($HWCRpattern,1);
			$HWCnot = true;
		}

		if ((!$HWCnot && preg_match($HWCRpattern, $HWCurl)) || ($HWCnot && !preg_match($HWCRpattern, $HWCurl))) {
			$HWCbcache = $HWCRbcache;
			$HWCcache_name = (!empty($HWCcache_rule[3])) ? $HWCcache_rule[3] : '';
			$HWCcache_time = $HWCRcache_time;
			if (!empty($HWCcache_rule[5])) $HWCmime = $HWCcache_rule[5];

			break;
		}
	}

	if ($HWCbcache) {


		if ($HWCcache_name) $HWCcache_name .= '_';
		$HWCcache_name .=  md5($HWCurl);

		if ($HWCuseSubDirs) {
			$char1 = substr($HWCcache_name,0,1);
			$char2 = substr($HWCcache_name,1,1);

			if (!is_dir($HWCcache_path . $char1)) mkdir($HWCcache_path . $char1);
			if (!is_dir($HWCcache_path . $char1 . '/' . $char2)) mkdir($HWCcache_path . $char1 . '/' . $char2);
			$HWCcache_file = $HWCcache_path . $char1 . '/' . $char2 . '/' . $HWCcache_name;
			if ($HWCcache_clean_interval > 0) HWCclean_cache($HWCcache_path . $char1 . '/' . $char2 . '/',$HWCcache_clean_interval, true);
		}
		else {
			$HWCcache_file = $HWCcache_path . $HWCcache_name;
		}

		if (file_exists($HWCcache_file)) {
			$HWCage = (time() - filemtime($HWCcache_file))/60;

			if ($HWCage <= $HWCcache_time) {
				if ($handle = fopen($HWCcache_file, "r")) {
					if (isset($HWCmimes[$HWCmime])) {header("Content-Type: " . $HWCmimes[$HWCmime]);}
					else {header("Content-Type: $HWCmime"); }

					if (!$HWCshow_details || $HWCmime != 'html') header("Content-Length: ".filesize($HWCcache_file));

					while ($contents = fread($handle, 8192)) {

							print $contents;

					}
					fclose($handle);


					if ($HWCshow_details && $HWCmime == 'html') HWCshow_details($HWCtime_start, "Used Cache: $HWCcache_name");
					exit();
				}
			}
		}

		if ($HWCbcache) ob_start();
	}

}



require_once($HWChw_path);



if ($HWCcache_file && $HWCbcache) {
	if ($fp = fopen($HWCcache_file, 'w')) {
		// save the contents of output buffer to the file
		fwrite($fp, ob_get_contents());
		// close the file
		fclose($fp);
		@chmod($HWCcache_file,0666);
	}
	// Send the output to the browser
	ob_end_flush();
}


if ($HWCbcache) $msg = "Updated Cache: $HWCcache_name";
else $msg = "No Cache Used";
if ($HWCshow_details && $HWCmime == 'html') HWCshow_details($HWCtime_start);

exit;


function HWCshow_details($time_start, $msg = '') {
	if (function_exists('microtime') && function_exists('memory_get_peak_usage')) {
		$time_end = microtime(true);$time = round($time_end - $time_start, 3);
		print  round(memory_get_peak_usage(true) / 1024 / 1024, 2) . "MB : $time seconds";
		if ($msg) print " : $msg";
	}
}


function HWCclean_cache ($dir,$maxage, $noFlag = false) {
	if (!$dir || !is_dir($dir)) return;

	if (!preg_match('/[\/]$/',$dir)) $dir .= '/';

	$cache_file = $dir . 'cache.txt';

	if ($noFlag || !file_exists($cache_file) || ( (time() - filemtime($cache_file))/60 > $maxage)) {
		if (!$noFlag && $fh = fopen($cache_file,"w")) {
			fwrite($fh, time());
			fclose($fh);
			@chmod($cache_file,0666);
		}

		if ($dh = opendir($dir)) {
        	while (($file = readdir($dh)) !== false) {
        		if (preg_match('/^\w+$/', $file) && !is_dir($dir . $file) && (time() - filemtime($dir . $file))/60 >= $maxage) {
        			unlink ($dir .  $file);
        		}
        	}
        	closedir($dh);
        }
    }
}

function HWCdeltree( $f, $i=0 ){
        if( is_dir( $f ) ){
            foreach( scandir( $f ) as $item ){
                if( !preg_match('/^(\w+|cache\.txt)$/', $item) ) continue;
                if (substr($f,-1,1) != '/') $f .= '/';
                HWCdeltree( $f . $item, $i+1 );
            }
            if ($i > 0) rmdir( $f );
        } else {
            unlink( $f );
        }
    }



?>