<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/WV/20.jpg


[TropicalZones]
total_zones=21

1=PZZ110
2=PZZ150
3=PZZ153
4=PZZ156
5=PZZ210
6=PZZ250
7=PZZ255
8=PZZ350
9=PZZ353
10=PZZ356
11=PZZ450
12=PZZ455
13=PZZ530
14=PZZ550
15=PZZ555
16=PZZ650
17=PZZ655
18=PZZ670
19=PZZ673
20=PZZ750
21=PKZ041


[DisplayCities]
total_cities=21

1=seattle,wa
2=portland,or
3=medford,or
4=san+francisco,ca
5=fresno,ca
6=reno,nv
7=boise,id
8=salt+lake+city,ut
9=las+vegas,nv
10=los+angeles,ca
11=san+diego,ca
12=phoenix,az
13=el+paso,tx
14=laredo,tx
15=tucson,az
16=eureka,ca
17=MMLP
18=MMCU
19=MMMY
20=MMLM
21=MMTM

*/
?>