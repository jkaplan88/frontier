<?php
/*


%%LOAD_CONFIG=hw3image%%

[SystemSettings]
mime_type=

[Defaults]
; the default extension to use in graphics..
; if using GD.pm 1.19 or older should be set to "gif"
; for GD.pm 1.20+ it can either be "png" or "jpg"
gd_ext=jpg

[HWV Presets]
hwvgdext=%%INI:Defaults:gd_ext%%


*/
?>