<?php

/*

[URLs]
;;rss_url should be set tot he domain of the server
;containg the data you need
rss_url=rss.msnbc.msn.com

;;rss_prefix should be set to the path on the domain where
;the needed data is located. Note that the plugin will allow
;you to use simple "INI" variables. Parameters passed in the
;query string (or form post), the cache_path or data obtained from
;looking up information on a location. In close the variable name with
;"%%" as in "%%state%%". You can also prepend the variable name with
;"lc_" to turn the result to lowercase or "uc_" for uppercase.
rss_prefix=/id/3032127/device/rss/rss.xml

;rss_cache_file should be set to the path to cache the data
rss_cache_file=%%cache_path%%/msnwxnews.rss

[ForecastTypes]
# type = IWIN/URL, max age, template,parse code
;; Set the beginning portion the "type" to the value you will pass in the
;"forecast" parameter. The right side of the equal sign is:
;    IWIN/URL : no longer used
;   Max Age : used to control the max age in to cache the data for
;   template : the template to use when outputting the data
;   Plugin Module : Set to "FetchRSS"
;   Plugin routine name : set to "fetch_rss"
;   ParseCode : Normally 0 for this plugin, set it to 1 if you want
;                       HW3 to obtain information on a location before
;                       fetching the data
msnwxnews=notused,60,rss.html,FetchRSS,fetch_rss,0



*/
?>