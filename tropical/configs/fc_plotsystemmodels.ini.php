<?php
/*
[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
plotsystemmodels=,15,tropmap.html,MakeTropMap,main,0


%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
plot_models=1
plot_current_position=1
plot_info_box=0
color_section=tropsystem Colors

#autocrop_resample=<? ('%%zoom%%' == 2) ? 0 : 1 ?>





#if make_image_map=1 then when plotting the map
# an imagemap file will be made as well, which can be used
# with in the html. This is useful if you want
# to allow a map to clickable
make_image_map=1

image_map_name=tropimap
#image_map_save_name=%%usemap%%
#image_map_save_name=%%usemap%%
image_map_save_ext=imap

image_map_url_current_pos=[scripturl]?forecast=tropsystems&region=[stormregion]&hwvregstormid=[stormeventnum]&year=[stormyear]&eventnum=[stormeventnum]&alt=tropsystempage

image_map_other_current_pos=onclick="return false;" onmouseover="toolTip('<div id=\'HWtropinfo\'  style=\'height:99px;\'><div id=\'storm\'>[stormtype_full]</div><div id=\'details\'><span class=\'title\'>Position at <span class=\'date\'>[stormadvisorydate]</span></span><div class=\'row\'><div class=\'label\'>Position:</div><div class=\'data\'>[stormlat], [stormlon]</div></div><div class=\'row\'><div class=\'label\'>Winds:</div><div class=\'data\'>[stormwind_mph] mph ([stormwind_kts] kts)</div></div><div class=\'row\'><div class=\'label\'>Pressure:</div><div class=\'data\'>[stormpressure_mb] mb ([stormpressure_in] in)</div></div></div></div>', '#333333', '#FFFFFF', true);" onmouseout="toolTip();"


image_map_url_models=#
image_map_other_models=onclick="return false;" onmouseover="toolTip('<div id=\'HWtropinfo\'  style=\'background-color:#E4E4E4;height:99px;\'><div id=\'storm\'>[model_tech] Model</div><div id=\'details\'  ><span class=\'title\'>Position at <span class=\'date\'>[fdate]</span></span><div class=\'row\'><div class=\'label\'>Storm Type:</div><div class=\'data\'>[stormtype_full]</div></div><div class=\'row\'><div class=\'label\'>Position:</div><div class=\'data\'>[model_lat][model_lat_dir], [model_lon][model_lon_dir]</div></div><div class=\'row\'><div class=\'label\'>Winds:</div><div class=\'data\'>[model_wind_mph] mph ([model_wind_kts] kts)</div></div></div></div>', '#333333', '#FFFFFF', true);" onmouseout="toolTip();"




[Legend Settings]
add_legend=<? ('%%nolegend%%') ? 0 : 1 ?>

add_method=image
legend_image=tropmodels_legend_640x480.png

legend_x=0
legend_y=0



[Title Settings]
#title_text=<? ($this->form['eventnum'] == 'active') ? 'Active Tropical System Models' : ( ('%%INI:Title Settings:title_small%%') ? preg_replace('/Tropical Depression/', 'TD', $this->storm_name) : ($this->storm_name . ' Models') ) ?>
title_text=<? 'Active Tropical System Models' ?>



[model Colors]
CARQ=996699

WRNG=990066

OFCL=ffffff
OFCI=eaeaea
OFC2=d9d9d9

OHPC=c2c2c2
OOPC=b2b2b2
OMPC=a8a8a8

XTRP=336699

HURN=339999

CLIP=66cc66
CLP5=33cc66

A67=00be16
A72=00aa14
A73=009612
A83=00730e
A90E=31a53e
A90L=45c354
90AE=5bb665
90BE=71db7e
A98E=6fb577
A9UK=9fd2a5

PSS=a6fca6
PSDE=98e698
PSDL=89ce89
P91E=79b579
P91L=6da26d
P9UK=5e8b5e

SBAR=99cc33

BAMD=05b1c8
BAMM=00e1ff
BAMS=8df1fe
BAMA=83cad4
BAMB=6fc0cb
BAMC=68afb8

VBAR=cd4800
VBRI=e75302
LBAR=ff5a00
FSSE=b33f00
FSSI=b15421
MFM=d7733d
QLM=f78f56
QLMI=d78559
HWRF=f9be9d
HWFI=dda383
HWF2=ba7b59

GFDL=90ff00
GFDI=7fdf02
GFD2=70c402
GHMI=5da401
GHM2=4c8601
GFDT=3d6b01
GFTI=5b7736
GFT2=637848
GTSI=729149
GTS2=8db559
GFDA=a2d363
GFDE=b2de79
GFDU=cdf895
GFUI=b8d78f
GFDC=9eb383
GFCI=839a66

GFDN=8400ff
GFNI=7524c1
GFN2=8153ac

ETA=b9d5ff
ETAI=9db6da
ETA2=8aa1c4

NAM=00ffc0
NAMI=00dba5
NAM2=01b589

NGM=0078ff
NGMI=006ae2

AFW1=ffb5db
AF1I=e1a1c2
AF12=c991ae
MM36=d8b7c8
M36I=f3cde1

COCE=ff6af6
COEI=e25fda
COE2=cc56c5
COAL=ff8bf8
COAI=dd79d7
COA2=eba6e7

AVNO=ff0000
AVNI=db0303
AVN2=b90202
AP01=9f0101
AP02=7f0001
AP03=832626
AP04=a33030
AP05=c94141
AP06=f15555
AP07=f08888
AP08=cf7c7c
AP09=a76666
AP10=ee8caa
AP11=bf5f7c
AP12=8f3652
AP13=b54064
AP14=e14f7c
AP15=b41f4d
AP16=88193b
AP17=8b042d
AP18=c1043e
AP19=ec0048
AP20=ec008b
AEMN=c10272
AEMI=d33994
AEM2=f946af
AMMN=e272b4

MRFO=ffff99

UKM=6ea74f
UKMI=73985f
UKM2=91ac82

EGRR=fff800
EGRI=e0da01
EGR2=bbb600
UKX=f9f696
UKXI=d1cf7d
UKX2=aba967

NGPS=ffa300
NGPI=d28701
NGP2=ae6f01
NGX=faca75
NGXI=d1a861
NGX2=a9884e

CMC=b400ff
CEMN=9002cb

ECM=ffd200
ECMI=e9c000
ECM2=d0ab01
EMX=b69601
EMXI=a38600
EMX2=8a7201
EEMN=fadb49
ECMO=e2c642
ECOI=cfb53f
ECO2=ddca70
EEMO=f5e492

FV5=cccc99
FVGI=999966

GUNS=ffbd48
GUNA=dfa53e
CGUN=c49c56
CONU=e1b76c
CONE=e5c285
CCON=ccb387

DEF1=c076ff
SHF5=a565db
DSHF=8a55b7
SHIP=cea2f4
DSHP=b188d4
SHPP=9a76b9
DSPP=7f609a

ICON=a4d4da
HCON=8fb5ba
CONI=769ea3

MRCL=c2b2ff
MRCI=ac9ee2
DRCL=988cc8
DRCI=8479ac

OCD5=9999cc
BCD5=ccccff




*/
?>