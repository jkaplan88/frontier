<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/WV/20.jpg


[TropicalZones]
total_zones=11

1=PZZ356
2=PZZ450
3=PZZ455
4=PZZ530
5=PZZ550
6=PZZ555
7=PZZ650
8=PZZ655
9=PZZ670
10=PZZ673
11=PZZ750


[DisplayCities]
total_cities=19

1=los+angeles,ca
2=san+diego,ca
3=blythe,ca
4=san+bernardino,ca
5=santa+clara,ca
6=las+vegas,nv
7=yuma,az
8=goleta,ca
9=bakersfield,ca
10=fresno,ca
11=salinas,ca
12=san+jose,ca
13=san+francisco,ca
14=sacramento,ca
15=reno,nv
16=chico,ca
17=redding,ca
18=eureka,ca
19=medford,or

*/
?>