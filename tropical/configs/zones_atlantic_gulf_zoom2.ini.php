<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/WV/20.jpg

[TropicalZones]
total_zones=46

1=AMZ150
2=AMZ152
3=AMZ154
4=AMZ156
5=AMZ158
6=AMZ250
7=AMZ252
8=AMZ254
9=AMZ256
10=AMZ270
11=AMZ350
12=AMZ352
13=AMZ354
14=AMZ450
15=AMZ452
16=AMZ454
17=AMZ550
18=AMZ555
19=AMZ650
20=AMZ651
21=GMZ052
22=GMZ053
23=GMZ054
24=GMZ031
25=GMZ032
26=GMZ075
27=GMZ075
28=GMZ150
29=GMZ155
30=GMZ250
31=GMZ255
32=GMZ350
33=GMZ355
34=GMZ450
35=GMZ455
36=GMZ550
37=GMZ555
38=GMZ650
39=GMZ655
40=GMZ656
41=GMZ657
42=GMZ750
43=GMZ755
44=GMZ850
45=GMZ853
46=GMZ856

[DisplayCities]
total_cities=12

1=brownsville,tx
2=houston,tx
3=dallas,tx
4=new+orleans,la
5=mobile,al
6=jacksonville,fl
7=jackson,ms
8=birmingham,al
9=atlanta,ga
10=tampa,fl
11=miami,fl
12=columbia,sc

%%LOAD_CONFIG=marinezones%%

*/
?>