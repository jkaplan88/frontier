<?php
/*
[Defaults]
gd_ext=png

template_ext=hwi3

default_ttf=activa

default_repeat=0

default_maxdays=0

plot_cities=1


[Paths]
hw3image_cache=%%INI:Paths:html_side%%/images/hw3image
hw3image_cache_url=%%INI:Paths:html_side_url%%/images/hw3image

hw3image_save=%%INI:Paths:html_side%%/images/hwi3_mapset/generated
hw3image_save_url=%%INI:Paths:html_side_url%%/images/hwi3_mapset/generated


hw3image_fcicons=%%INI:Paths:html_side%%/images/hwi_fcicons
hw3image_mapicons=%%INI:Paths:html_side%%/images/hwi3_mapset/map_icons
hw3image_backgmaps=%%INI:Paths:html_side%%/images/hwi3_mapset
hw3image_tropical=%%INI:Paths:html_side%%/images/hwi3_mapset/tropical
hw3image_tropsymbols=%%INI:Paths:html_side%%/images/hwi3_mapset/graphics/tropsymbols


[HWV Presets]
hwvplot_cities=1


#title Bar settings
hwvadd_title=1
hwvadd_legend=1
hwvtitle_height=30
hwvtitle_color=4F4B89
hwvtitle_blend=80
hwvlegend_blend=80
hwvtitle_use_ttf=1
hwvtitle_font=arial
hwvtitle_font_pt=12
hwvtitle_font_color=FFFFFF
hwvbaseline_color=FFFFFF
hwvtitle_right_string=Frontier Weather, Inc.
hwvcitycolor=yellow
hwvcitypoint=white


hwimage_temp=/tmp


# the following is the file path to the directory where the True Type Fonts are stored
ttf_path=FONTDIR_ABSOLUTE_PATH

[CleanCacheDirs]
clean_cache=%%INI:CleanCacheDirs:clean_cache%%,%%INI:Paths:hw3image_cache%%

*/
?>