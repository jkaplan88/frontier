<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/WV/20.jpg


[TropicalZones]
total_zones=12

1=PZZ110
2=PZZ150
3=PZZ153
4=PZZ156
5=PZZ210
6=PZZ250
7=PZZ255
8=PZZ350
9=PZZ353
10=PZZ356
11=PZZ450
12=PZZ455


[DisplayCities]
total_cities=14

1=bellingham,wa
2=seattle,wa
3=tacoma,wa
4=yakima,wa
5=vancouver,wa
6=ocean+shores,wa
7=kennewick,wa
8=portland,or
9=salem,or
10=eugene,or
11=bend,or
12=coos+bay,or
13=medford,or
14=astoria,or

*/
?>