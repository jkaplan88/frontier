<?php
/*
%%LOAD_CONFIG=general_warnings%%


[ForecastTypes]
warnings=allwarnings.html,4,warnings.html,FetchWarnings,warnings,1


[URLs]
warnings_url=www.weather.gov|www.srh.noaa.gov|iwin.nws.noaa.gov
warnings_prefix=/alerts/wwarssget.php?zone=%%zone%%|/showsigwx.php?warnzone=%%zone%%&warncounty=%%warncounty%%|/iwin/%%state%/allwarnings.html
warnings_cache_file=%%cache_path%%/%%forecast%%-%%zone%%.rss|%%cache_path%%/%%forecast%%-%%zone%%.txt

[HWV Presets]
hwvPragma=<meta http-equiv="Pragma" content="no-cache">
hwvExpires=<meta http-equiv="expires" content="0">
hwvPragma_II=<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">

[Warnings Addin]
; the following line can be used to control what items display when using the weather.gov rss feeds for the warnings
types=ALL !NOW !HWO !SPS

*/
?>