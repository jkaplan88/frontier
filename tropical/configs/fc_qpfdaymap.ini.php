<?php
/*
%%LOAD_CONFIG=fc_qpfday%%

[ForecastTypes]
qpfdaymap=qpfday.html,30,qpfdaytext.html,MakeQPFMap,main,0


; load the image pluing api configs, which we can then
; override. This allows us to only change items we need to
; while allowing the end user to keep font sizes, credits
; set in a single config file.
%%LOAD_CONFIG=imagepluginapi%%


[Image Settings]
notavail_string=NO DATA AVAILABLE AT THIS TIME


[qpfdaymap Colors]
1=97C8FF
25=01C0FF
50=00FF00
100=FFFF00
150=FA834B
200=FF0000
300=990099
400=FF40FF
500=FFC0FF
600=FFFFFF
700=FFFFFF
800=FFFFFF
900=FFFFFF
1000=FFFFFF



[Legend Settings]
add_legend=1


label_what=($i < 100) ? '.' . sprintf('%02u', $i) . '"' : (($i == 600) ? '6+"' : $i/100 . '"')
label_xoffset=21
label_yoffset=0

legend_order=1,25,50,100,150,200,300,400,500,600


[Title Settings]
add_title=1

title_text=24hr Quantitative Precip Forecast


[Labels]
issue_date=Issued: %%qpf_issuetime%% %%qpf_issuedate%%|10|bottom|left|bottom
notavail=%%notavail%%|center|center|center|center
valid_date=<? ('%%qpf_validepoch%%') ? 'Valid at: ' . ((date('I')) ? gmdate('g:i A', %%qpf_validepoch%%-4*3600) . ' EDT' . strtoupper(gmdate(' D M j Y', %%qpf_validepoch%%-4*3600))   : gmdate('g:i A', %%qpf_validepoch%%-5*3600) . ' EST' . strtoupper(gmdate(' D M j Y', %%qpf_validepoch%%-5*3600))) : '' ?>|10|<? $h-28 ?>|left|bottom


[PlotCities Settings]
plotcities=1


[PlotCities]
#name=pands|DisplayName|x|y|halign|valign
# if x and Y are not set then HW will query the db to
# determine the x/y location
# if DisplayName is not set, then HW3 will use the
# name as found from the Database
1=houston,tx
2=miami,fl
3=norfolk,va
4=boston,ma
5=chicago,il
6=atlanta,ga
7=denver,co
8=dallas,tx
9=seattle,wa
10=san francisco,ca
11=phoenix,az
12=los angeles,ca
13=boise,id
14=Minneapolis,MN


*/
?>