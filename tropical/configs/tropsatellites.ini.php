<?php
/*

%%LOAD_CONFIG=fc_tropicalinfo%%


[HW Sats]
prefix=http://js.hamweather.net/sats/wx1T8Z8PCsGHAX/640x480/
suffix=
NT=trop
GMX=gulf
CRB=crb
PZ=tropw
PA=tropw
WATL=watl
NWATL=nwatl

#satellite types... use the code from the source url
bwir=bwir
clir=clir
vis=vis
wv=wv


[HW Sats Bounds]
gmx_lon1=-99.8035
gmx_lat1=18.4822
gmx_lon2=-81.8935
gmx_lat2=31.5113

crb_lon1=-80.1994
crb_lat1=8.1155
crb_lon2=-58.538
crb_lat2=22.153

watl_lon1=-80.5221
watl_lat1=24.1295
watl_lon2=-68.2191
watl_lat2=34.2139

nwatl_lon1=-76.569
nwatl_lat1=35.7064
nwatl_lon2=-63.9433
nwatl_lat2=41.5957


[NOAA Sats]
prefix=http://www.ssd.noaa.gov/goes/
suffix=
satext=jpg

NT=east/natl/
GMX=east/gmex/
CRB=
PZ=west/nepac/
PA=west/cpac/
#satellite types... use the code from the source url
bwir=ir4
clir=avn
vis=vis
wv=wv


*/
?>
