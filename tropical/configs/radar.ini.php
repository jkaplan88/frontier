<?php

/*

[Radar Options]

# set radar_type=ridge for the new ridge radars
# or set it to blank for the normal nexrad radar
radar_type=ridge

# the following are optional overlays you can add
# for the ridge radars
# to turn off an overlay from plotting change the value to 0
legend=1
topo=1
rivers=1
highways=1
county=1
city=1
warnings=1
latlon=0

*/

?>