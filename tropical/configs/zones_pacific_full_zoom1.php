<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/WV/20.jpg


[TropicalZones]
total_zones=0

[DisplayCities]
total_cities=16

1=houston,tx
2=los_angeles,ca
3=san+francisco,ca
4=seattle,wa
5=portland,or
6=eureka,ca
7=phoenix,az
8=salt+lake+city,ut
9=honolulu,hi
10=MMMX
11=MMLP
12=MMAA
13=MGGT
14=CYPR
15=MMCP
16=MMCU

*/
?>