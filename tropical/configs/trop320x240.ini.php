<?php
/*

%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
autocrop_width=320
autocrop_height=240


[tropsystem automaps]
nt_zoom0=atlantic_merc_320x240
nt_zoom1=atlantic_merc_640x480
nt_zoom2=atlantic_merc_1280x960
nt_zoom3=atlantic_merc_2560x1920
nt_zoom4=atlantic_merc_3520x2640

pz_zoom0=tropics_merc_320x240
pz_zoom1=tropics_merc_640x480
pz_zoom2=tropics_merc_1280x960
pz_zoom3=tropics_merc_2560x1920
pz_zoom4=tropics_merc_3520x2640

pa_zoom0=pacific_merc_320x240
pa_zoom1=pacific_merc_640x480
pa_zoom2=pacific_merc_1280x960
pa_zoom3=pacific_merc_2560x1920
pa_zoom4=pacific_merc_3520x2640

pa_pz_zoom0=pacific_merc_320x240
pa_pz_zoom1=pacific_merc_640x480
pa_pz_zoom2=pacific_merc_1280x960
pa_pz_zoom3=pacific_merc_2560x1920
pa_pz_zoom4=pacific_merc_3520x2640

default_zoom0=tropics_merc_320x240
default_zoom1=tropics_merc_640x480
default_zoom2=tropics_merc_1280x960
default_zoom3=tropics_merc_2560x1920
default_zoom4=tropics_merc_3520x2640


[Title Settings]
add_title=1
title_small=1

title_TTF_font_pt=10
title_gdfont=MEDIUM

credit_TTF_font_pt=8

[Label Settings]
add_labels=1

TTF_font_pt=8

*/
?>
