<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/WV/20.jpg


[TropicalZones]
total_zones=26

1=GMZ052
2=GMZ053
3=GMZ054
4=GMZ031
5=GMZ032
6=GMZ075
7=GMZ075
8=GMZ150
9=GMZ155
10=GMZ250
11=GMZ255
12=GMZ350
13=GMZ355
14=GMZ450
15=GMZ455
16=GMZ550
17=GMZ555
18=GMZ650
19=GMZ655
20=GMZ656
21=GMZ657
22=GMZ750
23=GMZ755
24=GMZ850
25=GMZ853
26=GMZ856


[DisplayCities]
total_cities=16

1=marfa,tx
2=brownsville,tx
3=san+antonio,tx
4=MMMX
5=MMLP
6=MMCU
7=MMMY
8=MMLM
9=MMTM
10=MMDO
11=MMAA
12=MMMT
13=MMCP
14=MZBZ
15=MSSS
16=MHTG

*/
?>