<?php
/*

[WxInfo]
# type=DayDisplayName|Day Icon|Night Icon|NightDisplayName
# If no NightDisplayName is given then the DayDisplayName is used
# if no Night Icon is given then the Day Icon will be used

BLIZZARD CONDITION=Blizzard Conditions|blizzard.gif|blizzard.gif
BLIZZARD=Blizzard|blizzard.gif|blizzard.gif
INCREASING CLOUDS=Increasing Clouds|mcloudy.gif|mcloudyn.gif
BECOMING CLOUDY=Becoming Cloudy|mcloudy.gif|mcloudyn.gif
HAZY=Hazy|hazy.gif|hazy.gif
HAZE=Hazy|hazy.gif|hazy.gif
HZ=Hazy|hazy.gif|hazy.gif
SUN AND CLOUD=Partly Cloudy|pcloudy.gif|pcloudyn.gif
FEW CLOUD=Partly Cloudy|pcloudy.gif|pcloudyn.gif
PARTIAL CLEARING=Partial Clearing|mcloudy.gif|mcloudyn.gif
CLEARING=Clearing|pcloudy.gif|pcloudyn.gif
VARIABLE CLOUDINESS=Partly Cloudy|pcloudy.gif|pcloudyn.gif
VARIABLE CLOUDS=Variable Clouds|pcloudy.gif|pcloudyn.gif
BLOWING SNOW=Blowing Snow|blowingsnow.gif|blowingsnow.gif
DRIFTING SNOW=Drifting Snow|blowingsnow.gif|blowingsnow.gif
RAIN\b[^.]+\bto\b[^.]+SNOW\b=Rain to Snow|rainandsnow.gif|rainandsnow.gif
SNOW\b.+\bto\b.+FREEZING.+?RAIN\b=Ice/Snow Mixture|rainandsnow.gif|rainandsnow.gif
SNOW\b[^.]+\bto\b[^.]+RAIN\b= Snow to Rain|rainandsnow.gif|rainandsnow.gif
RAIN\s+AND\s+SNOW\s+?SHOWERS=Rain/Snow Showers|rainandsnow.gif|rainandsnow.gif
FREEZING RAIN AND SNOW=Ice/Snow Mixture|rainandsnow.gif|rainandsnow.gif
RAIN AND (?:LIGHT)?\s*?SNOW=Rain and Snow|rainandsnow.gif|rainandsnow.gif
SNOW AND RAIN=Snow and Rain|rainandsnow.gif|rainandsnow.gif
CHANCE OF RAIN OR (?:LIGHT)?\s*?SNOW=Chance of Rain/Snow|rainandsnow.gif|rainandsnow.gif
FREEZING RAIN OR (?:LIGHT)?\s*?SNOW=Freezing Rain/Snow|rainandsnow.gif|rainandsnow.gif
RAIN OR (?:LIGHT)?\s+?SNOW=Rain or Snow|rainandsnow.gif|rainandsnow.gif
RAIN[^.]+?MIXED[^.]+SNOW=Wintry Mix|rainandsnow.gif|rainandsnow.gif
SNOW[^.]+?MIXED[^.]+RAIN=Wintry Mix|rainandsnow.gif|rainandsnow.gif
MIXPCPN=Mixed Precip|rainandsnow.gif|rainandsnow.gif
SLEET\s+?AND\s+?SNOW=Sleet and Snow|rainandsnow.gif|rainandsnow.gif
SLEET=Sleet|sleet.gif|sleet.gif
WINTRY MIX=Wintry Mix|rainandsnow.gif|rainandsnow.gif
CHANCE OF SNOW OR RAIN=Chance of Snow/Rain|rainandsnow.gif|rainandsnow.gif
SNOW OR RAIN=Snow or Rain|rainandsnow.gif|rainandsnow.gif
(?:PARTLY|MOSTLY) (?:CLOUDY|SUNNY).+THUNDERSTORM=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
CHANCE OF.+THUNDERSTORM=Chance of T-Storm|chancetstorm.gif|chancetstormn.gif
THUNDERSTORM=Thunder storms|tstorm.gif|tstormn.gif
TSRA=Thunder storms|tstorm.gif|tstormn.gif
(?<!\w)TS\b=Thunder storms|tstorm.gif|tstormn.gif
VCTS=Thunder storms|tstorm.gif|tstormn.gif
ISOLATED SHOWER=Isolated Showers|pcloudyr.gif|pcloudyrn.gif
HEAVY SNOW=Heavy Snow|snow.gif|snow.gif
SNOW SHOWER=Snow Showers|snowshowers.gif|snowshowers.gif
PARTLY CLOUDY.+SHOWERS LIKELY=Showers Likely|pcloudyr.gif|pcloudyrn.gif
PARTLY CLOUDY.+SHOWER=Chance of Showers|pcloudyr.gif|pcloudyrn.gif
(?:MOSTLY CLOUDY|PARTLY SUNNY).+SHOWERS LIKELY=Showers Likely|mcloudyr.gif|pcloudyrn.gif
(?:MOSTLY CLOUDY|PARTLY SUNNY).+SHOWER=Chance of Showers|mcloudyr.gif|pcloudyrn.gif
CHANCE OF SHOWER=Chance of Showers|showers.gif|showers.gif
SCATTERED SHOWER=Scattered Showers|showers.gif|showers.gif
RAIN SHOWER=Rain Showers|showers.gif|showers.gif
SHOWER=Showers|showers.gif|showers.gif
MAINLY CLOUDY=Mostly Cloudy|mcloudy.gif|mcloudyn.gif
CLOUDY PERIODS=Cloudy Periods|pcloudy.gif|pcloudyn.gif
FLURR=Flurries|flurries.gif|flurries.gif
FAIR=Fair|fair.gif|pcloudyn.gif
LIGHT SNOW=Light Snow|snowshowers.gif|snowshowers.gif
CHANCE OF SNOW(?![^.]+?PERCENT)=Chance of Snow|snow.gif|snow.gif
SNOW(?!\s+?LEVEL)=Snow|snow.gif|snow.gif
HEAVY SNOW=Snow|snow.gif|snow.gif
FRZDRZL=Freezing Drizzle|freezingrain.gif|freezingrain.gif
FREEZING DRIZZLE=Freezing Drizzle|freezingrain.gif|freezingrain.gif
FREEZING RAIN=Freezing Rain|freezingrain.gif|freezingrain.gif
HEAVY RAIN=Heavy Rain|rain.gif|rain.gif
LIGHT RAIN=Light Rain|showers.gif|showers.gif
PARTLY CLOUDY.+CHANCE OF RAIN(?![^.]+?PERCENT)=Chance of Rain|pcloudyr.gif|pcloudyrn.gif
MOSTLY CLOUDY.+CHANCE OF RAIN(?![^.]+?PERCENT)=Chance of Rain|mcloudyr.gif|pcloudyrn.gif
CHANCE OF RAIN(?![^.]+?PERCENT)=Chance of Rain|rain.gif|rain.gif
OCCASIONAL RAIN=Occasional Rain|rain.gif|rain.gif
RAIN\b=Rain|rain.gif|rain.gif
WINDY=Windy|wind.gif|wind.gif
BLUSTERY=Windy|wind.gif|wind.gif
PATCHY FOG=Patchy Fog|fog.gif|fog.gif
AREAS OF FOG=Areas of Fog|fog.gif|fog.gif
WIDESPREAD FOG=WIDESPREAD Fog|fog.gif|fog.gif
FOG.MIST=FOG and MIST|fog.gif|fog.gif
FOG.+?DRIZZLE=FOG and DRIZZLE|drizzle.gif|drizzle.gif
PATCHY.+?DRIZZLE=Patchy Drizzle|drizzle.gif|drizzle.gif
DRIZZLE=Drizzle|drizzle.gif|drizzle.gif
MIST=Mist|drizzle.gif|drizzle.gif
SMOKE=Smoke|smoke.gif|smoke.gif
FROZEN PRECIP=Frozen Precip|rainandsnow.gif|rainandsnow.gif
DRY=Dry|sunny.gif|sunnyn.gif
VARIABLE HIGH CLOUDINESS=Variable Cloudiness|pcloudy.gif|pcloudyn.gif
PARTLY CLOUDY=Partly Cloudy|pcloudy.gif|pcloudyn.gif
MOSTLY CLOUDY=Mostly Cloudy|mcloudy.gif|mcloudyn.gif
MOSTLY SUNNY=Mostly Sunny|sunny.gif|sunnyn.gif
PARTLY SUNNY=Partly Sunny|mcloudy.gif|mcloudyn.gif
SUNNY=Sunny|sunny.gif|sunnyn.gif
INCREASING CLOUDINESS=Increasing Clouds|mcloudy.gif|mcloudyn.gif
FOG=Fog|fog.gif|fog.gif
CLOUDY=Cloudy|cloudy.gif|cloudy.gif
OCCASIONAL SUNSHINE=Occasional Sunshine|mcloudy.gif|mcloudyn.gif
PARTIAL SUNSHINE=Partial Sunshine|mcloudy.gif|mcloudyn.gif
CUMULONIMBUS CLOUDS OBSERVED=Cumulonimbus Clouds Observed|pcloudy.gif|pcloudyn.gif
TOWERING CUMULUS CLOUDS OBSERVED=Partly Cloudy|pcloudy.gif|pcloudyn.gif
MORNING CLOUDS?=Morning Clouds|mcloudy.gif|mcloudyn.gif
CLOUDS?=Clouds|mcloudy.gif|mcloudyn.gif
OVERCAST=Overcast|cloudy.gif|cloudy.gif
MOSTLY CLEAR=Mostly Clear|sunny.gif|sunnyn.gif
CLEAR=Clear|sunny.gif|sunnyn.gif
ICE CRYSTALS=Ice Crystals|flurries.gif|flurries.gif
NO PRECIPITATION=Fair|fair.gif|pcloudyn.gif
LIGHTNING OBSERVED=Lightning Observed|tstorm.gif|tstormn.gif
THUNDER=Thunder|tstorm.gif|tstormn.gif
MILD AND BREEZY=mild and breezy|wind.gif|wind.gif
HOT AND HUMID=Hot And Humid|sunny.gif|sunnyn.gif
CONTINUED HOT=Continued Hot|sunny.gif|sunnyn.gif
FILTERED SUNSHINE=Filtered Sunshine|mcloudy.gif|mcloudyn.gif
N/A=N/A|na.gif|na.gif



*/
?>