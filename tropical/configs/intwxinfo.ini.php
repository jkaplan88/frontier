<?php
/*

[IntWxInfo]


CL=Clear|sunny.gif|sunnyn.gif
FW=Partly Cloudy|pcloudy.gif|pcloudyn.gif
SC=Partly Cloudy|pcloudy.gif|pcloudyn.gif
BK=Mostly Cloudy|mcloudy.gif|mcloudyn.gif
OV=Overcast|cloudy.gif|cloudyn.gif

RA=Rain|rain.gif|rain.gif
RASH=Showers|pcloudyr.gif|pcloudyrn.gif
SN=Snow|snow.gif|snow.gif
SNSH=Snow Showers|snowshowers.gif|snowshowers.gif
RASN=Rain and Snow|rainandsnow.gif|rainandsnow.gif
FL=Flurries|flurries.gif|flurries.gif
FZ=Freezing Rain|freezingrain.gif|freezingrain.gif
FZDZ=Freezing Drizzle|freezingrain.gif|freezingrain.gif
SL=Sleet|sleet.gif|sleet.gif

; following mixutres can also potentially occur
FZSL=Sleet and Freezing Rain|sleet.gif|sleet.gif
FZRA=Rain and Freezing Rain|freezingrain.gif|freezingrain.gif
FZSLRA=Rain and Ice|freezingrain.gif|freezingrain.gif
FZSN=Ice and Snow|rainandsnow.gif|rainandsnow.gif
FZSLSN=Ice and Snow|rainandsnow.gif|rainandsnow.gif
SLRA=Rain and Sleet|sleet.gif|sleet.gif
SLSN=Sleet and Snow|rainandsnow.gif|rainandsnow.gif
MIX=Wintry Mix|rainandsnow.gif|rainandsnow.gif





FWRA=Rain Possible|pcloudyr.gif|pcloudyrn.gif
FWVLRA=Sprinkles Possible|pcloudyr.gif|pcloudyrn.gif
FWLRA=Light Rain Possible|pcloudyr.gif|pcloudyrn.gif

SCRA=Rain Possible|pcloudyr.gif|pcloudyrn.gif
SCVLRA=Sprinkles Possible|pcloudyr.gif|pcloudyrn.gif
SCLRA=Light Rain Possible|pcloudyr.gif|pcloudyrn.gif

BKRA=Occassional Showers|mcloudyr.gif|mcloudyrn.png
BKVLRA=Occassional Sprinkles|mcloudyr.gif|mcloudyrn.png
BKLRA=Occassional Light Rain|mcloudyr.gif|mcloudyrn.png

OVRA=Rain|rain.gif|rain.gif
OVVLRA=Cloudy with Light Rain|rain.gif|rain.gif

FWRASH=Showers Possible|pcloudyr.gif|pcloudyrn.gif
SCRASH=Showers Possible|pcloudyr.gif|pcloudyrn.gif
BKRASH=Occassional Showers|mcloudyr.gif|pcloudyrn.gif

FWFL=Flurries Possible|pcloudys.gif|pcloudysfn.png
SCFL=Flurries Possible|pcloudys.gif|pcloudysfn.png
BKFL=Occassional Flurries|mcloudys.gif|mcloudysfn.png
OVFL=Cloudy with Flurries|flurries.gif|flurries.gif

FWSNSH=Flurries Possible|pcloudys.gif|pcloudysn.gif
SCSNSH=Flurries Possible|pcloudys.gif|pcloudysn.gif
BKSNSH=Occassional Flurries|mcloudys.gif|mcloudysn.gif


N/A=N/A|na.gif|na.gif

[WxIntensities]
VL=Very light
L=light
H=Heavy
VH=Very Heavy



*/

?>