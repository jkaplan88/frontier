<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/CPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/CPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/CPAC/WV/20.jpg


[TropicalZones]
total_zones=15

1=PHZ110
2=PHZ111
3=PHZ112
4=PHZ113
5=PHZ114
6=PHZ115
7=PHZ116
8=PHZ117
9=PHZ118
10=PHZ119
11=PHZ120
12=PHZ121
13=PHZ122
14=PHZ123
15=PHZ124


[DisplayCities]
total_cities=5

1=lihue,hi
2=honolulu,hi
3=kahulu,hi
4=hilo,hi
5=kona,hi

*/
?>