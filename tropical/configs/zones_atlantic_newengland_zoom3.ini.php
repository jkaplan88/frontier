<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/WV/20.jpg

[TropicalZones]
total_zones=21

1=ANZ050
2=ANZ150
3=ANZ250
4=ANZ254
5=ANZ255
6=ANZ270
7=ANZ350
8=ANZ353
9=ANZ355
10=ANZ370
11=ANZ450
12=ANZ451
13=ANZ452
14=ANZ453
15=ANZ454
16=ANZ455
17=ANZ470
18=ANZ650
19=ANZ652
20=ANZ654
21=ANZ656

[DisplayCities]
total_cities=0


%%LOAD_CONFIG=marinezones%%

*/
?>