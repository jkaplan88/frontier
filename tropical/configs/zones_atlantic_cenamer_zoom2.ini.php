<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/WV/20.jpg

[TropicalZones]
total_zones=36

1=AMZ350
2=AMZ352
3=AMZ354
4=AMZ450
5=AMZ452
6=AMZ454
7=AMZ550
8=AMZ555
9=AMZ650
10=AMZ651
11=GMZ052
12=GMZ053
13=GMZ054
14=GMZ031
15=GMZ032
16=GMZ075
17=GMZ075
18=GMZ150
19=GMZ155
20=GMZ250
21=GMZ255
22=GMZ350
23=GMZ355
24=GMZ450
25=GMZ455
26=GMZ550
27=GMZ555
28=GMZ650
29=GMZ655
30=GMZ656
31=GMZ657
32=GMZ750
33=GMZ755
34=GMZ850
35=GMZ853
36=GMZ856

[DisplayCities]
total_cities=23

1=houston,tx
2=brownsville,tx
3=marfa,tx
4=MMCU
5=MMMY
6=MMLP
7=MMDO
8=MMTM
9=MMMX
10=MMAA
11=MMCP
12=MMMT
13=MZBZ
14=MGGT
15=MHLM
16=MSSS
17=MNPC
18=MRLM
19=MPTO
20=miami,fl
21=new+orleans,la
22=MKJP
23=MUHA


%%LOAD_CONFIG=marinezones%%

*/
?>