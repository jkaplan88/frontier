<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/EPAC/WV/20.jpg


[TropicalZones]
total_zones=2

1=PZZ655
2=PZZ750


[DisplayCities]
total_cities=12

1=los+angeles,ca
2=san+diego,ca
3=blythe,ca
4=san+bernardino,ca
5=santa+clara,ca
6=las+vegas,nv
7=yuma,az
8=phoenix,az
9=tucson,az
10=prescott,az
11=MMTJ
12=MMHO

*/
?>