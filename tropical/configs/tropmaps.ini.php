<?php
/*

[Defaults]
gd_ext=png
template_ext=hwi3
default_ttf=arial
default_bold_ttf=arialbd
default_repeat=0
default_maxdays=0


[Paths]
hw3image_cache=%%INI:Paths:html_side%%/images/hw3image
hw3image_cache_url=%%INI:Paths:html_side_url%%/images/hw3image

hw3image_save=%%INI:Paths:html_side%%/images/hwi3_mapset/generated
hw3image_save_url=%%INI:Paths:html_side_url%%/hwi3_mapset/generated


hw3image_fcicons=%%INI:Paths:html_side%%/images/hwi_fcicons
hw3image_mapicons=%%INI:Paths:html_side%%/images/hwi3_mapset/map_icons
hw3image_backgmaps=%%INI:Paths:html_side%%/images/hwi3_mapset
hw3image_tropical=%%INI:Paths:html_side%%/images/hwi3_mapset/tropical
hw3image_tropsymbols=%%INI:Paths:html_side%%/images/hwi3_mapset/graphics/tropsymbols


[HWV Presets]
hwvplot_cities=1
show_satimage=1
hwvusettf=1
hwvcitycolor=yellow

#map dimensions
hwvtropmapwidth=625
hwvtropmapheight=460

#storm category colors
hwvstmcolor_dep=FFFF00
hwvstmcolor_tropstm=FFCC00
hwvstmcolor_cat1hurr=FF6633
hwvstmcolor_cat2hurr=FF0000
hwvstmcolor_cat3hurr=FF0099
hwvstmcolor_cat4hurr=FF00FF
hwvstmcolor_cat5hurr=FFCCFF

#storm wind field colors
hwvwindfld_tropstm=FFCC00
hwvwindfld_hurr=FF0000

#storm advisory colors
hwvadv_tropstm_watch=FFCC00
hwvadv_tropstm_warn=FF6600
hwvadv_hurr_watch=FF3300
hwvadv_hurr_warn=CC3300
hwvadv_smcraft=99FF00
hwvadv_gale=FFFF00
hwvadv_storm=FF00FC
hwvadv_spcmarine=00FFFC

#map titlebar settings
hwvadd_title=1
hwvadd_legend=1
hwvtitle_height=30
hwvtitle_color=4F4B89
hwvtitle_blend=80
hwvlegend_blend=80
hwvtitle_font_pt=12
hwvtitle_font_color=FFFFFF
hwvbaseline_color=FFFFFF
hwvtitle_right_string=Frontier Weather, Inc.
hwvsitename_font_pt=8
hwvlegend_font_pt=8


[CleanCacheDirs]
clean_cache=%%INI:CleanCacheDirs:clean_cache%%,%%INI:Paths:hw3image_cache%%
*/
?>