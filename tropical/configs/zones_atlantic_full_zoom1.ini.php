<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWATL/WV/20.jpg

[TropicalZones]
total_zones=0


[DisplayCities]
total_cities=27

1=houston,tx
2=mobile,al
3=miami,fl
4=savannah,ga
5=wilmington,nc
6=norfolk,va
7=new+york,ny
8=boston,ma
9=bangor,me
10=dallas,tx
11=nashville,tn
12=buffalo,ny
13=chicago,il
14=st+louis,mo
15=TKPN
16=MTPP
17=MUHA
18=brownsville,tx
19=MMTM
20=MMMX
21=MMCP
22=MHLM
23=SVCS
24=SOCA
25=SBBE
26=GOOY
27=GVAC

*/
?>