<?php
/*
%%LOAD_CONFIG=zone%%

[URLs]
shortterm_url=weather.noaa.gov|iwin.nws.noaa.gov
shortterm_prefix=/pub/data/forecasts/nowcast/%%find_state%%/%%find_state%%z%%find_place%%.txt|/iwin/%%find_state%%/%%iwin_file%%
shortterm_postfix=
shortterm_cache_file=%%cache_path%%/%%country%%-%%find_state%%-%%find_place%%-%%iwin_file%%|%%cache_path%%/%%country%%-%%find_state%%-%%iwin_file%%



[ForecastTypes]
#zandh=zone.html,H10:H16:H22:H3:60,zandh.html,FetchZandH,zandh,1
shortterm=shortterm.html,120,shortterm.html,FetchShortTerm,shortterm,1
now=shortterm.html,120,shortterm.html,FetchShortTerm,shortterm,1
*/
?>