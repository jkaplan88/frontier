<?php
/*

[URLs]

marine_url=weather.noaa.gov
marine_prefix=/pub/data/forecasts/marine/coastal/%%marine_cat%%/%%marine_zone%%
marine_postfix=.txt

marine_cache_file=%%cache_path%%/marine_%%marine_zone%%.txt


[Defaults]
maxdays=

[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
tropadv=tropadv.html,H10:H16:H22:H3:30,tropadv.html,FetchTropAdv,marine_zone,1


*/
?>