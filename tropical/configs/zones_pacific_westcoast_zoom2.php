<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/WV/20.jpg


[TropicalZones]
total_zones=22

1=PZZ110
2=PZZ150
3=PZZ153
4=PZZ156
5=PZZ210
6=PZZ250
7=PZZ255
8=PZZ350
9=PZZ353
10=PZZ356
11=PZZ450
12=PZZ455
13=PZZ530
14=PZZ550
15=PZZ555
16=PZZ650
17=PZZ655
18=PZZ670
19=PZZ673
20=PZZ750
21=PKZ041
22=PHZ122


[DisplayCities]
total_cities=16

1=seattle,wa
2=portland,or
3=medford,or
4=eureka,ca
5=spokane,wa
6=reno,nv
7=san+francisco,ca
8=fresno,ca
9=monterey,ca
10=los+angeles,ca
11=san+diego,ca
12=CYPR
13=CYVR
14=CYZT
15=CYXS
16=CYKA

*/
?>