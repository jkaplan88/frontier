<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/WV/20.jpg

[TropicalZones]
total_zones=36

1=AMZ152
2=AMZ154
3=AMZ156
4=AMZ158
5=AMZ250
6=AMZ252
7=AMZ254
8=AMZ256
9=AMZ270
10=AMZ350
11=AMZ352
12=AMZ354
13=AMZ450
14=AMZ452
15=AMZ454
16=AMZ550
17=AMZ555
18=AMZ650
19=AMZ651
20=GMZ031
21=GMZ032
22=GMZ052
23=GMZ053
24=GMZ054
25=GMZ075
26=GMZ550
27=GMZ555
28=GMZ650
29=GMZ655
30=GMZ656
31=GMZ657
32=GMZ750
33=GMZ755
34=GMZ850
35=GMZ853
36=GMZ856


[DisplayCities]
total_cities=30

1=new+orleans,la
2=jackson,ms
3=meridian,ms
4=hattiesburg,ms
5=mobile,al
6=birmingham,al
7=montgomery,al
8=dothan,al
9=pensacola,fl
10=panama+city,fl
11=tallahassee,fl
12=albany,ga
13=columbus,ga
14=atlanta,ga
15=augusta,ga
16=macon,ga
17=spartanburg,sc
18=columbia,sc
19=savannah,ga
20=gainesville,fl
21=daytona+beach,fl
22=melbourne,fl
23=orlando,fl
24=sarasota,fl
25=west+palm+beach,fl
26=miami,fl
27=key+largo,fl
28=key+west,fl
29=naples,fl
30=fort+meyers,fl

%%LOAD_CONFIG=marinezones%%

*/
?>