<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NWPAC/WV/20.jpg


[TropicalZones]
total_zones=28

1=PKZ041
2=PKZ042
3=PKZ043
4=PKZ051
5=PKZ052
6=PKZ120
7=PKZ125
8=PKZ130
9=PKZ132
10=PKZ138
11=PKZ140
12=PKZ150
13=PKZ155
14=PKZ160
15=PKZ165
16=PKZ170
17=PKZ172
18=PKZ175
19=PKZ176
20=PKZ179
21=PKZ180
22=PKZ185
23=PKZ200
24=PKZ205
25=PKZ210
26=PKZ215
27=PKZ290
28=PKZ295


[DisplayCities]
total_cities=8

1=juneau,ak
2=anchorage,ak
3=dillingham,ak
4=barrow,ak
5=nome,ak
6=point+hope,ak
7=prudhoe+bay,ak
8=fairbanks,ak

*/
?>