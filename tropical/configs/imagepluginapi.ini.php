<?php
/*


[SystemSettings]
# Remove the pound sign below to stop web users from accessing this plug-in
#no_web_access=1

#remove the pound sign to make the plug-in redirect to the created image
# instead of outputting a page
redirect_to_image=1



[Paths]
# this is the main TTF_path which will be duplicated in the other sections
TTF_path=/home/8/4/2/12504/12504/public_html/cgi-bin/

legends_path=%%INI:Paths:html_side%%/images/legends

watermark_path=%%INI:Paths:html_side%%/images/watermarks



[Image Settings]
#this is the main section used by the contour plug-in

#set nocache=1 to force recreation of the image on each request
# use caution setting to 1 as it will increase load
#nocache=1

#image type we are using
gdext=png

#blank map path
blank_map_path=%%INI:Paths:html_side%%/images/blankmaps


#default map to use if not specified
usemap=us_albers_640x480

# with contours you pretty much always need an overlay
overlay=1


# save map directory
save_map_path=%%INI:Paths:html_side%%/images/hw3image

# the url to the saved contour maps
save_map_url=%%INI:Paths:html_side_url%%/images/hw3image

# the gd extention to use for the contour maps
# uncomment to use, otherwise will default to the default
# gdext.  Setting this allows the blankmaps to be one type &
# the contour maps saved as a different type
#save_map_gdext=png

#name to save as
#save_map_name=

#set allow_save_name=1 to allow passin sn=xxx in the parameters to set the name
allow_save_name=0


# belend level to add the contour filles/lines to the backgroud map
# must be 0 to 100
blend_level=100


truecolor=0



[Legend Settings]
add_legend=0
add_method=draw

direction=horizontal
legend_x=center
legend_y=35

legend_size=450

block_outline_color=000000
TTF_path=%%INI:Paths:TTF_path%%


block_offset=10
block_width=30
block_height=6

no_gradient=1

add_labels=label

label_when=(1)
label_what=

label_xoffset=0
label_yoffset=0
label_TTF_font=AppleGaramond.ttf
label_font_color=FFFFFF
label_TTF_font_pt=8
label_gdfont=medium
label_dropshadow_xoffset=2
label_dropshadow_yoffset=2
label_dropshadow_color=000000

legend_order=



[Title Settings]
add_title=0
TTF_path=%%INI:Paths:TTF_path%%

position=top
mode=0
height=30
bg_color=4F4B89
bg_blend_level=80
baseline_color=FFFFFF

title_text=
title_TTF_font=AppleGaramond.ttf
title_TTF_font_pt=15
title_gdfont=LARGE
title_font_color=FFFFFF
title_text_x=left
title_text_y=21



credit_text=FrontierWeather, Inc.
credit_TTF_font_pt=14
credit_TTF_font=AppleGaramond.ttf
credit_gdfont=LARGE
credit_font_color=FFFFFF
credit_text_x=right
credit_text_y=21



[Label Settings]
add_labels=1

TTF_path=%%INI:Paths:TTF_path%%
TTF_font=AppleGaramond.ttf
TTF_font_pt=10
TTF_font_angle=0
gdfont=LARGE
font_color=FFFFFF
dropshadow_xoffset=2
dropshadow_yoffset=2
dropshadow_color=000000

bottom_offset=10
left_offset=10
top_offset=10
right_offset=10

[Labels]


[Watermark Settings]
add_watermarks=0
watermark_path=%%INI:Paths:watermark_path%%

[Watermarks]
hw=hw.png|right|bottom|||30




[PlotCities Settings]
plotcities=0


TTF_path=%%INI:Paths:TTF_path%%
TTF_font=AppleGaramond.ttf
TTF_font_pt=10
TTF_font_angle=0
gdfont=MEDIUM
font_color=FFFF00
dropshadow_xoffset=2
dropshadow_yoffset=2
dropshadow_color=000000

default_halign=center
default_valign=center


[PlotCities]
#name=pands|DisplayName|x|y|halign|valign
# if x and Y are not set then HW will query the db to
# determine the x/y location
# if DisplayName is not set, then HW3 will use the
# name as found from the Database


*/
?>