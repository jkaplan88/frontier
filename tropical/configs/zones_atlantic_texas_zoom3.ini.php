<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/WV/20.jpg

[TropicalZones]
total_zones=10

1=GMZ555
2=GMZ650
3=GMZ655
4=GMZ656
5=GMZ657
6=GMZ750
7=GMZ755
8=GMZ850
9=GMZ853
10=GMZ856

[DisplayCities]
total_cities=0

%%LOAD_CONFIG=marinezones%%

*/
?>