<?php
/*

[TAF Phrases]
MAIN=%%wx%%

BECMG_SKY=Becoming %%wx%%
BECMG_WX=Beginning to %%wx%%
BECMG_WXS=Beginning to have %%wx%%

FROM_SKY=Turning %%wx%%
FROM_WX=Starting to %%wx%%
FROM_WXS=Starting to have %%wx%%

PROB_WX=A %%prob%% percent chance to %%wx%%
PROB_WXS=A %%prob%% percent chance of %%wx%%

WIND=With winds from the %%WIND_DIR_FULLNAME%% at %%dec0_WIND_MPH%% mph
WIND_VARIABLE=Winds variable in direction at %%dec0_WIND_MPH%% mph
WIND_CALM=Winds calm

;GUST=and gusts up to %%dec0_WIND_MPH_GUST%% mph
GUST=y r�fagas de hasta %%dec0_WIND_MPH_GUST%% mph

THEN=Then

WIND_S=south
WIND_W=west
WIND_E=east
WIND_N=north
WIND_VRB=variable


[TAF WxDescript]
NSW=
MI=SHALLOW
BC=PATCHES
BL= BLOWING
TS=THUNDERSTORM
PR=PARTIAL
DR=LOW DRIFTING
SH=SHOWERS
FZ=FREEZING

RA=RAIN
SN=SNOW
IC=ICE CRYSTALS
GR=HAIL
UP=KNOWN PRECIP
DZ=DRIZZLE
SG=SNOW GRAINS
PE=ICE PELLETS
GS=SMALL HAIL

FG=FOG
BR=MIST
DU=WIDESPREAD DUST
SA=SAND
VA=VOLCANIC ASH
HZ=HAZE
FU=SMOKE
PY=SPRAY

SQ=SQUALL
DS=DUSTSTORM
FC=FUNNEL CLOUD
PO=DUST SWIRLS
SS=SANDSTORM


[TAF SkyCond]
CLR=clear
SKC=clear
FEW=partlya cloudya
SCT=partlyb cloudyb
BKN=mostlyc cloudyc
OVC=overcast
NSC=clear
CAVOK=fair


[TAF CloudTypes]
AC=Altocumulus
AS=Alstostratus
CB=Cumuloninumbus
CC=Cirrocumulus
CI=Cirrus
CS=Cirrostratus
CU=Cumulus
FC=Fractocumulus
FS=Fractostratus
NS=Nimbostratus
SC=Stratocumulus
ST=Stratus
TCU=Towering Cumulus


*/
?>