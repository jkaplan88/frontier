<?php
/*
[ForecastTypes]
# type = IWIN/URL, max age, template,clean code, template code
plotsystemtrack=,15,tropmap.html,MakeTropMap,main,0


%%LOAD_CONFIG=tropical_plot_common%%

[Image Settings]
plot_tracks=1
color_section=tropsystem Colors

track_markers=<? ('%%ptm%%') ? 1 : 0?>

[Title Settings]

title_text=<? ('%%INI:Title Settings:title_small%%') ? ( (strtolower('%%eventnum%%') == 'all') ? '%%year%% Season Storm Tracks' : ((strtolower('%%eventnum%%') == 'active') ? 'Active Storms'  :  $this->storm_name ) ) : ( (strtolower('%%eventnum%%') == 'all')  ? '%%year%% Season Storm Tracks' : ((strtolower('%%eventnum%%') == 'active') ? 'Active Storm Tracks'  :  $this->storm_name . ' Storm Track (' .  ( ('%%year%%') ? '%%year%%' : gmdate('Y') ) . ')' ) ) ?>

%%LOAD_CONFIG=troplegend%%

*/
?>
