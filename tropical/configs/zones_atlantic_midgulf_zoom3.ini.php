<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/GMEX/WV/20.jpg

[TropicalZones]
total_zones=23

1=GMZ054
2=GMZ032
3=GMZ075
4=GMZ075
5=GMZ150
6=GMZ155
7=GMZ250
8=GMZ255
9=GMZ350
10=GMZ355
11=GMZ450
12=GMZ455
13=GMZ550
14=GMZ555
15=GMZ650
16=GMZ655
17=GMZ656
18=GMZ657
19=GMZ750
20=GMZ755
21=GMZ850
22=GMZ853
23=GMZ856

[DisplayCities]
total_cities=22

1=brownsville,tx
2=corpus+christi,tx
3=austin,tx
4=houston,tx
5=lake+charles,la
6=shreveport,la
7=alexandria,la
8=new+orleans,la
9=baton+rouge,la
10=houma,la
11=jackson,ms
12=meridian,ms
13=hattiesburg,ms
14=mobile,al
15=birmingham,al
16=montgomery,al
17=dothan,al
18=pensacola,fl
19=panama+city,fl
20=tallahassee,fl
21=albany,ga
22=columbus,ga

%%LOAD_CONFIG=marinezones%%

*/
?>