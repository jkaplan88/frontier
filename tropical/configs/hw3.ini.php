<?php
/*

[Paths]

; url to the hw3.php
scripturl=hw3.php

; html_side_url is the url to the directory containing the
; hamweather "images" directory
html_side_url=.

; html_side is the file path tot he directory containing
; the "images" directory
html_side=.


[SystemSettings]
allow_cookies=true
cookie_name=HW3

; debug_mode:
; 0 = no debug mode,
; 1 = debug mode based on passed "debug var"
; 2 = debug always on
; 3 = debug mode based on passed debug var & debugpw var
debug_mode=1
; debug_pw used when debug_mode=3 set to password to turn debug mode on
debug_pw=

;Daylight Savings Time Information
; when DST is set to 0 no DST processing is done
; but when set to 1 then HW will perform proper
; DST processing on the time zones listed in
; TZnames_with_DST
; if use_system_DST=1 then use server settings.
use_system_DST=1
DST=1
TZnames_with_DST=EST,CST,PST,AST, AHST,MST
DST_not_used_in_states=az,hi

logging=1
# max age in minutes (1440 minutes= 1 day)
log_file_max_age=43200

[Defaults]
country=us
state=GA
place=Atlanta
pass=
forecast=zandh
bad_place_template=badplace.html

allow_multiple_places=1
pass_template_if_multiple=multiple_places

default_hsky=fair
; used for displaying Icons..
; HW3 uses these variable as local time to the location
; thus 7 is 7am at the location requested
day begin=7
day end=19

maxdays=6

template_ext=html

[PassTemplates]
multiple_places=multiple_places.html

[HWI Sections]
; HWI sections is used to defined extra directories that HWI include
; files can be loaded from
; in the IMAP setting below, HW3 willk auto convert %%country%% to
; the proper country directory
IMAP=%%base%%/hw3dbs/%%country%%/imap

[Parse Plugins]
; This Section is where Parser Plugins are defined.
; The use the following structure:

; PluginCallName=Full::PluginName|PluginRoutine|Preparse|PostParse|AutoLoad
; where:
; PluginCallName= the name that will be used in the templates to load the plugin..
; Full::PluginName= need I say more?
; PluginRoutine = the routine in the plugin that the parse shoudl call
; Preparse = If set to 1 the Template parser will call this routine before
;                  replaceing template variables
; PostParse = If set to 1 the Template Parser will call this routine after
;                 replaceing template variables
; AutoLoad = Should this parser plugins be auto loaded upon every request to
;                HW3?  Uses more resources!

HWMatchState=HW3Plugins::MatchState|parse_line|0|1|0
HWSSI=HW3Plugins::ssi|parse_line|0|1|0
HWConvert=HW3Plugins::Convert|parse_line|0|1|0

[referrer]
referrer_check=0
referrer_file=referrer.txt
referrer_url=http://www.frontierweather.com


[Database Access]
debug_no_show=1
type=DBTYPE
database_type=DBSQLTYPE
database_name=DBNAME
hostname=DBHOSTNAME
username=DBUSERNAME
password=DBPASSWORD


allow_fuzzy_search=1
fuzzy_min_chars=5


keepopen=1
open_at_init=1
use_persistent=0

*/
?>