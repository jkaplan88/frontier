<?php
/*
%%LOAD_CONFIG=wxinfo%%


[URLs]
metar_url=www.nws.noaa.gov|adds.aviationweather.noaa.gov|weather.noaa.gov|weather.noaa.gov
metar_prefix=/data/current_obs/%%find_place%%|/metars/index.php?std_trans=translated&chk_metars=on&hoursStr=most+recent+only&station_ids=%%find_place%%|/pub/data/observations/metar/decoded/%%find_place%%|/weather/current/%%find_place%%
metar_postfix=.xml||.TXT|.html

metar_url_int=weather.noaa.gov|adds.aviationweather.noaa.gov|weather.noaa.gov
metar_prefix_int=/pub/data/observations/metar/decoded/%%find_place%%|/metars/index.php?std_trans=translated&chk_metars=on&hoursStr=most+recent+only&station_ids=%%find_place%%|/weather/current/%%find_place%%
metar_postfix_int=.TXT||.html

pws_url=pws.hamweather.net
pws_prefix=/weather/current/%%find_place%%
pws_postfix=.html

[ForecastTypes]
metar=file.html,15,metar.html,FetchZandH,hourly,1



[NOAAIconWx]
BKN=MOSTLY CLOUDY
FG=FOG
SKC=FAIR
FEW=FEW CLOUDS
SCT=PARTLY CLOUDY
OVC=OVERCAST
NFG=FOG/MIST
SMOKE=SMOKE
FZRA=FREEZING RAIN
IP=ICE PELLETS
MIX=FREEZING RAIN SNOW
RAIP=RAIN ICE PELLETS
RASN=RAIN AND SNOW
SHRA=RAIN SHOWERS
TSRA=THUNDERSTORM
SN=SNOW
WIND=WINDY
HI_SHWRS=SHOWERS
TZRARA=FREEZING RAIN
HI_TSRA=THUNDERSTORM
RA1=LIGHT RAIN
RA=RAIN
NSVRTSRA=FUNNEL CLOUD
DUST=DUST
MIST=HAZE




*/
?>