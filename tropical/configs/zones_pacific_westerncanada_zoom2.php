<?php
/*

[SatelliteImagery]
ir_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/IR4/20.jpg
vis_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/VIS/20.jpg
wv_sat=http://www.ssd.noaa.gov/PS/TROP/DATA/RT/NEPAC/WV/20.jpg


[TropicalZones]
total_zones=21

1=PZZ110
2=PZZ150
3=PZZ153
4=PZZ156
5=PZZ210
6=PZZ250
7=PZZ255
8=PZZ350
9=PZZ353
10=PZZ356
11=PZZ450
12=PZZ455
13=PZZ530
14=PZZ550
15=PZZ555
16=PKZ041
17=PKZ042
18=PKZ043
19=PKZ051
20=PKZ052
21=PKZ120


[DisplayCities]
total_cities=15

1=juneau,ak
2=seattle,wa
3=portland,or
4=medford,or
5=eureka,ca
6=spokane,wa
7=reno,nv
8=san+francisco,ca
9=fresno,ca
10=monterey,ca
11=CYPR
12=CYVR
13=CYZT
14=CYXS
15=CYKA

*/
?>