<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id$
*
*/


class FetchMetarFromDB {

	var $cfg;
	var $debug = false;

	var $data = array();

	var $db_table='';
	var $db_maxage=0;




	Function FetchMetarFromDB($cfg, $debug) {
		$this->cfg =& $cfg;
		$this->debug = $debug;

		$this->dbtype = $this->data['db_type']= $cfg->val('Database Access', 'database_type'); # Database type.
		$this->data['db_username']  = $cfg->val('Database Access', 'username'); # Username.
		$this->data['db_password']  = $cfg->val('Database Access', 'password'); # Password.
		$this->data['db_hostname']  = $cfg->val('Database Access', 'hostname'); # Hostname.
		$this->data['db_database']  = $cfg->val('Database Access', 'database_name'); # Database name.

		if ($cfg->val('HWdata', 'database_name')) $this->data['db_database'] = $cfg->val('HWdata', 'database_name') ;			if ($cfg->val('HWdata', 'hostname')) $this->data['db_hostname'] = $cfg->val('HWdata', 'hostname') ;
		if ($cfg->val('HWdata', 'username')) $this->data['db_username'] = $cfg->val('HWdata', 'username') ;
		if ($cfg->val('HWdata', 'password')) $this->data['db_password'] = $cfg->val('HWdata', 'password') ;


		if ($cfg->val('METAR', 'database_name')) $this->data['db_database'] = $cfg->val('METAR', 'database_name') ;
		if ($cfg->val('METAR', 'hostname')) $this->data['db_hostname'] = $cfg->val('METAR', 'hostname') ;
		if ($cfg->val('METAR', 'username')) $this->data['db_username'] = $cfg->val('METAR', 'username') ;
		if ($cfg->val('METAR', 'password')) $this->data['db_password'] = $cfg->val('METAR', 'password') ;



	    $dbtable = $cfg->val('METAR', 'database_table'); # Database name.
	    $this->db_table = ($dbtable) ? $dbtable : 'hwdata_currents';

	    $ma = $cfg->val('METAR', 'db_maxage')+0;
	    $this->db_maxage = $ma;


	}


	Function db_establish($dbtype,$database, $dbhostname, $dbusername, $dbpassword) {
		$dbh = null;
		if ($dbtype == 'odbc') {
			$dbh = odbc_connect($dbhostname, $dbusername, $dbpassword) or die(odbc_error());
			odbc_select_db($database,$dbh) or die(odbc_error());
		}
		elseif ($dbtype == 'mssql') {
			$dbh = mssql_connect($dbhostname, $dbusername, $dbpassword) or die(mssql_error());
			mssql_select_db($database,$dbh) or die(mssql_error());
		}
		elseif ($dbtype == 'mysql') {
			$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword,0) or die(mysql_error());
			mysql_select_db($database,$dbh) or die(mysql_error());
		}

		return $dbh;
	}



Function fetch_metar($icao) {

	$error = '';
	$isold = 1;
	$debug = $this->debug;


	$data = array();
   	$data['hsky'] = $data['hsky_conditions'] = $data['hweather'] =  $data['htempf'] = $data['hdewptf'] = $data['htempc'] = $data['hdewptc'] =
   	$data['hrh'] = $data['hwind'] = $data['hwinddir'] = $data['hwindspd'] =
     $data['hwcf'] = $data['hhif'] = $data['hwcc'] = $data['hhic'] = $data['hpressure'] =
     $data['hpressure_mb'] = $data['hremarks'] = $data['hvisibility'] =
     $data['hquk'] = $data['hqul'] = $data['metar'] = $data['gusts'] = $data['hwinddir_deg'] = $data['hvisibility_miles'] =
     $data['hvisibility_meters'] = $data['hsixhrhif'] = $data['hsixhrlof'] =
     $data['h24hrhif'] = $data['h24hrlof'] = $data['hsixhrhi_c'] = $data['hsixhrlo_c'] = $data['h24hrhi_c'] =
     $data['h24hrlo_c'] = $data['hthrhrprec'] = $data['hsixhrprec'] = $data['h24hrprec'] ='N/A';


	$icao =preg_replace('/\W/','',$icao);



	if ($icao) {
		$dbh = $this->db_establish($this->['db_type'], $this->['db_database'], $this->['db_hostname'],
		          $this->['db_username'],$this->['db_password']);
		if (!$dbh) {
			return array($error, $isold);
		}

		$sql = 'SELECT Temperature,DewPoint,RelativeHumidity,WindSpeed,WindDirection,WindDirectionEng,WindGust,Pressure,Weather,Clouds,WeatherIcon,Visibility,HeatIndex,WindChill,MaxTempSixHr,MinTempSixHr,MaxTemp24Hr,MinTemp24Hr,PrecipOneHr,PrecipThreeHr,PrecipSixHr,Precip24Hr,ReportEpoch,ReportDate FROM ' . $this->['db_table'] . " WHERE ICAO='$icao'";

		$ma =$self->{db_maxage};
		if ($ma > 0) {
			$now = time();
			$now -= $ma * 60; # get the current time minus the max age converted to seconds
			$sql .= " AND ReportEpoch >= $now";
		}

		if ($debug) print "sql=$sql<br>\n" ;

		if ($this->dbtype == 'mysql') {
			$result = mysql_query($sql, $dbh);
			$row = mysql_fetch_row($result);
		}
		elseif ($this->dbtype == 'odbc') {
			$result = odbc_query($sql, $dbh);
			$row = odbc_fetch_row($result);
		}
		elseif ($this->dbtype == 'msssql') {
			$result = mssql_query($sql,$dbh);
			$row = mssql_fetch_row($result);
		}
		if (sizeof($row) > 0) {
			list ($Temperature, $DewPoint, $RelativeHumidity, $WindSpeed, $WindDirection, $WindDirectionEng, $WindGust, $Pressure, $Weather, $Clouds, $WeatherIcon, $Visibility, $HeatIndex, $WindChill, $MaxTempSixHr, $MinTempSixHr, $MaxTemp24Hr, $MinTemp24Hr, $PrecipOneHr, $PrecipThreeHr, $PrecipSixHr, $Precip24Hr, $ReportEpoch, $ReportDate) = $row;



			my ($min,$h,$d,$m,$y) = (gmtime($ReportEpoch))[1,2,3,4,5];
			$m++; $y+=1900;
			$data{hforecastdate} = sprintf('%04u.%02u.%02u %02u%02u', $y,$m,$d,$h,$min) . ' UTC';









}



Function GetValue($key, $index) {
		return  (isset($this->data[$key])) ? $this->data[$key] : '';
	}
}
?>