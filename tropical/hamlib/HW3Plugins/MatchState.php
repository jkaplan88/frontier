<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: MatchState.php,v 1.4 2003/02/24 12:49:48 lee Exp $
*
*/

class MatchState {

	var $us_state_full = array('al'=>'alabama', 'ak' => 'alaska', 'az' =>'arizona', 'ar'=>'arkansas', 'ca' =>'california', 'co'=>'colorado',
                  'ct'=>'connecticut','de'=>'delaware', 'fl'=> 'florida', 'ga' =>'georgia', 'hi'=>'hawaii', 'id'=>'idaho', 'il'=>'illinois',
                  'in'=>'indiana','ia'=>'iowa', 'ks'=>'kansas', 'ky'=>'kentucky', 'la'=>'louisiana', 'me'=>'maine', 'md'=>'maryland', 'ma'=>'massachusetts',
                  'mi'=>'michigan', 'mn'=>'minnesota', 'ms'=>'mississippi', 'mo'=>'missouri', 'mt'=>'montana', 'ne'=>'nebraska', 'nv'=>'nevada',
                  'nh'=>'new hampshire', 'nj'=>'new jersey', 'nm'=>'new mexico','ny'=>'new york', 'nc'=>'north carolina', 'nd'=>'north dakota',
                  'oh'=>'ohio','ok'=>'oklahoma','or'=>'oregon', 'pa'=>'pennsylvania', 'ri'=>'rhode island', 'sc'=>'south carolina',
                  'sd'=>'south dakota', 'tn'=>'tennessee', 'tx'=>'texas', 'ut'=>'utah', 'vt'=>'vermont', 'va'=>'virginia', 'wa'=>'washington',
                  'wv'=>'west virginia', 'wi'=>'wisconsin', 'wy'=>'wyoming','us'=>'united states', 'pr'=>'puerto rico', 'vi'=>'virgin islands', 'gu'=>'guam' );

         var $ca_province_full = array('ab'=>'alberta', 'bc'=>'british columbia', 'pe'=>'prince edward', 'mb'=>'manitoba', 'nb'=>'new brunswick',
                  'ns'=>'nova scotia', 'on'=>'ontario', 'qc'=>'qu�bec', 'sk'=>'saskatchewan', 'nf'=>'new foundland', 'nt'=>'northwest territories',
                  'nu'=>'nunavut', 'yk'=>'yukon');

	var $core_path = '';
	Function MatchState($core_path) {
		$this->core_path = $core_path;

	}


	Function parse_line(&$line, $post_parse, &$template, $print_now, &$save_file_fh, $pm, &$extra_parse, &$hashes) {
		if ($post_parse) {
			$line = preg_replace('/US_STATE_FULL_(\w+)/e', "\$this->do_us_state('$1');", $line);
			$line = preg_replace('/CA_PROVINCE_FULL_(\w+)/e', "\$this->do_ca_state('$1');", $line);

		}
		return array($line, $pm);
	}

	Function do_us_state($state) {
		$state = strtolower($state);
		if (isset($this->us_state_full[$state])) { return $this->us_state_full[$state];}
		else {return ''; }
	}

	Function do_ca_state($province) {
		$state = strtolower($province);
		if (isset($this->ca_province_full[$province])) { return $this->ca_province_full[$province];}
		else {return ''; }
	}



}
?>