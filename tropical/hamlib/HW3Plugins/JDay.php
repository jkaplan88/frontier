<?php

/*
#############################################
##    jday (original code take from date.pl package
##   modified by Lee huffman of HAMweather, LLC
#
# Usage:  $julian_day = &jday($month,$day,$year)
#############################################
*/
Function jday($m,$d,$y) {

	if ($m > 2) {$m -= 3;}
	   else {	$m += 9; --$y;}
	$c = floor($y/100);
	$ya = $y - (100 * $c);
	$jd =  floor((146097 * $c) / 4) +
		   floor((1461 * $ya) / 4) +
		   floor((153 * $m + 2) / 5) +
		   $d + 1721119;
	return $jd;

}



/*
#############################################
##    jdate (original code take from date.pl package
##   modified by Lee huffman of HAMweather, LLC
#
# Usage:  ($month,$day,$year,$weekday) = &jdate($julian_day)
#############################################
*/
Function jdate ($jd) {



	$wkday = ($jd + 1) % 7;       // calculate weekday (0=Sun,6=Sat)
	$jdate_tmp = $jd - 1721119;
	$y = floor((4 * $jdate_tmp - 1)/146097);
	$jdate_tmp = 4 * $jdate_tmp - 1 - 146097 * $y;
	$d = floor($jdate_tmp/4);
	$jdate_tmp = floor((4 * $d + 3)/1461);
	$d = 4 * $d + 3 - 1461 * $jdate_tmp;
	$d = floor(($d + 4)/4);
	$m = floor((5 * $d - 3)/153);
	$d = 5 * $d - 3 - 153 * $m;
	$d = floor(($d + 5) / 5);
	$y = 100 * $y + $jdate_tmp;
	if($m < 10) {$m += 3;}
	   else {	$m -= 9; ++$y;}

	return array($m, $d, $y, $wkday);



}

function weekDayString($weekday) {
$myArray = Array( 0 => "Sun",
1 => "Mon",
2 => "Tue",
3 => "Wed",
4 => "Thu",
5 => "Fri",
6 => "Sat");
return $myArray[$weekday];
}

?>