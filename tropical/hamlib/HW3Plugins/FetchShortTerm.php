<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchShortTerm.php,v 1.15 2005/08/15 02:29:21 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");

class FetchShortterm {
	var $cfg;

	var $debug = false;

	var $tzname;
	var $tzdif;

	var $state;
	var $found;

	var $core_path ='';

	Function FetchShortterm($core_path) {
		$this->core_path = $core_path;
		$this->tzdif=0;
		$this->tzname='';
	}


	Function Debug ($mode) {
		$this->debug = $mode;
	}


	Function shortterm (&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {

		$this_cfg = $cfg;

		if ($var["tzname"] && $var["tzdif"]) {
      			$this->tzname = $var["tzname"];
      			$this->tzdif = $var["tzdif"];
   		}

		$error = "";


		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;

		//Grab place info from var hash
		$country = $var["country"];
		$state = $var["state"];
		$place = $var["place"];
		$alt_zone_info = $var["alt_zone_info"];

		if (!$country) {$country = 'us';}

		$this->state = $state;

//		$cache_path = $cfg->val('Paths', 'cache');
//		if (!$cache_path) { $cache_path = 'cache'; }
//		elseif(!preg_match('/\/|\w:/', $cache_path)) { $cache_path = 'cache'; }
		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;

		$found = 0;

		$today = getdate();
		$h = $today['hours'];
		$m = $today['minutes'];
		$mon = $today['mon'];
		$d = $today['mday'];
		$y = $today['year'];
		$gmtime = $y . sprintf('%02u%02u%02u%02u', $mon,$d,$h,$m);

		if ($debug) print "alt_zone_info=$alt_zone_info<br>\n";
		$zone_areas = explode("%", $alt_zone_info);
		while(list($key, $zone_area) = each($zone_areas)) {
			$temp_array = explode(":", $zone_area);
			$alt_info = $temp_array[0];
			$code = $temp_array[1];

			$find_place = "";
			$find_zone = "";
			$find_state = "";

			//We are looking for a us zone forecast
			if ($code < 3) {
				if ($code < 2) {
              				// means alt_info is a place name
               				$find_place = $alt_info;
            			}
				else{
               				//means alt_info is a zone number
					if ($debug) print "alt_info=$alt_info<br>\n";
					if (preg_match('/^(?:([a-zA-Z][a-zA-Z])[Zz]?)?(\d+)$/', $alt_info, $matches)) {
						$find_state = strtolower($matches[1]);
						$find_zone = strtolower($matches[2]);
					}
            			}

				if (!$find_state) {
					$find_state = strtolower($state);
				}
            			$use_find_state = ($find_state == 'dc') ? 'va' : $find_state;

				$find_place = $find_zone ? sprintf('%03u', $find_zone) : $find_place;
				$find_place = preg_replace('/\W/', "", $find_place);

				$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'shortterm_cache_file'));
				if (!$cache_file) {$cache_file = $cache_path . strtolower("/$country-$find_state-$iwin_file"); }

				$cache_file2 = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'shortterm_cache_file2'));
				if (!$cache_file2) {$cache_file2 = $cache_path . strtolower("/$country-$find_state-$find_place-$iwin_file");}

				$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'shortterm_prefix'));


				//Call Fetch Forcast
				$data  = &fetch_forecast($cfg,$ma, $cache_file, $cache_file2,'',
					$cfg->val('URLs', 'shortterm_url'), $prefix,
					$cfg->val('URLs', 'shortterm_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
			      $which_used = $data[0];
			      $wx_data = $data[1];

				if (preg_match('/Expires:\s*?(\d\d\d\d\d\d\d\d\d\d\d\d)/', $wx_data, $matches)) {
					if ($matches[1] < $gmtime) {
						$wx_data = '';
						if ($debug) print "Data expired at ".$matches[1].".<br>\n";
					}
				}

				if ($wx_data == '') {
					if ($debug) print "no data found.<br>\n";
					return $t;
				}

				require_once("hamlib/HW3Plugins/Wx/Zone/USA.php");
				$this->zone = new zone($debug);
				$found_zone = $this->zone->find_zone($find_place, $find_zone, $find_state, $wx_data);

				if ($found_zone) {
					$this->state = $find_state;
					//if ($which_used != 2 && $cache_file ne $cache_file2) {
                 			//	&save_file($cache_file2, "\n\n" .$self->{local_}->header. "\n\n" . $self->{local_}->forecast_text . "\n\n\$\$\n\n");
               				//}


               				//$zone->process_zone($var["daysonly"], "");
               				$wxinfo =& $cfg->Parameters('WxInfo');

               				$this->zone->forecast_text = preg_replace('/^\s*(\bNOW\.+)\s+(\S)/', '$1$2', $this->zone->forecast_text);
               				$this->zone->process_zone($wxinfo, 0, $temp, $wxinfo,1);

               				//$this->zone->process_zone('', $temp, $wxinfo, '',1);
               				$found = 1;
             				break;
				}
				else {
               				//&save_file($cache_file2, "\n\nNOT AVBL\n\n\$\$\n\n");
					//$t = $self->{cfg_}->val('Defaults', 'noavail template');
            			}
			}



		}

		$this->found = $found;
		return $t;

	}


	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = '';

		if (substr($key,0,10) == 'shortterm_') {
//print "key=$key<br>\n";
			$key = substr($key,10);
//print "key=$key<br>\n";
			if ($index <> "") {
				$tmp = $this->$key;
				if (isset($tmp[$index])) $val = $tmp[$index];
			}
			else {
				$val = $this->$key;
			}

			if ($val == '' && $this->zone != "") {
				if ($index <> "") {
					$tmp = $this->zone->$key;
					if (isset($tmp[$index])) $val = $tmp[$index];
				}
				else {
					$val = $this->zone->$key;
				}
			}
		}


		error_reporting($old_error_reporting);

//print "val=$val<br>\n";
		return $val;
	}




















}


?>