<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchWxData.php,v 1.27 2006/05/07 17:55:14 wxfyhw Exp $
*
*/

if (defined('HTTP_MODE') && HTTP_MODE > 0) {
	require_once("hamlib/HW3Plugins/CurlHTTPClient.php");
}
else {
	require_once("hamlib/HW3Plugins/SimpleHTTPClient.php");
}

Function mygetKeyVal (&$hash, $key, $default) {
   if (isset($hash[$key])) { return $hash[$key]; }
      else {return $default; }
}

Function fetch_forecast(&$cfg, $max_age, $cache_files_1, $cache_files_2, $try_url, $try_domain,  $try_prefix, $try_postfix, $port, $debug, $binmode=0, $http_mode=0) {
	if ($debug) print "In fetch_forecast: max_age = $max_age<br>\n";
	$wx_info = "";
	$which_used = 1;
	if ($debug) {
		$turl = preg_replace('/wx\w+/', '', $try_prefix);

		echo "prefix=$turl<br>\n";
	}
	//Create Arrays from the urls, prefixes, postfixes
	$url_arry = explode("|", $try_domain);
	$domain_arry = explode('|', $try_domain);
	$prefix_arry = explode("|", $try_prefix);
	$postfix_arry = explode("|", $try_postfix);
	$cache_1_arry = explode("|", $cache_files_1);
	$cache_2_arry = explode("|", $cache_files_2);

	//Loop thru the arrays and try to fetch from the URL
	$continue_processing = true;
	for ($i = 0; $i < sizeof($url_arry); $i++) {
		if ($continue_processing) {
			if ($debug) print "i=$i<br>\n";
			$domain = mygetKeyVal($url_arry, $i, '');
			$prefix = mygetKeyVal($prefix_arry, $i, '');
			$postfix = mygetKeyVal($postfix_arry, $i, '');
			$cache_file_1 = mygetKeyVal($cache_1_arry, $i, $cache_files_1);
			$cache_file_2 = mygetKeyVal($cache_2_arry, $i, '');


			//Try a fetch data on this url
			$data = &fetch_data($cfg, $max_age, $cache_file_1, $cache_file_2, $domain, $prefix.$try_url.$postfix, $port,$debug,$binmode);
	                  $which_used = $data[0]; // set to either 1 or 2 based on file returned
	                  $wx_info = &$data[1]; // set wx_info to reference of data
	                  $error = $data[2]; // get error if set

			//If wx data was found then set the process flag to false and stop looping
			if (!$error && $wx_info != '') {
				$continue_processing = false;
			}

		}
	}

	return array ($which_used, $wx_info, $cache_file_1, $cache_file_2);
}

Function &fetch_data(&$cfg, $max_age, $cache_file_1, $cache_file_2, $domain, $url, $port, $debug,$binmode) {
	$old_error_reporting = error_reporting(0);

	$cache_mode = $cfg->val('SystemSettings', 'cache_mode'); // not currently used
	$http_mode = $cfg->val('SystemSettings', 'cache_mode');

	$return_string = '';
	$which_used = 1;
	if (!$permissions) $permissions = 0666;

	if ($debug) {
		$turl = preg_replace('/wx\w+/', '', "$domain$url");
		echo "URL: $turl<br>\n";
	}

	//Use cache 1 if it hasnt expired yet
	if ($debug) {print "Checking to see if cache_file_1('$cache_file_1') exists and age less than $max_age<br>\n"; }
	if (!check_cache_1($max_age, $cache_file_1, $debug)) {
		if ($debug) {
			print "...Cache File 1 exists and not expired.<br>\n";
			print "......Checking to see if cache_file_2('$cache_file_2') exists and age less than cache file 1<br>\n";
		}

		//Use cache file 2 if it's newer
		if (compare_caches($cache_file_1, $cache_file_2)) {
			if ($debug) { print "Loading Data from File 2 : $cache_file_2<br>\n"; }
			$bflag = ($binmode) ? 'b' : '';
			if ($file = fopen($cache_file_2, 'r', $bflag)) {
				$return_string = fread($file, filesize($cache_file_2));
				fclose($file);
				$which_used = 2;
			}
		}
		//Otherwise read from 1
		else {
			if ($debug) { print "Loading Data from File 1 : $cache_file_1<br>\n"; }
			$bflag = ($binmode) ? 'b' : '';
			if ($file = fopen($cache_file_1, 'r', $bflag)) {
				$return_string = fread($file, filesize($cache_file_1));
				fclose($file);
			}
		}
	}
	else{
		//Try to access the given url
		if ($debug) {
		   print "... Cache File 1 doesnt exist or is expired<br>\n";
		   $turl = preg_replace('/wx\w+/', '', $url);
		   print "Fetching data from url $turl<br>\n"; }

		if ($http_mode != 1) {
			$http_timeout = $cfg->val('SystemSettings', 'http_timeout');
			if (!$http_timeout) $http_timeout = 10;
			$return_string =& HTTPGet($url, $domain, $port, '', $cfg->val('SystemSettings', 'proxy_server'),
							    $cfg->val('SystemSettings', 'proxy_user'),
                  					    $cfg->val('SystemSettings', 'proxy_pw'), $debug, $http_timeout);
		}
		else {
			if ($file = fopen($url, "r")) {
				$return_string='';
				while(!feof($file))
				{
					$return_string .= fread($file, 1024);
				}
				fclose($file);
			}
		}




		//If something was returned, write it to cache 1
		if ($return_string != '' && !preg_match('/Page Not Found|-hwERR|404.+Not Found|[^\*]error\s+?\d+\b/is',$return_string)) {
		     if ($debug) { print "<b>...Success</b><br>\n"; }
			if (file_exists($cache_file_1)) unlink($cache_file_1);
			$bflag = ($binmode) ? 'b' : '';
			if ($file = @fopen($cache_file_1, 'wb', $bflag)) {
				fputs($file, $return_string);
				fclose($file);
				@chmod($cache_file_1, $permissions);
			}
		}
		else {
		   if ($debug) { print "<b>...Failure</b><br>\n"; }

		   if ($buf == '') { $buf = '-hwError Blank Page returned';}


		   return array($which_used, $buf, 1);

		}

	}

	error_reporting(E_ALL ^ E_NOTICE);
	//error_reporting(0);

	$result =  array($which_used, $return_string, 0);
	return $result;
}

Function check_cache_1($max_age, $cache_file_1, $debug) {

	$expired = false;

	//return false if file does not exist
	if (file_exists($cache_file_1) == false) {
	     if ($debug) {print "cache_file: $cache_file_1 doesnt exist<br>\n"; }
		return true;
	}

	if (!$max_age) {
	     if ($debug) {print "invalid Max ('$max_age') age of 0 or undefined.<br>\n"; }
		return false;
	}

	//Get the files minute and hour
	$temp_arry = localtime(filemtime($cache_file_1));
	$fm = $temp_arry[1];
	$fh = $temp_arry[2];
	$fhm = sprintf('%02u%02u', $fh,$fm);

	//Get the minute and hour of right now
	$temp_arry = localtime(time());
	$m = $temp_arry[1];
	$h = $temp_arry[2];
	$hm = sprintf('%02u%02u', $h,$m);

	$age = (time() - filemtime($cache_file_1))/60;

	if ($debug) {print "Age of cache_file ('$cache_file_1') is $age in minutes<br>\n"; }
	if ($debug) print "maxage=$max_age<br>\n";
	$age_arry = explode(":", $max_age);

	while(list($key, $max) = each($age_arry)) {
	   if ($debug) {print "max_age break down checking max=$max against age=$age<br>\n"; }

		if (substr($max,0,2) == 'HM') {
			$max_hm = substr($max,2);
		        if ($hm >= $max_hm && ($fhm < $max_hm  || $fhm > $hm) && $age >1)
		        	$expired = true;
		}
		elseif(substr($max,0,1) == 'H') {
			$max_h = substr($max,1);
		        if ($h>=$max_h && ($fh < $max_h || $fh > $h) && $age > 1)
		        	$expired = true;
		}
		elseif(substr($max,0,1) == 'M') {
			$max_m = substr($max,1);
		        if ($m>=$max_m && ($fm < $max_m  || $fm > $m) && $age > 1)
		        	$expired = true;
		}
		else {
		        if ($age >= $max) 	$expired = true;
		}

		if ($expired == true) {
			if ($debug) {print "...Cache file expired<br>\n"; }
			return $expired;
		}
	}

      if ($debug) {print "...expired=$expired<br>\n"; }
	return $expired;
}



Function compare_caches($cache_file_1, $cache_file_2) {
	$difference = filemtime($cache_file_1) - filemtime($cache_file_2);
	if ($difference > 0)
		return false;
	else
		return true;
}


	Function save_file ($file_name, $string, $permissions=0666) {
		if (!$permissions) $permissions = 0666;

		if ($file_name) {

			if (file_exists($file_name)) unlink($file_name);
			if ($file = @fopen($file_name, "w")) {
				fputs($file, $string);
				fclose($file);
				@chmod($file_name, $permissions);
			}
		}
	}

?>