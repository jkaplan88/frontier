<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: NOAAADDS.php,v 1.2 2006/03/12 02:40:35 wxfyhw Exp $
*
*/

Class CurrentsNOAAADDS {

	var $do_round = "";
	var $decoded_line = "";
	var $hsky = 'N/A';
	var $hsky_conditions = '';
	//var $hweather = 'N/A';
	var $htempf = 'N/A';
	var $htempc = 'N/A';
	var $hdewptf = 'N/A';
	var $hdewptc = 'N/A';
	var $hrh = 'N/A';
	var $hwind = 'N/A';
	var $hwinddir = 'N/A';
	var $hwindspd = 'N/A';
	var $hwinddir_deg = 'N/A';
	var $hwcf = 'N/A';
	var $hwcc = 'N/A';
	var $hhif = 'N/A';
	var $hhic = 'N/A';
	var $hpressure = 'N/A';
	var $hpressure_mb = 'N/A';
	var $hremarks = 'N/A';
	var $hvisibility = 'N/A';
	var $hvisibility_miles = 'N/A';
	var $hvisibility_meters= 'N/A';
	var $hquk = 'N/A';
	var $hqul = 'N/A';
	var $metar = 'N/A';
	var $weather = "";
	var $hweather='';
	var $gusts = "";
	var $hforecastdate='';

	var $hceiling = 'N/A';

	var $fuelmoisture = 'N/A';
	var $fueltempf = 'N/A';
	var $batvolt = 'N/A';


	var $hplace='';
	var $hstate='';
	var $hcountry = '';

	var $default_hsky = 'fair';

	var $hsixhrhif = 'N/A';
	var $hsixhrlof = 'N/A';
	var $h24hrhif = 'N/A';
	var $h24hrlof = 'N/A';
	var $hsixhrhi_c = 'N/A';
	var $hsixhrlo_c = 'N/A';
	var $h24hrhi_c = 'N/A';
	var $h24hrlo_c = 'N/A';
	var $hthrhrprec = 'N/A';
	var $hsixhrprec = 'N/A';
	var $h24hrprec = 'N/A';


	var $clouds_type = array();
	var $clouds_height = array();
	var $clouds_total = 0;

	var $months = array ('JAN'=>1, 'FEB'=>2, 'MAR'=>3,'APR'=>4,'MAY'=>5,'JUN'=>6,'JUL'=>7,'AUG'=>8,'SEP'=>9,'OCT'=>10,'NOV'=>11,'DEC'=>12);



	Function CurrentsNOAAADDS() {

	}

	Function process_metar($icao, $type, &$metar_data) {
      	$found = 0;


 		$this->hplace = '';
 		$this->hstate = '';
 		$this->hcountry = '';


		if (preg_match("/>($icao [^<]+)/", $metar_data, $m)) {
			$this->metar = preg_replace('/\s+/', ' ', trim($m[1]));
		}

		$metar_data = preg_replace('/^[A-Fa-f0-9](?:[A-Fa-f0-9])?\r?$/m', '', $metar_data);

 		$metar_data = preg_replace(array('/<[^>]+>/', '/\&\#160;/', '/\&\#46;/', '/\&deg;/','/\&\#45;/'),
 							  array(' ', ' ', '.', '','-'),
 							  $metar_data);



		//observed 1340 UTC 18 September 2005
		if (preg_match('/observed +(\d\d\d\d?) +UTC +(\d\d?) +([A-Za-z][A-Za-z][A-Za-z])[A-Za-z]* +(\d\d\d\d)/', $metar_data, $m)) {
			$this->hforecastdate = sprintf('%04u.%02u.%02u', $m[4], $this->months[strtoupper($m[3])], $m[2]) . ' ' . $m[1] . ' UTC';
		}
		else {
			$this->hforecastdate = "UNKNOWN";
		}


		//Get the temp
		if (preg_match('/Temperature:\s+(-?[\d\.]+)C +\((-?[\d\.]+)F/',	$metar_data, $m)) {
			$this->htempc = $m[1];
			$this->htempf = $m[2];
		}

		//Get the dew point
		if (preg_match('/Dewpoint:\s+(-?[\d\.]+)C +\((-?[\d\.]+)F/', $metar_data, $m)) {
			$this->hdewptc = $m[1];
			$this->hdewptf = $m[2];
		}
		//Get the relative humidity
		if (preg_match('/\[RH *= *(\d\d?\d?)/', $metar_data, $m)) {
			$this->hrh = $m[1];
		}

		// get pressure : 30.18 inches Hg (1022.1 mb)
		//	30.16 inches Hg (1021.4 mb)
		if (preg_match('/Pressure \(altimeter\):\s+(\d\d\.\d\d) +inches Hg +\((\d\d\d\d?)/', $metar_data, $m)) {
			$this->hpressure = $m[1];
			$this->hpressure_mb = $m[2];
		}



		// get winds
		//	calm
		//	from the NNW (340 degrees) at   5 MPH (4 knots;    2.1 m&#47;s)
		//	variable direction winds at 3 MPH (3 knots; 1.6 m/s)
		if (preg_match('/Winds:\s+[Cc]alm/', $metar_data)) {
			$this->hwind = 'calm';
			$this->hwinddir='N';
			$this->hwindspd=0;
			$this->hwinddir_deg=0;
		}
		elseif (preg_match('/Winds:\s+from the +([NESW]+) +\((\d\d?\d?) .+?(\d\d?\d?) MPH/',$metar_data,$m)) {
			$this->hwind = $m[1] . ' ' . $m[3] . 'MPH';
			$this->hwinddir=$m[1];
			$this->hwindspd=$m[3];
			$this->hwinddir_deg=$m[2];
		}
		elseif (preg_match('/Winds:\s+variable .+?(\d\d?\d?) MPH/',$metar_data,$m)) {
			$this->hwind = 'VRB ' . $m[3] . 'MPH';
			$this->hwinddir='VRB';
			$this->hwindspd=$m[1];
			$this->hwinddir_deg='VRB';
		}

		//get gusts
		//	gusting to 26 MPH (23 knots; 12.0 m/s)
		if (preg_match('/gusting to +(\d\d?\d?) MPH/',$metar_data,$m)) {
			$this->hwind .= ' G ' . $m[1];
			$this->gusts = $m[1];
		}

		// get visibility
		//	7 miles (11 km)
		//	10 or more miles (16+ km)
		if (preg_match('/Visibility:\s+([\d.]+) *miles \(([\d.]+) *km/', $metar_data, $m)) {
			$this->hvisibility = $m[1] . ' SM';
			$this->hvisibility_meters = $m[2]*1000;
			$this->hvisibility_miles = $m[1];
		}
		elseif (preg_match('/Visibility:\s+10 .+?miles/', $metar_data, $m)) {
			$this->hvisibility = '>10 SM';
			$this->hvisibility_meters = 16000;
			$this->hvisibility_miles = 10;
		}


		$this->parse_clouds($this->metar);


		// ceiling
		//	7500 feet AGL
		if (preg_match('/Ceiling:\s+[\w ]*?([\d,]+) feet/', $metar_data, $m)) {
			$this->hceiling = str_replace(',', '',$m[1]);
		}



		if (preg_match('/Clouds:\s+sky clear/', $metar_data)) {
			$this->hsky_conditions = 'clear';
		}
		elseif (preg_match('/Clouds:\s+.+?overcast/', $metar_data)) {
			$this->hsky_conditions = 'overcast';
		}
		elseif (preg_match('/Clouds:\s+.+?broken +clouds/', $metar_data)) {
			$this->hsky_conditions = 'mostly cloudy';
		}
		elseif (preg_match('/Clouds:\s+.*?(?:scattered|few) +clouds/', $metar_data)) {
			$this->hsky_conditions = 'partly cloudy';
		}

		if (preg_match('/Weather:\s+.+?\(([^\)]+)/', $metar_data, $m)) {
			$this->hweather = $this->hsky = $this->weather = $m[1];
		}
		else {
			$this->hsky = $this->sky_conditions;
		}

		if ($this->hsky == '' || $this->hsky == 'N/A') $this->hsky = $this->default_hsky;



		if (preg_match("/\bQUK(\d+)/", $metar_data, $m)) {
			$this->hquk = $m[1];
		}
		if (preg_match("/\bQUL(\d+)/", $metar_data, $m)) {
			$this->hqul = $m[1];
		}




		$this->hwcf_old = $this->hwcf;
		$this->hwcf = $this->wind_chill($this->htempf, $this->hwind);
		$this->hhif = $this->heat_index($this->htempf, $this->hrh);

  		$this->decoded_line = strtoupper($icao).'|'.$this->hsky.'|'.$this->htempf.'|'.$this->hdewptf.'|'.
		      $this->hrh.'|'.$this->hwind.'|'.$this->gusts.'|'.$this->hpressure.'|'.$this->hwcf.'|'.$this->hhif.'|'.
		      $this->hquk.'|'.$this->hqul.'|'.$this->hwinddir.'|'.$this->hwindspd.'|'.$this->hvisibility.'|'.$this->hwcf_old.'|'.
		      $this->pwsurltext.'|'.$this->pwslon.'|'.$this->pwslat.'|'.$this->pwsurl.'|'.$this->pwssoftware_type.'|'.
		      $this->pwsrainin.'|'.$this->hweather.'|'.$this->hsky_conditions.'|'.$this->hvisibility_miles .'|'.
		      $this->hwinddir_deg . '|' . $this->hvisibility_meters . '|' .
		      $this->fueltempf . '|' . $this->fuelmoisture . '|' . $this->batvolt . '|' . $this->hprecn. '|' . $this->hprecw. '|' . $this->hprestrend_in. '|' . $this->hprestrend_mb. '|' . $this->hprestrend. '|' . $this->hsixhrhif. '|' . $this->hsixhrlof. '|' . $self ->{h24hrhif}. '|' . $self ->{h24hrlof}. '|' . $this->hsixhrhi_c. '|' . $this->hsixhrlo_c. '|' . $self ->{h24hrhi_c}. '|' . $self ->{h24hrlo_c}. '|' .$this->hthrhrprec. '|' .$this->hsixhrprec. '|' . $this->h24hrprec . '|' . $this->hceiling;


		//Celcius conversions of temp
		$this->htempc = $this->f2c($this->htempf);
		$this->hwcc = $this->f2c($this->hwcf);
		$this->hhic = $this->f2c($this->hhif);
		$this->hdewptc = $this->f2c($this->hdewptc);

		return 1;

	}

	Function process_short_metar($icao, $type, $metar_data) {

		$place = $state = $country ='';
		$found =0;

		if (preg_match('/PLACE: ([^\n]+), *([^\n]+), *([^,\n]+), ([^,\n]+)?/', $metar_data, $m)) {
			$place = $m[2]; $state = $m[3];
			if ($type) $place .= ", $state";
			if (isset($m[4])) { $country = ($m[4]) ? $m[4] : $state; }
			  else {$country = $state; }
			if (strtolower($country) == 'united states') $country = 'us';
			$this->hplace=$place;
			$this->hstate = $state;
			$this->hcountry = $country;
		}
		else {
			return 0;
		}

		if (preg_match('/(\d+\.\d+\.\d+ \d\d\d\d UTC)/', $metar_data, $m)) {
			$this->hforecastdate= $m[1];
		}
		elseif (preg_match('/([a-z]{3}) (\d{2}), (\d{4}) \- ([\d\:]+) ([AP]M) ([a-z]{3})[^\n]*\n/', $metar_data, $m)) {
			$this->hforecastdate = $m[1].' '.$m[2].', '.$m[3].' - '.$m[4].' '.$m[5].' '.$m[6];
		}
		else {
			$this->hforecastdate = 'UNKNOWN';
		}

		if (preg_match('/^LINE: +(.+)$/m', $metar_data, $m)) {
			$line= $m[1]. '|';
			@list($icao, $this->hsky,$this->htempf,$this->hdewptf,
				$this->hrh,$this->hwind,$this->gusts,$this->hpressure,$this->hwcf,$this->hhif,
				$this->hquk,$this->hqul,$this->hwinddir,$this->hwindspd,$this->hvisibility,$this->hwcf_old,
				$this->pwsurltext,$this->pwslon,$this->pwslat,$this->pwsurl,$this->pwssoftware_type,
				$this->pwsrainin,$this->hweather,$this->hsky_conditions,$this->hvisibility_miles,
				$this->hwinddir_deg, $this->hvisibility_meters, $this->fueltempf, $this->fuelmoisture,$this->batvolt,$this->hprecn, $this->hprecw, $this->hprestrend_in, $this->hprestrend_mb, $this->hprestrend, $this->hsixhrhif, $this->hsixhrlof,  $this->h24hrhif, $this->h24hrlof, $this->hsixhrhi_c, $this->hsixhrlo_c,  $this->h24hrhi_c, $this->h24hrlo_c, $this->hthrhrprec, $this->hsixhrprec, $this->h24hrprec, $this->hceiling) =
				explode('|', $line);

			$this->hpressure_mb  = $this->Inches2MB($this->hpressure);
			if (preg_match('/^METAR: (.+)$/m', $metar_data,$m)) {
				$this->metar = $m[1];
				$this->parse_clouds($this->metar);
			}

			//Celcius conversions of temp
			$this->htempc = $this->f2c($this->htempf);
			$this->hwcc = $this->f2c($this->hwcf);
			$this->hhic = $this->f2c($this->hhif);
			$this->hdewptc = $this->f2c($this->hdewptc);

			return 1;
		}
	}



	Function set($key, $value) {
		$key = trim($key);
		if ($key != '') $this->$key=$value;
	}

	Function wind_chill($temp, $wind, $old = "") {
		if (preg_match("/N\/A/", $wind) || preg_match("/N\/A/", $temp)) {
			return 'N/A';
		}

		if (preg_match("/(\d+)/", $wind, $matches)) {
			$wind = $matches[1];
		}
		else {
			$wind = 0;
		}
		$wc = "";
		$wind = intval($wind);
		if ($wind == 0) {
		   $wc = $temp;
		}
		elseif ($old == "") {
			$wc = 35.74+0.6215 * $temp - 35.75 * pow($wind,0.16) + 0.4275 * $temp * pow($wind, 0.16);
		}
		else {
			$wc=0.0817*(3.71*pow($wind,0.5) + 5.81 - 0.25*$wind)*($temp - 91.4) + 91.4;
		}
		if ($this->do_round) {
			$wc = round($wc);
		}

		if ($wc > $temp) $wc = $temp;
		return $wc;
	}

	Function heat_index($temp, $rh) {
		if ($temp == 'N/A' || $rh == 'N/A') return 'N/A';
		$temp = intval($temp); $rh = intval($rh);

		$hi = -42.379 + 2.04901523*$temp + 10.14333127*$rh - 0.22475541*$temp*$rh - (6.83783E-03)*pow($temp,2) - (5.481717E-02)*pow($rh,2) + (1.22874E-03)*pow($temp,2)*$rh + (8.5282E-04)*$temp*pow($rh,2) - (1.99E-06)*pow($temp,2)*pow($rh,2);

		if ($this->do_round) {
			$hi = round($hi);
		}

		if ($hi < $temp || $temp < 70)  $hi = $temp;
		return $hi;
	}

	Function f2c($tempf) {
		if ($tempf == 'N/A' || $tempf == '') return $tempf;

		$tempc = ($tempf - 32) * (5/9);

		if ($this->do_round) {
			$tempc = round($tempc);
		}

		return $tempc;
	}

	Function Inches2MB($in) {
		if ($in == 'N/A' || $in == '') return $in;
		$mb = floor(($in *33.86389)+.5);
		return $mb;
	}


	function parse_clouds($metar) {


		$this->clouds_type= array();
		$this->clouds_height = array();
		$this->clouds_total = 0;
		if (preg_match('/CLR/', $metar)) {
			array_push($this->clouds_type, 'CLR');
			array_push($this->clouds_height, '999');
			$this->clouds_total=1;

		}
		elseif (preg_match_all('/(FEW|SCT|BKN|OVC)(\d\d\d)/', $metar,$m)) {
			for ($i = 0; $i < sizeof($m[0]); $i++) {
				array_push($this->clouds_type, $m[1][$i]);
				array_push($this->clouds_height, $m[2][$i] * 100);
			}
			$this->clouds_total = sizeof($m[0]);
		}
	}



}
?>
