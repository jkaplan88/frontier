<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: NOAARSS.php,v 1.5 2006/12/20 05:47:30 wxfyhw Exp $
*
*/


class WarningsNOAARSS{

	var $warning_type = array();
	var $warning_header = array();
	var $warning_pretext = array();
	var $warning_text = array();
	var $warning_date = array();
	var $warning_kill_time = array();
	var $warning_zones = array();
	var $warning_wmo_type = array();
	var $warning_total = 0;
	var $wmo_type = array();

	var $debug=0;

	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	Function Warnings($debug) {
		$this->debug=$debug;
	}


	Function match_warnings($state, &$wx_info, $types) {
		$this->warning_zones = array();
		return array($this->find_warning(-1,-1, $state, $wx_info, $types), $this->warning_zones);
	}

	Function find_warning($wx_zone, $wx_county, $state, &$wx_info, $types) {

		if (strlen($wx_county) == 5) $wx_county = substr($wx_county,2);
		$wx_zone = preg_replace('/^[A-Za-z][A-Za-z][Zz]?(\d+)$/', '\1', $wx_zone);

		$debug = $this->debug;

		$min = gmdate('i');
		$hour = gmdate('H');
		$day = gmdate('d');
		$year = gmdate('Y');
		$month = gmdate('m');

		$counter=0;
		$ctime=gmdate('dHi');



		if (preg_match_all('/<item>.+?<title>([^<]+)<.+?<link>([^<]+)<.+?<description>([^<]+)<.+?<\/item>/s', $wx_info, $m)) {
			for ($i = 0; $i < sizeof($m[0]); $i++) {

				$body = preg_replace('/&gt;/', '>', $m[3][$i]);
				$body = preg_replace('/&lt;/', '<', $body);
				$body = preg_replace('/<br>/', "\n", $body);
				$body = preg_replace('/<a.+?<\/a[^>]*>/', '', $body);

				$title = preg_replace('/-.+$/','', $m[1][$i]);
				if (preg_match('/no\s+active\s+watch/i', $title)) continue;

				$link = $m[2][$i];

				$this->warning_wmo_type[$counter] = $title;

				#VAC670.AKQSVRAKQ.193200
				#http://www.nws.noaa.gov/alerts/tx.html#TXC381.WNSWOU0.202200

				if (preg_match('/\d\.\w\w\w(\w\w\w)\w+?\./', $link, $mm)) {

					$wmo = $mm[1];
					if (preg_match('/!' . $wmo . '/i', $types)) {
						# this is a type we are to skip:
						continue;
					}
					elseif ($types && !preg_match('/ALL|' . $wmo . '/i', $types)) {
						# if they set types and this type is not in there lets skip
						continue;
					}

					$this->emwin_type[$counter] = $wmo;
				}
				if (preg_match('/\.((\d\d)(\d\d)(\d\d))$/', $link, $mm)) {
					$stime = $mm[1];
					$sh = $mm[2];
					$sm = $mm[3];
					$ss = $mm[4];

					$yest = ($ctime < $stime) ? -1 : 0;

					$epoch = gmmktime($sh,$sm,$ss, $month, $day, $year) + ($yest * 24 * 3600);
					$this->warning_date[$counter] = gmdate('Hi', $epoch) . " GMT " . gmdate('M d Y', $epoch);
				}
				elseif (preg_match('/(\d\d\d\d?) +?([AP][AP]?M) +?(\w+) +?(\w+) +?(\w+) +?(\d\d?) +?(\d\d\d\d)/', $body, $dm)) {
					$ampm = ($dm[2] == 'AM') ? 'AM' : 'PM';
					$this->warning_date[$counter] = $dm[1] . ' ' . $ampm . ' ' . $dm[3] . ' '   . $dm[4] . ' '  . $dm[5] . ' '  . $dm[6] . ' '  . $dm[7];
				}

				$body = preg_replace('/\s+$/m','', $body);

				if (preg_match('/^(.+?\s[A-Z][A-Z][A-Z]\s+?\d\d?\s+?\d\d\d\d)([^\w ].+)$/s', $body, $mm)) {
					$this->warning_header[$counter] = trim($mm[1]);
					$body = $mm[2];
				}

				$this->warning_text[$counter] = $body;
				$this->warning_expiration[$counter] = '';

				list ($this->warning_coords_raw[$counter], $this->warning_coords_lat[$counter], $this->warning_coords_lon[$counter]) = $this->_parse_lat_lons($body);


				$counter++;

			} # end foreach  $section ($$wx_info =~ /\012(\w\wZ.*?\012.*?)\012(?:\$\$|NNNN?)/sg)

		}

		$this->warning_total = $counter;

		if ($counter) return $counter;



		$this->warning_type = array();
		$this->warning_header = array();
		$this->warning_pretext = array();
		$this->warning_text = array();
		$this->warning_date = array();
		$this->warning_kill_time = array();
		$this->warning_zones = array();
		$this->warning_wmo_type = array();
		return 0;

	}


	function _parse_lat_lons ($sec) {

		$lons = array();
		$lats = array();

		$t='';
		if (preg_match('/LAT...LON\s+([\d\s]+)/', $sec, $mm)) {
			$t = preg_replace('/\s+/', '', trim($m[1]));

			$coords = explode(' ', $t);
			for ($i =0; $i < sizeof($coords); $i+=2) {
				array_push($lats, $coords[$i]/100);
				array_push($lons, -$coords[$i+1]/100);
			}
		}

		return array($t, $lats, $lons);

	}





}


?>