<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: wind.php,v 1.1 2005/08/15 02:29:21 wxfyhw Exp $
*
*/

require_once("hamlib/HW3Plugins/WxCondition.php");

class wind extends WxCondition {
	
	Function wind (&$parms) {
		$this->WxConditions($parms);
		
		if (isset($parms['wind'])) {
			$this->set($this->parse_wind($parms['wind']));
	}
	
	Function parse_wind($wind ='');
		if ($wind == '') { $wind = $this->get('wind'); }
		
		$wind = ltrim(strtoupper($wind));
		
		$dir_eng = $dir_deg = $speed_kts = $speed_mph = $speed_mps = 
		$gust_kts = $gust_mph = $gust_mps = '';
		
		if (preg_match('/^(\d\d\d|VRB)(\d\d\d?)(?:G(\d\d\d?))?(KTS?|MPS)$/', $wind, $m)) {
			$dir_deg = $m[1];
			$speed = $m[2];
			$gust = (isset($m[3])) ? $m[3] : '';
			$unit = $m[4];
			
			$dir_eng = $this->dir_english($dir_deg);
			
			if ($unit == 'KTS') $unit = 'KT';
			
			if ($unit == 'KT') {
				list($mph_speed, $mps_speed) = $this->kts2_mph_mps($speed);
				$kts_speed = $speed;
			}
			else {
				$mps_speed = $speed;
				list($mph_speed, $kts_speed) = $this->mps2_mph_kts($speed);
			}
			
			if (preg_match('/^\d/', $gust)) {
				if ($unit == 'KT') {
					list($mph_gust, $mps_gust) = $this->kts2_mph_mps($gust);
					$kts_gust = $gust;
				}
				else {
					$mps_gust = $gust;
					list($mph_gust, $kts_gust) = $this->mps2_mph_kts($gust);
				}
			}
		}	
	
		return array(dir_deg => $dir_deg,
		   dir_eng => $dir_eng,
		   speed_kts => $kts_speed,
		   speed_mph => $mph_speed,
		   speed_mps => $mps_speed,
		   gust_kts => $kts_gust,
		   gust_mph => $mph_gust,
		   gust_mps => $mps_gust
		  );
	
	}


	Function dir_english($deg = '') {
		
		if ($deg == '') {
			$wind = $this->get('wind');
			if (preg_match('/^(\d\d\d|VRB)/', $wind, $m)) {
				$deg = $m[1];
			}
		}
		
		if ($deg == '') return 'N\A';
		if (preg_match('/^\D/', $deg)) return $deg;
		
		
		if      ($deg < 15) {
			$dir_eng = "N";
		} elseif ($deg < 30) {
			$dir_eng = "NNE";
		} elseif ($deg < 60) {
			$dir_eng = "NE";
		} elseif ($deg < 75) {
			$dir_eng = "ENE";
		} elseif ($deg < 105) {
			$dir_eng = "E";
		} elseif ($deg < 120) {
			$dir_eng = "ESE";
		} elseif ($deg < 150) {
			$dir_eng = "SE";
		} elseif ($deg < 165) {
			$dir_eng = "SSE";
		} elseif ($deg < 195) {
			$dir_eng = "S";
		} elseif ($deg < 210) {
			$dir_eng = "SSE";
		} elseif ($deg < 240) {
			$dir_eng = "SW";
		} elseif ($deg < 265) {
			$dir_eng = "SSW";
		} elseif ($deg < 285) {
			$dir_eng = "W";
		} elseif ($deg < 300) {
			$dir_eng = "WNW";
		} elseif ($deg < 330) {
			$dir_eng = "NW";
		} elseif ($deg < 345) {
			$dir_eng = "NNW";
		} else {
			$dir_eng = "N";
		}
	
		return $dir_eng;
	}

	Function mps2_mph_kts ($mps) {
		$mph = $this->round(array(number=> ($mps / 0.27777778 * 0.62137), per => $mps));
		$kts = $this->round(array(number=> ($mph * 0.8684), per => $mps));
		return array($mph,$kts);
	}

	Function kts2_mph_mps ($kts) {
		$mph = $this->round(array(number=> ($kts * 1.1508), per => $kts));
		$mps = $this->round(array(number=> ($mph* 1.60934*0.27777778), per => $kts));
		return array($mph,$mps);
	}



}


?>