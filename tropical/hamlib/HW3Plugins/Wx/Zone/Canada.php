<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Canada.php,v 1.10 2006/03/09 02:04:47 wxfyhw Exp $
*
*/

class CAZone {


	var $forecastdate = "";
	var $forecast_text = "";
	var $header = "";
	var $warnings = "";
	var $zone_warnings = '';

	var $debug=0;

	var $day_total = 0;
	var $day_fc = array();
	var $day_wx = array();
	var $day_hi = array();
	var $day_lo = array();
	var $day_pop = array();
	var $day_title = array();
	var $day_uvi = array();



	//Day Titles array
	var $day_titles = array("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY");

	var $days_hash = array("SUNDAY"=>0, "MONDAY"=>1, "TUESDAY"=>2,"WEDNESDAY"=>3,"THURSDAY"=>4,
         "FRIDAY"=>5, "SATURDAY"=>6);

         var $months = array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');

	var $use_today = 'TONIGHT';


	Function CAZone($debug) {
		$this->debug = $debug;
	}


	Function find_zone ($place, $wx_zone, $province, &$wx_info, $tzdif=0, $tzname='') {

		$debug = $this->debug;

		$province = strtoupper($province);
		$place = strtoupper($place);
		if ($tzdif == '') { $tzdif=0; $tzname = 'UTC'; }
		elseif ($tzdif != 0 && $tzname == '') {
			if ($tzdif > 0) { $tzname = 'GMT+' . ($tzdif+0);}
			else { $tzname = 'GMT' . $tzdif;}
		}


		$header = ''; $section = '';

		//Return an error if there was an error in the wx info
		if (preg_match("/[Ee]rror/", $wx_info)) {
			return 0;
		}

		$wx_info = preg_replace('/--+.+?--+/s', '', $wx_info);
		$wx_info = preg_replace('/\r/', '', $wx_info);
//if ($debug) print "wx_info=<pre>$wx_info</pre><br>\n";
		if (preg_match_all('/^(\w\w\w\w\d+?\s\w+?\s(\d\d)(\d\d\d\d).+?)\n\n(.+?\n)E[Nn][Dd]\b/sm', $wx_info, $m)) {
if ($debug) print "here<br>\n";
			for ($i= 0; $i < sizeof($m[0]); $i++) {
				$header = strtoupper($m[1][$i]);
				$day = $m[2][$i];
				$hrmin = $m[3][$i];
				$sections = $m[4][$i];
if ($debug) print "sizeof(m[0]) = " . sizeof($m[0]). "<br>\n";
if ($debug) print "$i=<pre>$sections</pre><br>\n";
				if (preg_match_all('/^(.+?\.\n)([\.\w].+?\.\n\n)/sm', $sections, $sm)) {
if ($debug) print "here2<br>\n";
					for ($j = 0; $j < sizeof($sm[0]); $j++) {
						$places = preg_replace('/�/','�',strtoupper($sm[1][$j]));
						$forecast_text = strtoupper($sm[2][$j]);

if ($debug) print "place=$place<br>places=<pre>$places</pre><br>\n";
						if (preg_match("/\\b$place\\b/", $places)) {
if ($debug) print "matched place<br>\n";
							$forecast_text = preg_replace(array('/^\s+/', '/\s+$/'), array('',''), $forecast_text);
							if (preg_match('/(.+)\.\.\.+/',$forecast_text, $ftm)) {
								$this->zone_warnings = $ftm[1];
								$forecast_text = preg_replace('/(.+)\.\.\.+/', $forecast_text);
							}

							$this->forecast_text = $forecast_text;
							$this->header = $header . "\n\n" . $places;

							$mon = gmdate('m');
							$year = gmdate('Y');

							if (preg_match('/^(\d\d)(\d\d)$/', $hrmin, $hm)) {
								$hr = $hm[1]; $min = $hm[2];
							}
							else {$hr = ''; $min = '';}
							$ampm = ($hr > 11) ? 'PM' : 'AM';
							if ($tzdif == 0) {
								$this->forecastdate = sprintf('%02u%02u',$hr,$min) . " UTC ".$this->months[$mon-1]." $day $year";
							}
							else {
								$this->forecastdate = $this->locallize_date($year, $mon, $day, $hr, $min, $tzdif, $tzname);
							}


							return 1;
						}
					}
				}
			}
		}

		$this->header = '';
		$this->forecastdate='';
		$this->forecast_text = '';
		return 0;

	}

	Function get_wxtype($wx_str, $types) {
		for ($i = 0; $i < sizeof($types); $i++) {
			$t= str_replace('/','\/',$types[$i]);
			if (preg_match('/'.$t.'/i', $wx_str)) {
				return 	$types[$i];
			}
		}
		return "";
	}

function str2upper($text){
   return strtr($text,
   "abcdefghijklmnopqrstuvwxyz".
   "\xB1\xE6\xEA\xB3\xF1\xF3\xB6\xBC\xBF". // ISO 8859-2
   "\xB9\x9C\x9F", // win 1250
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
   "\xA1\xC6\xCA\xA3\xD1\xD3\xA6\xAC\xAF". // ISO 8859-2
   "\xA5\x8C\x8F"  // win 1250
   );
}

	Function locallize_date($year, $mon, $day, $hour, $min, $tzdif, $tzname) {

	$ltime = gmmktime($hour,$min,0 ,$mon-1,$day,$year);
	$ltime += $tzdif*3600;

	return gmdate('Hi', $ltime) . "$tzname " . gmdate('D M j Y', $ltime);

	}

	Function process_zone($daysonly, &$day_title_array, &$wx_array, $today='') {

		if (!$day_title_array) {$day_title_array =& $this->day_titles;}
		if (!$today) $today = $this->use_today;

		$day_title = "";
		$day_fc = "";
		$day_fcs = array();
		$day_wxs = array();
		$day_his = array();
		$day_los = array();
		$day_pops = array();
		$day_uvis = array();
		$day_total = 0;
		$line = "";
		$firstnight = 0;
		$counter = 0;
		$spec_temps = $sc_counter = 0;

		$ftext = $this->forecast_text;

		$lines = preg_split('/\.\n(?=\w[^.]+\.\.)/', $ftext);
		reset($lines);
		while (list (,$line) = each($lines)) {
			$line =  preg_replace(array('/^\s+/', '/\s+$/'), array('',''), $line);
			if (preg_match('/WARNING|WATCH|ADVISORY|THREAT/', $line)) {
				$this->zone_warnings = preg_replace(array('/^\.+/', '/\.+$/'), array('',''), $line) . "\n";
			}
			elseif ($line) {
				$line .='.';
				if (preg_match('/^(?:NORMALS FOR THE PERIOD|OUTLOOK FOR)/', $line)) continue;
				if (preg_match('/(?:EXTENDED (?:FORECAST|OUTLOOK))|3 TO \d DAY FORECAST/', $line)) continue;
				if (preg_match('/^(.*?)\.\.+(.*)$/s', $line, $m)) {
					$day_title = $m[1]; $day_fc = $m[2];

					$day_fc = preg_replace(array('/PROBABILITY OF PRECIPITATION.+$/s','/NORMALS FOR THE PERIOD.+$/s'),
									array('',''), $day_fc);

					if ($daysonly && preg_match('/NIGHT|EVENING|NUIT|CE SOIR/', $day_title)) {
						if (!$counter) {
							$firstnight = 1;
							if ($daysonly > 1) continue;
							$day_title = $today;
						}
						else {
							list(,$day_los[$counter-1]) = $this->parse_temps($day_title, $day_fc);


							if (preg_match('/\s[Aa][Nn][Dd]\s/', $day_title)) {
								list($day1,$day2) = preg_split('/\s[Aa][Nn][Dd]\s/', $day_title);
								$day_title = (preg_match('/NIGHT|EVENING|NUIT|CE SOIR/', $day1)) ? $day2 : $day1;
							}
							else { $sc_counter++; continue;}
						}
					}

					//Store the day title and day fc
					$this->day_title[$counter] = $day_title;
					$day_fcs[$counter] = $day_fc;

					$day_fc = preg_replace('/\s+/', ' ', $day_fc);

					//Now, get the day weather
					if ($wx_array) {
						$wxtype = "";
						if (preg_match('/^(.+?LIKELY)/', $day_fc, $matches)) {
							$wxtype = $this->get_wxtype($matches[1], $wx_array);
						}
						if (!$wxtype || $wxtype == 'N/A') {
							$wxtype = $this->get_wxtype($day_fc, $wx_array);
						}
						$day_wxs[$counter] = $wxtype;
					}

					//Get the pop
					$day_pops[$counter] = $this->parse_pop($day_fc);
					$day_uvis[$counter] = $this->parse_uvi($day_fc);

					list($day_his[$counter], $day_los[$counter]) = $this->parse_temps($day_title, $day_fc);

				         //if the day title has and then lets spilt up into proper days
				         if (preg_match('/\s[Aa][Nn][Dd]\s/', $this->day_title[$counter])) {
				         	$tmp_array = preg_split('/\s[Aa][Nn][Dd]\s/', $this->day_title[$counter]);
				         	$day1 = $tmp_array[0];
				         	$day2 = $tmp_array[1];
				         	$this->day_title[$counter] = $day1;
						$this->day_title[++$counter] = $day2;
						$day_fcs[$counter] = $day_fcs[$counter-1];
						$day_wxs[$counter]  = $day_wxs[$counter -1];
						$day_his[$counter] = $day_his[$counter-1];
						$day_los[$counter] = $day_los[$counter-1];
						$day_pops[$counter] = $day_pops[$counter - 1];
						$day_uvis[$counter] = $day_uvis[$counter - 1];
				         }
				         elseif (preg_match('/\sTHROUGH\s/', $this->day_title[$counter])) {
				         	$tmp_array = preg_split('/\sTHROUGH\s/', $this->day_title[$counter]);
				         	$day1 = $tmp_array[0];
				         	$day2 = $tmp_array[1];
				         	$day1 = trim($day1);
				         	$day2 = trim($day2);
				         	$day1d = preg_replace('/\s+(?:NIGHT|EVENING)/', "", $day1);
				         	$day2d = preg_replace('/\s+(?:NIGHT|EVENING)/', "", $day2);
				         	$day1s = ""; $day2e = "";

				         	if ($this->days_hash[$day1d] != "" && $this->days_hash[$day2d] != "") {
				         		$day1s = $days_hash[$day1d];
          							$day2e = $days_hash[$day2d];
          							$i = $day1s - 1;

          							while ($i != $day2e) {
          								if (++$i > 6) {$i = 0;}
          								if ((($i == $day1s && $day1 != $day1d) ||  ($i == $day2e && $day2 != $day2d)) && $daysonly) {
          									$tmp_array = $self->parse_temps($day1, $day_fc);
          									$day_los[$counter--] = $tmp_array[1];
          								}
          								else {
          									if ($i == $day1s) {
										$this->day_title[$counter] = $day1;
									}
									else {
										$this->day_title[++$counter] = $this->day_title[$i];
										$day_fcs[$counter] = $day_fcs[$counter-1];
										$day_wxs[$counter]  = $day_wxs[$counter -1];
										$day_his[$counter] = $day_his[$counter-1];
										$day_los[$counter] = $day_los[$counter-1];
										$day_pops[$counter] = $day_pops[$counter - 1];
										$day_uvis[$counter] = $day_uvis[$counter - 1];
									}
								}
          							}//end while
				         	}
				         }

					$counter++;
		     			$sc_counter++;
		     		}
		     	}
		}

		if ($daysonly > 1) {
			$wday = date('w');

			if ($firstnight == 0) {$wday--;}

			for ($i=0; $i< $counter; $i++) {
				if (++$wday > 6) {$wday = 0;}
				if ($daysonly == 2) {$this->day_title[$i] = $day_title_array[$wday]; }
				else {$this->day_title[$i] = substr($day_title_array[$wday],0,3); }
			}
		} //end if ($daysonly > 1)

		$counter--;
		$day_total = $counter;

		//lets step through and see if we need to add the 3rd days lows
		if ($counter > 1) {
			for ($i = 1; $i < $counter; $i++) {
				if ($day_los[$i] == '') {
					if ($day_los[$i-1] != '' && $day_los[$i+1] != '') {
						$t = ($day_los[$i-1]+$day_los[$i+1])/2;
						if ($t>0) {$t = floor($t+.5); } else {$t = floor($t-.5);}
						$day_los[$i]=$t;
					}
				}
			}
		}

 		$this->day_total = $day_total;
		$this->day_fc =& $day_fcs;
		$this->day_wx =& $day_wxs;
		$this->day_hi =& $day_his;
		$this->day_lo =& $day_los;
		$this->day_pop =& $day_pops;
		$this->day_uvi =& $day_uvis;




		return $counter;
	}

	Function parse_temps($day_title, $forecast) {
		$segment = "";
		$low = "";
		$high = "";

		$debug = $this->debug;

		$day_title = strtolower($day_title);
		$forecast = strtolower($forecast);


		$match = array(); $replacement = array();


		$match[0] = '/uv index\s+?\d+?(?:\.\s*?\d+?)?/'; $replacement[0] = "";
		$match[1] = '/[\n\r\t\f ]+/'; $replacement[1] = " ";
		$match[2] = '/\b(?:one|un)\b/'; $replacement[2] = '1';
		$match[3] = '/\//'; $replacement[3] = "";
		$match[4] = '/teens/'; $replacement[4] = "10s";
		$match[5] = '/freezing/'; $replacement[5] = "32";
		$match[6] = '/trade[^.]+?\./'; $replacement[6] = "";
		$match[7] = '/(?:on\sthe\s)?\b(?:coast|inland)\b/'; $replacement[7] = "";
		$match[8] = '/\salong[^.]+?\./'; $replacement[8] = "\.";
		$match[9] = '/\bbut\b/'; $replacement[9] = ".";
		$match[10] = '/\sexcept/'; $replacement[10] = "\.";
		$match[11] = '/ ?with heat indices[^.]+?\./'; $replacement[11] = "\.";
		$match[12] = '/high cloudiness/'; $replacement[12] = " ";
		$match[13] = '/low cloud/'; $replacement[13] = " ";
		$match[14] = '/high/'; $replacement[14] = ". high";
		$match[15] = '/accumulation[^.]+?\./'; $replacement[15] = "";
		$match[16] = '/(\d+)\s+below(?:\s+z[e�]ro)?/'; $replacement[16] = "-\\1";
		$match[17] = '/(\d+)\s+above/'; $replacement[17] = "+\\1";
		$match[18] = '/\bz[e�]ro/'; $replacement[18] = "0";
		$match[19] = '/m(?:inus|oins) *(\d)/'; $replacement[19] = '-\1';
		$match[20] = '/(?:plus|pr�s) */'; $replacement[20] = '';
		$match[21] = '/wind\s+chills[^.]*\./'; $replacement[21] = "";
		$match[22] = '/\babout\b/'; $replacement[22] = "near";
		$match[23] = '/\s+\./'; $replacement[23] = ".";
		$match[24] = '/winds?\s+[^.]*\./'; $replacement[24] = "";

		$forecast = preg_replace($match, $replacement, $forecast);

		if (preg_match('/night|evening|nuit|ce soir/', $day_title) &&
			!preg_match('/ (and|through) /', $day_title)) {

			if ($debug) print "Doing night<br>\n";
			$forecast = preg_replace('/\b(?:lows?|mins?)\s(in)/', '\1', $forecast);
			$low = $this->get_temp($forecast);
			$high = "";
		}
		else {
			if (preg_match('/(\b(?:lows?|mins?) [^\.]+\.)/', $forecast, $matches)) {
				if ($debug) print "doing low<br>\n";
				if ($debug) print "forecast=$forecast<br>seg=".$matches[1]."<br>\n";

				$segment = substr($matches[1], 3);
				$segment = preg_replace('/ and/', ".", $segment);
				$low = $this->get_temp($segment);
			}
			else {
				$low = "";
			}

			$forecast = preg_replace('/(?:highs?|maxs?) [^\d.]+\./', "", $forecast);
			if (preg_match('/((?:highs?|temperatures?|maxs?|temp�ratures?) [^.]+\.)/', $forecast, $matches)) {
				$segment = substr($matches[1], 4);
				$segment = preg_replace('/ and/', ".", $segment);
				$high = $this->get_temp($segment);
				if ($low > $high)
					$low = "";
			}
		}

		return array($high, $low);
	}

	Function get_temp($segment) {
		$temp = "";
		$debug = $this->debug;

		if ($debug) echo "Searching segment:".$segment."<br>\n";

		if (preg_match('/(?:temp[e�]rature|lows?|highs?|min?|max?)[^\d.+-]+([+-]?\d+)s? (?:to|\�).* ([+-]?\d+)s?\./s', $segment, $tm)) {
			$temp = floor(($tm[1]+$tm[2])/2);
		}
		elseif(preg_match('/(?:temp[e�]rature|lows?|highs?|min?|max?)[^\d.+-]+([+-]?\d+)s?\.?/s', $segment, $tm)) {
			$temp = $tm[1];
		}
		elseif (preg_match('/([+-]?\d+)s? (?:to|\�)[^\d.+-]+([+-]?\d+)s?\./s', $segment, $tm)) {
			$temp = floor(($tm[1]+$tm[2])/2);
		}
		elseif (preg_match('/(?:around|near)[^\d.+-]+([+-]?\d+)/', $segment, $tm)) {
			$temp = $tm[1];
		}
		elseif(preg_match('/\s*([+-]?\d+)[. ]/', $segment, $tm)) {
			$temp = $tm[1];
		}
		else {$temp =''; }


		if ($debug) print "...temp found = $temp<br>\n";
		if (substr($temp,0,1) == '+') {
			$temp += 0;
		}

		return $temp;
	}




	Function parse_uvi($forecast) {
		$uvi = "";
		$forecast = strtolower($forecast);

		if (preg_match('/uv\s*?i(?:ndex)?\s+(\d+)/i', $forecast, $matches)) {
			$uvi = $matches[1];
		}

		return $uvi;
	}

	Function parse_pop($forecast) {
		$pop = "";
		$forecast = strtolower($forecast);

		if (preg_match('/chance\s+?of\s+?[^.\d]+?(\d+)\s+?percent/', $forecast, $matches)) {
			$pop = $matches[1];
		}
		elseif (preg_match('/(\d+)\s+?percent(?:\s+?chance)?/', $forecast, $matches)) {
			$pop = $matches[1];
		}

		return $pop;
	}

	Function set($key, $value) {
		$this->$key=$value;

		return 1;
	}






}

?>