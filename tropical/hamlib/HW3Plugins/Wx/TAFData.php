<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: TAFData.php,v 1.3 2003/02/24 12:49:48 lee Exp $
*
*/


class TAFData{

	var $debug=0;

	var $taf = '';
	var $ICAO='';
	var $amended ='';
	var $corrected = '';
	var $origin_date='';
	var $valid_period = '';
	var $origin_full_date = '';
	var $begin_day = '';
	var $begin_hour = '';
	var $end_day ='';
	var $end_hour = '';

	var $data_total = 0;
	var $data = array();


	Function TAFData($debug) {
		$this->debug =$debug;
	}

	Function ParseTAF($taftext) {
		$this->taf = $taftext;

		$this->_parse_taf();
	}


	Function _parse_taf() {

		$taf = preg_replace(array('/[\r\n]+/','/=/', '/^\s+/'), array(' ','','') , $this->taf);
		$taf_data = array();
		$i=0;

		$secs = preg_split('/\s(?=FM\d\d\d\d|TEMPO|INTER|BECMG|GRADU|PROB)/', $taf);

		foreach ($secs as $sec) {
			$sec = preg_replace('/\s+/', ' ', $sec);
			$data=array();

			if (preg_match('/^(TEMPO|INTER)\s(.+)$/', $sec, $m)) {
				$type = $m[1]; $rest = $m[2];
				$bh=''; $eh='';
				if (preg_match('/^(\d\d)(\d\d)\s/', $rest, $m)) {
					if ($m[1]<23 && $m[2] > 0 && $m[2] < 24) {
						$bh = $m[1]; $eh=$m[2];
						$rest = substr($rest,5);
					}
				}
				if ($bh=='' && $eh =='') {
					if(isset($taf_data[$i-1]['begin_hour'])) {
						$bh=$taf_data[$i-1]['begin_hour'];
					}
					if(isset($taf_data[$i-1]['end_hour'])) {
						$eh = $taf_data[$i-1]['end_hour'];
					}
				}
				$taf_data[$i] = $this->get_taf_data($type, $bh, $eh, $rest);
				$taf_data[$i++]['segment'] = $sec;
			}
			elseif(preg_match('/^FM(\d\d)(\d\d)?Z?\s(.+)$/', $sec,$m)) {
				$taf_data[$i] = $this->get_taf_data('FROM', $m[1], $m[2], $m[3]);
				$taf_data[$i++]['segment'] = $sec;
			}
			elseif(	preg_match('/^(BECMG|GRADU)\s(\d\d)(\d\d)\s(.+)$/', $sec, $m)) {
				$taf_data[$i] = $this->get_taf_data($m[1], $m[2], $m[3], $m[4]);
				$taf_data[$i++]['segment'] = $sec;
			}
			elseif(preg_match('/^PROB\s*(\d\d)\s(\d\d)(\d\d)\s(.+)$/', $sec, $m)) {
				$prob = $m[1];
				$taf_data[$i] = $this->get_taf_data('PROB', $m[2], $m[3], $m[4]);
				$taf_data[$i]['prob'] = $prob;
				$taf_data[$i++]['segment'] = $sec;
			}
			elseif(preg_match('/^(\d\d\d\d\/\d\d\/\d\d\s\d\d:\d\d\s)?(?:\w\w\w\w\w+\s)?([A-Z0-9]{4})\s(?:TAF\s)?(?:(AMD)\s)?(?:(COR)\s)?(?:(\d\d\d\d\d\d)[Z+]\s)?(\d\d\d\d\d?\d?)\s(.+)$/s', $sec,$m)) {
				$full_date = $m[1];
				$this->ICAO=$m[2];
				$this->amended=$m[3];
				$this->corrected = $m[4];
				$this->origin_date = $m[5];
				$this->valid_period = $m[6];
				$rest = $m[7];

				if (preg_match('/^(\d\d\d\d)\/(\d\d)\/(\d\d)\s(\d\d):(\d\d)/',$full_date, $m)) {
					$this->origin_date = $m[3].$m[4].$m[5];
					$this->origin_full_date =$m[1].'/'.$m[2].'/'.$m[3];
				}

				if (preg_match('/^(\d\d)?(\d\d)(\d\d)$/', $this->valid_period, $m)) {
					$vbday=$m[1]; $vbhour = $m[2]; $vehour=$m[3];
				}
				else {$vbday= $vbhour = $vehour = ''; }
				if ($vehour == 24) $vehour = '00';

				if (!$vbday) $vbday = gmdate('d');
				$veday = ($vehour <= $vbhour) ? $vbday+1 : $vbday;

				$this->begin_day = $vbday;
				$this->begin_hour=$vbhour;
				$this->end_day = $veday;
				$this->end_hour = $vehour;

			         $taf_data[$i] = $this->get_taf_data('MAIN', $vbhour, $vehour, $rest);
			         $taf_data[$i++]['segment'] = $sec;
			}
		}

		$this->data_total = --$i;
		$this->data =& $taf_data;

		for ($i = 0; $i <= $this->data_total; $i++) {
			if (isset($taf_data[$i]['type']) && $taf_data[$i]['type'] == 'FROM') {
				$j = $this->get_next_taf_item($taf_data, $i);
				if (!$j ||!isset( $taf_data[$j]['begin_hour'])) { $taf_data[$i]['end_hour'] = $this->end_hour;}
				 else {$taf_data[$i]['end_hour'] = $taf_data[$j]['begin_hour'];}
			}
		}
	}

	Function get_next_taf_item(&$taf, $i) {
		$i++;
		$total = sizeof($taf);

		if ($i>$total) return 0;
		if (isset($taf[$i]['type']) && preg_match('/TEMPO|INTER/', $taf[$i]['type'])) {
			return $this->get_next_taf_item($taf, $i);
		}
		return $i;
	}

	Function get_last_taf_item(&$taf, $i) {
		$i--;
		$total = sizeof($taf);

		if ($i<0) return -1;
		if (isset($taf[$i]['type']) && preg_match('/TEMPO|INTER/', $taf[$i]['type'])) {
			return $this->get_last_taf_item($taf, $i);
		}
		return $i;
	}

	Function get_taf_data ($type, $begin, $end, $line) {
		$data = $this->get_taf_values($line);
		$data['type'] = $type;
		$data['begin_hour'] = $begin;
		$data['end_hour'] = $end;

		return $data;
	}


	Function get_taf_values($line) {
		$tokens = preg_split('/\s+/', $line);

		$toks = array();
		reset ($tokens);
		$next_tok_vis=0;
		while (list(,$tok) = each($tokens)) {

			if ($next_tok_vis) {
				$toks['vis'] .= ' ' . $tok;
				$next_tok_vis = 0;
			}
			// wind info
			elseif (!isset($toks['wind']) && preg_match('/(?:VRB|.*?(?:KT|MPS))$/', $tok)) {
				$toks['wind'] = $tok;
			}
			// visibility
			elseif(!isset($toks['visibility']) && preg_match('/^(?:.*?SM|\d\d\d\d)$/', $tok)) {
				$toks['vis'] = $tok;
			}
			elseif(preg_match('/^\d$/', $tok)) {
				$toks['vis'] = $tok;
				$next_tok_vis=1;
			}
			elseif(preg_match('/^(-|\+|VC)?(NSW|TS|SH|FZ|BL|DR|MI|BC|PR|RA|DZ|SN|SG|GR|GS|PE|IC|UP|BR|FG|FU|VA|DU|SA|HZ|PY|PO|SQ|FC|SS|DS|\/)+$/', $tok)) {
				if (!isset($toks['weather'])) $toks['weather'] = array();
				array_push($toks['weather'], $tok);
			}
			elseif(preg_match('/SKC|CLR|NSC|OK$/i', $tok) || preg_match('/(FEW|SCT|BKN|OVC|VV)(\d\d\d)(CB|TCU)?$/i', $tok)) {
				if (!isset($toks['sky'])) $toks['sky'] = array();
				array_push($toks['sky'], $tok);
				if ($tok == 'CAVOK') $toks['visibility'] = '9999';
			}
			elseif (preg_match('/^WS\d\d\d\/\d\d\d\d\dKT$/', $tok)) {
				$toks['windshear'] = $tok;
			}
			elseif(preg_match('/^QNH\d+INS$/', $tok)) {
				$toks['altimeter'] = $tok;
			}
			elseif(preg_match('/^\d\d\d\d\d\d$/', $tok)) {
				if (!isset($toks['icing'])) $toks['icing'] = array();
				array_push($toks['icing'], $tok);
			}
		}

		// get wind values;
		if (isset($toks['wind'])) $this->parse_wind($toks['wind'], $toks);
		if (isset($toks['altimeter'])) $this->parse_pressure($toks['altimeter'], $toks);
		$toks['VISIBILITY'] = (isset($toks['vis'])) ? $this->parse_visibility($toks['vis']) : '';

		return $toks;
	}

	Function parse_pressure($alt, &$toks) {
		if (preg_match('/QNH(\d+)INS/', $alt, $m)) {
			$toks['pressure_in'] = $m[1]/100;
			$toks['pressure_mb'] = floor($m[1]/100*33.8639+.5);
		}
	}

	Function parse_visibility($vis) {
		$visibility ='';

		if ($vis == '') return '';

		if (preg_match('/SM$/', $vis)) {
			$vis = preg_replace('/SM$/', '', $vis);
			if (preg_match('/^M?(\d\/\d)/', $vis, $m)) {
				$visibility = 'Less than '.$m[1].' statute miles';
			}
			elseif(preg_match('/^P6SM/', $vis)) {
				$visibility = "> 6 Statute Miles";
			}
			else{ $visibility = $vis . " Statute Miles"; }
		}
		else {
			if ($vis == '9999') {$visibility = '> 10KM';}
			else { $visibility = "$vis M"; }
		}


		return $visibility;
	}


	Function parse_wind($wind, &$toks) {

		$dir_deg = strtoupper(substr($wind,0,3));
		$dir_eng='';


		//Check for wind direction
		if ($dir_deg == 'VRB') {
			$dir_deg = 'Variable';
			$dir_eng = 'VRB';
		}
		else {
		      if      ($dir_deg < 15) {
		          $dir_eng = "N";
		      } elseif ($dir_deg < 30) {
		          $dir_eng = "NNE";
		      } elseif ($dir_deg < 60) {
		          $dir_eng = "NE";
		      } elseif ($dir_deg < 75) {
		          $dir_eng = "ENE";
		      } elseif ($dir_deg < 105) {
		          $dir_eng = "E";
		      } elseif ($dir_deg < 120) {
		          $dir_eng = "ESE";
		      } elseif ($dir_deg < 150) {
		          $dir_eng = "SE";
		      } elseif ($dir_deg < 165) {
		          $dir_eng = "SSE";
		      } elseif ($dir_deg < 195) {
		          $dir_eng = "S";
		      } elseif ($dir_deg < 210) {
		          $dir_eng = "SSE";
		      } elseif ($dir_deg < 240) {
		          $dir_eng = "SW";
		      } elseif ($dir_deg < 265) {
		          $dir_eng = "SSW";
		      } elseif ($dir_deg < 285) {
		          $dir_eng = "W";
		      } elseif ($dir_deg < 300) {
		          $dir_eng = "WNW";
		      } elseif ($dir_deg < 330) {
		          $dir_eng = "NW";
		      } elseif ($dir_deg < 345) {
		          $dir_eng = "NNW";
		      } else {
		          $dir_eng = "N";
		      }
		}

		if (preg_match('/...(\d\d\d?)/', $wind, $m)) {
			$speed = $m[1];
		}
		else {$speed = 0;}

		if (preg_match('/MPS/', $wind)) {
			$mps_speed=$speed;
			list($mph_speed, $kts_speed) = $this->mps2_mph_kts($speed);
		}
		else {
			$kts_speed=$speed;
			list($mph_speed, $mps_speed) = $this->kts2_mph_mps($speed);
		}

		$kts_gust ='';
		$mph_gust='';
		$mps_gust='';
		if (preg_match('/.{5,6}G(\d\d\d?)/', $wind, $m)) {
			$gust = $m[1];
			if (preg_match('/MPS/', $wind)) {
				$mps_gust = $gust;
				list($mph_gust, $kts_gust) = $this->mps2_mph_kts($gust);
			}
			else {
				$kts_gust = $gust;
				list($mph_gust, $mps_gust) = $this->kts2_mph_mps($gust);
			}
			$mph_gust = $kts_gust * 1.1508;
		}

		$toks['WIND_KTS'] = $kts_speed;
		$toks['WIND_MPH'] = $mph_speed;
		$toks['WIND_MPS'] = $mps_speed;

		$toks['WIND_KTS_GUST'] = $kts_gust;
		$toks['WIND_MPH_GUST'] = $mph_gust;
		$toks['WIND_MPS_GUST'] = $mps_gust;

		$toks['WIND_DIR_DEG'] = $dir_deg;
		$toks['WIND_DIR_ENG'] = $dir_eng;

	}

	Function mps2_mph_kts($mps) {
		$mph = $mps / 0.27777778 * 0.62137;
		$kts = $mph * 0.8684;
		return array($mph, $kts);
	}

	Function kts2_mph_mps($kts) {
		$mph = $kts * 1.1508;
		$mps = $mph* 1.60934*0.27777778;
		return array($mph, $mps);
	}

}



?>