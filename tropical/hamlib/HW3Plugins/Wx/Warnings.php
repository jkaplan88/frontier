<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Warnings.php,v 1.23 2006/06/27 11:41:07 wxfyhw Exp $
*
*/


class Warnings{

	var $warning_type = array();
	var $warning_header = array();
	var $warning_pretext = array();
	var $warning_text = array();
	var $warning_date = array();
	var $warning_kill_time = array();
	var $warning_zones = array();
	var $warning_wmo_type = array();
	var $warning_total = 0;
	var $wmo_type = array();

	var $debug=0;

	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	Function Warnings($debug) {
		$this->debug=$debug;
	}


	Function match_warnings($state, &$wx_info, $types) {
		$this->warning_zones = array();
		return array($this->find_warning(-1,-1, $state, $wx_info, $types), $this->warning_zones);
	}

	Function find_warning($wx_zone, $wx_county, $state, &$wx_info, $types) {
//print "find_warning routine<br>\n";

//print "wxinfo=$wx_info<br>\n";
		if (strlen($wx_county) == 5) { $wx_county = substr($wx_county,2); }
		$wx_zone = preg_replace('/^[A-Za-z][A-Za-z][Zz]?(\d+)$/', '\1', $wx_zone);

		# City and States must be capital
		$state = strtoupper($state);
		if ($types == 'ALL') $types = '';


		# Declare some working vars
		$header=''; $section=''; $ctime='';

		$ctime=gmdate('dHi');
		$day = gmdate('d');
		$this->warning_current_time = $ctime;

   		# Return error if there's an error
   		if (preg_match('/[Ee]rror/', $wx_info)) {return array(0, $wx_info); }

   		$wx_info = preg_replace(array('/<[Hh][Rr]>/','/<[^>]+?>/','/\r/', '/\$\$/'),
   				array("\n\n\03\n\n", '', '', "\n\n\$\$\n\n", ), $wx_info);
   		$wx_info = preg_replace('/^ +/m', '', $wx_info);
//
//print "wxinfo=<pre>$wx_info</pre><br>\n";
		//   my $year = (localtime(time))[5]+1900;
		# Find place's  or Wx zone's data from wx_info

		$counter = 0;
		$wtext = array(); $wheader = array(); $wdate = array(); $wkilltime=array();

		if (preg_match_all('/\012\w\w\w\w[^\03]+?\012\w\w[ZC][^\03]+?\03/', $wx_info, $matches)) {
			for ($i = 0; $i < sizeof($matches[0]); $i++) {
				$section = $matches[0][$i];
				if (preg_match('/^(.+?\d\d?\s+?\d\d\d\d)[^\w ]/s', $section, $hmatches)) {
					$header = $hmatches[1];
				}
				$section = str_replace($header, '', $section);
				$header = trim($header);

//print "<b>header</b>=<pre>$header</pre><br><b>section</b>=<pre>$section</pre><br>\n";

				if (preg_match('/\d\d\d\d\d\d-/', $header)) {
//print "<font color=red>TESTING</font><br>";
					# lets see if we need to make sure it is a valid type
					if (preg_match('/^(\w\w\w)\w\w[\w ]?[\r\n]/m', $header, $hmatches)) {
//print "valid type!<br>\n";
						if(!($types && strpos($types, $hmatches[1]) === false)) {
//print "<br><h2>lets check</h2><br>\n";
							if ($this->_check_warning($section, $header, $counter, $wx_zone, $wx_county, $state, $ctime, $day)) {
//print "<b>header1</b>=$header<br>\n";
//print "<h2>woohoo</h2><br>\n";
								$this->warning_pretext[$counter] = '';
								$counter++;
							}
						}
					}
				}
				else {
//print "<br><h2>ELSE check</h2><br>\n";
					if (preg_match('/^(.+?)(\012\w\w[ZC]\d.+)/s', $section, $smatches)) {
//print "<b>header2</b>=<pre>$header</pre><br>\n";
						$pretext = $smatches[1]; $section = $smatches[2];
//print "<b>section</b>=<pre>$section</pre><br>\n";
						preg_match_all('/\012(\w\w[ZC]\d.+?\n\n+)([^\03\$]+?)(?:\$\$|\03)/s', $section, $smatches);
						for ($j = 0; $j < sizeof($smatches[0]); $j++) {
							$theader = $header . "\n\n" . $smatches[1][$j];
							$ttext = $smatches[2][$j];
//print "<b>Theader</b>=<pre>$theader</pre><br>\n<b>text</b>=<pre>$ttext</pre><br>\n";
							if ($this->_check_warning($ttext, $theader, $counter, $wx_zone, $wx_county, $state, $ctime, $day)) {
								if ($wx_zone != -1) $this->warning_pretext[$counter] = $pretext;
								$counter++;
//print "<h1>woohoo</h1><br>\n";
							}
						}
					}
				}
			}
		}


//print "counter=$counter<br>\n";
		$this->warning_total = $counter;
		if ($counter) { return $counter; }
		else {
			$this->warning_text = array();
			$this->emwin_type = array();
			$this->wmo_type = array();
			$this->warning_pretext = array();
			$this->warning_header = array();
			$this->warning_date = array();
			$this->warning_kill_time = array();
		}
		return 0;
	}





	Function _check_warning  (&$section_ref, &$header_ref, $counter, $wx_zone, $wx_county, $state, $ctime, $day) {

		$debug = $this->debug;
		if ($debug) print "Blank _check_warning routine<br>\n";

		$kt_day = $ktime = '';
		$emwin_type ='';

		if (preg_match('/\012(\w\w\w)\w\w\w[\r\n]/', $header_ref, $m)) $emwin_type = $m[1];
		if (preg_match('/((\d\d)(\d\d\d\d))-/', $header_ref, $m)) {
			$ktime = $m[1];
			$kt_day = $m[2];
			$kt_time = $m[3];
		}

		$wmo_type = $this->_parse_wmo_type($header_ref);
		if (!$wmo_type)  $wmo_type = $this->_parse_wmo_type($section_ref);
if ($debug)  print "<hr><br>wmo_type=$wmo_type...ctime = $ctime<br>...ktime=$ktime<br>...day=$day<br>...kt_day=$kt_day<br>...wx_zone=$wx_zone<br>\n";
if ($debug)  print "...<br><pre>$header_ref</pre><br>\n      ";
		if ( ($ctime < $ktime || ($day - $kt_day) > 20) && $wx_zone != 0  &&
		      $this->_check_zones($wx_zone, $wx_county, $state,  $header_ref, $wmo_type))  {

			if ($wx_zone != -1 || $wx_county != -1) {
				$section_ref = preg_replace(array('/\03/', '/\n\d+\s*$/'), array('', "\n\n"), $section_ref);
				$section_ref = preg_replace(array('/^\n\n+/', '/\n\n+$/'), array("\n", "\n\n"), $section_ref);
				$this->warning_text[$counter] = preg_replace('/\$\$.*/', '', "\n" . $section_ref);
				$this->warning_header[$counter] = $header_ref;
				$this->warning_kill_time[$counter] = ($ktime) ? $ktime : 'UNKNOWN';
			         $this->emwin_type[$counter] = $emwin_type;
			         $this->warning_wmo_type[$counter] = $wmo_type;
//print "counter=$counter<br>warning_wmo_type=" . $this->warning_wmo_type[$counter] . "<br>\n";
//			         if (preg_match('/(\d{2,4}) ([AP]M) (\w+) (\w+) (\w+) ([^\.\n]+ \d\d\d\d)/', $header_ref, $matches)) {
//			         	$this->warning_date[$counter] = $matches[1] . ' ' . $matches[2]   . ' ' . $matches[3] . ' ' . $matches[4] . ' ' . $matches[5] . ' ' . $matches[6];
//			     	}
//			     	else {
//			     		$this->warning_date[$counter] = 'UNKNOWN';
//			     	}

				if (preg_match('/(\d\d\d\d?) +?([AP][AP]?M) +?(\w+) +?(\w+) +?(\w+) +?(\d\d?) +?(\d\d\d\d)/', $header_ref, $m)) {
					$ampm = ($m[2] == 'AM') ? 'AM' : 'PM';
					$this->warning_date[$counter] = $m[1] . ' ' . $ampm . ' ' . $m[3] . ' '   . $m[4] . ' '  . $m[5] . ' '  . $m[6] . ' '  . $m[7];
					$bday = $m[6]; $byear = $m[7];
					$bmon = $this->month_names[$m[5]]-1;
					$kmon = $this->month_names[$m[5]]-1;
					if ($kt_day < $bday) {
						$kmon = ($bmon == 11) ? 0 : $bmon+1;
					}
					$kyear = ($bmon == $kmon) ? $byear : $byear+1;

					$this->warning_expiration[$counter] = "$kt_time GMT " . $this->month_nums[$kmon] . sprintf(' %02u %04u', $kt_day, $kyear);

					if ($debug) print "ktime=$ktime...kt_time=$kt_time...kt_day=$kt_day<br>\n";
					if ($debug) print "byear=$byear...bmon=$bmon... bday= $bday<br>\n";
					if ($debug) print "kyear=$kyear...kmon=$kmon... kt_day= $kt_day<br>\n";
				}
				else {
					$this->warning_date[$counter] = 'UNKNOWN';
					$this->warning_expiration[$counter] = 'UNKNOWN';
				}

			}

			if ($debug) print "found!<br>leaving _check_warning<br>\n";
			return 1;
		}
		else {
			if ($debug)  print "NOT found!<br>leaving _check_warning<br>\n";
			return 0;
		}


	}

	Function _check_zones ($zone, $county, $state, &$check_string, $type) {

		$debug = $this->debug;
		if ($debug) print "blank _check_zones routine<br>\n";
		$found = 0;
		if ($debug) print "<b>checkstrin</b>=$$check_string<br>\n";
		if (preg_match('/('.$state.'[ZC].+-\s*?)\d\d\d\d\d\d-/s', $check_string, $matches)) {
			$main_zone_args = $matches[1];
			if ($debug) print "<b>type=$type<br>zone=$zone<br>main args to check for $state</b>=$main_zone_args<br>\n";
			if (preg_match_all('/(\w\w)([ZC])([^A-Za-z]+)/', $main_zone_args, $matches)) {
				for ($ii = 0; $ii < sizeof($matches[0]); $ii++) {
					if ($matches[1][$ii] == $state) {
						$zc = $matches[2][$ii];
						$zone_args = $matches[3][$ii];

						if (preg_match_all('/(.+?)-/', $zone_args, $zmatches)) {
							for ($jj = 0; $jj < sizeof($zmatches[0]); $jj++) {
							$zone_arg = $zmatches[1][$jj];
							if ($zone != -1) {
								if(($zc == 'Z' && $zone == $zone_arg) || ($zc == 'C' && $county == $zone_arg)) {
								 	if ($this->_check_type($state.$zc.($zone+0), $type)) $found =1;
								}
								elseif (preg_match('/(\d+)>(\d+)/',$zone_arg, $izmatches)) {
								 	$z1 = $izmatches[1]; $z2 = $izmatches[2];
								 	if (($zc == 'Z' && $zone >= $z1 && $zone <= $z2) || ($zc == 'C' && $county >= $z1 && $county <= $z2)) {
									 	if($this->_check_type($state.$zc.($zone+0), $type)) $found=1;
								 	}
								}
								if ($found) {return $found;}
							}
							else {
								if (preg_match('/(\d+)>(\d+)/',$zone_arg, $izm)) {
								 	$z1 = $izm[1]; $z2 = $izm[2];
								 	for ($iz = $z1; $iz <= $z2; $iz++) {
								 		if ($this->_check_type($state.$zc.($iz+0), $type)) {
								 			$found = 1;
								 			break;
								 		}
								 	}
								}
								else {
									if ($this->_check_type($state.$zc.($zone_arg+0), $type)) $found=1;
							 	}
							 	if ($found) return $found;
							}
						}
					}
				}
			}
		}
	}
	return $found;

	}


	Function _parse_wmo_type($sec) {

//		print " _parse_wmo_type routine<br>\n";

		$type = '';

		if (preg_match('/WIND\s+?ADV/', $sec)) {
			$type = 'WIND ADV';
		}
		elseif (preg_match('/HIGH\s+?WIND\s+?WA(TCH|RNING)/', $sec, $m)){
			$type = 'HIGH WIND WA' . $m[1];
		}
		elseif(preg_match('/DENSE\s+?FOG\s+?ADV/', $sec)) {
			$type = 'DENSE FOG ADV';
		}
		elseif (preg_match('/EXCESSIVE\s+?HEAT\s+?(ADV|WARN)/', $sec, $m)) {
			$type = 'EXCESSIVE HEAT ' . $m[1];
		}
		elseif (preg_match('/WIND\s+?CHILL\s+?(ADV|WARN|WATCH)/', $sec, $m)) {
			$type = 'WIND CHILL ' . $m[1];
		}
		elseif (preg_match('/(?:(HARD)\s+?)?(FROST|FREEZE)\s+?(WARN|ADV|WATCH)/', $sec, $m)) {
			$type = $m[2] . ' ' . $m[3];
			if (isset($m[1])) $type = 'HARD ' . $type;
		}
		elseif (preg_match('/AIR\s+?STAGNATION\s+?ADV/', $sec)) {
			$type = 'AIR STAGNATION ADVISORY';
		}
		elseif(preg_match('/TORNADO\s+?WA(TCH|RNING)/', $sec, $m)) {
			$type = 'TORNADO WA' . $m[1];
		}
		elseif (preg_match('/BLIZZARD\s+?WARN/', $sec)) {
			$type = 'BLIZZARD WARNING';
		}
		elseif(preg_match('/(HEAVY|LAKE|LAKE\s+?EFFECT)\s+?SNOW\s+?WA(TCH|RNING)/', $sec, $m)) {
			$type = $m[1] . ' SNOW WA' . $m[2];
		}
		elseif (preg_match('/(ICE|WINTER)\s+?STORM\s+?(WATCH|WARNING|OUTLOOK)/', $sec, $m)) {
			$type = $m[1] . ' STORM ' . $m[2];
		}
		elseif(preg_match('/(?:(BLOWING|LAKE|LAKE\s+?EFFECT)\s+?)?SNOW\s+?ADV/', $sec, $m)) {
			$type = 'SNOW ADV';
			if (isset($m[1])) $type = $m[1] . ' ' . $type;
		}
		elseif (preg_match('/FREEZING\s+?(RAIN|DRIZZLE)\s+?ADV/', $sec, $m)) {
			$type = 'FREEZING ' . $m[1] . ' ADV';
		}
		elseif(preg_match('/WINTER\s+?WEATHER\s+?ADV/', $sec)) {
			$type = 'WINTER WEATHER ADV';
		}
		elseif(preg_match('/(SEVERE\s+?THUNDERSTORM|TORNADO)\s+?WA(TCH|RNING)/', $sec, $m)) {
			$type = $m[1] . ' WA'. $m[2];
		}
		elseif(preg_match('/(?:(FLASH|RIVER)\s+?)?FLOOD\s+?WA(TCH|RNING)/', $sec, $m)) {
			$type = 'FLOOD WA' . $m[2];
			if (isset($m[1])) $type = $m[1] . ' ' . $type;
		}
		elseif (preg_match('/(URBAN|SMALL\s+?STREAM)\s+?FLOOD\s+?(WARN|ADV)/', $sec, $m)) {
			$type = $m[1] . ' FLOOD ' . $m[2];
		}
		elseif (preg_match('/(?:(FLASH|RIVER)\s+?)?FLOOD\s+?STATEMENT/', $sec, $m)) {
			$type =  'FLOOD STATEMENT';
			if (isset($m[1])) $type = $m[1] . ' ' . $type;
		}

		elseif (preg_match('/AIRPORT\s+?WEATHER\s+?WARN/', $sec)) {
			$type = 'AIRPORT WEATHER WARNING';
		}

		elseif (preg_match('/(SPECIAL MARINE|GALE|STORM)\s+?WARN/', $sec, $m)) {
			$type = $m[1] . ' WARNING';
		}
		elseif(preg_match('/SMALL\s+?CRAFT\s+?ADV/', $sec)) {
			$type = 'SMALL CRAFT ADV';
		}
		elseif(preg_match('/(?:(HAZARDOUS|SEVERE)\s+?)WEATHER\s+?OUTLOOK/', $sec, $m)) {
			$type = 'WEATHER OUTLOOK';
			if (isset($m[1])) $type = $m[1] . ' ' . $type;
		}
		elseif(preg_match('/(SPECIAL|SEVERE)\s+?WEATHER\s+?STATEMENT/',$sec,$m)) {
			$type = $m[1] .' WEATHER STATEMENT';
		}
		elseif(preg_match('/AVIATION WEATHER WARNING/', $sec)) {
			$type = 'AVIATION WEATHER WARNING';
		}
		elseif(preg_match('/HURRICANE LOCAL STATEMENT/', $sec)) {
			$type = 'HURRICANE LOCAL STATEMENT';
		}
		elseif(preg_match('/(TROPICAL DEPRESSION .+LOCAL STATEMENT)/', $sec,$m)) {
			$type = $m[1];
		}
		elseif(preg_match('/\bNOW\b/', $sec,$m)) {
			$type = 'SHORTTERM FORECAST';
		}
		$type = preg_replace(array('/\s+/', '/\bADV\b/', '/\bWARN\b/'),
					     array(' ', 'ADVISORY', 'WARNING'), $type);

		return $type;

	}


	Function _check_type ($loc, $type) {
		$res = 0;

		$debug = $this->debug;
		if ($debug) {
			print "_check_type...<br>...type=$type<br>";
			print "...$this->warning_zones[$loc] =" . $this->warning_zones[$loc] . "<br>\n";
		}

		if (isset($this->warning_zones[$loc])) {
			if ($type) {
				$pos = strpos($this->warning_zones[$loc], $type);
				if ($pos === false) $res=1;
			}
			else { $res =1;}

			if ($res) $this->warning_zones[$loc] .= $type . '|';
		}
		else {
			$this->warning_zones[$loc] = $type . '|';
			$res = 1;
		}




//print "res=$res<br>\n";
		return $res;
	}





}


?>