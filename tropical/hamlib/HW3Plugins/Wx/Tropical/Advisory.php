<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2007 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Advisory.php,v 1.2 2007/10/03 19:52:24 wxfyhw Exp $
*
*/


class TropAdivsory {



	var $storm_forecast_total = -1;
	var $storm_forecast_interval = array();
	var $storm_forecast_epoch =  array();
	var $storm_forecast_day =  array();
	var $storm_forecast_time = array();
	var $storm_forecast_lat =  array();
	var $storm_forecast_lat_dir =  array();
	var $storm_forecast_lat_dec =  array();
	var $storm_forecast_lon =  array();
	var $storm_forecast_lon_dir =  array();
	var $storm_forecast_lon_dec =  array();
	var $storm_forecast_wind_kts =  array();
	var $storm_forecast_wind_mph =  array();
	var $storm_forecast_gusts_kts =  array();
	var $storm_forecast_gusts_mph =  array();


	var $storm_forecast_windfield_kts	 = array();
	var $storm_forecast_windfield_ne	 = array();
	var $storm_forecast_windfield_se	 = array();
	var $storm_forecast_windfield_sw	 = array();
	var $storm_forecast_windfield_nw	 = array();
	var $storm_forecast_windfield_total = array();

	var $tropical_header ='';
	var $tropical_body = '';
	var $wmo_header = '';
	var $NOAAEventNUM = '';
	var $stormregion ='';
	var $storm_name ='';
	var $tropical_product='';
	var $tropical_product_number='';
	var $advisory_number ='';
	var $event_type ='';
	var $weather_service = '';
	var $release_time = '';
	var $epoch_date = '';

	var $release_year ='';
	var $release_mon = '';
	var $release_day = '';

	var $month_hash = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	var $tzs = array(
   		'EST' => -5,
   		'EDT' => -4,
   		'CST' => -6,
   		'CDT' => -5,
   		'AST' => -4,
   		'PST' => -8,
   		'PDT' => -7,
   	);

	var $debug=0;


	Function _parse_body(&$data) {

		if (preg_match('/\n[\s]*\n+(\w.+)$/s', trim($data), $m)) {
			$body = $m[1];
		}
		else { $body = '';}

		$body = trim(preg_replace('/\r/','',$body));
		$body = preg_replace('/^\s+$/m', '', $body);

		$this->tropical_body =& $body;

		$type = (isset($this->tropical_product_number)) ? $this->tropical_product_number : 0;

		if ($type == 2 || $type ==3) {
			if (preg_match('/MINIMUM\s+?CENTRAL\s+?PRESSURE[^\d]+?(\d\d\d\d?)\s+?MB/', $body, $m)) {
				$this->storm_pressure_mb = $m[1];
			}
			elseif(preg_match('/(\d\d\d\d?)\s+?MB\s+?CENTRAL\s+?PRESSURE/', $body, $m)) {
				$this->storm_pressure_mb = $m[1];
			}
			else {
				$this->storm_pressure_mb = 'N/A';
			}

			$this->storm_pressure_in = ($this->storm_pressure_mb != 'N/A') ? floor($this->storm_pressure_mb/33.8639*100)/100 : 'N/A';


			if (preg_match('/MAX(?:IMUM)?\s+?SUSTAINED\s+?WINDS\D+?(\d+)\s+?(\w+)(.+?)\.\n\n/s', $body, $m)) {

				$rest = $m[3];
				if ($m[2] == 'MPH') {
					$this->storm_winds_mph = $m[1];
					$this->storm_winds_kts = floor(($m[1] * 0.868391)*10)/10;
				}
				else {
					$this->storm_winds_kts = $m[1];
					$this->storm_winds_mph = floor(($m[1] / 0.868391)*10)/10;
				}
			}
			else {
				$this->storm_winds_kts = '';
				$this->storm_winds_mph = '';
			}

			if (!empty($rest)) {
				if (preg_match('/GUSTS\s+?TO\s+?(\d+)\s+(MPH|KT)/',$rest,$m)) {
					$winds = $m[1];
					$unit = $m[2];
					if ($unit == 'MPH') {
						$this->storm_gusts_mph = $winds;
						$this->storm_gusts_kts = round(($winds*0.868391)*10)/10;
					}
					elseif ($unit == 'KT') {
						$this->storm_gusts_kts = $winds;
						$this->storm_gusts_mph = round(($winds/0.868391)*10)/10;
					}
				}

				{

					if (preg_match_all('/(\d+) KT\.+ *(\d+)NE +(\d+)SE +(\d+)SW +(\d+)NW\./', $rest, $m)) {

						for ($i=0; $i < count($m[0]); $i++) {
							$this->storm_windfield_kts[$i] = $m[1][$i];
							$this->storm_windfield_ne[$i] = $m[2][$i];
							$this->storm_windfield_se[$i] = $m[3][$i];
							$this->storm_windfield_sw[$i] = $m[4][$i];
							$this->storm_windfield_nw[$i] = $m[5][$i];
						}
						$this->storm_windfield_total = count($m[0]);
					}
				}
				{


					if (preg_match_all('/(\d+) FT SEAS\.+ ?(\d+)NE +(\d+)SE +(\d+)SW +(\d+)NW\./', $rest, $m)) {
						for ($i=0; $i < count($m[0]); $i++) {
							$this->storm_seas_ft[$i] = $m[1][$i];
							$this->storm_seas_ne[$i] = $m[2][$i];
							$this->storm_seas_se[$i] = $m[3][$i];
							$this->storm_seas_sw[$i] = $m[4][$i];
							$this->storm_seas_nw[$i] = $m[5][$i];

						}
						$this->storm_seas_total = count($m[0]);
					}
				}
			}


			#HURRICANE FORCE WINDS EXTEND OUTWARD UP TO 25 MILES... 35 KM...
			#FROM THE CENTER...AND TROPICAL STORM FORCE WINDS EXTEND OUTWARD UP
			#TO 115 MILES...185 KM.

			if (preg_match('/TROPICAL\s+?STORM\s+?FORCE\s+?WINDS[^\d]+?(\d+)\s+?MILES/', $body, $m)) {
				$this->ts_winds_extend_miles = $m[1];
				$this->ts_winds_extend_km = $m[1] / 0.621371;
			}
			else {
				$this->ts_winds_extend_miles = '';
				$this->ts_winds_extend_km = '';
			}
			if (preg_match('/HURRICANE\s+?FORCE\s+?WINDS[^\d]+?(\d+?)\s+?MILES/', $body, $m)) {
				$this->hurr_winds_extend_miles = $m[1];
				$this->hurr_winds_extend_km = $m[1] / 0.621371;
			}
			else {
				$this->hurr_winds_extend_miles = '';
				$this->hurr_winds_extend_km = '';
			}

			#MOVEMENT
			#TOWARD...WEST-NORTHWEST NEAR 10 MPH
			#PRESENT MOVEMENT TOWARD THE WEST-NORTHWEST OR 300 DEGREES AT   9 KT
			if (preg_match('/MOV(?:EMEN|ING).+?((?:NORTH|EAST|WEST|SOUTH)(?:NORTH|EAST|WEST|SOUTH|\-| )*)(?:.+ (\d+) +(MPH|KT))?/', $body, $m)) {
				$this->storm_direction = $m[1];
				$speed = (isset($m[2])) ? $m[2] : 0;
				$unit = (isset($m[3])) ? $m[3] : 'KT';
				if ($unit == 'MPH') {
					$this->storm_speed_mph = $speed;
					$this->storm_speed_kts = floor(($speed* 0.868391)*10)/10;
				}
				else {
					$this->storm_speed_kts = $speed;
					$this->storm_speed_mph = floor(($speed/ 0.868391)*10)/10;
				}
			}
			elseif (preg_match('/MOVEMENT.+?(STATIONARY|MEANDERING)/', $body, $m)) {
				$this->storm_direction = $m[1];
				$this->storm_speed_mph = $this->storm_speed_kts = 0;
			}
			else {
				$this->storm_direction = $this->storm_speed_mph = $this->storm_speed_kts = '';
			}

			if (preg_match('/REPEATING.+POSITION[\s.]+?(\d+?\.\d+?)\s+?(N|S)[\s.]+?(\d+?\.\d+?)\s+(W|E)/', $body, $m)) {
				$this->storm_lat = $m[1];
				$this->storm_lat_dir = $m[2];
				$this->storm_lat_dec = ($m[2] == 'N') ? $m[1] : '-' . $m[1];
				$this->storm_lon = $m[3];
				$this->storm_lon_dir = $m[4];
				$this->storm_lon_dec = ($m[4] == 'E') ? $m[3] : '-' . $m[3];
			}
			elseif (preg_match('/CENTER\s+?LOCATED\s+?NEAR\s+?(\d+?\.\d)\s*?(N|S)\s+?(\d+?\.\d)\s*?(W|E)/', $body, $m)) {
				$this->storm_lat = $m[1];
				$this->storm_lat_dir = $m[2];
				$this->storm_lat_dec = ($m[2] == 'N') ? $m[1] : '-' . $m[1];
				$this->storm_lon = $m[3];
				$this->storm_lon_dir = $m[4];
				$this->storm_lon_dec = ($m[4] == 'E') ? $m[3] : '-' . $m[3];
			}
			else {
				if (preg_match('/LATITUDE[\s.]+?(\d+?\.\d)\s+?(NORTH|SOUTH|N|S)/', $body, $m)) {
					$this->storm_lat = $m[1];
					$this->storm_lat_dir = substr($m[2],0,1);
					$this->storm_lat_dec = ($this->storm_lat_dir == 'N') ? $m[1] : '-' . $m[1];
				}
				else {
					$this->storm_lat = $this->storm_lat_dir = $this->storm_lat_dec ='';
				}

				if (preg_match('/LONGITUDE[\s.]+?(\d+?\.\d)\s+?(WEST|EAST|W|E)/', $body, $m)) {
					$this->storm_lon = $m[1];
					$this->storm_lon_dir = substr($m[2],0,1);
					$this->storm_lon_dec = ($this->storm_lon_dir == 'E') ? $m[1] : '-' . $m[1];
				}
				else {
					$this->storm_lon = $this->storm_lon_dir = $this->storm_lon_dec ='';
				}
			}
		}

		$curdate = gmdate('n') . '-' . gmdate('j');
		if ($type == 2 && preg_match_all('/(\d\d)\/(\d\d\d\d)Z\s+?(\d+?\.\d)(N|S)\s+?(\d+?\.\d)(E|W)(?:\.+\w+)?\s+?MAX\s+?WIND\s+?(\d+?)\s*?KT.+?GUSTS\s+?(\d+)\s*?KT\.((?:\s+\d+ KT\.+ *\d+NE +\d+SE +\d+SW +\d+NW\.)*)/', $body, $m)) {
			$j=0;
			for ($i=0; $i< count($m[0]); $i++) {
				$this->storm_forecast_interval[$j] = $m[1][$i] . '/' . $m[2][$i];
				$this->storm_forecast_day[$j] = $m[1][$i];
				$this->storm_forecast_time[$j] = $m[2][$i];
				$this->storm_forecast_lat[$j] = $m[3][$i];
				$this->storm_forecast_lat_dir[$j] = $m[4][$i];
				$this->storm_forecast_lat_dec[$j] = ($m[4][$i] == 'N') ? $m[3][$i] : '-' . $m[3][$i];
				$this->storm_forecast_lon[$j] = $m[5][$i];
				$this->storm_forecast_lon_dir[$j] = $m[6][$i];
				$this->storm_forecast_lon_dec[$j] = ($m[6][$i] == 'E') ? $m[5][$i] : '-' . $m[5][$i];
				$this->storm_forecast_wind_kts[$j] = $m[7][$i];
				$this->storm_forecast_wind_mph[$j] = floor(($m[7][$i]/ 0.868391)*10)/10;
				$this->storm_forecast_gusts_kts[$j] = $m[8][$i];
				$this->storm_forecast_gusts_mph[$j] = floor(($m[8][$i]/ 0.868391)*10)/10;


				$valid_epoch = $this->_format_date($curdate, $m[1][$i], $m[2][$i]);
				$this->storm_forecast_valid_date[$j] = gmdate('Hi D M d', $valid_epoch);
				$curdate = gmdate('n-j', $valid_epoch);
				$this->storm_forecast_epoch[$j] = $valid_epoch;

				if ($j > 0) {
					$hours_diff = ($valid_epoch - $this->storm_forecast_epoch[$j-1] )/3600;
					$this->storm_forecast_moving[$j] = $this->_get_bearing($this->storm_forecast_lat_dec[$j-1], $this->storm_forecast_lon_dec[$j-1], $this->storm_forecast_lat_dec[$j], $this->storm_forecast_lon_dec[$j]);
					$this->storm_forecast_speed_mph[$j] = $this->_speed($this->storm_forecast_lat_dec[$j-1], $this->storm_forecast_lon_dec[$j-1], $this->storm_forecast_lat_dec[$j], $this->storm_forecast_lon_dec[$j], $hours_diff);
					$this->storm_forecast_speed[$j] = round($this->storm_forecast_speed_mph[$i] / 1.1507794);
				}
				if ($j==1) {

					$this->storm_forecast_moving[0] = $this->storm_forecast_moving[$j];
					$this->storm_forecast_speed_mph[0] = $this->storm_forecast_speed_mph[$j];
					$this->storm_forecast_speed[0] = $this->storm_forecast_speed[$j];
				}


				if (!empty($m[9][$i])) {
					$this->storm_forecast_windfield_kts[$j]	 = array();
					$this->storm_forecast_windfield_ne[$j]	 = array();
					$this->storm_forecast_windfield_se[$j]	 = array();
					$this->storm_forecast_windfield_sw[$j]	 = array();
					$this->storm_forecast_windfield_nw[$j]	 = array();


					if (preg_match_all('/(\d+) KT\.+ *(\d+)NE +(\d+)SE +(\d+)SW +(\d+)NW\./', $m[9][$i], $mm)) {


						for ($ii=0; $ii < count($mm[0]); $ii++) {
							$this->storm_forecast_windfield_kts[$j][$ii] = $mm[1][$ii];
							$this->storm_forecast_windfield_ne[$j][$ii] = $mm[2][$ii];
							$this->storm_forecast_windfield_se[$j][$ii] = $mm[3][$ii];
							$this->storm_forecast_windfield_sw[$j][$ii] = $mm[4][$ii];
							$this->storm_forecast_windfield_nw[$j][$ii] = $mm[5][$ii];
						}
						$this->storm_forecast_windfield_total[$j] = count($mm[0]);
					}
				}

				$j++;
			}
			$this->storm_forecast_total = --$j;
		}
		elseif (($type == 4 || $type == 6)  && preg_match_all('/([\w ]{13})(\d\d)\/(\d\d\d\d)Z\s+?(\d+?\.\d)(N|S)\s+?(\d+?\.\d)(E|W)\s+?(\d+)\s*?KT(\.\.\.EXTRATROPICAL)?/', $body, $m)) {
			$j = 0;
			for ($i=0; $i< count($m[0]); $i++) {
				$this->storm_forecast_interval[$j] = $m[1][$i] ;

				$this->storm_forecast_day[$j] = $m[2][$i];
				$this->storm_forecast_time[$j] = $m[3][$i];
				$this->storm_forecast_lat[$j] = $m[4][$i];
				$this->storm_forecast_lat_dir[$j] = $m[5][$i];
				$this->storm_forecast_lat_dec[$j] = ($m[5][$i] == 'N') ? $m[4][$i] : '-' . $m[4][$i];
				$this->storm_forecast_lon[$j] = $m[6][$i];
				$this->storm_forecast_lon_dir[$j] = $m[7][$i];
				$this->storm_forecast_lon_dec[$j] = ($m[7][$i] == 'E') ? $m[6][$i] : '-' . $m[6][$i];
				$this->storm_forecast_wind_kts[$j] = $m[8][$i];
				$this->storm_forecast_wind_mph[$j] = floor(($m[8][$i]/ 0.868391)*10)/10;
				$this->storm_forecast_gusts_kts[$j] = '';
				$this->storm_forecast_gusts_mph[$j] = '';
				$this->storm_forecast_extratropical[$j] = ($m[9][$j]) ? 1 : 0;

				if ($i > 0) {
					$hours_diff = ($m[2][$i] > $m[2][$i-1] || $m[2][$i] == $m[2][$i-1]) ? (24 + abs($m[2][$i] - $m[2][$i-1])) : abs($m[2][$i] - $m[2][$i-1]);
					$this->storm_forecast_moving[$j] = $this->_get_bearing($this->storm_forecast_lat_dec[$j-1], $this->storm_forecast_lon_dec[$j-1], $this->storm_forecast_lat_dec[$j], $this->storm_forecast_lon_dec[$j]);
					$this->storm_forecast_speed_mph[$j] = $this->_speed($this->storm_forecast_lat_dec[$j-1], $this->storm_forecast_lon_dec[$j-1], $this->storm_forecast_lat_dec[$j], $this->storm_forecast_lon_dec[$j], $hours_diff);
					$this->storm_forecast_speed[$j] = round($this->storm_forecast_speed_mph[$i] / 1.1507794);

					$valid_epoch = $this->_format_date($curdate, $m[2][$i], $m[3][$i]);
					$this->storm_forecast_valid_date[$j] = gmdate('Hi D M d', $valid_epoch);
					$curdate = gmdate('n-j', $valid_epoch);
					$this->storm_forecast_epoch[$j] = $valid_epoch;
				}
				else {
					$this->storm_forecast_moving[$j] = '';
					$this->storm_forecast_speed_mph[$j] = '';
					$this->storm_forecast_speed[$j] = '';
					$this->storm_forecast_valid_date[$j] = '';
					$this->storm_forecast_epoch[$j] = '';
				}

//echo "hey hey =" . $this->storm_forecast_lat_dec[$j];
				$j++;
			}
			$this->storm_forecast_total = --$j;
		}

		if (preg_match('/LAST\s+?(?:PUBLIC\s+?|FORECAST\/)ADVISORY/', $body)) {
			$this->last_advisory=1;
		}
		else {$this->last_advisory=0;}
	}

	function _parse_header(&$data) {

		$data = preg_replace('/^\s+/', '', $data);
		$data = preg_replace('/^(.+?[A-Z][A-Z][A-Z] \d\d? \d\d\d\d)\n(\w)/s', "$1\n\n$2", $data);
		if (!preg_match('/^(.+\d\d\d\d\n)(?:\s*\n|\.\.\.)/s',$data, $m)) {
			$this->tropical_header='';
			return 0;
		}
		$this->tropical_header=$head = $m[1];

		if (preg_match('/^(WT(\w\w)(\d)(\d) +?\w\w\w\w +?\d+)\b$/m', $head, $m)) {
			$this->wmo_header = $m[1];
			$this->NOAAEventNUM = $m[4];
			$this->stormregion = $m[2];
			$type = $m[3];
		}
		else { $type = ''; }


		$name = $num = $product = '';
		if ($type == 2) {
			$product = 'TROPICAL CYCLONE FORECAST';
			if (preg_match('/^(.+) +FORECAST\/ADVISORY +NUMBER +(\d+[A-Z]?)\b/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		elseif ($type ==3) {
			$product = 'TROPICAL CYCLONE PUBLIC ADVISORY';
			if (preg_match('/(^.*?) (?:INTERMEDIATE)? *?ADVISORY NUMBER +(\d+[A-Z]?)\b/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		elseif ($type ==4) {
			$product = 'TROPICAL CYCLONE DICUSSION';
			if (preg_match('/^(.+) DISCUSSION NUMBER +(\d+[A-Z]?) *$/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		elseif ($type ==5) {
			$product = 'TROPICAL CYCLONE POSITION ESTIMATE';
			if (preg_match('/^(.+) TROPICAL CYCLONE POSITION ESTIMATE *$/m', $head, $m)) {
				$name = $m[1];
				$num = 0;
			}
		}
		elseif ($type ==6) {
			$product = 'TROPICAL CYCLONE UPDATE';
			if (preg_match('/^(.+) TROPICAL CYCLONE UPDATE *$/m', $head)) {
				$thisname = $m[1];
			}
			$num = 0;
		}
		elseif ($type ==7) {
			$product = 'TROPICAL CYCLONE STRIKE PROBABILITIES';
			if (preg_match('/^(.+) PROBABILITIES NUMBER +(\d+[A-Z]?)$/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		elseif ($type ==8) {
			$product = 'EXPERIMENTAL TROPICAL CYCLONE VALID TIME EVENT CODE';
			if (preg_match('/^(.+) WATCH\/WARNING BREAKPOINTS\/ADVISORY NUMBER +(\d+[A-Z]?)$/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		elseif ($type ==9) {
			$product = 'WIND SPEED PROBABILITIES';
			if (preg_match('/^(.+) WIND SPEED PROBABILITIES NUMBER +(\d+[A-Z]?)$/m', $head, $m)) {
				$name = $m[1];
				$num = $m[2];
			}
		}
		else {
			$product = 'TROPICAL PRODUCT';
		}

		$this->product = $product;

		$this->storm_name = preg_replace('/ SPECIAL/','',$name);
		$this->advisory_number = $num;
		$this->tropical_product = $product;
		$this->tropical_product_number = $type;

		if (preg_match('/^TROPICAL DEPRESSION/', $name)) {
			$this->event_type = 'TROPICAL DEPRESSION';
		}
		elseif (preg_match('/^SUBTROPICAL STORM/', $name)) {
			$this->event_type = 'TROPICAL STORM';
		}
		elseif(preg_match('/^TROPICAL DEPRESSION/', $name)) {
			$this->event_type = 'TROPICAL DEPRESSION';
		}
		elseif(preg_match('/^HURRICANE/', $name)) {
			$this->event_type = 'HURRICANE';
		}
		else {
			$this->event_type = 'OTHER';
		}

		if (preg_match('/^((?:NATIONAL WEATHER SERVICE |NWS TPC).+)$/m', $head, $m)) {
			$this->weather_service = $m[1];
		}

		$head = preg_replace('/^NOON /m', '1200 ', $head);
		if (preg_match('/^(\d[ \w]+\d\d\d\d)$/m', $head, $m)) {
			$this->release_time = $m[1];
			$this->epoch_date = $this->_extract_epoch_date($m[1], $data);
		}

		if ($this->debug) { print "<pre>$head</pre>";}

	}

	Function read_data (&$data) {
		$this->_parse_header($data);
		$this->_parse_body($data);

		return 1;
	}

	function _format_date ($prev_date, $date, $hour) {
		$pdate = explode('-', $prev_date);
		$month = $pdate[0];
		$pdate = $pdate[1];
		$year = gmdate('Y');
		if ($date < $pdate) $month++;
		if ($month > 12) {
			$month = 1;
			$year++;
		}
		$hour = preg_replace('/00$/', '', $hour);
		$vtime = gmmktime($hour, 0, 0, $month, $date, $year);

		return $vtime;
	}


	function _get_bearing ($lat1, $lon1, $lat2, $lon2, $as_str=true) {
		$lon_diff = deg2rad($lon2 - $lon1);
		$lat1 = deg2rad($lat1);
		$lat2 = deg2rad($lat2);
		$y = sin($lon_diff) * cos($lat2);
		$x = (cos($lat1) * sin($lat2)) - (sin($lat1) * cos($lat2) * cos($lon_diff));
		$bearing = (rad2deg(atan2($y, $x)) + 360) % 360;
		if ($as_str) {
			$code = round($bearing / 22.5);
			switch ($code) {
				case 1:
					return 'NNE';
					break;
				case 2:
					return 'NE';
					break;
				case 3:
					return 'ENE';
					break;
				case 4:
					return 'E';
					break;
				case 5:
					return 'ESE';
					break;
				case 6:
					return 'SE';
					break;
				case 7:
					return 'SSE';
					break;
				case 8:
					return 'S';
					break;
				case 9:
					return 'SSW';
					break;
				case 10:
					return 'SW';
					break;
				case 11:
					return 'WSW';
					break;
				case 12:
					return 'W';
					break;
				case 13:
					return 'WNW';
					break;
				case 14:
					return 'NW';
					break;
				case 15:
					return 'NNW';
					break;
				default:
					return 'N';
			}
		}
		else {
			return $bearing;
		}
	}

	function _speed ($lat1, $lon1, $lat2, $lon2, $hours, $metric=false) {
		$lon_diff = deg2rad($lon2 - $lon1);
		$lat1 = deg2rad($lat1);
		$lat2 = deg2rad($lat2);
		$d = acos( (sin($lat1) * sin($lat2)) + (cos($lat1) * cos($lat2) * cos($lon_diff)) ) * 3963;		// dist in miles
		if ($metric) $d *= 1.609344; // dist in km

		$speed = $d / $hours;	// mph or kmh
		if ($metric) $speed = $speed / 0.539956803;		//kts

		return round($speed);
	}


	Function _extract_epoch_date($release_time, &$data) {

		if (!$release_time) return '';
		$rtime = '';

		if (preg_match('/\s(\w{3})\s+(\d+)\s+(\d{4})$/', $release_time, $m)) {
			$month = $m[1];
			$mday = $m[2];
			$year = $m[3];

			$mon = $this->month_hash[strtoupper($month)]-1;

			if (preg_match('/(\d\d)(\d\d)(?:Z| UTC)/', $release_time,$m)) {
				$hour = $m[1]; $min = $m[2];
				if (preg_match('/\sPM\s\w\w\w/', $release_time) && $hour < 12) {
					$nday = date('m-d-Y', gmmktime($hour, $min, 0, $mon+1, $mday, $year)+86400);
					list($mon, $mday, $year) = explode('-',$nday);
					$mon--;
				}
			}
			elseif (preg_match('/(\d?\d)(\d\d)\s+(AM|PM)\s+(\w+)/', $release_time,$m)) {
				$tzdifs = (isset($this->tzs[$m[4]])) ? $this->tzs[$m[4]] : 0;
				$hour = $m[1];
				$min = $m[2];
				$ampm = $m[3];
				if ($ampm == 'PM' && $hour != 12) $hour += 12;
			}



			$this->release_year = $year;
			$this->release_mon = $mon;
			$this->release_day = $mday;

			//int gmmktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]])

			$rtime = gmmktime($hour, $min, 0, $mon+1, $mday, $year);
		}
		return $rtime;
	}




}


?>
