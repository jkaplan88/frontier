<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HW3PluginAPI.php,v 1.5 2007/09/20 15:36:38 wxfyhw Exp $
*
*/

require_once(HAMLIB_PATH . '/HW3Plugins/FetchWxData.php');


class HW3PluginAPI {
	var $cfg;
	var $var = array();
	var $form = array();
	var $forecast_info = array();
	var $forecast='';

	var $debug = false;
	var $debug_local = false;
	var $core_path ='';

	var $cache= array();

	var $objects = array();

	var $data  = array();
	var $getvalue_mode= 1;  // 0= never use $this->data, 1 = use $this->data if not empty, 2 = only use $this->data

	var $APIVersion = '1.5.1';

	Function HW3PluginAPI($core_path) {
		$this->core_path = $core_path;
	}



	function do_autoload() {
		return null;
	}


	function main (&$var, &$form, $forecast, &$cfg, &$forecast_info, $debug) {
		$this->var =& $var;
		$this->form =& $form;
		$this->forecast = $forecast;
		$this->cfg =& $cfg;
		$this->forecast_info =& $forecast_info;
		$this->debug = $debug;

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;
		return $t;

	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function DebugPrint ($msg, $force = false, $html=true) {

		if ($this->debug || $this->debug_local || $force) {

			if (function_exists('debug_backtrace')) {
				$bt = debug_backtrace();

				// get class, function called by caller of caller of caller
				$class = (isset($bt[1]['class'])) ? $bt[1]['class'] : '';
				$function = (isset($bt[1]['function'])) ? $bt[1]['function'] : '';

				// get file, line where call to caller of caller was made
				$file = (isset($bt[0]['file'])) ? $bt[0]['file'] : '';
				$line = (isset($bt[0]['line'])) ? $bt[0]['line'] : '';

				$from = "$class::$function";
				if ($line) $from .= ":$line";
			}
			else {
				$from ='';
			}

			if ($from) print "$from : ";
			print "$msg\n";
			if ($html) print "<br />";
		}

	}

	Function cleanval($val) {
		return preg_replace('/[^\w ]/','',$val);
	}



	function  _get_db_object (&$cfg, $cgi_path) {

		# lets get database object
		$dbdir = $cfg->val('Database Access', 'flatfilepath');
		$dbbase =$cfg->val('Paths', 'base');  if (!$dbbase) { $dbbase = 'hw3dbs';}
		if ($dbdir == '') { $dbdir = $cgi_path .'/'. $dbbase;}
		if (!preg_match(' /^(?:\/|[A-Za-z]:)/', $dbdir)) {$dbdir = $cgi_path . $dbdir;}

		$db_access = new_dbaccess($cfg->val('Database Access', 'type'), $cfg, $dbdir, $this->debug);
		return $db_access;
	}

	function _switchvars ($str, &$hashes) {

		$val = '';
		if (substr($str,0,3) == 'lc_') { $val = strtolower($this->switchvars(substr($str,3), $hashes)); }
		elseif (substr($str,0,3) == 'uc_') { $val = strtoupper($this->switchvars(substr($str,3), $hashes)); }
		else {
			foreach($hashes as $hash) {
				if (is_array($hash) && isset($hash[$str])) {
					$val = $hash[$str];
					if ($val !='') break;
				}
			}
		}
		if ($val == '') $val = $this->GetValue($str,'');

		return $val;
	}



	Function GetValue($key, $index=NULL) {
		$old_error_reporting = error_reporting(0);
		$val = NULL;

		$bdone= false;
		$get_valuemode = $this->getvalue_mode;
		if ($get_valuemode ==2 || ($get_valuemode ==1 && !empty($this->data))) {
			if (is_numeric($index)) {

				if (isset($this->data[$key])) {

					if(  isset($this->data[$key][$index])) $val = $this->data[$key][$index];
				}

			}
			else {

				if (isset($this->data[$key])) $val = $this->data[$key];
			}

			$bdone = true;
		}

		if (!$bdone) {


		if ($index <> '') {

			if (isset($this->$key)) {
				$tmp =& $this->$key;
				if(isset($tmp[$index])) $val = $tmp[$index];
			}

			foreach ($this->objs as $obj) {
				if (is_null($val) && isset($this->$obj->$key)) {
					$tmp =& $this->$obj->$key;
					if (isset($tmp[$index])) $val = $tmp[$index];
				}
				if (!is_null($val)) last;
			}


		}
		else {
			if (isset($this->$key)) $val = $this->$key;
		}
		if ($val == NULL) $val = $this->do_autoload($key,$index);
		}

		error_reporting($old_error_reporting);


		return $val;
	}



	Function _db_establish($database, $dbhostname, $dbusername, $dbpassword) {
 		$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword,true) or die(mysql_error());
		if ($dbh) {mysql_select_db($database,$dbh);}
		return $dbh;
	}



	Function cache_add($name, &$item) {
		if ($name) $this->cache["$name"] = $item;
	}

	Function &cache_get($name) {
		$null = (isset($this->cache["$name"])) ? $this->cache["$name"] : '';
		return $null;
	}
	Function cache_remove ($name) {
		if (isset($this->cache["$name"])) unset ($this->cache["$name"]);
	}

Function cache_clear() {
	$this->cache = array();
}



}
?>