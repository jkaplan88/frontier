<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: FetchIntWx.php,v 1.2 2006/06/27 11:06:26 wxfyhw Exp $
*
*/


require_once("hamlib/HW3Plugins/FetchWxData.php");
require_once("hamlib/HW3Plugins/FetchSunInfo.php");

class FetchIntWx {

	var $cfg;

	var $found_zone;
	var $tzname;
	var $tzdif;

	var $data = array();
	var $data_nodes = array();
	var $found_intwx;
	var $period_total;
	var $wxdata_total;



	var $core_path ='';

	var $vars = array();
	var $form = array();


	var $month_nums = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');

	var $month_names = array('JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4,'MAY'=>5,'JUN'=>6,
            		      'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10,'NOV'=>11,'DEC'=>12);

	Function FetchIntWx($core_path) {
		$this->core_path = $core_path;
		$this->tzdif=0;
		$this->tzname='';
	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}


	Function do_intwx(&$var, &$form, $forecast, &$cfg, $forecast_info, $debug) {

		$this->cfg = $cfg;

		$error = "";

		list($iwin_file, $ma, $t,$lib,$code,$tcode) = $forecast_info;


		$placeid = (isset($form['placeid'])) ? strtolower(preg_replace('/\W/','',$form['placeid'])) : '';


		if (!$placeid) {
			if (isset($var['alt_zone_info']) && preg_match('/intwx(\d+)/', $var['alt_zone_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['icao']) && $var['icao']) {
				$placeid = $var['icao'];
			}
			elseif (isset($var['alt_zone_info']) && preg_match('/^(\w\w\w\w):/', $var['alt_zone_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['alt_cc_info']) && preg_match('/^(\w\w\w\w):/', $var['alt_cc_info'], $m)) {
				$placeid = $m[1];
			}
			elseif (isset($var['closeby_icao1x']) && preg_match('/^(\w\w\w\w):/', $var['closeby_icao1x'], $m)) {
				$placeid = $m[1];
			}
		}

		$placeid = strtolower($placeid);

		$interval = (isset($form['interval'])) ? strtolower(preg_replace('/\D/','',$form['interval'])) : '';
		if ($interval == '') $interval = 24;
		$form['interval'] = $interval;








		$cache_path =$cfg->val('Paths', 'cache');
		if (!$cache_path) { $cache_path =  'cache'; }
		if (!preg_match('/^(?:\/|[A-Za-z]:)/', $cache_path) ) $cache_path = $this->core_path . $cache_path;


		$cache_file = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'intwx_cache_file'));
		if (!$cache_file) {$cache_file = $cache_path . strtolower("/intwx_$placeid\_$interval"); }

		$cache_file2 = '';
		$prefix = preg_replace("/%%(\w+)%%/e", '$$1', $cfg->Val('URLs', 'intwx_prefix'));

		//Call Fetch Forcast
		$data  =& fetch_forecast($cfg, $ma, $cache_file, $cache_file2,'',
			$cfg->val('URLs', 'intwx_url'),
			$prefix,
			$cfg->val('URLs', 'intwx_postfix'), $cfg->val('SystemSettings', 'proxy_port'), $debug,0);
		list($which_used, $wx_data) = $data;

		$this->found_intwx = $this->parse_intwx($wx_data);


		return $t;
	}


	function parse_intwx(&$xml) {


		if (preg_match('/<location ([^>]+)>/', $xml, $m)) {
			$location = ' ' . $m[0];
			if (preg_match_all('/ (\w+)="([^"]+)"/', $location, $mm, PREG_SET_ORDER)) {
				foreach ($mm as $m) {
					$this->data['wxdata_' . $m[1]] = $m[2];
				}
			}
		}

		$total=0;
		if (preg_match_all('/<period>(.+?)<\/period>/s', $xml, $mm, PREG_SET_ORDER)) {
			foreach ($mm as $m) {
				$period_data = array();
				if (preg_match_all('/<(\w[^>]+)>([^<]+)</', $m[1], $pmm, PREG_SET_ORDER)) {
					foreach ($pmm as $pm) {
						$period_data[$pm[1]] = $pm[2];
//						print "pm[1]=" . $pm[1] . " = " . $pm[2] . "<br>\n";
					}
					array_push($this->data_nodes, $period_data);
					++$total;
				}
			}
		}
		$this->data['wxdata_total']=$total;

//		print "total=$total<br>\n";

		return ($total) ? 1 : 0;


	}


	Function GetValue($key, $index) {
		$old_error_reporting = error_reporting(0);
		$val = NULL;

		if ($index != '') {

			if (preg_match('/^wxdata_(.+)$/', $key,$m)) {
				if (isset($this->data_nodes[$index]) && isset($this->data_nodes[$index][$m[1]])) {
					$val = $this->data_nodes[$index][$m[1]];
				}
				elseif (isset($this->$key)) {
					$tmp =& $this->$key;
					if(isset($tmp[$index])) $val = $tmp[$index];
				}
			}
		}
		else {
			if (isset($this->data[$key])) $val = $this->data[$key];
		}

		error_reporting($old_error_reporting);

		return $val;
	}





	Function dayornight ($epoch) {

		if ($epoch && isset($this->data['wxdata_lat']) && isset($this->data['wxdata_lon'])) {
			$this->vars['lat'] = $this->data['wxdata_lat'];
			$this->vars['lon'] = $this->data['wxdata_lon'];
			$tz = $this->vars['tz'] = $this->data['wxdata_tz'];

			$astro = new FetchSunInfo('');
			$astro->set_sunrise_params($this->vars, $this->form, '', $this->cfg, array('','',''), $this->debug);

			$eday = sprintf('%04u', str_replace(':', '', $astro->sunset(0)));
			$bday = sprintf('%04u', str_replace(':', '', $astro->sunrise(0)));
			$hour = gmdate('Hi', $epoch + 3600*$tz);
//print "bday=$bday...eday=$eday...hour=$hour<br>\n";
		}
		else {
			return 1;
		}


		if (($bday < $eday && $hour >= $bday && $hour < $eday) ) {
			return 1; //day time
		}
		else {
			return 0; // night time
		}
	}

	function period_wx($day) {

		$wx = $this->data_nodes[$day]['wx'];
		$wx_intensity = $this->data_nodes[$day]['wx_intensity'];
		$clouds_eng = $this->data_nodes[$day]['clouds_eng'];

		$vepoch = $this->data_nodes[$day]['validepoch_gmt'];
		$dayornight = $this->dayornight($vepoch);

		$twx_info='';
		$wx_info= $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx");
		if (!$wx_info) {
			$twx_info= $this->cfg->val('IntWxInfo', "$wx_intensity");
			$wx_info = $this->cfg->val('IntWxInfo', "$wx");
			if ($twx_info && $wx_info) $wxinfo .= ' ';
		}
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng");
		if (!$wx_info) $wx_info = $wx;

		$tdata = preg_split('/\|/', $wx_info);
		if ($dayornight || !isset($tdata[3])) {
			return $twx_info . $tdata[0];
		}
		else {
			return $twx_info . $tdata[3];
		}
	}



	function period_icon($day) {

		$wx = $this->data_nodes[$day]['wx'];
		$wx_intensity = $this->data_nodes[$day]['wx_intensity'];
		$clouds_eng = $this->data_nodes[$day]['clouds_eng'];

		$vepoch = $this->data_nodes[$day]['validepoch_gmt'];
		$dayornight = $this->dayornight($vepoch);

		$twx_info='';
		$wx_info= $this->cfg->val('IntWxInfo', "$clouds_eng$wx_intensity$wx");
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$wx_intensity$wx");
		if (!$wx_info) {
			$wx_info = $this->cfg->val('IntWxInfo', "$wx");
		}
		if (!$wx_info) $wx_info = $this->cfg->val('IntWxInfo', "$clouds_eng");
		if (!$wx_info) $wx_info = $wx;

		$tdata = preg_split('/\|/', $wx_info);
		if ($dayornight || !isset($tdata[2])) {
			return  $tdata[1];
		}
		else {
			return $tdata[2];
		}
	}





}

?>