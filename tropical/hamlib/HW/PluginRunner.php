<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: PluginRunner.php,v 1.25 2006/05/07 17:55:45 wxfyhw Exp $
*
*/




class PluginRunner {

	var $core_path;
	var $cfg = NULL;
	var $var;
	var $form ;
	var $debug=0;
	var $hw_cgi_path = '';
	var $placeparser;
	var $dbaccess;
	var $iniext = '.ini.php';

	Function PluginRunner( $core_path, &$form, &$var, &$cfg, $hw_cgi_path, $iniext, $debug, &$placeparser, &$dbaccess) {

		$this->core_path = $core_path;
		$this->cfg =& $cfg;
		$this->var =& $var;
		$this->form =& $form;
		$this->debug = $debug;
		$this->hw_cgi_path = $hw_cgi_path;
		$this->placeparser=& $placeparser;
		$this->dbaccess=& $dbaccess;
		$this->iniext = $iniext;

	}




	Function RunHWPlugin($allow_mult, $allow_process_place=1) {

		$return_array = array();
		$hash = array();
		$forecast = "";
		$pp = $this->placeparser;

		//Set the forecast from querystring or ini file
		if ($this->form["forecast"] != "") { $forecast = $this->form["forecast"];}
		else { $forecast = $this->cfg->val("Defaults", "forecast");}
		$forecast = trim(strtolower($forecast));

		//Grab the forecast info from the ini file
		$forecast_info = $this->cfg->val("ForecastTypes", $forecast);
		$forecast_info_array = explode(",", $forecast_info);

		//Call process place if we can
		if ($allow_process_place && $forecast_info_array[5] == "1") {
			$pp->process_place($this->dbaccess);

			if ($this->debug) {
				print "<hr><br><h2>After parse Place</h2><br>FORM: <pre>\n";
				var_dump($this->form);
				print "<br>Var: ";
				var_dump($this->var);
				print "</pre><br>\n";
			}




			//If a bad place came back handle that
			$bt = $this->cfg->val("Defaults", "bad_place_template");
			if ($bt && isset($this->var["BADPLACE"]) && $this->var["BADPLACE"] && (!isset($this->form["dbp"]))) {
				$return_array[0] = $bt;
				$return_array[1] = array();
				return $return_array;
			}

      		$intwxcanada = preg_replace('/\W/','',$this->cfg->val('Defaults', 'intwx_canada'));
      		$country = (isset($this->var["country"])) ? strtolower(trim($this->var["country"])) : '';

			if (isset($this->var["country"]) && preg_match("/^z(?:one|andh)$/", $forecast) && ($country != 'us'  &&($country != 'ca' || ($country == 'ca' && $intwxcanada )))) {
				$ic = strtolower($this->cfg->val('Defaults', 'intwx_countries'));

				$forecast = (!$ic || strpos($ic,$this->var["country"]) !== false) ?  preg_replace('/\W/','',$this->cfg->val('Defaults', 'intwx')) : '';
				if (!$forecast) $forecast = "tafzone";

				if (! preg_match('/\|/',$this->cfg->val('ForecastTypes', $forecast))) {
					$this->cfg->ReadINI ( "fc_$forecast");
				}


				$forecast_info = $this->cfg->val("ForecastTypes", $forecast);
				$forecast_info_array = explode(",", $forecast_info);
			}
			if (isset($this->var["dopass"]) && $allow_mult) {
				$return_array[0] = $this->cfg->val('PassTemplates', $this->var["mult_places_pass"]);
				$return_array[1] = array();
				return $return_array;
			}
		}

		//Load in the library file
		$this->var["expiration"] = $forecast_info_array[1];
		$lib = $forecast_info_array[3];
		$code = $forecast_info_array[4];
		$tcode = $forecast_info_array[5];

		require_once(HAMLIB_PATH . "HW3Plugins/".$lib.".php");
		$wx_lib = new $lib($this->core_path);
		$return_array[0] = $wx_lib->$code($this->var, $this->form, $forecast, $this->cfg, $forecast_info_array, $this->debug);
		$return_array[1] = $wx_lib;
		return $return_array;
	}


Function DoAnotherPlugin($args_string, $allow_mult, &$ref_hashes) {
	$targs = array();
	$args = array();
	$config_arr = array();

	$targs = explode(' ',trim($args_string));

	foreach ($targs as $value) {
		$value = str_replace('+', ' ', $value);
		$eqpos = strpos($value,'=');
		if ($eqpos) {
			$name=substr($value,0,$eqpos);
			if ($name) {
				$args[$name] = substr($value,$eqpos+1);
			}
		}
	}

	process_input($args,$this->var,$this->cfg, 0, $this->debug);

	$configs= $this->form['config'];
	$config_arr = explode(',', $args['config']);
	foreach ($config_arr as $key=>$value) {
		if ($value && !strstr($configs, $value)) {
			$this->cfg->ReadINI($value.'.'.$this->iniext);
		}
	}

	if (isset($args['forecast'])) { $tforecast=$args['forecast']; }
	elseif (isset($args['do'])) { $tforecast=$args['do']; }
	else { $tforecast='';}
	$tforecast = preg_replace('/\W/', '', $tforecast);

	if (!isset($args['nofc']) || !$args['nofc']) {
		if ($tforecast) { $this->cfg->ReadINI("fc_$tforecast.".$this->iniext); }
	}



	$mode = 0;
	if ($tforecast == 'dpp') { $mode = 1; }
	elseif (isset($args['mode'])) { $mode = $args['mode'];}
	elseif (isset($args['dpp'])) { $mode = $args['dpp']; }
	if ($mode) {
		$this->form['pands']='';
		$this->form['zipcode']='';
		$this->form['place']='';
		$this->form['state']='';
		$this->form['country']='';
		$this->form['fips']='';
		$this->form['icao']='';
		$this->form['county']='';

		$this->var['BADPLACE'] = '';

		foreach ($this->var as $key=>$value) {
			if (substr($key, 0, 9) == 'close_icao') { unset($this->var[$key]);}
		}

	}

	foreach ($args as $key=>$value) {

		$this->form[$key] = $value;
	}

	$this->load_hwv($this->cfg, $this->var, $this->form);

	if ($tforecast != 'dpp') {
		list($tt, $wx_object) = $this->RunHWPlugin($allow_mult,$mode);
		$dpf = $args['dpf'];
	//if ($mode=1) var_dump($ref_hashes);
		if ($dpf == 'all') {
			$ref_hashes = array($ref_hashes[0],$ref_hashes[1]);
		}
		elseif ($dpf > 0) {
			while ($dpf > 0) { array_pop ($ref_hashes); $dpf--; }
		}
		elseif ($dpf < 0) {
			while ($dpf < 0) { array_shift($ref_hashes); $dpf++;}
		}
	//if ($mode=1) var_dump($ref_hashes);
		array_push($ref_hashes, $wx_object);
	}
	else {
		$pp = $this->placeparser;
		$pp->process_place($this->dbaccess);

		if ($this->debug) {
			print "<hr><br><h2>After parse Place</h2><br>FORM: <pre>\n";
			var_dump($this->form);
			print "<br>Var: ";
			var_dump($this->var);
			print "</pre><br>\n";
		}

	}


}

function load_hwv(&$cfg, &$var, &$form, $mode=0){
	$hwvs = $cfg->SectionParams('HWV Presets');
	if ($hwvs != null) {
		foreach ($hwvs as $name=>$value) {
//print "DEBUG: $name=$value<br>\n";
			if (!preg_match('/^s?hwv/', $name)) $name = 'hwv' . $name;
			if (substr($name,0,1) == 's') {
				$var[substr($name,1)] = $value;
			}
			elseif(!isset($var[$name]) && !isset($form[$name])) {
				$var[$name] = $value;
			}
		}
	}
//print "DEBUG: var[hwvtest]=$var[hwvtest]<br>\n";
}







}




?>