<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: ParsePlace.php,v 1.28 2006/06/27 11:06:26 wxfyhw Exp $
*
*/

class ParsePlace {

	var $cfg;
	var $var = array();
	var $form = array();
	var $debug=0;
	var $hw_cgi_path = '';

	Function ParsePlace( &$form, &$var, &$cfg, $hw_cgi_path, $debug) {

		$this->cfg =& $cfg;
		$this->var =& $var;
		$this->form =& $form;
		$this->debug = $debug;
		$this->hw_cgi_path = $hw_cgi_path;

	}



Function process_place( &$db_access) {
	//Make $this->form, $this->var, and $this->cfg available in this scope

	$debug= $this->debug;
	$this->var['BADPLACE']=0;

	$matches = array();
	$full_state = '';
	$lat = $lon = $elev = '';
	$tzdif = $tzname = '';
	$full_country = '';
	$closeby = '';

	$tcounty = $tcwa = $tzipcode = $tzone = $airportname =
		$telev = $tlat= $tlon = $ttzdif = $ttzname = $alt_zone_info = $alt_cc_info = '';

	$pandseqicao =0;
	$used_icao_place = 0;
	$used_airport_place =0;
	$used_zone=0;

	//Set vars from the form hash
	//Must check for existance otherwise php will blow warnings all over the place
	$forecast = getKeyVal($this->form, 'forecast', '');
	$zipcode = getKeyVal($this->form,'zipcode', '');
	$pands = getKeyVal($this->form,'pands','');
	$place  = getKeyVal($this->form,'place','');
	$state = getKeyVal($this->form,'state', '');

	$icao = getKeyVal($this->form,'icao','');
	$airport = getKeyVal($this->form, 'airport', '');
	$zone = getKeyVal($this->form,'zone','');

	$fips = getKeyVal($this->form,'fips','');
	$county = getKeyVal($this->form,'county','');
	if (!$fips && preg_match('/^\d\d\d\d\d$/', $county)) $fips = $county;

	$radaricao = getKeyVal($this->form,'radaricao','');

	$country = getKeyVal($this->form,'country','');
	$country = preg_replace("/[^A-Za-z]/", "", $country);
	if(!$country) $country = $this->cfg->Val("Defaults", "country");
	$country = strtolower($country);
	$db_access->SetCountry($country);



	$zipcode = trim($zipcode);
	if ($zipcode != '')  $pands = $zipcode;
	$pands = trim($pands);
//echo "zipcode=$zipcode<br>\n";
	if ($zipcode || preg_match('/^(\d\d\d\d\d(?:[- +]*\d\d\d\d)?)|(\w\d\w\d(\w\d)+)$/', $pands)) {
		//Perform some data cleaning
		$pands = preg_replace('/\W/', '', $pands);
		$zipcode = $pands;
		if (preg_match('/^(\w\d\w)\d\w\d$/', $zipcode, $m)) {
			$zipcode = $m[1];
			$country='ca';
			$db_access->SetCountry('ca');
		}
		elseif ($country == "us" && strlen($zipcode) > 5) {
			$zipcode = substr($zipcode,0,5);
		}

		//Query the DB for the zipcode info
		if ($debug) print "fetching zipcode info for $zipcode<br>\n";
		$zip_array = $db_access->get_zipcode_info($zipcode);
if ($debug) {print "<pre>"; var_dump($zip_array); print "</pre>\n";}
	          if ($zip_array[0])  {
	                	$zipcode = $zip_array[0];
	      		$place = $zip_array[1];
	      		$state = $zip_array[2];
	      		$country = $zip_array[3];
	      		$db_access->SetCountry($country);
	      		if ($debug) print "Got zipcode: zipcode=$zipcode<br>\nplace=$place<br>\nstate=$state<br>\n";
	      	}
	      	else {
	      		$place = $state = $country = '';
	      	}

//print "zipcode=$zipcode<br>place=$place<br>\n";
	}
	elseif (preg_match('/^(.+),(.+),\s*(.+)$/', $pands, $m)) {
		$place = $m[1];
		$state = $m[2];
		$country = $m[3];
		$db_access->SetCountry($country);
	}
	elseif (preg_match('/^(.+?)(?:,(.+)|[, ]+([A-Za-z][A-Za-z]))$/', $pands, $m)) {
		$place = $m[1];
		if ($m[2]!= '') {$sorc = trim($m[2]);}
		else {	$sorc = trim($m[3]);}

		// this couldbe "place, state/province" or it could be "city, country"
		// check US state
		list($tabbrev,$tname) = $db_access->match_state($sorc,'us');
		if ($tabbrev) {
			$country = 'us';
			$state = $tabbrev;
			$db_access->SetCountry($country);
		}
		else {
			//check CA province
			list($tabbrev,$tname) = $db_access->match_state($sorc,'ca');
			if ($tabbrev) {
				$country = 'ca';
				$state = $tabbrev;
				$db_access->SetCountry($country);
			}
			else {
				//lets assume its a country
				$state = '';

				// lets try and get the country abbrev
				list ($tcountry, $tfull_country) = $db_access->match_country($sorc);

				if ($tcountry) {
					$country = strtolower($tcountry);
					$full_country = strtolower($tfull_country);
				}
				else { $country = $sorc;}

				$db_access->SetCountry($country);
			}
		}

	}
	elseif(preg_match('/^([A-Za-z][A-Za-z])([zZ]?)(\d+)$/', $pands, $m)) {

		list($ts, $tfs) = $db_access->match_state($m[1]);
		if ($tfs) { $place = ''; $state=$m[1]; $zone = $pands;}
		elseif (!$icao) { $icao = $pands;}
	}
	elseif ($pands) {
		$t_country = '';
		//If Washington DC
		if (preg_match("/\bd\.?c\.?/", $pands)) {
			$place = 'washington';
			$state ='dc';
			$t_country='us';
			$zone=1;
		}
		else {
			//Check the cities DB for the city name
			list($total, $ta) = $db_access->check_for_city($pands);
			if ($total > 0) {
				$place = $ta[0];
				$state = $ta[1];
				$t_country = $ta[2];
			}
			else {
				$place = ''; $state = ''; $t_country = '';
			}

			//If multiple cities found return the list
			if ($total > 1 && $this->cfg->Val('Defaults', 'pass_template_if_multiple') ) {
				$this->var['dopass'] = 1;
				$this->var['mult_places_pass'] = $this->cfg->val('Defaults', 'pass_template_if_multiple');
				$this->var['mult_places'] = $ta;
				$this->var['mult_place'] = array($place, $state, $t_country);
				$this->var['mult_places_total'] = $total;

				for ($i =0; $i < $total; $i++) {
					if (strtolower($this->var['mult_places'][$i*3+1]) == 'la')
						$this->var['mult_places'][$i*3] = preg_replace('/( parish| county)+/i', ' parish', $this->var['mult_places'][$i*3]);

				}
				return;
			}
		}

		if ($place) {
			$country = $t_country;
			$db_access->SetCountry($country);
		}
		else {
			list($state, $full_state) = $db_access->match_state($pands);
			if ($state) {
				$place = '';
				//$forecast = 'state';
			}
			else {
				list($tcountry, $tfull_country) = $db_access->match_country($pands);
				if ($tcountry) {
					$country = $tcountry;
					$full_country = $tfull_country;
					$place ='';
					//$forecast = 'country';
					$db_access->SetCountry($country);
				}
				elseif (preg_match('/^\w\w\w\w+$/', $pands)) {
					if ($debug) print "using ICAO $pands<br>\n";
					$icao=$pands;
				}
				elseif (preg_match('/^\w\w\w$/', $pands)) {
					if ($debug) print "using AIRPORT $pands<br>\n";
					$airport  = $pands;
				}
				else {
					$this->var['BADPLACE']=1;

				}

			}
		}
	}
	elseif (preg_match("/^\d\d\d\d\d$/", $fips) && (strtolower($country) == "us" || !$country) && (!$place && !$state)) {
		//Search the fips DB for the fips data
		$ta = $db_access->get_place_from_fips($fips);
		$place = $ta[1];
		$state = $ta[5];
		$zone = $ta[4];
		$lon = $ta[2];
		$lat = $ta[3];
		$county = $fips;
		//Attach ' county' on the end if it's a county
		if (!preg_match("/\scounty$/", $place)) $place .= " county";
		$this->form["dbp"] = 1;
	}
	elseif($pands) { $this->var['BADPLACE'] = 1;}

	$place = strtolower($place);

	//Check for washington DC
	if ($country == 'us' && (preg_match('/^(?:washington\s+)?d\.?c\.?/', $place) || $place == 'washington' && preg_match('/d\.?c\.?/', $state))) {
		$place = 'washington';
      		$state = 'dc';
	}

	if (($forecast == 'metar' || $forecast == 'tafzone') && !$icao) {
		$icao = $this->cfg->Val('Defaults', 'icao');
	}
	if ($airport) {
		$ta = $db_access->get_airport_data($airport);
		if (is_array($ta)) {
			list($airportname,$place,$state,$country,$lat,$lon,$elev,$icao,$zone,$county,$radaricao,$tzdif) = $ta;
			if ($airportname && $place) {
				if (isset($ta[12])) $tzname = $ta[12];
				if (!$tzname) $tzname = "GMT".$tzdif;
         			$this->form["dbp"] = 1;
         			$used_airport_place = 1;
         		}
		}
	}


	if (!$state && isset($this->form['state'])) $state = $this->form['state'];
	$state = strtolower(trim($state));
	$used_default_state = 0;

	if (!$state && !$icao && !$airport && strtoupper($country) == strtoupper($this->cfg->val('Defaults', 'country')) && !$this->var['BADPLACE'] ) {
		$state = $this->cfg->val('Defaults', 'state') ;
		$used_default_state = 1;
	}

	if (!$full_state && $state) {
		list($state, $full_state) = $db_access->match_state($state);
	}

	$zone = strtoupper($zone);
	if (preg_match('/^(?:([A-Z][A-Z])(Z?))?(\d+)$/', $zone,$m)) {
		$tstate = strtoupper($m[1]); $tzone=$m[3];
		if (!$tstate) {
			if ($state && !$used_default_state) {
				$zone = strtoupper($state).'Z'.sprintf('%03u',$tzone);
				$tstate = strtoupper($state);
			}
			else { $zone =''; }
		}
		elseif($m[2]) {

			$zone = $tstate .'Z'. sprintf('%03u', $tzone);
			if (strtoupper($state) != $tstate) {
				$state = $tstate;
				$used_default_state = 0;
				list($state, $full_state) = $db_access->match_state($state);
			}
		}
		if (!$place) {
			$ta = $db_access->get_place_from_zone($zone, $tstate);
			if (is_array($ta)) {
				list(,$place, $state, $lon, $lat, $tzdif) = $ta;
				$used_zone=1;
			}
			elseif ($debug) { print "Error fetching place from zone: $ta<br>\n"; }
		}
	}




	if (!$place && !$icao && ($used_default_state) && !$this->var['BADPLACE'] ) {
		$place = $this->cfg->Val('Defaults', 'place');
	}

	$place = strtolower(trim($place));
	//Convert any prefixes


	$found = 0;
	if ($place) {
//		print "place,state=$place,$state<br>\n";
		$place = preg_replace(array('/^mac +/','/^mc(\w)/','/^ft\.? /','/^mt\.? /','/^st(e?).? /'),
					array('mac', 'mc \1', 'fort ', 'mount ', 'saint\1 '), $place);
		$ta = $db_access->check_for_alt_place($state, $place);
		if (is_array($ta)) {
			//Need to verify order of returned information
			list(,$alt_zone_info,$alt_cc_info, $tzone, $tcounty, $tcwa,
				$tzipcode, $tlat, $tlon,  $telev, $ttzname, $ttzdif,
	            		$radaricao) = $ta;

			$iscounty = (isset($ta[13])) ? $ta[13] : '';
			$this->var['extra_placeType'] = (isset($ta[14])) ? $ta[14] : '';
			$this->var['extra_address'] = (isset($ta[15])) ? $ta[15] : '';
			$this->var['extra_city'] = (isset($ta[16])) ? $ta[16] : '';
			$this->var['extra_zipcode'] = (isset($ta[17])) ? $ta[17] : '';
			$this->var['extra_subType'] = (isset($ta[18])) ? $ta[18] : '';
			$this->var['extra_other'] = (isset($ta[19])) ? $ta[19] : '';
			$this->var['extra_url'] = (isset($ta[20])) ? $ta[20] : '';
			$this->var['extra_phone'] = (isset($ta[21])) ? $ta[21] : '';


			$found=1;
		}
	}
	if ($place && $found) {
		if ($iscounty  && !preg_match( '/\bcounty\b/i', $place)) { $place .= ' County';}
		if (strtolower($state) == 'la') $place = preg_replace('/( parish| county)+/i', ' parish', $place);
		if ($debug) print "alt_zone_info=$alt_zone_info<br>\n";

	}
	elseif ($used_zone) {
		$alt_zone_info =  $zone.':2';
	}
	elseif ($used_airport_place) {
		$alt_zone_info = ($zone) ? $zone.':2' : $icao.':4';
		$alt_cc_info = $icao.':2';
		$tzone = $tcounty = $tzipcode =  $closeby = '';
	}
	elseif ($icao) {
		//Get information from icao database
		$ta = $db_access->get_icao_data($icao);
		if (is_array($ta)) {
			list(,$place,$state, $full_country, $country, $tzdif,$lat,$lon,$elev) =$ta;
			if (isset($ta[9])) $tzname = $ta[9];
			if (!$tzname) $tzname = "GMT".$tzdif;

			if (isset($ta[10])) $zone = $ta[10];
			if (isset($ta[11])) $county = $ta[11];
			if (isset($ta[12])) $radaricao = $ta[12];


         		$this->form["dbp"] = 1;
         		$used_icao_place = 1;
			$alt_zone_info = ($zone) ? $zone.':2' : $icao.':4';
			$alt_cc_info = $icao.':2';
		}
		else {
			if ($debug) print "Error get_icao_data for $icao : $ta<br>\n";
			$this->var["BADPLACE"] = 1;
		}
	}
   	elseif (!isset($this->form['dbp']) || !$this->form['dbp']) {
      	$this->var['BADPLACE']=1;
   	}

   	if ($zone) { $alt_zone_info = "$zone:2%$alt_zone_info";}
   	else { $zone = $tzone;}
	if ($debug) print "zone=$zone...tzone=$tzone<br>\n";


   	if (!$this->var['BADPLACE'] && !$full_country) {
   		list(,$full_country) = $db_access->match_country($country);
   	}
   	if (!$zipcode) $zipcode = $tzipcode;

   	if (!$county) $county = $tcounty;
   	if ($lat == '' || $lon == ''){$lat = $tlat; $lon = $tlon;}
   	if (!$elev) $elev = $telev;
   	if ($tzdif == '' && !$tzname) {$tzdif = $ttzdif; $tzname = $ttzname;}

//   	//Not sure if this is right
//   	$this->var["closeby_total"] = explode(":", $closeby);


	if (isset($form['tzdif'])) {
		$tzdif =$form['tzdif'];
		$tzname = (isset($form['tzname'])) ? $form['tzname'] : '';
	}
   	//Daylight Savings time
 	if ($this->cfg->val('SystemSettings', 'use_system_DST')) { $isdst = date('I', time()); }
 	else { $isdst= $this->cfg->val('SystemSettings', 'DST'); }
   	if ($tzname != '' && $isdst && is_int(strpos($this->cfg->val('SystemSettings', 'TZnames_with_DST'), strtoupper($tzname)))) {
      		if (!preg_match("/$state/i", $this->cfg->val('SystemSettings', 'DST_not_used_in_states'))) {
	      		if ($tzdif <0) {$tzdif++; }	else { $tzdif--;}
	      		$tzname = str_replace("S", "D", strtoupper($tzname));
	      	}
   	}
   	elseif( !$tzname && $tzdif != '') {
   		if ($tzdif>=0) {$tzname = 'GMT+'.($tzdif+0);}
   		else {$tzname = "GMT$tzdif"; }
   	}

	$icaototal=0;
	if (preg_match_all('/\b(\w{4,}):([23])\b/', $alt_cc_info, $m)) {
		for ($i = 0; $i < sizeof($m[0]); $i++) {
			$ticao = $m[1][$i];
			$tcode = $m[2][$i];
			if ($tcode > 1) {
				$icaototal++;
				$this->var['close_icao' . $icaototal . 'x'] = $ticao;
				$this->var['close_icao_code' . $icaototal . 'x'] = $tcode;
			}
		}
	}
	$this->var['close_icao_total'] = $icaototal;


   	if (!$country) $country = "us";
//print "state=$state<br>\n";
   	$this->var["forecast"] = $forecast;
   	$this->var["zipcode"] = $zipcode;
   	$this->var["place"] = $place;
   	$this->var["state"] = $state;
    	$this->var["full_state"] = $full_state;
   	$this->var["country"] = $country;
   	$this->var["icao"] = $icao;
   	$this->var["zone"] = $zone;
   	$this->var["county"] = $county;
   	$this->var["alt_zone_info"] = $alt_zone_info;
   	$this->var["alt_cc_info"] = $alt_cc_info;
   	$this->var["lat"] = $lat;
   	$this->var["lon"] = $lon;
   	$this->var["elev"] = $elev;
   	$this->var["tzname"] = $tzname;
   	$this->var["tzdif"] = $tzdif;
   	$this->var["cwa"] = $tcwa;
   	if ($fips) { $this->var["fips"] = $fips;} else { $this->var["fips"] = $county; }
   	$this->var["radar_icao"] = $radaricao;
   	$this->var["full_country"] = $full_country;
   	$this->var['used_icao'] = $used_icao_place;
   	$this->var['used_airport'] = $used_airport_place;
   	$this->var['airportname'] = $airportname;


}

}
?>