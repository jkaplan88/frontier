<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Text.php,v 1.4 2005/06/02 12:52:01 wxfyhw Exp $
*
*/


class HWimageText {

	var $version = '$Id: Text.php,v 1.4 2005/06/02 12:52:01 wxfyhw Exp $';
	var $GDVersion=1;
	var $debug=0;

	Function HWimageText ($gdversion=1.6, $debug=0) {
		$this->GDVersion=$gdversion;
		$this->debug = $debug;
	}

	Function do_text(&$image, $x,$y,$text, $halign='',$valign='', &$chest) {
		$error ='';
		$bounds = array(imagesx($image), imagesy($image));

		if (strtolower($x) == 'center') {
			if (!$halign) $halign="CENTER";
			$x = floor($bounds[0]/2);
		}
		if (strtolower($y) == 'center') {
			if (!$valign) $valign="CENTER";
			$y = floor($bounds[1]/2);
		}

		if (!is_numeric($x)) {
			$error = "X coordinate '$x' is not an integer number or the word 'center' ";
		}
		elseif (!is_numeric($y)) {
			$error = "Y coordinate '$y' is not an integer number or the word 'center' ";
		}
		else {
			$halign= ('$halign') ? strtoupper($halign) : 'LEFT';
			$valign= ('$valign') ? strtoupper($valign) : 'TOP';

			if (!isset($chest['TTF_textangle']) || $chest['TTF_textangle'] == '') $chest['TTF_textangle'] = 0;

			$is_TTF = (isset($chest['is_TTF'])) ? $chest['is_TTF'] : 0;
			$lines = $this->_break_text_into_lines ($text, $halign, $chest, $is_TTF);
			if (strtoupper(substr($halign,0,9)) == 'SPACEWRAP') $halign = substr($halign,9);


			foreach ($lines as $line) {
				$line = trim($line);
				if ($line != '') {
					list ($w,$h,$b) = $this->_get_width_height($line, $chest, $is_TTF);
					list ($ux,$uy) =  $this->_do_text_justification($bounds[0], $x,$y, $w,$h, $b, $halign, $valign, $is_TTF);

					if ($is_TTF) {
						//imagettftext ( resource image, int size, int angle, int x, int y, int color, string fontfile, string text)
						imagettftext($image, $chest['TTF_textpt'],$chest['TTF_textangle'],$ux,$uy,$chest['textcolor'],$chest['TTF_textfont'],$line);
					}
					else {
						//int imagestring ( resource image, int font, int x, int y, string s, int col)
						imagestring($image,$chest['textfont'],$ux,$uy,$line,$chest['textcolor']);
					}
					$y+=($h+1);
				}
			}
		}


		return $error;
	}



	Function _get_width_height($text, $chest, $is_ttf=0) {
		$w = $h = $b = 0;

		if ($is_ttf) {
			$bounds = imagettfbbox ( $chest['TTF_textpt'], $chest['TTF_textangle'], $chest['TTF_textfont'], $text);
			$top = ($bounds[5]< $bounds[7]) ? $bounds[5] : $bounds[7];
			$bottom = ($bounds[1]>$bounds[3]) ? $bounds[1] : $bounds[3];
			$left = ($bounds[0]<$bounds[2]) ? $bounds[0] : $bounds[2];
			$right = ($bounds[4]>$bounds[6]) ? $bounds[4] : $bounds[6];
			$b = ($bottom > 0) ? $bottom+1 : 0;
			$w = abs($left-$right);
			$h = abs($top-$bottom)+1;
			//if (preg_match('/[ygj,qp]/', $text)) { $h += 5;}
		}
		else {
			$w  = $chest['textwidth'] * strlen($text);
			$h = $chest['textheight'];
		}

		return array($w,$h,$b);
	}


	Function _do_text_justification ( $max_width, $x, $y, $w,$h,$b, $halign='', $valign='', $is_ttf=0) {

		if (strtolower($x) == 'center') {
			$x = floor(($max_width - $w)/2);
		}
		elseif ($halign == 'CENTER') {
			$x -= floor($w/2);
		}
		elseif ($halign == 'RIGHT') {
			$x -= $w;
		}
		// other wise assume lect is just x

		$m = ($is_ttf) ? -1 : 1;

		if ($is_ttf) {
			if ($valign == 'CENTER') {
				$y = $y - floor($h/2);
			}
			elseif ($valign == 'TOP') {
				$y += $h;
			}
			elseif ($valign == 'BASE' || $valign == 'BOTTOM') {
				$y -= $b;
			}
		}
		else {
			if ($valign == 'CENTER') {
				$y -= floor($h/2);
			}
			elseif ($valign != 'TOP') {
				$y -= $h;
			}
		}

		return array ($x,$y);
	}



	Function _break_text_into_lines($text, $halign, &$chest, $is_ttf=0) {
		$textwrap = $chest['textwrap'];

		$lines = array();

		$text = preg_replace(array('/\\n/', '/^\s+/'), array("\n",''), $text);

		$halign = strtoupper($halign);
		if (substr($halign,0,9) == 'SPACEWRAP') {
			$lines = preg_split('/\s+/', $text);
		}
		elseif ($textwrap) {
			$textlines = preg_split('/\n+/', $text);
			foreach ($textlines as $line) {
				$line = trim($line);
				if ($line) {
					$words = preg_split('/\s+/', $line);
					$num_words = count($words);
					$i =0; $phrase=''; $lw=0;

					while ($i < $num_words) {
						$word = $words[$i];
						list ($w,$h,$b) = $this->_get_width_height($word . ' ', $chest, $is_ttf);
						if ($w <= $textwrap) {
							if ($lw +$w <= $textwrap) {
								$phrase .= $word.' ';
								$lw +=$w;
							}
							else {
								if ($phrase) array_push($lines, $phrase);
								$phrase = $word .' ';
								$lw = $w;
							}
						}
						else {
							if ($phrase) array_push($lines, $phrase);
							$max_letters = floor($textwrap / $w * strlen($word) -1);
							if ($max_letters < 1) $max_letters =1;
							while (strlen($word) >= $max_letters) {
								array_push($lines, substr($word, 0, $max_letters));
								$word = substr($word, $max_letters);
							}
							if ($word != '') {
								$phrase = $word . ' ';
								list ($w,$h,$b) = $this->_get_width_height($word . ' ', $chest, $is_ttf);
							}
						}
						$i++;
					}
					if ($phrase) array_push($lines, $phrase);
				}
			}
		}
		else {
			$lines = preg_split('/\n+/', $text);
		}

		return $lines;
	}



}
?>