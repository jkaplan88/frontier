<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Image.php,v 1.16 2007/09/13 21:08:54 wxfyhw Exp $
*
*/

define ('pi2', 2 * pi());
define ('DR', pi2/360);
define ('RD', 360/pi2);
define ('max_int', 32600);

define ('gdTinyFont',1);
define ('gdSmallFont',2);
define ('gdMediumBoldFont',3);
define ('gdLargeFont',4);
define ('gdGiantFont',5);



require_once (HAMLIB_PATH . 'HW/HW3image/Color.php');
require_once (HAMLIB_PATH . 'HW/HW3image/Text.php');
require_once (HAMLIB_PATH . 'HW/HW3image/GDVersion.php');
require_once (HAMLIB_PATH . 'HW/HW3image/LoadImage.php');
require_once (HAMLIB_PATH . 'HW/HW3image/Tools.php');
require_once (HAMLIB_PATH . 'HW/Maps/Params.php');
require_once (HAMLIB_PATH . 'HW/Maps/Projection.php');
//require_once ('Color/BrightnessContrast.php');

class HWimageEngine {

	var $version = '$Id: Image.php,v 1.16 2007/09/13 21:08:54 wxfyhw Exp $';
	var $cfg;
	var $debug=0;
	var $temp_path = '/tmp';

	var $images = array();
	var $current;
	var $color;
	var $tools=null;
	var $text=null;
	var $brush;
	var $type = 'png';


	var $gdversion = 1.21;


	Function HWimageEngine($cfg, $temp_path, $debug) {
		$this->cfg = $cfg;
		$this->temp_path = $temp_path;
		$this->debug = $debug;

		if ($debug) print 'HWimageEngine : ' . $this->version . '<br>';

		$this->color = new HWimageColor();

		$this->gdversion = GDVersion();

		if ($this->gdversion < 1.6) { $type = 'gif';}
		else {$type = 'png';}
		$this->type = $type;

		$this->tools = new HWimageTools($this->gdversion);

	}


	Function &_make_chest() {
		$chest = array('tpcolor'=> -1,
					'bgcolor'=>0,
					'drawwidth'=>1,
					'drawcolor'=>0,
					'fillcolor'=>0,
					'fillbordercolor'=>0,
					'textcolor'=>0,
					'textsize'=>gdSmallFont,
					'textw'=>6,
					'texth'=>12,
					'textwrap'=>0,

					'is_TTF'=>0,
					'TTF_textfont'=>'',
					'TTF_textpt'=>'',
					'TTF_textangle'=>0,
					'truecolor'=>0,
					'antialiasing' => 1,

					'blend'=>100
					);

		return $chest;
	}

//	Function _parse_type ($type='png') {
//		$type = strtolower($type);
//		if (!$type) $type = $this->type;
//		if ($type == 'jpg') $type = 'jpeg';
//
//		# lets try to correct type for GD version number
//		if ($this->gdversion < 1.6) { $type = 'gif';}
//		elseif ($type == 'gif' && $this->gdversion >= 1.6) { $type = 'png';}
//
//		return array($type, 'newfrom'.$type);
//	}

	Function _parse_type ($type='png') {
		$type = strtolower($type);
		if (!$type) $type = $this->type;
		if ($type == 'jpg') {$type = 'jpeg';}
		elseif ($type != 'jpeg') { $type = 'png';}

		return array($type, 'newfrom'.$type);
	}


	Function  _map2xy ($name, $lon, $lat) {
		$lon = strtoupper($lon);
		$lat = strtoupper($lat);
		if (substr($lon,0,4) != 'LON:' && substr($lat,0,4) != 'LAT:') return array('',$lon, $lat);

		$lon = preg_replace('/^LON:\s*/', '', $lon);
		$lat = preg_replace('/^LAT:\s*/', '', $lat);

		if (isset($this->images[$name][3]['map_proj'])) {
			$map_proj = $this->images[$name][3]['map_proj'];
			list($x,$y) = $map_proj->get_xy($lon,$lat);
			return array('',$x,$y);
		}
		else {
			return array("Error: No Map projection for image $name",-1,-1);
		}
	}

	Function _fetchimage ($url, $saveas, $max_age = 0) {
		if (preg_match('!^https?://([^/]+)(/.+)?$!', $url, $m)) {
			$domain = $m[1]; $path = $m[2];
			include_once('hamlib/HW3Plugins/FetchWxData.php');
			if ($this->debug) print "domain=$domain<br>path=$path<br>\n";
			fetch_data($this->cfg, $max_age, $saveas, '', $domain, $path, 80, $this->debug,1);

			$type =  (preg_match('/\.(\w+)$/',$path,$m)) ? $m[1] : 'png';

			return 1;
		}
		else {return 0;}



	}



	Function _loadimage ($path, $type, $map='') {
		if (!$map && strtolower($type) == 'map') { $type =''; $map = 'MAP';}
		list ($type, $type_rou) = $this->_parse_type($type);

		if ($type != 'gif') { $path = preg_replace('/\.gif$/', ".$type",$path);}
		if ($this->debug) { print "loading image $path<br>\n";}

		list ($image, $error) = load_image2($path, $type);

		return array($error, &$image, $path, $map);
	}

	Function _loadmap_info($name='', $path) {
		if (!$name) $name = 'Default';
		$path = preg_replace('/\.[^.]+$/', '.txt', $path);

		$map_params = new HWmapParams();
		$map_info = $map_params->load_map_info($path);
		if (!$map_info) return "Error: No map info found at $path";

		$map_proj = new_projection($map_info, $this->debug);

		$chest =& $this->images[$name][3];
		$chest['map_proj'] =& $map_proj;
		$chest['map_params'] =& $map_params;

		return '';
	}

	Function do_loadmap ($name, $path, $type=''){
		return $this->do_loadimage($name, $path, $type, 'MAP');
	}

	Function do_killimage($name='') {
		if (!$name) $name = $this->current;
		if (isset($this->images[$name])) {
			unset($this->images[$name]);
			if ($name == $this->current) {
				reset($this->images);
				$this->current = key($this->images);
			}
		}
		return 1;
	}

	Function do_insertimage() {

		$error='';

		$numargs = func_num_args();
		if ($numargs <3) return "Error: Not enough args";

		$args = func_get_args();
		$name = array_shift($args);
		if ($this->debug) print "Insert Image name=$name<br>\n";
		if (isset($this->images[$name])) {
			$insert_image =& $this->images[$name][0];
		}
		else {
			$type = array_shift($args);
			list ($error, $insert_image, $tpath,$tmap) = $this->_loadimage($name, $type);

			if (($error || !$insert_image) && $this->debug) print "Error inserting image $error<br>\n";
			if ($error) return $error;
		}
		$x = array_shift($args);
		$y = array_shift($args);
		$y_off=0; $x_off=0;
		$halign = (count($args) > 0) ? array_shift($args) : '';
		$valign = (count($args) > 0) ? array_shift($args) : '';
		$w = (count($args) > 0) ? array_shift($args) : '';
		$h = (count($args) > 0) ? array_shift($args) : '';

		$current = $this->current;
		$image =& $this->images[$current][0];

		$im_bounds=array();
		$im_bounds[0]=imagesx($insert_image);
		$im_bounds[1]=imagesy($insert_image);

		$cm_bounds=array();
		$cm_bounds[0] = imagesx($image);
		$cm_bounds[1] = imagesy($image);

		$xx = strtolower($x);
		if ($xx == 'center') {$x = floor($cm_bounds[0]/2); $halign='CENTER';}
		elseif ($xx == 'left') { $x =0;}
		elseif ($xx == 'right') { $x = $cm_bounds[0];}

		$yy = strtolower($y);
		if ($yy == 'center') { $y = floor($cm_bounds[1]); $valign='CENTER';}
		elseif ($yy == 'top') { $y =0;}
		elseif ($yy == 'bottom') { $y = $cm_bounds[1];}

		list ($error, $x,$y) = $this->_map2xy($current, $x, $y);

		$halign = strtoupper($halign);
		$valign = strtoupper($valign);

		list ($nw,$nh) = $im_bounds;
		if ($w && $h) { $nw = $w; $nh = $h;}
		if ($halign == 'CENTER') { $x_off = -floor($nw/2);}
		elseif ($halign == 'RIGHT') { $x_off = $nw;}
		else { $x_off=0;}

		if ($valign == 'MIDDLE' || $valign=='CENTER') { $y_off = - floor($nh/2);}
		elseif ($valign == 'BOTTOM') { $y_off = -$nh;}
		else { $y_off = 0;}

		$blend = $this->images[$current][3]['blend'];
		if (!$blend || $blend > 100) $blend = 100;

		if ($this->debug) 	{
			print "INSERT IMAGE AT: (x,y) =($x,$y) : offset x,y=($x_off,$y_off) w,h=($w,$h)<br>\n";
			print "src w,h=(" . $im_bounds[0] . ',' .$im_bounds[1].")<br>\n";
			print "blend=$blend<br>\n";
		}

		if ($w && $h) {
			if ($this->gdversion < 1.6 || $blend == 100) {
				imagecopyresized( $image,$insert_image, $x+$x_off, $y+$y_off, 0,0,$w,$h,$im_bounds[0], $im_bounds[1]);
			}
			elseif ($blend > 0) {
				$timage = $this->createimage($w,$h, $this->images[$current][3]['truecolor']);
				if (!$timage) return "Error: Out of Memory";
				imagecopyresized($timage,$insert_image,  0,0,0,0,$w,$h,$im_bounds[0], $im_bounds[1]);
				imagecopymerge($image,$timage, $x+$x_off, $y+$y_off,0,0,$im_bounds[0], $im_bounds[1], $blend);
			}
		}
		else {
			if ($this->gdversion < 1.6 || $blend == 100) {

// imagecopy ( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h)
				imagecopy($image,$insert_image, $x+$x_off, $y+$y_off, 0,0,$im_bounds[0], $im_bounds[1]);
			}
			elseif ($blend > 0) {
				imagecopymerge($image,$insert_image, $x+$x_off, $y+$y_off,0,0,$im_bounds[0], $im_bounds[1], $blend);
			}
		}


		return '';
	}


	Function createimage ($w,$h, $truecolor=0) {

		if (!$truecolor || $this->gdversion < 2 ) { $new_image = imagecreate($w,$h); }
		else { $new_image = @ImageCreateTrueColor($w,$h) or $new_image= imagecreate($w,$h);}

		return $new_image;
	}


	Function do_fetchimage ($name, $url, $type='', $max_age =0) {

		$cfg = $this->cfg;
		if (!$type) $type = $this->type;

		if ($url) {
			$url = preg_replace('/\.gif$/', ".$type", $url);
			$temp_path = $this->temp_path;

			if ($this->debug) {
				print "Fetch image : url = $url<br>\n";
				print "Fetch Image temp_path=$temp_path<br>\n";
			}

			if ($max_age) {
				$temp_file = preg_replace('/\W/', '_', $url);
			}
			else {
				$temp_file = 'hwimage_' . floor(rand(0,1)* 9999999)+100;
				$tries =0;
				while (file_exists("$temp_path/$temp_file.$type") && $tries < 5) {
					$temp_file = 'hwimage_' . floor(rand(0,1)* 9999999)+100;
					$tries++;
				}
			}

//			$fetch=1;
//			if ($max_age) {
//				if (file_exists("$temp_path/$temp_file.$type")) {
//					$age = (time() - filemtime("$temp_path/$temp_file.$type"))/60;
//					if ($age < $max_age) $fetch=0;
//				}
//			}
//			if ($fetch) {
//				list ($img, $error) = load_image2($url, $type);
//				if (!$img) return "-hwError fetching image from url '$url' ($error)";
//				$tools->output_image($img, $type, "$temp_path/$temp_file.$type");
//			}
			$this->_fetchimage($url, "$temp_path/$temp_file.$type", $max_age);

			return $this->do_loadimage($name, "$temp_path/$temp_file.$type", $type);
		}

		return ("-hwError : URL : '$url' is not a valid url");
	}

	Function do_loadimage ($name, $file, $type='', $map='') {
		if (!$name) $name = 'Default';
		list ($error, $image, $path, $map) = $this->_loadimage($file, $type, $map);

		if (!$error) {
			$chest =& $this->_make_chest();
			$this->images[$name] = array($image, imagesx($image), imagesy($image), $chest);
			$this->current = $name;

			if ($map) {
				$error = $this->_loadmap_info($name, $path, $map);
			}
		}

		return $error;
	}

	Function do_newimage ($name, $w, $h, $truecolor=0) {
		$error ='';
		if (!$name) $name = 'Default';
		$w = floor(abs($w)); if ($w<1) $w=100;
		$h = floor(abs($h)); if ($h<1) $h=100;



		$image = $this->createimage($w,$h, $truecolor);
		if (!$image) {
			$error =  "Error: Out of Memory";
		}
		else {
			$chest =& $this->_make_chest();
			$chest['bgcolor'] = $this->color->get_color($image, 'FFFFFF');

			$this->images[$name] = array($image,$w,$h,$chest);
			$this->current = $name;
			$this->images[$this->current][3]['truecolor']= $truecolor;
		}

		return $error;
	}

	Function do_switch2image ($name) {
		if (!$name) $name = 'Default';

		$error='';
		if (! isset($this->images[$name])) {
			$error = "ERROR: $name is not a valid image";
		}
		else {
			$this->current = $name;
		}

		return $error;
	}

	Function do_outputimage ($type='', $name='', $path='') {


		$error = '';
		if (!$name || !isset($this->images[$name])) {
			$name = $this->current;
		}
		if (!$name) {
			$error = 'ERROR: No image to output';
		}
		else {
			if (!$type) $type = $this->type;
			$type = strtolower($type);
			if ($type == 'jpeg') $type = 'jpg';

			$image =& $this->images[$name][0];

			$this->tools->output_image ($image, $type, $path);
		}

		return $error;
	}

	Function do_save ($path, $type='', $name='') {


		$error = '';
		if (!$name || !isset($this->images[$name])) {
			$name = $this->current;
		}
		if (!$name) {
			$error = 'ERROR: No image to save';
		}
		else {
			if (!$type) $type = $this->type;
			$type = strtolower($type);
			if ($type == 'jpeg') $type = 'jpg';

			$image =& $this->images[$name][0];

			list ($junk, $error) = $this->tools->output_image ($image, $type, $path);
			if ($error && $this->debug) print "Error saving $path : $error<br>\n";
		}

		return $error;
	}

	Function do_interlace ($onoff = 'ON') {
		$onoff = strtoupper(trim($onoff));
		$image =& $this->images[$this->current][0];
		if ($onoff == 'OFF' || $onoff == 'NO') {
			imageinterlace($image, 0);
		}
		else {
			imageinterlace($image, 1);
		}

		return;
	}

	Function do_truecolor ($truecolor = 0) {
		 $current = $this->current;
		 $this->images[$current][3]['truecolor'] = $truecolor;
	}


	Function do_blend ($val =100) {
		$val = floor($val);

		if ($val < 0) { $val =0;}
		elseif ($val > 100) { $val = 100;}

		return $this->_set_param('blend',0,$val);
	}

	Function do_drawwidth ($val=1) {
		return $this->_set_param('drawwidth',0,$val);
	}

	Function do_drawcolor ($val) {
		return $this->_set_param('drawcolor',1,$val);
	}
	Function do_textcolor ($val) {
		if (isset($val)) {
			return $this->_set_param('textcolor',1,$val);
		}
		else {
			return;
		}
	}
	Function do_fillcolor ($val) {
		return $this->_set_param('fillcolor',1,$val);
	}


	Function do_brightcontrast ($bright=0, $contrast=0) {

		$image =& $this->images[$this->current][0];
		$bc = new HWimageBrghtContrast();
		$bc->adj_bright_contrast($image, $bright/100, $contrast/100);
	}

	Function do_TTF_textangle($angle=0) {
		if (!$angle) $angle=0;
		$chest =& $this->images[$this->current][3];
		$chest['TTF_textangle'] = $angle * DR;

		return;
	}

	Function do_TTF_textfont ($font) {

		$error ='';

		$chest =& $this->images[$this->current][3];
		$font = trim($font);

		if ($font && !file_exists($font)) {
			$error = "Error: font path '$font' is not valid.";
			$chest['is_TTF']=0;
		}
		else {
			$chest['TTF_textfont'] = $font;
			$chest['is_TTF']=1;
		}

		return $error;
	}

	function do_TTF_textpt($pt = 10) {
		if (!$pt) { $pt = 10;}
		elseif ($pt < 1) { $pt = 1;}

		$chest =& $this->images[$this->current][3];
		$chest['TTF_textpt'] = $pt;
	}

	Function do_textsize ($size='SMALL') {

		$error = '';
		if (is_numeric($size)) {
			$thefont = $size;
		}
		else {
			$size = strtoupper($size);
			if ($size == 'TINY') { $thefont=gdTinyFont;}
			elseif ($size == 'SMALL') { $thefont = gdSmallFont;}
			elseif ($size == 'MEDIUM') { $thefont = gdMediumBoldFont;}
			elseif ($size == 'LARGE') { $thefont = gdLargeFont;}
			elseif ($size == 'GIANT') { $thefont = gdGiantFont;}
			else { $thefont = gdSmallFont;}
		}

		$chest =& $this->images[$this->current][3];
		$chest['textfont'] = $thefont;
		$chest['textsize'] = $thefont;
		$chest['textwidth'] = imagefontwidth($thefont);
		$chest['textheight'] = imagefontheight($thefont);

		return $error;
	}

	Function do_text ($x,$y,$text='',$halign='', $valign='') {
		$stext = trim($text);
		if ($stext != '') {
			if ($this->text == null) $this->text = new HWimageText($this->gdversion, $this->debug);
			$image =& $this->images[$this->current][0];
			$chest =& $this->images[$this->current][3];
			if (!$chest['TTF_textpt']) $chest['TTF_textpt']=10;
			$this->text->do_text($image, $x, $y, $text, $halign, $valign, $chest);
		}
	}

	Function do_textwrap ($wrap_char=0) {
		$wrap_char = strtolower(trim($wrap_char));
		if ($wrap_char == 'off'	|| $wrap_char == 'no' || !$wrap_char) $wrap_char=0;
		return $this->_set_param('textwrap',0,$wrap_char);
	}

	Function do_textantialiasing ($antialiasing=1) {
		$antialiasing = strtolower(trim($antialiasing));
		if ($antialiasing == 'off'	|| $antialiasing == 'no' || $antialiasing == '0') {
			$antialiasing=-1;
		}
		else {
			$antialiasing=1;
		}
		return $this->_set_param('antialiasing',0,$antialiasing);
	}



	Function do_transparentcolor() {
		$parms = func_get_args();

		if (count($parms) > 0) {

			$image =& $this->images[$this->current][0];
			$chest =& $this->images[$this->current][3];

			if (strtoupper($parms[0]) == 'NONE') {
				imagecolortransparent($image,-1);
				$chest['tpcolor'] = null;
			}
			else {
				if (isset($parms[2])) { $rgb = $this->color->parse_colorpsec($parms); }
				else { $rgb = $this->color->parse_colorspec($parms[0]);}

				$tp = imagecolorexact($image, $rgb[0], $rgb[1], $rgb[2]);
				if ($tp == -1) $tp = imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]);
				imagecolortransparent($image,$tp);
				$chest['tpcolor'] = $tp;
			}
		}

		return;
	}


	Function do_background () {
		$parms = func_get_args();

		if (count($parms) > 0) {
			$image =& $this->images[$this->current][0];
			$chest =& $this->images[$this->current][3];

			$bgcolor = (isset($chest['bgcolor'])) ? $chest['bgcolor'] : null;

			//if ($bgcolor != null) imagecolordeallocate($image, $bgcolor);

			if (strtolower($parms[0]) == 'transparent') {
				if ($bgcolor == null) {$bgcolor = $this->color->get_color($image, 'FFFFFF');}
				imagecolortransparent($image, $bgcolor);
				$chest['bgcolor'] = $bgcolor;
				$chest['tpcolor'] = $bgcolor;
			}
			else {
				if ($bgcolor =='0' || $bgcolor>0) imagecolordeallocate($image, $bgcolor);
				if (isset($parms[2])) {
					$bgcolor = $this->color->get_color($image, $parms);
				}
				else {
					$bgcolor = $this->color->get_color($image, $parms[0]);
				}
				$chest['bgcolor']=$bgcolor;
			}
		}

		return;
	}





	Function do_fill ($x,$y) {
		$current = $this->current;
		list ($error, $x,$y) = $this->_map2xy($current, $x,$y);
		if (!$error) {
			imagefill($this->images[$current][0], $x,$y, $this->images[$current][3]['fillcolor']);
		}
		return $error;
	}


	Function do_rectangle ($x1, $y1, $x2, $y2, $fill=0) {

		$current = $this->current;
		list ($error, $x1,$y1) = $this->_map2xy($current, $x1,$y1);
		list ($error, $x2,$y2) = $this->_map2xy($current, $x2,$y2);

		$image =& $this->images[$this->current][0];
		$chest =& $this->images[$this->current][3];

		$color = $this->_make_brush($current, $image);
		if ($fill) {
			imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);
		}
		else { imagerectangle($image, $x1,$y1, $x2, $y2, $color); }

		return $error;
	}

	Function do_crop ($x,$y,$w,$h) {

		if ($w < 1) $w=100;
		if ($h < 1) $h=100;
		$current = $this->current;

		list ($error, $x,$y) = $this->_map2xy($current, $x,$y);
		if (!$error) {
			$new_image = $this->createimage($w,$h,  $this->images[$current][3]['truecolor']);
			imagecopy($new_image, $this->images[$current][0],0,0,$x,$y,$w,$h);
			$this->images[$current] = array($new_image, $w,$h, $this->images[$current][3]);
		}

		return $error;
	}


	Function _copy_color_table (&$old_image, &$new_image) {
		imagepalettecopy($new_image, $old_image);

		$tpcolor = imagecolortransparent($old_image);
		if ($tpcolor > -1) imagecolortransparent($new_image, $tpcolor);

	}


	Function do_resize ($w,$h) {

		$error ='';
		$w = floor(abs($w)); $h = floor(abs($h));

		if ($w<1 or $h<1) {
			$error = "Error: Resize parms must be 1 or higher";
		}
		else {
			$current = $this->current;
			$old_image =& $this->images[$this->current][0];
			$new_image = $this->createimage($w,$h,  $this->images[$current][3]['truecolor']);

			$this->_copy_color_table ($old_image, $new_image);

			imagecopyresized($new_image,$old_image,  0,0,0,0,$w,$h, imagesx($old_image), imagesy($old_image));
			$this->images[$current] = array(&$new_image, $w,$h,$this->images[$current][3]);

		}
		return $error;
	}

	Function do_polygon () {
		$pts = func_get_args();
		$num = count($pts);

		if ($num % 2 != 0) { $fill=1; $num--; array_pop($pts);}
		else {$fill=0;}

		$current = $this->current;
		$error = '';

		$xypts=array();
		$numpts = 0;
		for ($i =0; $i < $num; $i+=2) {
			list ($error, $x,$y) = $this->_map2xy($current, $pts[$i], $pts[$i+1]);
			array_push($xypts, $x,$y);
			$numpts++;
			if ($this->debug) print "added point ($x,$y)<br>\n";
		}

		$image =& $this->images[$this->current][0];
		$color = $this->_make_brush($current, $image);
		if ($fill) { imagefilledpolygon($image, $xypts, $numpts,$color); }
		else { imagepolygon ($image, $xypts,$numpts, $color);}

		return $error;
	}

	Function do_arc ($x,$y,$w,$h,$st,$e, $fill=0) {
		$current  = $this->current;
		list ($error, $x,$y) = $this->_map2xy($current, $x,$y);
		$image =& $this->images[$this->current][0];
		$color = $this->_make_brush($current, $image);

		if ($fill) {
			imagefilledarc($image, $x,$y,$w,$h,$st,$e,$color,IMG_ARC_PIE);
		}
		else {
			imagearc($image, $x,$y,$w,$h,$st,$e,$color);
		}

		return '';
	}


	Function do_dased_line ($x1,$y1,$x2,$y2) {
		return $this->do_line ($x1,$y1,$x2,$y2, 'DASHED');
	}


	Function do_line ($x1,$y1,$x2,$y2, $dashed='') {
		$current  = $this->current;

		list ($error, $x1,$y1) = $this->_map2xy($current, $x1,$y1);
		list ($error, $x2,$y2) = $this->_map2xy($current, $x2,$y2);

		if (!$error) {
			$image =& $this->images[$this->current][0];
			$color = $this->_make_brush($current, $image);
			if ($dashed) {
				if ($this->debug) 	print "Doing dashedLine($x1, $y1, $x2, $y2, $color)<br>\n";
				imagedashedline($image, $x1, $y1, $x2, $y2, $color);
			}
			else {
				if ($this->debug) 	print "Doing Line($x1, $y1, $x2, $y2, $color)<br>\n";
				imageline($image, $x1, $y1, $x2, $y2, $color);
			}

		}
		return $error;
	}

	Function _make_brush ($current, &$image) {

		$chest =& $this->images[$current][3];
		$width = $chest['drawwidth'];
		if ($width > 1) {
			$this->brush = $this->_create_linebrush($current, $image, $width);
			$color = IMG_COLOR_BRUSHED;
			imagesetbrush ($image, $this->brush);
		}
		else {
			$color = $chest['drawcolor'];
		}
		return $color;
	}

	Function _create_linebrush ($current, &$image, $width=1) {

		$chest =& $this->images[$current][3];

		$brush  =$this->createimage($width, $width);
		$background = imagecolorallocate($brush, 255,255,255);
		imagecolortransparent($brush, $background);

		$rgb = imagecolorsforindex($image, $chest['drawcolor']);
		$foreground = imagecolorallocate($brush, $rgb['red'],$rgb['green'],$rgb['blue']);

		imagefilledrectangle($brush,0,0,$width-1,$width-1,$foreground);

		return $brush;
	}

	Function _set_param ($name, $type, $vals) {

		$error = '';

		if (!$name) {
			$error = 'Error: No parameter to set';
		}
		else {
			$current = $this->current;
			$image =& $this->images[$current][0];
			$chest =& $this->images[$current][3];

			if (!$type) { //integer
				$val = (is_array($vals)) ? $vals[0] : $vals;
				if (!$val) { $val = 1;}
				elseif ($val > max_int) { $val = max_int;}
			}
			elseif ($type ==1) {
				$val = $this->color->get_color($image, $vals);
			}
			elseif ($type ==2 ) {
				$val = $this->color->get_color($image, $vals, -1);
			}
			elseif (is_array($vals)) {
				$val = $vals[0];
			}
			else { $val = $vals;}

			$chest[$name]=$val;
		}

		return $error;
	}


	Function pp_do_getxy ($data='') {

		$data = preg_replace('/^\s+/', '', strtoupper($data));

		if (preg_match('/LAT:\s*([+\d.-]+)/', $data, $m)) {
			$lat = 'LAT:'.$m[1];
			if (preg_match('/LON:\s*([+\d.-]+)/', $data,$m)) {
				$lon = 'LON:'.$m[1];
			}
		}
		else {
			list ($lon, $lat) = preg_split('/\s+/', $data,2);
		}

		list ($error, $x, $y) = $this->_map2xy($this->current, $lon,$lat);
		if ($error) { return $error; }
		else { return "$x $y"; }
	}


	Function pp_do_getx ($data='') {
		$xy = $this->pp_do_getxy($data);

		$axy = preg_split('/ /', $xy);
		return $axy[0];
	}

	Function pp_do_gety ($data='') {
		$xy = $this->pp_do_getxy($data);

		$axy = preg_split('/ /', $xy);
		if (isset($axy[1])) { return $axy[1];}
		else { return '0';}

	}

	Function pp_do_getwidth() {
		$current = $this->current;
		$image =& $this->images[$current][0];
		return imagesx($image);
	}

	Function pp_do_getheight() {
		$current = $this->current;
		$image =& $this->images[$current][0];
		return imagesy($image);
	}

	Function gethwivar ($var) {

		$current = $this->current;
		$image =& $this->images[$current][0];

		$varname = strtolower($var);

		if ($varname == 'width') { $val = imagesx($image);}
		elseif ($varname == 'height') { $val = iamgesy($image);}
		elseif ($varname == 'version') { $val == $this->version;}
		elseif ($varname == 'gdversion') { $val = $this->gdversion;}
		else {
			$chest =& $this->images[$current][3];
			$val =  (isset($chest[$var])) ? $chest[$var] : '';
		}

		return $val;
	}







}


?>