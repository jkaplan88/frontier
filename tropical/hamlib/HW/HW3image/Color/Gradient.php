<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Gradient.php,v 1.2 2007/09/13 21:08:54 wxfyhw Exp $
*
*/

require_once (HAMLIB_PATH . '/HW/HW3image/Color.php');


class Gradient {

	var $version = '$Id: Gradient.php,v 1.2 2007/09/13 21:08:54 wxfyhw Exp $';


	Function Gradient() {

	}


	function _dif ($start, $end) {
		$dif =0;
		if ($start >= $end) { $dif = $start - $end;}
		else { $dif = $end - $start; }

		return $dif;
	}

	function _get_color_gradient($start,$end,$pos,$step_width) {

		$color =0;
		if ($start > $end) {$color = $start - $step_width * $pos; }
		else {$color = $start + $step_width * $pos; }

		return $color;
	}

	function gradient_colors ($start_color, $stop_color, $total_intervals) {
		$color = new HWimageColor();
		 $gradient_colors = array();

		if ($total_intervals != 0)  {

			list  ($red_start, $green_start, $blue_start) = $color->parse_colorspec($start_color);
			list ($red_end, $green_end, $blue_end) = $color->parse_colorspec($stop_color);

			$dif_red = $this->_dif($red_start,$red_end);
			$dif_green = $this->_dif($green_start,$green_end);
			$dif_blue = $this->_dif($blue_start,$blue_end);

			 $step_red = $dif_red / $total_intervals;
			 $step_green = $dif_green / $total_intervals;
			 $step_blue = $dif_blue / $total_intervals;



			for ($pos = 0; $pos <= $total_intervals; $pos++) {
				$red = $this->_get_color_gradient($red_start,$red_end,$pos,$step_red);
				$green = $this->_get_color_gradient($green_start,$green_end,$pos,$step_green);
				$blue =  $this->_get_color_gradient($blue_start,$blue_end,$pos,$step_blue);

				array_push ($gradient_colors, sprintf('%02X%02X%02X', $red,$green,$blue));
			}
		}


		return $gradient_colors;
	}



function fill_in_gradients($start, $stop, $interval, &$colors) {

	$num_dec=0;


	 if (!$interval) $interval = 1;
	if (preg_match('/\.(\d+)$/', $interval,$m)) {
		$num_dec = strlen($m[1]);
	}



	for ($i = $start; $i < $stop; $i = round($i + $interval, $num_dec) ){
		if (isset ($colors["$i"])) {
			$start_color = $colors["$i"];
			$num=0;

			for ($j= $i+$interval; $j <= $stop; $j =round($j+ $interval, $num_dec)) {
				$num++;
				if (isset ($colors["$j"])) {
					if ($num > 1) {
						$stop_color = $colors["$j"];
						$gradients =& $this->gradient_colors($start_color, $stop_color,$num);
						for ($k = 0; $k < $num; $k++) {
							$ind = round($k*$interval + $i, $num_dec);
							#$ind = floor ( ($k*$interval + $i) * pow(10,$num_dec) +.5* (($i < 0) ? -1 : 1) )/pow(10,$num_dec);

							$colors["$ind"] = $gradients["$k"];
						} # next k
						$i = round($j-$interval, $num_dec);
						break; #end the j loop
					}
					else {
						break; #ned j loop
					}

				}
			} # next j
		}
	} # next i

}




}

?>