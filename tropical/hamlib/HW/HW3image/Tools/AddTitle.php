<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: AddTitle.php,v 1.2 2007/09/20 19:58:18 wxfyhw Exp $
*
*/

require_once (HAMLIB_PATH . '/HW/HW3image/Color.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Tools/Fonts.php');


class AddTitle {

	var $version = '$Id: AddTitle.php,v 1.2 2007/09/20 19:58:18 wxfyhw Exp $';


	Function AddTitle() {

	}


Function create_image($w, $h, $truecolor, $background, $color='') {

	# if not passed a color lets create
	if (empty($color))   $color = new HWimageColor();

	# we have to handle the backgroud color for true color images differently
	$image = '';
	if ($truecolor) {
		$image = imagecreatetruecolor($w,$h);
		$bg = $color->get_color($image, $background);
		imagefilledrectangle($image, 0,0, $w-1,$h-1,$bg);
	}
	else {
		$image = imagecreate($w,$h);
		$bg = $color->get_color($image, $background);
	}

	return $image;
}


Function get_gdfont($size) {

	$thefont = gdSmallFont;

	$size = strtoupper($size);
	if ($size == 'TINY') { $thefont=gdTinyFont;}
	elseif ($size == 'SMALL') { $thefont = gdSmallFont;}
	elseif ($size == 'MEDIUM') { $thefont = gdMediumBoldFont;}
	elseif ($size == 'LARGE') { $thefont = gdLargeFont;}
	elseif ($size == 'GIANT') { $thefont = gdGiantFont;}

	return $thefont;
}




Function add_title(&$image, &$title_parms) {

	$color = new HWimageColor();

	$w = imagesx($image);
	$h = imagesy($image);

	$height = (isset($title_parms['height'])) ? $title_parms['height'] : 30;
	if ($height < 1) $height = 30;

	 # if "pos" = 0 or "top" we put the the titlebar at the top otherwise its at the bottom
	 $pos = (!isset($title_parms['position']) || !$title_parms['position'] || preg_match('/top/i', $title_parms['position'])) ? 0 : 1;

	 $truecolor = (isset($title_parms['truecolor'])) ? $title_parms['truecolor'] : 0;

	 $image_trans = imagecolortransparent ($image);

	 $ugg_trans = (isset($title_parms['temp_transparency_color'])) ? $title_parms['temp_transparency_color'] : '78d68b';
	 $ugg = $color->get_color($image, $ugg_trans);
	 imagecolortransparent ($image,$ugg);



	#if "mode" = 1 then we need to add space to the top/bottom of the image for the title bar
	# otherwise we blend it into the image
	$newimage='';
	if (isset($title_parms['mode']) && $title_parms['mode']) {
		# create new image adding title height to it

		$newimage = $this->create_image($w, $h+$height, $truecolor, 'FFFFFF', $color);


		# copy the original image to new image..
		# if pos = 0 then we leave room for the title at the top
		# otherwise we put the title at the bottom
		if (!$pos) {
			imagecopy ($newimage, $image, 0, $height, 0, 0, $w, $h);
		}
		else {
			imagecopy ($newimage, $image, 0,0, 0, 0, $w, $h);
		}
	}
	else {
		# we will blend the title into  the image so it doesnt matter
		# about the position to copy the old image into the new image;
		$newimage = $this->create_image($w, $h, $truecolor, 'FFFFFF', $color);

		imagecopy ($newimage, $image, 0,0, 0, 0, $w, $h);
	}

	# create a temp image for title bar
	$bgcolor = (isset($title_parms['bg_color'])) ? $title_parms['bg_color'] : '000000';
	$titlebar = $this->create_image($w,$height, 0, $bgcolor, $color);


	# blend the title bar into the newimage
	$blend_level = (isset($title_parms['bg_blend_level'])) ? $title_parms['bg_blend_level'] : 100;
	if ($blend_level < 0 || $blend_level > 100) $blend_level = 100;


	if (!$pos) {
		#blend into the top
		imagecopymerge($newimage, $titlebar, 0,0,0,0,$w,$height, $blend_level);
	}
	else {
		# blend into the bottom
		imagecopymerge($newimage, $titlebar, 0,$h,0,0,$w,$height, $blend_level);
	}
	imagedestroy($titlebar);

	# add the baseline separate if needed
	$baseline = (isset($title_parms['baseline_color'])) ? $title_parms['baseline_color'] : '';
	if ($baseline != '') {
		$blcolor = $color->get_color($newimage, $baseline);

		if (!$pos) {
			#add to top
			imageline($newimage, 0,$height, $w, $height, $blcolor);
		}
		else {
			# add to title at bottm
			imageline($newimage,0,$h-1, $w, $h -1, $blcolor);
		}

	}



	$title = (isset($title_parms['title_text'])) ? $title_parms['title_text'] : '';
	if ($title != '') {

		$fgcolor = $color->get_color($newimage, (isset($title_parms['title_font_color'])) ? $title_parms['title_font_color'] : '000000');

		$ttfpath = (isset($title_parms['TTF_path'])) ? $title_parms['TTF_path'] : '';
		$ttf_font = (isset($title_parms['title_TTF_font'])) ? $title_parms['title_TTF_font'] : '';
		$ttf_font = $ttfpath . '/' . $ttf_font;

		$title_text = (isset($title_parms['title_text'])) ? $title_parms['title_text'] : '';

		$text_x = (isset($title_parms['title_text_x']) && $title_parms['title_text_x']) ? $title_parms['title_text_x'] : 'left';
		if (preg_match ('/left/i', $text_x)) $text_x = 5;

		$text_y = (isset($title_parms['title_text_y']) && $title_parms['title_text_y']) ? $title_parms['title_text_y'] : $height -2;

		$xoffset = (isset($title_parms['title_dropshadow_xoffset'])) ? $title_parms['title_dropshadow_xoffset'] : 0;
		$yoffset = (isset($title_parms['title_dropshadow_yoffset'])) ? $title_parms['title_dropshadow_yoffset'] : 0;
		$dscolor  = $color->get_color($newimage, (isset($title_parms['title_dropshadow_color'])) ? $title_parms['title_dropshadow_color'] : '000000');

		if ($ttf_font && file_exists($ttf_font) ) {

			$ttf_font_pt = (isset($title_parms['title_TTF_font_pt'])) ? $title_parms['title_TTF_font_pt'] : 10;
			$ttf_font_angle = (isset($title_parms['title_TTF_font_angle'])) ? $title_parms['title_TTF_font_angle'] : 0;

			 if ($yoffset!=0 || $xoffset !=0) {
			 	imagettftext($newimage, $ttf_font_pt, $ttf_font_angle, $text_x+$xoffset, $text_y+$yoffset, $dscolor, $ttf_font, $title);
			}
			 imagettftext($newimage, $ttf_font_pt, $ttf_font_angle, $text_x, $text_y, $fgcolor, $ttf_font, $title);

		}
		else {
			$gdfont = $this->get_gdfont( (isset($title_parms['title_gdfont'])) ? $title_parms['title_gdfont'] : 'MEDIUM' );
			if ($yoffset!=0 || $xoffset !=0) {
				imagestring($newimage,$gdfont, $text_x+$xoffset, $text_y - imagefontheight($gdfont)+$yoffset, $title, $fgcolor);
			}
			imagestring($newimage,$gdfont, $text_x, $text_y - imagefontheight($gdfont), $title, $fgcolor);
		}
	}


	$credit = (isset($title_parms['credit_text'])) ? $title_parms['credit_text'] : '';
	if ($credit != '') {

		$fgcolor = $color->get_color($newimage, (isset($title_parms['credit_font_color'])) ? $title_parms['credit_font_color'] : '000000');

		$ttfpath = (isset($title_parms['TTF_path'])) ? $title_parms['TTF_path'] : '';
		$ttf_font = (isset($title_parms['credit_TTF_font'])) ? $title_parms['credit_TTF_font'] : '';
		$ttf_font = $ttfpath . '/' . $ttf_font;

		$title_text = (isset($title_parms['credit_text'])) ? $title_parms['credit_text'] : '';

		$text_x = (isset($title_parms['credit_text_x']) && $title_parms['credit_text_x']) ? $title_parms['credit_text_x'] : 'left';
		if (preg_match ('/left/i', $text_x)) $text_x = 5;

		$text_y = (isset($title_parms['credit_text_y']) && $title_parms['credit_text_y']) ? $title_parms['credit_text_y'] : $height -2;

		$xoffset = (isset($title_parms['credit_dropshadow_xoffset'])) ? $title_parms['credit_dropshadow_xoffset'] : 0;
		$yoffset = (isset($title_parms['credit_dropshadow_yoffset'])) ? $title_parms['credit_dropshadow_yoffset'] : 0;
		$dscolor  = $color->get_color($newimage, (isset($title_parms['credit_dropshadow_color'])) ? $title_parms['credit_dropshadow_color'] : '000000');

		if ($ttf_font && file_exists($ttf_font) ) {

			$ttf_font_pt = (isset($title_parms['credit_TTF_font_pt'])) ? $title_parms['credit_TTF_font_pt'] : 10;
			$ttf_font_angle = (isset($title_parms['credit_TTF_font_angle'])) ? $title_parms['credit_TTF_font_angle'] : 0;

			if (preg_match('/right/i', $text_x)) {

				$bounds =  imagettfbbox ( $ttf_font_pt, $ttf_font_angle, $ttf_font, $credit);
				$cwidth = abs($bounds[2] - $bounds[0]);
				$text_x = $w - 5 - $cwidth;
			}
			if ($yoffset!=0 || $xoffset !=0) {
				imagettftext($newimage, $ttf_font_pt, $ttf_font_angle, $text_x+$xoffset, $text_y+$yoffset, $dscolor, $ttf_font, $credit);
			}
			imagettftext($newimage, $ttf_font_pt, $ttf_font_angle, $text_x, $text_y, $fgcolor, $ttf_font, $credit);


		}
		else {
			$gdfont = $this->get_gdfont( (isset($title_parms['credit_gdfont'])) ? $title_parms['credit_gdfont'] : 'MEDIUM' );

			if (preg_match('/right/i', $text_x)) {
				$cwidth = strlen($credit) * imagefontwidth($gdfont);
				$text_x = $w - 5 - $cwidth;
			}
			if ($yoffset!=0 || $xoffset !=0) {
				imagestring($newimage, $gdfont, $text_x+$xoffset, $text_y  - imagefontheight($gdfont) + $yoffset, $credit, $fgcolor);
			}
			imagestring($newimage, $gdfont, $text_x, $text_y - imagefontheight($gdfont), $credit, $fgcolor);
		}

	}


	imagecolortransparent ($image, $image_trans);
	return $newimage;

}


}


?>