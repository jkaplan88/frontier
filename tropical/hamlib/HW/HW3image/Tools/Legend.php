<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Legend.php,v 1.2 2007/09/22 00:48:34 wxfyhw Exp $
*
*/

require_once (HAMLIB_PATH . '/HW/HW3image/Color.php');
require_once (HAMLIB_PATH . '/HW/HW3image/Color/Gradient.php');


class HWimageLegend {
	var $version = '$Id: Legend.php,v 1.2 2007/09/22 00:48:34 wxfyhw Exp $';
	var $GDVersion=1;
	var $debug=0;

	Function HWimageLegend ($gdversion=1.6, $debug=0) {
		$this->GDVersion=$gdversion;
		$this->debug = $debug;
	}


	Function draw_legend ($image, &$parms) {

		$color = new HWimageColor();

		$start = (isset($parms['start'])) ? $parms['start'] : 0;
		$stop = (isset($parms['stop'])) ? $parms['stop'] : 0;
		$interval = (isset($parms['interval'])) ? $parms['interval'] : 0;
		$no_gradient = (isset($parms['no_gradient'])) ? $parms['no_gradient'] : 0;

		# if we have not interval & no_gradient wasnt set
		# then we have nothing to do lets leave

		if (!$interval && !$no_gradient) return;


		#lets get the total_blocks
		$block_offset = (isset($parms['block_offset'])) ? $parms['block_offset'] : 0;
		$block_width = (isset($parms['block_width'])) ? $parms['block_width'] : 0;
		$block_height = (isset($parms['block_height'])) ? $parms['block_height'] : 0;
		$legend_size = (isset($parms['legend_size'])) ? $parms['legend_size'] : 0;

		$block_outline_color = (isset($parms['block_outline_color'])) ? $parms['block_outline_color'] : 0;

		$colors =& $parms['colors'];


		if (!$no_gradient) {
			#lets make sure we have all the colors we need
			$gradient = new Gradient();
			$gradient->fill_in_gradients ($start,$stop+10,$interval,$colors);
			$gradient = null;
		}


		$labels = (isset($parms['add_labels']) && $parms['add_labels']) ? $parms['add_labels'] : 'label';

		$label_names = explode(',',$labels);
		$label_parms = array();

		foreach ($label_names as $label_name) {
			$ldata = array();
			$ldata['TTF_path'] = (isset($parms['TTF_path'])) ? $parms['TTF_path'] : '';
			$ldata['label_when'] = (isset($parms[$label_name . '_when'])) ? $parms[$label_name . '_when'] : 0;
			$ldata['label_what'] = (isset($parms[$label_name . '_what'])) ? $parms[$label_name . '_what'] : '$i';
			$ldata['label_xoffset'] = (isset($parms[$label_name . '_xoffset'])) ? $parms[$label_name . '_xoffset'] : 0;
			$ldata['label_yoffset'] = (isset($parms[$label_name . '_yoffset'])) ? $parms[$label_name . '_yoffset'] : 0;

			$ldata['label_start'] = (isset($parms[$label_name . '_start'])) ? $parms[$label_name . '_start'] : '';
			$ldata['label_stop'] = (isset($parms[$label_name . '_stop'])) ? $parms[$label_name . '_stop'] : '';
			$ldata['label_interval'] = (isset($parms[$label_name . '_interval'])) ? $parms[$label_name . '_interval'] : '';
			$ldata['label_where'] = (isset($parms[$label_name . '_where'])) ? $parms[$label_name . '_where'] : '';

			$ldata['label_color']  = (isset($parms[$label_name . '_label_color'])) ? $color->get_color($image, $parms[$label_name . '_label_color']) : $color->get_color($image, '000000');
			$ldata['label_ttf_font'] = (isset($parms[$label_name . '_TTF_font'])) ? $ldata['TTF_path'] . '/' . $parms[$label_name . '_TTF_font'] : '';
			$ldata['label_ttf_font_pt'] = (isset($parms[$label_name . '_TTF_font_pt'])) ? $parms[$label_name . '_TTF_font_pt'] : 8;
			$ldata['label_gdfont_name'] = (isset($parms[$label_name . '_gdfont'])) ? $parms[$label_name . '_gdfont'] : 0;

			//$ldata{label_gdfont_name} = exists ( $gdfonts{uc $$parms{$label_name . '_gdfont'}}) ? uc $$parms{$label_name . '_gdfont'} : 'small';

			$label_name_length = strlen($label_name)+1;
			foreach ($parms as $key => $value) {
				if (substr($key, 0, $label_name_length) == $label_name . '_') {
					$ldata[substr($key, $label_name_length)] = $value;
				}
			}
			$label_parms[$label_name] = $ldata;

		}


		$legend_x = (isset($parms['legend_x']) && $parms['legend_x'] != '') ? strtolower($parms['legend_x']) : 'center';
		$legend_y = (isset($parms['legend_y']) && $parms['legend_y'] != '') ? strtolower($parms['legend_y']) : 'center';

		# get the direction : 0 = horizontal; 1 = verticle
		$direction ='';
		$yoffset = 0; $xoffset =0;

		# let determine what all we need to legend
		# if doing normal gradient we will step through each  potential value
		# putting into an array. if no gradient then we will use the "legend_order"
		# setting if available or we will use all the colors in numerical order ascending
		$legend_values = array();
		if (!$no_gradient) {
			$num_dec=0;
			if (preg_match('/\.(\d+)$/', $interval,$m)) {
				$num_dec = strlen($m[1]);
			}
			# we are using gradient so we must step through from start to finish
			for ($i=$start; $i <=$stop; $i =round ($i+$interval, $num_dec)) {
				array_push ($legend_values,$i);

			}
		}
		else {
			$legend_order = (isset($parms['legend_order'])) ? $parms['legend_order'] : 0;
//			$tcolors= array();
			if (!$legend_order) {
				foreach ($colors as $key => $value) {
					if (preg_match('/^[\-\d\.]+$/', $key)) {

						array_push($legend_values, $key);
					}
				}
				sort($legend_values);
				reset($legend_values);

			}
			else {
				$legend_values = explode(',', $legend_order);
			}
		}


		$image_w = imagesx($image);
		$image_h = imagesy($image);
		$r =1;

		$direction = (isset($parms['direction'])) ? strtolower($parms['direction']) : 0;
		if (!$direction || $direction == 'horizontal') {
			$direction =0;
			$xoffset += $block_offset;

			$num_values = count($legend_values);

			if ($legend_size > 0) {
				$block_width = floor(($legend_size - $block_offset*2) / $num_values+.5);
			}
			$legend_size = floor($num_values * $block_width + $block_offset*2);


			if (abs($stop - $start) == 0) {
				$r = 0;
			}
			else {
				$r = floor(($num_values-1) * $block_width)/ abs($stop - $start);
			}

			if ($legend_x == 'center') {
				$legend_x = floor( ($image_w - $legend_size) / 2);
			}
			if (preg_match(' /left|top/', $legend_y)) {
				$legend_y = 5;
			}
		}
		else {
			$direction = 1;
			$yoffset += $block_offset;

			$num_values = count($legend_values);

			if ($legend_size > 0) {
				$block_height = floor (($legend_size - $block_offset*2) / $num_values +.5);
			}

			$legend_size = floor($num_values * $block_height + $block_offset*2);

			if (abs($stop - $start) == 0) {
				$r = 0;
			}
			else {
				$r = floor(($num_values-1)  * $block_height)/ abs($stop - $start);
			}

			if ($legend_y == 'center') {
				$legend_y = floor( ( $image_h - $legend_size) / 2);
			}
			if (preg_match(' /left|top/', $legend_x)) {
				$legend_x =5;
			}
		}



		$text = new HWimageText();

		$block_num=0; $x = $legend_x; $y = $legend_y;
		$do_label=0;
		$label_text='';

		foreach ($legend_values as $i ) {


			$tcolor = $color->get_color($image, $colors["$i"]);
			if (!$direction) {$x = $legend_x +  $block_num * $block_width + $xoffset;}
			else {$y = $legend_y + $block_num * $block_height + $yoffset;}

			imagefilledrectangle($image, $x, $y, $x+ $block_width-1, $y+$block_height, $tcolor);
			$block_num++;
		}


		# lets add theoutside outline as needed
		if ($block_outline_color <> '') {
			$ocolor = $color->get_color($image,$block_outline_color);

			if (!$direction) {
				imagerectangle($image,$legend_x + $xoffset , $legend_y + $yoffset, $legend_x + $block_num*$block_width -1+$xoffset, $legend_y + $block_height+$yoffset, $ocolor);
			}
			else {
				imagerectangle($image, $legend_x + $xoffset , $legend_y + $yoffset, $legend_x + $block_width -1+$xoffset, $legend_y +  $block_num * $block_height+$yoffset, $ocolor);

			}
		}


		# lets add the legend labels here
		foreach ($label_parms as $labelname => $lparms) {


			$label_start = $lparms['label_start'];
			$label_stop = $lparms['label_stop'];
			$label_interval = $lparms['label_interval'];
			$label_where = $lparms['label_where'];
			$label_where = str_replace(" int(", " floor(", $label_where);

			$label_when = $lparms['label_when'];
			$label_what = $lparms['label_what'];

			$label_array = array();

			if ($label_start != '' && $label_stop != '' && $label_interval != '') {
				$num_dec=0;
				if (preg_match('/\.(\d+)$/', $label_interval,$m)) {
					$num_dec = strlen($m[1]);
				}

				# we are using gradient so we must step through from start to finish
				for ($i=$label_start; $i <=$label_stop; $i =round ($i+$label_interval, $num_dec)) {
					array_push ($label_array,$i);
				}
			}
			else {
				$label_array = $legend_values;
			}

			$block_num=0; $x = $legend_x; $y = $legend_y;
			$do_label=0;
			$label_text='';
			foreach ($label_array as $i) {


				if (!$direction) {
					if ($label_where != '') {
						eval ("\$x = $label_where;");
					}
					else { $x = $legend_x +  $block_num * $block_width + $xoffset;}
				}
				else {
					if ($label_where != '') { eval ("\$y = $label_where;"); }
					else {$y = $legend_y + $block_num * $block_height + $yoffset;}
				}

				eval ("\$do_label = $label_when;");
//				print "i=$i<br>\$label_text = $label_what;<br>\n";
				eval ("\$label_text = $label_what;");


				if ($do_label) {
					if (!$direction) {
						$text->do_text($image, $x + $lparms['label_xoffset'],$y+$block_height+3+$lparms['label_yoffset'],$label_text,'center','top', $lparms);
					}
					else {
						$text->do_text($image, $x+$block_width+3+$lparms['label_xoffset'],$y+$lparms['label_yoffset'],$label_text,'center','center', $lparms);

					}
				}
			$block_num++;
			}
		}
	}






}


?>