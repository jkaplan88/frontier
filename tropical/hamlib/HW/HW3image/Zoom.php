<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Zoom.php,v 1.2 2003/12/19 05:07:37 lee Exp $
*
*/


class HWimageZoom {

	var $GDVersion=1;
	var $version = '$Id: Zoom.php,v 1.2 2003/12/19 05:07:37 lee Exp $';

	Function HWImageZoom ($gdversion=1) {
		$this->GDVersion = $gdversion;
	}
	
	Function zoom(&$image, $zf, $xc, $yc, $zw, $zh) {
		
		
		# Lets quit if  no image was supplied to us
		# also check the validity of some of the other
		# inputs
		if (!$image) return '';
		if (!$zf) $zf=1;
		$zfactor = ($zf>0) ? $zf : 1/$zf;
		
		$ow = imagesx($image);
		$oh = imagesy($image);

		if ($xc == '') $xc = floor($ow/2);
		if ($yc == '') $yc = floor($oh/2);
		
		if ($zw < 1) $zw = $ow;
		if ($zh < 1) $zh = $oh;
		
		# lets get the transparency of th eold image
		# default to white background if no transparency
		
		$bgcolor = imagecolortransparent ($image);
		$rgb_color = array();
		if ($bgcolor == -1) {
			$rgb_color = array('red'=>255,'green'=>255,'blue'=>255);
			$ztrans=0;
		}
		else {
			$rgb_color = imagecolorsforindex($bgcolor);
			$ztrans=1;
		}
		
		if ($this->GDVersion < 2) {
			$zimage= imagecreate($zw,$zh);
		}
		else {
			$zimage = @ImageCreateTrueColor($zw,$zh) or $zimage= imagecreate($zw,$zh);
		}
		if (!zimage) return '';
		
		# set background
		$bgcolor = imagecolorallocate($image, $rgb_bgcolor['red'],$rgb_bgcolor['green'],$rgb_bgcolor['blue']);
		if ($ztrans) imagecolortransparent($image, $bgcolor);
		$zx1 = floor($xc -  $zw/2/$zfactor);
		$zy1 = floor($yc - $zh/2/$zfactor);

		

		if ($zx1<0) {
			$dstx = -$zx1*$zfactor;$srcx =0;
			$dstw = $zw - $dstx; $srcw = floor($dstw/$zfactor);
		}
		else {
			$dstx = 0; $srcx = $zx1;
			$srcw = $ow-$srcx - floor(((($ow - $srcx)*$zfactor)-$zw)/$zfactor);
			if ($srcx + $srcw > $ow)$srcw = $ow - $srcx ;
			$dstw = $srcw * $zfactor;
		}

		if ($zy1<0) {
			$dsty = - $zy1*$zfactor; $srcy = 0;
			$dsth = $zh - $dsty; $srch= floor($dsth/$zfactor);
		}
		else {
			$dsty = 0; $srcy = $zy1;
			$srch = $oh - $srcy - floor(((($oh - $srcy)*$zfactor) - $zh)/$zfactor);
			if ($srcy + $srch > $oh) $srch = $oh - $srcy ;
			$dsth = $srch * $zfactor;
		}

		imagecopyresized($zimage, $image,$dstx,$dsty,$srcx,$srcy,$dstw,$dsth,$srcw,$srch);

   		return $zimage;
	}


}


?>