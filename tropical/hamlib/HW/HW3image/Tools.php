<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Tools.php,v 1.5 2007/09/14 00:05:44 wxfyhw Exp $
*
*/


class HWimageTools {

	var $version = '$Id: Tools.php,v 1.5 2007/09/14 00:05:44 wxfyhw Exp $';
	var $GDVersion=1;
	var $debug = 0;

	Function HWimageTools ($gdversion=1, $debug=0) {
		$this->GDVersion = $gdversion;
		$this->debug = $debug;
	}

	Function output_image (&$image, $gd_ext, $filename='',$quality=-1) {
		$gd_ext = strtolower($gd_ext);

		if ($gd_ext == 'png') {
			$use_quality = ($quality >= 0) ? $quality : 0;
			if ($filename) {imagepng($image, $filename, $use_quality); } else {imagepng($image);}
		}
		elseif ($gd_ext == 'jpg' || $gd_ext == 'jpeg') {
			$use_quality = ($quality >= 0) ? $quality : 100;
			if ($filename) {imagejpeg($image, $filename, $use_quality); } else {imagejpeg($image);}
		}
		elseif ($gd_ext == 'gif') {
			if ($filename) {imagegif($image, $filename); } else {imagejpeg($image);}
		}
		if ($filename) {
			if (file_exists($filename)) { @chmod ("$filename",0666);}
			else { return array(0,"Error saving image $filename"); }
		}

		return array(1,'');
	}


	Function save_image (&$image, $map_path, $name, $gd_ext, $quality = -1) {
		return $this->output_image ($image, $gd_ext, "$map_path/$name", $quality);
	}



	/*

	######################################################################
	###
	###  Rotate (and optionally mirror) an image
	###
	###   This code was originally found in gifc/imc was "borrowed"
	###    and modified Dec 2000 by HAMweather, LLC  for use with
	###   HW3 frontalmap plugin
	######################################################################

	# This function returns a new image, which is the rotation (of
	# optionally the horizonally mirrored image) of the given image over
	# the given angle.
	# This is a "slow" implementation: to be really fast, it should be in
	# C, but then it should be implemented in the gd library, which is
	# not mine...
	# php supports a version of this in php >= 4.3.0 so not allwill have it
	# and have seen issues with it as well so for now will continue
	# to use this slower version

	*/

	Function rotate_and_mirror_image(&$image, $angle, $mirror, $x1, $y1,$x2,$y2){

		$angle %= 360;
		$rad_angle = $angle * atan2(1,1) / 45;   # degrees to radians
		$cos = cos($rad_angle); $abscos = abs($cos);
		$sin = sin($rad_angle);$abssin = abs($sin);

		$size = array(); $newsize= array();


		$size[0] = imagesx($image); $size[1] = imagesy($image);
		$newsize[0] = round($size[0] * $abscos + $size[1] * $abssin);
		$newsize[1] = round($size[0] * $abssin + $size[1] * $abscos);

		if ($this->GDVersion < 2) { $new_image = imagecreate($newsize[0],$newsize[1]); }
		else { $new_image = @ImageCreateTrueColor($newsize[0],$newsize[1]) or $new_image= imagecreate($newsize[0],$newsize[1]);}

		$im_trans = imagecolortransparent($image);
		if ($im_trans == -1) {
			$trans_bg = imagecolorallocate($new_image, 255,255,255);
		}
		else {
			$trgb =  imagecolorsforindex($image, $im_trans);
			$trans_bg  = imagecolorallocate($new_image, $trgb['red'],$trgb['green'],$trgb['blue']);
		}
		$trgb =  imagecolorsforindex($new_image, $trans_bg);

		imagecolortransparent($new_image, $trans_bg);
		$xt = round($size[0]/2); $yt=0;

		$new_orig = array();
		if ($angle < 90) {
			$new_orig[0] = round($size[0] * $abssin);
			$new_orig[1] = 0;
		}
		elseif ($angle < 180) {
			$new_orig[0] = $new_size[0] -1;
			$new_orig[1] = round($new_size[1] * $abscos);
		}
		elseif($angle < 270) {
			$new_orig[0] = $new_size[0] - 1 - round($new_size[1] * $abssin);
			$new_orig[1] = $new_size[1] -1;
		}
		else {
			$new_orig[0] = 0;
			$new_orig[1] = $new_size[1] - 1 - round($size[1] * $abscos);
		}

		$offsets = array(0,0);
		for ($xa = 0, $xc = $xa - $new_orig[0], $xccos = $xc * $cos, $xcsin = $xc * $sin;
	     		$xa < $new_size[0];
	     		$xa++, $xc++, $xccos += $cos, $xcsin += $sin) {

	     	$inside ='';
	     	for ($ya = 0, $yc = $ya - $new_orig[1], $yccos = $yc * $cos, $ycsin = $yc * $sin;
					$ya < $new_size[1];
					$ya++, $yc++, $yccos += $cos, $ycsin += $sin) {

				$x = round($xccos +$ycsin);
				$xx = floor($xccos+$ycsin);

				if (($x==$xt || $xx == $xt) && ($yy == $yt || $y==$yt)) {$offsets = array($xa,$ya); }
				if (($x < 0) or ($x > $size[0])) {
					if ($inside != '') { continue 2; } { $continue;}
				}

				if ($mirror == 1) {
					$x = $size[0] - 1 - $x;
					$xx = $size[0] - 1 - $xx;
				}

				$y = round(- $xcsin +$yccos);
				$yy = floor(-$xcsin+$yccos);
				if (($x==$xt || $xx == $xt) && ($yy == $yt || $y==$yt)) {$offsets = array($xa,$ya); }
				if (($y < 0) or ($y > $size[1])) {
					if ($inside != '') { continue 2; } { $continue;}
				}

				$inside = '1';

				$col = imagecolorat($image, $x,$y);
				$rgb = imagecolorsforindex($image, $col);

				if (!($rgb['red'] == $trgb['red'] && $rgb['green']== $trgb['green'] && $rgb['blue']== $trgb['blue'])) {
					$col = imagecolorexact($new_image, $rgb['red'],$rgb['green'],$rgb['blue']);
					if ($col == -1) { $col = imagecolorallocate($new_image, $rgb['red'],$rgb['green'],$rgb['blue']);}
					imagesetpixel($new_image, $xa, $ya, $col);
				}
			}
		}

		return array ($new_image, $offsets);
	}


}






?>