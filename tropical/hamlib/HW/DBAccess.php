<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: DBAccess.php,v 1.13 2005/08/15 12:28:16 wxfyhw Exp $
*
*/


	Function new_dbaccess($method, &$cfg, $path,$debug=0){
		if ($debug) print "DBmethod requested: $method<br>\n";
		if (strtoupper($method) == 'SQL') {
			if ($debug) {print "database_type=" . ($cfg->val('Database Access', 'database_type')) . "<br>\n";}
			$method = 'HW' . $cfg->val('Database Access', 'database_type');
		}
		else { $method = 'Flatfile'; }
		if ($debug) {print "Loading DB file: DBAccess/$method.php<br>\n";}
		require_once(HAMLIB_PATH ."HW/DBAccess/$method.php");

		return new $method($cfg, $path, $debug);
	}

?>