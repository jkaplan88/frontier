<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: HWDistance.php,v 1.1 2007/09/13 20:47:52 wxfyhw Exp $
*
*/

define ('PI2', 2*pi());
define ('DR', PI2/360);
define ('RD', 360/PI2);
define ('RADIUS_EARTH', 3963);
define ('_a', 6378.14);
define ('_f', 1/298.257);
define ('_E', 0.08181922);
define ('M2KM', 1.609);

class HWDistance {

	var $debug=0;

	var $cfg;
	var $var = array();


	Function HWDistance () {

	}

	Function Debug ($mode) {
		$this->debug = $mode;
	}

	Function calc_latlon_from_distance ($lat0,$lon0,$dis,$bear) {
		$dis *= M2KM;


		$lat0 = deg2rad($lat0);
		$lon0 = deg2rad($lon0);
		$bear = deg2rad($bear);




		$R =_a*(1 - _E*_E)/pow(1 - _E*_E*pow(sin($lat0),2),1.5);

		$psi = $dis/$R;
		$phi = pi()/2 - $lat0;
		$arccos = cos($psi)*cos($phi) + sin($psi)*sin($phi)*cos($bear);
		$lat2 = rad2deg(pi()/2 - acos($arccos));

		$arcsin = sin($bear)*sin($psi)/sin($phi);
		$lon2 = rad2deg($lon0 - asin($arcsin));

		return array($lat2,$lon2);
	}

//	function acos($v) {
//		return atan2(sqrt(pow((1-$v),2)), $v);
//	}
//
//	function asin($v) {
//		return atan2($v, sqrt(1-$v*$v));
//	}

} // end class

?>
