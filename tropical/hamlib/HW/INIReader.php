<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: INIReader.php,v 1.15 2005/02/15 03:44:10 wxfyhw Exp $
*
*/

class INIReader {

	/***************************************************
	/Class Data
	/**************************************************/
	var $sections = array();	//Hash of hashes, stores settings
	var $current_section;		//Current section being added to
	var $loaded_files;		//Comma Delim list of loaded files
	var $config_dir = '';		//Dir to look in for ini files
	var $ignore_case = false;	//Ignore case when looking for value
	var $debug=0; // Debug mode
	var $iniext = '.ini';


	var $stuff = "";

	Function INIReader($config_dir, $iniext, $debug) {
		$this->loaded_files = '';
		$this->config_dir = $config_dir;
		$this->iniext = $iniext;
		$this->debug = $debug;

	}

      Function Debug($mode) {
         $this->debug = $mode;
      }




	/***************************************************
	/Function SetConfigDIR
	/$dir - the new config dir
	/Set the config directory to use for loading files
	/**************************************************/
	Function SetConfigDIR($dir) {
		$this->config_dir = $dir;
	}



	/***************************************************
	/Function IgnoreCase
	/$case - true or false, ignore case or not
	/Set the config directory to use for loading files
	/**************************************************/
	Function IgnoreCase($case) {
		$this->ignore_case = $case;
	}

	/***************************************************
	/Function ReadINI
	/$filename - the filename of the ini file
	/Returns error message (none if operation went ok)
	/Reads an INI file and stores it's information in the class hashes
	/**************************************************/
	Function ReadINI($filename) {

		//Check for file existance w/o adding the include dir
		if (!file_exists($filename) || !is_file($filename)) {
			$filename = $this->config_dir.'/'.$filename;
			if (!file_exists($filename)) {
			   $filename = $filename . '.' . $this->iniext;
			   if (!file_exists($filename)) {
				if ($this->debug) { print "Error: file '$filename' could not be found<br>\n"; }
				return "Error: file '$filename' could not be found";
			   }
			}
		}

         if ($this->debug) { print "Loading INI file $filename<br>\n";}

		//Open the file and place it's contents in a line by line array
		if (!$file_contents = file($filename)) {
		      if ($this->debug) { print "Error: file '$filename' could not be found<br>\n"; }
			return "Error: File could not be read";
		}

		//Get JUST the filename if the full path is included
		$filename_only = strrchr($filename, "/");
		if ($filename_only == false) { $filename_only = strrchr($filename, "/"); }
		if ($filename_only == false) {
			$filename_only = $filename;
		} else {
			$filename_only = substr($filename_only, 1);
		}

		//Check if the filname if already in the file list
		if (strchr($this->loaded_files, $filename_only.",") == false) {
			$this->loaded_files = $this->loaded_files.$filename_only.",";
		} else {
			return "";
		}



		//Loop through the lines and take appropriate action based on the line
		foreach ($file_contents as $line_num => $line) {
			$line = trim($line);
//print "line=$line<br>\n";
			if (substr($line,0,1) == ';' || substr($line, 0, 1) == '#') continue;

			//Line begins with '[' and ends with ']' - a section header
			if (substr($line, 0, 1) == "[" && substr($line, -1) == "]") {
				//If the section does not exist, create it
				$name = substr($line, 1, strlen($line)-2);
				if ($this->ignore_case == true) { $name = strtolower($name);}
				if (! isset($this->sections[$name])) {
					$this->sections[$name] = array();
				}
				//Set this section to the current section
				$this->current_section = $name;
			}
			//Command line, begins and ends with %%
			elseif (substr($line, 0, 2) == "%%" && substr($line, -2) == "%%") {
				$command_line = substr($line, 2, strlen($line)-4);
				//Determine Command Type
				//LOAD_CONFIG, load a new ini file
				if (substr($command_line, 0, 11) == "LOAD_CONFIG") {
					$file_to_load = substr($command_line, 12);
					$this->ReadINI($file_to_load);
				}
				//KILL_SECTION, deletes all settings for current section
				elseif (substr($command_line, 0, 12) == "KILL_SECTION") {
					$this->sections[$this->current_section] = array();
				}
				//KILL_ALL, deletes all settings for all sections
				elseif (substr($command_line, 0, 8) == "KILL_ALL") {
					$this->sections = array();
				}
			}
			//If the line is not blank then it's a setting
			elseif ($line != '' && strpos($line, "=") !== false) {
				//Split the setting line in two based on =
				$equal_pos = strpos($line, "=");
				$key = substr($line, 0, $equal_pos);
				$value = substr($line, $equal_pos+1);
                     $this->set($this->current_section, $key, $value);
			}
		}

		return "";
	}

   Function set($section, $key, $value) {
      if ($section == '' || $key == '') { return null ; }
      $section = trim ($section);
      $key = trim($key);

      if ($this->ignore_case == true) {
         $section = strtolower($section);
         $key = strtolower($key);
      }

      if (! isset($this->sections[$section])) {
         $this->sections[$section] = array();
      }

      $value = preg_replace('/%%INI:([^:]+?):([^%]+)%%/e',
				"\$this->Val('$1','$2');",
				$value);
if ($this->debug) {print "Set section=[$section]<br>...key=$key<br>...value=$value<br>\n";}
      $this->sections[$section][$key] = trim($value);

      return 1;

   }


	/***************************************************
	/Function Val
	/$section - the section the setting is in
	/$key - the key of the value requested
	/Returns value or "" if not found
	/Requests a setting based on section and key
	/**************************************************/
	Function Val($section, $key) {
		if ($this->ignore_case == true) {
			$section = strtolower($section);
			$key = strtolower($key);
		}
		if (isset( $this->sections[$section][$key])) {
			return $this->sections[$section][$key];
		}
		else {
			return "";
		}
	}

	/***************************************************
	/Function SectionsParams
	/$section - the section to get the parameters for
	/Returns the hash of parameters or null if no hash found for $section
	/**************************************************/
	Function SectionParams($section) {
		if ($this->ignore_case == true) { $section = strtolower($section); }

		if (isset($this->sections[$section])) {
			return $this->sections[$section];
		}
		else {
			return null;
		}
	}

	/***************************************************
	/Function Sections
	/Returns the hash of sections
	/**************************************************/
	Function Sections() {
		return array_keys($this->sections);
	}

	/***************************************************
	/Function Parameters
	/$section - the section to get the parameters for
	/Returns an array of the parameter keys in the given section
	/**************************************************/
	Function Parameters($section) {
		$param_array = array();
		$array_count = 0;
		if (isset($this->sections[$section])) {
			foreach($this->sections[$section] as $key=>$val) {
				$param_array[$array_count] = $key;
				$array_count = $array_count + 1;
			}
		}
		return $param_array;
	}
}
?>