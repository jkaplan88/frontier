<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Lambert2.php,v 1.2 2005/05/18 21:51:45 wxfyhw Exp $
*
*/


if (!defined('pi2')) define ('pi2', 2 * pi());





class Lambert2 {

	var $version = '$Id: Lambert2.php,v 1.2 2005/05/18 21:51:45 wxfyhw Exp $';
	var $debug=0;
	var $error='';
	var $info = array();



	Function Lambert2 (&$info, &$args) {

		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;

		if ($debug) print "Albers version:" . $this->version . "<br>\n";

		$this->init_projection($info);
	}



	Function init_projection(&$info) {

		if (isset($info['lm'])) { $this->info['lm'] = abs($info['lm']); }
		elseif (isset($info['left meters'])) { $this->info['lm'] = abs($info['left meters']); }
		else { $this->info['lm'] = 0;}


		if (isset($info['tm'])) { $this->info['tm'] = abs($info['tm']); }
		elseif (isset($info['top meters'])) { $this->info['tm'] = abs($info['top meters']); }
		else { $this->info['tm'] = 0;}

		if (isset($info['rm'])) { $this->info['rm'] = abs($info['rm']); }
		elseif (isset($info['right meters'])) { $this->info['rm'] = abs($info['right meters']); }
		else { $this->info['rm'] = 0;}

		if (isset($info['bm'])) { $this->info['bm'] = abs($info['bm']); }
		elseif (isset($info['bottom meters'])) { $this->info['bm'] = abs($info['bottom meters']); }
		else { $this->info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		if (isset($info['sp1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['sp1']); }
		elseif (isset($info['standard parallel 1'])) { $slat1 = $this->info['slat1'] = deg2rad($info['standard parallel 1']); }
		else { $slat1 = $this->info['slat1'] = 0;}

		if (isset($info['sp2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['sp2']); }
		elseif (isset($info['standard parallel 2'])) { $slat2 = $this->info['slat2'] = deg2rad($info['standard parallel 2']); }
		else { $slat2 = $this->info['slat2'] = 0;}

		if (isset($info['cm'])) { $cm = $this->info['cm'] = deg2rad($info['cm']); }
		elseif (isset($info['central meridian'])) { $cm = $this->info['cm'] = deg2rad($info['central meridian']); }
		else { $cm = $this->info['cm'] = 0;}

		if (isset($info['rlat'])) { $rlat = $this->info['rlat'] = deg2rad($info['rlat']); }
		elseif (isset($info['reference latitude'])) { $rlat = $this->info['rlat'] = deg2rad($info['reference latitude']); }
		else { $rlat = $this->info['rlat'] = 0;}

		if (isset($info['semi-major axis'])) { $a = $this->info['a'] = $info['semi-major axis']; }
		else { $a = $this->info['a'] = 6378206.4000000004;}

		if (isset($info['flattening'])) { $f = $this->info['f'] = $info['flattening']; }
		else { $f = $this->info['f'] = (1/294.97870);}

		if (isset($info['rho'])) { $rho = $this->info['rho'] = $info['rho']; }
		else { $rho = $this->info['rho'] = 63710000;}




		//#Lambert2 stuff here
		if (($info['tm'] > 0 && $info['bm'] > 0) || ($info['tm'] < 0 && $info['bm'] < 0)) {
			$yscale = $this->info['yscale'] = (abs($this->info['tm'] - $this->info['bm']))/$this->info['height'];
		}
		else {
			$yscale = $this->info['yscale'] = (abs($this->info['tm']) + abs($this->info['bm']))/$this->info['height'];
		}

		if (($info['lm'] > 0 && $info['rm'] > 0) || ($info['lm'] < 0 && $info['rm'] < 0)) {
			$xscale = $this->info['xscale'] = (abs($this->info['lm'] - $this->info['rm']))/$this->info['width'];
		}
		else {
			$xscale = $this->info['xscale'] = (abs($this->info['lm']) + abs($this->info['rm']))/$this->info['width'];
		}

		$this->info['ww'] = $this->info['lm']/$xscale;
		$this->info['hh'] = $this->info['tm']/$yscale;

		//More strict Lambert2 Stuff
		list ($m1,$t1) = $this->_fetch_lambert2_constants($slat1);
		list ($m2,$t2) = $this->_fetch_lambert2_constants($slat2);
;
		$n =  (log($m1) - log($m2))/(log($t1) - log($t2));

		$this->info['n'] = $n;
		$F =  $m1/($n*pow($t1,$n));
		$this->info['F'] = $F;


		list ($m0,$t0) = $this->_fetch_lambert2_constants($rlat);
		$r0 = $this->info['r0'] = $a * $F * pow($t0,$n);



		return 1;
	}



	Function get_xy ($lon, $lat) {


		$lon = deg2rad($lon);
		$lat = deg2rad($lat);

		$a = $this->info['a'];
		$n = $this->info['n'];
		$F = $this->info['F'];
		$cm = $this->info['cm'];
		$r0 = $this->info['r0'];

		list ($m,$t) = $this->_fetch_lambert2_constants($lat);

      	$theta = $n * ($lon - $cm);
      	$r = $a * $F * pow($t,$n);

      	$x_ns = ($r * sin($theta));
      	$y_ns = ($r0 - $r * cos($theta));



      	$lm = $this->info['lm'];
      	$tm = $this->info['tm'];
      	$xm=($lat < $cm) ? -1+($lm-$x_ns)/$lm : 1-($lm-$x_ns)/$lm;

      	$ym = ($lat < $this->info['rlat']) ? 1-($tm-$y_ns)/$tm : -1+($tm-$y_ns)/$tm;


      	$x = ($lm+$x_ns)/$this->info['xscale'] ;
      	$y = ($tm-$y_ns)/$this->info['yscale'] ;



      	$x = floor($x);
      	$y = floor($y);



      	return array($x,$y);

	}







	Function _fetch_lambert2_constants($lat) {
   		$sin_lat = sin($lat);
   		$sin2_lat = $this->sin2($lat);
		$a = $this->info['a'];
		$f = $this->info['f'];



   		$esq = $f * (2-$f);
   		$e = sqrt($esq);
   		$m = cos($lat)/ pow(1-$esq*$sin2_lat,.5);
   		$t = tan(pi()/4-$lat/2)/(pow((1-$e*$sin_lat)/ (1+$e*$sin_lat),$e/2));


   		return array($m, $t);

	}


	Function cos2 ($num) {
		$cos = cos($num);
		return $cos * $cos;
	}


	Function sin2 ($num) {
		$sin = sin($num);
		return $sin * $sin;
	}




}


?>