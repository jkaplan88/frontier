<?php

/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Mercator.php,v 1.1 2007/09/13 21:08:54 wxfyhw Exp $
*
*/



if (!defined('pi2')) define ('pi2', 2 * pi());




class mercator {

	var $version = '$Id: Mercator.php,v 1.1 2007/09/13 21:08:54 wxfyhw Exp $';
	var $debug=0;
	var $error='';
	var $info = array();



	Function mercator (&$info, &$args) {

		$debug = (is_array($args) && isset($args[0])) ? $args[0] : 0;
		$this->debug = $debug;

		if ($debug) print "Grid version:" . $this->version . "<br>\n";

		$this->init_projection($info);
	}


	Function init_projection(&$info) {

		if (isset($info['lm'])) { $this->info['lm'] = $info['lm']; }
		elseif (isset($info['left meters'])) { $this->info['lm'] = $info['left meters']; }
		else { $this->info['lm'] = 0;}

		if (isset($info['tm'])) { $this->info['tm'] = $info['tm']; }
		elseif (isset($info['top meters'])) { $this->info['tm'] = $info['top meters']; }
		else { $this->info['tm'] = 0;}

		if (isset($info['rm'])) { $this->info['rm'] = $info['rm']; }
		elseif (isset($info['right meters'])) { $this->info['rm'] = $info['right meters']; }
		else { $this->info['rm'] = 0;}

		if (isset($info['bm'])) { $this->info['bm'] = $info['bm']; }
		elseif (isset($info['bottom meters'])) { $this->info['bm'] = $info['bottom meters']; }
		else { $this->info['bm'] = 0;}

		if (isset($info['w'])) { $this->info['width'] = $info['w']; }
		elseif (isset($info['width'])) { $this->info['width'] = $info['width']; }
		else { $this->info['width'] = 0;}

		if (isset($info['h'])) { $this->info['height'] = $info['h']; }
		elseif (isset($info['height'])) { $this->info['height'] = $info['height']; }
		else { $this->info['height'] = 0;}

		$lon1 = $this->info['lon1'] = $info['lon1'];
		$lat1 = $this->info['lat1'] = $info['lat1'];
		$lon2 = $this->info['lon2'] = $info['lon2'];
		$lat2 = $this->info['lat2'] = $info['lat2'];
//print "($lon1,$lat1)...($lon2,$lat2)<br>\n";

		if (($lon1 >= 0 && $lon2 >=0) || ( $lon1 <=0 && $lon2 <=0)) {
			$this->info['xscale'] = abs(abs($lon1) - abs($lon2))/$this->info['width'];
		}
		elseif ($lon1 <0 && $lon2 >=0) {
			$this->info['xscale'] = abs(abs($lon1) + abs($lon2))/$this->info['width'];
		}
		elseif ($lon1 >=0 && $lon2 < 0) {
			$this->info['xscale'] = (180-$lon1 + 180-abs($lon2))/$this->info['width'];
		}
		else {
			$this->info['xscale'] = abs(abs(180-$lon1) + abs($lon2))/$this->info['width'];
		}

		if (($lat1 >= 0 && $lat2 >=0) || ($lat1 <=0 && $lat2 <=0)) {
			$this->info['yscale'] = abs(abs($lat1) - abs($lat2))/$this->info['height'];
		}
		else  {
			$this->info['yscale'] = abs(abs($lat1) + abs($lat2))/$this->info['height'];
		}

		$olat1 = $lat1;
		$olat2 = $lat2;
		$lat1 = deg2rad($lat1);
		$lat2 = deg2rad($lat2);

//		$this->info['y_top'] = $ytop = asinh(tan($lat1));//($olat1 == 90) ? pi : .5 * log( (1+ sin ($lat1)) / (1 - sin($lat1)));
//		$this->info['y_bottom'] = $ybottom = asinh(tan($lat2));//($olat2 == -90) ?  -pi :  .5 * log( (1+ sin ($lat2)) / (1 - sin($lat2)));


		$this->info['y_top'] = $ytop = ($olat1 == 90) ? pi : .5 * log( (1+ sin ($lat1)) / (1 - sin($lat1)));
		$this->info['y_bottom'] = $ybottom = ($olat2 == -90) ?  -pi :  .5 * log( (1+ sin ($lat2)) / (1 - sin($lat2)));


		$this->info['yscale'] = ($ytop - $ybottom) / $this->info['height'];

		return 1;
	}

	Function get_lonlat($x,$y) {

		$lon = ($x * $this->info['xscale']) + $this->info['lon1'];
		$lat = $this->info['lat1'] - ($y * $this->info['yscale']);

		return array($lon,$lat);
	}



	Function get_xy ($lon, $lat) {

		$xloc=$yloc=0;
		$lon1 = $this->info['lon1'];
		$lat1 = $this->info['lat1'];
		$lon2 = $this->info['lon2'];
		$lat2 = $this->info['lat2'];

		$ytop = $this->info['y_top'];

		/*
		# if both are same sign then we take difference & devide by scale
		# if lon1 <  = $this->info['lat2'];

		$ytop = $this->info['y_top'];

		/*
		# if both are same sign then we take difference & devide by scale
		# if lon1 < 0 yet lon >= 0 then we add the absolute values
		# to get the total difference then divide by scale
		*/

		if (($lon1 >= 0 && $lon2 >=0)) {
			if ($lon < $lon1) {
				$xloc = 0 - abs($lon1 - $lon)/$this->info['xscale'];
			}
			else {
				$xloc = abs(($lon1) - ($lon))/$this->info['xscale'];
			}
		}
		elseif ( $lon1 <=0 && $lon2 <=0) {
			if ($lon > 0) {
				$xloc = ($lon-360 - $lon1) / $this->info['xscale'];
			}
			elseif ($lon < $lon1) {
				$xloc = (abs($lon1) - abs($lon))/$this->info['xscale'];
			}
			else {
				$xloc = (abs($lon1) - abs($lon))/$this->info['xscale'];
			}

		}
		elseif ($lon1 < 0 && $lon2 >= 0) {
			if ($lon < 0) {
				$xloc = (abs($lon1) - abs($lon))/$this->info['xscale'];
			}
			else {
				$xloc = (abs($lon1) + abs($lon))/$this->info['xscale'];
			}
		}
		elseif ($lon1 >= 0 && $lon2 < 0) {
			if ($lon < 0) {
				$xloc = (180-$lon1 + 180-abs($lon))/$this->info['xscale'];
			}
			elseif ($lon < $lon1) {
				$xloc = ($lon - $lon1)/$this->info['xscale'];
			}
			else {
				$xloc = ($lon - $lon1)/$this->info['xscale'];
			}
		}
		else {
			$xloc = (abs(180-$lon1) + abs($lon))/$this->info['xscale'];
		}


		$yscale = $this->info['yscale'];
		$latrad = deg2rad($lat);
//		$ytemp = (1+ sin ($latrad)) / (1 - sin($latrad));
//		$yns2 = ($ytemp == 0)  ? 0 : .5 * log($ytemp);

		if (!function_exists("asinh")) {
			$ytemp = (1+ sin ($latrad)) / (1 - sin($latrad));
			$yns = ($ytemp == 0)  ? 0 : .5 * log($ytemp);
		}
		else {
			$yns = asinh(tan($latrad));
		}
//		print "ytop=$ytop...yns=$yns...yns2=$yns2<br>\n";

		$yloc = ($ytop-$yns) / $yscale;
//		$yloc = $yns / $yscale;
		$yloc = round($yloc);
		$xloc = round($xloc);


      	return array($xloc, $yloc);


	}



}
?>