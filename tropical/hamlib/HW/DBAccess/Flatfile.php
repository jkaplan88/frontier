<?php
/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2003 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: Flatfile.php,v 1.12 2006/03/11 05:37:23 wxfyhw Exp $
*
* 2005_02_02 - updated check_for_alt_place to allow $country to be passed
* 			and to allow searchiing in "hw3dbs/global/altplaces.txt" if the location
*			is not found in the country dir
*
*		   - Allow searching for alt city in "hw3dbs/global/altcities.txt" if the location
*			is not found in the countries dir.
*/

class Flatfile {
	var $db_path = "";
	var $country = "us";

	var $cfg;
	var $debug=0;

function str_to_upper($str){
   return strtr($str,
   "abcdefghijklmnopqrstuvwxyz".
   "\x9C\x9A\xE0\xE1\xE2\xE3".
   "\xE4\xE5\xE6\xE7\xE8\xE9".
   "\xEA\xEB\xEC\xED\xEE\xEF".
   "\xF0\xF1\xF2\xF3\xF4\xF5".
   "\xF6\xF8\xF9\xFA\xFB\xFC".
   "\xFD\xFE\xFF",
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
   "\x8C\x8A\xC0\xC1\xC2\xC3\xC4".
   "\xC5\xC6\xC7\xC8\xC9\xCA\xCB".
   "\xCC\xCD\xCE\xCF\xD0\xD1\xD2".
   "\xD3\xD4\xD5\xD6\xD8\xD9\xDA".
   "\xDB\xDC\xDD\xDE\x9F");
}

function str_to_lower($str){
   return strtr($str,
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
   "\x8C\x8A\xC0\xC1\xC2\xC3\xC4".
   "\xC5\xC6\xC7\xC8\xC9\xCA\xCB".
   "\xCC\xCD\xCE\xCF\xD0\xD1\xD2".
   "\xD3\xD4\xD5\xD6\xD8\xD9\xDA".
   "\xDB\xDC\xDD\xDE\x9F",
   "abcdefghijklmnopqrstuvwxyz".
   "\x9C\x9A\xE0\xE1\xE2\xE3".
   "\xE4\xE5\xE6\xE7\xE8\xE9".
   "\xEA\xEB\xEC\xED\xEE\xEF".
   "\xF0\xF1\xF2\xF3\xF4\xF5".
   "\xF6\xF8\xF9\xFA\xFB\xFC".
   "\xFD\xFE\xFF");
}


	/***************************************************
	/Function DBAccess
	/$path - the path where the flatfiles reside
	/Object Constructor, sets the db path
	/**************************************************/
	Function Flatfile(&$cfg, $path, $debug) {
		$this->db_path = $path;
		$this->debug=$debug;
	}


	/***************************************************
	/Function SetCountry
	/$new_country - the country to use
	/Sets the country, used if you dont want the 'us' default
	/**************************************************/
	Function SetCountry($new_country) {
	   	$new_country=trim($new_country);
		if (!$new_country) { $new_country = 'us';}
		$this->country = $this->str_to_lower($new_country);
	}

	/***************************************************
	/Function check_for_alt_place
	/$state - the state to look in
	/$place - the place to look for
	/Returns info from the alt place file for the given parameters
	/**************************************************/
	Function check_for_alt_place($state, $place, $country = '') {

		$debug = $this->debug;

		if ($country == '') $country = $this->country;
		$lstate= $this->str_to_lower(trim($state));
		$lcountry = $this->str_to_lower(trim($country));
		$lplace = $this->str_to_lower(trim($place));
		$file_path = $this->db_path.'/'. $lcountry."/altplaces/". $lcountry ."-". $lstate."-altplaces.txt";

		if ($this->debug)  echo "looking in $file_path<br>\n";
		//Make sure the file exists
		if (!file_exists($file_path)) {
			if ($this->debug) echo "...file not found<br>\n";
//			return "Error locating file $file_path";
		}
		else {
			$res =  $this->SearchFile($file_path, "|", $place);
			if (is_array($res)) return $res; # we found the location
		}

		#lets try the global file
		$file_path = $this->db_path . '/global/altplaces.txt';
		if ($debug) echo "looking  in the global file $file_path<br>\n";
		if (file_exists($file_path)) {

			$search_data = $this->str_to_upper("$lcountry|$lstate|$lplace");
			$search_length = strlen($search_data);
			if ($debug) print "searching for $search_data in file $file_path<br>\n";
			if ($file = fopen ($file_path, "r")) {


				//Search the file for the left line
				while (!feof ($file)) {
				    //Get the next line
				    $line = fgets($file, 4096);
				    //Check this line to see if it's what we're looking for
			               $line=trim($line);
			               if ($line != '') {

					$ra = explode('|', $line);
					if ($debug) print "...$ra[0]|$ra[1]|$ra[2]<br>\n";
					if ($lcountry == $this->str_to_lower($ra[0]) && (($lstate  && $lstate = $this->str_to_lower($ra[1])) || !$lstate) && $lplace == $this->str_to_lower($ra[2])) {
			               		array_shift($ra); #remove country
			               		array_shift($ra); # remove state
			               		if ($debug) print "...found $line<br>\n";
			               		fclose($file);
			               		return $ra;
			               	}


			               }
				}

				//Close the file
				fclose ($file);
			}
			elseif ($this->debug) { print "file does not exist<br>\n"; }
		}

		return "Entry not found";


	}

	/***************************************************
	/Function get_icao_data
	/$icao - the icao code to search for
	/Returns the info for the given icao code
	/**************************************************/
	Function get_icao_data($icao) {
		$file_path = $this->db_path."/icao/icaos.txt";
		if ($this->debug)  echo "print looking in $file_path<br>\n";
		//Make sure the file exists
		if (!file_exists($file_path)) {
			if ($this->debug) echo "...file not found<br>\n";
			return "Error locating file $file_path";
		}

		return $this->SearchFile($file_path, "|", $icao);
	}

	/***************************************************
	/Function check_for_city
	/$city - the city to look for
	/Searches for the given city name, can return multiple finds in array or arrays
	/**************************************************/
	Function check_for_city($city) {
		$file_path = $this->db_path."/".$this->country."/altcities/city-places-".$this->str_to_lower(substr($city, 0, 1)).".txt";
		if ($this->debug)  echo "print looking in $file_path<br>\n";
		$cities = array();
		$total = 0;

		$scity = $this->str_to_upper($city);
		if (preg_match('/^MC\S/', $city)) { $scity .= '|MC ' . substr($city,2); }
		if (preg_match('/ COUNTY/', $city)) { $scity .= "|$city COUNTY"; }


		//Make sure the file exists
		if (!file_exists($file_path)) {
			if ($this->debug) echo "...file not found<br>\n";
			return "Error locating file $file_path";
		}

		//Open the alt places file
		$file = fopen ($file_path, "r");

		//Search the file for the right line

		while (!feof ($file)) {
		    //Get the next line
		    $line = fgets($file, 4096);

		    //Check this line to see if it's what we're looking for
		    if (preg_match('/^'.$city.'\|/i', $line)) {
		    	$line = preg_replace('/\s+$/', '', $line);
		    	$t = explode('|', $line);
		    	$cities[$total*3]=$t[1]; $cities[$total*3+1] =$t[2]; $cities[$total*3+2] =$t[3];
		    	$total++;
		    }
		}

		//Add check for all cities file, did not have a flatfile for this

		//Close the file
		fclose ($file);

		// check hw3dbs/global/altcities.txt if exists
		$file_path = $this->db_path."/global/city-places.txt";
		if ($this->debug)  echo "print looking in $file_path<br>\n";
		if (file_exists($file_path)) {
			$file = fopen ($file_path, "r");
			while (!feof ($file)) {
				//Get the next line
				$line =fgets($file, 4096);

				if (preg_match('/^' . $city . '\|/i', $line)) {
					$t = explode('|', $line);
				    	$cities[$total*3]=$t[1]; $cities[$total*3+1] =$t[2]; $cities[$total*3+2] =$t[3];
				    	$total++;
				}
			}
			fclose($file);
		}
		else{
			if ($this->debug) print "no global file...skipping<br>\n";
		}





		return  array($total,$cities);



	}

	/***************************************************
	/Function get_zipcode_info
	/$zip - the zipcode to look for
	/Returns info on the given zipcode
	/**************************************************/
	Function get_zipcode_info($zip) {
		$file_path = $this->db_path.'/'.$this->country.'/zipcode/'.$this->country."-".substr($zip, 0, 2).".txt";
		if ($this->debug)  echo "print looking in $file_path<br>\n";
//print "$file_path<br>\n";
		//Make sure the file exists
		if (!file_exists($file_path)) {
			if ($this->debug) echo "...file not found<br>\n";
			return "Error locating file $file_path";
		}

		return $this->SearchFile($file_path, ",", $zip);

	}

	/***************************************************
	/Function get_place_from_fips
	/$fips - the fips code to look for
	/Returns data found for given fips
	/**************************************************/
	Function get_place_from_fips($fips) {
		//Setup the fips code hash
		$state_fips = array('01'=>'AL', '02'=>'AK', '04'=>'AZ', '05'=>'AR', '06'=>'CA', '08'=>'CO', '09'=>'CT', '10'=>'DE', '11'=>'DC',
	      	'12'=>'FL', '13'=>'GA', '15'=>'HI', '16'=>'ID', '17'=>'IL', '18'=>'IN', '19'=>'IA', '20'=>'KS', '21'=>'KY', '22'=>'LA', '23'=>'ME',
	      	'24'=>'MD', '25'=>'MA', '26'=>'MI', '27'=>'MN', '28'=>'MS', '29'=>'MO', '30'=>'MT', '31'=>'NE', '32'=>'NV', '33'=>'NH',
	      	'34'=>'NJ', '35'=>'NM', '36'=>'NY', '37'=>'NC', '38'=>'ND', '39'=>'OH', '40'=>'OK', '41'=>'OR', '42'=>'PA', '44'=>'RI',
	      	'45'=>'SC', '46'=>'SD', '47'=>'TN', '48'=>'TX', '49'=>'UT', '50'=>'VT', '51'=>'VA', '53'=>'WA', '54'=>'WV', '55'=>'WI', '56'=>'WY' );

	      	$file_path = $this->db_path."/".$this->country."/counties/".$this->country."-".$this->str_to_lower($state_fips[substr($fips, 0, 2)])."-counties.txt";

		//Make sure the file exists
		if (!file_exists($file_path)) {
			return "Error locating file $file_path";
		}

		$ta = $this->SearchFile($file_path, ",", $fips);
		if (is_array($ta)) {
			$ta[5] = $this->str_to_lower($state_fips[substr($fips, 0, 2)]);
		}
		return $ta;
	}

	Function get_place_from_zone($zone, $state) {
		$file_path = $this->db_path.'/'.$this->country.'/zones/'.$this->country.'-zones.txt';
		if (!file_exists($file_path)) {
			return "Error locating file $file_path";
		}

		$ta = $this->SearchFile($file_path,',',$zone);
		return $ta;
	}


	/***************************************************
	/Function match_state
	/$name - a state name or a 2 letter state abbreviation
	/If passed a name, return it's abbreviation, if passed
	/ an abbreviation, returns the state mane
	/**************************************************/
	Function match_state($name, $country='') {
		//US and Canada have different states and abbrevs
		$country = ($country == '') ? $this->country : $this->str_to_lower($country);

		if ($this->country == "us") {

			$state_abbrevs = array (
		         'al', 'ak', 'az', 'ar', 'ca', 'co', 'ct', 'de', 'fl', 'ga', 'hi',
		         'id', 'il', 'in', 'ia', 'ks', 'ky', 'la', 'me', 'md', 'ma', 'mi', 'mn',
		         'ms', 'mo', 'mt', 'ne', 'nv', 'nh', 'nj', 'nm', 'ny', 'nc', 'nd', 'oh',
		         'ok', 'or', 'pa', 'ri', 'sc', 'sd', 'tn', 'tx', 'ut', 'vt', 'va', 'wa',
		         'wv', 'wi', 'wy', 'us', 'dc', 'pr', 'vi' );

		         $state_names = array ('alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'florida',
		         'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts',
		         'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new hampshire', 'new jersey', 'new mexico',
		         'new york', 'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode island',  'south carolina', 'south dakota',
		         'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west virginia', 'wisconsin', 'wyoming', 'united states', 'd.c.',
		         'puerto rico', 'virgin islands' );
		}
		else {
			$state_abbrevs = array ('ab', 'bc', 'pe', 'pe', 'mb', 'nb', 'ns', 'on', 'qc', 'qc', 'sk', 'nf', 'nf', 'nt', 'nu', 'yk', 'yk' );
			$state_names = array (
		         'alberta', 'british columbia', 'prince edward island', 'prince edward', 'manitoba', 'new brunswick',
		         'nova scotia', 'ontario', 'qu�bec', 'quebec', 'saskatchewan', 'newfoundland', 'new foundland', 'northwest territories',
		         'nunavut', 'yukon territory', 'yukon' );
		}
$name = $this->str_to_lower($name);

		//Now loop through each array and find a match
		for ($i = 0; $i < count($state_abbrevs); $i++) {
			if ($state_abbrevs[$i] == $name) {
				return array($name, $state_names[$i]);
			}
			elseif ($state_names[$i] == $name) {
				return array($state_abbrevs[$i], $name);
			}
		}

		//Otherwise return what the name parameter was
		return array('', '');
	}

	Function match_country($name) {
		$name = $this->str_to_upper(trim($name));
		$debug = $this->debug;

		$file_path = $this->db_path."/global/countries.txt";
		if ($this->debug)  echo "print looking in $file_path for $name<br>\n";

		//Make sure the file exists
		if (!file_exists($file_path)) {
			if ($this->debug) echo "...file not found<br>\n";
			return array('','');
		}

		//Open the alt places file
		$file = fopen ($file_path, "r");

		//Search the file for the right line

		while (!feof ($file)) {
			//Get the next line
			$line = fgets($file, 4096);

			//Check this line to see if it's what we're looking for
			$t = explode('|', $this->str_to_upper(trim($line)));
			if ($debug) print "...$t[0]|$t[1]<br>\n";
			if (isset($t[0]) && isset($t[1])  && ($t[0] == $name || $t[1] == $name)) {
				fclose($file);
				if (isset($t[2])) {
					return array ($t[0],$t[2]);
				}
				else {
					return array ($t[0],$t[1]);
				}
			}
		}
		fclose($file);


		return array('','');
	}


	/***************************************************
	/Function SearchFile
	/$filename - the name of the flat file to search
	/$delim_char - the delim character of the flatfile
	/$search_data - what is being searched for in the flatfile
	/Searched the given file for the given data, if found returns an array
	/of all the data on the matching line
	/**************************************************/
	Function SearchFile($filename, $delim_char, $search_data) {
		//Open the file

		$search_data = $this->str_to_upper($search_data);
		$debug = $this->debug;


		if ($debug)print "searching for $search_data in file $filename<br>\n";
		if ($file = fopen ($filename, "r")) {
			//Search the file for the right line
			while (!feof ($file)) {
			    //Get the next line
			    $line = fgets($file, 4096);
			    //Check this line to see if it's what we're looking for
		               if ($line != '') {
		               	$ra = explode($delim_char, trim($line));
		               	if ($debug) print "...".$ra[0]."<br>\n";
		                   if ($this->str_to_upper($ra[0]) == $search_data) {
		                   	//$line = preg_replace('/\s+$/', '', $line);
		                   	if ($debug) print "...found $line<br>\n";
		                   	fclose($file);
		                   	return $ra;
		                   }
		               }
			}

			//Close the file
			fclose ($file);
		}
		elseif ($debug) { print "file does not exist<br>\n"; }

		return "Entry not found";
	}

	Function get_airport_data($airport) { return ''; }

}
?>