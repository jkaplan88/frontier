<?php
//User id - specific to your account
$userid='1T8Z8PCsGHAX';
//if you want to allow the map to be clickable to your HW install
// set the fillowing to the url of HW:
// i.e. $hwurl='http://www.hamweather.net/cgi-bin/hw3/hw3.cgi';
$hwurl='http://www.frontierweather.com/weather/hw3.php';
// NORMALLY YOU SHOULD NOT NEED TO CHANGE ANYTHING BELOW THIS LIN


/*
#######################
#  HAMweather 3
#  This script is copyright(c) 1997-2005 by HAMweather, LLC all rights reserved.
#  It is subject to the license agreement that can be found at the following
#  URL: http://www.hamweather.net/hw3/license.shtml
#######################
*
* Revision:
* $Id: warnfetch.php,v 1.2 2005/04/29 20:45:11 wxfyhw Exp $
*/

error_reporting( E_ALL );



//url to HW server
$server = "warnings.hamweather.net";
$url = "/wx$userid/";

$cache_dir = './warningmaps';
$cache_url = 'warningmaps';

$size_default = '480x360';
$max_age_default=5;

// default output type
// img=fetch and output an image
// js=javascript/html
// html= html image tag
// none= no output
$default_output = 'img';

$default_clickable=1;


// used to control setting the allowed referers
$referer_file = 'referer.php';
$reject_url = '';

$ref = strtolower(getenv('HTTP_REFERER'));
if ($ref && file_exists($referer_file)) {
	$referers = array();
	include_once($referer_file);

	$rej=1;
	foreach ($referers as $referer) {
		if ($ref == strtolower($referer)) {
			$rej=0;
			break;
		}
	}

	if ($rej) {
		if ($reject_url) {
			header("Location: $reject_url");
		}
		else { exit; }
	}
}


$om = (isset($om)) ? $om : ((!empty($_GET['om'])) ? strtolower($_GET['om']) : $default_output);

// get the image
$state = (isset($state)) ? $state : ((!empty($_GET['state'])) ? $_GET['state'] : 'us');
$state = preg_replace('/\W/', '', $state);
if (!$state) {$state='us'; }
elseif ($state != 'us' && strlen($state) == 2) { $state = "us$state"; }

// get the type
$size = (isset($size)) ? $size : ((!empty($_GET['size'])) ? $_GET['size'] : '');
if (!preg_match('/^\d\d\d+x\d\d\d+$/', $size)) $size = $size_default;

// is this the key image?
$iskey = (isset($iskey) && $iskey) ? '_key' : ((!empty($_GET['key']) && $_GET['key']) ? '_key' : '');

$ma = $max_age_default;


// if we are simply fetching and image an outputting we are done
if ($om == 'img' || $om=='') {
	$cache_img_name = $state . '_' . $size . $iskey . '.png';
	if ((file_exists("$cache_dir/$cache_img_name") &&	((time() - filemtime("$cache_dir/$cache_img_name"))/60) < $ma)) {
		header("Location: $cache_url/$cache_img_name");
		exit;
	}
	# fetch the image
	$image =& fetch_and_save("$cache_dir/$cache_img_name", $server, "$url$size/$state$iskey.png");
	header("Content-type: image/png");
	print $image;
	exit;
}

// if we are only fetching the key & warning & leaving
else {
	$cache_img_name = $state . '_' . $size  . '.png';
	$cache_key_name = $state . '_' . $size . '_key.png';
	if (! (file_exists("$cache_dir/$cache_img_name") &&	((time() - filemtime("$cache_dir/$cache_img_name"))/60) < $ma)) {
		fetch_and_save("$cache_dir/$cache_img_name", $server, "$url$size/$state.png");
		if ($iskey) fetch_and_save("$cache_dir/$cache_key_name", $server, "$url$size/$state" . "_key.png");
	}

	if ($om == 'js' || $om=='html') {
		$w=480; $h=360;
		if (preg_match('/^(\d\d\d+)x(\d\d\d+)$/', $size, $m)) {
			$w=$m[1]; $h=$m[2];
		}


		$clickable = (isset($clickable)) ? $clickable : ((!empty($_GET['clickable'])) ? $_GET['clickable'] : 0);
		$imap='';
		if ($clickable) {
			// need to get the key files
			$cache_key_file = $state . '_' . $size . '.key';
			fetch_and_save("$cache_dir/$cache_key_file", $server, "$url$size/$state.key");

			if ($state == 'us') {
				$cache_imap_file = $state . '_' . $size . '.imap';
				if (!(file_exists("$cache_dir/$cache_imap_file")))
					fetch_and_save("$cache_dir/$cache_imap_file", $server, "$url$size/$state.imap");

				if (file_exists("$cache_dir/$cache_imap_file")) {
					if ($file = fopen("$cache_dir/$cache_imap_file", "r")) {
						$imap = fread($file, filesize("$cache_dir/$cache_imap_file"));
						fclose($file);


						if (!$hwurl) {
							$hwurl= $_SERVER['PHP_SELF'];
							$hwparms ="size=$size&om=$om&clickable=$clickable&key=$iskey";
						}
						else {
							$parms ="size=$size&forecast=pass&pass=warningmap";
						}
						$imap = preg_replace('/SCRIPT/', $hwurl, $imap);
						$imap = preg_replace('/PARMS/', $parms, $imap);

						if ($om == 'js') {
							$imap =  preg_replace('/^(.+)$/m', "document.write('$1');", $imap);
						}
						print $imap;
					}
				}
			} // end if ($state == 'us')
		} // end if ($clickable)
		output_html_js($om,"$cache_url/$cache_img_name", $w, $h, $imap,(($iskey) ? "$cache_url/$cache_key_name" : '') );
	}


}





function output_html_js ($om, $image_url, $w, $h, $imap, $key_url='') {

//	if ($om=='js') print "document.write('";
//	print "<html><head></head><body>";
//	if ($om=='js') print "');\n";



	if ($imap) {
		print $imap;
		$imap=" useMap=\"#WARNINGMAP\" border=\"0\"";
	}

	if ($om=='js') print "document.write('";
	print "<img $imap src=\"$image_url\" width=\"$w\" height=\"$h\">";
	if ($om=='js') print "');\n";


	if ($key_url) {
		if ($om=='js') print "document.write('";
		print "<br>";
		if ($om=='js') print "');\n";


		if ($om=='js') print "document.write('";
		print "<img src=\"$key_url\" width=\"$w\" >";
		if ($om=='js') print "');\n";
	}

//	if ($om=='js') print "document.write('";
//	print "</body></html>";
//	if ($om=='js') print "');\n";

}






function fetch_and_save($cachefile, $server, $url, $port = 80) {
	$image =& _getcontent ($server, "$url");
	# save image to fle
	if ($image) {
		if ($file = fopen("$cachefile", "wb")) {
			fputs($file, $image);
			fclose($file);
		}

	}

	return $image;
}


function _getcontent($server, $file, $port =80) {
   $cont = "";
   if (getenv('HTTP_REFERER')) {
   	if (preg_match('/\?/', $file)) { $file .='&'; } else { $file .='?'; }
   	$file .= 'referer=' . urlencode(getenv('HTTP_REFERER'));
   }

   $ip = gethostbyname($server);
   $fp = fsockopen($ip, $port);
   if (!$fp)
   {
       return "Unknown";
   }
   else
   {

	fputs($fp, "GET $file HTTP/1.0\r\n");
	fputs($fp, "Host: $server:$port\r\n");
	fputs($fp, "Accept: text/html, text/plain, image/gif, image/jpg, image/png, */*\r\n");
	fputs($fp, "User-Agent: HWwarnings Image Fetcher/1.0\r\n");
	fputs($fp, "Connection: close\r\n");
	fputs($fp, "\r\n");
	fputs($fp, "\r\n");



       while (!feof($fp))
       {
           $cont .= fread($fp, 8192);
       }
       fclose($fp);
       $cont = substr($cont, strpos($cont, "\r\n\r\n") + 4);

       $cont = preg_replace('/\r\n\r\n$/', '', $cont);

       return $cont;
   }
}





?>