<?php

error_reporting  (E_ALL);

$core_path = dirname(__FILE__) . '/';
$iniext = 'ini.php';

$configs_path = $core_path . 'configs';

require ($core_path . "hamlib/HW/INIReader.php");

//Load up the ini file
$cfg = new INIReader($configs_path, $iniext,0);
$cfg->ReadINI("hw3.$iniext");

$dbtype = $cfg->val('Database Access', 'database_type');


if ($dbtype != 'mysql') {
	print "Database not mysql. This update script is only for the HW3 mysql database. Note no update is required for the HW3 Flat file Database.";
	exit;
}


$dbusername = $cfg->val('Database Access', 'username'); # Username.
$dbpassword = $cfg->val('Database Access', 'password'); # Password.
$dbhostname = $cfg->val('Database Access', 'hostname'); # Hostname.
$dbdatabase =$cfg->val('Database Access', 'database_name'); # Database name.


$dbh = mysql_connect($dbhostname, $dbusername, $dbpassword) or die(mysql_error());
mysql_select_db($dbdatabase,$dbh) or die(mysql_error());


$result = mysql_query("DESCRIBE icaos");
if (!$result) {
   echo 'Describe failed: ', mysql_error();
   exit;
}

while ($row = mysql_fetch_assoc($result)) {

  // Can save column name into an array for later (numeric index)
  $fields_nbr[] = $row['Field'];

  // Or can save column name as key of array and save the column type too
  $fields_assoc[$row['Field']] = $row['Type'];
}

if (!isSet($fields_assoc['wxzone'])) {
	$sql = 'ALTER TABLE icaos ADD wxzone VARCHAR( 6 ) NOT NULL , ADD fips VARCHAR( 5 ) NOT NULL , ADD radaricao VARCHAR( 4 ) NOT NULL';
	mysql_query($sql) || die ("Update failed");
}


?>
<html>
	<head>
		<title>Updating HW3 db to match HW3.98</title>
	</head>
	<body>
		HW3 database structure Successfully Updated.
	</body>
</html>


