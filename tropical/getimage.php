<?php
$hw3_url = 'http://www.frontierweather.com/tropical/hw3.php';
$image_cache_url = 'http://www.frontierweather.com/tropical/images/hwi3_mapset/generated';
$image_cache_path = './images/hwi3_mapset/generated';
$image_ext = 'png';
$max_age = 30; // max cache age in minutes


// do not change below this line
$filename = preg_replace ('/\W+/', '_', $hwvusename);
if (file_exists("$image_cache_path/$filename.$image_ext")) {
	$diff = (time() - filemtime("$image_cache_path/$filename.$image_ext.$image_ext"))/60;
//print "dif=$diff<br>\n";
	if ($diff < $max_age) {
//print "good age<br>\n";
		header("Location: $image_cache_url/$filename.$image_ext");
		exit;
	}
}
//echo "$image_cache_path/$filename.$image_ext does not exists <br>\n";
$querystring = getenv("QUERY_STRING");
header("Location: $hw3_url?$querystring");

?>