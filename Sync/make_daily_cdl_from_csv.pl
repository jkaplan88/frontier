#! /usr/bin/perl

use warnings;
use strict;
use autodie qw/ open chdir /;

# ---------------------------------------------------------------------------------
#
#  make_daily_cdl_from_csv.pl
#
#  Script attempts to make a CDL file out of a .csv file that contains values of
#  something over a global lat-lon grid from lat [-90:90] and lon [0:360] at
#  0.5 degree resolution.
#
# ---------------------------------------------------------------------------------

#  Set up some paths...
my $home_dir = '/Users/jimsouthard/grads/CFSR';
my $scripts_dir = "$home_dir/scripts";
my $csv_dir     = "$home_dir/csv/daily";
my $cdl_dir     = "$home_dir/cdl/daily";

my $start_year = 1979;
my $end_year = 2015;

for (my $year = $start_year; $year <= $end_year; $year++) {

  for (my $month = 1; $month <= 12; $month++) {
    my $end_day;
    if ($month == 2) {
      if (($year % 4) == 0) {
        $end_day = 29;
      }
      else {
        $end_day = 28;
      }
    }
    elsif (($month == 4) || ($month == 6) || ($month == 9) || ($month == 11)) {
      $end_day = 30;
    }
    else {
      $end_day = 31;
    }

    $month = sprintf("%02d", $month) if $month < 10;
    for (my $day = 1; $day <= $end_day; $day++) {
      $day = sprintf("%02d", $day) if $day < 10;

      print "Now making CDL file for ${year}${month}${day}...\n";

      #  Open the output CDL file for writing...
      open OUTFILE, "> $cdl_dir/CFSR_prate_${year}${month}${day}.cdl";

      #  Print headers and definitions and metadata and stuff...
      print OUTFILE "netcdf CFSR_prate_${year}${month}${day} {\n";
      print OUTFILE "dimensions:\n";
      print OUTFILE "	lon=721\;\n";
      print OUTFILE "	lat=361\;\n";
      print OUTFILE "variables:\n";
      print OUTFILE "	double time\;\n";
      print OUTFILE "		time:standard_name = \"time\"\;\n";
      print OUTFILE "		time:units = \"minutes since ${year}-${month}-${day}T00:00:00\"\;\n";
      print OUTFILE "		time:string = \"${year}-${month}-${day}T00:00:00\"\;\n";
      print OUTFILE "		time:period_string = \"0 minutes\"\;\n";
      print OUTFILE "	double forecast_reference_time\;\n";
      print OUTFILE "		forecast_reference_time:standard_name = \"forecast_reference_time\"\;\n";
      print OUTFILE "		forecast_reference_time:units = \"minutes since ${year}-${month}-${day}T00:00:00\"\;\n";
      print OUTFILE "		forecast_reference_time:string = \"${year}-${month}-${day}T00:00:00\"\;\n";
      print OUTFILE "	float latitude(lat, lon)\;\n";
      print OUTFILE "		latitude:least_significant_digit = 1\;\n";
      print OUTFILE "		latitude:standard_name = \"latitude\"\;\n";
      print OUTFILE "		latitude:long_name = \"latitude\"\;\n";
      print OUTFILE "		latitude:units=\"degrees_north\"\;\n";
      print OUTFILE "		latitude:_DeflateLevel = 1\;\n";
      print OUTFILE "	float longitude(lat, lon)\;\n";
      print OUTFILE "		longitude:least_significant_digit = 1\;\n";
      print OUTFILE "		longitude:standard_name = \"longitude\"\;\n";
      print OUTFILE "		longitude:long_name = \"longitude\"\;\n";
      print OUTFILE "		longitude:units = \"degrees_east\"\;\n";
      print OUTFILE "		longitude:_DeflateLevel = 1\;\n";
      print OUTFILE "	float prate(lat,lon)\;\n";
      print OUTFILE "		prate:long_name = \"daily precipitation accumulation\"\;\n";
      print OUTFILE "		prate:units = \"mm\"\;\n";
      print OUTFILE "		prate:_FillValue = -9999.f\;\n";
      print OUTFILE "		prate:missing_data_value = -9999\;\n";
      print OUTFILE "		prate:least_significant_digit = 2\;\n";
      print OUTFILE "		prate:_DeflateLevel = 1\;\n";
      print OUTFILE "\n";
      print OUTFILE "//  global attributes:\n";
      print OUTFILE "	:map_proj = \"lat-lon\"\;\n";
      print OUTFILE "	:nx = 721\;\n";
      print OUTFILE "	:ny = 361\;\n";
      print OUTFILE "	:dx = 0.5\;\n";
      print OUTFILE "	:dy = 0.5\;\n";
      print OUTFILE "	:dlon = 0.5\;\n";
      print OUTFILE "	:dlat = 0.5\;\n";
      print OUTFILE "	:llcrnrlon = 0.\;\n";
      print OUTFILE "	:llcrnrlat = -90.\;\n";
      print OUTFILE "	:urcrnrlon = 360.\;\n";
      print OUTFILE "	:urcrnrlat = 90.\;\n";
      print OUTFILE "	:Sources = \"WDTClimo\"\;\n";
      print OUTFILE "	:DataConvention = \"WE:SN\"\;\n";
      print OUTFILE "	:Conventions = \"CF-1.6\"\;\n";
      print OUTFILE "	:institution = \"Weather Decision Technologies, Inc.\"\;\n";
      print OUTFILE "	:grid_mapping_name = \"latitude_longitude\"\;\n";
      print OUTFILE "\n";
      print OUTFILE "data:\n";
      print OUTFILE "	time = 0\;\n\n";
      print OUTFILE "	forecast_reference_time = 0\;\n\n";

      print OUTFILE "	latitude = -90";
      for (my $lon = 0.5; $lon <= 360; $lon += 0.5) {
        print OUTFILE ", -90";
      }

      for (my $lat = -89.5; $lat <= 90; $lat += 0.5) {
        for (my $lon = 0; $lon <= 360; $lon += 0.5) {
          print OUTFILE ", $lat";
        }
      }
      print OUTFILE "\;\n\n";


      print OUTFILE "	longitude = 0";
      for (my $lon = 0.5; $lon <= 360; $lon += 0.5) {
        print OUTFILE ", $lon";
      }

      for (my $lat = -90; $lat <= 90; $lat += 0.5) {
        for (my $lon = 0; $lon <= 360; $lon += 0.5) {
          print OUTFILE ", $lon";
        }
      }

      print OUTFILE "\;\n\n";          

      print OUTFILE "	prate = ";

      #  Here, read in all the values of prate from the .csv file...
      open INFILE, "< $csv_dir/CFSR_prate_${year}${month}${day}.csv";
      chomp(my @lines = <INFILE>);
      close INFILE;

      my $first = 1;
      foreach my $line (@lines) {
        if ($first) {
          print OUTFILE "$line";
          $first = 0;
        }
        else {
          print OUTFILE ", $line";
        }
      }

      print OUTFILE "\;\n\n";
      print OUTFILE "}\n";

      #  Close the output CDL file...
      close OUTFILE;

      die;

    }  #  for day  #

  }  #  for month  #

}  #  for year  #
