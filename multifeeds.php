<?php
// Include the SimplePie library
require_once('php/simplepie.inc');
 
// Because we're using multiple feeds, let's just set the headers here.
header('Content-type:text/html; charset=utf-8');

$feeds = array(
	'http://rss.news.yahoo.com/rss/topstories',
	'http://news.google.com/?output=atom',
	'http://rss.cnn.com/rss/cnn_topstories.rss'
);
 
// This array will hold the items we'll be grabbing.
$first_items = array();
 
// Let's go through the array, feed by feed, and store the items we want.
foreach ($feeds as $url)
{
    // Use the long syntax
    $feed = new SimplePie();
    $feed->set_feed_url($url);
    $feed->init();
 
	// How many items per feed should we try to grab?
	$items_per_feed = 5;
 
	// As long as we're not trying to grab more items than the feed has, go through them one by one and add them to the array.
	for ($x = 0; $x < $feed->get_item_quantity($items_per_feed); $x++)
	{
		$first_items[] = $feed->get_item($x);
	}
 
    // We're done with this feed, so let's release some memory.
    unset($feed);
}
 
// We need to sort the items by date with a user-defined sorting function.  Since usort() won't accept "SimplePie::sort_items", we need to wrap it in a new function.
function sort_items($a, $b)
{
	return SimplePie::sort_items($a, $b);
}
 
// Now we can sort $first_items with our custom sorting function.
usort($first_items, "sort_items");
 
 
// Begin the (X)HTML page.
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	
<head>
	<title>Multifeeds Test page</title>
	<link rel="stylesheet" href="../demo/for_the_demo/simplepie.css" type="text/css" media="screen" title="SimplePie Styles" charset="utf-8" />
	<style type="text/css">
	div#site {
		width:600px;
	}
	span.footnote {
		white-space:nowrap;
	}
	h1 {
		line-height:1.4em;
	}
	.clearBoth {
		clear:both;
	}
	</style>
</head>
<body>
<div id="site">
 
	<div class="chunk">
		<h1>Quick-n-Dirty Multifeeds Demo</a></h1>
	</div>
 
	<?php
	foreach($first_items as $item):
		$feed = $item->get_feed();
		?>
 
		<div class="chunk">
			<h4><a href="<?php echo $item->get_permalink(); ?>"><?php echo html_entity_decode($item->get_title(), ENT_QUOTES, 'UTF-8'); ?></a></h4>
 
			
			<p class="footnote">Source: <a href="<?php echo $feed->get_permalink(); ?>"><?php echo $feed->get_title(); ?></a> | <?php echo $item->get_date('j M Y | g:i a'); ?></p>
		</div>
 
	<?php endforeach; ?>
 
</div>
</body>
</html>