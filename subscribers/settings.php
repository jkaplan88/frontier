<?php
$baseURL = '/var/www/vhosts/frontierweather.com/www/subscribers/'; // set the file where the blog will be installed - no trailing '/'. DOCUMENT_ROOT is a path to your server's root. (eg. /var/www/html/)

$pageTitle = 'Weather News and Updates'; //set the page title here

$perPage = '10'; //This is where you set the number of posts per page.

$blogPageName = 'index.php'; //this is the file that the blog is displayed in

$USERS = array('frontier'=>'vgy78uhb','stephenstrum'=>'vgy78uhb','jimsouthard'=>'vgy78uhb'); //array of valid users. Format for arrays is ('user'=>'password',...)

$MULTIUSERS = '1'; //this will show the username of who wrote each post (true or false)

$imagesPerEntry = '10'; //this is the number of files that you can upload with each entry

$COMMENTS = ''; //display/allow comments (true or false)

$TABLENAME = 'entries'; //name of the database table that holds this blog's data

$TIMEOFFSET = '0'; //the difference between your time, and your server's time

//RSS Settings

$websiteRoot = 'http://www.frontierweather.com/subscribers/'; //root of your blog (eg. http://www.yoursite.com/blog)

$rssFileName = 'feed.rss'; //this is the name of the RSS file. You must create a blank file and put it into the same directory. Also, set it's permissions to 777.

$rssLink = 'http://www.frontierweather.com/subscribers/index.php';//set the link to your site

$rssDesc = 'FW weather updates';//set a description for what this feed is all about

//CMS Settings
$users_login = array('frontier'=>'vgy78uhb','stephenstrum'=>'vgy78uhb','jimsouthard'=>'vgy78uhb'); //array of valid users. Format for arrays is ('user'=>'password',...)

$show_global_nav = '1'; //true or false will either show or hide the global nav in the CMS pages

$global_nav_items = array('add_blog.php' => 'Add Blog Entry','chooseTable.php' => 'Choose Table'); //assoc array using file names as keys and titles as values


?>