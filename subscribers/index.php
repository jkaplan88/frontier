<?php
 
//****INCLUDE GLOBALS*******************************************************************

include("sqlStrings.php");
include("settings.php");

//****HANDLE FORM***********************************************************************

if (isset($_POST['submit']) || isset($_POST['update'])){ // Handle the form.

//****SET OPERATION*********************************************************************

if (isset($_POST['submit'])) { $_db_operation = 'insert';}
else if (isset($_POST['update'])){ $_db_operation = 'update';}

//****SORT OUT THE USERNAME AND PASSWORD*************************************************


//init the errors
$err = "the following errors occurred:\n";

	
if(!empty($_POST['username'])){
$_un = $_POST['username']; 
}else{
$err .= "Username was not set.\n";
}

if(!empty($_POST['password'])){
$_pw = $_POST['password'];
}else{
$err .= "Password was not set.\n";
}

//check for match
if(array_key_exists($_un, $users_login)){
	if($USERS[$_un] == $_pw){
		$userPassed = true;
	}else{
		$userPassed = false;
		$error= 'The password was not correct. Please try again.';
	}
}else{
		$userPassed = false;
		$error= 'The username and/or password was not correct. Please try again.';

}


//****CALCULATE THE OFFSET TIME************************************************************

	$OFFSETTIME = date("Y-m-d H:i:s", time() + ($TIMEOFFSET * 60 * 60));
	
//***************************************************************************************

if($userPassed){//check the password and username

// Function for escaping and trimming form data.
	function escape_data ($data) { 
		global $dbc;
		if (ini_get('magic_quotes_gpc')) {
			$data = stripslashes($data);
		}
		return mysql_real_escape_string (trim ($data), $dbc);
	} // End of escape_data() function.
	
	//check to see that the fields are filled in
	if(!empty($_POST['comment'])){
	$co = escape_data($_POST['comment']);
	} else {
	echo 'you forgot to set the comment.';
	
	die;}
	
	if(!empty($_POST['title'])){
	$ti = escape_data($_POST['title']);
	} else {
	echo 'you forgot to set the title.';
	
	die;}
	
	//fields for update only
	if(!empty($_POST['uid'])){
	$uid = escape_data($_POST['uid']);
	}
	if(!empty($_POST['col'])){
	$editCol = escape_data($_POST['col']);
	}
		

	
	//check to see if the entry time will be updated to current time, or remain the original time
	if($_POST['time']){
	$time = $OFFSETTIME;
	}else{
	$time = $_POST['origtime'];
	}
		
	$t = escape_data($_POST['title']);
	$c = escape_data($_POST['comment']);
	$un = escape_data($_POST['username']);
	
	
//****GET FILENAMES************************************************************************

	//concat the filenames into an array
	$filename = "";
	
	//prepend the files with the month and date to organize, and to ensure that 2 files dont have the same name
	$prependfile = date("my");
	
	for($i=0; $i<$imagesPerEntry; $i++){
		$curImageFile = "upload$i";
		$oldImageFile = "image$i";
		
		//make sure that it isnt an empty field, because that will destroy the array structure
		if(!empty($_FILES[$curImageFile]['name'])){
			if($i == 0){//no comma before the first entry
				$filename .= $prependfile.$_FILES[$curImageFile]['name'];
			}else{
				$filename .= ', '.$prependfile.$_FILES[$curImageFile]['name'];
			}
		}else{ //we should try to keep the old image
			if($i == 0){//no comma before the first entry
				$filename .= $_POST[$oldImageFile];
			}else{
				$filename .= ', '.$_POST[$oldImageFile];
			}
		}
	}
	

//****INSERT INTO DB***********************************************************************	

	if($_db_operation == 'insert'){
	//Add the record to the database.
	$query = "INSERT INTO $TABLENAME (comment, title, image, time, postedBy) VALUES ('$c', '$t', '$filename', '$OFFSETTIME', '$un' )";
	$result = @mysql_query($query);
	echo '<h1>Comment added</h1>';	
	}
	else if($_db_operation == 'update'){
	//Update the record to the database.
	$query = "UPDATE $TABLENAME SET comment = '$c' , title = '$t', image = '$filename', time = '$time', postedBy = '$un' WHERE $editCol = '$uid'";
	$result = @mysql_query ($query);
	echo '<h1>Comment added</h1>';
	}



//****UPLOAD IMAGES************************************************************************	
	
	//split the filenames into an array
	$filesArray = split(", ", $filename);

	if ($result) {
		//Move file(s) over
	
		//loop through the filesArray and upload files
		for($i=0; $i<$imagesPerEntry; $i++){
			$curImageFile = "upload$i";
			if(!empty($filesArray[$i])){
				if (move_uploaded_file($_FILES[$curImageFile]['tmp_name'], "$baseURL/blogImages/$filesArray[$i]")) { //moving to the 'blogImages' directory
					chmod("$baseURL/blogImages/$filesArray[$i]", 0755); //added to ensure that the files are readable
					echo '<p>The file has been uploaded!</p>';
				} else {
					echo '<p><font color="red">The file could not be moved.</font></p>';
				}
			
			}else{/*no file in here*/}
		
		} //end of for loop
		
	} else { // If the query did not run OK.
		echo '<p><font color="red">Your submission could not be processed due to a system error. We apologize for any inconvenience.</font></p>'; 
		echo 'this is the error: '. mysql_error();
	}

//refresh the page to clear out the POSTback	

}//end of username and password check
else{
	echo '<p><font color="red">The Username/Password that you entered does not match.</font></p>';
}

//****GENERATE RSS FEED********************************************************************	

include('feed_maker.php');
$makerss = new GenerateFeed;
$genFile = $makerss->makeFeed($TABLENAME,$websiteRoot, $pageTitle, $rssDesc, $rssLink, $rssFileName);


//*****************************************************************************************	

} // End of the main Submit conditional.
?>	


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $pageTitle ?></title>
	<style type="text/css" media="all">@import "corporatestyle2.css";</style>
	<link rel="alternate" type="application/rss+xml" title="RSS news feed (summaries)" href="<?php echo $websiteRoot.'/'.$rssFileName?>" />
<script language="JavaScript" src="http://www.frontierweather.com/servertime.php"></script>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript" SRC="../../textClock.js"></SCRIPT>
<script type="text/javascript" src="monthlyUSHDDCDDforecasttotals_wbr.js"></script>
<script type="text/javascript" src="http://www.frontierweather.com/highslide/highslide.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.frontierweather.com/highslide/highslide.css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="http://www.frontierweather.com/highslide/highslide-ie6.css" />
<![endif]-->

<script type="text/javascript"> 
//<![CDATA[
hs.registerOverlay({
	html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
	position: 'top right',
	fade: 2 // fading the semi-transparent overlay looks bad in IE
});

var galleryOptions = {
	slideshowGroup: 'gallery',
	wrapperClassName: 'dark',
	//outlineType: 'glossy-dark',
	dimmingOpacity: 0.8,
	align: 'center',
	transitions: ['expand', 'crossfade'],
	fadeInOut: true,
	wrapperClassName: 'borderless floating-caption',
	marginLeft: 100,
	marginBottom: 80,
	numberPosition: 'caption'
};
 
if (hs.addSlideshow) hs.addSlideshow({
    slideshowGroup: 'gallery',
    interval: 5000,
    repeat: false,
    useControls: true,
    overlayOptions: {
    	className: 'text-controls',
		position: 'bottom center',
		relativeTo: 'viewport',
		offsetY: -60
	},
	thumbstrip: {
		position: 'bottom center',
		mode: 'horizontal',
		relativeTo: 'viewport'
	}
 
});
hs.Expander.prototype.onInit = function() {
	hs.marginBottom = (this.slideshowGroup == 'gallery') ? 150 : 15;
}
 
// focus the name field
hs.Expander.prototype.onAfterExpand = function() {
 
	if (this.a.id == 'contactAnchor') {
		var iframe = window.frames[this.iframe.name],
			doc = iframe.document;
    	if (doc.getElementById("theForm")) {
        	doc.getElementById("theForm").elements["name"].focus();
    	}
 
	}
}
 
 
hs.graphicsDir = '../../highslide/graphics/';
hs.wrapperClassName = 'borderless';
//]]>
</script>
<!-- Lucky orange start -->
<script type='text/javascript'>
window.__lo_site_id = 86772;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
<!-- Lucky orange end -->
</head>
<!--
super simple PHP blog

written by Todd Resudek for TR1design.net.
todd@tr1design.net

source available at http://www.supersimple.org
copyright 2006

-->

<BODY BGCOLOR="#336699" TEXT="#000000" LINK="#339966" VLINK="#663399" ALINK="#666666" leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0 marginheight=0 marginwidth=0>
<center>
<table cellspacing="25" cellpadding="0" border=0 bgcolor="#336699"><tr><td>
  <TABLE cellpadding=0 cellspacing=0 border=0 width="100%" HEIGHT="100%"><tr><td ALIGN="LEFT" VALIGN="TOP" bgcolor="#ffffff">
 	  <script language="JavaScript" src="http://www.frontierweather.com/newheader2.js"></script>
	  <script language="JavaScript" src="http://www.frontierweather.com/menunew.js"></script>
	  <TABLE cellpadding="10" cellspacing=0 border=0 width="100%"  bgcolor="#ffffff"><tr><td class="Just"  VALIGN="TOP">
	 	<!-- CONTENT-->


<Center>
<script language="JavaScript" src="../../menu_wxapnew1.js"></script>
<p>
<p>
<table border="0" cellspacing="1" cellpadding="1" width="980"><tr height="5"><td colspan="4"></td></tr><tr>
<td valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td valign="top" width="500" align="left">
<h1 class="title">&nbsp;Latest Weather Reports</h1><p>
<center>
<a href="http://www.frontierweather.com/weathersummary/weathersummary.pdf"><img src="http://www.frontierweather.com/picts/mwsbutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/midday/middayupdate.pdf"><img src="http://www.frontierweather.com/picts/mdwsbutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/icfs/individualcityforecasts.pdf"><img src="http://www.frontierweather.com/picts/indcitybutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/degreedays/degreedaysummary.pdf"><img src="http://www.frontierweather.com/picts/ddbutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/tropical/tropicalupdate.pdf"><img src="http://www.frontierweather.com/picts/tropicalbutton.png" border="0" width="90"></a>

<br>
<a href="http://www.frontierweather.com/seasonal/seasonalforecast.pdf"><img src="http://www.frontierweather.com/picts/seasonalbutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/subscribers/ngddstorage.html"><img src="http://www.frontierweather.com/picts/ngddbutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/WeeklyMonthly/weeklyweatherviewer.html"><img src="http://www.frontierweather.com/picts/weeklybutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/WeeklyMonthly/monthlyweatherviewer.html"><img src="http://www.frontierweather.com/picts/monthlybutton.png" border="0" width="90"></a>
<a href="http://www.frontierweather.com/subscribers/mapwalls.html"><img src="http://www.frontierweather.com/picts/mapwallsbutton.png" border="0" width="90"></a>
<br>

</center>
<table border="0" cellpadding="0" cellspacing="0" width="470"><tr><td align="left">

<div id="container">

<?php

//****GET PAGE NUMBER********************************************************************	

if(isset($_GET['pg'])){$page = $_GET['pg'];}else{$page=1;} //finds the page number from the URL
$ePage = $page * $perPage; //tells the last posting on the page. (eg. 7 per page, page 2 the last posting is number 14)
$bPage = $ePage - $perPage; //counts back to the first listing on each page

//store the prev and next page numbers for use in the pagination nav later
$pPage = $page -1; // previous page
$nPage = $page +1; // next page

//****************************************************************************************	


//****GET COUNT OF ENTRIES****************************************************************	


// Query the database. 
$query = "SELECT uid FROM $TABLENAME";
$result = mysql_query ($query);
$totally = 0;
while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
$totally += 1; //loop runs to count the total number of posts. This sets the page forward and back links. It knows the last page and first page.
}

//****************************************************************************************	

//****GET ENTRIES FROM DATABASE***********************************************************	

//function for securing GET data
function cleanEntry($_entry_str){
	$_bad_chars = array(' ', 'select', 'insert', 'delete', 'update', ';', ',');
	$_remove_chars = str_replace($_bad_chars, '', strtolower($_entry_str));
	$_remove_html = strip_tags($_remove_chars);
	if(is_numeric($_remove_html)){
	return $_remove_html; 
	}else{
	die('You must refer to entries using integers only');
	}
}

//if the GET var 'entry' is set, this is a permalink and we will only ask for 1 entry 
if(isset($_GET['entry'])){
//set a var to check if we are in teh entry page view later
$_entryview = true;
//clean the entry number to remove possible security issues
$_perma_entry = cleanEntry($_GET['entry']);

$query = "SELECT comment, image, title, DATE_FORMAT(time, '%M %d, %Y %h:%i%p') AS time, uid, comments, postedBy, time AS ts FROM $TABLENAME WHERE uid = '$_perma_entry' LIMIT 1";

}else{

$query = "SELECT comment, image, title, DATE_FORMAT(time, '%M %d, %Y %h:%i%p') AS time, uid, comments, postedBy, time AS ts FROM $TABLENAME ORDER BY ts DESC LIMIT $bPage,$perPage ";

}


$result = mysql_query ($query);
while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
$dateTime = $row[3]; //makes the AM PM lowercase. CSS is set to Capitalize the date later

echo "<h1 class=\"title\">{$row[2]}</h1>\n";
echo "<font size=\"2\"><a id=\"{$row[4]}\"></a><p class=\"time\">$dateTime</p>"; //anchor tag set for easier linking

$ima = split(", ", $row[1]); //build array out of images list
for($i=0; $i<count($ima); $i++){
	if($imagesPerEntry == 1){
		if(!empty($ima[$i])) { 
			if(substr($ima[$i],-4) != '.pdf'){ //filter out PDFs
			echo "<img src=\"blogImages/$ima[$i]\" class=\"im\" alt=\"$ima[$i]\" />\n";
			}else{ //handle PDFs
			echo "<a href=\"blogImages/$ima[$i]\" target=\"_blank\"><img src=\"im/pdfIcon.gif\" border=\"0\" class=\"im\" alt=\"PDF\" /> $ima[$i]</a>";
			}	
		}
	}
}
//****************************************************************************************	


//****DISPLAY COMMENT*********************************************************************	


$com = nl2br($row[0]);
for($i=0; $i<count($ima); $i++){
$j = $i+1;
if($imagesPerEntry != 1){
	if(substr($ima[$i],-4) != '.pdf'){ //filter out PDFs
	$com = str_replace("[$j]", "<img src=\"blogImages/$ima[$i]\" class=\"im\" alt=\"$ima[$i]\" />\n", $com);
	}else{ //handle PDFs
	$com = str_replace("[$j]", "<a href=\"blogImages/$ima[$i]\" target=\"_blank\"><img src=\"im/pdfIcon.gif\" border=\"0\" class=\"im\" alt=\"PDF\" /> $ima[$i]</a>\n", $com);
	}

}
}
echo "<p align=\"justify\">$com</p>";

//****************************************************************************************	



//****DISPLAY POSTED BY******************************************************************

//if multiUsers is turned on in settings.php, display the username that wrote the entry
if($MULTIUSERS){
echo "<p class=\"postedBy\">Posted By: $row[6]</p>";
}

//***************************************************************************************



//****DISPLAY COMMENTS*******************************************************************

//you must have comments turned on (in settings.php) for this to display
if($COMMENTS){
	if($row[5] != 0){
		echo "<p class=\"userComment\"><a href=\"javascript:void(function(){var super = 'simple';});\" onclick=\"window.open('comments.php?entry=$row[4]', 'Comments', 'width=490,height=350,location=no,toolbars=no,status=no scrollbars=yes');\">$row[5] comments</a> | <a href=\"javascript:void(function(){var super = 'simple';});\" onclick=\"window.open('comments.php?entry=$row[4]', 'Comments', 'width=490,height=350,location=no,toolbars=no,status=no, scrollbars=yes');\">Comment</a><a class=\"permalink\" href=\"$blogPageName?entry=$row[4]\">permalink</a></p>";
	}else{
		echo "<p class=\"userComment\">$row[5] comments | <a href=\"javascript:void(function(){var super = 'simple';});\" onclick=\"window.open('comments.php?entry=$row[4]', 'Comments', 'width=490,height=350,location=no,toolbars=no,status=no, scrollbars=yes');\">Comment</a><a class=\"permalink\" href=\"$blogPageName?entry=$row[4]\">permalink</a></p>";
	}
}

//***************************************************************************************



echo "<p class=\"hr\"></p>"; //this is the break line. set it's appearance in the CSS file.
}

echo "<div id=\"footer\">";

if(!$_entryview){ //if not the entry view, show pagination
	$pages = ceil($totally/$perPage); //divides the total posts by the posts per page then rounds up. This sets the total pages.
	
	//if on page 1, dont give a page back link
	if($page != 1){
		echo '<p><a href="'.$blogPageName.'?pg='.$pPage.'">&laquo;</a>&nbsp;&nbsp;&nbsp;';
	}

	echo $pages." total pages";
	
	//if on the last page, dont link ahead
	if($page != $pages && $totally != 0){
		echo '&nbsp;&nbsp;&nbsp;<a href="'.$blogPageName.'?pg='.$nPage.'">&raquo;</a></p><p>&nbsp;</p>';
	}
}else{ //this is an entry page
	echo "<p><a href=\"$blogPageName\">View all entries&raquo;</a></p>";
}

echo "<br /><a href=\"http://www.supersimple.org\"><img src=\"im/ss.gif\" alt=\"super simple blog script\" border=\"0\" /></a>";
echo " <a href=\"$rssFileName\"><img src=\"im/rss.gif\" alt=\"super simple RSS script\" border=\"0\" /></a>";
echo "</div>";
?>

<?php mysql_close(); // Close the database connection. ?>
</div>


</td></tr></table>
</td><td width="20"></td><td valign="top">

<table border="0" cellspacing="1" cellpadding="1" bgcolor="8888bb">
<tr><td bgcolor="eeeeee" colspan="4" width="240">
<center><font color="003366"><b>Latest Frontier Weather Forecast</b></font></td><td bgcolor="eeeeee" colspan="5" width="240">
<center><font color="003366"><b>Latest Model Guidance</b></font></td></tr>
<tr bgcolor="eeeeee"><td><center>
<a href="javascript:showforecast()">Current<br>Forecast</a></td>
<td><center>
<a href="javascript:previousforecast()">Previous<br>Forecast</a></td>
<td><center><a href="javascript:showchange()">Forecast<br>Change</a></td>
<td><center><a href="http://www.frontierweather.com/forecasts/weathermaps_forecasts.html">More</a></td>
<td><center><a href="javascript:graphsUS()">US<br>Anoms</a></td>
<td><center><a href="javascript:graphsEast()">EIA<br>East</a></td>
<td><center><a href="javascript:graphsWest()">EIA<br>West</a></td>
<td><center><a href="javascript:graphsProd()">EIA<br>Prod.</a></td>
<td><center><a href="http://www.frontierweather.com/forecasts/modelmaps/modeldatanew_graphs.html">More</a></td>
	</tr>
<tr bgcolor="ffffff"><Td valign="top" colspan="4" width="240"><table bgcolor="ffffff" cellpadding="0" cellspacing="0"><tr>
<td valign="top" width="240"><center>
<a href="javascript:forecastchange()"><img name="day25" src="http://www.frontierweather.com/forecasts/Days2-5Anom.png" border="0" width="240" height="200"></a><br>
<a href="javascript:forecastchange()"><img name="day610" src="http://www.frontierweather.com/forecasts/Days6-10Anom.png" border="0" width="240" height="200"></a><br>
<a href="javascript:forecastchange()"><img name="day1115" src="http://www.frontierweather.com/forecasts/Days11-15Anom.png" border="0" width="240" height="200"></a></td>
</tr>
</table>
</td><td valign="top" colspan="5"><table bgcolor="ffffff" cellpadding="0" cellspacing="0"><tr>
<td valign="top"><center>
<div id="modg1"><a href="http://www.frontierweather.com/forecasts/USCompositeModelGraph_3.png" class="highslide" onclick="return hs.expand(this)"><img name="mod1" src="http://www.frontierweather.com/forecasts/USCompositeModelGraph_4.png" border="0" width="240" ></a></div>
<div id="modg2"><a href="http://www.frontierweather.com/forecasts/USCompositeModelGraph_7.png" class="highslide" onclick="return hs.expand(this)"><img name="mod2" src="http://www.frontierweather.com/forecasts/USCompositeModelGraph_7.png" width="240" border="0" ></a></div>
<div id="modg3"><a href="http://www.frontierweather.com/forecasts/USCompositeModelGraph_8.png" class="highslide" onclick="return hs.expand(this)"><img name="mod3"  src="http://www.frontierweather.com/forecasts/USCompositeModelGraph_8.png" width="240" border="0" ></a></div>
</td></tr></table>
</td></tr>
<Tr><td colspan="9" bgcolor="ffffff"><center>
<a href="tempkey_period.png" class="highslide" onclick="return hs.expand(this)"><img src="tempkey_period.png" width="480" border="0"></a>
</td></tr>


<tr><td colspan="9" bgcolor="eeeeee"><centeR><b>Latest Long Range Forecast</b></td></tr>
<tr><td colspan="4" bgcolor="eeeeee"><center><a href="javascript:showseasonalf()">Show forecast for next 2 months</a></td><td colspan="5" bgcolor="eeeeee"><center><a href="javascript:showseasonala()">Show last 2 months actuals</a></td>
</tr><tr><td colspan="4" bgcolor="ffffff"><center><div id="month1name">Month 1 Obs + Forecast</div><a href="http://www.frontierweather.com/seasonal/seasonalforecastpage.html"><img name="month1" src="Month_1_Forecast.png" border="0" width="240"></a></td><td colspan="5" bgcolor="ffffff"><center><div id="month2name">Month 2 Forecast</div><a href="http://www.frontierweather.com/seasonal/seasonalforecastpage.html"><img name="month2" src="Month_2_Forecast.png" border="0" width="240"></a></td></tr>
<tr><td colspan="9" bgcolor="ffffff"><center><img src="CBar_seasonal.png" width="480" border="0"></td></tr>
<tr>
<td colspan="4" bgcolor="ffffff">
<center>US Gas Weighted HDD Forecasts<br>
<a href="USHDDsLast3Months.png" class="highslide" onclick="return hs.expand(this)"><img src="USHDDsLast3Months.png" border="0" width="240"></a>
</td><td colspan="5" bgcolor="ffffff">
<center>US Pop Weighted CDD Forecasts<br>
<a href="USCDDsLast3Months.png" class="highslide" onclick="return hs.expand(this)"><img src="USCDDsLast3Months.png" border="0" width="240"></a>
</td>
</tr>
<tr><td colspan="9" bgcolor="efefef"><font size="2"><center><a href="ddgraphs2.html">Click Here for More Graphs</a></td></tr>
<tr><td bgcolor="ffffff" colspan="9"><font size="2"><center>Monthly/Seasonal Degree Day Forecasts</font><div id="seasddtable"></div></td></tr>
<tr><td colspan="9" bgcolor="ffffff"><center>
<a href="http://www.frontierweather.com/tropical/index.html"><img src="http://www.frontierweather.com/tropical2/hw3.php?config=tropimg&forecast=plotsystemforecast&region=nt,pz,pa&eventnum=active,invest&lo=1&ib=0&year=2017&usemap=fulltropics_merc_640x280&nl=1&pn=1" border="0" width="480"></a>
<Br>
<a href="http://www.ssd.noaa.gov/goes/east/tatl/avn-animated.gif" class="highslide" onclick="return hs.expand(this)"><img name="trop2" src="http://www.ssd.noaa.gov/goes/east/tatl/avn-animated.gif" border="0" width="480" ></a>
</td></tr>
</table>
<p>
<center>
<font size="3">Current Temperatures</font><br>
<a href="http://www.frontierweather.com/forecasts/weathermaps2.html"><IMG name="ctemps" src="https://maps.aerisapi.com/tfze44x5zQN6UFmMuA7ZA_yY2Xs7cT9Kkx7pZGz9MOvL8Bc3IHpuj7w8kjSX1j/flat,temperatures,clip-us-flat,states-outlines,temperatures-text/640x480/38.3417,-97.3828,4/current.png" width="480" height="360" border="0"></a>
<br>
<font size="3">Current Warnings</font><br>
<img src="https://maps.aerisapi.com/tfze44x5zQN6UFmMuA7ZA_yY2Xs7cT9Kkx7pZGz9MOvL8Bc3IHpuj7w8kjSX1j/flat,alerts,admin-cities-dk/640x480/38.4794,-97.3828,4/current.png" border="0" width="480" height="360">
<br>
<font size="3">Current Radar</font><br>
<IMG name="HWradImage" SRC="https://maps.aerisapi.com/tfze44x5zQN6UFmMuA7ZA_yY2Xs7cT9Kkx7pZGz9MOvL8Bc3IHpuj7w8kjSX1j/flat,clip-us-flat,states-outlines,radar,admin-cities/640x480/38.3417,-97.3828,4/current.png" width="480" height="360" border="0"><br>
<br>
<a href="http://www.ssd.noaa.gov/goes/comp/ceus/avn-animated.gif" class="highslide" onclick="return hs.expand(this)"><img name="trop2" src="http://www.ssd.noaa.gov/goes/comp/ceus/avn-animated.gif" border="0" width="480" ></a>
<br><center>
<font size="2">Latest Drought/Precipitation Information</font><br>
<table border="0" cellpadding="0" cellspacing="1" bgcolor="8888bb"><tr><td bgcolor="ffffff" valign="top"><center>
<div id="precipmenu">
<table border="0" cellpadding="0" cellspacing="1" width="480">
<tr><td bgcolor="dedede"><center><a href="javascript:pmap1()">Palmer</a></td><td bgcolor="dedede"><center><a href="javascript:pmap2()">Crop Moisture</a></td><td bgcolor="dedede"><center><a href="javascript:pmap3()">Precip Needed</a></td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:pmap4()">7 Day Totals</a></td><td bgcolor="dedede"><center><a href="javascript:pmap5()">30 Day Totals</a></td><td bgcolor="dedede"><center><a href="javascript:pmap6()">90 Day Totals</a></td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:pmap7()">7 Day Anomalies</a></td><td bgcolor="dedede"><center><a href="javascript:pmap8()">30 Day Anomalies</a></td><td bgcolor="dedede"><center><a href="javascript:pmap9()">90 Day Anomalies</a></td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:pmap10()">GFS 7 Day Fcst Total</a></td><td bgcolor="dedede"><center><a href="javascript:pmap13()">GFS 15 Day Fcst Total</a></td><td bgcolor="dedede"><center><a href="javascript:pmap11()">NAM 84 HR Fcst Total</a></td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:pmap14()">ECMWF 10 Day Fcst Total</a></td><td bgcolor="dedede"><center><a href="javascript:pmap15()">EC ENS 15 Day Fcst Total</a></td><td bgcolor="dedede"><center><a href="javascript:pmap12()">CMC 10 Day Fcst Totals</a></td></tr>
<tr><td colspan="3" bgcolor="eeeeee"><center><a href="http://www.frontierweather.com/forecasts/agweather.html">More Drought/Ag Maps</a></td></tr>
</table>
</div><br>
<img name="precipmaps" src="http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/regional_monitoring/palmer.gif" width="480">
</td></tr></table>

<br><center>
<font size="2">Latest Freeze/Snow Information</font><br>
<table border="0" cellpadding="0" cellspacing="1" bgcolor="8888bb"><tr><td bgcolor="ffffff" valign="top"><center>
<div id="precipmenu">
<table border="0" cellpadding="0" cellspacing="1" width="480">
<tr><td bgcolor="dedede"><center><a href="javascript:smap2()">North America Snow Cover</a></td><td bgcolor="dedede"><center><a href="javascript:smap4()">Day 2 Freeze Outlook</a></td><td bgcolor="dedede"><center><a href="javascript:smap5()">Day 3 Freeze Outlook</a>/td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:smap7()">GFS 7 Day Snow Fcst</a></td><td bgcolor="dedede"><center><a href="javascript:smap10()">GFS 15 Day Snow Fcst</a></td><td bgcolor="dedede"><center><a href="javascript:smap8()">NAM 84 HR Snow Fcst</a></td></tr>
<tr><td bgcolor="dedede"><center><a href="javascript:smap11()">ECMWF 10 Day Snow Fcst</a></td><td bgcolor="dedede"><center><a href="javascript:smap12()">EC Ens 15 Snow Fcst</a></td><td bgcolor="dedede"><center><a href="javascript:smap9()">CMC 10 Day Snow Fcst</a></td></tr>

</table>
</div><br>
<img name="snowmaps" src="http://www.frontierweather.com/forecasts/modelmaps/new/2mt/day2_latest_frostfreeze.png" width="480">
</td></tr></table>
<br>
<img src="http://www.frontierweather.com/free/wind/combined_genpot_us_loop.gif" border="0" width="480">
<br><a href="http://www.frontierweather.com/errorplots/dailyabserror.png" class="highslide" onclick="return hs.expand(this)"><img src="http://www.frontierweather.com/errorplots/dailyabserror.png" border="0" width="480"></a>
</td></tr><tr><td>
</td></tr></table>
<center><br>
</td></tr></table>
		<!-- CONTENT-->
	  </TD></TR></table>	 
  </td></tr><TR><TD VALIGN="top">
	 <img src="http://www.frontierweather.com/picts/bottomcurve.png" width="100%">
 </td></tr></table><br>
<script language="JavaScript" src="http://www.frontierweather.com/copyright2.js"></script>
 </td></tr></table>
</BODY>
<script type="text/javascript"> 

var mapsare=0;
var hournow=0;




function smap1(){
	document.snowmaps.src='http://www.frontierweather.com/climatemaps/snowdepth/snowcover.png';
}
function smap2(){
	document.snowmaps.src='http://www.natice.noaa.gov/pub/ims/ims_gif/DATA/cursnow_usa.gif';
}
function smap3(){
	document.snowmaps.src='http://www.frontierweather.com/climatemaps/snowdepth/iceandsnowcover_NH.png';
}
function smap4(){
	document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/2mt/day2_latest_frostfreeze.png';
}
function smap5(){
	document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/2mt/day3_latest_frostfreeze.png';
}
function smap6(){
	document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/2mt/day4_latest_frostfreeze.png';
}
function smap7(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_18z_snow.png';
	}
	else if(hournow<12){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_00z_snow.png';
	}
	else if(hournow<18){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_06z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_12z_snow.png';
	}
}
function smap8(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<5){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_18z_snow.png';
	}
	else if(hournow<11){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_00z_snow.png';
	}
	else if(hournow<17){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_06z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_12z_snow.png';
	}
}
function smap9(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_12z_snow.png';
	}
	else if(hournow<12){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_00z_snow.png';
	}
	else if(hournow<18){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_00z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_12z_snow.png';
	}
}
function smap10(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_18z_snow.png';
	}
	else if(hournow<12){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_00z_snow.png';
	}
	else if(hournow<18){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_06z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_12z_snow.png';
	}
}

function smap11(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<8){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_12z_snow.png';
	}
	else if(hournow<20){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_00z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_12z_snow.png';
	}
}
function smap12(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<10){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_12z_snow.png';
	}
	else if(hournow<22){
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_00z_snow.png';
	}
	else {
		document.snowmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_12z_snow.png';
	}
}

function pmap1(){
	document.precipmaps.src='http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/regional_monitoring/palmer.gif';
}
function pmap2(){
	document.precipmaps.src='http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/regional_monitoring/cmi.gif';
}
function pmap3(){
	document.precipmaps.src='http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/regional_monitoring/addpcp.gif';
}
function pmap4(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_7day_accum_us.png';
}
function pmap5(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_30day_accum_us.png';
}
function pmap6(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_90day_accum_us.png';
}
function pmap7(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_7day_anom_us.png';
}
function pmap8(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_30day_anom_us.png';
}
function pmap9(){
	document.precipmaps.src='http://www.frontierweather.com/climatemaps/Precip/latest_90day_anom_us.png';
}
function pmap10(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_18z_precip.png';
	}
	else if(hournow<12){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_00z_precip.png';
	}
	else if(hournow<18){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_06z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day7total_12z_precip.png';
	}
}
function pmap11(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<5){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_18z_precip.png';
	}
	else if(hournow<11){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_00z_precip.png';
	}
	else if(hournow<17){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_06z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/NAM_84hr_12z_precip.png';
	}
}
function pmap12(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_12z_precip.png';
	}
	else if(hournow<12){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_00z_precip.png';
	}
	else if(hournow<18){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_00z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/CMC_day10total_12z_precip.png';
	}
}
function pmap13(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<6){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_18z_precip.png';
	}
	else if(hournow<12){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_00z_precip.png';
	}
	else if(hournow<18){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_06z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/day15total_12z_precip.png';
	}
}
function pmap14(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<8){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_12z_precip.png';
	}
	else if(hournow<20){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_00z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_day10total_12z_precip.png';
	}
}
function pmap15(){
	hournow=servertimeOBJ.getHours();
	
	if(hournow<10){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_12z_precip.png';
	}
	else if(hournow<22){
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_00z_precip.png';
	}
	else {
		document.precipmaps.src='http://www.frontierweather.com/forecasts/modelmaps/new/ECMWF_ensmean_day15total_12z_precip.png';
	}
}
function showseasonalf(){
	document.getElementById("month1name").innerHTML='Month 1 Obs + Forecast';
	document.getElementById("month2name").innerHTML='Month 2 Forecast';
	document.month1.src='Month_1_Forecast.png';
	document.month2.src='Month_2_Forecast.png';
}
function showseasonala(){
	document.getElementById("month1name").innerHTML='Month -2 Observations';
	document.getElementById("month2name").innerHTML='Month -1 Observations';
	document.month1.src='Month_-2_Forecast.png';
	document.month2.src='Month_-1_Forecast.png';
}
function showforecast(){
	document.day25.src='http://www.frontierweather.com/forecasts/Days2-5Anom.png';
	document.day610.src='http://www.frontierweather.com/forecasts/Days6-10Anom.png';
	document.day1115.src='http://www.frontierweather.com/forecasts/Days11-15Anom.png';
	mapsare=0;
}
function previousforecast(){
	document.day25.src='http://www.frontierweather.com/forecasts/2-5day_last.png';
	document.day610.src='http://www.frontierweather.com/forecasts/6-10day_last.png';
	document.day1115.src='http://www.frontierweather.com/forecasts/11-15day_last.png';
	mapsare=0;
}
function showchange(){
	document.day25.src='http://www.frontierweather.com/forecasts/Day2-5change.png';
	document.day610.src='http://www.frontierweather.com/forecasts/Day6-10change.png';
	document.day1115.src='http://www.frontierweather.com/forecasts/Day11-15change.png';
	mapsare=1;
}
function forecastchange(){
	if(mapsare==0) {
		showchange();
	}
	else {
		showforecast();
	}	
}
function graphsUS(){
	document.getElementById("modg1").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_3.png" class="highslide" onclick="return hs.expand(this)"><img name="mod1" src="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_4.png" border="0" width="240"></a>';
	document.getElementById("modg2").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_7.png" class="highslide" onclick="return hs.expand(this)"><img name="mod2" src="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_7.png" width="240" border="0"></a>';
	document.getElementById("modg3").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_8.png" class="highslide" onclick="return hs.expand(this)"><img name="mod3"  src="http://www.frontierweather.com/forecasts/modelgraphs/USCompositeModelGraph_8.png" width="240" border="0"></a>';
}
function graphsEast(){
	document.getElementById("modg1").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/EIAEastModelGraph_3.png" class="highslide" onclick="return hs.expand(this)"><img name="mod1" src="http://www.frontierweather.com/forecasts/modelgraphs/EIAEastModelGraph_4.png" border="0" width="240"></a>';
	document.getElementById("modg2").innerHTML='';
	document.getElementById("modg3").innerHTML='';
}
function graphsWest(){
	document.getElementById("modg1").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/EIAWestModelGraph_3.png" class="highslide" onclick="return hs.expand(this)"><img name="mod1" src="http://www.frontierweather.com/forecasts/modelgraphs/EIAWestModelGraph_4.png" border="0" width="240"></a>';
	document.getElementById("modg2").innerHTML='';
	document.getElementById("modg3").innerHTML='';
}
function graphsProd(){
	document.getElementById("modg1").innerHTML='<a href="http://www.frontierweather.com/forecasts/modelgraphs/EIAProducingModelGraph_3.png" class="highslide" onclick="return hs.expand(this)"><img name="mod1" src="http://www.frontierweather.com/forecasts/modelgraphs/EIAProducingModelGraph_4.png" border="0" width="240"></a>';
	document.getElementById("modg2").innerHTML='';
	document.getElementById("modg3").innerHTML='';
}

function displayUSProHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="ffffff"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[0][0]+'</td><td bgcolor="efefef"><center>'+prowdata[0][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[0][2]+'</td><td bgcolor="efefef"><center>'+prowdata[0][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[0][6]+'</td><td bgcolor="efefef"><center>'+prowdata[0][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[1][0]+'</td><td bgcolor="efefef"><center>'+prowdata[1][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[1][2]+'</td><td bgcolor="efefef"><center>'+prowdata[1][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[1][6]+'</td><td bgcolor="efefef"><center>'+prowdata[1][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[2][0]+'</td><td bgcolor="efefef"><center>'+prowdata[2][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[2][2]+'</td><td bgcolor="efefef"><center>'+prowdata[2][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[2][6]+'</td><td bgcolor="efefef"><center>'+prowdata[2][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[3][0]+'</td><td bgcolor="efefef"><center>'+prowdata[3][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[3][2]+'</td><td bgcolor="efefef"><center>'+prowdata[3][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[3][6]+'</td><td bgcolor="efefef"><center>'+prowdata[3][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[4][0]+'</td><td bgcolor="efefef"><center>'+prowdata[4][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[4][2]+'</td><td bgcolor="efefef"><center>'+prowdata[4][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[4][6]+'</td><td bgcolor="efefef"><center>'+prowdata[4][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[5][0]+'</td><td bgcolor="efefef"><center>'+prowdata[5][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[5][2]+'</td><td bgcolor="efefef"><center>'+prowdata[5][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[5][6]+'</td><td bgcolor="efefef"><center>'+prowdata[5][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[6][0]+'</td><td bgcolor="efefef"><center>'+prowdata[6][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[6][2]+'</td><td bgcolor="efefef"><center>'+prowdata[6][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[6][6]+'</td><td bgcolor="efefef"><center>'+prowdata[6][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[7][0]+'</td><td bgcolor="efefef"><center>'+prowdata[7][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[7][2]+'</td><td bgcolor="efefef"><center>'+prowdata[7][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[7][6]+'</td><td bgcolor="efefef"><center>'+prowdata[7][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[8][0]+'</td><td bgcolor="efefef"><center>'+prowdata[8][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[8][2]+'</td><td bgcolor="efefef"><center>'+prowdata[8][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[8][6]+'</td><td bgcolor="efefef"><center>'+prowdata[8][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[9][0]+'</td><td bgcolor="efefef"><center>'+prowdata[9][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[9][2]+'</td><td bgcolor="efefef"><center>'+prowdata[9][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[9][6]+'</td><td bgcolor="efefef"><center>'+prowdata[9][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[10][0]+'</td><td bgcolor="efefef"><center>'+prowdata[10][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[10][2]+'</td><td bgcolor="efefef"><center>'+prowdata[10][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[10][6]+'</td><td bgcolor="efefef"><center>'+prowdata[10][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+prowdata[11][0]+'</td><td bgcolor="efefef"><center>'+prowdata[11][1]+'</td><td bgcolor="ffffff"><center>'+prowdata[11][2]+'</td><td bgcolor="efefef"><center>'+prowdata[11][4]+'</td><td bgcolor="ffffff"><center>'+prowdata[11][6]+'</td><td bgcolor="efefef"><center>'+prowdata[11][7]+'</td><td bgcolor="ffffff"><center>'+prowdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+gwtitle+'</td></tr></table>';
}

function displayUSElecHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="ffffff"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[0][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[0][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[0][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[0][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[0][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[0][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[1][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[1][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[1][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[1][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[1][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[1][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[2][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[2][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[2][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[2][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[2][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[2][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[3][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[3][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[3][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[3][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[3][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[3][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[4][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[4][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[4][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[4][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[4][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[4][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[5][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[5][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[5][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[5][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[5][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[5][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[6][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[6][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[6][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[6][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[6][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[6][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[7][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[7][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[7][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[7][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[7][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[7][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[8][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[8][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[8][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[8][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[8][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[8][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[9][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[9][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[9][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[9][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[9][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[9][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[10][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[10][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[10][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[10][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[10][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[10][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+ehwdata[11][0]+'</td><td bgcolor="efefef"><center>'+ehwdata[11][1]+'</td><td bgcolor="ffffff"><center>'+ehwdata[11][2]+'</td><td bgcolor="efefef"><center>'+ehwdata[11][4]+'</td><td bgcolor="ffffff"><center>'+ehwdata[11][6]+'</td><td bgcolor="efefef"><center>'+ehwdata[11][7]+'</td><td bgcolor="ffffff"><center>'+ehwdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+gwtitle+'</td></tr></table>';
}


function displayUSGWHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[0][0]+'</td><td bgcolor="efefef"><center>'+gwdata[0][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[0][2]+'</td><td bgcolor="efefef"><center>'+gwdata[0][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[0][6]+'</td><td bgcolor="efefef"><center>'+gwdata[0][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[1][0]+'</td><td bgcolor="efefef"><center>'+gwdata[1][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[1][2]+'</td><td bgcolor="efefef"><center>'+gwdata[1][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[1][6]+'</td><td bgcolor="efefef"><center>'+gwdata[1][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[2][0]+'</td><td bgcolor="efefef"><center>'+gwdata[2][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[2][2]+'</td><td bgcolor="efefef"><center>'+gwdata[2][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[2][6]+'</td><td bgcolor="efefef"><center>'+gwdata[2][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[3][0]+'</td><td bgcolor="efefef"><center>'+gwdata[3][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[3][2]+'</td><td bgcolor="efefef"><center>'+gwdata[3][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[3][6]+'</td><td bgcolor="efefef"><center>'+gwdata[3][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[4][0]+'</td><td bgcolor="efefef"><center>'+gwdata[4][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[4][2]+'</td><td bgcolor="efefef"><center>'+gwdata[4][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[4][6]+'</td><td bgcolor="efefef"><center>'+gwdata[4][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[5][0]+'</td><td bgcolor="efefef"><center>'+gwdata[5][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[5][2]+'</td><td bgcolor="efefef"><center>'+gwdata[5][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[5][6]+'</td><td bgcolor="efefef"><center>'+gwdata[5][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[6][0]+'</td><td bgcolor="efefef"><center>'+gwdata[6][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[6][2]+'</td><td bgcolor="efefef"><center>'+gwdata[6][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[6][6]+'</td><td bgcolor="efefef"><center>'+gwdata[6][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[7][0]+'</td><td bgcolor="efefef"><center>'+gwdata[7][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[7][2]+'</td><td bgcolor="efefef"><center>'+gwdata[7][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[7][6]+'</td><td bgcolor="efefef"><center>'+gwdata[7][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[8][0]+'</td><td bgcolor="efefef"><center>'+gwdata[8][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[8][2]+'</td><td bgcolor="efefef"><center>'+gwdata[8][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[8][6]+'</td><td bgcolor="efefef"><center>'+gwdata[8][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[9][0]+'</td><td bgcolor="efefef"><center>'+gwdata[9][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[9][2]+'</td><td bgcolor="efefef"><center>'+gwdata[9][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[9][6]+'</td><td bgcolor="efefef"><center>'+gwdata[9][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[10][0]+'</td><td bgcolor="efefef"><center>'+gwdata[10][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[10][2]+'</td><td bgcolor="efefef"><center>'+gwdata[10][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[10][6]+'</td><td bgcolor="efefef"><center>'+gwdata[10][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gwdata[11][0]+'</td><td bgcolor="efefef"><center>'+gwdata[11][1]+'</td><td bgcolor="ffffff"><center>'+gwdata[11][2]+'</td><td bgcolor="efefef"><center>'+gwdata[11][4]+'</td><td bgcolor="ffffff"><center>'+gwdata[11][6]+'</td><td bgcolor="efefef"><center>'+gwdata[11][7]+'</td><td bgcolor="ffffff"><center>'+gwdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+gwtitle+'</td></tr></table>';
}

function displayUSPWHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="ffffff"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[0][0]+'</td><td bgcolor="efefef"><center>'+pwdata[0][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[0][2]+'</td><td bgcolor="efefef"><center>'+pwdata[0][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[0][6]+'</td><td bgcolor="efefef"><center>'+pwdata[0][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[1][0]+'</td><td bgcolor="efefef"><center>'+pwdata[1][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[1][2]+'</td><td bgcolor="efefef"><center>'+pwdata[1][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[1][6]+'</td><td bgcolor="efefef"><center>'+pwdata[1][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[2][0]+'</td><td bgcolor="efefef"><center>'+pwdata[2][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[2][2]+'</td><td bgcolor="efefef"><center>'+pwdata[2][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[2][6]+'</td><td bgcolor="efefef"><center>'+pwdata[2][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[3][0]+'</td><td bgcolor="efefef"><center>'+pwdata[3][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[3][2]+'</td><td bgcolor="efefef"><center>'+pwdata[3][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[3][6]+'</td><td bgcolor="efefef"><center>'+pwdata[3][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[4][0]+'</td><td bgcolor="efefef"><center>'+pwdata[4][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[4][2]+'</td><td bgcolor="efefef"><center>'+pwdata[4][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[4][6]+'</td><td bgcolor="efefef"><center>'+pwdata[4][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[5][0]+'</td><td bgcolor="efefef"><center>'+pwdata[5][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[5][2]+'</td><td bgcolor="efefef"><center>'+pwdata[5][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[5][6]+'</td><td bgcolor="efefef"><center>'+pwdata[5][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[6][0]+'</td><td bgcolor="efefef"><center>'+pwdata[6][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[6][2]+'</td><td bgcolor="efefef"><center>'+pwdata[6][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[6][6]+'</td><td bgcolor="efefef"><center>'+pwdata[6][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[7][0]+'</td><td bgcolor="efefef"><center>'+pwdata[7][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[7][2]+'</td><td bgcolor="efefef"><center>'+pwdata[7][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[7][6]+'</td><td bgcolor="efefef"><center>'+pwdata[7][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[8][0]+'</td><td bgcolor="efefef"><center>'+pwdata[8][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[8][2]+'</td><td bgcolor="efefef"><center>'+pwdata[8][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[8][6]+'</td><td bgcolor="efefef"><center>'+pwdata[8][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[9][0]+'</td><td bgcolor="efefef"><center>'+pwdata[9][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[9][2]+'</td><td bgcolor="efefef"><center>'+pwdata[9][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[9][6]+'</td><td bgcolor="efefef"><center>'+pwdata[9][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[10][0]+'</td><td bgcolor="efefef"><center>'+pwdata[10][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[10][2]+'</td><td bgcolor="efefef"><center>'+pwdata[10][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[10][6]+'</td><td bgcolor="efefef"><center>'+pwdata[10][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwdata[11][0]+'</td><td bgcolor="efefef"><center>'+pwdata[11][1]+'</td><td bgcolor="ffffff"><center>'+pwdata[11][2]+'</td><td bgcolor="efefef"><center>'+pwdata[11][4]+'</td><td bgcolor="ffffff"><center>'+pwdata[11][6]+'</td><td bgcolor="efefef"><center>'+pwdata[11][7]+'</td><td bgcolor="ffffff"><center>'+pwdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+pwtitle+'</td></tr></table>';
}

function displayUSComboHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="ffffff"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[0][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[0][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[0][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[0][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[0][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[0][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[1][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[1][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[1][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[1][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[1][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[1][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[2][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[2][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[2][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[2][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[2][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[2][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[3][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[3][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[3][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[3][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[3][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[3][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[4][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[4][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[4][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[4][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[4][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[4][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[5][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[5][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[5][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[5][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[5][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[5][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[6][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[6][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[6][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[6][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[6][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[6][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[7][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[7][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[7][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[7][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[7][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[7][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[8][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[8][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[8][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[8][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[8][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[8][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[9][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[9][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[9][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[9][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[9][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[9][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[10][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[10][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[10][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[10][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[10][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[10][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+gehwdata[11][0]+'</td><td bgcolor="efefef"><center>'+gehwdata[11][1]+'</td><td bgcolor="ffffff"><center>'+gehwdata[11][2]+'</td><td bgcolor="efefef"><center>'+gehwdata[11][4]+'</td><td bgcolor="ffffff"><center>'+gehwdata[11][6]+'</td><td bgcolor="efefef"><center>'+gehwdata[11][7]+'</td><td bgcolor="ffffff"><center>'+gehwdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+gehwtitle+'</td></tr></table>';
}

function displayUSOWHDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="ffffff"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+howdata[0][0]+'</td><td bgcolor="efefef"><center>'+howdata[0][1]+'</td><td bgcolor="ffffff"><center>'+howdata[0][2]+'</td><td bgcolor="efefef"><center>'+howdata[0][4]+'</td><td bgcolor="ffffff"><center>'+howdata[0][6]+'</td><td bgcolor="efefef"><center>'+howdata[0][7]+'</td><td bgcolor="ffffff"><center>'+howdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[1][0]+'</td><td bgcolor="efefef"><center>'+howdata[1][1]+'</td><td bgcolor="ffffff"><center>'+howdata[1][2]+'</td><td bgcolor="efefef"><center>'+howdata[1][4]+'</td><td bgcolor="ffffff"><center>'+howdata[1][6]+'</td><td bgcolor="efefef"><center>'+howdata[1][7]+'</td><td bgcolor="ffffff"><center>'+howdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[2][0]+'</td><td bgcolor="efefef"><center>'+howdata[2][1]+'</td><td bgcolor="ffffff"><center>'+howdata[2][2]+'</td><td bgcolor="efefef"><center>'+howdata[2][4]+'</td><td bgcolor="ffffff"><center>'+howdata[2][6]+'</td><td bgcolor="efefef"><center>'+howdata[2][7]+'</td><td bgcolor="ffffff"><center>'+howdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+howdata[3][0]+'</td><td bgcolor="efefef"><center>'+howdata[3][1]+'</td><td bgcolor="ffffff"><center>'+howdata[3][2]+'</td><td bgcolor="efefef"><center>'+howdata[3][4]+'</td><td bgcolor="ffffff"><center>'+howdata[3][6]+'</td><td bgcolor="efefef"><center>'+howdata[3][7]+'</td><td bgcolor="ffffff"><center>'+howdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[4][0]+'</td><td bgcolor="efefef"><center>'+howdata[4][1]+'</td><td bgcolor="ffffff"><center>'+howdata[4][2]+'</td><td bgcolor="efefef"><center>'+howdata[4][4]+'</td><td bgcolor="ffffff"><center>'+howdata[4][6]+'</td><td bgcolor="efefef"><center>'+howdata[4][7]+'</td><td bgcolor="ffffff"><center>'+howdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[5][0]+'</td><td bgcolor="efefef"><center>'+howdata[5][1]+'</td><td bgcolor="ffffff"><center>'+howdata[5][2]+'</td><td bgcolor="efefef"><center>'+howdata[5][4]+'</td><td bgcolor="ffffff"><center>'+howdata[5][6]+'</td><td bgcolor="efefef"><center>'+howdata[5][7]+'</td><td bgcolor="ffffff"><center>'+howdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[6][0]+'</td><td bgcolor="efefef"><center>'+howdata[6][1]+'</td><td bgcolor="ffffff"><center>'+howdata[6][2]+'</td><td bgcolor="efefef"><center>'+howdata[6][4]+'</td><td bgcolor="ffffff"><center>'+howdata[6][6]+'</td><td bgcolor="efefef"><center>'+howdata[6][7]+'</td><td bgcolor="ffffff"><center>'+howdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[7][0]+'</td><td bgcolor="efefef"><center>'+howdata[7][1]+'</td><td bgcolor="ffffff"><center>'+howdata[7][2]+'</td><td bgcolor="efefef"><center>'+howdata[7][4]+'</td><td bgcolor="ffffff"><center>'+howdata[7][6]+'</td><td bgcolor="efefef"><center>'+howdata[7][7]+'</td><td bgcolor="ffffff"><center>'+howdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[8][0]+'</td><td bgcolor="efefef"><center>'+howdata[8][1]+'</td><td bgcolor="ffffff"><center>'+howdata[8][2]+'</td><td bgcolor="efefef"><center>'+howdata[8][4]+'</td><td bgcolor="ffffff"><center>'+howdata[8][6]+'</td><td bgcolor="efefef"><center>'+howdata[8][7]+'</td><td bgcolor="ffffff"><center>'+howdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+howdata[9][0]+'</td><td bgcolor="efefef"><center>'+howdata[9][1]+'</td><td bgcolor="ffffff"><center>'+howdata[9][2]+'</td><td bgcolor="efefef"><center>'+howdata[9][4]+'</td><td bgcolor="ffffff"><center>'+howdata[9][6]+'</td><td bgcolor="efefef"><center>'+howdata[9][7]+'</td><td bgcolor="ffffff"><center>'+howdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[10][0]+'</td><td bgcolor="efefef"><center>'+howdata[10][1]+'</td><td bgcolor="ffffff"><center>'+howdata[10][2]+'</td><td bgcolor="efefef"><center>'+howdata[10][4]+'</td><td bgcolor="ffffff"><center>'+howdata[10][6]+'</td><td bgcolor="efefef"><center>'+howdata[10][7]+'</td><td bgcolor="ffffff"><center>'+howdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+howdata[11][0]+'</td><td bgcolor="efefef"><center>'+howdata[11][1]+'</td><td bgcolor="ffffff"><center>'+howdata[11][2]+'</td><td bgcolor="efefef"><center>'+howdata[11][4]+'</td><td bgcolor="ffffff"><center>'+howdata[11][6]+'</td><td bgcolor="efefef"><center>'+howdata[11][7]+'</td><td bgcolor="ffffff"><center>'+howdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+howtitle+'</td></tr></table>';
}

function displayUSPWCDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="ffffff"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[0][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[0][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[0][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[0][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[0][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[0][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[1][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[1][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[1][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[1][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[1][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[1][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[2][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[2][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[2][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[2][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[2][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[2][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[3][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[3][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[3][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[3][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[3][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[3][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[4][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[4][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[4][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[4][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[4][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[4][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[5][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[5][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[5][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[5][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[5][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[5][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[6][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[6][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[6][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[6][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[6][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[6][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[7][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[7][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[7][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[7][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[7][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[7][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[8][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[8][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[8][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[8][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[8][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[8][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[9][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[9][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[9][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[9][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[9][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[9][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[10][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[10][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[10][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[10][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[10][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[10][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+pwcdata[11][0]+'</td><td bgcolor="efefef"><center>'+pwcdata[11][1]+'</td><td bgcolor="ffffff"><center>'+pwcdata[11][2]+'</td><td bgcolor="efefef"><center>'+pwcdata[11][4]+'</td><td bgcolor="ffffff"><center>'+pwcdata[11][6]+'</td><td bgcolor="efefef"><center>'+pwcdata[11][7]+'</td><td bgcolor="ffffff"><center>'+pwcdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+pwctitle+'</td></tr></table>';
}

function displayUSACWCDDs() {
document.getElementById("seasddtable").innerHTML='<table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="ffffff" colspan="7"><table border="0" cellspacing="1" bgcolor="ffffff" width="480"><tr><td bgcolor="dedede"><center><a href="javascript:displayUSGWHDDs()">US Gas Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSComboHDDs()">US Gas+Electric Heating<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSPWHDDs()">US Population<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSOWHDDs()">US Heating Oil<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSProHDDs()">US Propane<br>Weighted HDDs</a></td><td bgcolor="dedede"><center><a href="javascript:displayUSElecHDDs()">US Electric Heating<br>Weighted HDDs</a></td></tr><tr><td bgcolor="dedede"><center><a href="javascript:displayUSPWCDDs()">US Population<br>Weighted CDDs</a></td><td bgcolor="ffffff"><center><a href="javascript:displayUSACWCDDs()">US %AC X %Gas Pwr Gen<br>Weighted CDDs</a></td><td bgcolor="ffffff"></td></tr></table></td></tr><tr><td colspan="7" bgcolor="ffffff"><center>Current Forecast Updated at '+updatedDate+'</td><tr><td bgcolor="ffffff"><center>Month/Season<br>Obs/Forecast</td><td bgcolor="efefef"><center>Current<br>Forecast</td><td bgcolor="ffffff"><center>Previous<br>Forecast</td><td bgcolor="efefef"><center>Forecast from<br>'+lastDate+'</td><td bgcolor="ffffff"><center>Last<br>Year</td><td bgcolor="efefef"><center>5 Year<br>Average</td><td bgcolor="ffffff"><center>30 Year<br>Average</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[0][0]+'</td><td bgcolor="efefef"><center>'+acwdata[0][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[0][2]+'</td><td bgcolor="efefef"><center>'+acwdata[0][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[0][6]+'</td><td bgcolor="efefef"><center>'+acwdata[0][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[0][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[1][0]+'</td><td bgcolor="efefef"><center>'+acwdata[1][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[1][2]+'</td><td bgcolor="efefef"><center>'+acwdata[1][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[1][6]+'</td><td bgcolor="efefef"><center>'+acwdata[1][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[1][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[2][0]+'</td><td bgcolor="efefef"><center>'+acwdata[2][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[2][2]+'</td><td bgcolor="efefef"><center>'+acwdata[2][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[2][6]+'</td><td bgcolor="efefef"><center>'+acwdata[2][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[2][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[3][0]+'</td><td bgcolor="efefef"><center>'+acwdata[3][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[3][2]+'</td><td bgcolor="efefef"><center>'+acwdata[3][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[3][6]+'</td><td bgcolor="efefef"><center>'+acwdata[3][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[3][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[4][0]+'</td><td bgcolor="efefef"><center>'+acwdata[4][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[4][2]+'</td><td bgcolor="efefef"><center>'+acwdata[4][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[4][6]+'</td><td bgcolor="efefef"><center>'+acwdata[4][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[4][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[5][0]+'</td><td bgcolor="efefef"><center>'+acwdata[5][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[5][2]+'</td><td bgcolor="efefef"><center>'+acwdata[5][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[5][6]+'</td><td bgcolor="efefef"><center>'+acwdata[5][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[5][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[6][0]+'</td><td bgcolor="efefef"><center>'+acwdata[6][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[6][2]+'</td><td bgcolor="efefef"><center>'+acwdata[6][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[6][6]+'</td><td bgcolor="efefef"><center>'+acwdata[6][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[6][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[7][0]+'</td><td bgcolor="efefef"><center>'+acwdata[7][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[7][2]+'</td><td bgcolor="efefef"><center>'+acwdata[7][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[7][6]+'</td><td bgcolor="efefef"><center>'+acwdata[7][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[7][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[8][0]+'</td><td bgcolor="efefef"><center>'+acwdata[8][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[8][2]+'</td><td bgcolor="efefef"><center>'+acwdata[8][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[8][6]+'</td><td bgcolor="efefef"><center>'+acwdata[8][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[8][8]+'</td></tr><tr><td colspan="7" bgcolor="cccccc" ></td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[9][0]+'</td><td bgcolor="efefef"><center>'+acwdata[9][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[9][2]+'</td><td bgcolor="efefef"><center>'+acwdata[9][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[9][6]+'</td><td bgcolor="efefef"><center>'+acwdata[9][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[9][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[10][0]+'</td><td bgcolor="efefef"><center>'+acwdata[10][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[10][2]+'</td><td bgcolor="efefef"><center>'+acwdata[10][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[10][6]+'</td><td bgcolor="efefef"><center>'+acwdata[10][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[10][8]+'</td></tr><tr><td bgcolor="ffffff"><center>'+acwdata[11][0]+'</td><td bgcolor="efefef"><center>'+acwdata[11][1]+'</td><td bgcolor="ffffff"><center>'+acwdata[11][2]+'</td><td bgcolor="efefef"><center>'+acwdata[11][4]+'</td><td bgcolor="ffffff"><center>'+acwdata[11][6]+'</td><td bgcolor="efefef"><center>'+acwdata[11][7]+'</td><td bgcolor="ffffff"><center>'+acwdata[11][8]+'</td></tr><tr><td bgcolor="ffffff" colspan="7"><center>'+acwtitle+'</td></tr></table>';
}

displayUSGWHDDs();
</script>
</html>
