<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Basic Interactive Map</title>
 
 
    <link rel="stylesheet" type="text/css" media="all" href="http://mesh.hamweather.net/wx1T8Z8PCsGHAX/css/wn-map-app-v1.2.css" />
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAGFx3qN8HJnKF-MLMuaMehBSRm9f5b0zeL4qsxNFNlnQc9tyNUBTZBWrb9cegdFPFob-LfTBEE8XFqw"></script>
    <script src="http://mesh.hamweather.net/wx1T8Z8PCsGHAX/js/wn-map-app-v1.2.js?mapsets=default"></script>
 
    <style>
    #content {}
    #wnmap { width: 800px; height:600px;}
    </style>
 
	<script type="text/javascript">
 
	    $(document).ready(function () {
	        var myCfg = {
                            initTool: 'wx',
                            map: {
                                start: {
                                    lat: 40,
                                    lon: -96,
                                    zoom: 4
                                }
                            }
	                    };
 
 
	        $('#wnmap').wnmapapp(myCfg);
	    });
	</script>
 
 
 
</head>
 
	<body >
 
<div id="content">
    <div id="wnmap"></div>
</div>
 
 
</body>
</html>
 
