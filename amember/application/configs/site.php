<?php

if (!defined('INCLUDED_AMEMBER_CONFIG')) 
    die("Direct access to this location is not allowed");
  
/*
*  aMember Pro site customization file
*
*  Rename this file to site.php and put your site customizations, 
*  such as fields additions, custom hooks and so on to this file
*  This file will not be overwritten during upgrade
*                                                                               
*/

Am_Di::getInstance()->hook->add(Am_Event::AUTH_GET_OK_REDIRECT, 'myGetOkRedirect');

function myGetOkRedirect(Am_Event $e) {
    /* @var $user User */
    $user = $e->getUser();
    $e->setReturn("http://www.frontierweather.com/custom/" . $user->login . "/index.html"); }

Am_Di::getInstance()->hook->add('userMenu', 'siteUserMenu');
function siteUserMenu(Am_Event $event)
{
      // $user = $event->getUser(); // if required
      $menu = $event->getMenu();
 
       // Remove a tab
       $page = $menu->findOneBy('id', 'add-renew');
       $menu->removePage($page);

       // Remove a tab
       $page = $menu->findOneBy('id', 'payment-history');
       $menu->removePage($page);
 
       // Remove a tab
       $page = $menu->findOneBy('id', 'profile');
       $menu->removePage($page);

       // Remove a tab
       $page = $menu->findOneBy('id', 'member');
       $menu->removePage($page);
}

?>

