<?php

/**
 * Plugin allow admin to set up some discount
 * if user buy defined number of products at once.
 *
 */

class Am_Plugin_BundleDiscount extends Am_Plugin
{
    const PLUGIN_STATUS = self::STATUS_BETA;
    const PLUGIN_COMM = self::COMM_COMMERCIAL;
    const PLUGIN_REVISION = '4.3.6';

    protected $_configPrefix = 'misc.';

    public function onAdminMenu(Am_Event $event)
    {
        $page = $event->getMenu()->findOneBy('id', 'products');

        $page->addPage(array(
            'id' => 'bundle-discount',
            'uri' => REL_ROOT_URL . '/admin-bundle-discount',
            'label' => ___('Bundle Discount'),
            'resource' => "grid_product"
        ));
    }

    function onInvoiceGetCalculators(Am_Event_InvoiceGetCalculators $event)
    {
        if ($bundleDiscount = $this->getDi()->bundleDiscountTable->match($event->getInvoice())) {
            $event->insertBeforeTax(new Am_Invoice_Calc_BundleDiscount($bundleDiscount));
        }
    }

    public static function activate($id, $pluginType)
    {
        try
        {
            Am_Di::getInstance()->db->query("CREATE TABLE ?_bundle_discount (
                `bd_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `priority` INT NOT NULL,
                `condition` INT NOT NULL,
                `is_count_free` TINYINT NOT NULL,
                `discount` DECIMAL(12,2),
                `discount_type` ENUM('percent','number'),
                `comment` VARCHAR(255),
                `is_disabled` TINYINT NOT NULL
                ) CHARACTER SET utf8 COLLATE utf8_general_ci");
        }
        catch (Am_Exception_Db $e)
        {

        }

        return parent::activate($id, $pluginType);
    }
}

class AdminBundleDiscountController extends Am_Controller_Grid
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->setActiveMenu('bundle-discount');
    }

    public function checkAdminPermissions(Admin $admin)
    {
        return $admin->hasPermission("grid_product");
    }

    public function createGrid()
    {
        $ds = new Am_Query($this->getDi()->bundleDiscountTable);
        $ds = $ds->addOrder('priority', true);
        $grid = new Am_Grid_Editable('_bd', 'Bundle Discount', $ds, $this->_request, $this->view, $this->getDi());
        $grid->setRecordTitle(___('Discount Rule'));
        $grid->addField('priority', ___('Priority'), true, null, null, '5%');
        $grid->addField('condition', ___('Condition'), true, '', array($this, 'renderCondition'), '5%');
        $grid->addField('discount', ___('Discount'), true, '', array($this, 'renderDiscount'), '5%');
        $grid->addField('comment', 'Comment');
        $grid->addField(new Am_Grid_Field_IsDisabled());
        $grid->setForm(array($this, 'createForm'));
        $grid->actionAdd(new Am_Grid_Action_LiveEdit('comment'));
        $grid->actionAdd(new Am_Grid_Action_LiveEdit('priority'));
        $grid->actionAdd(new Am_Grid_Action_TestBundleDiscount);
        return $grid;
    }

    function renderDiscount($record)
    {
        return sprintf("<td>%s&nbsp;%s</td>",
            $record->discount,
            ($record->discount_type == BundleDiscount::DISCOUNT_PERCENT ? '%' : $this->getDi()->config->get('currency'))
        );
    }

    function renderCondition($record)
    {
        return sprintf("<td><strong>%s</strong>%s</td>",
            $record->condition,
            ($record->is_count_free ? ' (including free items)' : '')
        );
    }

    function createForm()
    {

        $form = new Am_Form_Admin('bundle-discount');

        $form->addText('priority')
            ->setLabel(array(___('Priority of discount'), ___('First match discount with higher priority will be applied')));

        $form->addText('condition')
            ->setLabel(___('Minimum Number of Items in invoice to apply discount'));

        $form->addAdvCheckbox('is_count_free')
            ->setLabel(___('Count Free item to match discount?'));

        $discountGroup = $form->addGroup()
                ->setSeparator(' ')
                ->setLabel(___("Discount\nthis discount will be applied to total of invoice. " .
                        "In case of discount is more than total - discount will be ignored. " .
                        "if user use coupon with purchase - max discount will be used"));
        $discountGroup->addText('discount', array('size' => 5))
            ->addRule('gt', 'must be greater than 0', 0);
        $discountGroup->addSelect('discount_type')
            ->loadOptions(array(
                BundleDiscount::DISCOUNT_PERCENT => '%',
                BundleDiscount::DISCOUNT_NUMBER => Am_Currency::getDefault()
            ));

        $form->addTextarea('comment', array('class'=>'el-wide'))
            ->setLabel(___("Comment\nfor admin reference"));

        return $form;
    }
}

class Am_Grid_Action_TestBundleDiscount extends Am_Grid_Action_Abstract
{

    protected $type = Am_Grid_Action_Abstract::NORECORD;
    protected $title = 'Test Discount Rules';

    public function run()
    {
        $f = $this->createForm();
        $f->setDataSources(array($this->grid->getCompleteRequest()));
        echo $this->renderTitle();
        if ($f->isSubmitted() && $f->validate() && $this->process($f))
            return;
        echo $f;
    }

    function process(Am_Form $f)
    {
        $vars = $f->getValue();
        $user = Am_Di::getInstance()->userTable->findFirstByLogin($vars['user']);
        if (!$user) {
            list($el) = $f->getElementsByName('user');
            $el->setError(___('User %s not found', $vars['user']));
            return false;
        }
        /* @var $invoice Invoice */
        $invoice = Am_Di::getInstance()->invoiceTable->createRecord();
        $invoice->setUser($user);
        if ($vars['coupon'])
        {
            $invoice->setCouponCode($vars['coupon']);
            $error = $invoice->validateCoupon();
            if ($error)
                throw new Am_Exception_InputError($error);
        }
        foreach ($vars['product_id'] as $plan_id => $qty)
        {
            $p = Am_Di::getInstance()->billingPlanTable->load($plan_id);
            $pr = $p->getProduct();
            $invoice->add($pr, $qty);
        }

        echo "<pre>";
        echo "Choosen Discount Rule:\n";

        if ($bd = Am_Di::getInstance()->bundleDiscountTable->match($invoice))
        {
            printf("* %s&nbsp;%s %s\n",
                $bd->discount,
                ($bd->discount_type == BundleDiscount::DISCOUNT_PERCENT ?
                    '%' :
                    Am_Di::getInstance()->config->get('currency')),
                $bd->comment
            );
        } else {
            echo "* None";
        }

        echo "\n\n";

        $invoice->paysys_id = 'manual';
        $invoice->calculate();

        $invoice->invoice_id = '00000';
        $invoice->public_id = 'TEST';
        $invoice->tm_added = sqlTime('now');

        echo $invoice->render();
        echo
        "\nBilling Terms: " . $invoice->getTerms() .
        "\n" . str_repeat("-", 70) . "\n";
        echo "</pre>";
        return true;
    }

    protected function createForm()
    {
        $f = new Am_Form_Admin;
        $f->addText('user')->setLabel('Enter username of existing user')
            ->addRule('required', 'This field is required');
        $f->addText('coupon')->setLabel('Enter coupon code or leave field empty');

        $f->addElement(new Am_Form_Element_ProductsWithQty('product_id'))
            ->setLabel(___('Choose products to include into test invoice'))
            ->loadOptions(Am_Di::getInstance()->billingPlanTable->selectAllSorted())
            ->addRule('required');
        $f->addSubmit('', array('value' => 'Test'));
        $f->addScript()->setScript(<<<CUT
$(function(){
    $("#user-0" ).autocomplete({
        minLength: 2,
        source: window.rootUrl + "/admin-users/autocomplete"
    });
});
CUT
        );
        foreach ($this->grid->getVariablesList() as $k)
        {
            $kk = $this->grid->getId() . '_' . $k;
            if ($v = @$_REQUEST[$kk])
                $f->addHidden($kk)->setValue($v);
        }
        return $f;
    }

}

class BundleDiscountTable extends Am_Table
{
    protected $_table = '?_bundle_discount';
    protected $_recordClass = 'BundleDiscount';
    protected $_key = 'bd_id';

    function match(Invoice $invoice)
    {
        $cntAll = $cntPaid = 0;

        foreach ($invoice->getItems() as $item)
        {
            $cntAll += $item->qty;
            $cntPaid += ( (float) $item->first_price ? $item->qty : 0);
        }

        foreach ($this->findBy(array('is_disabled' => 0), null, null, 'priority DESC') as $record)
        {
            if ($record->condition <= ($record->is_count_free ? $cntAll : $cntPaid))
                return $record;
        }
    }
}

/**
 * @property int $bd_id
 * @property string $comment
 * @property int $condition
 * @property float $discount
 * @property enum $discount_type
 * @property bool $is_disabled
 * @property bool $is_count_free
 */
class BundleDiscount extends Am_Record
{
    const DISCOUNT_NUMBER = 'number';
    const DISCOUNT_PERCENT = 'percent';
}

class Am_Invoice_Calc_BundleDiscount extends Am_Invoice_Calc
{
    protected $bundelDiscount = null;

    public function __construct(BundleDiscount $bundelDiscount)
    {
        $this->bundelDiscount = $bundelDiscount;
    }

    public function calculate(Invoice $invoiceBill)
    {
        $bundelDiscount = $this->bundelDiscount;

        if ($bundelDiscount->discount_type == Coupon::DISCOUNT_PERCENT)
        {
            foreach ($invoiceBill->getItems() as $item)
            {
                $first_discount = moneyRound($item->first_price * $item->qty * $bundelDiscount->discount / 100);
                $item->first_discount = max($item->first_discount, $first_discount);
            }
        } else { // absolute discount
            $total = 0;
            foreach ($invoiceBill->getItems() as $item)
            {
                $total += $item->first_price * $item->qty;
            }

            $used = 0.00;
            $items = array_filter($invoiceBill->getItems(), create_function('$item', 'return (float)$item->first_price;'));
            while (count($items)) {
                $item = array_shift($items);
                if (count($items) == 0) {//last item, apply unused discount to it
                    $first_discount = max($item->first_discount, moneyRound($bundelDiscount->discount - $used));
                } else {
                    $discountPerItem = moneyRound(($bundelDiscount->discount/100) * (($item->first_price * $item->qty * 100)/$total));
                    $first_discount = max($item->first_discount, moneyRound($discountPerItem * $item->qty));
                    $used += moneyRound($discountPerItem * $item->qty);
                }

                $item->first_discount = $item->first_price * $item->qty >= $first_discount ? $first_discount : $item->first_discount;
            }
        }
        foreach ($invoiceBill->getItems() as $item)
        {
            $item->_calculateTotal();
        }
    }
}